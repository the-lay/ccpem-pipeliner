#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
from gemmi import cif

from pipeliner.starfile_handler import (
    StarFile,
    BodyFile,
    JobStar,
    DataStarFile,
    PipelineStarFile,
    compare_starfiles,
)
from pipeliner_tests import test_data
from pipeliner.process import Process
from pipeliner.nodes import Node
from pipeliner.data_structure import JOBSTATUS_SUCCESS


class TestingToolsTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_dir = tempfile.mkdtemp(prefix="relion_")
        self.test_data = os.path.dirname(test_data.__file__)

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @staticmethod
    def compare_two_files(file1, file2):
        with open(file1) as f:
            f1_lines = f.readlines()
        with open(file2) as f:
            f2_lines = f.readlines()
        diffs = []

        cleanf1, cleanf2 = [], []
        for line in f1_lines:
            if line.split():
                cleanf1.append(line.split())
        for line in f2_lines:
            if line.split():
                cleanf2.append(line.split())

        for n, oline in enumerate(cleanf1):
            try:
                if oline != cleanf2[n]:
                    diffs.append([oline, cleanf2[n]])
            except IndexError:
                diffs.append([oline, "<no line>"])
        return diffs

    def test_creating_StarFile_object(self):
        """Check reading a motioncorr file where the data labels
        have been changed to avoid using reserved words"""
        f = os.path.join(self.test_data, "StarFiles/motioncorr_changed.star")
        shutil.copy(f, os.path.join(self.test_dir, "thefile.star"))
        obj = StarFile("thefile.star")
        assert obj.file_name == "thefile.star"
        assert not os.path.isfile("thefile.star.old")

    def test_creating_StarFile_object_with_quotated_reserved_words_errors(self):
        """Check read a files that contains reserved words as labels
        but they have been quotated so they don't need to be corrected"""
        f = os.path.join(self.test_data, "StarFiles/motioncorr_quotated.star")
        shutil.copy(f, os.path.join(self.test_dir, "thefile.star"))
        obj = StarFile("thefile.star")
        assert obj.file_name == "thefile.star"
        assert not os.path.isfile("thefile.star.old")

    def test_creating_StarFile_object_with_reserved_words_errors(self):
        file = os.path.join(
            self.test_data, "StarFiles/motioncorr_not_quotated_fail.star"
        )
        shutil.copy(file, "with_errors.star")
        obj = StarFile("with_errors.star")
        assert os.path.isfile("with_errors.star.old")
        assert obj.file_name == "with_errors.star"
        diffs = self.compare_two_files("with_errors.star.old", "with_errors.star")
        assert diffs == [
            [["save_noDW", "No"], ['"save_noDW"', "No"]],
            [["save_ps", "Yes"], ['"save_ps"', "Yes"]],
        ]

    def test_creating_BodyFile(self):
        """Test reading a body file for multibody refinement"""
        f = os.path.join(self.test_data, "JobFiles/MultiBody/bodyfile.star")
        bodyfile = BodyFile(f)
        assert bodyfile.file_name == f
        assert bodyfile.bodycount == 3

    def test_creating_JobStar(self):
        file = os.path.join(self.test_data, "JobFiles/Fetch/fetch_pdb_ent_job.star")
        shutil.copy(file, "thefile_job.star")
        obj = JobStar("thefile_job.star")
        assert obj.file_name == "thefile_job.star"
        assert obj.joboptions == {"db_id": "7NDO", "format": "pdb"}
        assert obj.jobtype == "pipeliner.fetch.pdb"
        assert not obj.is_tomo
        assert not obj.is_continue

    def test_writing_from_JobStar(self):
        file = os.path.join(self.test_data, "JobFiles/Fetch/fetch_pdb_ent_job.star")
        shutil.copy(file, "thefile_job.star")
        obj = JobStar("thefile_job.star")
        obj.is_continue = True
        obj.joboptions["db_id"] = "mbili"
        obj.write("altered.star")
        new = JobStar("altered.star")
        print(new.__dict__)
        assert new.is_continue
        assert not new.is_tomo
        assert new.joboptions["db_id"] == "mbili"
        assert new.joboptions["format"] == "pdb"

    def test_writing_from_JobStar_overwrite(self):
        file = os.path.join(self.test_data, "JobFiles/Fetch/fetch_pdb_ent_job.star")
        shutil.copy(file, "thefile_job.star")
        obj = JobStar("thefile_job.star")
        obj.is_continue = True
        obj.joboptions["db_id"] = "mbili"
        obj.write()
        new = JobStar("thefile_job.star")
        assert new.is_continue
        assert not new.is_tomo
        assert new.joboptions["db_id"] == "mbili"
        assert new.joboptions["format"] == "pdb"

    def test_writing_from_JobStar_direct_instantiation(self):

        obj = JobStar()
        obj.is_continue = True
        obj.is_tomo = False
        obj.jobtype = "fake.job"
        obj.joboptions["db_id"] = "mbili"
        obj.write()
        new = JobStar("job.star")
        assert new.is_continue
        assert not new.is_tomo
        assert new.joboptions["db_id"] == "mbili"

    def test_modify_JobStar(self):
        file = os.path.join(self.test_data, "JobFiles/Fetch/fetch_pdb_ent_job.star")
        shutil.copy(file, "thefile_job.star")
        obj = JobStar("thefile_job.star")
        obj.modify({"_rlnJobIsContinue": "1", "db_id": "moja", "format": "mmcif"})
        obj.write("altered.star")
        new = JobStar("altered.star")
        assert new.is_continue
        assert not new.is_tomo
        assert new.joboptions["db_id"] == "moja"
        assert new.joboptions["format"] == "mmcif"

    def test_modify_JobStar_no_adding_allowed(self):
        with self.assertRaises(ValueError):
            file = os.path.join(self.test_data, "JobFiles/Fetch/fetch_pdb_ent_job.star")
            shutil.copy(file, "thefile_job.star")
            obj = JobStar("thefile_job.star")
            obj.modify(
                {
                    "_rlnJobIsContinue": "1",
                    "db_id": "moja",
                    "format": "mmcif",
                    "mbili": "tatu",
                }
            )

    def test_modify_JobStar_with_adding(self):
        file = os.path.join(self.test_data, "JobFiles/Fetch/fetch_pdb_ent_job.star")
        shutil.copy(file, "thefile_job.star")
        obj = JobStar("thefile_job.star")
        obj.modify(
            {
                "_rlnJobIsContinue": "1",
                "db_id": "moja",
                "format": "mmcif",
                "mbili": "tatu",
            },
            allow_add=True,
        )
        obj.write("altered.star")
        new = JobStar("altered.star")
        assert new.is_continue
        assert not new.is_tomo
        assert new.joboptions["db_id"] == "moja"
        assert new.joboptions["format"] == "mmcif"
        assert new.joboptions["mbili"] == "tatu"

    def test_DataStarFile_creation(self):
        f = os.path.join(self.test_data, "class2d_data.star")
        shutil.copy(f, os.path.join(self.test_dir, "data.star"))
        obj = DataStarFile("data.star")
        print(obj.__dict__)
        assert obj.file_name == "data.star"
        assert type(obj.data) == cif.Document

    def test_DataStarFile_count_block(self):
        f = os.path.join(self.test_data, "class2d_data.star")
        shutil.copy(f, os.path.join(self.test_dir, "data.star"))
        obj = DataStarFile("data.star")
        assert obj.count_block("optics") == 1
        assert obj.count_block("particles") == 2377

    def test_DataStarFile_get_block(self):
        f = os.path.join(self.test_data, "class2d_data.star")
        shutil.copy(f, os.path.join(self.test_dir, "data.star"))
        obj = DataStarFile("data.star")
        block = obj.get_block("particles")
        assert type(block) == cif.Block
        loop = block.find_loop("_rlnCoordinateX")
        assert len(loop) == 2377
        assert loop[0] == "1614.217657"
        assert loop[-1] == "1083.670595"

    def test_DataStarFile_list_from_column(self):
        f = os.path.join(self.test_data, "class2d_data.star")
        shutil.copy(f, os.path.join(self.test_dir, "data.star"))
        obj = DataStarFile("data.star")
        col = obj.column_as_list(blockname="particles", colname="_rlnCoordinateX")
        assert len(col) == 2377
        assert col[0] == "1614.217657"
        assert col[-1] == "1083.670595"

    def test_PipelineStarFile_creation(self):
        f = os.path.join(self.test_data, "Pipelines/relion40_tutorial.star")
        shutil.copy(f, "default_pipeline.star")
        obj = PipelineStarFile("default_pipeline.star")
        assert obj.file_name == "default_pipeline.star"
        assert len(obj.procs) == 31
        for i in obj.procs:
            assert type(i) == Process
        assert len(obj.nodes) == 76
        for i in obj.nodes:
            assert type(i) == Node
        assert len(obj.output_edges) == 31
        for i in obj.output_edges:
            assert type(i) == Process
            for node in obj.output_edges[i]:
                assert type(node) == Node
        assert len(obj.outputs) == 31
        for i in obj.outputs:
            assert type(i) == str
            for node in obj.outputs[i]:
                assert type(node) == Node
        assert len(obj.input_edges) == 27
        for i in obj.input_edges:
            assert type(i) == Node
            for proc in obj.input_edges[i]:
                assert type(proc) == Process
        assert len(obj.inputs) == 27
        for i in obj.inputs:
            assert type(i) == str
            for proc in obj.inputs[i]:
                assert type(proc) == Process

        assert obj.pipeline_version == "relion4"

        test_outputs = obj.outputs["LocalRes/job031/"]
        assert type(test_outputs[0]) == Node
        assert test_outputs[0].name == "LocalRes/job031/histogram.pdf"
        assert test_outputs[0].kwds == ["relion", "localres"]
        assert test_outputs[0].type == "LogFile.pdf.relion.localres"
        assert test_outputs[0].format == "pdf"
        assert test_outputs[0].output_from_process.name == "LocalRes/job031/"
        assert test_outputs[0].input_for_processes_list == []

        assert type(test_outputs[1]) == Node
        assert test_outputs[1].name == "LocalRes/job031/relion_locres_filtered.mrc"
        assert test_outputs[1].kwds == ["relion", "localresfiltered"]
        assert test_outputs[1].type == "DensityMap.mrc.relion.localresfiltered"
        assert test_outputs[1].format == "mrc"
        assert test_outputs[1].output_from_process.name == "LocalRes/job031/"
        assert test_outputs[1].input_for_processes_list == []

        assert type(test_outputs[2]) == Node
        assert test_outputs[2].name == "LocalRes/job031/relion_locres.mrc"
        assert test_outputs[2].kwds == ["resmap", "localresmap"]
        assert test_outputs[2].type == "Image3D.mrc.resmap.localresmap"
        assert test_outputs[2].format == "mrc"
        assert test_outputs[2].output_from_process.name == "LocalRes/job031/"
        assert test_outputs[2].input_for_processes_list == []

        test_inputs = obj.inputs["Refine3D/job025/run_data.star"]
        assert type(test_inputs[0]) == Process
        assert test_inputs[0].name == "Polish/job027/"
        assert not test_inputs[0].alias
        assert test_inputs[0].outdir == "Polish"
        assert test_inputs[0].type == "relion.polish"
        assert test_inputs[0].status == JOBSTATUS_SUCCESS

        assert type(test_inputs[1]) == Process
        assert test_inputs[1].name == "Polish/job028/"
        assert not test_inputs[1].alias
        assert test_inputs[1].outdir == "Polish"
        assert test_inputs[1].type == "relion.polish"
        assert test_inputs[1].status == JOBSTATUS_SUCCESS

    def test_star_loop_as_list_all_single_row(self):
        test_file = StarFile(os.path.join(self.test_data, "corrected_micrographs.star"))
        found = test_file.loop_as_list("optics")
        expected = (
            [
                "_rlnOpticsGroupName",
                "_rlnOpticsGroup",
                "_rlnMtfFileName",
                "_rlnMicrographOriginalPixelSize",
                "_rlnVoltage",
                "_rlnSphericalAberration",
                "_rlnAmplitudeContrast",
                "_rlnMicrographPixelSize",
            ],
            [
                [
                    "opticsGroup1",
                    "1",
                    "mtf_k2_200kV.star",
                    "0.885000",
                    "200.000000",
                    "1.400000",
                    "0.100000",
                    "0.885000",
                ]
            ],
        )
        assert found == expected

    def test_star_loop_as_list_all_multi_rows(self):
        test_file = StarFile(os.path.join(self.test_data, "corrected_micrographs.star"))
        found = test_file.loop_as_list("micrographs")
        columns = [
            "_rlnCtfPowerSpectrum",
            "_rlnMicrographName",
            "_rlnMicrographMetadata",
            "_rlnOpticsGroup",
            "_rlnAccumMotionTotal",
            "_rlnAccumMotionEarly",
            "_rlnAccumMotionLate",
        ]

        assert found[0] == columns
        assert len(found[1]) == 24
        for i in found[1]:
            assert len(i) == 7

    def test_star_loop_as_list_specific_multi_rows(self):
        test_file = StarFile(os.path.join(self.test_data, "corrected_micrographs.star"))
        columns = [
            "_rlnCtfPowerSpectrum",
            "_rlnMicrographName",
            "_rlnMicrographMetadata",
        ]
        found = test_file.loop_as_list("micrographs", columns)
        assert found[0] == columns
        assert len(found[1]) == 24
        for i in found[1]:
            assert len(i) == 3

    def test_star_loop_as_list_block_error(self):
        test_file = StarFile(os.path.join(self.test_data, "corrected_micrographs.star"))
        columns = [
            "_rlnCtfPowerSpectrum",
            "_rlnMicrographName",
            "_rlnMicrographMetadata",
        ]
        with self.assertRaises(ValueError):
            test_file.loop_as_list("bad", columns)

    def test_star_loop_as_list_column_error(self):
        test_file = StarFile(os.path.join(self.test_data, "corrected_micrographs.star"))
        columns = ["bad_column"]
        with self.assertRaises(ValueError):
            test_file.loop_as_list("micrographs", columns)

    def test_star_loop_as_list_wrong_blocktype_error(self):
        test_file = StarFile(
            os.path.join(self.test_data, "JobFiles/Import/import_job.star")
        )
        with self.assertRaises(ValueError):
            test_file.loop_as_list("job")

    def test_star_pairs_as_dict(self):
        test_file = StarFile(
            os.path.join(self.test_data, "JobFiles/Import/import_job.star")
        )
        actual = test_file.pairs_as_dict("job")
        expected = {
            "_rlnJobTypeLabel": "relion.Import.Movies",
            "_rlnJobIsContinue": "0",
            "_rlnJobIsTomo": "0",
        }
        assert actual == expected

    def test_star_pairs_as_dict_error_not_valuepair(self):
        test_file = StarFile(os.path.join(self.test_data, "corrected_micrographs.star"))
        with self.assertRaises(ValueError):
            test_file.pairs_as_dict("micrographs")

    def test_compare_starfiles_identical(self):
        test_file = os.path.join(self.test_data, "corrected_micrographs.star")
        test_file2 = test_file
        struct, data = compare_starfiles(test_file, test_file2)
        assert struct
        assert data

    def test_compare_starfiles_identical_blockorder_different(self):
        test_file = os.path.join(self.test_data, "corrected_micrographs.star")
        test_file2 = os.path.join(
            self.test_data, "corrected_micrographs_header_order.star"
        )
        struct, data = compare_starfiles(test_file, test_file2)
        assert struct
        assert data

    def test_compare_starfiles_both_different(self):
        test_file = os.path.join(self.test_data, "corrected_micrographs.star")
        test_file2 = os.path.join(self.test_data, "class2d_model.star")
        struct, data = compare_starfiles(test_file, test_file2)
        assert not struct
        assert not data

    def test_compare_starfiles_different_data(self):
        test_file = os.path.join(self.test_data, "corrected_micrographs.star")
        test_file2 = os.path.join(self.test_data, "corrected_micrographs2.star")

        struct, data = compare_starfiles(test_file, test_file2)
        assert struct
        assert not data

    if __name__ == "__main__":
        unittest.main()
