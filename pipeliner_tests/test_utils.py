#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import shutil
import os
import sys
import tempfile


from pipeliner.utils import (
    truncate_number,
    clean_jobname,
    smart_strip_quotes,
    run_subprocess,
    check_for_illegal_symbols,
    make_pretty_header,
    get_job_number,
    str_is_hex_colour,
)
from pipeliner.mrc_image_tools import get_mrc_dims
from pipeliner_tests import test_data


class PipelinerUtilsTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_truncate_number_seven_places(self):
        x = 1.66666666666667
        trunc = truncate_number(x, 7)
        assert trunc == "1.6666667", trunc

    def test_truncate_number_two_places(self):
        x = 1.66666666666667
        trunc = truncate_number(x, 2)
        assert trunc == "1.67", trunc

    def test_truncate_number_seven_places_trailing_zeros(self):
        x = 2.0
        trunc = truncate_number(x, 7)
        assert trunc == "2", trunc

    def test_truncate_number_two_places_with_two_places_input(self):
        x = 0.75
        trunc = truncate_number(x, 2)
        assert trunc == "0.75", trunc

    def test_truncate_number_with_int(self):
        x = 2
        trunc = truncate_number(x, 7)
        assert trunc == "2", trunc

    def test_truncate_number_with_large_int(self):
        x = 1234567890
        trunc = truncate_number(x, 7)
        assert trunc == "1234567890", trunc

    def test_truncate_number_with_near_multiple_of_ten(self):
        x = 1234567890.1
        trunc = truncate_number(x, 0)
        assert trunc == "1234567890", trunc

    def test_truncate_number_zero_places_rounding_up(self):
        x = 1.89
        trunc = truncate_number(x, 0)
        assert trunc == "2", trunc

    def test_truncate_number_zero_places_rounding_down(self):
        x = 1.324
        trunc = truncate_number(x, 0)
        assert trunc == "1", trunc

    def test_truncate_negative_number_zero_places_rounding_down(self):
        x = -1.324
        trunc = truncate_number(x, 0)
        assert trunc == "-1", trunc

    def test_truncate_negative_number_zero_places_rounding_up(self):
        x = -509.87
        trunc = truncate_number(x, 0)
        assert trunc == "-510", trunc

    def test_clean_jobname_with_trailing_slash(self):
        jobname = "Refine/job012/"
        cleaned = clean_jobname(jobname)
        assert cleaned == jobname

    def test_clean_jobname_without_trailing_slash(self):
        jobname = "Refine/job012"
        cleaned = clean_jobname(jobname)
        assert cleaned == "Refine/job012/"

    def test_clean_jobname_leading_slash(self):
        jobname = clean_jobname("/testjob/job003/")
        assert jobname == "testjob/job003/"

    def test_clean_jobname_double_slash(self):
        jobname = clean_jobname("testjob//job003/")
        assert jobname == "testjob/job003/"

    def test_clean_jobname_multiple_problems(self):
        jobname = clean_jobname("/testjob////////////job003")
        assert jobname == "testjob/job003/"

    def test_clean_jobname_illegal_character(self):
        with self.assertRaises(ValueError):
            clean_jobname("testjob/job@@3/")

    def test_get_mrc_dims_3d(self):
        dims = get_mrc_dims(os.path.join(self.test_data, "emd_3488.mrc"))
        assert dims == (100, 100, 100)

    def test_smart_strip_quotes_single(self):
        test_str = '"test"'
        result = smart_strip_quotes(test_str)
        assert result == "test"

    def test_smart_strip_quotes_double(self):
        test_str = "'test'"
        result = smart_strip_quotes(test_str)
        assert result == "test"

    def test_smart_strip_quotes_with_internal(self):
        test_str = '''"test with 'internal quotes'"'''
        result = smart_strip_quotes(test_str)
        assert result == "test with 'internal quotes'"

    def test_check_for_illegal_symbols_with_good_input(self):
        result = check_for_illegal_symbols("abc123")
        assert result is None

    def test_check_for_illegal_symbols_with_bad_input(self):
        result = check_for_illegal_symbols("bad string *")
        assert "'*' in input" in result

    def test_check_for_illegal_symbols_with_bad_alias(self):
        result = check_for_illegal_symbols("bad alias /", string_name="alias")
        assert "'/' in alias" in result

    def test_check_for_illegal_symbols_with_excluded_symbol(self):
        result = check_for_illegal_symbols("good pipeline /", exclude="*/!")
        assert result is None

    def test_make_pretty_header_main(self):
        header = make_pretty_header("the test text")
        assert header.split("\n") == [
            "-=-=-=-=-=-=-",
            "the test text",
            "-=-=-=-=-=-=-",
        ]

    def test_make_pretty_header_main_with_breaks(self):
        header = make_pretty_header("the test text\nis of varied lengths\nyes!")
        assert header.split("\n") == [
            "-=-=-=-=-=-=-=-=-=-=",
            "the test text",
            "is of varied lengths",
            "yes!",
            "-=-=-=-=-=-=-=-=-=-=",
        ]

    def test_make_pretty_header_single_char(self):
        header = make_pretty_header("the test text", char="-")
        assert header.split("\n") == [
            "-------------",
            "the test text",
            "-------------",
        ]

    def test_make_pretty_header_sub_with_breaks(self):
        header = make_pretty_header(
            "the test text\nis of varied lengths\nyes!", char="-", top=False
        )
        assert header.split("\n") == [
            "the test text",
            "is of varied lengths",
            "yes!",
            "--------------------",
        ]

    def test_make_pretty_header_long_char(self):
        header = make_pretty_header("the test text", char="12345678")
        assert header.split("\n") == [
            "1234567812345",
            "the test text",
            "1234567812345",
        ]

    def test_get_job_number_no_slash(self):
        jn = get_job_number("Import/job001")
        assert jn == 1

    def test_get_job_number_with_slash(self):
        jn = get_job_number("Import/job001/")
        assert jn == 1

    def test_str_is_hexcolour(self):
        assert str_is_hex_colour("#122334")
        assert not str_is_hex_colour("122334")
        assert str_is_hex_colour("#ffffff")
        assert not str_is_hex_colour("#abcdeh")
        assert not str_is_hex_colour("#ffff")


def test_run_subprocess_uses_normal_ld_path_outside_pyinstaller_mode(monkeypatch):
    monkeypatch.setenv("LD_LIBRARY_PATH", "test_ld_path")
    result = run_subprocess(
        ["bash", "-c", "echo -n $LD_LIBRARY_PATH"], capture_output=True, text=True
    )
    assert result.stdout == "test_ld_path"


def test_run_subprocess_does_not_set_ld_path_outside_pyinstaller_mode(monkeypatch):
    monkeypatch.delenv("LD_LIBRARY_PATH", raising=False)
    result = run_subprocess(
        ["bash", "-c", "echo -n $LD_LIBRARY_PATH"], capture_output=True, text=True
    )
    assert result.stdout == ""


def test_run_subprocess_unsets_ld_path_in_pyinstaller_bundle_mode(monkeypatch):
    # Pretend we're running in a Pyinstaller bundle
    # See https://pyinstaller.org/en/stable/runtime-information.html
    monkeypatch.setattr(sys, "frozen", True, raising=False)
    monkeypatch.setattr(sys, "_MEIPASS", "", raising=False)
    monkeypatch.setenv("LD_LIBRARY_PATH", "test_ld_path")
    result = run_subprocess(
        ["bash", "-c", "echo -n $LD_LIBRARY_PATH"], capture_output=True, text=True
    )
    assert result.stdout == ""


def test_run_subprocess_uses_orig_ld_path_in_pyinstaller_bundle_mode(monkeypatch):
    # Pretend we're running in a Pyinstaller bundle
    # See https://pyinstaller.org/en/stable/runtime-information.html
    monkeypatch.setattr(sys, "frozen", True, raising=False)
    monkeypatch.setattr(sys, "_MEIPASS", "", raising=False)
    monkeypatch.setenv("LD_LIBRARY_PATH", "test_ld_path")
    monkeypatch.setenv("LD_LIBRARY_PATH_ORIG", "orig_ld_path")
    result = run_subprocess(
        ["bash", "-c", "echo -n $LD_LIBRARY_PATH"], capture_output=True, text=True
    )
    assert result.stdout == "orig_ld_path"


if __name__ == "__main__":
    unittest.main()
