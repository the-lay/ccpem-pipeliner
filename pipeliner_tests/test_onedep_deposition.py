#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
import tempfile
import shutil
import unittest
from dataclasses import asdict

from pipeliner_tests import test_data
from pipeliner.deposition_tools.onedep_deposition import merge
from pipeliner.deposition_tools.pdb_deposition_objects import (
    SoftwareType,
    software_type_entry,
    OtherType,
    other_type_entry,
    PhaseReversalType,
    phase_reversal_type_entry,
    AmplitudeCorrectionType,
    amplitude_correction_type_entry,
    RandomConicalTiltType,
    random_conical_tilt_type_entry,
    OrthogonalTiltType,
    orthogonal_tilt_type_entry,
    EmdbType,
    emdb_type_entry,
    PdbChainType,
    pdb_chain_type_entry,
    PdbModelType,
    pdb_model_type_entry,
    ProjectionMatchingProcessingType,
    projection_matching_processing_type_entry,
    DimensionsType,
    dimensions_type_entry,
    BackgroundMaskType,
    background_mask_type_entry,
    SpatialFilteringType,
    spatial_filtering_type_entry,
    SharpeningType,
    sharpening_type_entry,
    BfSharpeningType,
    bf_sharpening_type_entry,
    ReconstructionFilteringType,
    reconstruction_filtering_type_entry,
    HelicalParamsType,
    helical_params_type_entry,
    SymmetryType,
    symmetry_type_entry,
    InitialAngleType,
    initial_angle_type_entry,
    FinalAngleType,
    final_angle_type_entry,
    final_multi_reference_alignment_type_entry,
    final_2d_classification_type_entry,
    final_3d_classification_type_entry,
    FinalReconstructionType,
    final_reconstruction_type_entry,
    InSilicoStartupModelType,
    in_silico_startup_model_type_entry,
    OtherStartupModelType,
    other_startup_model_type_entry,
    RandomConicalTiltStartupModelType,
    random_conical_tilt_startup_model_type_entry,
    EmdbStartupModelType,
    emdb_startup_model_type_entry,
    PdbModelStartupModelType,
    pdb_model_startup_model_type_entry,
    CtfCorrectionType,
    ctf_correction_type_entry,
    ParticleSelectionType,
    particle_selection_type_entry,
)


class OneDepTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)
        os.mkdir("CtfRefine")
        os.mkdir("CtfRefine/job018")

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_default_SoftwareType(self):
        sf = SoftwareType().check()
        print(sf)
        sfe = sf.software_type
        for att in [
            sfe.name,
            sfe.version,
            sfe.processing_details,
        ]:
            assert att is None

    def test_SoftwareType(self):
        name = "test software"
        version = "1.0"
        processing_details = "I did stuff"
        sf = (
            software_type_entry(
                name=name, version=version, processing_details=processing_details
            )
            .check()
            .software_type
        )

        assert sf.name == name
        assert sf.version == version
        assert sf.processing_details == processing_details

    def test_OtherType(self):
        software_list = (SoftwareType(), SoftwareType())
        details = "Some stuff"
        ot = (
            other_type_entry(software_list=software_list, details=details).check().other
        )

        assert ot.software_list == software_list
        assert ot.details == details

    def test_defalt_OtherType(self):
        ot = OtherType().check().other

        assert ot.software_list == ()
        assert ot.details is None

    def test_default_PhaseReversalType(self):
        pr = phase_reversal_type_entry().check().phase_reversal
        assert pr.anisotropic is None
        assert pr.correction_space is None

    def test_PhaseReversalType(self):
        aniso = True
        corr_space = "real"
        pr = (
            phase_reversal_type_entry(anisotropic=aniso, correction_space=corr_space)
            .check()
            .phase_reversal
        )

        assert pr.anisotropic == aniso
        assert pr.correction_space == corr_space

    def test_PhaseReversalType_error(self):
        aniso = True
        corr_space = "magic"
        with self.assertRaises(ValueError):
            phase_reversal_type_entry(
                anisotropic=aniso, correction_space=corr_space
            ).check()

    def test_default_AmplitudeCorrectionType(self):
        at = AmplitudeCorrectionType().check().amplitude_correction

        assert at.factor is None
        assert at.correction_space is None

    def test_AmplitudeCorrectionType(self):
        factor = 1.0
        corr_space = "reciprocal"
        at = amplitude_correction_type_entry(
            factor=factor, correction_space=corr_space
        ).check()
        ata = at.amplitude_correction
        print(asdict(ata))
        assert ata.factor == factor
        assert ata.correction_space is corr_space

    def test_AmplitudeCorrectionType_error(self):
        factor = 1.0
        corr_space = "upsidedown"
        with self.assertRaises(ValueError):
            amplitude_correction_type_entry(
                factor=factor, correction_space=corr_space
            ).check()

    def test_default_CtfCorrectionType(self):
        cc = CtfCorrectionType().check().ctf_correction

        for i in [
            cc.phase_reversal,
            cc.amplitude_correction,
            cc.correction_operation,
            cc.details,
        ]:
            assert i is None
        assert cc.software_list == ()

    def test_CtfCorrectionType(self):
        pr = PhaseReversalType()
        ac = AmplitudeCorrectionType()
        co = "multiplication"
        sol = (SoftwareType(), SoftwareType())
        details = "did some stuff"

        cc = (
            ctf_correction_type_entry(
                phase_reversal=pr,
                amplitude_correction=ac,
                correction_operation=co,
                software_list=sol,
                details=details,
            )
            .check()
            .ctf_correction
        )
        assert cc.phase_reversal == pr
        assert cc.amplitude_correction == ac
        assert cc.correction_operation == co
        assert cc.details == details
        assert cc.software_list == sol

    def test_default_ParticleSelectionType(self):
        ps = ParticleSelectionType().check().particle_selection

        for i in [
            ps.number_selected,
            ps.reference_model,
            ps.method,
            ps.details,
        ]:
            assert i is None

        assert ps.software == ()

    def test_ParticleSelectionType(self):
        ns = 100000
        rm = "my model"
        meth = "the way I did it"
        software = (SoftwareType(),)
        details = "them deets"
        ps = (
            particle_selection_type_entry(
                number_selected=ns,
                reference_model=rm,
                method=meth,
                software=software,
                details=details,
            )
            .check()
            .particle_selection
        )

        assert ps.number_selected == ns
        assert ps.reference_model == rm
        assert ps.method == meth
        assert ps.software == software
        assert ps.details == details

    def test_default_RandomConicalTiltType(self):
        rt = RandomConicalTiltType().check().random_conical_tilt

        assert rt.software_list == ()
        for i in [rt.number_images, rt.tilt_angle, rt.details]:
            assert i is None

    def test_RandomConicalTiltType(self):
        ni = 10
        ta = 76.8
        sl = [SoftwareType(), SoftwareType()]
        details = "som deets"

        rt = (
            random_conical_tilt_type_entry(
                number_images=ni,
                tilt_angle=ta,
                software_list=sl,
                details=details,
            )
            .check()
            .random_conical_tilt
        )

        assert rt.software_list == sl
        assert rt.number_images == ni
        assert rt.tilt_angle == ta
        assert rt.details == details

    def test_default_OrthogonalTiltType(self):
        ot = OrthogonalTiltType().check().orthogonal_tilt

        for i in [
            ot.number_images,
            ot.tilt_angle1,
            ot.tilt_angle2,
            ot.details,
        ]:
            assert i is None
        assert ot.software_list == ()

    def test_OrthogonalTiltType(self):
        ni = 100
        ta1 = 180.0
        ta2 = 179.9
        sl = (SoftwareType(), SoftwareType())
        details = "deez deets"
        ot = (
            orthogonal_tilt_type_entry(
                number_images=ni,
                tilt_angle1=ta1,
                tilt_angle2=ta2,
                software_list=sl,
                details=details,
            )
            .check()
            .orthogonal_tilt
        )

        assert ot.number_images == ni
        assert ot.tilt_angle1 == ta1
        assert ot.tilt_angle2 == ta2
        assert ot.details == details
        assert ot.software_list == sl

    def test_default_EmdbType(self):
        et = EmdbType().check().emdb_id

        assert et.emdb_id is None

    def test_EmdbType(self):
        et = emdb_type_entry(emdb_id="emdb-27636").check().emdb_id
        assert et.emdb_id == "emdb-27636"

    # TODO: test regex validation of EmdbType when it is added

    def test_default_PdbChainType(self):
        pt = PdbChainType().check().pdb_chain

        assert pt.chain_id is None
        assert pt.residue_range is None

    def test_PdbChainType(self):
        pt = (
            pdb_chain_type_entry(chain_id="A", residue_range="100-200")
            .check()
            .pdb_chain
        )

        assert pt.chain_id == "A"
        assert pt.residue_range == "100-200"

    def test_default_PdbModelType(self):
        pm = PdbModelType().check().pdb_model
        assert pm.pdb_id is None
        assert pm.chain_id_list == ()

    def test_PdbModelType(self):
        pdbid = "2G53G"
        chain_id = PdbChainType()
        pm = pdb_model_type_entry(pdb_id=pdbid, chain_id_list=(chain_id,))
        assert pm.pdb_model.pdb_id == pdbid
        assert pm.pdb_model.chain_id_list == (chain_id,)

    # TODO: add test for regex validation of PdbChainType when it is added

    def test_default_InSilicoStartupModelType(self):
        ist = InSilicoStartupModelType().check().in_silico_startup_model

        assert ist.startup_model is None
        assert ist.details is None

    def test_InSilicoStartupModelType(self):
        model = "my_model.mrc"
        details = "deets"
        ist = (
            in_silico_startup_model_type_entry(startup_model=model, details=details)
            .check()
            .in_silico_startup_model
        )

        assert ist.startup_model == model
        assert ist.details == details

    def test_default_OtherStartupModelType(self):
        ist = OtherStartupModelType().check().other_startup_model

        assert ist.startup_model is None
        assert ist.details is None

    def test_OtherStartupModelType(self):
        model = "my_model.mrc"
        details = "deets"
        ist = (
            other_startup_model_type_entry(startup_model=model, details=details)
            .check()
            .other_startup_model
        )

        assert ist.startup_model == model
        assert ist.details == details

    def test_default_RandomConicalTiltStartupModelType(self):
        ist = (
            RandomConicalTiltStartupModelType()
            .check()
            .random_conical_tilt_startup_model
        )
        print(asdict(ist))
        assert ist.startup_model is None
        assert ist.details is None

    def test_RandomConicalTiltStartupModelType(self):
        model = RandomConicalTiltType().check()
        details = "deets"
        ist = (
            random_conical_tilt_startup_model_type_entry(
                startup_model=model, details=details
            )
            .check()
            .random_conical_tilt_startup_model
        )

        assert ist.startup_model == model
        assert ist.details == details

    def test_default_EmdbStartupModelType(self):
        ist = EmdbStartupModelType().check().emdb_id_starting_model

        assert ist.startup_model is None
        assert ist.details is None

    def test_EmdbStartupModelType(self):
        model = EmdbType().check()
        details = "deets"
        ist = (
            emdb_startup_model_type_entry(startup_model=model, details=details)
            .check()
            .emdb_id_starting_model
        )
        assert ist.startup_model == model
        assert ist.details == details

    def test_default_PdbStartupModelType(self):
        ist = PdbModelStartupModelType().check().pdb_model_startup_model

        assert ist.startup_model is None
        assert ist.details is None

    def test_PdbStartupModelType(self):
        model = PdbModelType()
        details = "deets"
        ist = (
            pdb_model_startup_model_type_entry(startup_model=model, details=details)
            .check()
            .pdb_model_startup_model
        )

        assert ist.startup_model == model
        assert ist.details == details

    def test_default_ProjectionMatchingProcessingType(self):
        pmp = ProjectionMatchingProcessingType().check().projection_matching_processing

        for i in (
            pmp.number_reference_projections,
            pmp.merit_function,
            pmp.angular_sampling,
        ):
            assert i is None

    def test_ProjectionMatchingProcessingType(self):
        nr = 100
        mf = "function"
        asmp = 2.4
        pmp = (
            projection_matching_processing_type_entry(
                number_reference_projections=nr,
                merit_function=mf,
                angular_sampling=asmp,
            )
            .check()
            .projection_matching_processing
        )

        assert pmp.number_reference_projections == nr
        assert pmp.merit_function == mf
        assert pmp.angular_sampling == asmp

    def test_default_InitialAngleType(self):
        ia = InitialAngleType().check().initial_angle_assignment

        assert ia.type is None
        assert ia.projection_matching_processing is None
        assert ia.software_list == ()
        assert ia.details is None

    def test_InitialAngleType(self):
        atype = "ANGULAR RECONSTITUTION"
        pmp = ProjectionMatchingProcessingType()
        sw = (SoftwareType(), SoftwareType())
        details = "deez deets"
        ia = (
            initial_angle_type_entry(
                entry_type=atype,
                projection_matching_processing=pmp,
                software_list=sw,
                details=details,
            )
            .check()
            .initial_angle_assignment
        )

        assert ia.type == atype
        assert ia.projection_matching_processing == pmp
        assert ia.software_list == sw
        assert ia.details == details

    def test_default_FinalAngleType(self):
        ia = FinalAngleType().check().final_angle_assignment

        assert ia.type is None
        assert ia.projection_matching_processing is None
        assert ia.software_list == ()
        assert ia.details is None

    def test_FinalAngleType(self):
        atype = "ANGULAR RECONSTITUTION"
        pmp = ProjectionMatchingProcessingType()
        sw = (SoftwareType(), SoftwareType())
        details = "deez deets"
        ia = (
            final_angle_type_entry(
                entry_type=atype,
                projection_matching_processing=pmp,
                software_list=sw,
                details=details,
            )
            .check()
            .final_angle_assignment
        )

        assert ia.type == atype
        assert ia.projection_matching_processing == pmp
        assert ia.software_list == sw
        assert ia.details == details

    def test_FinalAngleType_error_badatype(self):
        """Invalid option for final angle assignment type"""
        atype = "INVALID CHOICE"
        pmp = ProjectionMatchingProcessingType()
        sw = (SoftwareType(), SoftwareType())
        details = "deez deets"
        with self.assertRaises(ValueError):
            final_angle_type_entry(
                entry_type=atype,
                projection_matching_processing=pmp,
                software_list=sw,
                details=details,
            ).check()

    def test_defaultFinalMultiReferenceAlignmentType(self):
        mrat = (
            final_multi_reference_alignment_type_entry().final_multi_reference_alignment
        )

        for i in [
            mrat.number_reference_projections,
            mrat.merit_function,
            mrat.angular_sampling,
            mrat.details,
        ]:
            assert i is None
        assert mrat.software_list == ()

    def test_FinalMultiReferenceAlignmentType(self):
        nrp = 420
        merit = "mymeritfunct"
        angs = 1.23
        details = "deetz"
        sf = (SoftwareType(),)
        mrat = (
            final_multi_reference_alignment_type_entry(
                number_reference_projections=nrp,
                merit_function=merit,
                angular_sampling=angs,
                details=details,
                software_list=sf,
            )
            .check()
            .final_multi_reference_alignment
        )

        assert mrat.number_reference_projections == nrp
        assert mrat.merit_function == merit
        assert mrat.angular_sampling == angs
        assert mrat.details == details
        assert mrat.software_list == sf

    def test_default_Final2DClassificationType(self):
        td = final_2d_classification_type_entry().check().final_two_d_classification

        assert td.number_classes is None
        assert td.average_number_members_per_class is None
        assert td.software_list == ()
        assert td.details is None

    def test_Final2DClassificationType(self):
        nc = 100
        anmc = 1234
        sf = [SoftwareType(), SoftwareType()]
        details = "test details"
        td = (
            final_2d_classification_type_entry(
                number_classes=nc,
                average_number_members_per_class=anmc,
                software_list=sf,
                details=details,
            )
            .check()
            .final_two_d_classification
        )

        assert td.number_classes == nc
        assert td.average_number_members_per_class == anmc
        assert td.software_list == sf
        assert td.details is details

    def test_default_Final3DClassificationType(self):
        td = final_3d_classification_type_entry().check().final_three_d_classification

        assert td.number_classes is None
        assert td.average_number_members_per_class is None
        assert td.software_list == ()
        assert td.details is None

    def test_Final3DClassificationType(self):
        nc = 100
        anmc = 1234
        sf = (SoftwareType(), SoftwareType())
        details = "test details"
        td = (
            final_3d_classification_type_entry(
                number_classes=nc,
                average_number_members_per_class=anmc,
                software_list=sf,
                details=details,
            )
            .check()
            .final_three_d_classification
        )

        assert td.number_classes == nc
        assert td.average_number_members_per_class == anmc
        assert td.software_list == sf
        assert td.details is details

    def test_default_DimensionsType(self):
        dt = DimensionsType().check().dimensions

        for i in (dt.radius, dt.width, dt.height, dt.depth):
            assert i is None

    def test_DimensionsType(self):
        r, h, w, d = 1.0, 1.0, 1.0, 1.0
        dt = (
            dimensions_type_entry(radius=r, height=h, width=w, depth=d)
            .check()
            .dimensions
        )
        for i in (dt.radius, dt.width, dt.height, dt.depth):
            assert i == 1.0

    def test_default_BackgroundMaskType(self):
        bgm = BackgroundMaskType().check().background_masked

        assert bgm.geometrical_shape is None
        assert bgm.dimensions is None
        assert bgm.software_list == ()
        assert bgm.details is None

    def test_BackgroundMaskType(self):
        gs = "SPHERE"
        dims = DimensionsType().check()
        sf = (SoftwareType(),)
        details = "deets"
        bgm = (
            background_mask_type_entry(
                geometrical_shape=gs, dimensions=dims, software_list=sf, details=details
            )
            .check()
            .background_masked
        )

        assert bgm.geometrical_shape == gs
        assert bgm.dimensions == dims
        assert bgm.software_list == sf
        assert bgm.details == details

    def test_BackgroundMaskType_error_shape(self):
        """error is geometrical_shape not in approved list"""
        gs = "ERROR"
        dims = DimensionsType().check()
        sf = (SoftwareType(),)
        details = "deets"
        with self.assertRaises(ValueError):
            background_mask_type_entry(
                geometrical_shape=gs, dimensions=dims, software_list=sf, details=details
            ).check()

    def test_default_SpatialFilteringType(self):
        sf = SpatialFilteringType().check().spatial_filtering

        assert sf.low_frequency_cutoff is None
        assert sf.high_frequency_cutoff is None
        assert sf.filter_function is None
        assert sf.software_list == ()

    def test_SpatialFilteringType(self):
        lf = 10
        hf = 20
        funct = "function"
        st = (SoftwareType(),)
        sf = (
            spatial_filtering_type_entry(
                low_frequency_cutoff=lf,
                high_frequency_cutoff=hf,
                filter_function=funct,
                software_list=st,
            )
            .check()
            .spatial_filtering
        )

        assert sf.low_frequency_cutoff == lf
        assert sf.high_frequency_cutoff == hf
        assert sf.filter_function == funct
        assert sf.software_list == st

    def test_default_SharpeningType(self):
        st = SharpeningType().check().sharpening

        assert st.details is None
        assert st.software_list == ()

    def test_SharpeningType(self):
        details = "details"
        sl = (SoftwareType(), SoftwareType())
        st = sharpening_type_entry(software_list=sl, details=details).check().sharpening

        assert st.details == details
        assert st.software_list == sl

    def test_default_BfSharpeningType(self):
        bft = BfSharpeningType().check().bfactorSharpening

        assert bft.brestore is None
        assert bft.software_list == ()
        assert bft.details is None

    def test_BfSharpeningType(self):
        b = 100.0
        details = "deetz"
        sl = (SoftwareType(),)
        bft = (
            bf_sharpening_type_entry(brestore=b, details=details, software_list=sl)
            .check()
            .bfactorSharpening
        )
        assert bft.brestore == b
        assert bft.software_list == sl
        assert bft.details is details

    def test_defeult_ReconstructionFilteringType(self):
        rft = ReconstructionFilteringType().check().reconstruction_filtering

        assert rft.spatial_filtering is None
        assert rft.sharpening is None
        assert rft.background_masked is None
        assert rft.bfactorSharpening is None
        assert rft.other is None

    def test_ReconstructionFilteringType(self):
        sf = SpatialFilteringType()
        sh = SharpeningType()
        bf = BfSharpeningType()
        bm = BackgroundMaskType()
        o = OtherType()
        rft = (
            reconstruction_filtering_type_entry(
                spatial_filtering=sf,
                sharpening=sh,
                background_masked=bm,
                bfactorsharpening=bf,
                other=o,
            )
            .check()
            .reconstruction_filtering
        )

        assert rft.spatial_filtering == sf
        assert rft.sharpening == sh
        assert rft.background_masked == bm
        assert rft.bfactorSharpening == bf
        assert rft.other == o

    def test_default_HelicalParamsType(self):
        hp = HelicalParamsType().check().helical_parameters

        assert hp.delta_z is None
        assert hp.delta_phi is None
        assert hp.axial_symmetry is None

    def test_HelicalParamsType(self):
        z = 2.5
        phi = 1.3
        ax = "sym"
        hp = (
            helical_params_type_entry(delta_z=z, delta_phi=phi, axial_symmetry=ax)
            .check()
            .helical_parameters
        )

        assert hp.delta_z == z
        assert hp.delta_phi == phi
        assert hp.axial_symmetry == ax

    def test_default_SymmetryType(self):
        st = SymmetryType().check().applied_symmetry

        assert st.space_group is None
        assert st.point_group is None
        assert st.helical_parameters is None

    def test_SymmetryType(self):
        sg = 1
        pg = "d"
        hp = HelicalParamsType()
        st = (
            symmetry_type_entry(
                space_group=sg,
                point_group=pg,
                helical_parameters=hp,
            )
            .check()
            .applied_symmetry
        )

        assert st.space_group == sg
        assert st.point_group == pg
        assert st.helical_parameters == hp

    # TO DO write the rest of these tests

    def test_depobject_as_dict(self):
        rt = random_conical_tilt_type_entry(
            number_images=10,
            tilt_angle=180.0,
            software_list=(software_type_entry(name="soft1", version="1.0"),),
            details="test",
        )
        expanded_dict = {
            "number_images": 10,
            "tilt_angle": 180.0,
            "software_list": (
                {
                    "software_type": {
                        "name": "soft1",
                        "processing_details": None,
                        "version": "1.0",
                    }
                },
            ),
            "details": "test",
        }

        rt_dict = asdict(rt.random_conical_tilt)

        assert rt_dict == expanded_dict, rt_dict

    def test_default_FinalReconstructionType(self):
        fr = FinalReconstructionType().check().final_reconstruction

        assert fr.number_of_classes_used is None
        assert fr.applied_symmetry is None
        assert fr.algorithm is None
        assert fr.resolution is None
        assert fr.resolution_method is None
        assert fr.reconstruction_filtering is None

    def test_FinalReconstructionType(self):
        nc = 100
        aps = SymmetryType()
        algo = "BACK PROJECTION"
        reso = 1.0
        resomet = "FSC 0.143 CUT-OFF"
        rf = ReconstructionFilteringType()
        fr = (
            final_reconstruction_type_entry(
                number_of_classes_used=nc,
                applied_symmetry=aps,
                algorithm=algo,
                resolution=reso,
                resolution_method=resomet,
                reconstruction_filtering=rf,
            )
            .check()
            .final_reconstruction
        )

        assert fr.number_of_classes_used == nc
        assert fr.applied_symmetry == aps
        assert fr.algorithm == algo
        assert fr.resolution == reso
        assert fr.resolution_method == resomet
        assert fr.reconstruction_filtering == rf

    def test_FinalReconstructionType_error_recalgo(self):
        """value must be in list of valid options"""
        nc = 100
        aps = SymmetryType()
        algo = "ERROR"
        reso = 1.0
        resomet = "FSC 0.143 CUT-OFF"
        rf = ReconstructionFilteringType()
        with self.assertRaises(ValueError):
            final_reconstruction_type_entry(
                number_of_classes_used=nc,
                applied_symmetry=aps,
                algorithm=algo,
                resolution=reso,
                resolution_method=resomet,
                reconstruction_filtering=rf,
            ).check()

    def test_FinalReconstructionType_error_res_method(self):
        """value must be in list of valid options"""
        nc = 100
        aps = SymmetryType()
        algo = "BACK PROJECTION"
        reso = 1.0
        resomet = "ERROR!"
        rf = ReconstructionFilteringType()
        with self.assertRaises(ValueError):
            final_reconstruction_type_entry(
                number_of_classes_used=nc,
                applied_symmetry=aps,
                algorithm=algo,
                resolution=reso,
                resolution_method=resomet,
                reconstruction_filtering=rf,
            ).check()

    def test_merge_nts_dont_duplicate_software(self):
        """number_images should be overwritten, tilt_angle should not,
        software list should not be duplicated"""
        rt1 = random_conical_tilt_type_entry(
            number_images=10,
            tilt_angle=180.0,
            software_list=(software_type_entry(name="soft1", version="1.0"),),
        )
        rt2 = random_conical_tilt_type_entry(
            number_images=11,
            software_list=(software_type_entry(name="soft1", version="1.0"),),
            details="test",
        )
        expdict = {
            "number_images": 11,
            "tilt_angle": 180.0,
            "software_list": [
                {
                    "software_type": {
                        "name": "soft1",
                        "processing_details": None,
                        "version": "1.0",
                    }
                }
            ],
            "details": "test",
        }

        rt1_dict = asdict(rt1)
        rt2_dict = asdict(rt2)
        merged = merge(rt1_dict, rt2_dict)
        assert merged["random_conical_tilt"] == expdict

    def test_merge_nts_merge_software(self):
        """number_images should be overwritten, tilt_angle should not,
        software list should be merged"""
        rt1 = random_conical_tilt_type_entry(
            number_images=10,
            tilt_angle=180.0,
            software_list=(software_type_entry(name="soft1", version="1.0"),),
        )
        rt2 = random_conical_tilt_type_entry(
            number_images=11,
            software_list=(software_type_entry(name="soft2", version="2.0"),),
            details="test",
        )
        expdict = {
            "number_images": 11,
            "tilt_angle": 180.0,
            "software_list": [
                {
                    "software_type": {
                        "name": "soft1",
                        "processing_details": None,
                        "version": "1.0",
                    }
                },
                {
                    "software_type": {
                        "name": "soft2",
                        "processing_details": None,
                        "version": "2.0",
                    }
                },
            ],
            "details": "test",
        }

        rt1_dict = asdict(rt1)
        rt2_dict = asdict(rt2)
        merged = merge(rt1_dict, rt2_dict)
        assert merged["random_conical_tilt"] == expdict


if __name__ == "__main__":
    unittest.main()
