#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import pkg_resources
import unittest

from pipeliner import job_factory
from pipeliner.jobs.ccpem import molrep_job
from pipeliner.jobs.relion import refine3D_job
from pipeliner.jobs.other import cryolo_job


class JobFactoryTest(unittest.TestCase):
    def test_gather_all_jobs(self):
        job_dict = job_factory.gather_all_jobtypes()

        # Check a few example jobs
        assert job_dict["molrep.fit_model"] is molrep_job.MolrepJob
        assert job_dict["relion.refine3d"] is refine3D_job.RelionRefine3D
        assert job_dict["cryolo.autopick"] is cryolo_job.CrYOLOAutopick

    def test_all_jobs_have_same_process_name_in_class_and_entry_point_definitions(self):
        # TODO: avoid the need for this test by removing the duplication of names
        for entry_point in pkg_resources.iter_entry_points("ccpem_pipeliner.jobs"):
            job_class = entry_point.load()
            assert job_class.PROCESS_NAME == entry_point.name


if __name__ == "__main__":
    unittest.main()
