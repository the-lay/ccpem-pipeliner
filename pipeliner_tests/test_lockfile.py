#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import stat

from pipeliner_tests import test_data
from pipeliner.project_graph import ProjectGraph
from pipeliner_tests.generic_tests import do_interactive_tests


class LockfilesTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_dir = tempfile.mkdtemp(prefix="relion_")
        self.test_data = os.path.dirname(test_data.__file__)

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            os.path.join(self.test_dir, "default_pipeline.star"),
        )

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_read_pipline_makes_lock(self):
        pipeline = ProjectGraph(name="default")
        pipeline.read(do_lock=True)
        assert os.path.isfile(".relion_lock/lock_default_pipeline.star")

    def test_write_pipline_removes_lock(self):
        pipeline = ProjectGraph(name="default")
        pipeline.read(do_lock=True)
        assert os.path.isfile(".relion_lock/lock_default_pipeline.star")
        pipeline.write()
        assert not os.path.isfile(".relion_lock/lock_default_pipeline.star")

    def test_renaming_pipline_changes_lock(self):
        pipeline = ProjectGraph(name="default")
        pipeline.read(do_lock=True)
        assert os.path.isfile(".relion_lock/lock_default_pipeline.star")
        pipeline.set_name("new_name", True)
        assert not os.path.isfile(".relion_lock/lock_default_pipeline.star")
        assert os.path.isfile(".relion_lock/lock_new_name_pipeline.star")
        assert os.path.isfile("new_name_pipeline.star")

    def test_cant_write_if_not_locked(self):
        pipeline = ProjectGraph(name="default")
        pipeline.read()
        assert not os.path.isfile(".relion_lock/lock_default_pipeline.star")
        with self.assertRaises(RuntimeError):
            pipeline.write(lock_wait=0.01)

    def test_can_write_if_not_locked_with_exception(self):
        pipeline = ProjectGraph(name="default")
        pipeline.read()
        assert not os.path.isfile(".relion_lock/lock_default_pipeline.star")
        pipeline.write(lockfile_expected=False)

    def test_can_reread_locked_pipeline_if_do_lock_is_false(self):
        pipeline = ProjectGraph(name="default")
        pipeline.read(do_lock=True)
        assert os.path.isfile(".relion_lock/lock_default_pipeline.star")
        another_pipe = ProjectGraph(name="default")
        another_pipe.read()
        pipe_plist = [x.name for x in pipeline.process_list]
        pipe2_plist = [x.name for x in another_pipe.process_list]
        assert pipe_plist == pipe2_plist

    def test_cant_reread_locked_pipeline_with_lock(self):
        pipeline = ProjectGraph(name="default")
        pipeline.read(do_lock=True)
        assert os.path.isfile(".relion_lock/lock_default_pipeline.star")
        another_pipe = ProjectGraph(name="default")
        with self.assertRaises(RuntimeError):
            another_pipe.read(do_lock=True, lock_wait=0.01)

    def test_can_reread_locked_pipeline_but_cant_rewrite(self):
        pipeline = ProjectGraph(name="default")
        pipeline.read(do_lock=True)
        assert os.path.isfile(".relion_lock/lock_default_pipeline.star")
        another_pipe = ProjectGraph(name="default")
        with self.assertRaises(RuntimeError):
            another_pipe.read(do_lock=True, lock_wait=0.01)

    @unittest.skipUnless(do_interactive_tests(), "Don't include in ci tests")
    def test_cant_lock_trouble_writing_lockfile(self):
        pipeline = ProjectGraph(name="default")
        os.makedirs(".relion_lock")
        os.chmod(".relion_lock", stat.S_ENFMT)
        with self.assertRaises(PermissionError):
            pipeline.read(do_lock=True, lock_wait=0.01)
        os.chmod(".relion_lock", stat.S_IRWXU)

    @unittest.skipUnless(do_interactive_tests(), "Don't include in ci tests")
    def test_cant_lock_trouble_making_lockdir(self):
        pipeline = ProjectGraph(name="default")
        os.chmod(self.test_dir, stat.S_ENFMT)
        with self.assertRaises(RuntimeError):
            pipeline.read(do_lock=True, lock_wait=0.01)
        os.chmod(self.test_dir, stat.S_IRWXU)

    def test_error_when_write_with_missing_lock(self):
        pipeline = ProjectGraph(name="default")
        pipeline.read(do_lock=True)
        assert os.path.isfile(".relion_lock/lock_default_pipeline.star")
        os.remove(".relion_lock/lock_default_pipeline.star")
        with self.assertRaises(RuntimeError):
            pipeline.write(lock_wait=0.01)


if __name__ == "__main__":
    unittest.main()
