#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests import test_data, generic_tests
from pipeliner_tests.generic_tests import check_for_relion
from pipeliner.star_writer import COMMENT_LINE


class JobStarWriterTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def split_lines(self, linelist):
        return [x.split() for x in linelist]

    @unittest.skipUnless(check_for_relion(), "Relion needed for test")
    def test_with_postprocess_withmtf(self):
        generic_tests.running_job(
            test_jobfile="postprocess_adhocbf.job",
            job_dir_type="PostProcess",
            job_no=3,
            input_files=[
                ("Import/job001", "3488_run_half1_class001_unfil.mrc"),
                ("Import/job001", "3488_run_half2_class001_unfil.mrc"),
                ("Import/job002", "emd_3488_mask.mrc"),
                ("Import/mtffile", "mtf_k2_200kV.star"),
            ],
            expected_outfiles=(
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
            sleep_time=1,
        )
        actual_path = os.path.join(
            self.test_data, "JobFiles/PostProcess/postprocess_job.star"
        )
        wrote_path = os.path.join(self.test_dir, "PostProcess/job003/job.star")
        with open(actual_path) as actual:
            actual_file = actual.readlines()
        with open(wrote_path) as wrote:
            wrote_file = wrote.readlines()
        for line in actual_file:
            if "# version" not in line:
                assert line.split() in self.split_lines(wrote_file), line
        for line in wrote_file:
            if line != f"# {COMMENT_LINE}\n":
                assert line.split() in self.split_lines(actual_file), line

    @unittest.skipUnless(check_for_relion(), "Relion needed for test")
    def test_with_maskcreate_job(self):
        generic_tests.running_job(
            test_jobfile="maskcreate.job",
            job_dir_type="MaskCreate",
            job_no=2,
            input_files=[("Import/job001", "emd_3488.mrc")],
            expected_outfiles=("run.out", "run.err", "mask.mrc"),
            sleep_time=5,
        )
        actual_path = os.path.join(
            self.test_data, "JobFiles/MaskCreate/maskcreate_job.star"
        )
        wrote_path = os.path.join(self.test_dir, "MaskCreate/job002/job.star")
        # subprocess.call(["cat",wrote_path])
        with open(actual_path) as actual:
            actual_file = actual.readlines()
        with open(wrote_path) as wrote:
            wrote_file = wrote.readlines()
        for line in actual_file:
            if "# version" not in line and len(line) > 5:
                assert line.split() in self.split_lines(wrote_file), line
        for line in wrote_file:
            if line != f"# {COMMENT_LINE}\n" and len(line) > 5:
                assert line.split() in self.split_lines(actual_file), line


if __name__ == "__main__":
    unittest.main()
