#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
from unittest import mock
import os
import tempfile
import shutil
from pipeliner.utils import touch
from pipeliner.scripts.recover_project import recover
from pipeliner_tests import test_data


class PipelinerRecoveryScriptTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_successful_restore_nolock(self):
        touch("default_pipeline.star")
        for jdir in ["Import/job001", "MotionCorr/job002", "CtfFind/job003"]:
            os.makedirs(jdir)
            pipefile = os.path.join(jdir, "default_pipeline.star")
            touch(pipefile)
            with open(pipefile, "w") as pf:
                pf.write(jdir)

        recover(confirm=False)
        assert os.path.isfile("default_pipeline.star")
        with open("default_pipeline.star", "r") as wrote:
            assert wrote.read() == "CtfFind/job003"

    def test_successful_restore_locked(self):
        touch("default_pipeline.star")
        for jdir in [
            "Import/job001",
            "MotionCorr/job002",
            "CtfFind/job003",
            ".relion_lock",
        ]:
            os.makedirs(jdir)
            pipefile = os.path.join(jdir, "default_pipeline.star")
            touch(pipefile)
            with open(pipefile, "w") as pf:
                pf.write(jdir)
        recover(confirm=False)
        assert os.path.isfile("default_pipeline.star")
        with open("default_pipeline.star", "r") as wrote:
            assert wrote.read() == "CtfFind/job003"
        assert not os.path.isdir(".relion_lock")

    def test_user_says_no(self):
        for jdir in [
            "Import/job001",
            "MotionCorr/job002",
            "CtfFind/job003",
            ".relion_lock",
        ]:
            os.makedirs(jdir)
            pipefile = os.path.join(jdir, "default_pipeline.star")
            touch(pipefile)
            with open(pipefile, "w") as pf:
                pf.write(jdir)
        with open("default_pipeline.star", "w") as default:
            default.write("NO CHANGES")
        with mock.patch("builtins.input", return_value="n"):
            with self.assertRaises(SystemExit):
                recover()
        assert os.path.isfile("default_pipeline.star")
        with open("default_pipeline.star", "r") as wrote:
            assert wrote.read() == "NO CHANGES"
        assert os.path.isdir(".relion_lock")

    def test_user_says_yes(self):
        touch("default_pipeline.star")
        for jdir in [
            "Import/job001",
            "MotionCorr/job002",
            "CtfFind/job003",
            ".relion_lock",
        ]:
            os.makedirs(jdir)
            pipefile = os.path.join(jdir, "default_pipeline.star")
            touch(pipefile)
            with open(pipefile, "w") as pf:
                pf.write(jdir)
        with mock.patch("builtins.input", return_value="y"):
            recover()
        assert os.path.isfile("default_pipeline.star")
        with open("default_pipeline.star", "r") as wrote:
            assert wrote.read() == "CtfFind/job003"
        assert not os.path.isdir(".relion_lock")

    def test_fail_if_no_pipeline_file_present(self):
        with self.assertRaises(SystemExit):
            recover(confirm=False)

    def test_only_removes_lock_if_no_backup_dirs(self):
        os.makedirs(".relion_lock")
        with open("default_pipeline.star", "w") as default:
            default.write("NO CHANGES")
        recover(confirm=False)
        assert os.path.isfile("default_pipeline.star")
        with open("default_pipeline.star", "r") as wrote:
            assert wrote.read() == "NO CHANGES"
        assert not os.path.isdir(".relion_lock")

    def test_only_removes_lock_if_no_backup_in_dir(self):
        for jdir in [
            "Import/job001",
            "MotionCorr/job002",
            "CtfFind/job003",
            ".relion_lock",
        ]:
            os.makedirs(jdir)

        with open("default_pipeline.star", "w") as default:
            default.write("NO CHANGES")
        recover(confirm=False)
        assert os.path.isfile("default_pipeline.star")
        with open("default_pipeline.star", "r") as wrote:
            assert wrote.read() == "NO CHANGES"
        assert not os.path.isdir(".relion_lock")

    def test_uses_penultimate_job_if_no_backup_in_last_job(self):
        touch("default_pipeline.star")
        for jdir in [
            "Import/job001",
            "MotionCorr/job002",
            "CtfFind/job003",
            ".relion_lock",
        ]:
            os.makedirs(jdir)

        pipefile = os.path.join("MotionCorr/job002", "default_pipeline.star")
        touch(pipefile)
        with open(pipefile, "w") as pf:
            pf.write("MotionCorr/job002")

        recover(confirm=False)
        assert os.path.isfile("default_pipeline.star")
        with open("default_pipeline.star", "r") as wrote:
            assert wrote.read() == "MotionCorr/job002"
        assert not os.path.isdir(".relion_lock")
