#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import contextlib
import io
import json
import os
import re
import shutil
import tempfile
import textwrap
import time
import unittest
from glob import glob
from pathlib import Path
from unittest.mock import patch

from pipeliner.job_runner import JobRunner, job_factory
from pipeliner_tests import test_data, generic_tests
from pipeliner.data_structure import (
    RELION_SUCCESS_FILE,
    SUCCESS_FILE,
    JOBINFO_FILE,
    STARFILE_READS_OFF,
    FAIL_FILE,
)
from pipeliner_tests.generic_tests import read_pipeline, check_for_relion
from pipeliner_tests.job_testing_tools import DummyJob
from pipeliner.utils import get_pipeliner_root, touch
from pipeliner.api.manage_project import PipelinerProject
from pipeliner.scripts import check_completion

do_full = generic_tests.do_slow_tests()
cc_path = os.path.join(get_pipeliner_root(), "scripts/check_completion.py")


class JobRunnerTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def wait_for_file(self, path, time_in_seconds=1):
        """Wait for a file to appear after a job"""
        step = 0.1
        total = 0
        while not os.path.isfile(path):
            time.sleep(step)
            total += step
            assert total < time_in_seconds, (
                f"File {path} does not exist after " f"{time_in_seconds} seconds"
            )

    # TODO: Turn this test back on when file validation is turned back on
    @unittest.skip("Currently skipped because file validation is turned off")
    # @unittest.skipUnless(check_for_relion(), "Relion needed for test")
    def test_running_fails_file_validation(self):

        # Prepare a new pipeline and run the job
        pipeline = JobRunner()
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Import/import_mask.job")
        )
        with self.assertRaises(ValueError):
            pipeline.run_job(job, None, False, False, False, False)

    @unittest.skipUnless(check_for_relion(), "Relion needed for test")
    def test_running_import_mask_job(self):
        # Copy the mask file into place
        # TODO: would be better to just pass the path to the mask as a job option
        shutil.copy(os.path.join(self.test_data, "emd_3488_mask.mrc"), self.test_dir)

        # Prepare a new pipeline and run the job
        pipeline = JobRunner()
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Import/import_mask.job")
        )
        job_dir = "Import/job001/"
        assert not os.path.isdir(job_dir)
        pipeline.run_job(job, None, False, False, False, False)
        # Need to wait a short time, otherwise test fails. Shouldn't really be
        # necessary. Maybe this happens because new files take time to be flushed to
        # disk?
        time.sleep(0.1)

        # Test for output files
        assert os.path.isdir(job_dir)
        assert os.path.isfile(os.path.join(job_dir, "run.out"))
        assert os.path.isfile(os.path.join(job_dir, "run.err"))
        assert os.path.isfile(os.path.join(job_dir, "emd_3488_mask.mrc"))

        # Test for correct project graph state
        project_graph = pipeline.graph
        assert len(project_graph.process_list) == 1
        assert project_graph.job_counter == 2
        assert len(project_graph.node_list) == 1
        assert project_graph.node_list[0].name == "Import/job001/emd_3488_mask.mrc"
        assert project_graph.node_list[0].type == "Mask3D.mrc"
        assert os.path.isfile(".gui_relion_import_otherjob.star")

        # look for the metadata file
        md_files = glob("Import/job001/*job_metadata.json")
        assert len(md_files) == 1

    @unittest.skipUnless(check_for_relion(), "Relion needed for test")
    def test_running_import_map_job(self):
        # Copy the map file into place
        # TODO: would be better to just pass the path to the mask as a job option
        shutil.copy(os.path.join(self.test_data, "emd_3488.mrc"), self.test_dir)

        # Prepare a new pipeline and run the job
        pipeline = JobRunner()
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Import/import_map.job")
        )
        job_dir = "Import/job001/"
        assert not os.path.isdir(job_dir)
        pipeline.run_job(job, None, False, False, False, False)

        # Need to wait a short time, otherwise test fails. Shouldn't really be
        # necessary. Maybe this happens because new files take time to be flushed to
        # disk?
        time.sleep(1)

        # Test for output files
        assert os.path.isdir(job_dir)
        assert os.path.isfile(os.path.join(job_dir, "run.out"))
        assert os.path.isfile(os.path.join(job_dir, "run.err"))
        assert os.path.isfile(os.path.join(job_dir, "emd_3488.mrc"))

        # Test for correct project graph state
        project_graph = pipeline.graph
        assert len(project_graph.process_list) == 1
        assert project_graph.job_counter == 2
        assert len(project_graph.node_list) == 1
        assert project_graph.node_list[0].name == "Import/job001/emd_3488.mrc"
        assert project_graph.node_list[0].type == "DensityMap.mrc"

        # look for the metadata file
        md_files = glob("Import/job001/*job_metadata.json")
        assert len(md_files) == 1

    @unittest.skipUnless(check_for_relion(), "Relion needed for test")
    def test_running_import_map_job_with_comments(self):
        # Copy the map file into place
        # TODO: would be better to just pass the path to the mask as a job option
        shutil.copy(os.path.join(self.test_data, "emd_3488.mrc"), self.test_dir)

        # Prepare a new pipeline and run the job
        pipeline = JobRunner()
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Import/import_map.job")
        )
        job_dir = "Import/job001/"
        assert not os.path.isdir(job_dir)
        pipeline.run_job(
            job, None, False, False, False, False, comment="Here is a comment"
        )

        # Need to wait a short time, otherwise test fails. Shouldn't really be
        # necessary. Maybe this happens because new files take time to be flushed to
        # disk?
        time.sleep(1)

        # Test for output files
        assert os.path.isdir(job_dir)
        assert os.path.isfile(os.path.join(job_dir, "run.out"))
        assert os.path.isfile(os.path.join(job_dir, "run.err"))
        assert os.path.isfile(os.path.join(job_dir, "emd_3488.mrc"))

        # Test for correct project graph state
        project_graph = pipeline.graph
        assert len(project_graph.process_list) == 1
        assert project_graph.job_counter == 2
        assert len(project_graph.node_list) == 1
        assert project_graph.node_list[0].name == "Import/job001/emd_3488.mrc"
        assert project_graph.node_list[0].type == "DensityMap.mrc"

        # look for the metadata file
        md_files = glob("Import/job001/*job_metadata.json")
        assert len(md_files) == 1

        # check the jobinfo file
        with open(f"Import/job001/{JOBINFO_FILE}", "r") as ji:
            actual_ji = json.load(ji)
        assert actual_ji["job directory"] == "Import/job001/"
        assert actual_ji["comments"] == ["Here is a comment"]
        assert actual_ji["rank"] is None
        ts = list(actual_ji["history"])[0]
        assert re.match(r"\d+-\d+-\d+ \d+:\d+:\d+", ts)
        assert actual_ji["history"][ts] == "Run"
        com = [
            "cp emd_3488.mrc Import/job001/emd_3488.mrc",
            f"{cc_path} Import/job001 emd_3488.mrc",
        ]
        assert actual_ji["command history"][ts] == com

    @unittest.skipUnless(check_for_relion(), "Relion needed for test")
    def test_running_postprocess_job(self):
        # Prepare the directory structure as if Import jobs have been run
        halfmap_import_dir = "Import/job001/"
        os.makedirs(halfmap_import_dir)
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        mask_import_dir = "Import/job002/"
        os.makedirs(mask_import_dir)
        shutil.copy(os.path.join(self.test_data, "emd_3488_mask.mrc"), mask_import_dir)

        pipeline = JobRunner()
        pipeline.graph.job_counter = 3
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/PostProcess/postprocess.job"),
        )

        job_dir = "PostProcess/job003/"

        assert not os.path.isdir(job_dir)
        pipeline.run_job(job, None, False, False)

        # Wait for job to finish, otherwise test fails. Shouldn't be necessary...
        time.sleep(1)

        # Test for output files
        assert os.path.isdir(job_dir)
        assert os.path.isfile(os.path.join(job_dir, "run.out"))
        assert os.path.isfile(os.path.join(job_dir, "run.err"))
        assert os.path.isfile(os.path.join(job_dir, "postprocess.mrc"))
        assert os.path.isfile(os.path.join(job_dir, "postprocess_masked.mrc"))
        assert os.path.isfile(os.path.join(job_dir, "logfile.pdf"))
        assert os.path.isfile(os.path.join(job_dir, RELION_SUCCESS_FILE))
        assert os.path.isfile(os.path.join(job_dir, SUCCESS_FILE))
        assert os.path.isfile(".gui_relion_postprocessjob.star")

        # look for the metadata file
        md_files = glob("PostProcess/job003/*job_metadata.json")
        assert len(md_files) == 1

    @unittest.skipUnless(check_for_relion(), "Relion needed for test")
    def test_running_postprocess_job_with_alias(self):
        # copy in a pipeline so it has somthing to operate on
        pipefile = os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star")
        shutil.copy(pipefile, "default_pipeline.star")

        # Prepare the directory structure as if Import jobs have been run
        halfmap_import_dir = "Import/job001/"
        os.makedirs(halfmap_import_dir)
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        mask_import_dir = "Import/job002/"
        os.makedirs(mask_import_dir)
        shutil.copy(os.path.join(self.test_data, "emd_3488_mask.mrc"), mask_import_dir)

        pipeline = JobRunner()
        pipeline.graph.job_counter = 3
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/PostProcess/postprocess.job"),
        )

        job_dir = "PostProcess/job003/"

        assert not os.path.isdir(job_dir)
        pipeline.run_job(job, None, False, False, alias="test_alias")

        # Wait for job to finish, otherwise test fails. Shouldn't be necessary...
        time.sleep(1)

        # Test for output files
        assert os.path.isdir(job_dir)
        assert os.path.isfile(os.path.join(job_dir, "run.out"))
        assert os.path.isfile(os.path.join(job_dir, "run.err"))
        assert os.path.isfile(os.path.join(job_dir, "postprocess.mrc"))
        assert os.path.isfile(os.path.join(job_dir, "postprocess_masked.mrc"))
        assert os.path.isfile(os.path.join(job_dir, "logfile.pdf"))
        assert os.path.isfile(os.path.join(job_dir, RELION_SUCCESS_FILE))
        assert os.path.isfile(os.path.join(job_dir, SUCCESS_FILE))
        assert os.path.isfile(".gui_relion_postprocessjob.star")

        # look for the metadata file
        md_files = glob("PostProcess/job003/*job_metadata.json")
        assert len(md_files) == 1

        the_job = pipeline.graph.find_process("PostProcess/job003/")
        assert the_job.alias == "PostProcess/test_alias/"
        assert os.path.islink("PostProcess/test_alias")

    @unittest.skipUnless(check_for_relion(), "Relion needed for test")
    def test_schedule_postprocess_job(self):
        # copy in a pipeline so it has somthing to operate on
        pipefile = os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star")
        shutil.copy(pipefile, "default_pipeline.star")

        # Prepare the directory structure as if Import jobs have been run
        halfmap_import_dir = "Import/job001/"
        os.makedirs(halfmap_import_dir)
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        mask_import_dir = "Import/job002/"
        os.makedirs(mask_import_dir)
        shutil.copy(os.path.join(self.test_data, "emd_3488_mask.mrc"), mask_import_dir)

        pipeline = JobRunner()
        pipeline.graph.job_counter = 3
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/PostProcess/postprocess.job"),
        )

        job_dir = "PostProcess/job003/"

        assert not os.path.isdir(job_dir)
        pipeline.schedule_job(job, None, False, False)

        the_job = pipeline.graph.find_process("PostProcess/job003/")
        assert os.path.isdir("PostProcess/job003")
        assert the_job.status == "Scheduled"
        assert not the_job.alias

    @unittest.skipUnless(check_for_relion(), "Relion needed for test")
    def test_schedule_postprocess_job_with_alias(self):
        # copy in a pipeline so it has somthing to operate on
        pipefile = os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star")
        shutil.copy(pipefile, "default_pipeline.star")

        # Prepare the directory structure as if Import jobs have been run
        halfmap_import_dir = "Import/job001/"
        os.makedirs(halfmap_import_dir)
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        mask_import_dir = "Import/job002/"
        os.makedirs(mask_import_dir)
        shutil.copy(os.path.join(self.test_data, "emd_3488_mask.mrc"), mask_import_dir)

        pipeline = JobRunner()
        pipeline.graph.job_counter = 3
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/PostProcess/postprocess.job"),
        )

        job_dir = "PostProcess/job003/"

        assert not os.path.isdir(job_dir)
        pipeline.schedule_job(job, None, False, False, alias="test_alias")

        the_job = pipeline.graph.find_process("PostProcess/job003/")
        assert the_job.alias == "PostProcess/test_alias/"
        assert os.path.isdir("PostProcess/job003")
        assert os.path.islink("PostProcess/test_alias")
        assert the_job.status == "Scheduled"
        assert the_job.alias == "PostProcess/test_alias/"

    @unittest.skipUnless(check_for_relion(), "Relion needed for test")
    def test_generic_with_PP(self):
        generic_tests.running_job(
            test_jobfile="postprocess.job",
            job_dir_type="PostProcess",
            job_no=3,
            input_files=[
                ("Import/job001", "3488_run_half1_class001_unfil.mrc"),
                ("Import/job001", "3488_run_half2_class001_unfil.mrc"),
                ("Import/job002", "emd_3488_mask.mrc"),
            ],
            expected_outfiles=(
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
            sleep_time=1,
        )

    @unittest.skipUnless(check_for_relion(), "Relion needed for test")
    def test_running_maskcreate_job(self):
        generic_tests.running_job(
            test_jobfile="maskcreate.job",
            job_dir_type="MaskCreate",
            job_no=2,
            input_files=[("Import/job001", "emd_3488.mrc")],
            expected_outfiles=("run.out", "run.err", "mask.mrc"),
            sleep_time=5,
        )

    @unittest.skipUnless(check_for_relion(), "Relion needed for test")
    def test_postprocess_adhocbf(self):
        generic_tests.running_job(
            test_jobfile="postprocess_adhocbf.job",
            job_dir_type="PostProcess",
            job_no=3,
            input_files=[
                ("Import/job001", "3488_run_half1_class001_unfil.mrc"),
                ("Import/job001", "3488_run_half2_class001_unfil.mrc"),
                ("Import/job002", "emd_3488_mask.mrc"),
            ],
            expected_outfiles=(
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
            sleep_time=1,
        )

    @unittest.skipUnless(check_for_relion(), "Relion needed for test")
    def test_postprocess_autobf(self):
        generic_tests.running_job(
            test_jobfile="postprocess_autobf.job",
            job_dir_type="PostProcess",
            job_no=3,
            input_files=[
                ("Import/job001", "3488_run_half1_class001_unfil.mrc"),
                ("Import/job001", "3488_run_half2_class001_unfil.mrc"),
                ("Import/job002", "emd_3488_mask.mrc"),
            ],
            expected_outfiles=(
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
            sleep_time=1,
        )

    @unittest.skipUnless(
        do_full and check_for_relion(), "Slow test: Only runs in full unittest"
    )
    def test_split_job(self):
        """Select jobs that split star files in defined size groups generate
        an unknown number of output files so they are a special case where
        output nodes are generated after the job runs"""

        # first run a job to create the dirs
        generic_tests.running_job(
            test_jobfile="select_parts_split_runtest.job",
            job_dir_type="Select",
            job_no=3,
            input_files=[("Extract/job001", "particles.star")],
            expected_outfiles=(
                "run.out",
                "run.err",
                "particles_split1.star",
                "particles_split2.star",
                "particles_split3.star",
                "particles_split4.star",
                "particles_split5.star",
                "particles_split6.star",
                "particles_split7.star",
                "particles_split8.star",
                "particles_split9.star",
                "particles_split10.star",
                "particles_split11.star",
                "particles_split12.star",
            ),
            sleep_time=1,
        )

        partnode = "ParticlesData.star.relion"
        exp_lines = [
            ["Select/job003/", "Select/job003/particles_split1.star"],
            ["Select/job003/", "Select/job003/particles_split10.star"],
            ["Select/job003/", "Select/job003/particles_split11.star"],
            ["Select/job003/", "Select/job003/particles_split12.star"],
            ["Select/job003/", "Select/job003/particles_split2.star"],
            ["Select/job003/", "Select/job003/particles_split3.star"],
            ["Select/job003/", "Select/job003/particles_split4.star"],
            ["Select/job003/", "Select/job003/particles_split5.star"],
            ["Select/job003/", "Select/job003/particles_split6.star"],
            ["Select/job003/", "Select/job003/particles_split7.star"],
            ["Select/job003/", "Select/job003/particles_split8.star"],
            ["Select/job003/", "Select/job003/particles_split9.star"],
            ["Select/job003/particles_split1.star", partnode],
            ["Select/job003/particles_split10.star", partnode],
            ["Select/job003/particles_split11.star", partnode],
            ["Select/job003/particles_split12.star", partnode],
            ["Select/job003/particles_split2.star", partnode],
            ["Select/job003/particles_split3.star", partnode],
            ["Select/job003/particles_split4.star", partnode],
            ["Select/job003/particles_split5.star", partnode],
            ["Select/job003/particles_split6.star", partnode],
            ["Select/job003/particles_split7.star", partnode],
            ["Select/job003/particles_split8.star", partnode],
            ["Select/job003/particles_split9.star", partnode],
        ]
        lines = read_pipeline("default_pipeline.star")
        for i in lines:
            print(i)
        for line in exp_lines:
            assert line in lines, line

    @unittest.skipUnless(check_for_relion(), "Relion needed for test")
    def test_overwriting_job_with_different_type_raises_error(self):
        """Can't overwrite a job with a different type of job"""
        # first run a job to create the dirs
        job_run = generic_tests.running_job(
            test_jobfile="postprocess_autobf.job",
            job_dir_type="PostProcess",
            job_no=3,
            input_files=[
                ("Import/job001", "3488_run_half1_class001_unfil.mrc"),
                ("Import/job001", "3488_run_half2_class001_unfil.mrc"),
                ("Import/job002", "emd_3488_mask.mrc"),
            ],
            expected_outfiles=(
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
            sleep_time=1,
        )

        # then overwrite it with a different job
        with self.assertRaises(ValueError):
            generic_tests.running_job(
                test_jobfile="maskcreate.job",
                job_dir_type="MaskCreate",
                job_no=0,  # this should not be used because it's overwrite
                input_files=[("Import/job001", "emd_3488.mrc")],
                expected_outfiles=("run.out", "run.err", "mask.mrc"),
                sleep_time=5,
                overwrite=True,
                target_job=job_run,
            )

    @unittest.skipUnless(check_for_relion(), "Relion needed for test")
    def test_overwriting_job(self):
        """Overwrite a job with a new run of the same type"""
        # first run a job to create the dirs
        job_run = generic_tests.running_job(
            test_jobfile="postprocess_autobf.job",
            job_dir_type="PostProcess",
            job_no=3,
            input_files=[
                ("Import/job001", "3488_run_half1_class001_unfil.mrc"),
                ("Import/job001", "3488_run_half2_class001_unfil.mrc"),
                ("Import/job002", "emd_3488_mask.mrc"),
            ],
            expected_outfiles=(
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
            sleep_time=1,
        )

        # then overwrite it with another job
        generic_tests.running_job(
            test_jobfile="postprocess_skipfsc.job",
            job_dir_type="PostProcess",
            job_no=3,
            input_files=[
                ("Import/job001", "3488_run_half1_class001_unfil.mrc"),
                ("Import/job001", "3488_run_half2_class001_unfil.mrc"),
                ("Import/job002", "emd_3488_mask.mrc"),
            ],
            expected_outfiles=(
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
            sleep_time=1,
            overwrite=True,
            target_job=job_run,
            n_metadata_files_expected=2,
        )

    @unittest.skipUnless(check_for_relion(), "Relion needed for test")
    def test_overwriting_job_with_children(self):
        """Overwrite a job with children, make sure it is archived and
        The change is noted on the children"""
        # first run a job to create the dirs
        job_run = generic_tests.running_job(
            test_jobfile="postprocess_autobf.job",
            job_dir_type="PostProcess",
            job_no=3,
            input_files=[
                ("Import/job001", "3488_run_half1_class001_unfil.mrc"),
                ("Import/job001", "3488_run_half2_class001_unfil.mrc"),
                ("Import/job002", "emd_3488_mask.mrc"),
            ],
            expected_outfiles=(
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
            sleep_time=2,
        )

        # copy in a pipeline with fake child jobs
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/children_pipeline.star"),
            "default_pipeline.star",
        )

        # make the childjob dirs
        os.makedirs("ChildProcess/job004/")
        os.makedirs("ChildProcess/job005/")

        # then overwrite it with another job
        generic_tests.running_job(
            test_jobfile="postprocess_adhocbf.job",
            job_dir_type="PostProcess",
            job_no=3,
            input_files=[
                ("Import/job001", "3488_run_half1_class001_unfil.mrc"),
                ("Import/job001", "3488_run_half2_class001_unfil.mrc"),
                ("Import/job002", "emd_3488_mask.mrc"),
                ("Import/mtffile", "mtf_k2_200kV.star"),
            ],
            expected_outfiles=(
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
            sleep_time=2,
            overwrite=True,
            target_job=job_run,
            n_metadata_files_expected=2,
        )
        # check that the overwritten markers have been written
        for child_dir in ["ChildProcess/job004/", "ChildProcess/job005/"]:
            po_marker = glob(child_dir + "PARENT_OVERWRITTEN*")
            assert len(po_marker) == 1, po_marker

        # find the archive directory
        alldirs = glob(".*")
        archive_dir = None
        for i in alldirs:
            if "PostProcess.job003" in i:
                archive_dir = i
                break

        # get the files in it
        adir_files = glob(archive_dir + "/*")
        expected_files = [
            RELION_SUCCESS_FILE,
            SUCCESS_FILE,
            "continue_job.star",
            "default_pipeline.star",
            "job.star",
            "job_pipeline.star",
            "logfile.pdf",
            "logfile.pdf.lst",
            "note.txt",
            "postprocess.mrc",
            "postprocess.star",
            "postprocess_fsc.eps",
            "postprocess_fsc.xml",
            "postprocess_guinier.eps",
            "postprocess_masked.mrc",
            "run.err",
            "run.job",
            "run.out",
        ]
        for f in expected_files:
            assert archive_dir + "/" + f in adir_files, f

    @unittest.skipUnless(check_for_relion(), "Relion needed for test")
    def test_postprocess_skipfsc(self):
        generic_tests.running_job(
            test_jobfile="postprocess_skipfsc.job",
            job_dir_type="PostProcess",
            job_no=3,
            input_files=[
                ("Import/job001", "3488_run_half1_class001_unfil.mrc"),
                ("Import/job001", "3488_run_half2_class001_unfil.mrc"),
                ("Import/job002", "emd_3488_mask.mrc"),
            ],
            expected_outfiles=(
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
            sleep_time=1,
        )

    @unittest.skipUnless(check_for_relion(), "Relion needed for test")
    def test_postprocess_withmtf(self):
        generic_tests.running_job(
            test_jobfile="postprocess_adhocbf.job",
            job_dir_type="PostProcess",
            job_no=3,
            input_files=[
                ("Import/job001", "3488_run_half1_class001_unfil.mrc"),
                ("Import/job001", "3488_run_half2_class001_unfil.mrc"),
                ("Import/job002", "emd_3488_mask.mrc"),
                ("Import/mtffile", "mtf_k2_200kV.star"),
            ],
            expected_outfiles=(
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
            sleep_time=1,
        )

    @unittest.skipUnless(check_for_relion(), "Relion needed for test")
    def test_disable_results_and_metadata(self):
        touch(STARFILE_READS_OFF)
        generic_tests.running_job(
            test_jobfile="postprocess_adhocbf.job",
            job_dir_type="PostProcess",
            job_no=3,
            input_files=[
                ("Import/job001", "3488_run_half1_class001_unfil.mrc"),
                ("Import/job001", "3488_run_half2_class001_unfil.mrc"),
                ("Import/job002", "emd_3488_mask.mrc"),
                ("Import/mtffile", "mtf_k2_200kV.star"),
            ],
            expected_outfiles=(
                "run.out",
                "run.err",
                "postprocess.mrc",
                "postprocess_masked.mrc",
                "logfile.pdf",
            ),
            sleep_time=1,
        )
        results_file = os.path.join(
            self.test_dir, "PostProcess/job003/.results_display001_text.json"
        )
        with open(results_file, "r") as rf:
            results = json.load(rf)
        assert results == {
            "title": "Result display disabled",
            "display_data": "Results display has been disabled for this project. "
            "This is usually done because of memory issues caused by reading large"
            " starfiles.  To re-enable this feature delete the file "
            ".PIPELINER_SKIP_STARFILE_READS",
            "associated_data": [],
        }

        mdfile = glob("PostProcess/job003/*_job_metadata.json")
        with open(mdfile[0]) as mdf:
            metadat = json.load(mdf)
        assert metadat == {
            "No metadata collected": "This project has metadata collection disabled. "
            "This is usually done because of memory issues caused by reading large "
            "starfiles.  To re-enable this feature delete the file .PIPELINER_SKIP_"
            "STARFILE_READS"
        }

    @patch.object(DummyJob, "post_run_actions")
    def test_errors_in_post_run_actions_cause_job_to_fail(self, mock_pra):
        mock_pra.side_effect = ValueError("An error was raised")

        job = DummyJob()
        proj = PipelinerProject(make_new_project=True)
        proj.run_job(job)

        runjob = proj.pipeline.find_process("Test/job001/")
        assert runjob.status == "Failed"
        assert os.path.isfile(f"Test/job001/{FAIL_FILE}")
        err = "WARNING: post_run_actions for Test/job001/ raised an error:"
        with open("Test/job001/run.err", "r") as errfile:
            the_error = errfile.read()
        assert err in the_error

    def test_running_dummy_job(self):
        job = DummyJob()
        job_runner = JobRunner()

        job_dir = Path("Test/job001")

        assert not job_dir.is_dir()

        with contextlib.redirect_stdout(io.StringIO()) as redirected_stdout:
            job_runner.run_job(
                job, current_proc=None, is_main_continue=False, is_scheduled=False
            )
        captured_stdout = redirected_stdout.getvalue()

        for item in job_dir.iterdir():
            print(item)

        assert job_dir.is_dir()
        assert os.path.isdir("Test/job001")
        assert (job_dir / "run.job").is_file()
        assert (job_dir / "job.star").is_file()
        assert (job_dir / ".CCPEM_pipeliner_jobinfo").is_file()
        assert (job_dir / "job_pipeline.star").is_file()
        assert (job_dir / "default_pipeline.star").is_file()
        assert (job_dir / "run.out").is_file()
        assert (job_dir / "run.err").is_file()
        assert (job_dir / "note.txt").is_file()
        assert (job_dir / "PIPELINER_JOB_EXIT_SUCCESS").is_file()
        assert (job_dir / "test_file.txt").is_file()

        expected_commands = [
            "touch Test/job001/test_file.txt",
            f"{check_completion.__file__} Test/job001 test_file.txt",
        ]

        assert captured_stdout == textwrap.dedent(
            f"""\
            Executing Test/job001/ (1/2): {expected_commands[0]}
            Executing Test/job001/ (2/2): {expected_commands[1]}
            """
        )
        assert (job_dir / "run.out").read_text() == textwrap.dedent(
            """\
            ------------------------
            Checking job completion
            ------------------------
            Looking for 1 expected outputs:
            Test/job001/test_file.txt... FOUND
            All outputs found, job completed successfully
            """
        )
        assert (job_dir / "run.err").read_text() == ""
        assert (job_dir / "note.txt").read_text() == textwrap.dedent(
            f"""\
            {expected_commands[0]}
            {expected_commands[1]}
            """
        )
        assert (job_dir / "test_file.txt").read_text() == (
            "post_run_actions() wrote this line"
        )


if __name__ == "__main__":
    unittest.main()
