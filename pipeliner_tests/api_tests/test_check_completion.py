#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner.scripts import check_completion
from pipeliner.data_structure import (
    SUCCESS_FILE,
    FAIL_FILE,
    ABORT_FILE,
    RELION_SUCCESS_FILE,
    RELION_FAIL_FILE,
    RELION_ABORT_FILE,
)
from pipeliner.utils import touch

JOB_DIR = "PluginJob/job001/"


class CheckCompletionTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

        # set up the directory structure
        os.makedirs(JOB_DIR)
        touch(os.path.join(JOB_DIR, "run_class001_half2_unfil.mrc"))
        touch(os.path.join(JOB_DIR, "run_class001.mrc"))
        touch(os.path.join(JOB_DIR, "run_class001_half1_unfil.mrc"))

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_writing_success(self):
        """job finished successfully all outputs accounted for"""
        success = os.path.join(JOB_DIR, SUCCESS_FILE)
        assert not os.path.isfile(success)
        check_completion.main(
            [
                JOB_DIR,
                "run_class001_half2_unfil.mrc",
                "run_class001.mrc",
                "run_class001_half1_unfil.mrc",
            ]
        )
        assert os.path.isfile(success)

    def test_writing_fail(self):
        """job failed one output missing"""
        is_fail = os.path.join(JOB_DIR, FAIL_FILE)
        os.remove(os.path.join(JOB_DIR, "run_class001.mrc"))
        assert not os.path.isfile(is_fail)
        check_completion.main(
            [
                JOB_DIR,
                "run_class001_half2_unfil.mrc",
                "run_class001.mrc",
                "run_class001_half1_unfil.mrc",
            ]
        )
        assert os.path.isfile(is_fail)

    def test_SUCCESS_FILE_already_exists(self):
        """RELION reported the job successful so marked as success"""
        touch(os.path.join(JOB_DIR, RELION_SUCCESS_FILE))

        # Ensure files are as expected
        assert os.path.isfile(os.path.join(JOB_DIR, RELION_SUCCESS_FILE))
        assert not os.path.isfile(os.path.join(JOB_DIR, SUCCESS_FILE))
        assert not os.path.isfile(os.path.join(JOB_DIR, FAIL_FILE))
        assert not os.path.isfile(os.path.join(JOB_DIR, ABORT_FILE))

        # Run check_completion
        check_completion.main(
            [
                JOB_DIR,
                "run_class001_half2_unfil.mrc",
                "run_class001.mrc",
                "run_class001_half1_unfil.mrc",
            ]
        )
        # Check files have not changed
        assert os.path.isfile(os.path.join(JOB_DIR, SUCCESS_FILE))
        assert not os.path.isfile(os.path.join(JOB_DIR, FAIL_FILE))
        assert not os.path.isfile(os.path.join(JOB_DIR, ABORT_FILE))

    def test_FAIL_FILE_already_exists(self):
        """RELION marked the job as failed so fail"""
        touch(os.path.join(JOB_DIR, RELION_FAIL_FILE))

        # Ensure files are as expected
        assert not os.path.isfile(os.path.join(JOB_DIR, SUCCESS_FILE))
        assert os.path.isfile(os.path.join(JOB_DIR, RELION_FAIL_FILE))
        assert not os.path.isfile(os.path.join(JOB_DIR, ABORT_FILE))
        assert not os.path.isfile(os.path.join(JOB_DIR, FAIL_FILE))

        # Run check_completion
        check_completion.main(
            [
                JOB_DIR,
                "run_class001_half2_unfil.mrc",
                "run_class001.mrc",
                "run_class001_half1_unfil.mrc",
            ]
        )
        # Check files have not changed
        assert not os.path.isfile(os.path.join(JOB_DIR, SUCCESS_FILE))
        assert os.path.isfile(os.path.join(JOB_DIR, FAIL_FILE))
        assert not os.path.isfile(os.path.join(JOB_DIR, ABORT_FILE))

    def test_ABORT_FILE_already_exists(self):
        """RELION marked the job aborted so mark aborted"""
        touch(os.path.join(JOB_DIR, RELION_ABORT_FILE))

        # Ensure files are as expected
        assert not os.path.isfile(os.path.join(JOB_DIR, SUCCESS_FILE))
        assert not os.path.isfile(os.path.join(JOB_DIR, FAIL_FILE))
        assert os.path.isfile(os.path.join(JOB_DIR, RELION_ABORT_FILE))
        assert not os.path.isfile(os.path.join(JOB_DIR, ABORT_FILE))

        # Run check_completion
        check_completion.main(
            [
                JOB_DIR,
                "run_class001_half2_unfil.mrc",
                "run_class001.mrc",
                "run_class001_half1_unfil.mrc",
            ]
        )
        # Check files have not changed
        assert not os.path.isfile(os.path.join(JOB_DIR, SUCCESS_FILE))
        assert not os.path.isfile(os.path.join(JOB_DIR, FAIL_FILE))
        assert os.path.isfile(os.path.join(JOB_DIR, ABORT_FILE))


if __name__ == "__main__":
    unittest.main()
