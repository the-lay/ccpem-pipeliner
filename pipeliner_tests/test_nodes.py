#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import json
import unittest
import os
import tempfile
import shutil

from pipeliner.nodes import (
    Node,
    DensityMapNode,
    MicrographMovieGroupMetadataNode,
    ParticlesDataNode,
    MicrographGroupMetadataNode,
    Image2DGroupMetadataNode,
    NODE_DISPLAY_FILE,
    NODE_DISPLAY_DIR,
    check_file_is_mrc,
    check_file_is_cif,
    check_file_is_tif,
    check_file_is_text,
    GenNodeFormatConverter,
)
from pipeliner_tests import test_data
from pipeliner.results_display_objects import (
    ResultsDisplayTextFile,
    ResultsDisplayMapModel,
)
from pipeliner.starfile_handler import DataStarFile


class NodesTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_node_with_empty_name_raises_error(self):
        with self.assertRaises(ValueError):
            Node("", "test.node")

    def test_instantiate_node_superclass(self):
        node = Node("my_file.txt", "MyNodeType", ["Moja", "Mbili"])
        assert node.__dict__ == {
            "format": "txt",
            "format_converter": None,
            "input_for_processes_list": [],
            "kwds": ["moja", "mbili"],
            "name": "my_file.txt",
            "output_from_process": None,
            "toplevel_description": "A general nodetype for files without a more "
            "specific Node class defined",
            "toplevel_type": "MyNodeType",
            "type": "MyNodeType.txt.moja.mbili",
        }

    def test_check_file_is_mrc_works(self):
        mrcfile = os.path.join(self.test_data, "emd_3488.mrc")
        check = check_file_is_mrc(mrcfile)
        assert check
        nonmrcfile = os.path.join(self.test_data, "autopick.star")
        check = check_file_is_mrc(nonmrcfile)
        assert not check

    def test_check_file_is_text_works(self):
        mrcfile = os.path.join(self.test_data, "emd_3488.mrc")
        check = check_file_is_text(mrcfile)
        assert not check
        nonmrcfile = os.path.join(self.test_data, "autopick.star")
        check = check_file_is_text(nonmrcfile)
        assert check

    def test_check_file_is_cif_works(self):
        ciffile = os.path.join(self.test_data, "20starfile_converted.star")
        check = check_file_is_cif(ciffile)
        assert check
        nonciffile = os.path.join(self.test_data, "emd_3488.mrc")
        check = check_file_is_cif(nonciffile)
        assert not check

    def test_is_tiff_works(self):
        ciffile = os.path.join(self.test_data, "20starfile_converted.star")
        check = check_file_is_tif(ciffile)
        assert not check
        tifffile = os.path.join(self.test_data, "movie_tiff.tiff")
        check = check_file_is_tif(tifffile)
        assert check

    def test_node_format_generaliser(self):
        gen = GenNodeFormatConverter(
            allowed_ext={("mrc", "mrcs"): "mrc"},
            check_funct={"mrc": check_file_is_mrc},
        )
        mrcfile = os.path.join(self.test_data, "image_stack.mrcs")
        ext = gen.gen_nodetype(mrcfile)
        assert ext == "mrc"

    def test_node_format_generaliser_file_not_present(self):
        gen = GenNodeFormatConverter(
            allowed_ext={("mrc", "mrcs"): "mrc"},
            check_funct={"mrc": check_file_is_mrc},
        )
        ext = gen.gen_nodetype("ghost_file.mrc")
        assert ext == "mrc"

    def test_node_format_generaliser_type_not_in_allowed(self):
        gen = GenNodeFormatConverter(
            allowed_ext={("mrc", "mrcs"): "mrc"},
            check_funct={"mrc": check_file_is_mrc},
        )
        ext = gen.gen_nodetype("ghost_file.star")
        assert ext == "star"

    def test_node_format_generaliser_filetype_wrong(self):
        mrcfile = os.path.join(self.test_data, "image_stack.mrcs")
        shutil.copy(mrcfile, os.path.join(self.test_dir, "file.star"))
        gen = GenNodeFormatConverter(
            allowed_ext={("star"): "star"},
            check_funct={"star": check_file_is_cif},
        )
        ext = gen.gen_nodetype("file.star")
        assert ext == "XstarX"

    def test_node_updates_format(self):
        node = Node(
            name="test_node.log",
            toplevel_type="LogFile",
            kwds=["test"],
            toplevel_description="testnode",
            format_converter=GenNodeFormatConverter(
                allowed_ext={("log",): "txt"}, check_funct={}
            ),
        )

        assert node.format == "txt"
        assert node.type == "LogFile.txt.test"

    def test_node_format_no_ext_no_override(self):
        node = Node(
            name="test_node",
            toplevel_type="LogFile",
            kwds=["test"],
            toplevel_description="testnode",
            format_converter=GenNodeFormatConverter(
                allowed_ext={("log",): "txt"}, check_funct={}
            ),
        )

        assert node.format == "XX"
        assert node.type == "LogFile.XX.test"

    def test_node_format_no_ext_with_override(self):
        node = Node(
            name="test_node",
            toplevel_type="LogFile",
            kwds=["test"],
            toplevel_description="testnode",
            format_converter=GenNodeFormatConverter(
                allowed_ext={("log",): "txt"}, check_funct={}
            ),
            override_format="txt",
        )

        assert node.format == "txt"
        assert node.type == "LogFile.txt.test"

    def test_node_format_no_ext_with_override_generalised(self):
        node = Node(
            name="test_node",
            toplevel_type="LogFile",
            kwds=["test"],
            toplevel_description="testnode",
            format_converter=GenNodeFormatConverter(
                allowed_ext={("log",): "txt"}, check_funct={}
            ),
            override_format="log",
        )

        assert node.format == "txt"
        assert node.type == "LogFile.txt.test"

    def test_node_format_with_override(self):
        node = Node(
            name="test_node.XXX",
            toplevel_type="LogFile",
            kwds=["test"],
            toplevel_description="testnode",
            format_converter=GenNodeFormatConverter(
                allowed_ext={("log",): "txt"}, check_funct={}
            ),
            override_format="txt",
        )

        assert node.format == "txt"
        assert node.type == "LogFile.txt.test"

    def test_node_format_with_override_and_generalise(self):
        node = Node(
            name="test_node.XXX",
            toplevel_type="LogFile",
            kwds=["test"],
            toplevel_description="testnode",
            format_converter=GenNodeFormatConverter(
                allowed_ext={("log",): "txt"}, check_funct={}
            ),
            override_format="log",
        )

        assert node.format == "txt"
        assert node.type == "LogFile.txt.test"

    def test_making_default_display_non_text(self):
        mrcfile = os.path.join(self.test_data, "emd_3488.mrc")
        shutil.copy(mrcfile, "emd_3488.mrc")
        node = Node(name="emd_3488.mrc", toplevel_type="SomeNode")
        rdo = node.default_results_display(output_dir="myoutdir/")
        expected = {
            "title": "emd_3488.mrc",
            "dobj_type": "text",
            "start_collapsed": False,
            "display_data": "The node type for this file in myoutdir/ (SomeNode.mrc) "
            "has no default display method",
            "associated_data": ["emd_3488.mrc"],
            "flag": "",
        }
        assert rdo.__dict__ == expected

    def test_creating_default_node_densitymap(self):
        mrcfile = os.path.join(self.test_data, "emd_3488.mrc")
        shutil.copy(mrcfile, os.path.join(self.test_dir, "file.mrc"))
        os.makedirs("outdir")
        node = DensityMapNode(name="file.mrc")
        rdo = node.default_results_display(output_dir="outdir")
        expected = {
            "title": "Map: file.mrc",
            "dobj_type": "mapmodel",
            "start_collapsed": False,
            "maps": ["outdir/Thumbnails/file.mrc"],
            "maps_opacity": [1.0],
            "models": [],
            "maps_data": "file.mrc",
            "models_data": "",
            "associated_data": ["file.mrc"],
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }
        assert type(rdo) == ResultsDisplayMapModel
        assert rdo.__dict__ == expected
        assert os.path.isfile("outdir/Thumbnails/file.mrc")

    def test_making_default_general_node_text_files(self):
        textfile = os.path.join(self.test_data, "particles.star")
        shutil.copy(textfile, os.path.join(self.test_dir, "file.star"))
        node = Node(name="file.star", toplevel_type="Generic")
        rdo = node.default_results_display(output_dir="outdir")
        expected = {
            "file_path": "file.star",
            "dobj_type": "textfile",
            "start_collapsed": False,
            "title": "file.star",
            "flag": "",
        }
        assert type(rdo) == ResultsDisplayTextFile
        assert rdo.__dict__ == expected

    def test_making_default_node_relion_kwd_moviesgroup(self):
        movies = os.path.join(self.test_data, "movies_mrcs.star")
        movfile = os.path.join(self.test_data, "single.mrc")
        shutil.copy(movies, "movies.star")
        os.makedirs("Movies")
        for n in range(1, 25):
            os.symlink(movfile, f"Movies/20170629_{n:05d}_frameImage.mrcs")
        movnode = MicrographMovieGroupMetadataNode(name="movies.star", kwds=["relion"])
        do = movnode.default_results_display(output_dir="")
        assert do.__dict__ == {
            "title": "movies.star; 2/24 images",
            "start_collapsed": False,
            "dobj_type": "montage",
            "flag": "",
            "xvalues": [0, 1],
            "yvalues": [0, 0],
            "labels": [
                "Movies/20170629_00001_frameImage.mrcs",
                "Movies/20170629_00002_frameImage.mrcs",
            ],
            "associated_data": ["movies.star"],
            "img": "Thumbnails/montage_f0.png",
        }
        assert os.path.isfile("Thumbnails/montage_f0.png")

    def test_making_default_node_relion_kwd_particlesgroup(self):
        partsfile = os.path.join(self.test_data, "particles.star")
        shutil.copy(partsfile, "particles.star")
        os.makedirs("Extract/job007/Movies")
        imgstack = os.path.join(self.test_data, "image_stack.mrcs")
        shutil.copy(imgstack, "Extract/job007/Movies/20170629_00021_frameImage.mrcs")
        parts_node = ParticlesDataNode(name="particles.star", kwds=["relion"])
        do = parts_node.default_results_display(output_dir="")
        with open(
            os.path.join(self.test_data, "ResultsFiles/nodes_particles.json")
        ) as f:
            expected = json.load(f)
        assert do.__dict__ == expected
        assert os.path.isfile("Thumbnails/montage_s0.png")

    def test_making_default_node_relion_kwd_micsgroup(self):
        mics = os.path.join(self.test_data, "micrographs_ctf.star")
        micfile = os.path.join(self.test_data, "single.mrc")
        shutil.copy(mics, "micrographs.star")
        os.makedirs("MotionCorr/job002/Movies")
        for n in range(21, 32):
            os.symlink(
                micfile, f"MotionCorr/job002/Movies/20170629_{n:05d}_frameImage.mrc"
            )
        movnode = MicrographGroupMetadataNode(name="micrographs.star", kwds=["relion"])
        do = movnode.default_results_display(output_dir="")
        with open(os.path.join(self.test_data, "ResultsFiles/nodes_mics.json")) as f:
            expected = json.load(f)
        assert do.__dict__ == expected

    def test_making_default_node_relion_kwd_classavs(self):
        clavsfile = os.path.join(self.test_data, "class_averages.star")
        shutil.copy(clavsfile, "clavs.star")
        os.makedirs("Class2D/job008")
        imgstack = os.path.join(self.test_data, "image_stack.mrcs")
        shutil.copy(imgstack, "Class2D/job008/run_it025_classes.mrcs")
        clavs_node = Image2DGroupMetadataNode(
            name="clavs.star", kwds=["relion", "classaverages"]
        )
        do = clavs_node.default_results_display(output_dir="")
        assert do.__dict__ == {
            "title": "clavs.star; 5/5 images",
            "start_collapsed": False,
            "dobj_type": "montage",
            "flag": "",
            "xvalues": [0, 1, 2, 3, 4],
            "yvalues": [0, 0, 0, 0, 0],
            "labels": [
                "000023@Class2D/job008/run_it025_classes.mrcs",
                "000039@Class2D/job008/run_it025_classes.mrcs",
                "000040@Class2D/job008/run_it025_classes.mrcs",
                "000043@Class2D/job008/run_it025_classes.mrcs",
                "000046@Class2D/job008/run_it025_classes.mrcs",
            ],
            "associated_data": ["clavs.star"],
            "img": "Thumbnails/montage_s0.png",
        }
        assert os.path.isfile("Thumbnails/montage_s0.png")

    def test_making_default_node_relion_kwd_classavs_missing_kwd_gives_textfile(self):
        clavsfile = os.path.join(self.test_data, "class_averages.star")
        shutil.copy(clavsfile, "clavs.star")
        os.makedirs("Class2D/job008")
        imgstack = os.path.join(self.test_data, "image_stack.mrcs")
        shutil.copy(imgstack, "Class2D/job008/run_it025_classes.mrcs")
        clavs_node = Image2DGroupMetadataNode(name="clavs.star", kwds=["relion"])
        do = clavs_node.default_results_display(output_dir="")
        assert do.__dict__ == {
            "file_path": "clavs.star",
            "flag": "",
            "start_collapsed": False,
            "title": "clavs.star",
            "dobj_type": "textfile",
        }

    def test_writing_display_file_no_nodes_starfile(self):
        clavsfile = os.path.join(self.test_data, "class_averages.star")
        os.makedirs("Select/job009")
        shutil.copy(clavsfile, "Select/job009/clavs.star")
        os.makedirs("Class2D/job008")
        imgstack = os.path.join(self.test_data, "image_stack.mrcs")
        shutil.copy(imgstack, "Class2D/job008/run_it025_classes.mrcs")
        clavs_node = Image2DGroupMetadataNode(
            name="Select/job009/clavs.star", kwds=["relion", "classaverages"]
        )
        clavs_node.write_default_result_file()
        assert os.path.isfile("node_display.star")
        assert os.path.isdir("Select/job009/NodeDisplay")
        exp = DataStarFile(
            os.path.join(self.test_data, "ResultsFiles/nodes_results_single.star")
        )
        expected = exp.loop_as_list(
            block="pipeliner_node_display",
            columns=["_pipelinerNodeName", "_pipelinerDisplayFile"],
        )
        assert expected == (
            ["_pipelinerNodeName", "_pipelinerDisplayFile"],
            [
                [
                    "Select/job009/clavs.star",
                    "Select/job009/NodeDisplay/results_display001_montage.json",
                ],
            ],
        )

    def test_writing_display_file_nodes_starfile_exists(self):
        nd_file = os.path.join(self.test_data, "ResultsFiles/nodes_results_single.star")
        shutil.copy(nd_file, NODE_DISPLAY_FILE)
        clavsfile = os.path.join(self.test_data, "class_averages.star")
        os.makedirs("Select/job010")
        shutil.copy(clavsfile, "Select/job010/clavs.star")
        os.makedirs("Class2D/job008")
        imgstack = os.path.join(self.test_data, "image_stack.mrcs")
        shutil.copy(imgstack, "Class2D/job008/run_it025_classes.mrcs")
        clavs_node = Image2DGroupMetadataNode(
            name="Select/job010/clavs.star", kwds=["relion", "classaverages"]
        )
        exp = DataStarFile(NODE_DISPLAY_FILE)
        clavs_node.write_default_result_file()
        assert os.path.isfile(NODE_DISPLAY_FILE)
        assert os.path.isdir("Select/job010/NodeDisplay")

        expected = exp.loop_as_list(
            block="pipeliner_node_display",
            columns=["_pipelinerNodeName", "_pipelinerDisplayFile"],
        )
        assert expected == (
            ["_pipelinerNodeName", "_pipelinerDisplayFile"],
            [
                [
                    "Select/job009/clavs.star",
                    "Select/job009/NodeDisplay/results_display001_montage.json",
                ],
                [
                    "Select/job010/clavs.star",
                    "Select/job010/NodeDisplay/results_display001_montage.json",
                ],
            ],
        )

    def test_writing_display_file_already_in_nodes_starfile(self):
        nd_file = os.path.join(self.test_data, "ResultsFiles/nodes_results_single.star")
        shutil.copy(nd_file, NODE_DISPLAY_FILE)
        clavsfile = os.path.join(self.test_data, "class_averages.star")
        os.makedirs("Select/job009")
        shutil.copy(clavsfile, "Select/job009/clavs.star")
        os.makedirs("Class2D/job008")
        imgstack = os.path.join(self.test_data, "image_stack.mrcs")
        shutil.copy(imgstack, "Class2D/job008/run_it025_classes.mrcs")
        clavs_node = Image2DGroupMetadataNode(
            name="Select/job009/clavs.star", kwds=["relion", "classaverages"]
        )
        clavs_node.write_default_result_file()
        assert os.path.isfile("node_display.star")
        assert os.path.isdir("Select/job009/NodeDisplay")
        exp = DataStarFile(NODE_DISPLAY_FILE)
        expected = exp.loop_as_list(
            block="pipeliner_node_display",
            columns=["_pipelinerNodeName", "_pipelinerDisplayFile"],
        )
        assert expected == (
            ["_pipelinerNodeName", "_pipelinerDisplayFile"],
            [
                [
                    "Select/job009/clavs.star",
                    "Select/job009/NodeDisplay/results_display001_montage.json",
                ],
            ],
        )

    def test_get_node_display_object_already_in_starfile(self):
        os.makedirs("Class2D/job008")
        imgstack = os.path.join(self.test_data, "image_stack.mrcs")
        shutil.copy(imgstack, "Class2D/job008/run_it025_classes.mrcs")

        nd_file = os.path.join(self.test_data, "ResultsFiles/nodes_results_multi.star")
        shutil.copy(nd_file, NODE_DISPLAY_FILE)
        clavsfile = os.path.join(self.test_data, "class_averages.star")
        os.makedirs("Select/job009")
        os.makedirs(f"Select/job009/{NODE_DISPLAY_DIR}")
        shutil.copy(clavsfile, "Select/job009/clavs.star")
        dispfile = os.path.join(self.test_data, "ResultsFiles/select_2dauto.json")
        shutil.copy(
            dispfile,
            f"Select/job009/{NODE_DISPLAY_DIR}/results_display001_montage.json",
        )
        clavs_node = Image2DGroupMetadataNode(
            name="Select/job009/clavs.star", kwds=["relion", "classaverages"]
        )
        dispobj = clavs_node.get_result_display_object()
        with open(dispfile) as exp:
            expected = json.load(exp)
        assert dispobj.__dict__ == expected

    def test_get_node_display_object_not_in_starfile(self):
        """If not in the star file it should be created"""
        os.makedirs("Class2D/job008")
        imgstack = os.path.join(self.test_data, "image_stack.mrcs")
        shutil.copy(imgstack, "Class2D/job008/run_it025_classes.mrcs")

        nd_file = os.path.join(self.test_data, "ResultsFiles/nodes_results_single.star")
        shutil.copy(nd_file, NODE_DISPLAY_FILE)

        clavsfile = os.path.join(self.test_data, "class_averages.star")
        os.makedirs("Select/job010")
        shutil.copy(clavsfile, "Select/job010/clavs.star")
        clavs_node = Image2DGroupMetadataNode(
            name="Select/job010/clavs.star", kwds=["relion", "classaverages"]
        )
        dispobj = clavs_node.get_result_display_object()
        assert dispobj.__dict__ == {
            "title": "Select/job010/clavs.star; 5/5 images",
            "start_collapsed": False,
            "dobj_type": "montage",
            "flag": "",
            "xvalues": [0, 1, 2, 3, 4],
            "yvalues": [0, 0, 0, 0, 0],
            "labels": [
                "000023@Class2D/job008/run_it025_classes.mrcs",
                "000039@Class2D/job008/run_it025_classes.mrcs",
                "000040@Class2D/job008/run_it025_classes.mrcs",
                "000043@Class2D/job008/run_it025_classes.mrcs",
                "000046@Class2D/job008/run_it025_classes.mrcs",
            ],
            "associated_data": ["Select/job010/clavs.star"],
            "img": "Select/job010/Thumbnails/montage_s0.png",
        }

    def test_get_node_display_object_starfile_doesnt_exist(self):
        """If not in the star file it should be created"""

        os.makedirs("Class2D/job008")
        imgstack = os.path.join(self.test_data, "image_stack.mrcs")
        shutil.copy(imgstack, "Class2D/job008/run_it025_classes.mrcs")

        clavsfile = os.path.join(self.test_data, "class_averages.star")
        os.makedirs("Select/job010")
        shutil.copy(clavsfile, "Select/job010/clavs.star")
        clavs_node = Image2DGroupMetadataNode(
            name="Select/job010/clavs.star", kwds=["relion", "classaverages"]
        )
        dispobj = clavs_node.get_result_display_object()
        assert os.path.isfile(NODE_DISPLAY_FILE)
        assert os.path.isfile("Select/job010/Thumbnails/montage_s0.png")
        assert dispobj.__dict__ == {
            "title": "Select/job010/clavs.star; 5/5 images",
            "start_collapsed": False,
            "dobj_type": "montage",
            "flag": "",
            "xvalues": [0, 1, 2, 3, 4],
            "yvalues": [0, 0, 0, 0, 0],
            "labels": [
                "000023@Class2D/job008/run_it025_classes.mrcs",
                "000039@Class2D/job008/run_it025_classes.mrcs",
                "000040@Class2D/job008/run_it025_classes.mrcs",
                "000043@Class2D/job008/run_it025_classes.mrcs",
                "000046@Class2D/job008/run_it025_classes.mrcs",
            ],
            "associated_data": ["Select/job010/clavs.star"],
            "img": "Select/job010/Thumbnails/montage_s0.png",
        }

    def test_get_node_display_object_rewrite_pending(self):
        os.makedirs("Class2D/job008")
        imgstack = os.path.join(self.test_data, "image_stack.mrcs")
        shutil.copy(imgstack, "Class2D/job008/run_it025_classes.mrcs")

        nd_file = os.path.join(
            self.test_data, "ResultsFiles/nodes_results_multi_pending.star"
        )
        shutil.copy(nd_file, NODE_DISPLAY_FILE)
        clavsfile = os.path.join(self.test_data, "class_averages.star")
        os.makedirs("Select/job009")
        os.makedirs(f"Select/job009/{NODE_DISPLAY_DIR}")
        shutil.copy(clavsfile, "Select/job009/clavs.star")
        dispfile = os.path.join(
            self.test_data, "ResultsFiles/pending_results_file.json"
        )
        shutil.copy(
            dispfile,
            f"Select/job009/{NODE_DISPLAY_DIR}/results_display001_pending.json",
        )
        clavs_node = Image2DGroupMetadataNode(
            name="Select/job009/clavs.star", kwds=["relion", "classaverages"]
        )
        dispobj = clavs_node.get_result_display_object()
        assert dispobj.__dict__ == {
            "title": "Select/job009/clavs.star; 5/5 images",
            "start_collapsed": False,
            "dobj_type": "montage",
            "flag": "",
            "xvalues": [0, 1, 2, 3, 4],
            "yvalues": [0, 0, 0, 0, 0],
            "labels": [
                "000023@Class2D/job008/run_it025_classes.mrcs",
                "000039@Class2D/job008/run_it025_classes.mrcs",
                "000040@Class2D/job008/run_it025_classes.mrcs",
                "000043@Class2D/job008/run_it025_classes.mrcs",
                "000046@Class2D/job008/run_it025_classes.mrcs",
            ],
            "associated_data": ["Select/job009/clavs.star"],
            "img": "Select/job009/Thumbnails/montage_s0.png",
        }
        assert os.path.isfile("Select/job009/Thumbnails/montage_s0.png")
        assert not os.path.isfile("Select/job009/results_display0_pending.json")

        exp_nd_file = os.path.join(
            self.test_data, "ResultsFiles/nodes_results_multi.star"
        )
        exp_nd = DataStarFile(exp_nd_file)
        actual_nd = DataStarFile(NODE_DISPLAY_FILE)
        key = "pipeliner_node_display"
        assert exp_nd.loop_as_list(key) == actual_nd.loop_as_list(key)


if __name__ == "__main__":
    unittest.main()
