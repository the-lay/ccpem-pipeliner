#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import json
import re
from glob import glob

from pipeliner.project_graph import ProjectGraph
from pipeliner.utils import touch
from pipeliner_tests import test_data
from pipeliner.data_structure import SUCCESS_FILE, JOBINFO_FILE
from pipeliner_tests.generic_tests import ShortpipeFileStructure
from pipeliner_tests.generic_tests import do_slow_tests
from pipeliner.nodes import Node

do_slow_tests = do_slow_tests()


class ProjectGraphCleanupTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @unittest.skipUnless(do_slow_tests, "Slow test - skip unless full test")
    def test_making_file_structure(self):
        outdict = ShortpipeFileStructure(["all"]).outfiles
        filecounts = {
            "Import": 9,
            "MotionCorr": 1017,
            "CtfFind": 422,
            "AutoPick": 210,
            "Extract": 209,
            "Select": 11,
            "Class2D": 133,
            "Class3D": 258,
            "Refine3D": 159,
            "Polish": 712,
            "MaskCreate": 10,
            "JoinStar": 9,
            "Subtract": 39,
            "PostProcess": 15,
            "InitialModel": 1508,
            "MultiBody": 219,
            "CtfRefine": 518,
            "External": 18,
            "LocalRes": 11,
        }
        for proc in outdict:
            assert len(outdict[proc]) == filecounts[proc], (
                proc,
                len(outdict[proc]),
                filecounts[proc],
            )

    def test_read_short_pipeline(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        pipeline_data = ProjectGraph(name="short_full")
        pipeline_data.read()
        processes = [
            "Import/job001/",
            "MotionCorr/job002/",
            "CtfFind/job003/",
            "AutoPick/job004/",
            "Extract/job005/",
            "Class2D/job006/",
            "Select/job007/",
            "InitialModel/job008/",
            "Class3D/job009/",
            "Refine3D/job010/",
            "MultiBody/job011/",
            "CtfRefine/job012/",
            "MaskCreate/job013/",
            "Polish/job014/",
            "JoinStar/job015/",
            "Subtract/job016/",
            "PostProcess/job017/",
            "External/job018/",
            "LocalRes/job019/",
        ]
        nodes = [
            "Import/job001/movies.star",
            "MotionCorr/job002/corrected_micrographs.star",
            "MotionCorr/job002/logfile.pdf",
            "CtfFind/job003/micrographs_ctf.star",
            "CtfFind/job003/logfile.pdf",
            "AutoPick/job004/coords_suffix_autopick.star",
            "AutoPick/job004/logfile.pdf",
            "Extract/job005/particles.star",
            "Class2D/job006/run_it025_data.star",
            "Class2D/job006/run_it025_optimiser.star",
            "Select/job007/selected_particles.star",
            "InitialModel/job008/run_it150_class001.mrc",
            "InitialModel/job008/run_it150_class002.mrc",
            "InitialModel/job008/run_it150_class001_data.star",
            "InitialModel/job008/run_it150_class002_data.star",
            "InitialModel/job008/run_it150_optimiser.star",
            "Class3D/job009/run_it025_class001.mrc",
            "Class3D/job009/run_it025_class002.mrc",
            "Class3D/job009/run_it025_class003.mrc",
            "Class3D/job009/run_it025_data.star",
            "Class3D/job009/run_it025_optimiser.star",
            "Refine3D/job010/run_data.star",
            "Refine3D/job010/run_optimiser.star",
            "Refine3D/job010/run_class001.mrc",
            "Refine3D/job010/run_class001_half1_unfil.mrc",
            "MultiBody/job011/run_class001_half1_unfil.mrc",
            "MultiBody/job011/run_class002_half1_unfil.mrc",
            "CtfRefine/job012/logfile.pdf",
            "CtfRefine/job012/particles_ctf_refine.star",
            "MaskCreate/job013/mask.mrc",
            "Polish/job014/opt_params_all_groups.txt",
            "Polish/job014/logfile.pdf",
            "Polish/job014/shiny.star",
            "JoinStar/job015/join_particles.star",
            "Subtract/job016/particles_subtract.star",
            "PostProcess/job017/logfile.pdf",
            "PostProcess/job017/postprocess.star",
            "LocalRes/job019/relion_locres_filtered.mrc",
            "LocalRes/job019/relion_locres.mrc",
        ]
        process_names = [x.name for x in pipeline_data.process_list]
        node_names = [x.name for x in pipeline_data.node_list]

        for i in processes:
            assert i in process_names, i
        for i in nodes:
            assert i in node_names, i

    def test_cleanup_import(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        pipeline_data = ProjectGraph(name="short_full")
        pipeline_data.read()

        outfiles = ShortpipeFileStructure(["Import"]).outfiles
        files = glob("Import/job001/*")

        assert len(files) == len(outfiles["Import"])
        pipeline_data.clean_up_job(pipeline_data.process_list[0], False)
        assert len(files) == len(outfiles["Import"])

    @unittest.skipUnless(do_slow_tests, "Slow test - skip unless full test")
    def test_cleanup_motioncorr(self):
        procname = "MotionCorr"
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        pipeline_data = ProjectGraph(name="short_full")
        pipeline_data.read()

        outfiles = ShortpipeFileStructure([procname]).outfiles
        files = glob("MotionCorr/job002/**/*", recursive=True)
        assert len(files) - 1 == len(outfiles[procname])
        pipeline_data.clean_up_job(pipeline_data.process_list[1], False)

        del_exts = ["com", "out", "err", "log"]
        removed, kept = [], []
        excluded = [
            "MotionCorr/job002/run.out",
            "MotionCorr/job002/run.err",
            "MotionCorr/job002/" + SUCCESS_FILE,
        ]
        for f in outfiles[procname]:
            if f not in excluded:
                if f.split(".")[1] in del_exts:
                    removed.append(f)
                else:
                    kept.append(f)

        files = glob("MotionCorr/job002/**/*", recursive=True)
        for f in kept:
            assert f in files, f
        for f in removed:
            assert f not in files, f

        # check the jobinfo file was written correctly
        with open(f"MotionCorr/job002/{JOBINFO_FILE}", "r") as jif:
            jobinfo = json.load(jif)
            assert jobinfo["job directory"] == "MotionCorr/job002/"
            assert jobinfo["comments"] == []
            assert jobinfo["rank"] is None
            ts = jobinfo["created"]
            assert re.match(r"\d+-\d+-\d+ \d+:\d+:\d+", ts)
            assert (
                jobinfo["history"][ts] == "Cleanup; 700 file(s), 0 directory(s) removed"
            )
            assert jobinfo["command history"] == {}

    @unittest.skipUnless(do_slow_tests, "Slow test - skip unless full test")
    def test_cleanup_motioncorr_harsh(self):
        procname = "MotionCorr"
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        pipeline_data = ProjectGraph(name="short_full")
        pipeline_data.read()

        outfiles = ShortpipeFileStructure([procname]).outfiles

        files = glob("MotionCorr/job002/**/*", recursive=True)
        assert len(files) - 1 == len(outfiles[procname])
        assert os.path.isdir("MotionCorr/job002/Raw_data")
        pipeline_data.clean_up_job(pipeline_data.process_list[1], True)
        assert not os.path.isdir("MotionCorr/job002/Raw_data")

        # check the jobinfo file was written correctly
        with open(f"MotionCorr/job002/{JOBINFO_FILE}", "r") as jif:
            ji = json.load(jif)
            assert ji["job directory"] == "MotionCorr/job002/"
            assert ji["comments"] == []
            assert ji["rank"] is None
            ts = ji["created"]
            assert re.match(r"\d+-\d+-\d+ \d+:\d+:\d+", ts)
            assert (
                ji["history"][ts]
                == "Harsh cleanup; 1000 file(s), 1 directory(s) removed"
            )
            assert ji["command history"] == {}

    def test_cleanup_ctffind(self):
        procname = "CtfFind"
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        pipeline_data = ProjectGraph(name="short_full")
        pipeline_data.read()

        outfiles = ShortpipeFileStructure([procname]).outfiles

        files = glob("CtfFind/job003/**/*", recursive=True)
        assert len(files) - 1 == len(outfiles[procname]), (
            len(files) - 1,
            len(outfiles[procname]),
        )

        globlist = [
            "CtfFind/job003/gctf*.out",
            "CtfFind/job003/gctf*.err",
            "CtfFind/job003/Raw_data/*",
        ]

        del_files = []
        for f in globlist:
            del_files += glob(f)

        pipeline_data.clean_up_job(pipeline_data.process_list[2], False)

        removed, kept = [], []
        for f in outfiles[procname]:
            if f in del_files:
                removed.append(f)
            else:
                kept.append(f)
        files = glob("CtfFind/job003/**/*", recursive=True)
        for f in kept:
            assert f in files, f
        for f in removed:
            assert f not in files, f
        assert not os.path.isdir("CtfFind/job003/Raw_data")

    def test_cleanup_autopick(self):
        procname = "AutoPick"
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        pipeline_data = ProjectGraph(name="short_full")
        pipeline_data.read()

        outfiles = ShortpipeFileStructure([procname]).outfiles

        files = glob("AutoPick/job004/**/*", recursive=True)
        assert len(files) - 1 == len(outfiles[procname]), (
            len(files) - 1,
            len(outfiles[procname]),
        )

        globlist = ["AutoPick/job004/Raw_data/*.spi"]

        del_files = []
        for f in globlist:
            del_files += glob(f)

        pipeline_data.clean_up_job(pipeline_data.process_list[3], False)

        removed, kept = [], []
        for f in outfiles[procname]:
            if f in del_files:
                removed.append(f)
            else:
                kept.append(f)
        files = glob("AutoPick/job004/**/*", recursive=True)

        for f in kept:
            assert f in files, f
        for f in removed:
            assert f not in files, f

    def test_cleanup_extract(self):
        procname = "Extract"
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        pipeline_data = ProjectGraph(name="short_full")
        pipeline_data.read()

        outfiles = ShortpipeFileStructure([procname]).outfiles

        files = glob("Extract/job005/**/*", recursive=True)
        assert len(files) - 1 == len(outfiles[procname]), (
            len(files) - 1,
            len(outfiles[procname]),
        )

        globlist = ["Extract/job005/Raw_data/*_extract.star"]

        del_files = []
        for f in globlist:
            del_files += glob(f)

        pipeline_data.clean_up_job(pipeline_data.process_list[4], False)

        removed, kept = [], []
        for f in outfiles[procname]:
            if f in del_files:
                removed.append(f)
            else:
                kept.append(f)
        files = glob("Extract/job005/**/*", recursive=True)

        for f in kept:
            assert f in files, f
        for f in removed:
            assert f not in files, f

    def test_cleanup_extract_harsh(self):
        procname = "Extract"
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        pipeline_data = ProjectGraph(name="short_full")
        pipeline_data.read()

        ShortpipeFileStructure([procname])

        assert os.path.isdir("Extract/job005/Raw_data")

        pipeline_data.clean_up_job(pipeline_data.process_list[4], True)

        assert not os.path.isdir("Extract/job005/Raw_data")

    def test_cleanup_Class2D(self):
        procname = "Class2D"
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        pipeline_data = ProjectGraph(name="short_full")
        pipeline_data.read()

        outfiles = ShortpipeFileStructure([procname]).outfiles

        files = glob("Class2D/job006/*")
        assert len(files) == len(outfiles[procname]), (
            len(files),
            len(outfiles[procname]),
        )

        del_files = glob("Class2D/job006/run_it*")
        for f in [
            "Class2D/job006/run_it025_data.star",
            "Class2D/job006/run_it025_sampling.star",
            "Class2D/job006/run_it025_model.star",
            "Class2D/job006/run_it025_optimiser.star",
            "Class2D/job006/run_it025_classes.mrcs",
        ]:
            del_files.remove(f)

        pipeline_data.clean_up_job(pipeline_data.process_list[5], False)

        removed, kept = [], []
        for f in outfiles[procname]:
            if f in del_files:
                removed.append(f)
            else:
                kept.append(f)

        files = glob("Class2D/job006/*")
        trash = glob("Trash/**/**/*")
        for f in kept:
            assert f in files, f
        for f in removed:
            assert f not in files, f
            assert "Trash/" + f in trash, f

    def test_cleanup_select(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        ShortpipeFileStructure(["Select"])
        pre_files = glob("Select/job007/*")
        pipeline_data = ProjectGraph(name="short_full")
        pipeline_data.read()
        pipeline_data.clean_up_job(pipeline_data.process_list[6], False)
        post_files = glob("Select/job007/*")

        # make sure nothing changed
        assert pre_files == post_files

    @unittest.skipUnless(do_slow_tests, "Slow test - skip unless full test")
    def test_cleanup_inimodel(self):
        procname = "InitialModel"
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        outfiles = ShortpipeFileStructure([procname]).outfiles
        files = glob("InitialModel/job008/*")
        assert len(files) == len(outfiles[procname]), (
            len(files),
            len(outfiles[procname]),
        )

        # list of files that should be deleted
        del_files = glob("InitialModel/job008/run_it*")
        for f in [
            "InitialModel/job008/run_it150_data.star",
            "InitialModel/job008/run_it150_class001_data.star",
            "InitialModel/job008/run_it150_class002_data.star",
            "InitialModel/job008/run_it150_sampling.star",
            "InitialModel/job008/run_it150_model.star",
            "InitialModel/job008/run_it150_optimiser.star",
            "InitialModel/job008/run_it150_class001.mrc",
            "InitialModel/job008/run_it150_grad001.mrc",
            "InitialModel/job008/run_it150_class002.mrc",
            "InitialModel/job008/run_it150_grad002.mrc",
        ]:
            del_files.remove(f)

        # do the cleanup
        pipeline_data = ProjectGraph(name="short_full")
        pipeline_data.read()
        pipeline_data.clean_up_job(pipeline_data.process_list[7], False)

        # sort the files
        removed, kept = [], []
        for f in outfiles[procname]:
            if f in del_files:
                removed.append(f)
            else:
                kept.append(f)
        # check they are all in the right place
        files = glob("InitialModel/job008/*")
        trash = glob("Trash/**/**/*")
        for f in kept:
            assert f in files, f
        for f in removed:
            assert f not in files, f
            assert "Trash/" + f in trash, f

    def test_cleanup_class3D(self):
        procname = "Class3D"
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        outfiles = ShortpipeFileStructure([procname]).outfiles
        files = glob("Class3D/job009/*")
        assert len(files) == len(outfiles[procname]), (
            len(files),
            len(outfiles[procname]),
        )

        # list of files that should be deleted
        del_files = glob("Class3D/job009/run_it*")
        for f in [
            "Class3D/job009/run_it025_class001.mrc",
            "Class3D/job009/run_it025_class001_angdist.bild",
            "Class3D/job009/run_it025_class002.mrc",
            "Class3D/job009/run_it025_class002_angdist.bild",
            "Class3D/job009/run_it025_class003.mrc",
            "Class3D/job009/run_it025_class003_angdist.bild",
            "Class3D/job009/run_it025_sampling.star",
            "Class3D/job009/run_it025_data.star",
            "Class3D/job009/run_it025_model.star",
            "Class3D/job009/run_it025_optimiser.star",
        ]:
            del_files.remove(f)

        # do the cleanup
        pipeline_data = ProjectGraph(name="short_full")
        pipeline_data.read()
        pipeline_data.clean_up_job(pipeline_data.process_list[8], False)

        # sort the files
        removed, kept = [], []
        for f in outfiles[procname]:
            if f in del_files:
                removed.append(f)
            else:
                kept.append(f)
        # check they are all in the right place
        files = glob("Class3D/job009/*")
        trash = glob("Trash/**/**/*")
        for f in kept:
            assert f in files, f
        for f in removed:
            assert f not in files, f
            assert "Trash/" + f in trash, f

    def test_cleanup_Refine3D(self):
        procname = "Refine3D"
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        outfiles = ShortpipeFileStructure([procname]).outfiles
        files = glob("Refine3D/job010/*")
        assert len(files) == len(outfiles[procname]), (
            len(files),
            len(outfiles[procname]),
        )
        # list of files that should be deleted
        del_files = glob("Refine3D/job010/run_it*")
        for f in [
            "Refine3D/job010/run_it016_half1_class001.mrc",
            "Refine3D/job010/run_it016_half1_class001_angdist.bild",
            "Refine3D/job010/run_it016_half1_model.star",
            "Refine3D/job010/run_it016_half2_class001.mrc",
            "Refine3D/job010/run_it016_half2_class001_angdist.bild",
            "Refine3D/job010/run_it016_half2_model.star",
            "Refine3D/job010/run_it016_sampling.star",
            "Refine3D/job010/run_it016_data.star",
            "Refine3D/job010/run_it016_optimiser.star",
        ]:
            del_files.remove(f)

        # do the cleanup
        pipeline_data = ProjectGraph(name="short_full")
        pipeline_data.read()
        pipeline_data.clean_up_job(pipeline_data.process_list[9], False)

        # sort the files
        removed, kept = [], []
        for f in outfiles[procname]:
            if f in del_files:
                removed.append(f)
            else:
                kept.append(f)
        # check they are all in the right place
        files = glob("Refine3D/job010/*")
        trash = glob("Trash/**/**/*")
        for f in kept:
            assert f in files, f
        for f in removed:
            assert f not in files, f
            assert "Trash/" + f in trash, f

    def test_cleanup_Refine3D_manual_node(self):
        """Special case where user has manually designated an iteration's results
        as an outout node so the associated files should be preserved as well"""
        procname = "Refine3D"
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        outfiles = ShortpipeFileStructure([procname]).outfiles
        files = glob("Refine3D/job010/*")
        assert len(files) == len(outfiles[procname]), (
            len(files),
            len(outfiles[procname]),
        )
        # list of files that should be deleted
        del_files = glob("Refine3D/job010/run_it*")
        for f in [
            "Refine3D/job010/run_it016_half1_class001.mrc",
            "Refine3D/job010/run_it016_half1_class001_angdist.bild",
            "Refine3D/job010/run_it016_half1_model.star",
            "Refine3D/job010/run_it016_half2_class001.mrc",
            "Refine3D/job010/run_it016_half2_class001_angdist.bild",
            "Refine3D/job010/run_it016_half2_model.star",
            "Refine3D/job010/run_it016_sampling.star",
            "Refine3D/job010/run_it016_data.star",
            "Refine3D/job010/run_it016_optimiser.star",
            "Refine3D/job010/run_it011_half1_class001.mrc",
            "Refine3D/job010/run_it011_half1_class001_angdist.bild",
            "Refine3D/job010/run_it011_half1_model.star",
            "Refine3D/job010/run_it011_half2_class001.mrc",
            "Refine3D/job010/run_it011_half2_class001_angdist.bild",
            "Refine3D/job010/run_it011_half2_model.star",
            "Refine3D/job010/run_it011_sampling.star",
            "Refine3D/job010/run_it011_data.star",
            "Refine3D/job010/run_it011_optimiser.star",
        ]:
            del_files.remove(f)

        # do the cleanup
        pipeline_data = ProjectGraph(name="short_full")
        pipeline_data.read()
        jobproc = pipeline_data.process_list[9]
        new_node = Node(
            "Refine3D/job010/run_it011_half1_class001.mrc",
            "DensityMap",
            ["relion", "halfmap"],
        )
        jobproc.output_nodes.append(new_node)
        pipeline_data.clean_up_job(jobproc, False)

        # sort the files
        removed, kept = [], []
        for f in outfiles[procname]:
            if f in del_files:
                removed.append(f)
            else:
                kept.append(f)
        # check they are all in the right place
        files = glob("Refine3D/job010/*")
        trash = glob("Trash/**/**/*")
        for f in kept:
            assert f in files, f
        for f in removed:
            assert f not in files, f
            assert "Trash/" + f in trash, f

    def test_cleanup_MultiBody(self):
        procname = "MultiBody"
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        outfiles = ShortpipeFileStructure([procname]).outfiles
        files = glob("MultiBody/job011/*")
        assert len(files) == len(outfiles[procname]), (
            len(files),
            len(outfiles[procname]),
        )

        # list of files that should be deleted
        del_files = glob("MultiBody/job011/run_it*")
        for f in [
            "MultiBody/job011/run_it012_data.star",
            "MultiBody/job011/run_it012_half1_body001_angdist.bild",
            "MultiBody/job011/run_it012_half1_body001.mrc",
            "MultiBody/job011/run_it012_half1_body002_angdist.bild",
            "MultiBody/job011/run_it012_half1_body002.mrc",
            "MultiBody/job011/run_it012_half1_model.star",
            "MultiBody/job011/run_it012_half2_body001_angdist.bild",
            "MultiBody/job011/run_it012_half2_body001.mrc",
            "MultiBody/job011/run_it012_half2_body002_angdist.bild",
            "MultiBody/job011/run_it012_half2_body002.mrc",
            "MultiBody/job011/run_it012_half2_model.star",
        ]:
            del_files.remove(f)

        # do the cleanup
        pipeline_data = ProjectGraph(name="short_full")
        pipeline_data.read()
        pipeline_data.clean_up_job(pipeline_data.process_list[10], False)

        # sort the files
        removed, kept = [], []
        for f in outfiles[procname]:
            if f in del_files:
                removed.append(f)
            else:
                kept.append(f)
        # check they are all in the right place
        files = glob("MultiBody/job011/*")
        trash = glob("Trash/**/**/*")

        for f in kept:
            assert f in files, f
        for f in removed:
            assert f not in files, f
            assert "Trash/" + f in trash, f

    def test_cleanup_MultiBody_harsh(self):
        procname = "MultiBody"
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        outfiles = ShortpipeFileStructure([procname]).outfiles
        files = glob("MultiBody/job011/*")
        assert len(files) == len(outfiles[procname]), (
            len(files),
            len(outfiles[procname]),
        )

        # list of files that should be deleted
        del_paths = [
            "MultiBody/job011/run_it*",
            "MultiBody/job011/analyse_component*_bin*.mrc",
        ]
        del_files = list()
        for p in del_paths:
            del_files += glob(p)

        for f in [
            "MultiBody/job011/run_it012_data.star",
            "MultiBody/job011/run_it012_half1_body001_angdist.bild",
            "MultiBody/job011/run_it012_half1_body001.mrc",
            "MultiBody/job011/run_it012_half1_body002_angdist.bild",
            "MultiBody/job011/run_it012_half1_body002.mrc",
            "MultiBody/job011/run_it012_half1_model.star",
            "MultiBody/job011/run_it012_half2_body001_angdist.bild",
            "MultiBody/job011/run_it012_half2_body001.mrc",
            "MultiBody/job011/run_it012_half2_body002_angdist.bild",
            "MultiBody/job011/run_it012_half2_body002.mrc",
            "MultiBody/job011/run_it012_half2_model.star",
        ]:
            del_files.remove(f)

        # do the cleanup
        pipeline_data = ProjectGraph(name="short_full")
        pipeline_data.read()
        pipeline_data.clean_up_job(pipeline_data.process_list[10], True)

        # sort the files
        removed, kept = [], []
        for f in outfiles[procname]:
            if f in del_files:
                removed.append(f)
            else:
                kept.append(f)
        # check they are all in the right place
        files = glob("MultiBody/job011/*")
        trash = glob("Trash/**/**/*")

        for f in kept:
            assert f in files, f
        for f in removed:
            assert f not in files, f
            assert "Trash/" + f in trash, f

    def test_cleanup_ctfrefine(self):
        procname = "CtfRefine"
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        outfiles = ShortpipeFileStructure([procname]).outfiles

        files = glob("CtfRefine/job012/**/*", recursive=True)

        assert len(files) == len(outfiles[procname]) + 1, (
            len(files),
            len(outfiles[procname]) + 1,
        )

        del_files = []
        for ext in ["*_wAcc.mrc", "*_xyAcc_real.mrc", "*_xyAcc_imag.mrc"]:
            del_files += glob("CtfRefine/job012/Raw_data/" + ext)

        # do the cleanup
        pipeline_data = ProjectGraph(name="short_full")
        pipeline_data.read()
        pipeline_data.clean_up_job(pipeline_data.process_list[11], False)

        # sort the files
        removed, kept = [], []
        for f in outfiles[procname]:
            if f in del_files:
                removed.append(f)
            else:
                kept.append(f)

        # check they are all in the right place
        files = glob("CtfRefine/job012/**/*", recursive=True)
        trash = glob("Trash/**/**/**/*", recursive=True)
        for f in kept:
            assert f in files, f
        for f in removed:
            assert f not in files, f
            assert "Trash/" + f in trash, f

    def test_cleanup_maskcreate(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        ShortpipeFileStructure(["MaskCreate"])
        pre_files = glob("MaskCreate/job013/*")
        pipeline_data = ProjectGraph(name="short_full")
        pipeline_data.read()
        pipeline_data.clean_up_job(pipeline_data.process_list[12], False)
        post_files = glob("MaskCreate/job013/*")

        # make sure nothing changed
        assert pre_files == post_files

    def test_cleanup_polish(self):
        procname = "Polish"
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        outfiles = ShortpipeFileStructure([procname]).outfiles

        files = glob("Polish/job014/**/*", recursive=True)

        assert len(files) == len(outfiles[procname]) + 1, (
            len(files),
            len(outfiles[procname]) + 1,
        )

        del_files = []
        for ext in ["*_FCC_cc.mrc", "*_FCC_w0.mrc", "*_FCC_w1.mrc"]:
            del_files += glob("Polish/job014/Raw_data/" + ext)

        # do the cleanup
        pipeline_data = ProjectGraph(name="short_full")
        pipeline_data.read()
        pipeline_data.clean_up_job(pipeline_data.process_list[13], False)

        # sort the files
        removed, kept = [], []
        for f in outfiles[procname]:
            if f in del_files:
                removed.append(f)
            else:
                kept.append(f)

        # check they are all in the right place
        files = glob("Polish/job014/**/*", recursive=True)
        trash = glob("Trash/**/**/**/*", recursive=True)
        for f in kept:
            assert f in files, f
        for f in removed:
            assert f not in files, f
            assert "Trash/" + f in trash, f

    def test_cleanup_polish_harsh(self):
        procname = "Polish"
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        outfiles = ShortpipeFileStructure([procname]).outfiles

        files = glob("Polish/job014/**/*", recursive=True)

        assert len(files) == len(outfiles[procname]) + 1, (
            len(files),
            len(outfiles[procname]) + 1,
        )

        del_files = []
        for ext in [
            "*_FCC_cc.mrc",
            "*_FCC_w0.mrc",
            "*_FCC_w1.mrc",
            "*shiny.star",
            "*shiny.mrcs",
        ]:
            del_files += glob("Polish/job014/Raw_data/" + ext)

        # do the cleanup
        pipeline_data = ProjectGraph(name="short_full")
        pipeline_data.read()
        pipeline_data.clean_up_job(pipeline_data.process_list[13], True)

        # sort the files
        removed, kept = [], []
        for f in outfiles[procname]:
            if f in del_files:
                removed.append(f)
            else:
                kept.append(f)

        # check they are all in the right place
        files = glob("Polish/job014/**/*", recursive=True)
        trash = glob("Trash/**/**/**/*", recursive=True)
        for f in kept:
            assert f in files, f
        for f in removed:
            assert f not in files, f
            assert "Trash/" + f in trash, f

    def test_cleanup_joinstar(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        ShortpipeFileStructure(["JoinStar"])
        pre_files = glob("JoinStar/job015/*")
        pipeline_data = ProjectGraph(name="short_full")
        pipeline_data.read()
        pipeline_data.clean_up_job(pipeline_data.process_list[14], False)
        post_files = glob("JoinStar/job015/*")

        # make sure nothing changed
        assert pre_files == post_files

    def test_cleanup_subtract_gentle(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        ShortpipeFileStructure(["Subtract"])
        pre_files = glob("Subtract/job016/*")
        pipeline_data = ProjectGraph(name="short_full")
        pipeline_data.read()
        pipeline_data.clean_up_job(pipeline_data.process_list[15], False)
        post_files = glob("Subtract/job016/*")

        # make sure nothing changed
        assert pre_files == post_files

    def test_cleanup_subtract_harsh(self):
        # The harsh clean on the subtraction job doesn't do anything
        # as written currently, ned to sort out what files they actually
        # want deleted

        procname = "Subtract"
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        outfiles = ShortpipeFileStructure([procname]).outfiles

        files = glob("Subtract/job016/**/*", recursive=True)

        assert len(files) == len(outfiles[procname]) + 1, (
            len(files),
            len(outfiles[procname]) + 1,
        )

        del_files = glob("Subtract/job016/*subtracted.star")
        # do the cleanup
        pipeline_data = ProjectGraph(name="short_full")
        pipeline_data.read()
        pipeline_data.clean_up_job(pipeline_data.process_list[15], True)

        # sort the files
        removed, kept = [], []
        for f in outfiles[procname]:
            if f in del_files:
                removed.append(f)
            else:
                kept.append(f)

        # check they are all in the right place
        files = glob("Subtract/job016/**/*", recursive=True)
        trash = glob("Trash/**/**/*", recursive=True)
        for f in kept:
            assert f in files, f
        for f in removed:
            assert f not in files, f
            assert "Trash/" + f in trash, f

    def test_cleanup_postprocess(self):
        procname = "PostProcess"
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        outfiles = ShortpipeFileStructure([procname]).outfiles

        files = glob("PostProcess/job017/*")

        assert len(files) == len(outfiles[procname]), (
            len(files),
            len(outfiles[procname]),
        )

        del_files = glob("PostProcess/job017/*masked.mrc")
        # do the cleanup
        pipeline_data = ProjectGraph(name="short_full")
        pipeline_data.read()
        pipeline_data.clean_up_job(pipeline_data.process_list[16], False)

        # sort the files
        removed, kept = [], []
        for f in outfiles[procname]:
            if f in del_files:
                removed.append(f)
            else:
                kept.append(f)

        # check they are all in the right place
        files = glob("PostProcess/job017/*", recursive=True)
        trash = glob("Trash/**/**/*", recursive=True)
        for f in kept:
            assert f in files, f
        for f in removed:
            assert f not in files, f
            assert "Trash/" + f in trash, f

    def test_cleanup_external(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        ShortpipeFileStructure(["External"])
        pre_files = glob("External/job018/*")
        pipeline_data = ProjectGraph(name="short_full")
        pipeline_data.read()
        pipeline_data.clean_up_job(pipeline_data.process_list[17], False)
        post_files = glob("External/job018/*")

        # make sure nothing changed
        assert pre_files == post_files

    def test_cleanup_localres(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        ShortpipeFileStructure(["LocalRes"])
        pre_files = glob("LocalRes/job019/*")
        pipeline_data = ProjectGraph(name="short_full")
        pipeline_data.read()
        pipeline_data.clean_up_job(pipeline_data.process_list[18], False)
        post_files = glob("LocalRes/job019/*")

        # make sure nothing changed
        assert pre_files == post_files

    @unittest.skipUnless(do_slow_tests, "Slow test - skip unless full test")
    def test_cleanup_alljobs(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        ShortpipeFileStructure(["all"])

        del_files = {
            "Import": [],
            "MotionCorr": [
                "/job002/Raw_data/*.com",
                "/job002/Raw_data/*.err",
                "/job002/Raw_data/*.out",
                "/job002/Raw_data/*.log",
            ],
            "CtfFind": [
                "/job003/gctf*.out",
                "/job003/gctf*.err",
                "/job003/Raw_data/*",
            ],
            "AutoPick": ["/job004/Raw_data/*.spi"],
            "Extract": ["/job005/Raw_data/*_extract.star"],
            "Class2D": ["/job006/run_it*"],
            "Select": [],
            "InitialModel": ["/job008/run_it*"],
            "Class3D": ["/job009/run_it*"],
            "Refine3D": ["/job010/run_it*"],
            "MultiBody": ["/job011/run_it*"],
            "CtfRefine": [
                "/job012/Raw_data/*_wAcc.mrc",
                "/job012/Raw_data/*_xyAcc_real.mrc",
                "/job012/Raw_data/*_xyAcc_imag.mrc",
            ],
            "MaskCreate": [],
            "Polish": [
                "/job014/Raw_data/*_FCC_cc.mrc",
                "/job014/Raw_data/*_FCC_w0.mrc",
                "/job014/Raw_data/*_FCC_w1.mrc",
            ],
            "JoinStar": [],
            "Subtract": [],
            "External": [],
            "PostProcess": ["/job017/*masked.mrc"],
            "LocalRes": [],
        }

        exclude_files = {
            "Import": [],
            "MotionCorr": [],
            "CtfFind": [],
            "AutoPick": [],
            "Extract": [],
            "Class2d": [
                "Class2D/job006/run_it025_data.star",
                "Class2D/job006/run_it025_sampling.star",
                "Class2D/job006/run_it025_model.star",
                "Class2D/job006/run_it025_optimiser.star",
                "Class2D/job006/run_it025_classes.mrcs",
            ],
            "Select": [],
            "InitialModel": [
                "InitialModel/job008/run_it150_data.star",
                "InitialModel/job008/run_it150_class001_data.star",
                "InitialModel/job008/run_it150_class002_data.star",
                "InitialModel/job008/run_it150_sampling.star",
                "InitialModel/job008/run_it150_model.star",
                "InitialModel/job008/run_it150_optimiser.star",
                "InitialModel/job008/run_it150_class001.mrc",
                "InitialModel/job008/run_it150_grad001.mrc",
                "InitialModel/job008/run_it150_class002.mrc",
                "InitialModel/job008/run_it150_grad002.mrc",
            ],
            "Class3D": [
                "Class3D/job009/run_it025_class001.mrc",
                "Class3D/job009/run_it025_class001_angdist.bild",
                "Class3D/job009/run_it025_class002.mrc",
                "Class3D/job009/run_it025_class002_angdist.bild",
                "Class3D/job009/run_it025_class003.mrc",
                "Class3D/job009/run_it025_class003_angdist.bild",
                "Class3D/job009/run_it025_sampling.star",
                "Class3D/job009/run_it025_data.star",
                "Class3D/job009/run_it025_model.star",
                "Class3D/job009/run_it025_optimiser.star",
            ],
            "Refine3D": [
                "Refine3D/job010/run_it016_half1_class001.mrc",
                "Refine3D/job010/run_it016_half1_class001_angdist.bild",
                "Refine3D/job010/run_it016_half1_model.star",
                "Refine3D/job010/run_it016_half2_class001.mrc",
                "Refine3D/job010/run_it016_half2_class001_angdist.bild",
                "Refine3D/job010/run_it016_half2_model.star",
                "Refine3D/job010/run_it016_sampling.star",
                "Refine3D/job010/run_it016_data.star",
                "Refine3D/job010/run_it016_optimiser.star",
            ],
            "MultiBody": [
                "MultiBody/job011/run_it012_data.star",
                "MultiBody/job011/run_it012_half1_body001_angdist.bild",
                "MultiBody/job011/run_it012_half1_body001.mrc",
                "MultiBody/job011/run_it012_half1_body002_angdist.bild",
                "MultiBody/job011/run_it012_half1_body002.mrc",
                "MultiBody/job011/run_it012_half1_model.star",
                "MultiBody/job011/run_it012_half2_body001_angdist.bild",
                "MultiBody/job011/run_it012_half2_body001.mrc",
                "MultiBody/job011/run_it012_half2_body002_angdist.bild",
                "MultiBody/job011/run_it012_half2_body002.mrc",
                "MultiBody/job011/run_it012_half2_model.star",
            ],
            "CtfRefine": [],
            "MaskCreate": [],
            "Polish": [],
            "JoinStar": [],
            "Subtract": [],
            "External": [],
            "LocalRes": [],
        }

        files = list()
        for search_string in del_files:
            files += glob("{}/*/**/*".format(search_string), recursive=True)

        delete_list = list()
        for f in del_files:
            for search_string in del_files[f]:
                if len(search_string) > 1:
                    add = glob(f + search_string)
                    delete_list += add

        for f in exclude_files:
            for ff in exclude_files[f]:
                delete_list.remove(ff)

        pipeline_data = ProjectGraph(name="short_full")
        pipeline_data.read()
        pipeline_data.cleanup_all_jobs(False)

        # sort the files
        dirnames = [
            "Import/job001",
            "MotionCorr/job002",
            "MotionCorr/job002/Raw_data",
            "CtfFind/job003",
            "CtfFind/job003/Raw_data",
            "AutoPick/job004/Raw_data",
            "Extract/job005/Raw_data",
            "Class2D/job006",
            "Select/job007",
            "InitialModel/job008",
            "Class3D/job009",
            "Refine3D/job010",
            "MultiBody/job011",
            "CtfRefine/job012/Raw_data",
            "MaskCreate/job013",
            "Polish/job014/Raw_data",
            "JoinStar/job015",
            "Subtract/job016",
            "External/job017",
            "PostProcess/job018",
            "LocalRes/job019",
        ]
        removed, kept = [], []
        for f in files:
            if f in delete_list:
                removed.append(f)
            else:
                if f not in dirnames:
                    kept.append(f)

        # check they are all in the right place
        files2 = list()
        for search_string in del_files:
            files2 += glob("{}/*/**/*".format(search_string), recursive=True)
        trash = glob("Trash/*/**/*", recursive=True)
        for f in kept:
            assert f in files2, f
        for f in removed:
            assert f not in files2, f
            assert "Trash/" + f in trash, "Trash/" + f

    @unittest.skipUnless(do_slow_tests, "Slow test - skip unless full test")
    def test_cleanup_alljobs_harsh(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        ShortpipeFileStructure(["all"])

        del_files = {
            "Import": [],
            "MotionCorr": ["/job002/Raw_data/*"],
            "CtfFind": [
                "/job003/gctf*.out",
                "/job003/gctf*.err",
                "/job003/Raw_data/*",
            ],
            "AutoPick": ["/job004/Raw_data/*.spi"],
            "Extract": ["/job005/Raw_data/*"],
            "Class2D": ["/job006/run_it*"],
            "Select": [],
            "InitialModel": ["/job008/run_it*"],
            "Class3D": ["/job009/run_it*"],
            "Refine3D": ["/job010/run_it*"],
            "MultiBody": ["/job011/run_it*", "/job011/analyse_component*_bin*.mrc"],
            "CtfRefine": [
                "/job012/Raw_data/*_wAcc.mrc",
                "/job012/Raw_data/*_xyAcc_real.mrc",
                "/job012/Raw_data/*_xyAcc_imag.mrc",
            ],
            "MaskCreate": [],
            "Polish": [
                "/job014/Raw_data/*_FCC_cc.mrc",
                "/job014/Raw_data/*_FCC_w0.mrc",
                "/job014/Raw_data/*_FCC_w1.mrc",
                "/job014/Raw_data/*shiny.star",
                "/job014/Raw_data/*shiny.mrcs",
            ],
            "JoinStar": [],
            "Subtract": ["/job016/subtracted_*"],
            "External": [],
            "PostProcess": ["/job017/*masked.mrc"],
            "LocalRes": [],
        }

        exclude_files = {
            "Import": [],
            "MotionCorr": [],
            "CtfFind": [],
            "AutoPick": [],
            "Extract": [],
            "Class2d": [
                "Class2D/job006/run_it025_data.star",
                "Class2D/job006/run_it025_sampling.star",
                "Class2D/job006/run_it025_model.star",
                "Class2D/job006/run_it025_optimiser.star",
                "Class2D/job006/run_it025_classes.mrcs",
            ],
            "Select": [],
            "InitialModel": [
                "InitialModel/job008/run_it150_data.star",
                "InitialModel/job008/run_it150_class001_data.star",
                "InitialModel/job008/run_it150_class002_data.star",
                "InitialModel/job008/run_it150_sampling.star",
                "InitialModel/job008/run_it150_model.star",
                "InitialModel/job008/run_it150_optimiser.star",
                "InitialModel/job008/run_it150_class001.mrc",
                "InitialModel/job008/run_it150_grad001.mrc",
                "InitialModel/job008/run_it150_class002.mrc",
                "InitialModel/job008/run_it150_grad002.mrc",
            ],
            "Class3D": [
                "Class3D/job009/run_it025_class001.mrc",
                "Class3D/job009/run_it025_class001_angdist.bild",
                "Class3D/job009/run_it025_class002.mrc",
                "Class3D/job009/run_it025_class002_angdist.bild",
                "Class3D/job009/run_it025_class003.mrc",
                "Class3D/job009/run_it025_class003_angdist.bild",
                "Class3D/job009/run_it025_sampling.star",
                "Class3D/job009/run_it025_data.star",
                "Class3D/job009/run_it025_model.star",
                "Class3D/job009/run_it025_optimiser.star",
            ],
            "Refine3D": [
                "Refine3D/job010/run_it016_half1_class001.mrc",
                "Refine3D/job010/run_it016_half1_class001_angdist.bild",
                "Refine3D/job010/run_it016_half1_model.star",
                "Refine3D/job010/run_it016_half2_class001.mrc",
                "Refine3D/job010/run_it016_half2_class001_angdist.bild",
                "Refine3D/job010/run_it016_half2_model.star",
                "Refine3D/job010/run_it016_sampling.star",
                "Refine3D/job010/run_it016_data.star",
                "Refine3D/job010/run_it016_optimiser.star",
            ],
            "MultiBody": [
                "MultiBody/job011/run_it012_data.star",
                "MultiBody/job011/run_it012_half1_body001_angdist.bild",
                "MultiBody/job011/run_it012_half1_body001.mrc",
                "MultiBody/job011/run_it012_half1_body002_angdist.bild",
                "MultiBody/job011/run_it012_half1_body002.mrc",
                "MultiBody/job011/run_it012_half1_model.star",
                "MultiBody/job011/run_it012_half2_body001_angdist.bild",
                "MultiBody/job011/run_it012_half2_body001.mrc",
                "MultiBody/job011/run_it012_half2_body002_angdist.bild",
                "MultiBody/job011/run_it012_half2_body002.mrc",
                "MultiBody/job011/run_it012_half2_model.star",
            ],
            "CtfRefine": [],
            "MaskCreate": [],
            "Polish": [],
            "JoinStar": [],
            "Subtract": [],
            "External": [],
            "LocalRes": [],
        }

        files = list()
        for search_string in del_files:
            files += glob("{}/*/**/*".format(search_string), recursive=True)

        delete_list = list()
        for f in del_files:
            for search_string in del_files[f]:
                if len(search_string) > 1:
                    add = glob(f + search_string)
                    delete_list += add

        for f in exclude_files:
            for ff in exclude_files[f]:
                delete_list.remove(ff)

        pipeline_data = ProjectGraph(name="short_full")
        pipeline_data.read()
        pipeline_data.cleanup_all_jobs(True)

        # sort the files
        dirnames = [
            "Import/job001",
            "MotionCorr/job002",
            "MotionCorr/job002/Raw_data",
            "CtfFind/job003",
            "CtfFind/job003/Raw_data",
            "AutoPick/job004/Raw_data",
            "Extract/job005/Raw_data",
            "Class2D/job006",
            "Select/job007",
            "InitialModel/job008",
            "Class3D/job009",
            "Refine3D/job010",
            "MultiBody/job011",
            "CtfRefine/job012/Raw_data",
            "MaskCreate/job013",
            "Polish/job014/Raw_data",
            "JoinStar/job015",
            "Subtract/job016",
            "External/job017",
            "PostProcess/job018",
            "LocalRes/job019",
        ]
        removed, kept = [], []
        for f in files:
            if f in delete_list:
                removed.append(f)
            else:
                if f not in dirnames:
                    kept.append(f)

        # check they are all in the right place
        files2 = list()
        for search_string in del_files:
            files2 += glob("{}/*/**/*".format(search_string), recursive=True)
        trash = glob("Trash/*/**/*", recursive=True)
        for f in kept:
            assert f in files2, f
        for f in removed:
            assert f not in files2, f
            assert "Trash/" + f in trash, "Trash/" + f

    @unittest.skipUnless(do_slow_tests, "Slow test - skip unless full test")
    def test_cleanup_alljobs_harsh_exclude_dir(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            self.test_dir,
        )
        ShortpipeFileStructure(["all"])
        # exclude the polish directory from the harsh clean
        touch("Polish/job014/NO_HARSH_CLEAN")
        del_files = {
            "Import": [],
            "MotionCorr": ["/job002/Raw_data/*"],
            "CtfFind": [
                "/job003/gctf*.out",
                "/job003/gctf*.err",
                "/job003/Raw_data/*",
            ],
            "AutoPick": ["/job004/Raw_data/*.spi"],
            "Extract": ["/job005/Raw_data/*"],
            "Class2D": ["/job006/run_it*"],
            "Select": [],
            "InitialModel": ["/job008/run_it*"],
            "Class3D": ["/job009/run_it*"],
            "Refine3D": ["/job010/run_it*"],
            "MultiBody": ["/job011/run_it*", "/job011/analyse_component*_bin*.mrc"],
            "CtfRefine": [
                "/job012/Raw_data/*_wAcc.mrc",
                "/job012/Raw_data/*_xyAcc_real.mrc",
                "/job012/Raw_data/*_xyAcc_imag.mrc",
            ],
            "MaskCreate": [],
            "Polish": [],
            "JoinStar": [],
            "Subtract": ["/job016/subtracted_*"],
            "External": [],
            "PostProcess": ["/job017/*masked.mrc"],
            "LocalRes": [],
        }

        exclude_files = {
            "Import": [],
            "MotionCorr": [],
            "CtfFind": [],
            "AutoPick": [],
            "Extract": [],
            "Class2d": [
                "Class2D/job006/run_it025_data.star",
                "Class2D/job006/run_it025_sampling.star",
                "Class2D/job006/run_it025_model.star",
                "Class2D/job006/run_it025_optimiser.star",
                "Class2D/job006/run_it025_classes.mrcs",
            ],
            "Select": [],
            "InitialModel": [
                "InitialModel/job008/run_it150_data.star",
                "InitialModel/job008/run_it150_class001_data.star",
                "InitialModel/job008/run_it150_class002_data.star",
                "InitialModel/job008/run_it150_sampling.star",
                "InitialModel/job008/run_it150_model.star",
                "InitialModel/job008/run_it150_optimiser.star",
                "InitialModel/job008/run_it150_class001.mrc",
                "InitialModel/job008/run_it150_grad001.mrc",
                "InitialModel/job008/run_it150_class002.mrc",
                "InitialModel/job008/run_it150_grad002.mrc",
            ],
            "Class3D": [
                "Class3D/job009/run_it025_class001.mrc",
                "Class3D/job009/run_it025_class001_angdist.bild",
                "Class3D/job009/run_it025_class002.mrc",
                "Class3D/job009/run_it025_class002_angdist.bild",
                "Class3D/job009/run_it025_class003.mrc",
                "Class3D/job009/run_it025_class003_angdist.bild",
                "Class3D/job009/run_it025_sampling.star",
                "Class3D/job009/run_it025_data.star",
                "Class3D/job009/run_it025_model.star",
                "Class3D/job009/run_it025_optimiser.star",
            ],
            "Refine3D": [
                "Refine3D/job010/run_it016_half1_class001.mrc",
                "Refine3D/job010/run_it016_half1_class001_angdist.bild",
                "Refine3D/job010/run_it016_half1_model.star",
                "Refine3D/job010/run_it016_half2_class001.mrc",
                "Refine3D/job010/run_it016_half2_class001_angdist.bild",
                "Refine3D/job010/run_it016_half2_model.star",
                "Refine3D/job010/run_it016_sampling.star",
                "Refine3D/job010/run_it016_data.star",
                "Refine3D/job010/run_it016_optimiser.star",
            ],
            "MultiBody": [
                "MultiBody/job011/run_it012_data.star",
                "MultiBody/job011/run_it012_half1_body001_angdist.bild",
                "MultiBody/job011/run_it012_half1_body001.mrc",
                "MultiBody/job011/run_it012_half1_body002_angdist.bild",
                "MultiBody/job011/run_it012_half1_body002.mrc",
                "MultiBody/job011/run_it012_half1_model.star",
                "MultiBody/job011/run_it012_half2_body001_angdist.bild",
                "MultiBody/job011/run_it012_half2_body001.mrc",
                "MultiBody/job011/run_it012_half2_body002_angdist.bild",
                "MultiBody/job011/run_it012_half2_body002.mrc",
                "MultiBody/job011/run_it012_half2_model.star",
            ],
            "CtfRefine": [],
            "MaskCreate": [],
            "Polish": [],
            "JoinStar": [],
            "Subtract": [],
            "External": [],
            "LocalRes": [],
        }

        files = list()
        for search_string in del_files:
            files += glob("{}/*/**/*".format(search_string), recursive=True)

        delete_list = list()
        for f in del_files:
            for search_string in del_files[f]:
                if len(search_string) > 1:
                    add = glob(f + search_string)
                    delete_list += add

        for f in exclude_files:
            for ff in exclude_files[f]:
                delete_list.remove(ff)

        pipeline_data = ProjectGraph(name="short_full")
        pipeline_data.read()
        pipeline_data.cleanup_all_jobs(True)

        # sort the files
        dirnames = [
            "Import/job001",
            "MotionCorr/job002",
            "MotionCorr/job002/Raw_data",
            "CtfFind/job003",
            "CtfFind/job003/Raw_data",
            "AutoPick/job004/Raw_data",
            "Extract/job005/Raw_data",
            "Class2D/job006",
            "Select/job007",
            "InitialModel/job008",
            "Class3D/job009",
            "Refine3D/job010",
            "MultiBody/job011",
            "CtfRefine/job012/Raw_data",
            "MaskCreate/job013",
            "Polish/job014/Raw_data",
            "JoinStar/job015",
            "Subtract/job016",
            "External/job017",
            "PostProcess/job018",
            "LocalRes/job019",
        ]
        removed, kept = [], []
        for f in files:
            if f in delete_list:
                removed.append(f)
            else:
                if f not in dirnames:
                    kept.append(f)

        # check they are all in the right place
        files2 = list()
        for search_string in del_files:
            files2 += glob("{}/*/**/*".format(search_string), recursive=True)
        trash = glob("Trash/*/**/*", recursive=True)
        for f in kept:
            assert f in files2, f
        for f in removed:
            assert f not in files2, f
            assert "Trash/" + f in trash, "Trash/" + f

    def test_job_has_no_cleanup_function(self):
        pipeline = os.path.join(self.test_data, "Pipelines/plugin_pipeline.star")
        shutil.copy(pipeline, self.test_dir)
        the_pipe = ProjectGraph("plugin")
        the_pipe.read()
        job_dir = os.path.join(self.test_dir, "CryoEF/job004")
        job_star = os.path.join(self.test_data, "JobFiles/CryoEF/cryoef_star_job.star")
        os.makedirs(job_dir)
        shutil.copy(job_star, os.path.join(job_dir, "job.star"))
        the_pipe.clean_up_job(the_pipe.process_list[1], do_harsh=False)


if __name__ == "__main__":
    unittest.main()
