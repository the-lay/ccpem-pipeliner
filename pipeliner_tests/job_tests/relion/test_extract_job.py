#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import json

from pipeliner_tests import test_data
from pipeliner_tests.generic_tests import (
    general_get_command_test,
    tutorial_data_available,
    get_relion_tutorial_data,
)
from pipeliner.project_graph import ProjectGraph
from pipeliner.nodes import (
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_PARTICLESDATA,
    NODE_MICROGRAPHCOORDSGROUP,
)


class ExtractTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_basic(self):
        general_get_command_test(
            jobtype="Extract",
            jobfile="extract_basic.job",
            jobnumber=7,
            input_nodes={
                "Select/job005/micrographs_selected.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
                "AutoPick/job006/autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star."
                "relion",
            },
            output_nodes={"particles.star": f"{NODE_PARTICLESDATA}.star.relion"},
            expected_commands=[
                "relion_preprocess --i "
                "Select/job005/micrographs_selected.star "
                "--coord_list AutoPick/job006/autopick.star "
                "--part_star Extract/job007/particles.star --part_dir "
                "Extract/job007/ --extract --extract_size 256 --scale 64 "
                "--norm --bg_radius 25 --white_dust -1 --black_dust -1 "
                "--invert_contrast --float16 --pipeline_control Extract/job007/"
            ],
        )

    def test_get_command_jobstar(self):
        general_get_command_test(
            jobtype="Extract",
            jobfile="extract_job.star",
            jobnumber=7,
            input_nodes={
                "Select/job005/micrographs_selected.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
                "AutoPick/job006/autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}."
                "star.relion",
            },
            output_nodes={"particles.star": f"{NODE_PARTICLESDATA}.star.relion"},
            expected_commands=[
                "relion_preprocess --i "
                "Select/job005/micrographs_selected.star "
                "--coord_list AutoPick/job006/autopick.star "
                "--part_star Extract/job007/particles.star --part_dir "
                "Extract/job007/ --extract --extract_size 256 --scale 64 "
                "--norm --bg_radius 24 --white_dust -1 --black_dust -1 "
                "--invert_contrast --pipeline_control Extract/job007/"
            ],
        )

    def test_get_command_FOM(self):
        """Extract using the selection based on FOM added in relion 4.0"""
        general_get_command_test(
            jobtype="Extract",
            jobfile="extract_onFOM_job.star",
            jobnumber=7,
            input_nodes={
                "Select/job005/micrographs_selected.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star."
                "relion",
                "AutoPick/job006/autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star."
                "relion",
            },
            output_nodes={"particles.star": f"{NODE_PARTICLESDATA}.star.relion"},
            expected_commands=[
                "relion_preprocess --i "
                "Select/job005/micrographs_selected.star "
                "--coord_list AutoPick/job006/autopick.star "
                "--part_star Extract/job007/particles.star --part_dir "
                "Extract/job007/ --extract --extract_size 256 --scale 64 "
                "--norm --bg_radius 24 --white_dust -1 --black_dust -1 "
                "--invert_contrast --minimum_pick_fom 0.1 --pipeline_control"
                " Extract/job007/"
            ],
        )

    def test_get_command_rextract_wrongfile(self):
        with self.assertRaises(ValueError):
            general_get_command_test(
                jobtype="Extract",
                jobfile="extract_rextract_wrongfile.job",
                jobnumber=7,
                input_nodes=2,
                output_nodes=1,
                expected_commands="",
            )

    def test_get_command_rextract(self):
        general_get_command_test(
            jobtype="Extract",
            jobfile="extract_rextract.job",
            jobnumber=7,
            input_nodes={
                "Select/job005/micrographs_selected.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
                "Refine3D/job200/run_data.star": f"{NODE_PARTICLESDATA}.star.relion",
            },
            output_nodes={
                "particles.star": f"{NODE_PARTICLESDATA}.star.relion",
                "reextract.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion",
            },
            expected_commands=[
                "mpirun -n 16 relion_preprocess_mpi --i "
                "Select/job005/micrographs_selected.star "
                "--reextract_data_star Refine3D/job200/run_data.star "
                "--part_star Extract/job007/particles.star --part_dir Extract/job007/"
                " --extract --extract_size 256 --scale 64 --norm --bg_radius 25"
                " --white_dust -1 --black_dust -1 --invert_contrast "
                "--rextract_on_this_one --pipeline_control Extract/job007/",
            ],
        )

    def test_get_command_rextract_recenter(self):
        general_get_command_test(
            jobtype="Extract",
            jobfile="extract_rextract_recenter.job",
            jobnumber=7,
            input_nodes={
                "Select/job005/micrographs_selected.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
                "Refine3D/job200/run_data.star": f"{NODE_PARTICLESDATA}.star.relion",
            },
            output_nodes={
                "particles.star": f"{NODE_PARTICLESDATA}.star.relion",
                "reextract.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion",
            },
            expected_commands=[
                "relion_preprocess --i "
                "Select/job005/micrographs_selected.star "
                "--reextract_data_star Refine3D/job200/run_data.star "
                "--recenter --recenter_x 10 --recenter_y 20 --recenter_z 30 "
                "--part_star Extract/job007/particles.star --part_dir Extract/job007/"
                " --extract --extract_size 256 --norm --bg_radius 125"
                " --white_dust -1 --black_dust -1 --invert_contrast"
                " --pipeline_control Extract/job007/",
            ],
        )

    def test_get_command_rextract_setbgdia(self):
        general_get_command_test(
            jobtype="Extract",
            jobfile="extract_rextract_set_bgdia.job",
            jobnumber=7,
            input_nodes={
                "Select/job005/micrographs_selected.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
                "Refine3D/job200/run_data.star": f"{NODE_PARTICLESDATA}.star.relion",
            },
            output_nodes={
                "particles.star": f"{NODE_PARTICLESDATA}.star.relion",
                "reextract.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion",
            },
            expected_commands=[
                "relion_preprocess --i "
                "Select/job005/micrographs_selected.star "
                "--reextract_data_star Refine3D/job200/run_data.star "
                "--part_star Extract/job007/particles.star --part_dir Extract/job007/"
                " --extract --extract_size 256 --scale 64 --norm --bg_radius 24"
                " --white_dust -1 --black_dust -1 --invert_contrast --pipeline_control"
                " Extract/job007/",
            ],
        )

    def test_get_command_helical(self):
        general_get_command_test(
            jobtype="Extract",
            jobfile="extract_helical.job",
            jobnumber=7,
            input_nodes={
                "Select/job005/micrographs_selected.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
                "AutoPick/job006/autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star."
                "relion."
                "helixstartend",
            },
            output_nodes={
                "particles.star": f"{NODE_PARTICLESDATA}.star.relion.helicalsegments",
                "helix_segments.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion."
                "helicalsegments",
            },
            expected_commands=[
                "relion_preprocess --i "
                "Select/job005/micrographs_selected.star "
                "--coord_list AutoPick/job006/autopick.star "
                "--part_star Extract/job007/particles.star --part_dir "
                "Extract/job007/ --extract --extract_size 256 --scale 64 "
                "--norm --bg_radius 25 --white_dust -1 --black_dust -1 "
                "--invert_contrast --helix --helical_outer_diameter 200"
                " --helical_bimodal_angular_priors --helical_tubes "
                "--helical_cut_into_segments --helical_nr_asu 3 "
                "--helical_rise 2.34 --pipeline_control Extract/job007/"
            ],
        )

    def test_get_command_helical_relionstyle_name(self):
        """Test automatic conversion of ambiguous relion style name"""
        general_get_command_test(
            jobtype="Extract",
            jobfile="extract_helical_relionstyle.job",
            jobnumber=7,
            input_nodes={
                "Select/job005/micrographs_selected.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
                "AutoPick/job006/autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star."
                "relion."
                "helixstartend",
            },
            output_nodes={
                "particles.star": f"{NODE_PARTICLESDATA}.star.relion.helicalsegments",
                "helix_segments.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion."
                "helicalsegments",
            },
            expected_commands=[
                "relion_preprocess --i "
                "Select/job005/micrographs_selected.star "
                "--coord_list AutoPick/job006/autopick.star "
                "--part_star Extract/job007/particles.star --part_dir "
                "Extract/job007/ --extract --extract_size 256 --scale 64 "
                "--norm --bg_radius 25 --white_dust -1 --black_dust -1 "
                "--invert_contrast --helix --helical_outer_diameter 200"
                " --helical_bimodal_angular_priors --helical_tubes "
                "--helical_cut_into_segments --helical_nr_asu 3 "
                "--helical_rise 2.34 --pipeline_control Extract/job007/"
            ],
        )

    def test_get_command_helical_nosegs(self):
        general_get_command_test(
            jobtype="Extract",
            jobfile="extract_helical_nosegs.job",
            jobnumber=7,
            input_nodes={
                "Select/job005/micrographs_selected.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
                "AutoPick/job006/autopick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star."
                "relion.helixstartend",
            },
            output_nodes={
                "particles.star": f"{NODE_PARTICLESDATA}.star.relion.helicalsegments"
            },
            expected_commands=[
                "relion_preprocess --i "
                "Select/job005/micrographs_selected.star "
                "--coord_list AutoPick/job006/autopick.star "
                "--part_star Extract/job007/particles.star --part_dir "
                "Extract/job007/ --extract --extract_size 256 --scale 64 "
                "--norm --bg_radius 25 --white_dust -1 --black_dust -1 "
                "--invert_contrast --float16 --helix --helical_outer_diameter 200"
                " --helical_bimodal_angular_priors --pipeline_control "
                "Extract/job007/"
            ],
        )

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_extract_generate_display_data(self):
        get_relion_tutorial_data(["Extract"])
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("Extract/job007/")
        dispobjs = pipeline.get_process_results_display(proc)

        expres = os.path.join(self.test_data, "ResultsFiles/extract_results.json")
        print(dispobjs[0].__dict__)
        with open(expres, "r") as er:
            expected = json.load(er)

        assert dispobjs[0].__dict__ == expected

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_reextract_generate_display_data(self):
        get_relion_tutorial_data(["Extract"])
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("Extract/job018/")
        dispobjs = pipeline.get_process_results_display(proc)

        expres = os.path.join(self.test_data, "ResultsFiles/reextract_results.json")
        with open(expres, "r") as er:
            expected = json.load(er)
        assert dispobjs[0].__dict__ == expected


if __name__ == "__main__":
    unittest.main()
