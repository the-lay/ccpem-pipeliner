#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
import shutil
import tempfile
import unittest
from unittest.mock import patch

from pipeliner import job_factory
from pipeliner_tests import test_data
from pipeliner_tests import generic_tests


class MultiBodyTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")
        shutil.copy(
            os.path.join(self.test_data, "JobFiles/MultiBody/bodyfile.star"),
            self.test_dir,
        )
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @patch.dict(os.environ)
    def test_get_scratch_env_var(self):
        # Ensure RELION_SCRATCH_DIR is unset to begin with
        os.environ.pop("RELION_SCRATCH_DIR", None)

        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/MultiBody/multibody.job")
        )
        assert job.joboptions["scratch_dir"].default_value == ""

        os.environ["RELION_SCRATCH_DIR"] = os.path.join(
            self.test_data, "fake_SCRATCH_dir"
        )
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/MultiBody/multibody.job")
        )
        assert job.joboptions["scratch_dir"].default_value == os.path.join(
            self.test_data, "fake_SCRATCH_dir"
        )

    def test_get_command_multibody_basic(self):
        generic_tests.general_get_command_test(
            jobtype="MultiBody",
            jobfile="multibody.job",
            jobnumber=12,
            input_nodes={
                "Refine3D/job400/run_it025_optimiser.star": "ProcessData.star.relion."
                "optimiser.refine3d",
                "bodyfile.star": "ProcessData.star.relion.body_definitions",
            },
            output_nodes={
                "run_half1_body001_unfil.mrc": "DensityMap.mrc.relion.halfmap."
                "multibody",
                "run_half1_body002_unfil.mrc": "DensityMap.mrc.relion.halfmap."
                "multibody",
                "run_half1_body003_unfil.mrc": "DensityMap.mrc.relion.halfmap."
                "multibody",
            },
            expected_commands=[
                "mpirun -n 16 relion_refine_mpi "
                "--continue Refine3D/job400/run_it025_optimiser.star "
                "--o MultiBody/job012/run --solvent_correct_fsc "
                "--multibody_masks bodyfile.star --oversampling 1 "
                "--healpix_order 4 --auto_local_healpix_order 4 "
                "--offset_range 3 --offset_step 1.5 --j 8 --gpu 1:2"
                " --dont_combine_weights_via_disc"
                " --pool 3 --pad 2 --skip_gridding --pipeline_control "
                "MultiBody/job012/"
            ],
        )

    def test_get_command_multibody_basic_relion_style_jobname(self):
        """Make sure ambiguous relion style job name is convereted"""
        generic_tests.general_get_command_test(
            jobtype="MultiBody",
            jobfile="multibody_relionstyle_job.star",
            jobnumber=12,
            input_nodes={
                "Refine3D/job400/run_it025_optimiser.star": "ProcessData.star.relion."
                "optimiser.refine3d",
                "bodyfile.star": "ProcessData.star.relion.body_definitions",
            },
            output_nodes={
                "run_half1_body001_unfil.mrc": "DensityMap.mrc.relion.halfmap."
                "multibody",
                "run_half1_body002_unfil.mrc": "DensityMap.mrc.relion.halfmap."
                "multibody",
                "run_half1_body003_unfil.mrc": "DensityMap.mrc.relion.halfmap."
                "multibody",
            },
            expected_commands=[
                "mpirun -n 16 relion_refine_mpi "
                "--continue Refine3D/job400/run_it025_optimiser.star "
                "--o MultiBody/job012/run --solvent_correct_fsc "
                "--multibody_masks bodyfile.star --oversampling 1 "
                "--healpix_order 4 --auto_local_healpix_order 4 "
                "--offset_range 3 --offset_step 1.5 --j 8 --gpu 1:2"
                " --dont_combine_weights_via_disc"
                " --pool 3 --pad 2 --skip_gridding --pipeline_control "
                "MultiBody/job012/"
            ],
        )

    def test_get_command_multibody_flex_relion_style_jobname(self):
        """Make sure ambiguous relion style job name is convereted
        A warning shoulf be raised because the flexibility analysis will not
        be performed"""
        generic_tests.general_get_command_test(
            jobtype="MultiBody",
            jobfile="multibody_relionstyle_analysis_job.star",
            jobnumber=12,
            input_nodes={
                "Refine3D/job400/run_it025_optimiser.star": "ProcessData.star.relion."
                "optimiser.refine3d",
                "bodyfile.star": "ProcessData.star.relion.body_definitions",
            },
            output_nodes={
                "run_half1_body001_unfil.mrc": "DensityMap.mrc.relion.halfmap."
                "multibody",
                "run_half1_body002_unfil.mrc": "DensityMap.mrc.relion.halfmap."
                "multibody",
                "run_half1_body003_unfil.mrc": "DensityMap.mrc.relion.halfmap."
                "multibody",
            },
            expected_commands=[
                "mpirun -n 16 relion_refine_mpi "
                "--continue Refine3D/job400/run_it025_optimiser.star "
                "--o MultiBody/job012/run --solvent_correct_fsc "
                "--multibody_masks bodyfile.star --oversampling 1 "
                "--healpix_order 4 --auto_local_healpix_order 4 "
                "--offset_range 3 --offset_step 1.5 --j 8 --gpu 1:2"
                " --dont_combine_weights_via_disc"
                " --pool 3 --pad 2 --skip_gridding --pipeline_control "
                "MultiBody/job012/"
            ],
        )

    def test_get_command_multibody_jobstar(self):
        generic_tests.general_get_command_test(
            jobtype="MultiBody",
            jobfile="multibody_job.star",
            jobnumber=12,
            input_nodes={
                "Refine3D/job400/run_it025_optimiser.star": "ProcessData.star.relion."
                "optimiser.refine3d",
                "bodyfile.star": "ProcessData.star.relion.body_definitions",
            },
            output_nodes={
                "run_half1_body001_unfil.mrc": "DensityMap.mrc.relion.halfmap."
                "multibody",
                "run_half1_body002_unfil.mrc": "DensityMap.mrc.relion.halfmap."
                "multibody",
                "run_half1_body003_unfil.mrc": "DensityMap.mrc.relion.halfmap."
                "multibody",
            },
            expected_commands=[
                "mpirun -n 16 relion_refine_mpi "
                "--continue Refine3D/job400/run_it025_optimiser.star "
                "--o MultiBody/job012/run --solvent_correct_fsc "
                "--multibody_masks bodyfile.star --oversampling 1 "
                "--healpix_order 4 --auto_local_healpix_order 4 "
                "--offset_range 3 --offset_step 1.5 --j 8 --gpu 1:2"
                " --dont_combine_weights_via_disc"
                " --pool 3 --pad 2 --skip_gridding --pipeline_control "
                "MultiBody/job012/"
            ],
        )

    def test_get_command_multibody_reconstruct_subtracted(self):
        generic_tests.general_get_command_test(
            jobtype="MultiBody",
            jobfile="multibody_rec_sub.job",
            jobnumber=12,
            input_nodes={
                "Refine3D/job400/run_it025_optimiser.star": "ProcessData.star.relion."
                "optimiser.refine3d",
                "bodyfile.star": "ProcessData.star.relion.body_definitions",
            },
            output_nodes={
                "run_half1_body001_unfil.mrc": "DensityMap.mrc.relion.halfmap."
                "multibody",
                "run_half1_body002_unfil.mrc": "DensityMap.mrc.relion.halfmap."
                "multibody",
                "run_half1_body003_unfil.mrc": "DensityMap.mrc.relion.halfmap."
                "multibody",
            },
            expected_commands=[
                "mpirun -n 16 relion_refine_mpi "
                "--continue Refine3D/job400/run_it025_optimiser.star "
                "--o MultiBody/job012/run --solvent_correct_fsc "
                "--multibody_masks bodyfile.star --oversampling 1 "
                "--healpix_order 4 --auto_local_healpix_order 4 "
                "--offset_range 3 --offset_step 1.5 --reconstruct_subtracted_bodies"
                " --j 8 --gpu 1:2 --dont_combine_weights_via_disc"
                " --pool 3 --pad 2 --skip_gridding --pipeline_control "
                "MultiBody/job012/"
            ],
        )

    def test_get_command_multibody_no_flexanalysis(self):
        generic_tests.general_get_command_test(
            jobtype="MultiBody",
            jobfile="multibody_noflex.job",
            jobnumber=12,
            input_nodes={
                "Refine3D/job400/run_it025_optimiser.star": "ProcessData.star.relion."
                "optimiser.refine3d",
                "bodyfile.star": "ProcessData.star.relion.body_definitions",
            },
            output_nodes={
                "run_half1_body001_unfil.mrc": "DensityMap.mrc.relion.halfmap."
                "multibody",
                "run_half1_body002_unfil.mrc": "DensityMap.mrc.relion.halfmap."
                "multibody",
                "run_half1_body003_unfil.mrc": "DensityMap.mrc.relion.halfmap."
                "multibody",
            },
            expected_commands=[
                "mpirun -n 16 relion_refine_mpi "
                "--continue Refine3D/job400/run_it025_optimiser.star "
                "--o MultiBody/job012/run --solvent_correct_fsc "
                "--multibody_masks bodyfile.star --oversampling 1 "
                "--healpix_order 4 --auto_local_healpix_order 4 "
                "--offset_range 3 --offset_step 1.5 --reconstruct_subtracted_bodies"
                " --j 8 --gpu 1:2 --dont_combine_weights_via_disc"
                " --pool 3 --pad 2 --skip_gridding --pipeline_control "
                "MultiBody/job012/"
            ],
        )

    def test_get_command_multibody_separate_oneigen(self):
        generic_tests.general_get_command_test(
            jobtype="MultiBody",
            jobfile="multibody_sep_eigen.job",
            jobnumber=13,
            input_nodes={
                "MultiBody/job012/run_model.star": "ProcessData.star.relion.optimiser"
                ".multibody",
                "bodyfile.star": "ProcessData.star.relion.body_definitions",
            },
            output_nodes={
                "analyse_eval1_select_min-15_max10.star": "ParticlesData.star.relion"
                ".flexanalysis.eigenselected",
                "analyse_logfile.pdf": "LogFile.pdf.relion.flexanalysis",
            },
            expected_commands=[
                "relion_flex_analyse --PCA_orient --model"
                " MultiBody/job012/run_model.star --data MultiBody/job012/run_data"
                ".star --bodies bodyfile.star --o MultiBody/job013/analyse --do_maps"
                " --k 3 --select_eigenvalue 1 --select_eigenvalue_min -15.0"
                " --select_eigenvalue_max 10.0 --write_pca_projections "
                "--pipeline_control MultiBody/job013/"
            ],
        )

    def test_get_command_multibody_separate_oneigen_small(self):
        generic_tests.general_get_command_test(
            jobtype="MultiBody",
            jobfile="multibody_sep_eigen_small.job",
            jobnumber=13,
            input_nodes={
                "MultiBody/job012/run_model.star": "ProcessData.star.relion.optimiser"
                ".multibody",
                "bodyfile.star": "ProcessData.star.relion.body_definitions",
            },
            output_nodes={
                "analyse_eval1_select_min-0p25_max0p5.star": "ParticlesData.star."
                "relion.flexanalysis.eigenselected",
                "analyse_logfile.pdf": "LogFile.pdf.relion.flexanalysis",
            },
            expected_commands=[
                "relion_flex_analyse --PCA_orient --model"
                " MultiBody/job012/run_model.star --data MultiBody/job012/run_data.star"
                " --bodies bodyfile.star --o MultiBody/job013/analyse --do_maps --k 3"
                " --select_eigenvalue 1 --select_eigenvalue_min -0.25"
                " --select_eigenvalue_max 0.5 --write_pca_projections "
                "--pipeline_control MultiBody/job013/"
            ],
        )

    def test_get_command_multibody_different_sampling(self):
        generic_tests.general_get_command_test(
            jobtype="MultiBody",
            jobfile="multibody_diff_samp.job",
            jobnumber=12,
            input_nodes={
                "Refine3D/job400/run_it025_optimiser.star": "ProcessData.star.relion."
                "optimiser.refine3d",
                "bodyfile.star": "ProcessData.star.relion.body_definitions",
            },
            output_nodes={
                "run_half1_body001_unfil.mrc": "DensityMap.mrc.relion.halfmap."
                "multibody",
                "run_half1_body002_unfil.mrc": "DensityMap.mrc.relion.halfmap."
                "multibody",
                "run_half1_body003_unfil.mrc": "DensityMap.mrc.relion.halfmap."
                "multibody",
            },
            expected_commands=[
                "mpirun -n 16 relion_refine_mpi "
                "--continue Refine3D/job400/run_it025_optimiser.star "
                "--o MultiBody/job012/run --solvent_correct_fsc "
                "--multibody_masks bodyfile.star --oversampling 1 "
                "--healpix_order 2 --auto_local_healpix_order 2 "
                "--offset_range 3 --offset_step 1.5 --j 8 --gpu 1:2"
                " --dont_combine_weights_via_disc"
                " --pool 3 --pad 2 --skip_gridding --pipeline_control "
                "MultiBody/job012/"
            ],
        )

    def test_get_command_multibody_noMPI(self):
        generic_tests.general_get_command_test(
            jobtype="MultiBody",
            jobfile="multibody_noMPI.job",
            jobnumber=12,
            input_nodes={
                "Refine3D/job400/run_it025_optimiser.star": "ProcessData.star.relion."
                "optimiser.refine3d",
                "bodyfile.star": "ProcessData.star.relion.body_definitions",
            },
            output_nodes={
                "run_half1_body001_unfil.mrc": "DensityMap.mrc.relion.halfmap."
                "multibody",
                "run_half1_body002_unfil.mrc": "DensityMap.mrc.relion.halfmap."
                "multibody",
                "run_half1_body003_unfil.mrc": "DensityMap.mrc.relion.halfmap."
                "multibody",
            },
            expected_commands=[
                "relion_refine "
                "--continue Refine3D/job400/run_it025_optimiser.star "
                "--o MultiBody/job012/run --solvent_correct_fsc "
                "--multibody_masks bodyfile.star --oversampling 1 "
                "--healpix_order 4 --auto_local_healpix_order 4 "
                "--offset_range 3 --offset_step 1.5 --j 16 --gpu 1:2"
                " --dont_combine_weights_via_disc"
                " --pool 3 --pad 2 --skip_gridding --pipeline_control"
                " MultiBody/job012/"
            ],
        )


if __name__ == "__main__":
    unittest.main()
