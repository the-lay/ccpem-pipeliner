#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner.nodes import NODE_DENSITYMAP
from pipeliner_tests import test_data
from pipeliner_tests.job_testing_tools import (
    job_generate_commands_test,
    job_display_object_generation_test,
)
from pipeliner.results_display_objects import ResultsDisplayMapModel


class FlipHandednessJobTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)

        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_commands(self):
        job_generate_commands_test(
            jobfile=os.path.join(self.test_data, "JobFiles/FlipMap/flip_map_job.star"),
            input_nodes={"test.mrc": NODE_DENSITYMAP + ".mrc"},
            output_nodes={"test_flipped.mrc": NODE_DENSITYMAP + ".mrc"},
            expected_commands=[
                "relion_image_handler --i test.mrc --invert_hand --o test_flipped.mrc "
                "--pipeline_control Flip/job999/"
            ],
        )

    def test_creating_results_display(self):
        mrc_file = os.path.join(self.test_data, "1ake_10A_molrep.mrc")
        expected_rdo = ResultsDisplayMapModel(
            title="Flipped map",
            start_collapsed=False,
            associated_data=["Flip/job999/test_flipped.mrc"],
            maps=["Flip/job999/Thumbnails/test_flipped.mrc"],
            maps_data="Flip/job999/test_flipped.mrc",
        )
        job_display_object_generation_test(
            jobfile=os.path.join(self.test_data, "JobFiles/FlipMap/flip_map_job.star"),
            expected_display_objects=[(expected_rdo, None)],
            files_to_create={"test_flipped.mrc": mrc_file},
        )


if __name__ == "__main__":
    unittest.main()
