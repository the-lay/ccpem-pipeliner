#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner import data_structure, job_factory
from pipeliner.pipeliner_job import ExternalProgram
from pipeliner.jobs.relion.relion_job import RelionJob, relion_program
from pipeliner_tests import test_data
from pipeliner_tests import generic_tests
from pipeliner.job_factory import new_job_of_type
from pipeliner.job_options import IntJobOption


class RelionJobTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_relion_job_cannot_be_instantiated(self):
        with self.assertRaises(NotImplementedError):
            RelionJob()

    def test_reading_import_mask_job(self):
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Import/import_mask.job")
        )
        assert job.PROCESS_NAME == data_structure.IMPORT_OTHER_NAME
        assert job.output_dir == ""
        assert len(job.input_nodes) == 0
        assert len(job.output_nodes) == 0
        assert len(job.joboptions) == 8
        assert job.joboptions["fn_in_other"].label == "Input file:"
        assert job.joboptions["fn_in_other"].value == "emd_3488_mask.mrc"
        assert job.joboptions["is_relion"].label == "Is the file a RELION STAR file?"
        assert job.joboptions["is_relion"].value == "No"
        assert job.joboptions["node_type"].label == "RELION STAR file type:"
        assert (
            job.joboptions["node_type"].value == "RELION coordinates STAR file (.star)"
        )
        assert (
            job.joboptions["optics_group_particles"].label
            == "Rename optics group for particles:"
        )
        assert job.joboptions["optics_group_particles"].value == ""
        assert (
            job.joboptions["pipeliner_node_type"].label
            == "Node type for imported file:"
        )
        assert job.joboptions["pipeliner_node_type"].value == "Mask3D"
        assert job.joboptions["alt_nodetype"].label == "OR: Create a new node type:"
        assert job.joboptions["alt_nodetype"].value == ""
        assert job.joboptions["kwds"].label == "Keywords"
        assert job.joboptions["kwds"].value == ""
        assert (
            job.joboptions["is_synthetic"].label
            == "Does the node contain synthetic data?"
        )
        assert job.joboptions["is_synthetic"].value == "No"

    def test_reading_import_map_job(self):
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Import/import_map.job")
        )
        assert job.PROCESS_NAME == data_structure.IMPORT_OTHER_NAME
        assert job.output_dir == ""
        assert len(job.input_nodes) == 0
        assert len(job.output_nodes) == 0
        assert len(job.joboptions) == 8
        assert job.joboptions["fn_in_other"].label == "Input file:"
        assert job.joboptions["fn_in_other"].value == "emd_3488.mrc"
        assert job.joboptions["is_relion"].label == "Is the file a RELION STAR file?"
        assert job.joboptions["is_relion"].value == "No"
        assert job.joboptions["node_type"].label == "RELION STAR file type:"
        assert (
            job.joboptions["node_type"].value == "RELION coordinates STAR file (.star)"
        )
        assert (
            job.joboptions["optics_group_particles"].label
            == "Rename optics group for particles:"
        )
        assert job.joboptions["optics_group_particles"].value == ""
        assert (
            job.joboptions["pipeliner_node_type"].label
            == "Node type for imported file:"
        )
        assert job.joboptions["pipeliner_node_type"].value == "DensityMap"
        assert job.joboptions["alt_nodetype"].label == "OR: Create a new node type:"
        assert job.joboptions["alt_nodetype"].value == ""
        assert job.joboptions["kwds"].label == "Keywords"
        assert job.joboptions["kwds"].value == ""
        assert (
            job.joboptions["is_synthetic"].label
            == "Does the node contain synthetic data?"
        )
        assert job.joboptions["is_synthetic"].value == "No"

    def test_reading_import_halfmap_job(self):
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Import/import_halfmaps.job")
        )
        assert job.PROCESS_NAME == data_structure.IMPORT_OTHER_NAME
        assert job.output_dir == ""
        assert len(job.input_nodes) == 0
        assert len(job.output_nodes) == 0
        assert len(job.joboptions) == 8
        assert job.joboptions["fn_in_other"].label == "Input file:"
        assert job.joboptions["fn_in_other"].value == "3488_run_half1_class001.mrc"
        assert job.joboptions["is_relion"].label == "Is the file a RELION STAR file?"
        assert job.joboptions["is_relion"].value == "No"
        assert job.joboptions["node_type"].label == "RELION STAR file type:"
        assert (
            job.joboptions["node_type"].value == "RELION coordinates STAR file (.star)"
        )
        assert (
            job.joboptions["optics_group_particles"].label
            == "Rename optics group for particles:"
        )
        assert job.joboptions["optics_group_particles"].value == ""
        assert (
            job.joboptions["pipeliner_node_type"].label
            == "Node type for imported file:"
        )
        assert job.joboptions["pipeliner_node_type"].value == "DensityMap"
        assert job.joboptions["alt_nodetype"].label == "OR: Create a new node type:"
        assert job.joboptions["alt_nodetype"].value == ""
        assert job.joboptions["kwds"].label == "Keywords"
        assert job.joboptions["kwds"].value == "halfmap"
        assert (
            job.joboptions["is_synthetic"].label
            == "Does the node contain synthetic data?"
        )
        assert job.joboptions["is_synthetic"].value == "No"

    def test_get_import_mask_commands(self):
        generic_tests.general_get_command_test(
            jobtype="Import",
            jobfile="import_mask.job",
            jobnumber=1,
            input_nodes={},
            output_nodes={"emd_3488_mask.mrc": "Mask3D.mrc"},
            expected_commands=["cp emd_3488_mask.mrc Import/job001/emd_3488_mask.mrc"],
        )

    def test_get_import_halfmaps_commands(self):
        generic_tests.general_get_command_test(
            jobtype="Import",
            jobfile="import_halfmaps.job",
            jobnumber=1,
            input_nodes={},
            output_nodes={"3488_run_half1_class001.mrc": "DensityMap.mrc.halfmap"},
            expected_commands=[
                "cp 3488_run_half1_class001.mrc Import/job001/3488_run_"
                "half1_class001.mrc",
            ],
        )

    def test_get_import_halfmaps_jobstar(self):
        generic_tests.general_get_command_test(
            jobtype="Import",
            jobfile="import_halfmaps_job.star",
            jobnumber=1,
            input_nodes={},
            output_nodes={"3488_run_half1_class001_unfil.mrc": "DensityMap.mrc"},
            expected_commands=[
                "cp 3488_run_half1_class001_unfil.mrc Import/job001/3488_run_"
                "half1_class001_unfil.mrc",
            ],
        )

    def test_get_import_map_commands(self):
        generic_tests.general_get_command_test(
            jobtype="Import",
            jobfile="import_map_job.star",
            jobnumber=1,
            input_nodes={},
            output_nodes={"emd_3488.mrc": "DensityMap.mrc"},
            expected_commands=[
                "cp emd_3488.mrc Import/job001/emd_3488.mrc",
            ],
        )

    def test_import_job_directory_creation_with_default_jobdir(self):
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Import/import_mask.job")
        )
        job_dir = os.path.join(self.test_dir, "Import/job023/")
        assert not os.path.isdir(job_dir)

        output_dir = "Import/job023/"
        job.output_dir = output_dir
        commandlist = job.get_commands()
        job.prepare_final_command(commandlist, True)

        assert os.path.isdir(job_dir)

    def test_import_job_directory_creation_with_defined_jobdir(self):
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Import/import_mask.job")
        )
        job_dir = os.path.join(self.test_dir, "job_directory/")
        assert not os.path.isdir(job_dir)

        job.output_dir = job_dir
        commandlist = job.get_commands()
        job.prepare_final_command(commandlist, True)
        assert os.path.isdir(job_dir)

    def test_import_job_movies_parameter_validation(self):
        job = new_job_of_type("relion.import.movies")
        job.joboptions["fn_in_raw"].value = "particles.star"
        job.joboptions["optics_group_name"].value = "@pticGroups!"
        exp_err = [
            (
                "error",
                "Value format must match pattern '^[a-zA-Z0-9_]+$'",
                "Optics group name:",
            )
        ]
        errors = job.validate_joboptions()
        formatted_errors = []
        for i in errors:
            error = (i.type, i.message, i.raised_by[0].label)
            formatted_errors.append(error)
            assert error in exp_err
        for i in exp_err:
            assert i in formatted_errors

    def test_import_job_additional_parameter_validation(self):
        job = new_job_of_type("relion.import.other")
        job.joboptions["node_type"].value = "Particles STAR file (.star)"
        job.joboptions["fn_in_other"].value = "particles.star"
        job.joboptions["optics_group_particles"].value = "@pticGroups!"
        job.joboptions["is_relion"].value = "Yes"

        exp_err = [
            (
                "error",
                "Value format must match pattern '^[a-zA-Z0-9_]*$'",
                "Rename optics group for particles:",
            ),
        ]
        errors = job.validate_joboptions()
        for i in errors:
            print(i.__dict__)
        assert len(errors) == 1
        formatted_errors = []
        for i in errors:
            error = (i.type, i.message, i.raised_by[0].label)
            formatted_errors.append(error)
            assert error in exp_err
        for i in exp_err:
            assert i in formatted_errors

    def test_relion_program_returns_expected(self):
        relpro = relion_program("relion_refine")
        expected = ExternalProgram(
            command="relion_refine", vers_com=["relion", "--version"], vers_lines=[0]
        )
        assert relpro.__dict__ == expected.__dict__

    def test_assert_mpi_is_assigned_correctly(self):
        job = RelionJob(force=True)
        job.joboptions["nr_mpi"] = IntJobOption(label="Number of MPI:", default_value=1)
        job.joboptions["nr_mpi"].value = 4
        relpro = relion_program("relion_refine", auto_mpi=job)
        expected = ExternalProgram(
            command="relion_refine_mpi",
            vers_com=["relion", "--version"],
            vers_lines=[0],
        )
        assert relpro.__dict__ == expected.__dict__

    def test_assert_no_mpi_assigned_if_mpis_is_1(self):
        job = RelionJob(force=True)
        job.joboptions["nr_mpi"] = IntJobOption(label="Number of MPI:", default_value=1)
        relpro = relion_program("relion_refine", auto_mpi=job)
        expected = ExternalProgram(
            command="relion_refine",
            vers_com=["relion", "--version"],
            vers_lines=[0],
        )
        assert relpro.__dict__ == expected.__dict__

    def test_assert_no_mpi_assigned_if_no_mpi_jobop(self):
        job = RelionJob(force=True)
        relpro = relion_program("relion_refine", auto_mpi=job)
        expected = ExternalProgram(
            command="relion_refine",
            vers_com=["relion", "--version"],
            vers_lines=[0],
        )
        assert relpro.__dict__ == expected.__dict__


if __name__ == "__main__":
    unittest.main()
