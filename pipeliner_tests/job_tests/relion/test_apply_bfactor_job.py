#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner.nodes import NODE_DENSITYMAP
from pipeliner_tests import test_data
from pipeliner_tests.job_testing_tools import (
    job_generate_commands_test,
    job_display_object_generation_test,
)
from pipeliner.results_display_objects import ResultsDisplayMapModel


class ApplyBFactorJobTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)

        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_commands(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/BFactor/apply_bfactor_job.star"
            ),
            input_nodes={"test.mrc": NODE_DENSITYMAP + ".mrc"},
            output_nodes={"test_bfact.mrc": NODE_DENSITYMAP + ".mrc.b_factor"},
            expected_commands=[
                "relion_image_handler --i test.mrc --bfactor -100.0 --o test_bfact.mrc "
                "--pipeline_control BFactor/job999/"
            ],
        )

    def test_creating_results_display(self):
        mrc_file = os.path.join(self.test_data, "1ake_10A_molrep.mrc")
        expected_rdo = ResultsDisplayMapModel(
            title="Map with b-factor applied: -100.0 \u212B^2",
            start_collapsed=False,
            associated_data=["BFactor/job999/test_bfact.mrc"],
            maps=["BFactor/job999/Thumbnails/test_bfact.mrc"],
            maps_data="BFactor/job999/test_bfact.mrc",
        )
        job_display_object_generation_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/BFactor/apply_bfactor_job.star"
            ),
            expected_display_objects=[(expected_rdo, None)],
            files_to_create={"test_bfact.mrc": mrc_file},
        )


if __name__ == "__main__":
    unittest.main()
