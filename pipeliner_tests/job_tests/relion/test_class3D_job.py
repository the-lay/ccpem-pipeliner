#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import json
import os
import shutil
import tempfile
import unittest
from unittest.mock import patch

from pipeliner.job_factory import read_job
from pipeliner_tests import test_data
from pipeliner.project_graph import ProjectGraph
from pipeliner_tests.generic_tests import (
    general_get_command_test,
    tutorial_data_available,
    get_relion_tutorial_data,
)

from pipeliner.star_writer import COMMENT_LINE
from pipeliner.job_factory import active_job_from_proc
from pipeliner.deposition_tools.pdb_deposition_objects import (
    Final3DClassificationType,
    Final3DClassificationTypeContents,
    SoftwareType,
    SoftwareTypeContents,
)
from pipeliner.deposition_tools.empiar_deposition_objects import (
    EmpiarParticlesType,
    EmpiarParticlesTypeContents,
)
from pipeliner.deposition_tools.pdb_deposition_objects import DEPOSITION_COMMENT
from pipeliner.jobs.relion.Class3D import make_multiref_starfile
from pipeliner.nodes import (
    NODE_DENSITYMAP,
    NODE_PROCESSDATA,
    NODE_PARTICLESDATA,
    NODE_DENSITYMAPGROUPMETADATA,
)


class Class3DTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @patch.dict(os.environ)
    def test_get_scratch_env_var(self):
        # Ensure RELION_SCRATCH_DIR is unset to begin with
        os.environ.pop("RELION_SCRATCH_DIR", None)

        job = read_job(os.path.join(self.test_data, "JobFiles/Class3D/class3D.job"))
        assert job.joboptions["scratch_dir"].default_value == ""

        os.environ["RELION_SCRATCH_DIR"] = os.path.join(
            self.test_data, "fake_SCRATCH_dir"
        )
        job = read_job(os.path.join(self.test_data, "JobFiles/Class3D/class3D.job"))
        assert job.joboptions["scratch_dir"].default_value == os.path.join(
            self.test_data, "fake_SCRATCH_dir"
        )

    def test_get_command_class3D_basic(self):
        general_get_command_test(
            jobtype="Class3D",
            jobfile="class3D.job",
            jobnumber=11,
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLESDATA}.star.relion",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_PROCESSDATA}.star.relion.class3d."
                "optimiser",
                "run_it025_data.star": f"{NODE_PARTICLESDATA}.star.relion.class3d",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 "
                "--offset_step 2.0 --sym C1 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--pipeline_control Class3D/job011/"
            ],
        )

    def test_get_command_class3D_with_symmetry(self):
        general_get_command_test(
            jobtype="Class3D",
            jobfile="class3D_c4sym.job",
            jobnumber=11,
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLESDATA}.star.relion",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_PROCESSDATA}.star.relion.class3d."
                f"c4sym.optimiser",
                "run_it025_data.star": f"{NODE_PARTICLESDATA}.star.relion.class3d."
                "c4sym",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d.c4sym",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d.c4sym",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d.c4sym",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d.c4sym",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 "
                "--offset_step 2.0 --sym C4 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--pipeline_control Class3D/job011/"
            ],
        )

    def test_get_command_class3D_multi(self):
        general_get_command_test(
            jobtype="Class3D",
            jobfile="class3D_multi.job",
            jobnumber=11,
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLESDATA}.star.relion",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
                "reference2.mrc": f"{NODE_DENSITYMAP}.mrc",
                "reference3.mrc": f"{NODE_DENSITYMAP}.mrc",
                "reference4.mrc": f"{NODE_DENSITYMAP}.mrc",
                "reference5.mrc": f"{NODE_DENSITYMAP}.mrc",
                "Class3D/job011/references.star": f"{NODE_DENSITYMAPGROUPMETADATA}."
                "star",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_PROCESSDATA}.star.relion.class3d."
                "optimiser",
                "run_it025_data.star": f"{NODE_PARTICLESDATA}.star.relion.class3d",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class005.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            },
            expected_commands=[
                f"{make_multiref_starfile.__file__} --output_dir Class3D/job011/ "
                "--references InitialModel/job015/run_it150_class001_symD2.mrc "
                "reference2.mrc reference3.mrc reference4.mrc reference5.mrc",
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "Class3D/job011/references.star --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 5 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 "
                "--offset_step 2.0 --sym C1 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--pipeline_control Class3D/job011/",
            ],
        )
        os.makedirs("Class3D/job011")
        make_multiref_starfile.main(
            [
                "--output_dir",
                "Class3D/job011/",
                "--references",
                "InitialModel/job015/run_it150_class001_symD2.mrc",
                "reference2.mrc",
                "reference3.mrc",
                "reference4.mrc",
                "reference5.mrc",
            ]
        )
        with open("Class3D/job011/references.star", "r") as refsfile:
            wrote_refs = [x.replace(" ", "") for x in refsfile.readlines()]
        for line in [
            f"#{COMMENT_LINE.replace(' ','')}\n",
            "data_references\n",
            "loop_\n",
            "_rlnReferenceImage#1\n",
            "InitialModel/job015/run_it150_class001_symD2.mrc\n",
            "reference2.mrc\n",
            "reference3.mrc\n",
            "reference4.mrc\n",
            "reference5.mrc\n",
        ]:
            assert line in wrote_refs, line

    def test_get_command_class3D_multi_with_UserFiles(self):
        general_get_command_test(
            jobtype="Class3D",
            jobfile="class3D_multi_UF.job",
            jobnumber=11,
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLESDATA}.star.relion",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
                "Class3D/job011/reference2.mrc": f"{NODE_DENSITYMAP}.mrc",
                "reference3.mrc": f"{NODE_DENSITYMAP}.mrc",
                "reference4.mrc": f"{NODE_DENSITYMAP}.mrc",
                "reference5.mrc": f"{NODE_DENSITYMAP}.mrc",
                "Class3D/job011/references.star": f"{NODE_DENSITYMAPGROUPMETADATA}"
                ".star",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_PROCESSDATA}.star.relion.class3d."
                "optimiser",
                "run_it025_data.star": f"{NODE_PARTICLESDATA}.star.relion.class3d",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class005.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            },
            expected_commands=[
                "mv UserFiles/reference2.mrc Class3D/job011/reference2.mrc",
                f"{make_multiref_starfile.__file__} --output_dir Class3D/job011/ "
                "--references InitialModel/job015/run_it150_class001_symD2.mrc "
                "Class3D/job011/reference2.mrc reference3.mrc reference4.mrc "
                "reference5.mrc",
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "Class3D/job011/references.star --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 5 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 "
                "--offset_step 2.0 --sym C1 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--pipeline_control Class3D/job011/",
            ],
        )
        os.makedirs("Class3D/job011")
        make_multiref_starfile.main(
            [
                "--output_dir",
                "Class3D/job011/",
                "--references",
                "InitialModel/job015/run_it150_class001_symD2.mrc",
                "Class3D/job011/reference2.mrc",
                "reference3.mrc",
                "reference4.mrc",
                "reference5.mrc",
            ]
        )
        with open("Class3D/job011/references.star", "r") as refsfile:
            wrote_refs = [x.replace(" ", "") for x in refsfile.readlines()]
        for line in [
            f"#{COMMENT_LINE.replace(' ','')}\n",
            "data_references\n",
            "loop_\n",
            "_rlnReferenceImage#1\n",
            "InitialModel/job015/run_it150_class001_symD2.mrc\n",
            "Class3D/job011/reference2.mrc\n",
            "reference3.mrc\n",
            "reference4.mrc\n",
            "reference5.mrc\n",
        ]:
            assert line in wrote_refs, line

    def test_get_command_class3D_multi_error_not_enough_refs(self):
        with self.assertRaises(ValueError):
            general_get_command_test(
                jobtype="Class3D",
                jobfile="class3D_multi_toofew.job",
                jobnumber=11,
                input_nodes={},
                output_nodes={},
                expected_commands=[],
            )

    def test_get_command_class3D_jobstar(self):
        general_get_command_test(
            jobtype="Class3D",
            jobfile="class3D_job.star",
            jobnumber=11,
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLESDATA}.star.relion",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_PROCESSDATA}.star.relion.class3d."
                "optimiser",
                "run_it025_data.star": f"{NODE_PARTICLESDATA}.star.relion.class3d",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 "
                "--offset_step 2.0 --sym C1 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--pipeline_control Class3D/job011/"
            ],
        )

    def test_get_command_class3D_mask(self):
        general_get_command_test(
            jobtype="Class3D",
            jobfile="class3D_mask.job",
            jobnumber=11,
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLESDATA}.star.relion",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
                "MaskCreate/job012/mask.mrc": "Mask3D.mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_PROCESSDATA}.star.relion.class3d."
                "optimiser",
                "run_it025_data.star": f"{NODE_PARTICLESDATA}.star.relion.class3d",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --solvent_mask MaskCreate/job012/mask.mrc"
                " --oversampling 1 --healpix_order 2 --offset_range 5 "
                "--offset_step 2.0 --sym C1 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--pipeline_control Class3D/job011/"
            ],
        )

    def test_get_command_class3D_2masks(self):
        general_get_command_test(
            jobtype="Class3D",
            jobfile="class3D_2masks.job",
            jobnumber=11,
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLESDATA}.star.relion",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
                "MaskCreate/job012/mask.mrc": "Mask3D.mrc",
                "MaskCreate/job013/mask.mrc": "Mask3D.mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_PROCESSDATA}.star.relion.class3d."
                "optimiser",
                "run_it025_data.star": f"{NODE_PARTICLESDATA}.star.relion.class3d",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --solvent_mask MaskCreate/job012/mask.mrc"
                " --oversampling 1 --healpix_order 2 --offset_range 5 "
                "--offset_step 2.0 --sym C1 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--solvent_mask2 MaskCreate/job013/mask.mrc "
                "--pipeline_control Class3D/job011/"
            ],
        )

    def test_get_command_class3D_mask_absgrey(self):
        general_get_command_test(
            jobtype="Class3D",
            jobfile="class3D_mask_noabsgrey.job",
            jobnumber=11,
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLESDATA}.star.relion",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
                "MaskCreate/job012/mask.mrc": "Mask3D.mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_PROCESSDATA}.star.relion.class3d."
                "optimiser",
                "run_it025_data.star": f"{NODE_PARTICLESDATA}.star.relion.class3d",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --firstiter_cc"
                " --ini_high 50.0 --dont_combine_weights_via_disc --preread_images"
                " --pool 30 --pad 2 --skip_gridding --ctf --iter "
                "25 --tau2_fudge 4 --particle_diameter 200 --K 4 --flatten_solvent "
                "--zero_mask --solvent_mask MaskCreate/job012/mask.mrc"
                " --oversampling 1 --healpix_order 2 --offset_range 5 --offset_step "
                "2.0 --sym C1 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--pipeline_control Class3D/job011/"
            ],
        )

    def test_get_command_class3D_noctfref(self):
        general_get_command_test(
            jobtype="Class3D",
            jobfile="class3D_noctfcorref.job",
            jobnumber=11,
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLESDATA}.star.relion",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_PROCESSDATA}.star.relion.class3d."
                "optimiser",
                "run_it025_data.star": f"{NODE_PARTICLESDATA}.star.relion.class3d",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 --offset_step "
                "2.0 --sym C1 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--pipeline_control Class3D/job011/"
            ],
        )

    def test_get_command_class3D_ctfintact(self):
        general_get_command_test(
            jobtype="Class3D",
            jobfile="class3D_ctfintact.job",
            jobnumber=11,
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLESDATA}.star.relion",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_PROCESSDATA}.star.relion.class3d."
                "optimiser",
                "run_it025_data.star": f"{NODE_PARTICLESDATA}.star.relion.class3d",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --ctf_intact_first_peak"
                " --iter 25 --tau2_fudge 4 --particle_diameter 200 --K 4 "
                "--flatten_solvent --zero_mask --oversampling 1 --healpix_order 2"
                " --offset_range 5 --offset_step 2.0"
                " --sym C1 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--pipeline_control Class3D/job011/"
            ],
        )

    def test_get_command_class3D_nozeromask(self):
        general_get_command_test(
            jobtype="Class3D",
            jobfile="class3D_nozero.job",
            jobnumber=11,
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLESDATA}.star.relion",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_PROCESSDATA}.star.relion.class3d."
                "optimiser",
                "run_it025_data.star": f"{NODE_PARTICLESDATA}.star.relion.class3d",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent"
                " --oversampling 1 --healpix_order 2 --offset_range 5 --offset_step "
                "2.0 --sym C1 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--pipeline_control Class3D/job011/"
            ],
        )

    def test_get_command_class3D_hireslim(self):
        general_get_command_test(
            jobtype="Class3D",
            jobfile="class3D_hireslim.job",
            jobnumber=11,
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLESDATA}.star.relion",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_PROCESSDATA}.star.relion.class3d."
                "optimiser",
                "run_it025_data.star": f"{NODE_PARTICLESDATA}.star.relion.class3d",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --strict_highres_exp 12.0 --oversampling 1 --healpix_order 2 "
                "--offset_range 5 --offset_step 2.0 --sym C1 --norm --scale --j 6 "
                "--gpu 4:5:6:7 --pipeline_control Class3D/job011/"
            ],
        )

    def test_get_command_class3D_dfferent_sampling(self):
        general_get_command_test(
            jobtype="Class3D",
            jobfile="class3D_diffsamp.job",
            jobnumber=11,
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLESDATA}.star.relion",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_PROCESSDATA}.star.relion.class3d."
                "optimiser",
                "run_it025_data.star": f"{NODE_PARTICLESDATA}.star.relion.class3d",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 3 --offset_range 5 --offset_step "
                "2.0 --sym C1 --norm --scale --j 6 --gpu 4:5:6:7 "
                "--pipeline_control Class3D/job011/"
            ],
        )

    def test_get_command_class3D_local(self):
        general_get_command_test(
            jobtype="Class3D",
            jobfile="class3D_local.job",
            jobnumber=11,
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLESDATA}.star.relion",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_PROCESSDATA}.star.relion.class3d."
                "optimiser",
                "run_it025_data.star": f"{NODE_PARTICLESDATA}.star.relion.class3d",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --sigma_ang 1.66667 "
                "--offset_range 5 --offset_step 2.0 --sym C1 --norm --scale --j "
                "6 --gpu 4:5:6:7 --pipeline_control Class3D/job011/"
            ],
        )

    def test_get_command_class3D_coarse(self):
        general_get_command_test(
            jobtype="Class3D",
            jobfile="class3D_coarse.job",
            jobnumber=11,
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLESDATA}.star.relion",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_PROCESSDATA}.star.relion.class3d."
                "optimiser",
                "run_it025_data.star": f"{NODE_PARTICLESDATA}.star.relion.class3d",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 --offset_step "
                "2.0 --allow_coarser_sampling --sym C1 --norm --scale --j 6 --gpu "
                "4:5:6:7 --pipeline_control Class3D/job011/"
            ],
        )

    def test_get_command_class3D_helical(self):
        general_get_command_test(
            jobtype="Class3D",
            jobfile="class3D_helical.job",
            jobnumber=11,
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLESDATA}.star.relion."
                "helical",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_PROCESSDATA}.star.relion.class3d."
                "helical.optimiser",
                "run_it025_data.star": f"{NODE_PARTICLESDATA}.star.relion.class3d."
                "helical",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 --offset_step "
                "2.0 --sym C1 --norm --scale --helix --helical_inner_diameter 20"
                " --helical_outer_diameter 50 --helical_nr_asu 1"
                " --helical_twist_initial 0 --helical_rise_initial 0 "
                "--helical_z_percentage 0.3 --helical_keep_tilt_prior_fixed "
                "--sigma_tilt 5 --sigma_psi 3.33333 --sigma_rot 3.33333 --j "
                "6 --gpu 4:5:6:7 --pipeline_control Class3D/job011/"
            ],
        )

    def test_get_command_class3D_helical_multiref(self):
        general_get_command_test(
            jobtype="Class3D",
            jobfile="class3D_helical_multi.job",
            jobnumber=11,
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLESDATA}.star.relion."
                "helical",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
                "reference2.mrc": f"{NODE_DENSITYMAP}.mrc",
                "reference3.mrc": f"{NODE_DENSITYMAP}.mrc",
                "Class3D/job011/references.star": f"{NODE_DENSITYMAPGROUPMETADATA}."
                "star",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_PROCESSDATA}.star.relion.class3d."
                "helical."
                "optimiser",
                "run_it025_data.star": f"{NODE_PARTICLESDATA}.star.relion.class3d."
                "helical",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
            },
            expected_commands=[
                f"{make_multiref_starfile.__file__}"
                " --output_dir Class3D/job011/ --references InitialModel/job"
                "015/run_it150_class001_symD2.mrc reference2.mrc reference3.mrc",
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "Class3D/job011/references.star --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 3 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 --offset_step "
                "2.0 --sym C1 --norm --scale --helix --helical_inner_diameter 20"
                " --helical_outer_diameter 50 --helical_nr_asu 1"
                " --helical_twist_initial 0 --helical_rise_initial 0 "
                "--helical_z_percentage 0.3 --helical_keep_tilt_prior_fixed "
                "--sigma_tilt 5 --sigma_psi 3.33333 --sigma_rot 3.33333 --j "
                "6 --gpu 4:5:6:7 --pipeline_control Class3D/job011/",
            ],
        )
        os.makedirs("Class3D/job011")
        make_multiref_starfile.main(
            [
                "--output_dir",
                "Class3D/job011/",
                "--references",
                "InitialModel/job015/run_it150_class001_symD2.mrc",
                "reference2.mrc",
                "reference3.mrc",
            ]
        )
        with open("Class3D/job011/references.star", "r") as refsfile:
            wrote_refs = [x.replace(" ", "") for x in refsfile.readlines()]
        for line in [
            f"#{COMMENT_LINE.replace(' ','')}\n",
            "data_references\n",
            "loop_\n",
            "_rlnReferenceImage#1\n",
            "InitialModel/job015/run_it150_class001_symD2.mrc\n",
            "reference2.mrc\n",
            "reference3.mrc\n",
        ]:
            assert line in wrote_refs, line

    def test_get_command_class3D_helical_multiref_with_UserFiles(self):
        general_get_command_test(
            jobtype="Class3D",
            jobfile="class3D_helical_multi_UF.job",
            jobnumber=11,
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLESDATA}.star.relion."
                "helical",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
                "Class3D/job011/reference2.mrc": f"{NODE_DENSITYMAP}.mrc",
                "Class3D/job011/reference3.mrc": f"{NODE_DENSITYMAP}.mrc",
                "Class3D/job011/references.star": f"{NODE_DENSITYMAPGROUPMETADATA}."
                "star",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_PROCESSDATA}.star.relion.class3d."
                "helical.optimiser",
                "run_it025_data.star": f"{NODE_PARTICLESDATA}.star.relion.class3d."
                "helical",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
            },
            expected_commands=[
                "mv UserFiles/reference2.mrc Class3D/job011/reference2.mrc",
                "mv UserFiles/reference3.mrc Class3D/job011/reference3.mrc",
                f"{make_multiref_starfile.__file__}"
                " --output_dir Class3D/job011/ --references InitialModel/job"
                "015/run_it150_class001_symD2.mrc Class3D/job011/reference2.mrc "
                "Class3D/job011/reference3.mrc",
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "Class3D/job011/references.star --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 3 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 --offset_step "
                "2.0 --sym C1 --norm --scale --helix --helical_inner_diameter 20"
                " --helical_outer_diameter 50 --helical_nr_asu 1"
                " --helical_twist_initial 0 --helical_rise_initial 0 "
                "--helical_z_percentage 0.3 --helical_keep_tilt_prior_fixed "
                "--sigma_tilt 5 --sigma_psi 3.33333 --sigma_rot 3.33333 --j "
                "6 --gpu 4:5:6:7 --pipeline_control Class3D/job011/",
            ],
        )
        os.makedirs("Class3D/job011")
        make_multiref_starfile.main(
            [
                "--output_dir",
                "Class3D/job011/",
                "--references",
                "InitialModel/job015/run_it150_class001_symD2.mrc",
                "Class3D/job011/reference2.mrc",
                "Class3D/job011/reference3.mrc",
            ]
        )
        with open("Class3D/job011/references.star", "r") as refsfile:
            wrote_refs = [x.replace(" ", "") for x in refsfile.readlines()]
        for line in [
            f"#{COMMENT_LINE.replace(' ','')}\n",
            "data_references\n",
            "loop_\n",
            "_rlnReferenceImage#1\n",
            "InitialModel/job015/run_it150_class001_symD2.mrc\n",
            "Class3D/job011/reference2.mrc\n",
            "Class3D/job011/reference3.mrc\n",
        ]:
            assert line in wrote_refs, line

    def test_get_command_class3D_helical_noTPF(self):
        general_get_command_test(
            jobtype="Class3D",
            jobfile="class3D_helical_noTPF.job",
            jobnumber=11,
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLESDATA}.star.relion."
                "helical",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_PROCESSDATA}.star.relion.class3d."
                f"helical.optimiser",
                "run_it025_data.star": f"{NODE_PARTICLESDATA}.star.relion.class3d."
                "helical",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 --offset_step "
                "2.0 --sym C1 --norm --scale --helix --helical_inner_diameter 20"
                " --helical_outer_diameter 50 --helical_nr_asu 1"
                " --helical_twist_initial 0 --helical_rise_initial 0 "
                "--helical_z_percentage 0.3 "
                "--sigma_tilt 5 --sigma_psi 3.33333 --sigma_rot 3.33333 --j "
                "6 --gpu 4:5:6:7 --pipeline_control Class3D/job011/"
            ],
        )

    def test_get_command_class3D_helical_nosym(self):
        general_get_command_test(
            jobtype="Class3D",
            jobfile="class3D_helical_nosym.job",
            jobnumber=11,
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLESDATA}.star.relion."
                "helical",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_PROCESSDATA}.star.relion.class3d."
                "helical.optimiser",
                "run_it025_data.star": f"{NODE_PARTICLESDATA}.star.relion.class3d."
                "helical",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 --offset_step "
                "2.0 --sym C1 --norm --scale --helix --helical_inner_diameter 20"
                " --helical_outer_diameter 50 --ignore_helical_symmetry "
                "--helical_keep_tilt_prior_fixed "
                "--sigma_tilt 5 --sigma_psi 3.33333 --sigma_rot 3.33333 --j "
                "6 --gpu 4:5:6:7 --pipeline_control Class3D/job011/"
            ],
        )

    def test_get_command_class3D_helical_search(self):
        general_get_command_test(
            jobtype="Class3D",
            jobfile="class3D_helical_search.job",
            jobnumber=11,
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLESDATA}.star.relion."
                "helical",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_PROCESSDATA}.star.relion.class3d."
                f"helical.optimiser",
                "run_it025_data.star": f"{NODE_PARTICLESDATA}.star.relion.class3d."
                "helical",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 --offset_step "
                "2.0 --sym C1 --norm --scale --helix --helical_inner_diameter 20"
                " --helical_outer_diameter 50 --helical_nr_asu 1"
                " --helical_twist_initial 0 --helical_rise_initial 0 "
                "--helical_z_percentage 0.3 --helical_symmetry_search "
                "--helical_twist_min 15 --helical_twist_max 30 "
                "--helical_twist_inistep 2 --helical_rise_min 10 "
                "--helical_rise_max 20 --helical_rise_inistep 1 "
                "--helical_keep_tilt_prior_fixed "
                "--sigma_tilt 5 --sigma_psi 3.33333 --sigma_rot 3.33333 --j "
                "6 --gpu 4:5:6:7 --pipeline_control Class3D/job011/"
            ],
        )

    def test_get_command_class3D_helical_search_relionstyle_name(self):
        """Test automatuic conversion of ambiguous relion style name"""
        general_get_command_test(
            jobtype="Class3D",
            jobfile="class3D_helical_search_relionstyle.job",
            jobnumber=11,
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLESDATA}.star.relion."
                "helical",
                "InitialModel/job015/run_it150_class001_symD2.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
            },
            output_nodes={
                "run_it025_optimiser.star": f"{NODE_PROCESSDATA}.star.relion.class3d."
                "helical.optimiser",
                "run_it025_data.star": f"{NODE_PARTICLESDATA}.star.relion.class3d."
                "helical",
                "run_it025_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
                "run_it025_class004.mrc": f"{NODE_DENSITYMAP}.mrc.relion.class3d."
                "helical",
            },
            expected_commands=[
                "relion_refine --i "
                "Select/job014/particles.star --o Class3D/job011/run --ref "
                "InitialModel/job015/run_it150_class001_symD2.mrc --ini_high 50.0"
                " --dont_combine_weights_via_disc --preread_images --pool 30 --pad 2"
                " --skip_gridding --ctf --iter 25 --tau2_fudge 4"
                " --particle_diameter 200 --K 4 --flatten_solvent --zero_mask"
                " --oversampling 1 --healpix_order 2 --offset_range 5 --offset_step "
                "2.0 --sym C1 --norm --scale --helix --helical_inner_diameter 20"
                " --helical_outer_diameter 50 --helical_nr_asu 1"
                " --helical_twist_initial 0 --helical_rise_initial 0 "
                "--helical_z_percentage 0.3 --helical_symmetry_search "
                "--helical_twist_min 15 --helical_twist_max 30 "
                "--helical_twist_inistep 2 --helical_rise_min 10 "
                "--helical_rise_max 20 --helical_rise_inistep 1 "
                "--helical_keep_tilt_prior_fixed "
                "--sigma_tilt 5 --sigma_psi 3.33333 --sigma_rot 3.33333 --j "
                "6 --gpu 4:5:6:7 --pipeline_control Class3D/job011/"
            ],
        )

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_create_display_cl3d(self):
        get_relion_tutorial_data(["Class3D"])
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("Class3D/job016/")
        dispobjs = pipeline.get_process_results_display(proc)
        expected = [
            {
                "maps": ["Class3D/job016/Thumbnails/run_it025_class001.mrc"],
                "dobj_type": "mapmodel",
                "maps_opacity": [1.0],
                "models": [],
                "title": "Class 1 - 25.17 \u212B - 154 particles",
                "maps_data": "25.17 \u212B - 154 particles",
                "models_data": "",
                "associated_data": [
                    "Class3D/job016/run_it025_class001.mrc",
                    "Class3D/job016/run_it025_data.star",
                ],
                "start_collapsed": True,
                "flag": "",
                "maps_colours": [],
                "models_colours": [],
            },
            {
                "maps": ["Class3D/job016/Thumbnails/run_it025_class002.mrc"],
                "dobj_type": "mapmodel",
                "maps_opacity": [1.0],
                "models": [],
                "title": "Class 2 - 20.6 \u212B - 736 particles",
                "maps_data": "20.6 \u212B - 736 particles",
                "models_data": "",
                "associated_data": [
                    "Class3D/job016/run_it025_class002.mrc",
                    "Class3D/job016/run_it025_data.star",
                ],
                "start_collapsed": True,
                "flag": "",
                "maps_colours": [],
                "models_colours": [],
            },
            {
                "maps": ["Class3D/job016/Thumbnails/run_it025_class003.mrc"],
                "dobj_type": "mapmodel",
                "maps_opacity": [1.0],
                "models": [],
                "title": "Class 3 - 25.17 \u212B - 173 particles",
                "maps_data": "25.17 \u212B - 173 particles",
                "models_data": "",
                "associated_data": [
                    "Class3D/job016/run_it025_class003.mrc",
                    "Class3D/job016/run_it025_data.star",
                ],
                "start_collapsed": True,
                "flag": "",
                "maps_colours": [],
                "models_colours": [],
            },
            {
                "maps": ["Class3D/job016/Thumbnails/run_it025_class004.mrc"],
                "dobj_type": "mapmodel",
                "maps_opacity": [1.0],
                "models": [],
                "title": "Class 4 - 9.85 \u212B - 4747 particles",
                "maps_data": "9.85 \u212B - 4747 particles",
                "models_data": "",
                "associated_data": [
                    "Class3D/job016/run_it025_class004.mrc",
                    "Class3D/job016/run_it025_data.star",
                ],
                "start_collapsed": True,
                "flag": "",
                "maps_colours": [],
                "models_colours": [],
            },
        ]
        dist_results = os.path.join(
            self.test_data, "ResultsFiles/class3d_dist_results.json"
        )
        with open(dist_results, "r") as dr:
            expected.append(json.load(dr))

        do_dicts = [x.__dict__ for x in dispobjs]
        for i in do_dicts:
            assert i in expected, print(i, expected[-1])
        for i in expected:
            assert i in do_dicts

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_get_onedep_data(self):
        get_relion_tutorial_data(["Class3D", "Extract"])
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("Class3D/job016/")
        job = active_job_from_proc(proc)
        depobjs = job.prepare_onedep_data()

        expected_depobjs = [
            EmpiarParticlesType(
                particle_images=EmpiarParticlesTypeContents(
                    name="Particle images",
                    directory="Extract/job012/Movies",
                    category="('T5', '')",
                    header_format="('T2', '')",
                    data_format="('OT', '16-bit float')",
                    num_images_or_tilt_series=24,
                    frames_per_image=1,
                    voxel_type="('OT', '16-bit float')",
                    pixel_width=0.885,
                    pixel_height=0.885,
                    details="5812 total particles; Voltage 1.4; Spherical aberration "
                    "1.4; Image data in file: Class3D/job016/run_it025_data."
                    f"star; {DEPOSITION_COMMENT}",
                    image_width=64,
                    image_height=64,
                    micrographs_file_pattern="Extract/job012/Movies/20170629_000*_"
                    "frameImage.mrcs",
                    picked_particles_file_pattern="Class3D/job016/run_it025_data.star",
                )
            ),
            Final3DClassificationType(
                final_three_d_classification=Final3DClassificationTypeContents(
                    number_classes=4,
                    average_number_members_per_class=1453,
                    software_list=(
                        SoftwareType(
                            software_type=SoftwareTypeContents(
                                name="relion_refine_mpi",
                                version="RELION version: 3.1.3-commit-8bbf42 ",
                                processing_details=DEPOSITION_COMMENT,
                            )
                        ),
                    ),
                    details=DEPOSITION_COMMENT,
                ),
            ),
        ]

        assert depobjs[0] == expected_depobjs[0]
        assert depobjs[1] == expected_depobjs[1]

    def test_make_reference_file_script(self):
        os.makedirs("Class3D/job001")
        make_multiref_starfile.main(
            ["--output_dir", "Class3D/job001/", "--references", "ref1.mrc", "ref2.mrc"]
        )
        reflines = [
            "data_references",
            "loop_",
            "_rlnReferenceImage #1",
            "ref1.mrc",
            "ref2.mrc",
        ]
        with open("Class3D/job001/references.star") as rf:
            wrote = [x.strip() for x in rf.readlines()]
        for line in reflines:
            assert line in wrote


if __name__ == "__main__":
    unittest.main()
