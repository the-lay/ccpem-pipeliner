#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
import shutil
import tempfile
import unittest
from unittest.mock import patch

from pipeliner import job_factory
from pipeliner_tests import test_data
from pipeliner_tests.generic_tests import (
    general_get_command_test,
    tutorial_data_available,
    get_relion_tutorial_data,
)
from pipeliner.project_graph import ProjectGraph


class Refine3DTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @patch.dict(os.environ)
    def test_get_scratch_env_var(self):
        # Ensure RELION_SCRATCH_DIR is unset to begin with
        os.environ.pop("RELION_SCRATCH_DIR", None)

        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Refine3D/refine3D.job")
        )
        assert job.joboptions["scratch_dir"].default_value == ""

        os.environ["RELION_SCRATCH_DIR"] = os.path.join(
            self.test_data, "fake_SCRATCH_dir"
        )
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Refine3D/refine3D.job")
        )
        assert job.joboptions["scratch_dir"].default_value == os.path.join(
            self.test_data, "fake_SCRATCH_dir"
        )

    def test_get_command_refine3D_basic(self):
        general_get_command_test(
            jobtype="Refine3D",
            jobfile="refine3D.job",
            jobnumber=12,
            input_nodes={
                "Extract/job018/particles.star": "ParticlesData.star.relion",
                "Class3D/job016/run_it025_class001_box256.mrc": "DensityMap.mrc",
            },
            output_nodes={
                "run_data.star": "ParticlesData.star.relion.refine3d",
                "run_optimiser.star": "ProcessData.star.relion.refine3d.optimiser",
                "run_half1_class001_unfil.mrc": "DensityMap.mrc.relion.refine3d.d2sym."
                "halfmap",
                "run_class001.mrc": "DensityMap.mrc.relion.refine3d" + ".d2sym",
            },
            expected_commands=[
                "mpirun -n 5 relion_refine_mpi --i "
                "Extract/job018/particles.star --o Refine3D/job012/run --auto_refine"
                " --split_random_halves --ref Class3D/job016/run_it025_class001"
                "_box256.mrc --firstiter_cc --ini_high 50 --dont_combine_weights_via"
                "_disc --preread_images --pool 30 --pad 2 --skip_gridding --ctf "
                "--particle_diameter 200 --flatten_solvent --zero_mask "
                "--oversampling 1 --healpix_order 2 --auto_local_healpix_order 4 "
                "--offset_range 5 --offset_step 2 --sym D2 --low_resol_join_halves 40 "
                "--norm --scale --j 6 --gpu 4:5:6:7 --pipeline_control "
                "Refine3D/job012/"
            ],
        )

    def test_get_command_refine3D_jobstar(self):
        general_get_command_test(
            jobtype="Refine3D",
            jobfile="refine3d_job.star",
            jobnumber=12,
            input_nodes={
                "Extract/job018/particles.star": "ParticlesData.star.relion",
                "Class3D/job016/run_it025_class001_box256.mrc": "DensityMap.mrc",
            },
            output_nodes={
                "run_data.star": "ParticlesData.star.relion.refine3d",
                "run_optimiser.star": "ProcessData.star.relion.refine3d.optimiser",
                "run_half1_class001_unfil.mrc": "DensityMap.mrc.relion.refine3d.d2sym."
                "halfmap",
                "run_class001.mrc": "DensityMap.mrc.relion.refine3d" + ".d2sym",
            },
            expected_commands=[
                "mpirun -n 5 relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job012/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --skip_gridding --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_ord"
                "er 2 --auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --"
                "sym D2 --low_resol_join_halves 40 --norm --scale --j 6 --gpu "
                "4:5:6:7 --pipeline_control Refine3D/job012/"
            ],
        )

    def test_get_command_refine3D_samplingcheck(self):
        general_get_command_test(
            jobtype="Refine3D",
            jobfile="refine3D_sampling.job",
            jobnumber=12,
            input_nodes={
                "Extract/job018/particles.star": "ParticlesData.star.relion",
                "Class3D/job016/run_it025_class001_box256.mrc": "DensityMap.mrc",
            },
            output_nodes={
                "run_data.star": "ParticlesData.star.relion.refine3d",
                "run_optimiser.star": "ProcessData.star.relion.refine3d.optimiser",
                "run_half1_class001_unfil.mrc": "DensityMap.mrc.relion.refine3d.d2sym."
                "halfmap",
                "run_class001.mrc": "DensityMap.mrc.relion.refine3d" + ".d2sym",
            },
            expected_commands=[
                "mpirun -n 5 relion_refine_mpi --i Extract/job018/particles.sta"
                "r --o Refine3D/job012/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --skip_gridding --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_ord"
                "er 4 --auto_local_healpix_order 7 --offset_range 5 --offset_step 2 --"
                "sym D2 --low_resol_join_halves 40 --norm --scale --j 6 --gpu "
                "4:5:6:7 --pipeline_control Refine3D/job012/"
            ],
        )

    def test_get_command_refine3D_mask(self):
        general_get_command_test(
            jobtype="Refine3D",
            jobfile="refine3D_mask.job",
            jobnumber=12,
            input_nodes={
                "Extract/job018/particles.star": "ParticlesData.star.relion",
                "Class3D/job016/run_it025_class001_box256.mrc": "DensityMap.mrc",
                "MaskCreate/job200/mask.mrc": "Mask3D.mrc",
            },
            output_nodes={
                "run_data.star": "ParticlesData.star.relion.refine3d",
                "run_optimiser.star": "ProcessData.star.relion.refine3d.optimiser",
                "run_half1_class001_unfil.mrc": "DensityMap.mrc.relion.refine3d.d2sym."
                "halfmap",
                "run_class001.mrc": "DensityMap.mrc.relion.refine3d" + ".d2sym",
            },
            expected_commands=[
                "mpirun -n 5 relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job012/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --skip_gridding --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask "
                "--solvent_mask MaskCreate/job200/mask.mrc"
                " --oversampling 1 --healpix_order 2 --auto_local_healpix_order 4 "
                "--offset_range 5 --offset_step 2 --sym D2"
                " --low_resol_join_halves 40 --norm --scale --j 6 --gpu 4:5:6:7"
                " --pipeline_control Refine3D/job012/"
            ],
        )

    def test_get_command_refine3D_mask_solvflat(self):
        general_get_command_test(
            jobtype="Refine3D",
            jobfile="refine3D_mask_solvflat.job",
            jobnumber=12,
            input_nodes={
                "Extract/job018/particles.star": "ParticlesData.star.relion",
                "Class3D/job016/run_it025_class001_box256.mrc": "DensityMap.mrc",
                "MaskCreate/job200/mask.mrc": "Mask3D.mrc",
            },
            output_nodes={
                "run_data.star": "ParticlesData.star.relion.refine3d",
                "run_optimiser.star": "ProcessData.star.relion.refine3d.optimiser",
                "run_half1_class001_unfil.mrc": "DensityMap.mrc.relion.refine3d.d2sym."
                "halfmap",
                "run_class001.mrc": "DensityMap.mrc.relion.refine3d" + ".d2sym",
            },
            expected_commands=[
                "mpirun -n 5 relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job012/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --skip_gridding --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask --solvent_mask MaskCreate/job2"
                "00/mask.mrc --solvent_correct_fsc --oversampling 1 --healpix_order 2 "
                "--auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --sym D"
                "2 --low_resol_join_halves 40 --norm --scale --j 6 --gpu 4:5:6:7"
                " --pipeline_control Refine3D/job012/"
            ],
        )

    def test_get_command_refine3D_absgrey(self):
        general_get_command_test(
            jobtype="Refine3D",
            jobfile="refine3D_absgrey.job",
            jobnumber=12,
            input_nodes={
                "Extract/job018/particles.star": "ParticlesData.star.relion",
                "Class3D/job016/run_it025_class001_box256.mrc": "DensityMap.mrc",
            },
            output_nodes={
                "run_data.star": "ParticlesData.star.relion.refine3d",
                "run_optimiser.star": "ProcessData.star.relion.refine3d.optimiser",
                "run_half1_class001_unfil.mrc": "DensityMap.mrc.relion.refine3d.d2sym."
                "halfmap",
                "run_class001.mrc": "DensityMap.mrc.relion.refine3d" + ".d2sym",
            },
            expected_commands=[
                "mpirun -n 5 relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job012/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --skip_gridding --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_ord"
                "er 2 --auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --"
                "sym D2 --low_resol_join_halves 40 --norm --scale --j 6 --gpu 4:5:6:7"
                " --pipeline_control Refine3D/job012/"
            ],
        )

    def test_get_command_refine3D_ctf1stpeak(self):
        general_get_command_test(
            jobtype="Refine3D",
            jobfile="refine3D_ctf1stpeak.job",
            jobnumber=12,
            input_nodes={
                "Extract/job018/particles.star": "ParticlesData.star.relion",
                "Class3D/job016/run_it025_class001_box256.mrc": "DensityMap.mrc",
            },
            output_nodes={
                "run_data.star": "ParticlesData.star.relion.refine3d",
                "run_optimiser.star": "ProcessData.star.relion.refine3d.optimiser",
                "run_half1_class001_unfil.mrc": "DensityMap.mrc.relion.refine3d.d2sym."
                "halfmap",
                "run_class001.mrc": "DensityMap.mrc.relion.refine3d" + ".d2sym",
            },
            expected_commands=[
                "mpirun -n 5 relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job012/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --skip_gridding --ctf --ctf_intact_fi"
                "rst_peak --particle_diameter 200 --flatten_solvent --zero_mask --over"
                "sampling 1 --healpix_order 2 --auto_local_healpix_order 4 --offset_ra"
                "nge 5 --offset_step 2 --sym D2"
                " --low_resol_join_halves 40 --norm --scale --j 6 --gpu 4:5:6:7"
                " --pipeline_control Refine3D/job012/"
            ],
        )

    def test_get_command_refine3D_finerfaster(self):
        general_get_command_test(
            jobtype="Refine3D",
            jobfile="refine3D_finerfaster.job",
            jobnumber=12,
            input_nodes={
                "Extract/job018/particles.star": "ParticlesData.star.relion",
                "Class3D/job016/run_it025_class001_box256.mrc": "DensityMap.mrc",
            },
            output_nodes={
                "run_data.star": "ParticlesData.star.relion.refine3d",
                "run_optimiser.star": "ProcessData.star.relion.refine3d.optimiser",
                "run_half1_class001_unfil.mrc": "DensityMap.mrc.relion.refine3d.d2sym."
                "halfmap",
                "run_class001.mrc": "DensityMap.mrc.relion.refine3d.d2sym",
            },
            expected_commands=[
                "mpirun -n 5 relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job012/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --skip_gridding --auto_ignore_angles --auto_resol_angles "
                "--ctf --particle_diameter"
                " 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_order 2"
                " --auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --sym "
                "D2 --low_resol_join_halves 40 --norm --scale --j 6 --gpu 4:5:6:7"
                " --pipeline_control Refine3D/job012/"
            ],
        )

    def test_get_command_refine3D_helical(self):
        general_get_command_test(
            jobtype="Refine3D",
            jobfile="refine3D_helical.job",
            jobnumber=12,
            input_nodes={
                "Extract/job018/particles.star": "ParticlesData.star.relion."
                "helicalsegments",
                "Class3D/job016/run_it025_class001_box256.mrc": "DensityMap.mrc",
            },
            output_nodes={
                "run_data.star": "ParticlesData.star.relion.refine3d.helical",
                "run_optimiser.star": "ProcessData.star.relion.refine3d.helical."
                "optimiser",
                "run_half1_class001_unfil.mrc": "DensityMap.mrc.relion.refine3d."
                "helical.d2sym.halfmap",
                "run_class001.mrc": "DensityMap.mrc.relion.refine3d.helical.d2sym",
            },
            expected_commands=[
                "mpirun -n 5 relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job012/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --skip_gridding --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_ord"
                "er 2 --auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --"
                "sym D2 --low_resol_join_halves 40 --norm --scale --helix "
                "--helical_inner_diameter 30 --helical_outer_diameter 100 --helical_nr"
                "_asu 3 --helical_twist_initial 25 --helical_rise_initial 3.8 "
                "--helical_z_percentage 0.3 --sigma_tilt 5 --sigma_psi 3.33333 --sigma"
                "_rot 0 --helical_keep_tilt_prior_fixed --j 6 --gpu 4:5:6:7"
                " --pipeline_control Refine3D/job012/"
            ],
        )

    def test_get_command_refine3D_helical_relionstyle_jobname(self):
        """Make sure an ambiguous relion style job name is converted"""
        general_get_command_test(
            jobtype="Refine3D",
            jobfile="refine3D_helical_relionstyle.job",
            jobnumber=12,
            input_nodes={
                "Extract/job018/particles.star": "ParticlesData.star.relion."
                "helicalsegments",
                "Class3D/job016/run_it025_class001_box256.mrc": "DensityMap.mrc",
            },
            output_nodes={
                "run_data.star": "ParticlesData.star.relion.refine3d.helical",
                "run_optimiser.star": "ProcessData.star.relion.refine3d.helical."
                "optimiser",
                "run_half1_class001_unfil.mrc": "DensityMap.mrc.relion.refine3d."
                "helical.d2sym.halfmap",
                "run_class001.mrc": "DensityMap.mrc.relion.refine3d.helical.d2sym",
            },
            expected_commands=[
                "mpirun -n 5 relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job012/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --skip_gridding --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_ord"
                "er 2 --auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --"
                "sym D2 --low_resol_join_halves 40 --norm --scale --helix "
                "--helical_inner_diameter 30 --helical_outer_diameter 100 --helical_nr"
                "_asu 3 --helical_twist_initial 25 --helical_rise_initial 3.8 "
                "--helical_z_percentage 0.3 --sigma_tilt 5 --sigma_psi 3.33333 --sigma"
                "_rot 0 --helical_keep_tilt_prior_fixed --j 6 --gpu 4:5:6:7"
                " --pipeline_control Refine3D/job012/"
            ],
        )

    def test_get_command_refine3D_noprior(self):
        general_get_command_test(
            jobtype="Refine3D",
            jobfile="refine3D_helical_noprior.job",
            jobnumber=12,
            input_nodes={
                "Extract/job018/particles.star": "ParticlesData.star.relion."
                "helicalsegments",
                "Class3D/job016/run_it025_class001_box256.mrc": "DensityMap.mrc",
            },
            output_nodes={
                "run_data.star": "ParticlesData.star.relion.refine3d.helical",
                "run_optimiser.star": "ProcessData.star.relion.refine3d.helical."
                "optimiser",
                "run_half1_class001_unfil.mrc": "DensityMap.mrc.relion.refine3d."
                "helical.d2sym.halfmap",
                "run_class001.mrc": "DensityMap.mrc.relion.refine3d.helical.d2sym",
            },
            expected_commands=[
                "mpirun -n 5 relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job012/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --skip_gridding --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_ord"
                "er 2 --auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --"
                "sym D2 --low_resol_join_halves 40 --norm --scale --helix "
                "--helical_inner_diameter 30 --helical_outer_diameter 100 --helical_nr"
                "_asu 3 --helical_twist_initial 25 --helical_rise_initial 3.8 "
                "--helical_z_percentage 0.3 --sigma_tilt 5 --sigma_psi 3.33333 --sigma"
                "_rot 0 --j 6 --gpu 4:5:6:7 --pipeline_control Refine3D/job012/"
            ],
        )

    def test_get_command_refine3D_helical_noHsym(self):
        general_get_command_test(
            jobtype="Refine3D",
            jobfile="refine3D_helical_noHsym.job",
            jobnumber=12,
            input_nodes={
                "Extract/job018/particles.star": "ParticlesData.star.relion."
                "helicalsegments",
                "Class3D/job016/run_it025_class001_box256.mrc": "DensityMap.mrc",
            },
            output_nodes={
                "run_data.star": "ParticlesData.star.relion.refine3d.helical",
                "run_optimiser.star": "ProcessData.star.relion.refine3d.helical."
                "optimiser",
                "run_half1_class001_unfil.mrc": "DensityMap.mrc.relion.refine3d."
                "helical.d2sym.halfmap",
                "run_class001.mrc": "DensityMap.mrc.relion.refine3d.helical.d2sym",
            },
            expected_commands=[
                "mpirun -n 5 relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job012/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --skip_gridding --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_ord"
                "er 2 --auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --"
                "sym D2 --low_resol_join_halves 40 --norm --scale --helix "
                "--helical_inner_diameter 30 --helical_outer_diameter 100 "
                "--ignore_helical_symmetry --sigma_tilt 5 --sigma_psi 3.33333 --sigma_"
                "rot 0 --helical_keep_tilt_prior_fixed --j 6 --gpu 4:5:6:7"
                " --pipeline_control Refine3D/job012/"
            ],
        )

    def test_get_command_refine3D_helical_search(self):
        general_get_command_test(
            jobtype="Refine3D",
            jobfile="refine3D_helical_search.job",
            jobnumber=12,
            input_nodes={
                "Extract/job018/particles.star": "ParticlesData.star.relion."
                "helicalsegments",
                "Class3D/job016/run_it025_class001_box256.mrc": "DensityMap.mrc",
            },
            output_nodes={
                "run_data.star": "ParticlesData.star.relion.refine3d.helical",
                "run_optimiser.star": "ProcessData.star.relion.refine3d.helical."
                "optimiser",
                "run_half1_class001_unfil.mrc": "DensityMap.mrc.relion.refine3d."
                "helical.d2sym.halfmap",
                "run_class001.mrc": "DensityMap.mrc.relion.refine3d.helical.d2sym",
            },
            expected_commands=[
                "mpirun -n 5 relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job012/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --skip_gridding --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_ord"
                "er 2 --auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --"
                "sym D2 --low_resol_join_halves 40 --norm --scale --helix "
                "--helical_inner_diameter 30 --helical_outer_diameter 100 --helical_nr"
                "_asu 3 --helical_twist_initial 25 --helical_rise_initial 3.8 "
                "--helical_z_percentage 0.3 --helical_symmetry_search --helical_twist_"
                "min 20 --helical_twist_max 30 --helical_twist_inistep 1 --helical_ris"
                "e_min 3 --helical_rise_max 4.5 --helical_rise_inistep 0.25 --sigma_ti"
                "lt 5 --sigma_psi 3.33333 --sigma_rot"
                " 0 --helical_keep_tilt_prior_fixed --j 6 --gpu 4:5:6:7"
                " --pipeline_control Refine3D/job012/"
            ],
        )

    def test_get_command_refine3D_helical_sigmarot(self):
        general_get_command_test(
            jobtype="Refine3D",
            jobfile="refine3D_helical_sigmarot.job",
            jobnumber=12,
            input_nodes={
                "Extract/job018/particles.star": "ParticlesData.star.relion."
                "helicalsegments",
                "Class3D/job016/run_it025_class001_box256.mrc": "DensityMap.mrc",
            },
            output_nodes={
                "run_data.star": "ParticlesData.star.relion.refine3d.helical",
                "run_optimiser.star": "ProcessData.star.relion.refine3d.helical."
                "optimiser",
                "run_half1_class001_unfil.mrc": "DensityMap.mrc.relion.refine3d."
                "helical.d2sym.halfmap",
                "run_class001.mrc": "DensityMap.mrc.relion.refine3d.helical.d2sym",
            },
            expected_commands=[
                "mpirun -n 5 relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job012/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --skip_gridding --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_ord"
                "er 2 --auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --"
                "sym D2 --low_resol_join_halves 40 --norm --scale --helix "
                "--helical_inner_diameter 30 --helical_outer_diameter 100 --helical_nr"
                "_asu 3 --helical_twist_initial 25 --helical_rise_initial 3.8 "
                "--helical_z_percentage 0.3 --sigma_tilt 5 --sigma_psi 3.33333 --sigma"
                "_rot 0 --helical_sigma_distance 5 --helical_keep_tilt_prior_fixed --j"
                " 6 --gpu 4:5:6:7 --pipeline_control Refine3D/job012/"
            ],
        )

    def test_get_command_refine3D_nompiGPU(self):
        general_get_command_test(
            jobtype="Refine3D",
            jobfile="refine3D_nompiGPU.job",
            jobnumber=12,
            input_nodes={
                "Extract/job018/particles.star": "ParticlesData.star.relion",
                "Class3D/job016/run_it025_class001_box256.mrc": "DensityMap.mrc",
            },
            output_nodes={
                "run_data.star": "ParticlesData.star.relion.refine3d",
                "run_optimiser.star": "ProcessData.star.relion.refine3d.optimiser",
                "run_half1_class001_unfil.mrc": "DensityMap.mrc.relion.refine3d."
                "d2sym.halfmap",
                "run_class001.mrc": "DensityMap.mrc.relion.refine3d" + ".d2sym",
            },
            expected_commands=[
                "relion_refine --i Extract/job018/particles.star "
                "--o Refine3D/job012/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --skip_gridding --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_ord"
                "er 2 --auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --"
                "sym D2 --low_resol_join_halves 40 --norm --scale --j 6 "
                "--pipeline_control Refine3D/job012/"
            ],
        )

    def test_get_command_refine3D_2masks(self):
        general_get_command_test(
            jobtype="Refine3D",
            jobfile="refine3D_2masks.job",
            jobnumber=12,
            input_nodes={
                "Extract/job018/particles.star": "ParticlesData.star.relion",
                "Class3D/job016/run_it025_class001_box256.mrc": "DensityMap.mrc",
                "MaskCreate/job200/mask.mrc": "Mask3D.mrc",
                "MaskCreate/job013/mask.mrc": "Mask3D.mrc",
            },
            output_nodes={
                "run_data.star": "ParticlesData.star.relion.refine3d",
                "run_optimiser.star": "ProcessData.star.relion.refine3d.optimiser",
                "run_half1_class001_unfil.mrc": "DensityMap.mrc.relion.refine3d."
                "d2sym.halfmap",
                "run_class001.mrc": "DensityMap.mrc.relion.refine3d" + ".d2sym",
            },
            expected_commands=[
                "mpirun -n 5 relion_refine_mpi --i Extract/job018/particles.st"
                "ar --o Refine3D/job012/run --auto_refine --split_random_halves "
                "--ref Class3D/job016/run_it025_class001_box256.mrc --firstiter_cc "
                "--ini_high 50 --dont_combine_weights_via_disc --preread_images --pool"
                " 30 --pad 2 --skip_gridding --ctf --particle_diam"
                "eter 200 --flatten_solvent --zero_mask "
                "--solvent_mask MaskCreate/job200/mask.mrc"
                " --oversampling 1 --healpix_order 2 --auto_local_healpix_order 4 "
                "--offset_range 5 --offset_step 2 --sym D2"
                " --low_resol_join_halves 40 --norm --scale --j 6 --gpu 4:5:6:7"
                " --solvent_mask2 MaskCreate/job013/mask.mrc --pipeline_control "
                "Refine3D/job012/"
            ],
        )

    def test_get_command_refine3D_continue_nofile(self):
        with self.assertRaises(ValueError):
            general_get_command_test(
                jobtype="Refine3D",
                jobfile="refine3D_continue_nofile.job",
                jobnumber=12,
                input_nodes=2,
                output_nodes=3,
                expected_commands="",
            )

    def test_get_command_even_no_mpis_raises_error(self):
        with self.assertRaises(ValueError):
            general_get_command_test(
                jobtype="Refine3D",
                jobfile="even_mpis.job",
                jobnumber=12,
                input_nodes=2,
                output_nodes=3,
                expected_commands="",
            )

    def test_get_command_refine3D_continue(self):
        with self.assertRaises(ValueError):
            general_get_command_test(
                jobtype="Refine3D",
                jobfile="refine3D_continue_nofile.job",
                jobnumber=12,
                input_nodes={
                    "Extract/job018/particles.star": "ParticlesData.star.relion",
                    "Class3D/job016/run_it025_class001_box256.mrc": "DensityMap.mrc",
                },
                output_nodes={
                    "run_class001.mrc": "DensityMap.mrc.relion.refine3d" + ".d2sym",
                    "run_half1_class001_unfil.mrc": "DensityMap.mrc.relion.refine3d."
                    "d2sym.halfmap",
                    "run_data.star": "ParticlesData.star.relion.refine3d",
                    "run_optimiser.star": "ProcessData.star.relion.refine3d.optimiser",
                },
                expected_commands=[
                    "mpirun -n 5 relion_refine_mpi --continue "
                    "run_ct01_it003_optimiser.star --o Refine3D/job011/run_ct3 "
                    "--dont_combine_weights_via_disc --preread_images  --pool 3 --pad "
                    "2 --skip_gridding  --particle_diameter 200 --j 6 --gpu 4:5:6:7"
                    "--pipeline_control Refine3D/job012/"
                ],
            )

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_refine3d_generate_results(self):
        get_relion_tutorial_data(["Refine3D"])
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("Refine3D/job019/")
        dispobjs = pipeline.get_process_results_display(proc)
        assert dispobjs[0].__dict__ == {
            "maps": ["Refine3D/job019/Thumbnails/run_class001.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [1.0],
            "models": [],
            "title": "Final reconstruction - 3.89 \u212B",
            "maps_data": "Refine3D/job019/run_class001.mrc",
            "models_data": "",
            "associated_data": ["Refine3D/job019/run_class001.mrc"],
            "start_collapsed": False,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_refine3d_generate_results_not_finished(self):
        get_relion_tutorial_data(["Refine3D"])
        os.remove("Refine3D/job019/run_class001.mrc")
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("Refine3D/job019/")
        dispobjs = pipeline.get_process_results_display(proc)
        assert dispobjs[0].__dict__ == {
            "maps": ["Refine3D/job019/Thumbnails/run_it011_half1_class001.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [1.0],
            "models": [],
            "title": "Intermediate reconstruction - iteration 11 - 7.59 \u212B",
            "maps_data": "Refine3D/job019/run_it011_half1_class001.mrc",
            "models_data": "",
            "associated_data": ["Refine3D/job019/run_it011_half1_class001.mrc"],
            "start_collapsed": False,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }


if __name__ == "__main__":
    unittest.main()
