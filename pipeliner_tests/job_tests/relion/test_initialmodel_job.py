#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
import shutil
import tempfile
import unittest
from unittest.mock import patch

from pipeliner import job_factory
from pipeliner_tests import test_data
from pipeliner_tests.generic_tests import (
    general_get_command_test,
    get_relion_tutorial_data,
    tutorial_data_available,
)
from pipeliner.project_graph import ProjectGraph
from pipeliner.data_structure import RELION_SUCCESS_FILE


class InitialModelTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @patch.dict(os.environ)
    def test_get_scratch_env_var(self):
        # Ensure RELION_SCRATCH_DIR is unset to begin with
        os.environ.pop("RELION_SCRATCH_DIR", None)

        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/InitialModel/initialmodel.job")
        )
        assert job.joboptions["scratch_dir"].default_value == ""

        os.environ["RELION_SCRATCH_DIR"] = os.path.join(
            self.test_data, "fake_SCRATCH_dir"
        )
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/InitialModel/initialmodel.job")
        )
        assert job.joboptions["scratch_dir"].default_value == os.path.join(
            self.test_data, "fake_SCRATCH_dir"
        )

    def test_get_command_inimodel(self):
        """Test with run.job file, default params"""
        general_get_command_test(
            jobtype="InitialModel",
            jobfile="initialmodel.job",
            jobnumber=12,
            input_nodes={"Select/job014/particles.star": "ParticlesData.star.relion"},
            output_nodes={
                "run_it200_optimiser.star": "ProcessData.star.relion.optimiser."
                "initialmodel",
                "run_it200_data.star": "ParticlesData.star.relion.initialmodel",
                "run_it200_class001.mrc": "DensityMap.mrc.relion.initialmodel",
                "initial_model.mrc": "DensityMap.mrc.relion.initialmodel",
            },
            expected_commands=[
                "relion_refine --grad --denovo_3dref --i Select/job014/particles.star "
                "--o InitialModel/job012/run --iter 200 --ctf --K 1 --sym C1 "
                "--flatten_solvent --zero_mask --dont_combine_weights_via_disc "
                "--scratch_dir None --pool 3 --pad 2 --skip_gridding "
                "--particle_diameter 200 --oversampling 1 --healpix_order 1 "
                "--offset_range 6 --offset_step 4.0 --j 1 --pipeline_control "
                "InitialModel/job012/",
                "rm -f InitialModel/job012/RELION_JOB_EXIT_SUCCESS",
                "relion_align_symmetry --i InitialModel/job012/run_it200_model.star"
                " --o InitialModel/job012/initial_model.mrc --sym C1 --apply_sym "
                "--select_largest_class --pipeline_control InitialModel/job012/",
            ],
        )

    def test_get_command_inimodel_jobstar(self):
        """Test with job.star file default params"""
        general_get_command_test(
            jobtype="InitialModel",
            jobfile="initialmodel_job.star",
            jobnumber=12,
            input_nodes={"Select/job014/particles.star": "ParticlesData.star.relion"},
            output_nodes={
                "run_it200_optimiser.star": "ProcessData.star.relion.optimiser."
                "initialmodel",
                "run_it200_data.star": "ParticlesData.star.relion.initialmodel",
                "run_it200_class001.mrc": "DensityMap.mrc.relion.initialmodel",
                "initial_model.mrc": "DensityMap.mrc.relion.initialmodel",
            },
            expected_commands=[
                "relion_refine --grad --denovo_3dref --i Select/job014/particles.star "
                "--o InitialModel/job012/run --iter 200 --ctf --K 1 --sym C1 "
                "--flatten_solvent --zero_mask --dont_combine_weights_via_disc "
                "--scratch_dir None --pool 3 --pad 2 --skip_gridding "
                "--particle_diameter 200 --oversampling 1 --healpix_order 1 "
                "--offset_range 6 --offset_step 4.0 --j 1 --pipeline_control "
                "InitialModel/job012/",
                "rm -f InitialModel/job012/RELION_JOB_EXIT_SUCCESS",
                "relion_align_symmetry --i InitialModel/job012/run_it200_model.star"
                " --o InitialModel/job012/initial_model.mrc --sym C1 --apply_sym "
                "--select_largest_class --pipeline_control InitialModel/job012/",
            ],
        )

    def test_get_command_inimodel_3classes_c1(self):
        """Three classes with c1 symmetry"""
        general_get_command_test(
            jobtype="InitialModel",
            jobfile="initialmodel_3classes.job",
            jobnumber=12,
            input_nodes={"Select/job014/particles.star": "ParticlesData.star.relion"},
            output_nodes={
                "run_it200_optimiser.star": "ProcessData.star.relion.optimiser."
                "initialmodel",
                "run_it200_data.star": "ParticlesData.star.relion.initialmodel",
                "run_it200_class001.mrc": "DensityMap.mrc.relion.initialmodel",
                "run_it200_class002.mrc": "DensityMap.mrc.relion.initialmodel",
                "run_it200_class003.mrc": "DensityMap.mrc.relion.initialmodel",
                "initial_model.mrc": "DensityMap.mrc.relion.initialmodel",
            },
            expected_commands=[
                "relion_refine --grad --denovo_3dref --i Select/job014/particles.star "
                "--o InitialModel/job012/run --iter 200 --ctf --K 3 --sym C1 "
                "--flatten_solvent --zero_mask --dont_combine_weights_via_disc "
                "--scratch_dir None --pool 3 --pad 2 --skip_gridding "
                "--particle_diameter 200 --oversampling 1 --healpix_order 1 "
                "--offset_range 6 --offset_step 4.0 --j 1 --pipeline_control "
                "InitialModel/job012/",
                "rm -f InitialModel/job012/RELION_JOB_EXIT_SUCCESS",
                "relion_align_symmetry --i InitialModel/job012/run_it200_model.star"
                " --o InitialModel/job012/initial_model.mrc --sym C1 --apply_sym "
                "--select_largest_class --pipeline_control InitialModel/job012/",
            ],
        )

    def test_get_command_inimodel_continue(self):
        general_get_command_test(
            jobtype="InitialModel",
            jobfile="initialmodel_continue.job",
            jobnumber=12,
            input_nodes={"Select/job014/particles.star": "ParticlesData.star.relion"},
            output_nodes={
                "run_it300_optimiser.star": "ProcessData.star.relion.optimiser."
                "initialmodel",
                "run_it300_data.star": "ParticlesData.star.relion.initialmodel",
                "run_it300_class001.mrc": "DensityMap.mrc.relion.initialmodel",
                "initial_model.mrc": "DensityMap.mrc.relion.initialmodel",
            },
            expected_commands=[
                "relion_refine --continue InitialModel/job012/run_it200_optimiser.star"
                " --o InitialModel/job012/run --iter 300 --ctf --K 1 --sym C1"
                " --flatten_solvent --zero_mask --dont_combine_weights_via_disc "
                "--scratch_dir None --pool 3 --pad 2 --skip_gridding "
                "--particle_diameter 200 --oversampling 1 --healpix_order 1 "
                "--offset_range 6 --offset_step 4.0 --j 1 --pipeline_control "
                "InitialModel/job012/",
                "rm -f InitialModel/job012/RELION_JOB_EXIT_SUCCESS",
                "relion_align_symmetry --i "
                "InitialModel/job012/run_it300_model.star --o "
                "InitialModel/job012/initial_model.mrc --sym C1 --apply_sym "
                "--select_largest_class --pipeline_control InitialModel/job012/",
            ],
        )

    def test_get_command_inimodel_finesampling(self):
        """Test with job.star file finer sampling"""
        general_get_command_test(
            jobtype="InitialModel",
            jobfile="initialmodel_finesampling.job",
            jobnumber=12,
            input_nodes={"Select/job014/particles.star": "ParticlesData.star.relion"},
            output_nodes={
                "run_it200_optimiser.star": "ProcessData.star.relion.optimiser."
                "initialmodel",
                "run_it200_data.star": "ParticlesData.star.relion.initialmodel",
                "run_it200_class001.mrc": "DensityMap.mrc.relion.initialmodel",
                "initial_model.mrc": "DensityMap.mrc.relion.initialmodel",
            },
            expected_commands=[
                "relion_refine --grad --denovo_3dref --i Select/job014/particles.star "
                "--o InitialModel/job012/run --iter 200 --ctf --K 1 --sym C1 "
                "--flatten_solvent --zero_mask --dont_combine_weights_via_disc "
                "--scratch_dir None --pool 3 --pad 2 --skip_gridding "
                "--particle_diameter 200 --oversampling 1 --healpix_order 5 "
                "--offset_range 6 --offset_step 4.0 --j 1 --pipeline_control "
                "InitialModel/job012/",
                "rm -f InitialModel/job012/RELION_JOB_EXIT_SUCCESS",
                "relion_align_symmetry --i InitialModel/job012/run_it200_model.star"
                " --o InitialModel/job012/initial_model.mrc --sym C1 --apply_sym "
                "--select_largest_class --pipeline_control InitialModel/job012/",
            ],
        )

    def test_get_command_inimodel_with_sym_runas_c1(self):
        """single class non-c1 symmetry, symmetry applied after refinement"""
        general_get_command_test(
            jobtype="InitialModel",
            jobfile="initialmodel_c4_job.star",
            jobnumber=12,
            input_nodes={"Select/job014/particles.star": "ParticlesData.star.relion"},
            output_nodes={
                "run_it200_optimiser.star": "ProcessData.star.relion.optimiser."
                "initialmodel",
                "run_it200_data.star": "ParticlesData.star.relion.initialmodel",
                "run_it200_class001.mrc": "DensityMap.mrc.relion.initialmodel",
                "initial_model.mrc": "DensityMap.mrc.relion.initialmodel" + ".c4sym",
            },
            expected_commands=[
                "relion_refine --grad --denovo_3dref --i Select/job014/particles.star "
                "--o InitialModel/job012/run --iter 200 --ctf --K 1 --sym C1 "
                "--flatten_solvent --zero_mask --dont_combine_weights_via_disc "
                "--scratch_dir None --pool 3 --pad 2 --skip_gridding "
                "--particle_diameter 200 --oversampling 1 --healpix_order 1 "
                "--offset_range 6 --offset_step 4.0 --j 1 --pipeline_control "
                "InitialModel/job012/",
                "rm -f InitialModel/job012/RELION_JOB_EXIT_SUCCESS",
                "relion_align_symmetry --i InitialModel/job012/run_it200_model.star"
                " --o InitialModel/job012/initial_model.mrc --sym C4 --apply_sym "
                "--select_largest_class --pipeline_control InitialModel/job012/",
            ],
        )

    def test_get_command_inimodel_with_sym(self):
        """single class non-c1 symmetry, symmetry applied during refinement"""
        general_get_command_test(
            jobtype="InitialModel",
            jobfile="initialmodel_c4_inrefine_job.star",
            jobnumber=12,
            input_nodes={"Select/job014/particles.star": "ParticlesData.star.relion"},
            output_nodes={
                "run_it200_optimiser.star": "ProcessData.star.relion.optimiser."
                "initialmodel",
                "run_it200_data.star": "ParticlesData.star.relion.initialmodel",
                "run_it200_class001.mrc": "DensityMap.mrc.relion.initialmodel"
                + ".c4sym",
                "initial_model.mrc": "DensityMap.mrc.relion.initialmodel" + ".c4sym",
            },
            expected_commands=[
                "relion_refine --grad --denovo_3dref --i Select/job014/particles.star "
                "--o InitialModel/job012/run --iter 200 --ctf --K 1 --sym C4 "
                "--flatten_solvent --zero_mask --dont_combine_weights_via_disc "
                "--scratch_dir None --pool 3 --pad 2 --skip_gridding "
                "--particle_diameter 200 --oversampling 1 --healpix_order 1 "
                "--offset_range 6 --offset_step 4.0 --j 1 --pipeline_control "
                "InitialModel/job012/",
                "rm -f InitialModel/job012/RELION_JOB_EXIT_SUCCESS",
                "relion_align_symmetry --i InitialModel/job012/run_it200_model.star "
                "--o InitialModel/job012/initial_model.mrc --sym C1 --apply_sym "
                "--select_largest_class --pipeline_control InitialModel/job012/",
            ],
        )

    def test_get_command_inimodel_with_sym_runas_c1_multiclass(self):
        """single class non-C1 symmetry, symmetry applied after refinement with
        multiple classes"""
        general_get_command_test(
            jobtype="InitialModel",
            jobfile="initialmodel_c4_multiclass_job.star",
            jobnumber=12,
            input_nodes={"Select/job014/particles.star": "ParticlesData.star.relion"},
            output_nodes={
                "run_it200_optimiser.star": "ProcessData.star.relion.optimiser."
                "initialmodel",
                "run_it200_data.star": "ParticlesData.star.relion.initialmodel",
                "run_it200_class001.mrc": "DensityMap.mrc.relion.initialmodel",
                "run_it200_class002.mrc": "DensityMap.mrc.relion.initialmodel",
                "run_it200_class003.mrc": "DensityMap.mrc.relion.initialmodel",
                "initial_model.mrc": "DensityMap.mrc.relion.initialmodel" + ".c4sym",
            },
            expected_commands=[
                "relion_refine --grad --denovo_3dref --i Select/job014/particles.star "
                "--o InitialModel/job012/run --iter 200 --ctf --K 3 --sym C1 "
                "--flatten_solvent --zero_mask --dont_combine_weights_via_disc "
                "--scratch_dir None --pool 3 --pad 2 --skip_gridding "
                "--particle_diameter 200 --oversampling 1 --healpix_order 1 "
                "--offset_range 6 --offset_step 4.0 --j 1 --pipeline_control "
                "InitialModel/job012/",
                "rm -f InitialModel/job012/RELION_JOB_EXIT_SUCCESS",
                "relion_align_symmetry --i InitialModel/job012/run_it200_model.star "
                "--o InitialModel/job012/initial_model.mrc --sym C4 --apply_sym "
                "--select_largest_class --pipeline_control InitialModel/job012/",
            ],
        )

    def test_get_command_inimodel_with_sym_multiclass(self):
        """single class non-C1 symmetry, symmetry applied during refinement with
        multiple classes"""
        general_get_command_test(
            jobtype="InitialModel",
            jobfile="initialmodel_c4_inrefine_multiclass_job.star",
            jobnumber=12,
            input_nodes={"Select/job014/particles.star": "ParticlesData.star.relion"},
            output_nodes={
                "run_it200_optimiser.star": "ProcessData.star.relion.optimiser."
                "initialmodel",
                "run_it200_data.star": "ParticlesData.star.relion.initialmodel",
                "run_it200_class001.mrc": "DensityMap.mrc.relion.initialmodel"
                + ".c4sym",
                "run_it200_class002.mrc": "DensityMap.mrc.relion.initialmodel"
                + ".c4sym",
                "run_it200_class003.mrc": "DensityMap.mrc.relion.initialmodel"
                + ".c4sym",
                "initial_model.mrc": "DensityMap.mrc.relion.initialmodel" + ".c4sym",
            },
            expected_commands=[
                "relion_refine --grad --denovo_3dref --i Select/job014/particles.star "
                "--o InitialModel/job012/run --iter 200 --ctf --K 3 --sym C4 "
                "--flatten_solvent --zero_mask --dont_combine_weights_via_disc "
                "--scratch_dir None --pool 3 --pad 2 --skip_gridding "
                "--particle_diameter 200 --oversampling 1 --healpix_order 1 "
                "--offset_range 6 --offset_step 4.0 --j 1 --pipeline_control "
                "InitialModel/job012/",
                "rm -f InitialModel/job012/RELION_JOB_EXIT_SUCCESS",
                "relion_align_symmetry --i InitialModel/job012/run_it200_model.star "
                "--o InitialModel/job012/initial_model.mrc --sym C1 --apply_sym "
                "--select_largest_class --pipeline_control InitialModel/job012/",
            ],
        )

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_create_display_inimod(self):
        get_relion_tutorial_data("InitialModel")
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("InitialModel/job015/")
        dispobjs = pipeline.get_process_results_display(proc)
        expected = {
            "maps": ["InitialModel/job015/Thumbnails/initial_model.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [1.0],
            "models": [],
            "title": "Initial Model: InitialModel/job015/initial_model.mrc",
            "maps_data": "InitialModel/job015/initial_model.mrc",
            "models_data": "",
            "associated_data": ["InitialModel/job015/initial_model.mrc"],
            "start_collapsed": False,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }
        assert dispobjs[0].__dict__ == expected

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_create_display_inimod_not_finished(self):
        get_relion_tutorial_data("InitialModel")
        os.remove(f"InitialModel/job015/{RELION_SUCCESS_FILE}")
        os.remove("InitialModel/job015/run_it100_class001.mrc")
        os.remove("InitialModel/job015/initial_model.mrc")
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("InitialModel/job015/")
        dispobjs = pipeline.get_process_results_display(proc)
        expected = {
            "maps": ["InitialModel/job015/Thumbnails/run_it090_class001.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [1.0],
            "models": [],
            "title": (
                "Initial model: run_it090_class001.mrc intermediate result iteration"
                " 90/100"
            ),
            "maps_data": "InitialModel/job015/run_it090_class001.mrc",
            "models_data": "",
            "associated_data": ["InitialModel/job015/run_it090_class001.mrc"],
            "start_collapsed": False,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }
        assert dispobjs[0].__dict__ == expected


if __name__ == "__main__":
    unittest.main()
