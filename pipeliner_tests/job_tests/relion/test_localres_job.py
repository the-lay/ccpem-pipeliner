#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import json

from pipeliner_tests import test_data
from pipeliner.pipeliner_job import ExternalProgram
from pipeliner.project_graph import ProjectGraph
from pipeliner_tests.generic_tests import (
    general_get_command_test,
    tutorial_data_available,
    get_relion_tutorial_data,
)
from pipeliner.utils import touch
from pipeliner.jobs.relion.LocalRes import make_halfmap_symlinks

resmap = ExternalProgram("resmap")


class LocalResTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)
        # make some dirs for testing sym link making
        os.mkdir("Refine3D")
        os.mkdir("Refine3D/job029")
        shutil.copy(
            os.path.join(self.test_data, "single.mrc"),
            "Refine3D/job029/run_half1_class001_unfil.mrc",
        )
        shutil.copy(
            os.path.join(self.test_data, "single.mrc"),
            "Refine3D/job029/run_half2_class001_unfil.mrc",
        )
        os.mkdir("LocalRes")
        os.mkdir("LocalRes/job014")

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_relion_localres(self):
        general_get_command_test(
            jobtype="LocalRes",
            jobfile="localres_relion.job",
            jobnumber=14,
            input_nodes={
                "Refine3D/job029/run_half1_class001_unfil.mrc": "DensityMap.mrc.halfmap"
            },
            output_nodes={
                "histogram.pdf": "LogFile.pdf.relion.localres",
                "relion_locres_filtered.mrc": "DensityMap.mrc.relion.localresfiltered",
                "relion_locres.mrc": "Image3D.mrc.relion.localresmap",
            },
            expected_commands=[
                "mpirun -n 12 relion_postprocess_mpi --locres --i "
                "Refine3D/job029/run_half1_class001_unfil.mrc --o "
                "LocalRes/job014/relion --angpix 1.244 --adhoc_bfac -30"
                " --pipeline_control LocalRes/job014/"
            ],
        )

    def test_relion_localres_jobstar(self):
        general_get_command_test(
            jobtype="LocalRes",
            jobfile="localres_relion_job.star",
            jobnumber=14,
            input_nodes={
                "Refine3D/job029/run_half1_class001_unfil.mrc": "DensityMap.mrc.halfmap"
            },
            output_nodes={
                "histogram.pdf": "LogFile.pdf.relion.localres",
                "relion_locres_filtered.mrc": "DensityMap.mrc.relion.localresfiltered",
                "relion_locres.mrc": "Image3D.mrc.relion.localresmap",
            },
            expected_commands=[
                "mpirun -n 12 relion_postprocess_mpi --locres --i "
                "Refine3D/job029/run_half1_class001_unfil.mrc --o "
                "LocalRes/job014/relion --angpix 1.244 --adhoc_bfac -30"
                " --pipeline_control LocalRes/job014/"
            ],
        )

    def test_relion_localres_mtf(self):
        general_get_command_test(
            jobtype="LocalRes",
            jobfile="localres_relion_mtf.job",
            jobnumber=14,
            input_nodes={
                "Refine3D/job029/run_half1_class001_unfil.mrc": "DensityMap.mrc."
                "halfmap",
                "my_MTF_file.star": "MicroscopeData.star.mtf",
            },
            output_nodes={
                "histogram.pdf": "LogFile.pdf.relion.localres",
                "relion_locres_filtered.mrc": "DensityMap.mrc.relion.localresfiltered",
                "relion_locres.mrc": "Image3D.mrc.relion.localresmap",
            },
            expected_commands=[
                "mpirun -n 12 relion_postprocess_mpi --locres --i "
                "Refine3D/job029/run_half1_class001_unfil.mrc --o "
                "LocalRes/job014/relion --angpix 1.244 --adhoc_bfac -30"
                " --mtf my_MTF_file.star --pipeline_control LocalRes/job014/"
            ],
        )

    def test_relion_localres_mtf_relionstyle_name(self):
        """Test Automatic concersion of ambiguous relion style job name"""
        general_get_command_test(
            jobtype="LocalRes",
            jobfile="localres_relion_mtf_relionstyle.job",
            jobnumber=14,
            input_nodes={
                "Refine3D/job029/run_half1_class001_unfil.mrc": "DensityMap.mrc."
                "halfmap",
                "my_MTF_file.star": "MicroscopeData.star.mtf",
            },
            output_nodes={
                "histogram.pdf": "LogFile.pdf.relion.localres",
                "relion_locres_filtered.mrc": "DensityMap.mrc.relion.localresfiltered",
                "relion_locres.mrc": "Image3D.mrc.relion.localresmap",
            },
            expected_commands=[
                "mpirun -n 12 relion_postprocess_mpi --locres --i "
                "Refine3D/job029/run_half1_class001_unfil.mrc --o "
                "LocalRes/job014/relion --angpix 1.244 --adhoc_bfac -30"
                " --mtf my_MTF_file.star --pipeline_control LocalRes/job014/"
            ],
        )

    def test_relion_localres_nompi(self):
        general_get_command_test(
            jobtype="LocalRes",
            jobfile="localres_relion_nompi.job",
            jobnumber=14,
            input_nodes={
                "Refine3D/job029/run_half1_class001_unfil.mrc": "DensityMap.mrc.halfmap"
            },
            output_nodes={
                "histogram.pdf": "LogFile.pdf.relion.localres",
                "relion_locres_filtered.mrc": "DensityMap.mrc.relion.localresfiltered",
                "relion_locres.mrc": "Image3D.mrc.relion.localresmap",
            },
            expected_commands=[
                "relion_postprocess --locres --i "
                "Refine3D/job029/run_half1_class001_unfil.mrc --o "
                "LocalRes/job014/relion --angpix 1.244 --adhoc_bfac -30"
                " --pipeline_control LocalRes/job014/"
            ],
        )

    def test_resmap_localres(self):
        # have to copy in actual files because symlinks are made
        general_get_command_test(
            jobtype="LocalRes",
            jobfile="localres_resmap.job",
            jobnumber=14,
            input_nodes={
                "Refine3D/job029/run_half1_class001_unfil.mrc": "DensityMap.mrc."
                "halfmap",
                "MaskCreate/job400/mask.mrc": "Mask3D.mrc",
            },
            output_nodes={"half1_resmap.mrc": "Image3D.mrc.resmap.localresmap"},
            expected_commands=[
                f"{make_halfmap_symlinks.__file__} "
                "--hm1 Refine3D/job029/run_half1_class001_unfil.mrc --hm2 "
                "Refine3D/job029/run_half2_class001_unfil.mrc --output_dir "
                "LocalRes/job014/",
                f"{resmap.exe_path} --maskVol=MaskCreate/job400/mask.mrc --noguiSplit "
                "LocalRes/job014/half1.mrc LocalRes/job014/half2.mrc --vxSize=1.244 "
                "--pVal=0.05 --minRes=10 --maxRes=2.5 --stepRes=1",
            ],
        )

    def test_resmap_localres_relionstyle_jobname(self):
        """test automatic conversion of ambiguous relion style jobname"""
        general_get_command_test(
            jobtype="LocalRes",
            jobfile="localres_resmap_relionstyle.job",
            jobnumber=14,
            input_nodes={
                "Refine3D/job029/run_half1_class001_unfil.mrc": "DensityMap.mrc."
                "halfmap",
                "MaskCreate/job400/mask.mrc": "Mask3D.mrc",
            },
            output_nodes={"half1_resmap.mrc": "Image3D.mrc.resmap.localresmap"},
            expected_commands=[
                f"{make_halfmap_symlinks.__file__} "
                "--hm1 Refine3D/job029/run_half1_class001_unfil.mrc --hm2 "
                "Refine3D/job029/run_half2_class001_unfil.mrc --output_dir "
                "LocalRes/job014/",
                f"{resmap.exe_path} --maskVol=MaskCreate/job400/mask.mrc --noguiSplit "
                "LocalRes/job014/half1.mrc LocalRes/job014/half2.mrc --vxSize=1.244 "
                "--pVal=0.05 --minRes=10 --maxRes=2.5 --stepRes=1",
            ],
        )

    @unittest.skipUnless(tutorial_data_available(), "Needs Relion tutorial data")
    def test_create_display_locres_relion(self):
        get_relion_tutorial_data("LocalRes")
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("LocalRes/job031/")
        dispobjs = pipeline.get_process_results_display(proc)
        histfile = os.path.join(self.test_data, "ResultsFiles/local_res.json")
        with open(histfile, "r") as hf:
            exp_hist = json.load(hf)
        exp_map = {
            "maps": ["LocalRes/job031/Thumbnails/relion_locres_filtered.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [1.0],
            "models": [],
            "title": "Local resolution filtered map",
            "maps_data": "LocalRes/job031/relion_locres_filtered.mrc",
            "models_data": "",
            "associated_data": ["LocalRes/job031/relion_locres_filtered.mrc"],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }
        assert dispobjs[0].__dict__ == exp_hist
        assert dispobjs[1].__dict__ == exp_map

    def test_script_makes_symlinks_properly(self):
        lrdir = "LocalRes/job002"
        refdir = "Refine3D/job001"
        os.makedirs(lrdir)
        os.makedirs(refdir)
        hm_files = ["hm1.mrc", "hm2.mrc"]
        for f in hm_files:
            touch(os.path.join(refdir, f))
        make_halfmap_symlinks.main(
            [
                "--hm1",
                os.path.join(refdir, "hm1.mrc"),
                "--hm2",
                os.path.join(refdir, "hm2.mrc"),
                "--output_dir",
                lrdir,
            ]
        )
        for n in [1, 2]:
            link = os.path.join(lrdir, f"half{n}.mrc")
            assert os.path.islink(link)
            realpath = os.path.abspath(os.path.join(refdir, f"hm{n}.mrc"))
            assert os.path.realpath(os.path.join(lrdir, f"half{n}.mrc")) == realpath


if __name__ == "__main__":
    unittest.main()
