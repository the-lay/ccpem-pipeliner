#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests import test_data
from pipeliner_tests import generic_tests
from pipeliner.nodes import (
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_MICROGRAPHMOVIEGROUPMETADATA,
    NODE_MICROGRAPHCOORDSGROUP,
    NODE_PARTICLESDATA,
    NODE_DENSITYMAP,
    NODE_MASK3D,
    NODE_LOGFILE,
)


class ExternalJobTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)
        os.mkdir("CtfRefine")
        os.mkdir("CtfRefine/job018")

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_external_movies(self):
        generic_tests.general_get_command_test(
            jobtype="External",
            jobfile="external_movies.job",
            jobnumber=19,
            input_nodes={
                "Import/job001/movies.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star"
                ".relion"
            },
            output_nodes={"run.out": f"{NODE_LOGFILE}.txt"},
            expected_commands=[
                "/path/to/external/here.exe --in_movies Import/job001/movies.star "
                "--o External/job019/ --test_param1 1001 --j 16"
            ],
        )

    def test_get_command_external_parts(self):
        generic_tests.general_get_command_test(
            jobtype="External",
            jobfile="external_parts.job",
            jobnumber=19,
            input_nodes={
                "Import/job001/particles.star": f"{NODE_PARTICLESDATA}.star.relion"
            },
            output_nodes={"run.out": f"{NODE_LOGFILE}.txt"},
            expected_commands=[
                "/path/to/external/here.exe --in_parts Import/job001/particles.star "
                "--o External/job019/ --test_param1 1001 --j 16"
            ],
        )

    def test_get_command_external_3dref(self):
        generic_tests.general_get_command_test(
            jobtype="External",
            jobfile="external_3dref.job",
            jobnumber=19,
            input_nodes={"Import/job001/3dref.mrc": f"{NODE_DENSITYMAP}.mrc"},
            output_nodes={"run.out": f"{NODE_LOGFILE}.txt"},
            expected_commands=[
                "/path/to/external/here.exe --in_3Dref Import/job001/3dref.mrc "
                "--o External/job019/ --test_param1 1001 --j 16"
            ],
        )

    def test_get_command_external_coords(self):
        generic_tests.general_get_command_test(
            jobtype="External",
            jobfile="external_coords.job",
            jobnumber=19,
            input_nodes={
                "Import/job001/coords.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion"
            },
            output_nodes={"run.out": f"{NODE_LOGFILE}.txt"},
            expected_commands=[
                "/path/to/external/here.exe --in_coords Import/job001/coords.star "
                "--o External/job019/ --test_param1 1001 --j 16"
            ],
        )

    def test_get_command_external_mask(self):
        generic_tests.general_get_command_test(
            jobtype="External",
            jobfile="external_mask.job",
            jobnumber=19,
            input_nodes={"Import/job001/mask.mrc": f"{NODE_MASK3D}.mrc"},
            output_nodes={"run.out": f"{NODE_LOGFILE}.txt"},
            expected_commands=[
                "/path/to/external/here.exe --in_mask Import/job001/mask.mrc "
                "--o External/job019/ --test_param1 1001 --j 16"
            ],
        )

    def test_get_command_external_allin(self):
        generic_tests.general_get_command_test(
            jobtype="External",
            jobfile="external_allin.job",
            jobnumber=19,
            input_nodes={
                "Import/job001/mics.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star."
                "relion",
                "Import/job001/movies.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star"
                ".relion",
                "Import/job001/coords.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star."
                "relion",
                "Import/job001/parts.star": f"{NODE_PARTICLESDATA}.star.relion",
                "Import/job001/3dref.mrc": f"{NODE_DENSITYMAP}.mrc",
            },
            output_nodes={"run.out": f"{NODE_LOGFILE}.txt"},
            expected_commands=[
                "/path/to/external/here.exe --in_mics Import/job001/mics.star"
                " --in_movies Import/job001/movies.star --in_coords "
                "Import/job001/coords.star --in_parts Import/job001/parts.star"
                " --in_3Dref Import/job001/3dref.mrc "
                "--o External/job019/ --test_param1 1001 --j 16"
            ],
        )

    def test_get_command_external_allparams(self):
        generic_tests.general_get_command_test(
            jobtype="External",
            jobfile="external_all_params.job",
            jobnumber=19,
            input_nodes={"Import/job001/3dref.mrc": f"{NODE_DENSITYMAP}.mrc"},
            output_nodes={"run.out": f"{NODE_LOGFILE}.txt"},
            expected_commands=[
                "/path/to/external/here.exe --in_3Dref Import/job001/3dref.mrc "
                "--o External/job019/ --test_param1 1001"
                " --test_param2 2002 --test_param3 3003 --test_param4 4004"
                " --test_param5 5005 --test_param6 6006 --test_param7 7007"
                " --test_param8 8008 --test_param9 9009 --test_param10 1010 "
                "--j 16"
            ],
        )

    def test_get_command_external_allparams_jobstar(self):
        generic_tests.general_get_command_test(
            jobtype="External",
            jobfile="external_job.star",
            jobnumber=19,
            input_nodes={"Import/job001/3dref.mrc": f"{NODE_DENSITYMAP}.mrc"},
            output_nodes={"run.out": f"{NODE_LOGFILE}.txt"},
            expected_commands=[
                "/path/to/external/here.exe --in_3Dref Import/job001/3dref.mrc "
                "--o External/job019/ --test_param1 1001"
                " --test_param2 2002 --test_param3 3003 --test_param4 4004"
                " --test_param5 5005 --test_param6 6006 --test_param7 7007"
                " --test_param8 8008 --test_param9 9009 --test_param10 1010 --j "
                "16"
            ],
        )

    def test_get_command_external_parts_output(self):
        generic_tests.general_get_command_test(
            jobtype="External",
            jobfile="external_parts_out.job",
            jobnumber=19,
            input_nodes={
                "Import/job001/particles.star": f"{NODE_PARTICLESDATA}.star.relion"
            },
            output_nodes={
                "parts_out.star": f"{NODE_PARTICLESDATA}.star.kwd1.kwd2.kwd3"
            },
            expected_commands=[
                "/path/to/external/here.exe --in_parts Import/job001/particles.star "
                "--o External/job019/ --test_param1 1001 --j 16"
            ],
        )

    def test_get_command_external_mask_output(self):
        job = generic_tests.general_get_command_test(
            jobtype="External",
            jobfile="external_mask_out.job",
            jobnumber=19,
            input_nodes={"Import/job001/mask.mrc": f"{NODE_MASK3D}.mrc"},
            output_nodes={"mask_out.mrc": f"{NODE_MASK3D}.mrc"},
            expected_commands=[
                "/path/to/external/here.exe --in_mask Import/job001/mask.mrc "
                "--o External/job019/ --test_param1 1001 --j 16"
            ],
        )

        outnode = job.output_nodes[0]
        assert outnode.name == "External/job019/mask_out.mrc"
        assert outnode.type == f"{NODE_MASK3D}.mrc"

    def test_get_command_external_3dref_output(self):
        job = generic_tests.general_get_command_test(
            jobtype="External",
            jobfile="external_3dref_out.job",
            jobnumber=19,
            input_nodes={"Import/job001/3dref.mrc": f"{NODE_DENSITYMAP}.mrc"},
            output_nodes={"3dref_out.mrc": f"{NODE_DENSITYMAP}.mrc"},
            expected_commands=[
                "/path/to/external/here.exe --in_3Dref Import/job001/3dref.mrc "
                "--o External/job019/ --test_param1 1001 --j 16"
            ],
        )

        outnode = job.output_nodes[0]
        assert outnode.name == "External/job019/3dref_out.mrc"
        assert outnode.type == f"{NODE_DENSITYMAP}.mrc"

    def test_get_command_external_other_output(self):
        job = generic_tests.general_get_command_test(
            jobtype="External",
            jobfile="external_otherout.job",
            jobnumber=19,
            input_nodes={
                "Import/job001/movies.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star"
                ".relion"
            },
            output_nodes={"strange_file.xyz": "StrangeNodeType.xyz"},
            expected_commands=[
                "/path/to/external/here.exe --in_movies Import/job001/movies.star "
                "--o External/job019/ --test_param1 1001 --j 16"
            ],
        )

        outnode = job.output_nodes[0]
        assert outnode.name == "External/job019/strange_file.xyz"
        assert outnode.type == "StrangeNodeType.xyz"

    def test_get_command_external_other_output_no_type_entered(self):
        job = generic_tests.general_get_command_test(
            jobtype="External",
            jobfile="external_noout.job",
            jobnumber=19,
            input_nodes={
                "Import/job001/movies.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star"
                ".relion"
            },
            output_nodes={"strange_file.xyz": "TestNode.xyz"},
            expected_commands=[
                "/path/to/external/here.exe --in_movies Import/job001/movies.star "
                "--o External/job019/ --test_param1 1001 --j 16"
            ],
        )

        outnode = job.output_nodes[0]
        assert outnode.name == "External/job019/strange_file.xyz"
        assert outnode.type == "TestNode.xyz"

    def test_get_command_external_3dref_nothreads(self):
        generic_tests.general_get_command_test(
            jobtype="External",
            jobfile="external_3dref_nothreads.job",
            jobnumber=19,
            input_nodes={"Import/job001/3dref.mrc": f"{NODE_DENSITYMAP}.mrc"},
            output_nodes={"run.out": f"{NODE_LOGFILE}.txt"},
            expected_commands=[
                "/path/to/external/here.exe --in_3Dref Import/job001/3dref.mrc "
                "--o External/job019/ --test_param1 1001"
            ],
        )


if __name__ == "__main__":
    unittest.main()
