#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests import test_data
from pipeliner_tests.generic_tests import general_get_command_test
from pipeliner.jobs.relion.subtract_job import RelionSubtract
from pipeliner.utils import touch


class SubtractTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_subtract_commands(self):
        general_get_command_test(
            jobtype="Subtract",
            jobfile="subtract.job",
            jobnumber=4,
            input_nodes={
                "Refine3D/job500/run_it017_optimiser.star": "ProcessData.star.relion"
                ".optimiser",
                "MaskCreate/job501/mask.mrc": "Mask3D.mrc",
                "myother_particles.star": "ParticlesData.star.relion",
            },
            output_nodes={
                "particles_subtracted.star": "ParticlesData.star.relion.subtracted"
            },
            expected_commands=[
                "mpirun -n 16 relion_particle_subtract_mpi --i "
                "Refine3D/job500/run_it017_optimiser.star --mask "
                "MaskCreate/job501/mask.mrc --o Subtract/job004/ --new_box 420"
                " --pipeline_control Subtract/job004/"
            ],
        )

    def test_get_subtract_commands_relionstyle_name(self):
        """Check correct interpretation of ambiguous jobname"""
        general_get_command_test(
            jobtype="Subtract",
            jobfile="subtract_relionstyle.job",
            jobnumber=4,
            input_nodes={
                "Refine3D/job500/run_it017_optimiser.star": "ProcessData.star.relion."
                "optimiser",
                "MaskCreate/job501/mask.mrc": "Mask3D.mrc",
                "myother_particles.star": "ParticlesData.star.relion",
            },
            output_nodes={
                "particles_subtracted.star": "ParticlesData.star.relion.subtracted"
            },
            expected_commands=[
                "mpirun -n 16 relion_particle_subtract_mpi --i "
                "Refine3D/job500/run_it017_optimiser.star --mask "
                "MaskCreate/job501/mask.mrc --o Subtract/job004/ --new_box 420"
                " --pipeline_control Subtract/job004/"
            ],
        )

    def test_get_subtract_commands_diff_parts(self):
        general_get_command_test(
            jobtype="Subtract",
            jobfile="subtract_diff_parts.job",
            jobnumber=4,
            input_nodes={
                "Refine3D/job500/run_it017_optimiser.star": "ProcessData.star."
                "relion.optimiser",
                "MaskCreate/job501/mask.mrc": "Mask3D.mrc",
                "myother_particles.star": "ParticlesData.star.relion",
            },
            output_nodes={
                "particles_subtracted.star": "ParticlesData.star.relion.subtracted"
            },
            expected_commands=[
                "mpirun -n 16 relion_particle_subtract_mpi --i "
                "Refine3D/job500/run_it017_optimiser.star --mask "
                "MaskCreate/job501/mask.mrc --data myother_particles.star"
                " --o Subtract/job004/ --new_box 420 --pipeline_control Subtract/job00"
                "4/"
            ],
        )

    def test_mpi_default_value_is_2(self):
        job = RelionSubtract()
        assert job.joboptions["nr_mpi"].value == 2
        assert job.joboptions["nr_mpi"].default_value == 2

        touch("test_opt.star")
        job.joboptions["fn_opt"].value = "test_opt.star"
        exp_com = [
            [
                "mpirun",
                "-n",
                "2",
                "relion_particle_subtract_mpi",
                "--i",
                "test_opt.star",
                "--o",
                "",
                "--recenter_on_mask",
                "--new_box",
                "-1",
            ]
        ]
        assert job.get_commands() == exp_com

    def test_error_running_with_less_than_2_mpi(self):
        job = RelionSubtract()
        job.joboptions["nr_mpi"].value = 1
        with self.assertRaises(ValueError):
            job.get_commands()

    def test_get_subtract_commands_revert(self):
        os.mkdir("Subtract")
        os.mkdir("Subtract/job004")
        general_get_command_test(
            jobtype="Subtract",
            jobfile="subtract_revert.job",
            jobnumber=4,
            input_nodes={"these_were_original.star": "ParticlesData.star.relion"},
            output_nodes={"original.star": "ParticlesData.star.relion"},
            expected_commands=[
                "relion_particle_subtract --revert these_were_original.star"
                " --o Subtract/job004/ --pipeline_control Subtract/job004/"
            ],
        )

    def test_get_subtract_commands_revert_relionstyle_name(self):
        """Test conversion of relion style ambiguous job name"""
        os.mkdir("Subtract")
        os.mkdir("Subtract/job004")
        general_get_command_test(
            jobtype="Subtract",
            jobfile="subtract_revert_relionstyle_job.star",
            jobnumber=4,
            input_nodes={"Refine3D/job010/run_data.star": "ParticlesData.star.relion"},
            output_nodes={"original.star": "ParticlesData.star.relion"},
            expected_commands=[
                "relion_particle_subtract --revert Refine3D/job010/run_data.star"
                " --o Subtract/job004/ --pipeline_control Subtract/job004/"
            ],
        )

    def test_get_subtract_commands_centonmask(self):
        general_get_command_test(
            jobtype="Subtract",
            jobfile="subtract_centonmask.job",
            jobnumber=4,
            input_nodes={
                "Refine3D/job500/run_it017_optimiser.star": "ProcessData.star."
                "relion.optimiser",
                "MaskCreate/job501/mask.mrc": "Mask3D.mrc",
                "myother_particles.star": "ParticlesData.star.relion",
            },
            output_nodes={
                "particles_subtracted.star": "ParticlesData.star.relion.subtracted"
            },
            expected_commands=[
                "mpirun -n 16 relion_particle_subtract_mpi --i "
                "Refine3D/job500/run_it017_optimiser.star --mask "
                "MaskCreate/job501/mask.mrc --o Subtract/job004/ "
                "--recenter_on_mask --new_box 420 --pipeline_control Subtract/job004/"
            ],
        )

    def test_get_subtract_commands_centoncoords(self):
        general_get_command_test(
            jobtype="Subtract",
            jobfile="subtract_centoncoords.job",
            jobnumber=4,
            input_nodes={
                "Refine3D/job500/run_it017_optimiser.star": "ProcessData.star."
                "relion.optimiser",
                "MaskCreate/job501/mask.mrc": "Mask3D.mrc",
                "myother_particles.star": "ParticlesData.star.relion",
            },
            output_nodes={
                "particles_subtracted.star": "ParticlesData.star.relion.subtracted"
            },
            expected_commands=[
                "mpirun -n 16 relion_particle_subtract_mpi --i "
                "Refine3D/job500/run_it017_optimiser.star --mask "
                "MaskCreate/job501/mask.mrc --o Subtract/job004/ "
                "--center_x 101 --center_y 102 --center_z 103 --new_box 420"
                " --pipeline_control Subtract/job004/"
            ],
        )

    def test_get_subtract_commands_additional_arg(self):
        general_get_command_test(
            jobtype="Subtract",
            jobfile="subtract_addarg.job",
            jobnumber=4,
            input_nodes={
                "Refine3D/job500/run_it017_optimiser.star": "ProcessData.star."
                "relion.optimiser",
                "MaskCreate/job501/mask.mrc": "Mask3D.mrc",
                "myother_particles.star": "ParticlesData.star.relion",
            },
            output_nodes={
                "particles_subtracted.star": "ParticlesData.star.relion.subtracted"
            },
            expected_commands=[
                "mpirun -n 16 relion_particle_subtract_mpi --i "
                "Refine3D/job500/run_it017_optimiser.star --mask "
                "MaskCreate/job501/mask.mrc --o Subtract/job004/ --new_box 420 "
                "--here_is_an_additional_arg --pipeline_control Subtract/job004/"
            ],
        )

    def test_get_subtract_commands_additional_arg_jobstar(self):
        general_get_command_test(
            jobtype="Subtract",
            jobfile="subtract_addarg_job.star",
            jobnumber=4,
            input_nodes={
                "Refine3D/job500/run_it017_optimiser.star": "ProcessData.star."
                "relion.optimiser",
                "MaskCreate/job501/mask.mrc": "Mask3D.mrc",
            },
            output_nodes={
                "particles_subtracted.star": "ParticlesData.star.relion.subtracted"
            },
            expected_commands=[
                "mpirun -n 16 relion_particle_subtract_mpi --i "
                "Refine3D/job500/run_it017_optimiser.star --mask "
                "MaskCreate/job501/mask.mrc --o Subtract/job004/ --new_box 420 "
                "--here_is_an_additional_arg --pipeline_control Subtract/job004/"
            ],
        )


if __name__ == "__main__":
    unittest.main()
