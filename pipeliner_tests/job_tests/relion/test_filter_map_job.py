#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner.nodes import NODE_DENSITYMAP
from pipeliner_tests import test_data
from pipeliner_tests.job_testing_tools import (
    job_generate_commands_test,
    job_display_object_generation_test,
)
from pipeliner.results_display_objects import ResultsDisplayMapModel


class FilterJobTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)

        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_filter_advanced_both_directional(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/FilterMap/filter_adv_both_dir_job.star"
            ),
            input_nodes={"test.mrc": NODE_DENSITYMAP + ".mrc"},
            output_nodes={"test_filt.mrc": NODE_DENSITYMAP + ".mrc.filtered"},
            expected_commands=[
                "relion_image_handler --i test.mrc --lowpass 12.0 --highpass 5.0 "
                "--filter_edge_width 2 --directional X --o "
                "FilterMap/job999/test_filt.mrc --pipeline_control FilterMap/job999/"
            ],
        )

    def test_filter_advanced_both(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/FilterMap/filter_adv_both_job.star"
            ),
            input_nodes={"test.mrc": NODE_DENSITYMAP + ".mrc"},
            output_nodes={"test_filt.mrc": NODE_DENSITYMAP + ".mrc.filtered"},
            expected_commands=[
                "relion_image_handler --i test.mrc --lowpass 12.0 --highpass 5.0 "
                "--filter_edge_width 2 --o FilterMap/job999/test_filt.mrc "
                "--pipeline_control FilterMap/job999/"
            ],
        )

    def test_filter_advanced_high(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/FilterMap/filter_adv_high_job.star"
            ),
            input_nodes={"test.mrc": NODE_DENSITYMAP + ".mrc"},
            output_nodes={"test_filt.mrc": NODE_DENSITYMAP + ".mrc.filtered"},
            expected_commands=[
                "relion_image_handler --i test.mrc --highpass 5.0 "
                "--filter_edge_width 2 --o FilterMap/job999/test_filt.mrc "
                "--pipeline_control FilterMap/job999/"
            ],
        )

    def test_filter_both(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/FilterMap/filter_both_job.star"
            ),
            input_nodes={"test.mrc": NODE_DENSITYMAP + ".mrc"},
            output_nodes={"test_filt.mrc": NODE_DENSITYMAP + ".mrc.filtered"},
            expected_commands=[
                "relion_image_handler --i test.mrc --lowpass 12.0 --highpass 5.0 "
                "--o FilterMap/job999/test_filt.mrc --pipeline_control FilterMap/"
                "job999/"
            ],
        )

    def test_filter_high(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/FilterMap/filter_high_job.star"
            ),
            input_nodes={"test.mrc": NODE_DENSITYMAP + ".mrc"},
            output_nodes={"test_filt.mrc": NODE_DENSITYMAP + ".mrc.filtered"},
            expected_commands=[
                "relion_image_handler --i test.mrc --highpass 5.0 "
                "--o FilterMap/job999/test_filt.mrc --pipeline_control FilterMap/"
                "job999/"
            ],
        )

    def test_filter_low(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/FilterMap/filter_low_job.star"
            ),
            input_nodes={"test.mrc": NODE_DENSITYMAP + ".mrc"},
            output_nodes={"test_filt.mrc": NODE_DENSITYMAP + ".mrc.filtered"},
            expected_commands=[
                "relion_image_handler --i test.mrc --lowpass 12.0 "
                "--o FilterMap/job999/test_filt.mrc --pipeline_control FilterMap/"
                "job999/"
            ],
        )

    def test_filter_none(self):
        with self.assertRaises(ValueError):
            job_generate_commands_test(
                jobfile=os.path.join(
                    self.test_data, "JobFiles/FilterMap/filter_none_job.star"
                ),
                input_nodes={},
                output_nodes={},
                expected_commands=[],
            )

    def test_creating_results_display(self):
        mrc_file = os.path.join(self.test_data, "1ake_10A_molrep.mrc")
        expected_rdo = ResultsDisplayMapModel(
            title=(
                "Filtered map Low-pass filter 12.0 \u212B High-pass filter 5.0 \u212B"
            ),
            start_collapsed=False,
            associated_data=["FilterMap/job999/test_filt.mrc"],
            maps=["FilterMap/job999/Thumbnails/test_filt.mrc"],
            maps_data="FilterMap/job999/test_filt.mrc",
        )
        job_display_object_generation_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/FilterMap/filter_both_job.star"
            ),
            expected_display_objects=[(expected_rdo, None)],
            files_to_create={"test_filt.mrc": mrc_file},
        )


if __name__ == "__main__":
    unittest.main()
