#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests import test_data
from pipeliner_tests import generic_tests

from pipeliner.job_factory import active_job_from_proc

from pipeliner.jobs.ccpem.em_placement import em_placement_config
from pipeliner_tests.job_testing_tools import (
    job_running_test,
    job_generate_commands_test,
)

do_full = generic_tests.do_slow_tests()
skip_live_tests = True if shutil.which("phenix") is None else False


class EMPlacementTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="em_placement")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_em_placement(self):
        config_script = em_placement_config.__file__
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/EMPlacement/em_placement_fit_model_job.star"
            ),
            input_nodes={
                "Import/job001/emd_3488.mrc": "DensityMap.mrc",
                "Import/job001/3488_run_half1_class001_unfil.mrc": "DensityMap.mrc",
                "Import/job001/3488_run_half2_class001_unfil.mrc": "DensityMap.mrc",
                "Import/job002/5me2_a_to_move.pdb": "AtomCoords.pdb",
                "Import/job003/3488_5me2.fasta": "Sequence.fasta",
            },
            output_nodes={"run.out": "LogFile.txt"},
            expected_commands=[
                f"{config_script} ../../Import/job001/emd_3488.mrc "
                "../../Import/job001/3488_run_half1_class001_unfil.mrc "
                "../../Import/job001/3488_run_half2_class001_unfil.mrc 3.2 None "
                "../../Import/job003/3488_5me2.fasta 5me2_a_to_move "
                "../../Import/job002/5me2_a_to_move.pdb 1.5 em_placement.phil",
                "phenix.voyager.em_placement em_placement.phil",
            ],
        )

    @unittest.skipUnless(
        do_full and not skip_live_tests,
        "Slow test: Only runs in full unittest",
    )
    def test_em_placement_fit_model(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/EMPlacement/em_placement_fit_model_job.star"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "emd_3488.mrc"),
                ),
                (
                    "Import/job001",
                    os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
                ),
                (
                    "Import/job001",
                    os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
                ),
                (
                    "Import/job002",
                    os.path.join(self.test_data, "5me2_a_to_move.pdb"),
                ),
                (
                    "Import/job003",
                    os.path.join(self.test_data, "3488_5me2.fasta"),
                ),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "em_placement.phil",
                "emplaced_files/docked_model1.pdb",
                "emplaced_files/docked_model2.pdb",
            ],
            print_err=True,
            show_contents=True,
        )

        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        assert dispobjs[0].__dict__ == {
            "maps": ["EMPlacement/job998/Thumbnails/emd_3488.mrc"],
            "maps_opacity": [0.5],
            "models": [
                "Import/job002/5me2_a_to_move.pdb",
                "EMPlacement/job998/emplaced_files/docked_model1.pdb",
                "EMPlacement/job998/emplaced_files/docked_model2.pdb",
            ],
            "title": "em_placement fitted models",
            "maps_data": "Import/job001/emd_3488.mrc",
            "models_data": "Import/job002/5me2_a_to_move.pdb, "
            "EMPlacement/job998/emplaced_files/docked_model1.pdb, "
            "EMPlacement/job998/emplaced_files/docked_model2.pdb",
            "associated_data": [
                "Import/job001/emd_3488.mrc",
                "Import/job002/5me2_a_to_move.pdb",
                "EMPlacement/job998/emplaced_files/docked_model1.pdb",
                "EMPlacement/job998/emplaced_files/docked_model2.pdb",
            ],
            "maps_colours": [],
            "models_colours": [],
        }


if __name__ == "__main__":
    unittest.main()
