#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests import test_data
from pipeliner_tests import generic_tests
from pipeliner.job_factory import active_job_from_proc

from pipeliner_tests.job_testing_tools import (
    job_running_test,
    job_generate_commands_test,
)

do_full = generic_tests.do_slow_tests()
skip_live_tests = True if shutil.which("servalcat") is None else False


class RefmacServalcatTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="refmac-servalcat")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/Refmac_Servalcat/refine.job"
            ),
            input_nodes={
                "Import/job001/3488_run_half1_class001_unfil.mrc": "DensityMap.mrc",
                "Import/job001/3488_run_half2_class001_unfil.mrc": "DensityMap.mrc",
                "Import/job002/5me2_a.pdb": "AtomCoords.pdb",
            },
            output_nodes={"refined.pdb": "AtomCoords.pdb.refmac_servalcat.refined"},
            expected_commands=[
                "servalcat refine_spa --show_refmac_log --output_prefix "
                "refined --halfmaps "
                "../../Import/job001/3488_run_half1_class001_unfil.mrc "
                "../../Import/job001/3488_run_half2_class001_unfil.mrc --model "
                "../../Import/job002/5me2_a.pdb --resolution 3.2 --ncycle 3 "
                "--mask_radius 3.0 --bfactor 40.0 --ncsr local --jellybody "
                "--jellybody_params 0.02 4.2"
            ],
        )

    def test_get_command_whole_map(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/Refmac_Servalcat/refine_whole_map.job"
            ),
            input_nodes={
                "Import/job001/emd_3488.mrc": "DensityMap.mrc",
                "Import/job002/5me2_a.pdb": "AtomCoords.pdb",
            },
            output_nodes={"refined.pdb": "AtomCoords.pdb.refmac_servalcat.refined"},
            expected_commands=[
                "servalcat refine_spa --show_refmac_log --output_prefix "
                "refined --map "
                "../../Import/job001/emd_3488.mrc "
                "--model ../../Import/job002/5me2_a.pdb --resolution 3.2 --ncycle 3 "
                "--mask_radius 3.0 --bfactor 40.0 --ncsr local --jellybody "
                "--jellybody_params 0.02 4.2"
            ],
        )

    @unittest.skipUnless(
        do_full and not skip_live_tests, "Slow test: Only runs in full unittest"
    )
    def test_hb_refinement_half_maps(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/Refmac_Servalcat/refine.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
                ),
                (
                    "Import/job001",
                    os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
                ),
                ("Import/job002", os.path.join(self.test_data, "5me2_a.pdb")),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "refined.pdb",
                "diffmap.mtz",
                "servalcat.log",
                "diffmap_Fstats.log",
                "refined_fsc.json",
                "diffmap.mrc",
            ],
            print_err=True,
            show_contents=True,
        )

        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        assert dispobjs[0].__dict__ == {
            "maps": ["RefmacServalcat/job998/Thumbnails/diffmap.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [0.5],
            "models": ["RefmacServalcat/job998/refined.pdb"],
            "title": "Overlaid map: RefmacServalcat/job998/diffmap.mrc model: "
            "RefmacServalcat/job998/refined.pdb",
            "models_data": "RefmacServalcat/job998/refined.pdb",
            "maps_data": "RefmacServalcat/job998/diffmap.mrc",
            "associated_data": [
                "RefmacServalcat/job998/diffmap.mrc",
                "RefmacServalcat/job998/refined.pdb",
            ],
            "start_collapsed": False,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }

    @unittest.skipUnless(
        do_full and not skip_live_tests, "Slow test: Only runs in full unittest"
    )
    def test_hb_refinement_whole_map(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/Refmac_Servalcat/refine_whole_map.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "emd_3488.mrc"),
                ),
                ("Import/job002", os.path.join(self.test_data, "5me2_a.pdb")),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "refined.pdb",
                "diffmap.mtz",
                "servalcat.log",
                "diffmap_Fstats.log",
                "refined_fsc.json",
            ],
            print_err=True,
            show_contents=True,
        )

        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        assert dispobjs[0].__dict__ == {
            "title": "Overlaid map: Import/job001/emd_3488.mrc model: "
            "RefmacServalcat/job998/refined.pdb",
            "start_collapsed": False,
            "dobj_type": "mapmodel",
            "flag": "",
            "maps": ["RefmacServalcat/job998/Thumbnails/emd_3488.mrc"],
            "maps_opacity": [0.5],
            "models": ["RefmacServalcat/job998/refined.pdb"],
            "maps_data": "Import/job001/emd_3488.mrc",
            "models_data": "RefmacServalcat/job998/refined.pdb",
            "associated_data": [
                "Import/job001/emd_3488.mrc",
                "RefmacServalcat/job998/refined.pdb",
            ],
            "maps_colours": [],
            "models_colours": [],
        }


if __name__ == "__main__":
    unittest.main()
