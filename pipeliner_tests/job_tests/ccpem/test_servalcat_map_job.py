#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests import test_data
from pipeliner_tests import generic_tests
from pipeliner.job_factory import active_job_from_proc

from pipeliner_tests.job_testing_tools import (
    job_running_test,
    job_generate_commands_test,
)

do_full = generic_tests.do_slow_tests()
skip_live_tests = True if shutil.which("servalcat") is None else False


class ServalcatMapTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="servalcat-map")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/ServalcatMap/difference_map.job"
            ),
            input_nodes={
                "Import/job001/3488_run_half1_class001_unfil.mrc": "DensityMap.mrc",
                "Import/job001/3488_run_half2_class001_unfil.mrc": "DensityMap.mrc",
                "Import/job001/emd_3488_mask.mrc": "Mask3D.mrc",
                "Import/job002/5me2_a.pdb": "AtomCoords.pdb",
            },
            output_nodes={"run.out": "LogFile.txt"},
            expected_commands=[
                "servalcat fofc --model ../../Import/job002/5me2_a.pdb "
                "--halfmaps ../../Import/job001/3488_run_half1_class001_unfil.mrc "
                "../../Import/job001/3488_run_half2_class001_unfil.mrc "
                "--mask ../../Import/job001/emd_3488_mask.mrc "
                "--resolution 3.2 --normalized_map -o diffmap"
            ],
            show_coms=False,
            show_inputnodes=False,
            show_outputnodes=False,
        )

    @unittest.skipUnless(
        do_full and not skip_live_tests, "Slow test: Only runs in full unittest"
    )
    def test_difference_map(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/ServalcatMap/difference_map.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
                ),
                (
                    "Import/job001",
                    os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
                ),
                (
                    "Import/job001",
                    os.path.join(self.test_data, "emd_3488_mask.mrc"),
                ),
                ("Import/job002", os.path.join(self.test_data, "5me2_a.pdb")),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "diffmap.mtz",
                "servalcat.log",
                "diffmap_Fstats.log",
                "diffmap_normalized_fofc.mrc",
            ],
            print_err=True,
            show_contents=True,
        )

        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        assert dispobjs[0].__dict__ == {
            "maps": ["ServalcatMap/job998/Thumbnails/diffmap.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [0.5],
            "models": ["Import/job002/5me2_a.pdb"],
            "title": "FoFc difference map",
            "maps_data": "ServalcatMap/job998/diffmap.mrc",
            "models_data": "Import/job002/5me2_a.pdb",
            "associated_data": [
                "ServalcatMap/job998/diffmap.mrc",
                "Import/job002/5me2_a.pdb",
            ],
            "start_collapsed": False,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }


if __name__ == "__main__":
    unittest.main()
