#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests import test_data
from pipeliner_tests import generic_tests
from pipeliner.job_factory import active_job_from_proc

from pipeliner_tests.job_testing_tools import (
    job_running_test,
    job_generate_commands_test,
)

do_full = generic_tests.do_slow_tests()
skip_live_tests = True if shutil.which("slicendice") is None else False


class SliceNDiceTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="slicendice")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command(self):
        job_generate_commands_test(
            jobfile=os.path.join(self.test_data, "JobFiles/SliceNDice/slicendice.job"),
            input_nodes={
                "Import/job001/"
                "open_fold_7U5C_5_model_1_relaxed_2.pdb": "AtomCoords.pdb",
            },
            output_nodes={
                "slicendice_0/split_1/pdb_open_fold_"
                "7U5C_5_model_1_relaxed_2_cluster_0.pdb": "AtomCoords.pdb.slicendice",
            },
            expected_commands=[
                "slicendice "
                "-xyzin ../../Import/job001/open_fold_7U5C_5_model_1_relaxed_2.pdb "
                "-xyz_source alphafold"
            ],
            show_coms=False,
            show_inputnodes=False,
            show_outputnodes=False,
        )

    @unittest.skipUnless(
        do_full and not skip_live_tests, "Slow test: Only runs in full unittest"
    )
    def test_slice_only(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/SliceNDice/slicendice.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(
                        self.test_data, "open_fold_7U5C_5_model_1_relaxed_2.pdb"
                    ),
                ),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "slicendice_0/input.pdb",
                "slicendice_0/slicendice.log",
                (
                    "slicendice_0/split_1/"
                    "pdb_open_fold_7U5C_5_model_1_relaxed_2_cluster_0.pdb"
                ),
                (
                    "slicendice_0/split_3/"
                    "pdb_open_fold_7U5C_5_model_1_relaxed_2_cluster_2.pdb"
                ),
            ],
        )
        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        test_dic = {
            "maps": [],
            "maps_opacity": [],
            "models": [
                (
                    "SliceNDice/job998/slicendice_0/split_1/"
                    "pdb_open_fold_7U5C_5_model_1_relaxed_2_cluster_0.pdb"
                )
            ],
            "title": "SliceNDice Split 1",
            "models_data": (
                "SliceNDice/job998/slicendice_0/split_1/"
                "pdb_open_fold_7U5C_5_model_1_relaxed_2_cluster_0.pdb"
            ),
            "maps_data": "",
            "associated_data": [
                (
                    "SliceNDice/job998/slicendice_0/split_1/"
                    "pdb_open_fold_7U5C_5_model_1_relaxed_2_cluster_0.pdb"
                )
            ],
            "maps_colours": [],
            "models_colours": [],
        }
        for key in test_dic.keys():
            assert test_dic[key] == dispobjs[-1].__dict__[key]

    def test_slice_and_dice(self):
        """
        Add when dice functionality is available in CCP4 distribution
        """
        pass


if __name__ == "__main__":
    unittest.main()
