#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import json

from pipeliner_tests import generic_tests, test_data
from pipeliner.project_graph import ProjectGraph
from pipeliner.utils import touch
from pipeliner.job_factory import new_job_of_type
from pipeliner.user_settings import get_ccpem_share_dir

do_full = generic_tests.do_slow_tests()
ccpem_share_dir = get_ccpem_share_dir()
skip_live_tests = not shutil.which("proshade")


@unittest.skipUnless(ccpem_share_dir, "$PIPELINER_CCPEM_SHARE_DIR needs to be set")
class ProShadeTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="proshade_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_proshade_plugin_symmetry(self):
        """Doesn't include the output node because it is created by postrun
        actions()"""
        generic_tests.general_get_command_test(
            jobtype="ProShade",
            jobfile="proshade_symmetry_job.star",
            jobnumber=2,
            input_nodes={"test.mrc": "DensityMap.mrc"},
            output_nodes={"run.out": "LogFile.txt"},
            expected_commands=[
                f"proshade --rvpath {ccpem_share_dir} -S -f test.mrc",
                "mv proshade_report ProShade/job002/proshade_report",
            ],
        )

    def test_get_command_proshade_plugin_rebox(self):
        generic_tests.general_get_command_test(
            jobtype="ProShade",
            jobfile="proshade_reboxing_job.star",
            jobnumber=2,
            input_nodes={"test.mrc": "DensityMap.mrc"},
            output_nodes={"reboxed_map.mrc": "DensityMap.mrc.proshade"},
            expected_commands=[
                f"proshade --rvpath {ccpem_share_dir} -E -f test.mrc"
                "  --clearMap  ProShade/job002/reboxed_map.mrc --cellBorderSpace 1.0",
                "mv proshade_report ProShade/job002/proshade_report",
            ],
        )

    def test_get_command_proshade_plugin_overlay(self):
        generic_tests.general_get_command_test(
            jobtype="ProShade",
            jobfile="proshade_overlay_job.star",
            jobnumber=2,
            input_nodes={"test1.mrc": "DensityMap.mrc", "test2.mrc": "DensityMap.mrc"},
            output_nodes={"test2_overlaid.mrc": "DensityMap.mrc.proshade.overlaid"},
            expected_commands=[
                f"proshade --rvpath {ccpem_share_dir} -O -f test1.mrc -f"
                " test2.mrc  --clearMap  ProShade/job002/test2_overlaid.mrc "
                "--cellBorderSpace 20.0",
                "mv proshade_report ProShade/job002/proshade_report",
            ],
        )

    def test_get_command_proshade_plugin_overlay_with_coords(self):
        generic_tests.general_get_command_test(
            jobtype="ProShade",
            jobfile="proshade_overlay_with_coords_job.star",
            jobnumber=3,
            input_nodes={"test1.mrc": "DensityMap.mrc", "test2.pdb": "AtomCoords.pdb"},
            output_nodes={"test2_overlaid.pdb": "AtomCoords.pdb.proshade.overlaid"},
            expected_commands=[
                f"proshade --rvpath {ccpem_share_dir} -O "
                "-f test1.mrc -f test2.pdb  --clearMap  "
                "ProShade/job003/test2_overlaid.pdb",
                "mv proshade_report ProShade/job003/proshade_report",
            ],
        )

    def test_symmetry_writing_displayobjs(self):
        os.makedirs("ProShade/job001")
        touch("test.mrc")
        pipe = os.path.join(self.test_data, "JobFiles/ProShade/proshade_pipeline.star")
        shutil.copy(pipe, "default_pipeline.star")
        log = os.path.join(self.test_data, "JobFiles/ProShade/logfile.txt")
        shutil.copy(log, "ProShade/job001/run.out")
        jobstar = os.path.join(
            self.test_data, "JobFiles/ProShade/proshade_symmetry_job.star"
        )
        shutil.copy(jobstar, "ProShade/job001/job.star")
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("ProShade/job001/")
        dispobjs = pipeline.get_process_results_display(proc)
        axes = os.path.join(self.test_data, "JobFiles/ProShade/axes_results.json")
        elems = os.path.join(self.test_data, "JobFiles/ProShade/elements_results.json")
        alts = os.path.join(
            self.test_data, "JobFiles/ProShade/alternatives_results.json"
        )

        with open(axes, "r") as ax:
            exp_ax = json.load(ax)
        with open(elems, "r") as elm:
            exp_elem = json.load(elm)
        with open(alts, "r") as alt:
            exp_alt = json.load(alt)

        assert dispobjs[0].__dict__ == exp_ax
        assert dispobjs[1].__dict__ == exp_elem
        assert dispobjs[2].__dict__ == exp_alt

    @unittest.skipUnless(not skip_live_tests, "Slow test: Only runs in full unittest")
    def test_rebox_writing_displayobjs_dir_not_found(self):
        os.makedirs("ProShade/job001")
        touch("test.mrc")
        pipe = os.path.join(
            self.test_data, "JobFiles/ProShade/proshade_pipeline_rebox.star"
        )
        shutil.copy(pipe, "default_pipeline.star")
        jobstar = os.path.join(
            self.test_data, "JobFiles/ProShade/proshade_reboxing_job.star"
        )
        shutil.copy(jobstar, "ProShade/job001/job.star")
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("ProShade/job001/")
        dispobjs = pipeline.get_process_results_display(proc)
        assert dispobjs[0].__dict__["title"] == "Results pending..."
        assert dispobjs[0].__dict__["message"] == "Error creating results display"
        reason = (
            "Error creating ResultsDisplayRvapi: Directory ProShade/job001/p"
            "roshade_report does not exist"
        )
        assert reason in dispobjs[0].__dict__["reason"]

    @unittest.skipUnless(not skip_live_tests, "Slow test: Only runs in full unittest")
    def test_rebox_writing_displayobjs(self):
        os.makedirs("ProShade/job001/proshade_report")
        touch("test.mrc")
        pipe = os.path.join(
            self.test_data, "JobFiles/ProShade/proshade_pipeline_rebox.star"
        )
        shutil.copy(pipe, "default_pipeline.star")
        jobstar = os.path.join(
            self.test_data, "JobFiles/ProShade/proshade_reboxing_job.star"
        )
        shutil.copy(jobstar, "ProShade/job001/job.star")
        pipeline = ProjectGraph()
        pipeline.read()
        proc = pipeline.find_process("ProShade/job001/")
        dispobjs = pipeline.get_process_results_display(proc)
        assert dispobjs[0].__dict__ == {
            "rvapi_dir": "ProShade/job001/proshade_report",
            "dobj_type": "rvapi",
            "start_collapsed": False,
            "title": "ProShade results",
            "flag": "",
        }

    def test_sym_input_regex_validation(self):
        pss_job = new_job_of_type("proshade.map_analysis.symmetry")
        pss_job.joboptions["input_map"].value = "input_map.mrc"
        for good_sym in ["D5", "C4", "C27", "T", "O"]:
            pss_job.joboptions["point_sym"].value = good_sym
            val_results = pss_job.validate_joboptions()
            assert val_results == []
        for bad_sym in ["D", "C", "O8", "T4", "X", "X27"]:
            pss_job.joboptions["point_sym"].value = bad_sym
            val_results = pss_job.validate_joboptions()
            assert val_results[0].message == (
                "Value format must match pattern '[CD][0-9]+|^[TO]$'"
            )


if __name__ == "__main__":
    unittest.main()
