#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import math
from pipeliner_tests import test_data
from pipeliner_tests import generic_tests
from pipeliner.job_factory import active_job_from_proc
from pipeliner.utils import run_subprocess
from pipeliner_tests.job_testing_tools import (
    job_running_test,
    job_generate_commands_test,
)

from pipeliner.jobs.ccpem.model_validation_job import bfactor_path
from pipeliner.jobs.ccpem.model_validation import validation_results
from pipeliner.jobs.ccpem.model_validation.smoc import get_smoc_results
from pipeliner.jobs.ccpem.model_validation.molprobity import get_molprobity_results
from pipeliner.jobs.ccpem.model_validation.tortoize import get_tortoize_results

results_path = os.path.realpath(validation_results.__file__)
get_smoc_results_path = os.path.realpath(get_smoc_results.__file__)
get_molp_results_path = os.path.realpath(get_molprobity_results.__file__)

programs_available = all(
    [
        shutil.which("molprobity.molprobity"),
        shutil.which("TEMPy.smoc"),
        shutil.which("molprobity.reduce"),
        shutil.which("tortoize"),
    ]
)
skip_live_tests = False
do_full = generic_tests.do_slow_tests()
if (
    (
        shutil.which("molprobity.molprobity") is None
        or shutil.which("molprobity.molprobity") is None
    )
    and shutil.which("TEMPy.smoc") is None
    and shutil.which("tortoize") is None
):
    skip_live_tests = True


class ModelValidateTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="model-validation")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_tortoize_results(self):
        tortoize_out = os.path.join(self.test_data, "5me2_a_tortoize.json")
        run_subprocess(
            [
                "python3",
                os.path.realpath(get_tortoize_results.__file__),
                "-tortoize",
                tortoize_out,
                "-id",
                "5me2",
            ],
            capture_output=True,
            text=True,
        )
        assert os.path.isfile("5me2_tortoize_summary.json")
        assert math.isclose(
            os.stat(os.path.join(self.test_dir, "5me2_residue_tortoize.json")).st_size,
            16688,
            rel_tol=0.05,
        )

    def test_get_smoc_results(self):
        smoc_out = os.path.join(self.test_data, "smoc_score.json")
        residue_coordinates = os.path.join(
            self.test_data, "5me2_a_residue_coordinates.json"
        )
        run_subprocess(
            [
                "python3",
                os.path.realpath(get_smoc_results.__file__),
                "-smoc",
                smoc_out,
                "-coord",
                residue_coordinates,
                "-id",
                "5me2",
            ],
            capture_output=True,
            text=True,
        )
        assert os.path.isfile("5me2_residue_smoc.json")
        assert math.isclose(
            os.stat(os.path.join(self.test_dir, "5me2_residue_smoc.json")).st_size,
            20120,
            rel_tol=0.05,
        )

    def test_get_molprobity_results(self):
        molp_out = os.path.join(self.test_data, "molprobity_5fj8.out")
        run_subprocess(
            [
                "python3",
                os.path.realpath(get_molprobity_results.__file__),
                "-molp",
                molp_out,
                "-id",
                "5fj8",
            ],
            capture_output=True,
            text=True,
        )
        assert os.path.isfile("5fj8_molprobity_summary.json")
        assert math.isclose(
            os.stat(
                os.path.join(self.test_dir, "5fj8_molprobity_summary.json")
            ).st_size,
            767,
            rel_tol=0.05,
        )
        assert math.isclose(
            os.stat(
                os.path.join(self.test_dir, "5fj8_residue_molprobity_outliers.json")
            ).st_size,
            6872,
            rel_tol=0.05,
        )
        assert math.isclose(
            os.stat(
                os.path.join(self.test_dir, "5fj8_molprobity_outliers.json")
            ).st_size,
            4338,
            rel_tol=0.05,
        )

    @unittest.skipUnless(
        do_full and programs_available and not skip_live_tests,
        "Slow test: Only runs in full unittest",
    )
    def test_validation_task(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/ModelValidation/validate.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "5me2_a.pdb"),
                ),
                (
                    "Import/job002",
                    os.path.join(self.test_data, "emd_3488.mrc"),
                ),
            ],
            expected_outfiles=[
                "molprobity.out",
                "run.out",
                "run.err",
            ],
            print_err=True,
        )

        dispobjs = active_job_from_proc(proc).create_results_display()
        # molp table, cluster table, outlier plot * 4 chains
        assert len(dispobjs) == 7
        # molp table
        mp_table = dispobjs[1].__dict__
        assert len(mp_table["table_data"]) == 14
        assert (
            mp_table["table_data"][0][0] == "Ramachandran outliers"
            and mp_table["table_data"][0][1] == "0.00%"
        )
        # cluster table
        cluster_table = dispobjs[2].__dict__
        assert len(cluster_table["table_data"]) == 62
        assert (
            cluster_table["table_data"][0][0] == "0"
            and cluster_table["table_data"][0][1] == "A63"
        )
        # outlier plot chainA
        outlier_plotA = dispobjs[3].__dict__
        assert outlier_plotA["title"] == "Outliers: chain A"
        assert len(outlier_plotA["xvalues"]) == 5
        assert outlier_plotA["data_series_labels"] == [
            # "b-factor_dev/10.0",
            "molprobity",
            "smoc",
            "smoc_outlier",
            "tortoize_ramaz/2",
            "tortoize_torsionz/2",
        ]

    def test_get_command_modelvalidation(self):
        # Output nodes and commands differ depending on whether CCP-EM is available
        exp_out_nodes = {"molprobity.out": "LogFile.txt.molprobity.output"}
        exp_coms = []
        if bfactor_path:
            exp_coms += [
                f"python3 {bfactor_path} -p ../../Import/job001/5me2_a.pdb"
                " -mid 5me2_a"
            ]
        exp_coms += [
            "cp ../../Import/job001/5me2_a.pdb reduce_out.pdb",
            "sh -c molprobity.reduce -FLIP -Quiet ../../Import/job001/5me2_a.pdb >"
            " reduce_out.pdb",
            "molprobity.molprobity reduce_out.pdb output.percentiles=True",
            "sh -c molprobity.ramalyze ../../Import/job001/5me2_a.pdb > ramalyze_out",
            "sh -c molprobity.rotalyze ../../Import/job001/5me2_a.pdb > rotalyze_out",
            f"python3 {get_molp_results_path} -molp molprobity.out "
            "-rama ramalyze_out -rota rotalyze_out -id 5me2_a",
        ]
        # smoc
        exp_coms += [
            "TEMPy.smoc -m ../../Import/job002/emd_3488.mrc "
            "-p ../../Import/job001/5me2_a.pdb -r 3.2 --smoc-window 1"
            " --output-format json --output-prefix smoc_score",
            f"python3 {get_smoc_results_path} -smoc smoc_score.json"
            " -coord 5me2_a_residue_coordinates.json -id 5me2_a_emd_3488",
        ]
        # exp_out_nodes["5me2_a_1_smoc.pdb"] = OUTPUT_NODE_SMOCMODEL
        exp_out_nodes["smoc_score.json"] = "LogFile.json.tempy.smoc_scores"
        # tortoize
        exp_coms += [
            "tortoize ../../Import/job001/5me2_a.pdb 5me2_a_tortoize.json",
            f"python3 {get_tortoize_results.__file__} "
            "-tortoize 5me2_a_tortoize.json -id 5me2_a",
        ]
        # results
        exp_out_nodes["global_report.txt"] = "LogFile.txt.modelval.glob_output"
        # exp_out_nodes["5me2_a_outliersummary.txt"] = OUTPUT_NODE_MODELVAL_LOCAL
        exp_out_nodes["outlier_clusters.csv"] = "LogFile.csv.modelval.cluster_output"
        exp_coms += [f"python3 {results_path}"]

        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/ModelValidation/validate.job"
            ),
            input_nodes={
                "Import/job002/emd_3488.mrc": "DensityMap.mrc",
                "Import/job001/5me2_a.pdb": "AtomCoords.pdb",
            },
            output_nodes=exp_out_nodes,
            expected_commands=exp_coms,
            show_coms=True,
            show_outputnodes=True,
        )


if __name__ == "__main__":
    unittest.main()
