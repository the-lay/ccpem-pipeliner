#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests import test_data
from pipeliner_tests import generic_tests

from pipeliner.job_factory import active_job_from_proc

from pipeliner_tests.job_testing_tools import (
    job_running_test,
    job_generate_commands_test,
)

do_full = generic_tests.do_slow_tests()
skip_live_tests = True if shutil.which("locscale") is None else False


class LocScaleTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="locscale")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/LocScale/locscale_localsharpen_job.star"
            ),
            input_nodes={
                "Import/job001/1ake_10A.mrc": "DensityMap.mrc",
                "Import/job002/1ake_10A_fitted.pdb": "AtomCoords.pdb",
            },
            output_nodes={
                "locscale_output.mrc": "DensityMap.mrc.locscale.localsharpen",
            },
            expected_commands=[
                "locscale run_locscale -em ../../Import/job001/1ake_10A.mrc "
                "-res 5.0 -mc ../../Import/job002/1ake_10A_fitted.pdb"
            ],
            show_coms=False,
            show_inputnodes=False,
            show_outputnodes=False,
        )

    @unittest.skipUnless(
        do_full and not skip_live_tests, "Slow test or live tests skipped"
    )
    def test_model_based_locscale(self):
        """
        Fast implementation test with small, low resolution map (1ake).
        """
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/LocScale/locscale_localsharpen_job.star"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "1ake_10A.mrc"),
                ),
                ("Import/job002", os.path.join(self.test_data, "1ake_10A_fitted.pdb")),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "locscale_output.mrc",
            ],
        )
        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        test_dic = {
            "maps": [
                "LocScale/job998/Thumbnails/1ake_10A.mrc",
                "LocScale/job998/Thumbnails/locscale_output.mrc",
            ],
            "maps_opacity": [0.5, 1.0],
            "models": ["Import/job002/1ake_10A_fitted.pdb"],
            "title": "LocScale local sharpening",
            "models_data": "Import/job002/1ake_10A_fitted.pdb",
            "maps_data": (
                "Import/job001/1ake_10A.mrc, LocScale/job998/locscale_output.mrc"
            ),
            "associated_data": [
                "Import/job001/1ake_10A.mrc",
                "LocScale/job998/locscale_output.mrc",
                "Import/job002/1ake_10A_fitted.pdb",
            ],
            "maps_colours": [],
            "models_colours": [],
        }
        for key in test_dic.keys():
            assert test_dic[key] == dispobjs[0].__dict__[key]

    @unittest.skipUnless(
        do_full and not skip_live_tests, "Slow test or live tests skipped"
    )
    def test_model_free_based_locscale(self):
        """
        Fast implementation test with small, low resolution map (1ake).
        """
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data,
                "JobFiles/LocScale/locscale_localsharpen_model_free_job.star",
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "emd_3488.mrc"),
                ),
                (
                    "Import/job001",
                    os.path.join(self.test_data, "emd_3488_mask.mrc"),
                ),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "locscale_output.mrc",
            ],
        )
        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        test_dic = {
            "maps": [
                "LocScale/job998/Thumbnails/emd_3488.mrc",
                "LocScale/job998/Thumbnails/locscale_output.mrc",
            ],
            "maps_opacity": [0.5, 1.0],
            "models": [],
            "title": "LocScale local sharpening",
            "maps_data": (
                "Import/job001/emd_3488.mrc, LocScale/job998/locscale_output.mrc"
            ),
            "models_data": "",
            "associated_data": [
                "Import/job001/emd_3488.mrc",
                "LocScale/job998/locscale_output.mrc",
            ],
            "maps_colours": [],
            "models_colours": [],
        }
        for key in test_dic:
            assert test_dic == dispobjs[0].__dict__


if __name__ == "__main__":
    unittest.main()
