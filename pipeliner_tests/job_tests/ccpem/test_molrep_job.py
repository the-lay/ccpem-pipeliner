#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests import test_data
from pipeliner_tests import generic_tests
from pipeliner.job_factory import active_job_from_proc
from pipeliner.utils import get_pipeliner_root

from pipeliner_tests.job_testing_tools import (
    job_running_test,
    job_generate_commands_test,
)

do_full = generic_tests.do_slow_tests()
skip_live_tests = True if shutil.which("molrep") is None else False
molrep_scripts = os.path.join(
    get_pipeliner_root(), "scripts/task_utils/molrep_tools.py"
)


class MolrepTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="molrep")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_molrep(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/Molrep/molrep_fit_model.job"
            ),
            input_nodes={
                "Import/job001/1ake_10A_molrep.mrc": "DensityMap.mrc",
                "Import/job002/1AKE_cha_molrep_to_move.pdb": "AtomCoords.pdb",
            },
            output_nodes={"molrep.pdb": "AtomCoords.pdb"},
            expected_commands=[
                f"{molrep_scripts} -s ../../Import/job001/1ake_10A_molrep.mrc"
                " -k _NMON 1\nNCSM 1\nstick\nnp 1\nnpt 1\n",
                "molrep -m ../../Import/job002/1AKE_cha_molrep_to_"
                "move.pdb -f ../../Import/job001/1ake_10A_molrep.map -k keywords.txt",
                f"{molrep_scripts} -f",
            ],
        )

    @unittest.skipUnless(
        do_full and not skip_live_tests, "Slow test: Only runs in full unittest"
    )
    def test_molrep_fit_model(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/Molrep/molrep_fit_model.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "1ake_10A_molrep.mrc"),
                ),
                (
                    "Import/job002",
                    os.path.join(self.test_data, "1AKE_cha_molrep_to_move.pdb"),
                ),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "molrep.pdb",
                "molrep.btc",
                "molrep.pdb.orig",
                "keywords.txt",
            ],
            print_err=True,
            show_contents=True,
        )

        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        assert dispobjs[0].__dict__ == {
            "maps": ["Molrep/job998/Thumbnails/1ake_10A_molrep.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [0.5],
            "models": ["Molrep/job998/molrep.pdb"],
            "title": "Molrep docked model",
            "maps_data": "Import/job001/1ake_10A_molrep.mrc",
            "models_data": "Molrep/job998/molrep.pdb",
            "associated_data": [
                "Import/job001/1ake_10A_molrep.mrc",
                "Molrep/job998/molrep.pdb",
            ],
            "start_collapsed": False,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }


if __name__ == "__main__":
    unittest.main()
