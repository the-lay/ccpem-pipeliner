#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests import test_data
from pipeliner_tests import generic_tests
from pipeliner.job_factory import active_job_from_proc
from pipeliner.jobs.ccpem.gemmi_tools import model_to_map
from pipeliner_tests.job_testing_tools import (
    job_running_test,
    job_generate_commands_test,
)

do_full = generic_tests.do_slow_tests()


class GemmmiModelToMapTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="gemmi_model_to_map")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command(self):
        model_to_map_script = model_to_map.__file__
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/Gemmi/gemmi_model_to_map.job"
            ),
            input_nodes={
                "Import/job001/5me2_a.pdb": "AtomCoords.pdb",
            },
            output_nodes={
                "gemmi_5me2_a.mrc": "DensityMap.mrc.simulated",
                "gemmi_5me2_a.pdb": "AtomCoords.pdb",
            },
            expected_commands=[
                f"{model_to_map_script} --resolution 10.0 --pdb "
                "../../Import/job001/5me2_a.pdb"
            ],
        )

    @unittest.skipUnless(do_full, "Slow test: Only runs in full unittest")
    def test_model_to_map(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/Gemmi/gemmi_model_to_map.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "5me2_a.pdb"),
                ),
            ],
            expected_outfiles=["run.out", "run.err"],
        )
        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        assert dispobjs[0].__dict__ == {
            "maps": ["GemmiModelToMap/job998/Thumbnails/gemmi_5me2_a.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [0.5],
            "models": ["GemmiModelToMap/job998/gemmi_5me2_a.pdb"],
            "title": "Map from model",
            "maps_data": "GemmiModelToMap/job998/gemmi_5me2_a.mrc",
            "models_data": "GemmiModelToMap/job998/gemmi_5me2_a.pdb",
            "associated_data": [
                "GemmiModelToMap/job998/gemmi_5me2_a.mrc",
                "GemmiModelToMap/job998/gemmi_5me2_a.pdb",
            ],
            "start_collapsed": False,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }


if __name__ == "__main__":
    unittest.main()
