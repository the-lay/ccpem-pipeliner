#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests.generic_tests import (
    general_get_command_test,
)
from pipeliner_tests import test_data
from pipeliner.nodes import NODE_MICROGRAPHGROUPMETADATA, NODE_PARTICLESDATA

progs = [shutil.which("ib_group"), shutil.which("ib_job")]
skip_live_tests = True if None in progs else False


class IceBreakerTests(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_icebreaker_group_mics(self):
        general_get_command_test(
            jobtype="IceBreaker",
            jobfile="icebreaker_analysis_micrographs_job.star",
            jobnumber=2,
            input_nodes={
                "MotionCorr/job002/corrected_micrographs.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion"
            },
            output_nodes={
                "grouped_micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star."
                "relion.icebreaker.grouped",
            },
            expected_commands=[
                "ib_job --j 8 --mode group --in_mics "
                "MotionCorr/job002/corrected_micrographs.star --o IceBreaker/job002/"
            ],
        )

    def test_get_command_icebreaker_group_parts(self):
        general_get_command_test(
            jobtype="IceBreaker",
            jobfile="icebreaker_analysis_particles_job.star",
            jobnumber=2,
            input_nodes={
                "IceBreaker/job003/corrected_micrographs.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion.icebreaker.grouped",
                "Extract/job005/particles.star": f"{NODE_PARTICLESDATA}.star.relion",
            },
            output_nodes={
                "ib_icegroups.star": f"{NODE_PARTICLESDATA}.star.relion.icebreaker",
            },
            expected_commands=[
                "ib_group --j 4 --in_mics IceBreaker/job003/corrected_micrographs.star"
                " --in_parts Extract/job005/particles.star --o IceBreaker/job002/"
            ],
        )

    def test_get_command_icebreaker_flatten_mics(self):
        general_get_command_test(
            jobtype="IceBreaker",
            jobfile="icebreaker_enhancecontrast_job.star",
            jobnumber=4,
            input_nodes={
                "IceBreaker/job003/corrected_micrographs.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion.icebreaker.grouped"
            },
            output_nodes={
                "flattened_micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star."
                f"relion.icebreaker.flattened",
            },
            expected_commands=[
                "ib_job --j 4 --mode flatten --in_mics "
                "IceBreaker/job003/corrected_micrographs.star --o IceBreaker/job004/"
            ],
        )

    def test_get_command_icebreaker_five_fig(self):
        general_get_command_test(
            jobtype="IceBreaker",
            jobfile="icebreaker_summary_job.star",
            jobnumber=4,
            input_nodes={
                "IceBreaker/job003/corrected_micrographs.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion.icebreaker.grouped"
            },
            output_nodes={
                "five_figs_test.csv": f"{NODE_MICROGRAPHGROUPMETADATA}.csv.icebreaker",
            },
            expected_commands=[
                "ib_5fig --j 4 --in_mics "
                "IceBreaker/job003/corrected_micrographs.star --o IceBreaker/job004/"
            ],
        )


if __name__ == "__main__":
    unittest.main()
