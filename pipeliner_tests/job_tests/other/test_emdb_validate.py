#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests import test_data
from pipeliner_tests import generic_tests
from pipeliner.job_factory import active_job_from_proc

from pipeliner_tests.job_testing_tools import (
    job_running_test,
    job_generate_commands_test,
)

from pipeliner.scripts.task_utils import get_map_parameters

get_map_param_file = os.path.realpath(get_map_parameters.__file__)
skip_live_tests = False
do_full = generic_tests.do_slow_tests()
if shutil.which("va") is None:
    skip_live_tests = True


class EMDBValidateTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="emdb-validation")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @unittest.skipUnless(
        do_full and not skip_live_tests,
        "Slow test: Only runs in full unittest",
    )
    def test_validation_task(self):
        self.proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/EMDB_Validation/validate.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "emd_3488.mrc"),
                ),
                (
                    "Import/job002",
                    os.path.join(self.test_data, "5ni1_updated.cif"),
                ),
            ],
            expected_outfiles=[
                "emd_3488.mrc_all.json",
                "emd_3488.mrc_raps.json",
                "emd_3488.mrc_density_distribution.json",
                "emd_3488.mrc_mmfsc.json",
                "run.out",
                "run.err",
            ],
        )
        assert os.path.isfile("EMDB_validation/job998/emd_3488_map_parameters.json")
        assert os.path.isfile("EMDB_validation/job998/emd_3488.mrc_xprojection.tif")
        dispobjs = active_job_from_proc(self.proc).create_results_display()
        assert dispobjs[1].__dict__ == {
            "title": "Orthogonal projections",
            "xvalues": [0, 1, 2],
            "yvalues": [0, 0, 0],
            "labels": ["X", "Y", "Z"],
            "associated_data": [
                "EMDB_validation/job998/emd_3488.mrc_xprojection.tif",
                "EMDB_validation/job998/emd_3488.mrc_yprojection.tif",
                "EMDB_validation/job998/emd_3488.mrc_zprojection.tif",
            ],
            "img": "EMDB_validation/job998/Thumbnails/montage_f0.png",
        }

    def test_get_command_emdbvalidation(self):

        coms = ["cp Import/job001/emd_3488.mrc EMDB_validation/job999/"]
        coms += [
            "python3 "
            + get_map_param_file
            + " -m Import/job001/emd_3488.mrc -odir EMDB_validation/job999/"
        ]
        coms += ["cp Import/job002/5ni1_updated.cif EMDB_validation/job999/"]
        coms += [
            "va -m emd_3488.mrc -d EMDB_validation/job999/ -f 5ni1_updated.cif -i True "
            "-s 3.2 -cl 0.081"
        ]

        out_nodes = {"emd_3488.mrc_all.json": "ProcessData.json.emdb_va.all_results"}
        out_nodes["emd_3488.mrc_raps.json"] = "ProcessData.json.emdb_va.raps"
        out_nodes["emd_3488.mrc_density_distribution.json"] = (
            "ProcessData.json." "emdb_va.mapdistribution"
        )
        out_nodes["emd_3488.mrc_mmfsc.json"] = "ProcessData.json.emdb_va.mapmodelfsc"
        out_nodes["emd_3488.mrc_atom_inclusion.json"] = (
            "ProcessData.json.emdb_va." "atominclusion"
        )
        out_nodes["emd_3488.mrc_residue_inclusion.json"] = (
            "ProcessData.json.emdb_va." "residueinclusion"
        )

        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/EMDB_Validation/validate.job"
            ),
            input_nodes={
                "Import/job001/emd_3488.mrc": "DensityMap.mrc",
                "Import/job002/5ni1_updated.cif": "AtomCoords.cif",
            },
            output_nodes=out_nodes,
            expected_commands=coms,
            show_coms=True,
            show_outputnodes=True,
        )


if __name__ == "__main__":
    unittest.main()
