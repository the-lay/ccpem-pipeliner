#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
import shutil
import tempfile
import threading
import time
import unittest
from pathlib import Path
from typing import Optional
from unittest.mock import patch

from gemmi import cif

from pipeliner import star_writer


class EventWaitingDocWrapper(cif.Document):
    """Subclass of gemmi.cif.Document to add extra behaviour to the __iter__ method.

    It behaves just like a normal Document unless an Event is given in the `event`
    attribute, in which case it waits for the event to be set before returning an
    iterator from the `__iter__` method.

    This subclass is required because monkey-patching of Document objects fails because
    they are written in compiled code and so the attributes (including `__iter__`) are
    read-only.
    """

    event: Optional[threading.Event] = None

    def __iter__(self):
        # star_writer implicitly calls __iter__ with `for block in doc:` so it gives a
        # convenient point to interrupt the file writing and wait for an event that we
        # can control during tests.
        if self.event is not None:
            self.event.wait()
        return super().__iter__()


class StarWriterTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def create_test_doc(self):
        # Use EventWaitingDocWrapper so multi-threading tests can pause during writing
        doc = EventWaitingDocWrapper()
        block1 = doc.add_new_block("firstBlock")
        block1.set_pair("_key1", "value1")
        block2 = doc.add_new_block("secondBlock")
        loop = block2.init_loop("_col", ["1", "2", "3"])
        loop.add_row(["item1", "item2", "item3"])
        loop.add_row(["item4", "item5", "6"])
        return doc

    def test_write_jobstar_with_int(self):
        # Gemmi fails if passed an int, so this tests that we convert it to str first
        star_writer.write_jobstar(
            {"string_field": "string value", "int_field": 1}, "test_job.star"
        )
        assert os.path.isfile("test_job.star")

    def test_write_new_file(self):
        filename = Path("test.star")
        assert not filename.is_file()
        doc = self.create_test_doc()
        star_writer.write(doc, filename)
        assert filename.is_file()

    def test_write_overwriting_existing_file(self):
        filename = Path("test.star")
        filename.write_text("Old contents")
        assert filename.is_file()
        assert filename.read_text() == "Old contents"
        doc = self.create_test_doc()
        star_writer.write(doc, filename)
        assert filename.is_file()
        assert filename.read_text() != "Old contents"

    @patch("os.replace")
    def test_write_with_exception_from_os_replace(self, mock_os_replace):
        mock_os_replace.side_effect = Exception("Can't replace")
        filename = Path("test.star")
        doc = self.create_test_doc()
        with self.assertRaises(Exception):
            star_writer.write(doc, filename)
        # Check that no files are left behind if target file replacement fails
        assert len(list(Path().iterdir())) == 0

    @patch("os.fsync")
    def test_write_with_exception_from_os_fsync(self, mock_os_fsync):
        mock_os_fsync.side_effect = Exception("Can't fsync")
        filename = Path("test.star")
        doc = self.create_test_doc()
        with self.assertRaises(Exception):
            star_writer.write(doc, filename)
        # Check that no files are left behind if target file writing fails
        assert len(list(Path().iterdir())) == 0

    def test_write_uses_temporary_file(self):
        # Prepare the target file
        filename = Path("test.star")
        filename.write_text("Old contents")

        # Prepare the document to write with an Event that we can control
        resume_writing = threading.Event()
        doc = self.create_test_doc()
        doc.event = resume_writing

        # Check there is only one file in the current directory before we begin
        assert len(list(Path().iterdir())) == 1

        # Start the file writing thread, which will open a new file for writing but
        # then pause and wait for the resume_writing event before writing the contents
        file_writing_thread = threading.Thread(
            target=star_writer.write, args=(doc, filename), daemon=True
        )
        file_writing_thread.start()

        temp_filename = Path(f"{filename}.tmp{file_writing_thread.native_id}")

        try:
            # Wait for the new (but still empty) file to appear
            wait_count = 0
            while not temp_filename.is_file():
                time.sleep(0.05)
                wait_count += 1
                if wait_count > 10:
                    raise TimeoutError("Timed out waiting for file to appear")

            # Check that it uses a temporary file name while writing the file, to make
            # sure the target file remains unchanged until the writing is finished
            files = list(Path().iterdir())
            assert len(files) == 2
            assert filename.is_file()
            assert temp_filename.is_file()
        finally:
            # Signal the paused thread to continue and wait for it to finish
            resume_writing.set()
            file_writing_thread.join(timeout=1.0)

        # Check that the temporary file is gone and the target file contains (some of)
        # the expected data
        assert not temp_filename.is_file()
        assert filename.is_file()
        contents = filename.read_text()
        assert "Old contents" not in contents
        assert "firstBlock" in contents
