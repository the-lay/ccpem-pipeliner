#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import tempfile
import shutil

from pipeliner.job_options import (
    files_exts,
    IntJobOption,
    FloatJobOption,
    FileNameJobOption,
    StringJobOption,
    InputNodeJobOption,
    MultipleChoiceJobOption,
    BooleanJobOption,
    MultiFileNameJobOption,
    MultiStringJobOption,
    PathJobOption,
)
from pipeliner_tests import test_data
from pipeliner.utils import touch


class JobOptionsTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_files_extensions_single(self):
        name = "Test file type"
        exts = [".mrc"]
        result = files_exts(name, exts)
        assert result == "Test file type (*.mrc)"

    def test_files_extensions_multiple(self):
        name = "Test file types"
        exts = [".mrc", "_unfil.mrcs", ".txt"]
        result = files_exts(name, exts)
        assert result == "Test file types (*.mrc *_unfil.mrcs *.txt)"

    def test_files_extensions_single_exact(self):
        name = "Exact file type"
        exts = ["myfile.mrc"]
        result = files_exts(name, exts, True)
        assert result == "Exact file type (myfile.mrc)"

    def test_files_extensions_multiple_exact(self):
        name = "Exact file types"
        exts = ["myfile.mrc", "f_unfil.mrcs", "text.txt"]
        result = files_exts(name, exts, True)
        assert result == "Exact file types (myfile.mrc f_unfil.mrcs text.txt)"

    def test_validate_int_jo_noerror(self):
        int_jo = IntJobOption(
            label="Test int JobOption",
            default_value=0,
        )
        int_jo.value = 1
        assert len(int_jo.validate()) == 0

    def test_validate_int_jo_is_required(self):
        int_jo = IntJobOption(
            label="Test int JobOption", default_value=0, is_required=True
        )
        int_jo.value = None
        valobj = int_jo.validate()
        ermsg = "Required parameter empty"
        assert type(valobj) == list
        assert valobj[0].type == "error"
        assert valobj[0].message == ermsg
        assert type(valobj[0].raised_by[0]) == IntJobOption

    def test_validate_int_jo_below_hard_min(self):
        int_jo = IntJobOption(
            label="Test int JobOption",
            default_value=0,
            is_required=True,
            hard_min=0,
        )
        int_jo.value = -1
        ermsg = "Value cannot be less than 0"
        valobj = int_jo.validate()
        assert type(valobj) == list
        assert valobj[0].type == "error"
        assert valobj[0].message == ermsg
        assert type(valobj[0].raised_by[0]) == IntJobOption

    def test_validate_int_jo_above_hard_max(self):
        int_jo = IntJobOption(
            label="Test int JobOption",
            default_value=0,
            is_required=True,
            hard_max=0,
        )
        int_jo.value = 1
        ermsg = "Value cannot be greater than 0"
        valobj = int_jo.validate()
        assert type(valobj) == list
        assert valobj[0].type == "error"
        assert valobj[0].message == ermsg
        assert type(valobj[0].raised_by[0]) == IntJobOption

    def test_validate_float_jo_noerror(self):
        float_jo = FloatJobOption(
            label="Test float JobOption",
            default_value=0,
        )
        float_jo.value = 1
        assert len(float_jo.validate()) == 0

    def test_validate_float_jo_is_required(self):
        float_jo = FloatJobOption(
            label="Test float JobOption", default_value=0, is_required=True
        )
        float_jo.value = None
        ermsg = "Required parameter empty"
        valobj = float_jo.validate()
        assert type(valobj) == list
        assert valobj[0].type == "error"
        assert valobj[0].message == ermsg
        assert type(valobj[0].raised_by[0]) == FloatJobOption

    def test_validate_float_jo_below_hard_min(self):
        float_jo = FloatJobOption(
            label="Test float JobOption",
            default_value=0,
            is_required=True,
            hard_min=0,
        )
        float_jo.value = -1
        ermsg = "Value cannot be less than 0"
        valobj = float_jo.validate()
        assert type(valobj) == list
        assert valobj[0].type == "error"
        assert valobj[0].message == ermsg
        assert type(valobj[0].raised_by[0]) == FloatJobOption

    def test_validate_float_jo_above_hard_max(self):
        float_jo = FloatJobOption(
            label="Test float JobOption",
            default_value=0,
            is_required=True,
            hard_max=0,
        )
        float_jo.value = 1
        ermsg = "Value cannot be greater than 0"
        valobj = float_jo.validate()
        assert type(valobj) == list
        assert valobj[0].type == "error"
        assert valobj[0].message == ermsg
        assert type(valobj[0].raised_by[0]) == FloatJobOption

    def test_validate_string_jo_noerror(self):
        float_jo = StringJobOption(
            label="Test string JobOption",
            default_value="String",
        )
        assert len(float_jo.validate()) == 0

    def test_validate_string_jo_not_required(self):
        string_jo = StringJobOption(
            label="Test string JobOption",
            default_value="String",
        )
        string_jo.value = ""
        assert len(string_jo.validate()) == 0

    def test_validate_string_joboption_regexmatches(self):
        string_jo = StringJobOption(
            label="Test string JobOption",
            default_value="String",
            is_required=True,
            validation_regex="test .+ 123",
        )
        string_jo.value = "test string 123"
        assert len(string_jo.validate()) == 0

    def test_validate_string_joboption_regex_nomatch(self):
        string_jo = StringJobOption(
            label="Test string JobOption",
            default_value="String",
            is_required=True,
            validation_regex="test .+ 123",
        )
        string_jo.value = "NONONO string 123"
        ermsg = "Value format must match pattern 'test .+ 123'"
        valobj = string_jo.validate()
        assert type(valobj) == list
        assert valobj[0].type == "error"
        assert valobj[0].message == ermsg
        assert type(valobj[0].raised_by[0]) == StringJobOption

    def test_validate_filename_jo_noerror(self):
        float_jo = FileNameJobOption(
            label="Test filename JobOption",
            default_value="String",
        )
        assert len(float_jo.validate()) == 0

    def test_validate_filename_jo_not_required(self):
        filename_jo = FileNameJobOption(
            label="Test filename JobOption",
            default_value="String",
        )
        filename_jo.value = ""
        assert len(filename_jo.validate()) == 0

    def test_validate_filename_joboption_regexmatches(self):
        filename_jo = FileNameJobOption(
            label="Test filename JobOption",
            default_value="String",
            is_required=True,
            validation_regex="test .+ 123",
        )
        filename_jo.value = "test filename 123"
        assert len(filename_jo.validate()) == 0

    def test_validate_filename_joboption_regex_nomatch(self):
        filename_jo = FileNameJobOption(
            label="Test filename JobOption",
            default_value="String",
            is_required=True,
            validation_regex="test .+ 123",
        )
        filename_jo.value = "NONONO filename 123"
        ermsg = "Value format must match pattern 'test .+ 123'"
        valobj = filename_jo.validate()
        assert type(valobj) == list
        assert valobj[0].type == "error"
        assert valobj[0].message == ermsg
        assert type(valobj[0].raised_by[0]) == FileNameJobOption

    def test_filename_exists_validation_allOK(self):
        """File actually exists"""

        test_file = os.path.join(self.test_dir, "test.txt")
        touch(test_file)
        filename_jo = FileNameJobOption(
            label="Test filename JobOption",
            default_value=test_file,
            is_required=True,
        )
        filename_jo.value = test_file
        assert len(filename_jo.check_file()) == 0

    def test_filename_exists_validation_file_missing_checkfile_finds(self):
        """File missing check_file() chould catch it"""

        test_file = os.path.join(self.test_dir, "test.txt")
        filename_jo = FileNameJobOption(
            label="Test filename JobOption",
            default_value=test_file,
            is_required=True,
        )
        filename_jo.value = test_file
        ermsg = f"File {test_file} not found"
        valobj = filename_jo.check_file()
        assert type(valobj) == list
        assert valobj[0].type == "error"
        assert valobj[0].message == ermsg
        assert type(valobj[0].raised_by[0]) == FileNameJobOption

    def test_filename_exists_validation_file_missing_validate_doesntfind(self):
        """File missing validate() should not catch it"""

        test_file = os.path.join(self.test_dir, "test.txt")
        filename_jo = FileNameJobOption(
            label="Test filename JobOption",
            default_value=test_file,
            is_required=True,
        )
        filename_jo.value = test_file
        assert len(filename_jo.validate()) == 0

    def test_filename_exists_validation_file_inPATH(self):
        """File is an exe in the $PATH shouldn't fail validation"""

        filename_jo = FileNameJobOption(
            label="Test filename JobOption",
            default_value="ls",
            is_required=True,
        )
        filename_jo.value = "ls"
        assert len(filename_jo.check_file()) == 0

    def test_filename_exists_validation_files_wildcard(self):
        """File is a list to a search string"""

        os.makedirs("test")
        for n in range(5):
            touch(f"test/file_{n}.txt")

        filename_jo = FileNameJobOption(
            label="Test filename JobOption",
            default_value="test/*.txt",
            is_required=True,
        )
        filename_jo.value = "test/*.txt"
        assert len(filename_jo.check_file()) == 0

    def test_filename_exists_validation_files_wildcard_none_found(self):
        """File is a list to a search string, but no files were found"""

        os.makedirs("test")
        for n in range(5):
            touch(f"test/file_{n}.txt")

        filename_jo = FileNameJobOption(
            label="Test filename JobOption",
            default_value="test/*.mrc",
            is_required=True,
        )
        filename_jo.value = "test/*.mrc"
        ermsg = "File test/*.mrc not found"
        valobj = filename_jo.check_file()
        assert type(valobj) == list
        assert valobj[0].type == "error"
        assert valobj[0].message == ermsg
        assert type(valobj[0].raised_by[0]) == FileNameJobOption

    def test_inputnode_jo_empty_string(self):
        float_jo = InputNodeJobOption(
            label="Test inputnode JobOption",
            default_value="",
            node_type="test.node",
        )
        assert float_jo.get_string() == ""
        assert float_jo.get_input_nodes() == []

    def test_filename_jo_empty_string(self):
        filename_jo = FileNameJobOption(
            label="Test filename JobOption",
            default_value="",
            node_type="test.node",
        )
        assert filename_jo.get_string() == ""
        assert filename_jo.get_input_nodes() == []

    def test_multifilename_jo_empty_string(self):
        filename_jo = MultiFileNameJobOption(
            label="Test multifilename JobOption",
            default_value="",
            node_type="test.node",
        )
        assert filename_jo.get_string() == ""
        assert filename_jo.get_input_nodes() == []

    def test_validate_inputnode_jo_noerror(self):
        float_jo = InputNodeJobOption(
            label="Test inputnode JobOption",
            default_value="String",
            node_type="test.node",
        )
        assert len(float_jo.validate()) == 0

    def test_validate_inputnode_jo_not_required(self):
        inputnode_jo = InputNodeJobOption(
            label="Test inputnode JobOption",
            default_value="String",
            node_type="test.node",
        )
        inputnode_jo.value = ""
        assert len(inputnode_jo.validate()) == 0

    def test_validate_inputnode_joboption_regexmatches(self):
        inputnode_jo = InputNodeJobOption(
            label="Test inputnode JobOption",
            default_value="String",
            is_required=True,
            validation_regex="test .+ 123",
            node_type="test.node",
        )
        inputnode_jo.value = "test inputnode 123"
        assert len(inputnode_jo.validate()) == 0

    def test_validate_inputnode_joboption_regex_nomatch(self):
        inputnode_jo = InputNodeJobOption(
            label="Test inputnode JobOption",
            default_value="String",
            is_required=True,
            validation_regex="test .+ 123",
            node_type="test.node",
        )
        inputnode_jo.value = "NONONO inputnode 123"
        ermsg = "Value format must match pattern 'test .+ 123'"
        valobj = inputnode_jo.validate()
        assert type(valobj) == list
        assert valobj[0].type == "error"
        assert valobj[0].message == ermsg
        assert type(valobj[0].raised_by[0]) == InputNodeJobOption

    def test_validate_multichoice_jo_noerror(self):
        mc_jo = MultipleChoiceJobOption(
            label="Test mc JobOption",
            choices=["1", "2", "3", "4"],
            default_value_index=0,
        )
        assert len(mc_jo.validate()) == 0

    def test_validate_multichoice_bad_option(self):
        mc_jo = MultipleChoiceJobOption(
            label="Test mc JobOption",
            choices=["1", "2", "3", "4"],
            default_value_index=0,
        )
        mc_jo.value = "mbili"
        ermsg = "'mbili' not in list of accepted values: ['1', '2', '3', '4']"
        valobj = mc_jo.validate()
        assert type(valobj) == list
        assert valobj[0].type == "error"
        assert valobj[0].message == ermsg
        assert type(valobj[0].raised_by[0]) == MultipleChoiceJobOption

    def test_validate_multichoice_jo_noval_required(self):
        mc_jo = MultipleChoiceJobOption(
            label="Test mc JobOption",
            choices=["1", "2", "3", "4"],
            default_value_index=0,
            is_required=True,
        )
        mc_jo.value = ""
        ermsg = "Required parameter empty"
        valobj = mc_jo.validate()
        assert type(valobj) == list
        assert valobj[0].type == "error"
        assert valobj[0].message == ermsg
        assert type(valobj[0].raised_by[0]) == MultipleChoiceJobOption

    def test_validate_bool_joboption_noerror(self):
        bool_jo = BooleanJobOption(
            label="Boolio",
            default_value=True,
        )
        assert len(bool_jo.validate()) == 0

    def test_validate_bool_joboption_yes(self):
        bool_jo = BooleanJobOption(
            label="Boolio",
            default_value=True,
        )
        bool_jo.value = "Yes"
        assert len(bool_jo.validate()) == 0

    def test_validate_bool_joboption_no(self):
        bool_jo = BooleanJobOption(
            label="Boolio",
            default_value=True,
        )
        bool_jo.value = "No"
        assert len(bool_jo.validate()) == 0

    def test_bool_joboption_with_value_yes(self):
        bool_jo = BooleanJobOption(
            label="Boolio",
            default_value=True,
        )
        bool_jo.value = "Yes"
        assert bool_jo.get_boolean() is True

    def test_bool_joboption_with_value_no(self):
        bool_jo = BooleanJobOption(
            label="Boolio",
            default_value=True,
        )
        bool_jo.value = "No"
        assert bool_jo.get_boolean() is False

    def test_bool_joboption_with_value_empty(self):
        # Empty string is False, even if default value was True
        bool_jo = BooleanJobOption(
            label="Boolio",
            default_value=True,
        )
        bool_jo.value = ""
        assert bool_jo.get_boolean() is False

    def test_multifile_returns_list(self):
        mfjo = MultiFileNameJobOption(label="testjo")
        mfjo.value = "test1:::test2:::test3:::test4"
        assert mfjo.get_list() == ["test1", "test2", "test3", "test4"]

    def test_check_file_multifile(self):
        touch("f1.txt")
        touch("f2.txt")
        mfjo = MultiFileNameJobOption(label="testjo", is_required=True)
        mfjo.value = "f1.txt ::: f2.txt"
        assert not mfjo.check_file()

    def test_check_file_multifile_with_star(self):
        touch("f1.txt")
        touch("f2.txt")
        mfjo = MultiFileNameJobOption(label="testjo", is_required=True)
        mfjo.is_required = True
        mfjo.value = "f1.txt ::: *2.txt"
        assert not mfjo.check_file()

    def test_multistring_returns_list(self):
        msjo = MultiStringJobOption(label="testjo")
        msjo.value = "Test:::Test1:::Test2:::Test3"
        sval = msjo.get_list()
        assert sval == ["Test", "Test1", "Test2", "Test3"]

    def test_multistring_returns_list_from_single_val(self):
        msjo = MultiStringJobOption(label="testjo")
        msjo.value = "Test"
        sval = msjo.get_list()
        assert sval == ["Test"]

    def test_multistring_works_with_spaces(self):
        msjo = MultiStringJobOption(label="testjo")
        msjo.value = "Test ::: Test1:::   Test2 :::          Test3"
        sval = msjo.get_list()
        assert sval == ["Test", "Test1", "Test2", "Test3"]

    def test_multistring_with_regex_allmatch(self):
        msjo = MultiStringJobOption(label="testjo", validation_regex="test .+ 123")
        msjo.value = "test string 123 ::: test string2 123"
        sval = msjo.get_list()
        assert sval == ["test string 123", "test string2 123"]
        vali = msjo.validate()
        assert vali == []

    def test_multistring_with_regex_nomatch(self):
        msjo = MultiStringJobOption(label="testjo", validation_regex="test .+ 123")
        msjo.value = "string 123 ::: test string2"
        sval = msjo.get_list()
        assert sval == ["string 123", "test string2"]
        vali = msjo.validate()
        assert len(vali) == 2
        assert vali[0].__dict__["type"] == "error"
        assert (
            vali[0].__dict__["message"]
            == "Value 'string 123' format does not match pattern 'test .+ 123'"
        )
        assert vali[1].__dict__["type"] == "error"
        assert (
            vali[1].__dict__["message"]
            == "Value 'test string2' format does not match pattern 'test .+ 123'"
        )

    def test_path_joboption_wildcard_not_allowed(self):
        jo = PathJobOption(label="test_path_jo")
        jo.value = "*.*"
        errors = jo.validate()
        assert len(errors) == 1
        assert errors[0].__dict__["message"] == (
            "Wildcards (*) are not allowed in this parameter"
        )

    def test_path_joboption_wildcard_allowed(self):
        jo = PathJobOption(label="test_path_jo", wildcard_allowed=True)
        jo.value = "*.*"
        errors = jo.validate()
        assert not errors

    def test_path_joboptions_must_be_relative(self):
        jo = PathJobOption(label="test_path_jo", must_be_in_project=True)
        jo.value = "../not_a_god_file.txt"
        errors = jo.validate()
        assert len(errors) == 1
        assert errors[0].__dict__["message"] == (
            "This field requires a path relative to the project and all files "
            "must be in the project.  IE: Path cannot start with '/' or '../'"
        )

    def test_path_joboption_no_files_search_string(self):
        os.makedirs("test_files")
        jo = PathJobOption(
            label="test_path_jo",
            must_be_in_project=True,
            wildcard_allowed=True,
        )
        jo.value = "test_files/*.txt"
        errors = jo.check_file()
        assert len(errors) == 1
        assert errors[0].__dict__["message"] == "File test_files/*.txt not found"

    def test_path_joboption_no_files_single(self):
        os.makedirs("test_files")
        jo = PathJobOption(label="test_path_jo", must_be_in_project=True)
        jo.value = "test_files/test.txt"
        errors = jo.validate()
        errors += jo.check_file()
        assert len(errors) == 1
        assert errors[0].__dict__["message"] == "File test_files/test.txt not found"

    def test_path_joboption_found_file_single(self):
        os.makedirs("test_files")
        touch("test_files/test.txt")
        jo = PathJobOption(label="test_path_jo", must_be_in_project=True)
        jo.value = "test_files/test.txt"
        errors = jo.validate()
        errors += jo.check_file()
        assert not errors

    def test_path_joboption_is_dir_true(self):
        os.makedirs("test_files")
        jo = PathJobOption(label="test_path_jo", must_be_in_project=True, is_dir=True)
        jo.value = "test_files"
        errors = jo.validate()
        errors += jo.check_file()
        assert not errors

    def test_path_joboption_is_dir_false(self):
        jo = PathJobOption(label="test_path_jo", must_be_in_project=True, is_dir=True)
        jo.value = "test_files"
        errors = jo.validate()
        errors += jo.check_file()
        assert len(errors) == 1
        assert errors[0].__dict__["message"] == f"Directory {jo.value} not found"

    def test_path_joboption_found_files_string(self):
        os.makedirs("test_files")
        for n in range(10):
            touch(f"test_files/{n}.txt")
        jo = PathJobOption(
            label="test_path_jo",
            must_be_in_project=True,
            wildcard_allowed=True,
        )
        jo.value = "test_files/*.txt"
        errors = jo.validate()
        errors += jo.check_file()
        assert not errors


if __name__ == "__main__":
    unittest.main()
