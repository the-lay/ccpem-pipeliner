
# version 30001

data_optics

loop_ 
_rlnOpticsGroupName #1 
_rlnOpticsGroup #2 
_rlnMtfFileName #3 
_rlnMicrographOriginalPixelSize #4 
_rlnVoltage #5 
_rlnSphericalAberration #6 
_rlnAmplitudeContrast #7 
_rlnMicrographPixelSize #8 
opticsGroup1            1 mtf_k2_200kV.star     0.885000   200.000000     1.400000     0.100000     0.885000 
 

# version 30001

data_micrographs

loop_ 
_rlnMicrographName #1 
_rlnOpticsGroup #2 
_rlnCtfImage #3 
_rlnDefocusU #4 
_rlnDefocusV #5 
_rlnCtfAstigmatism #6 
_rlnDefocusAngle #7 
_rlnCtfFigureOfMerit #8 
_rlnCtfMaxResolution #9 
MotionCorr/job002/Movies/20170629_00021_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00021_frameImage_PS.ctf:mrc 10863.857422 10575.721680   288.135742    77.967194     0.131144     4.809192 
MotionCorr/job002/Movies/20170629_00022_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00022_frameImage_PS.ctf:mrc  9836.475586  9586.718750   249.756836    70.291290     0.168926     3.619038 
MotionCorr/job002/Movies/20170629_00023_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00023_frameImage_PS.ctf:mrc 10678.056641 10365.234375   312.822266    71.958046     0.166332     3.412236 
MotionCorr/job002/Movies/20170629_00024_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00024_frameImage_PS.ctf:mrc 11693.039062 11353.885742   339.153320    74.367035     0.153686     3.495461 
MotionCorr/job002/Movies/20170629_00025_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00025_frameImage_PS.ctf:mrc 10656.021484 10381.144531   274.876953    73.184746     0.156609     3.478493 
 
