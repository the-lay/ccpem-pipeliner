
# RELION version 4.0 / CCP-EM Pipeliner version 0.1.0

data_optics

loop_ 
_rlnOpticsGroupName #1 
_rlnOpticsGroup #2 
_rlnMtfFileName #3 
_rlnMicrographPixelSize #4 
_rlnMicrographOriginalPixelSize #5 
_rlnVoltage #6 
_rlnSphericalAberration #7 
_rlnAmplitudeContrast #8 
opticsGroup1            1 mtf_300kV_all_1.0.star          1.0          1.0        300.0          2.7          0.1 
 

# RELION version 4.0 / CCP-EM Pipeliner version 0.1.0

data_micrographs

loop_ 
_rlnMicrographName #1 
_rlnOpticsGroup #2 
 
