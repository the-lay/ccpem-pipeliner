<?xml version='1.0' encoding='utf-8'?>
<emd emdb_id="EMD-15285" version="3.0.2.7" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="https://github.com/emdb-empiar/emdb-schemas/blob/master/v3/v3_0_2_7/emdb.xsd">
    <admin>
        <current_status>
            <date>2022-09-28</date>
            <code>REL</code>
            <processing_site>PDBe</processing_site>
        </current_status>
        <sites>
            <deposition>PDBe</deposition>
            <last_processing>PDBe</last_processing>
        </sites>
        <key_dates>
            <deposition>2022-06-28</deposition>
            <header_release>2022-08-31</header_release>
            <map_release>2022-08-31</map_release>
            <update>2022-09-28</update>
        </key_dates>
        <grant_support>
            <grant_reference>
                <funding_body>Medical Research Council (MRC, United Kingdom)</funding_body>
                <code>MC_UP_A025_1013</code>
                <country>United Kingdom</country>
            </grant_reference>
            <grant_reference>
                <funding_body>Medical Research Council (MRC, United Kingdom)</funding_body>
                <code>MC_U105184291 to M.G.</code>
                <country>United Kingdom</country>
            </grant_reference>
            <grant_reference>
                <funding_body>Alzheimers Research UK (ARUK)</funding_body>
                <country>United Kingdom</country>
            </grant_reference>
            <grant_reference>
                <funding_body>National Institutes of Health/National Institute of Neurological Disorders and Stroke (NIH/NINDS)</funding_body>
                <country>United States</country>
            </grant_reference>
        </grant_support>
        <title>Cryo-EM structure of alpha-synuclein filaments from Parkinson's disease and dementia with Lewy bodies</title>
        <authors_list>
            <author ORCID="0000-0003-2238-6437">Yang Y</author>
            <author>Shi Y</author>
            <author>Schweighauser M</author>
            <author>Zhang XJ</author>
            <author>Kotecha A</author>
            <author>Murzin AG</author>
            <author>Garringer HJ</author>
            <author>Cullinane P</author>
            <author>Saito Y</author>
            <author>Foroud T</author>
            <author>Warner TT</author>
            <author>Hasegawa K</author>
            <author>Vidal R</author>
            <author>Murayama S</author>
            <author>Revesz T</author>
            <author>Ghetti B</author>
            <author>Hasegawa M</author>
            <author>Lashley T</author>
            <author>Scheres HWS</author>
            <author>Goedert M</author>
        </authors_list>
    </admin>
    <crossreferences>
        <citation_list>
            <primary_citation>
                <journal_citation published="true">
                    <author ORCID="0000-0003-2238-6437" order="1">Yang Y</author>
                    <author ORCID="0000-0003-1579-7561" order="2">Shi Y</author>
                    <author ORCID="0000-0002-1848-1610" order="3">Schweighauser M</author>
                    <author order="4">Zhang X</author>
                    <author ORCID="0000-0002-4480-5439" order="5">Kotecha A</author>
                    <author order="6">Murzin AG</author>
                    <author ORCID="0000-0002-1899-7676" order="7">Garringer HJ</author>
                    <author order="8">Cullinane PW</author>
                    <author order="9">Saito Y</author>
                    <author ORCID="0000-0002-5549-2212" order="10">Foroud T</author>
                    <author ORCID="0000-0001-6195-6995" order="11">Warner TT</author>
                    <author order="12">Hasegawa K</author>
                    <author order="13">Vidal R</author>
                    <author order="14">Murayama S</author>
                    <author order="15">Revesz T</author>
                    <author ORCID="0000-0002-1842-8019" order="16">Ghetti B</author>
                    <author ORCID="0000-0001-7415-8159" order="17">Hasegawa M</author>
                    <author order="18">Lashley T</author>
                    <author ORCID="0000-0002-0462-6540" order="19">Scheres SHW</author>
                    <author ORCID="0000-0002-5214-7886" order="20">Goedert M</author>
                    <title>Structures of alpha-synuclein filaments from human brains with Lewy pathology.</title>
                    <journal_abbreviation>Nature</journal_abbreviation>
                    <country>UK</country>
                    <year>2022</year>
                    <external_references type="PUBMED">36108674</external_references>
                    <external_references type="DOI">doi:10.1038/s41586-022-05319-3</external_references>
                    <external_references type="ISSN">1476-4687</external_references>
                    <external_references type="CSD">0006</external_references>
                    <external_references type="ASTM">NATUAS</external_references>
                </journal_citation>
            </primary_citation>
        </citation_list>
        <emdb_list>
            <emdb_reference>
                <emdb_id>EMD-15285</emdb_id>
                <relationship>
                    <other>associated EM volume</other>
                </relationship>
                <details>Cryo-EM structure of alpha-synuclein filaments from Parkinson's disease and dementia with Lewy bodies</details>
            </emdb_reference>
        </emdb_list>
        <pdb_list>
            <pdb_reference>
                <pdb_id>8a9l</pdb_id>
                <relationship>
                    <in_frame>FULLOVERLAP</in_frame>
                </relationship>
            </pdb_reference>
        </pdb_list>
    </crossreferences>
    <sample>
        <name>Alpha-synuclein filaments extracted from the human brain with PD, PDD, and DLB</name>
        <supramolecule_list>
            <tissue_supramolecule supramolecule_id="1">
                <name>Alpha-synuclein filaments extracted from the human brain with PD, PDD, and DLB</name>
                <parent>0</parent>
                <macromolecule_list>
                    <macromolecule>
                        <macromolecule_id>1</macromolecule_id>
                    </macromolecule>
                    <macromolecule>
                        <macromolecule_id>2</macromolecule_id>
                    </macromolecule>
                    <macromolecule>
                        <macromolecule_id>3</macromolecule_id>
                    </macromolecule>
                </macromolecule_list>
                <natural_source database="NCBI">
                    <organism ncbi="9606">Homo sapiens</organism>
                </natural_source>
            </tissue_supramolecule>
        </supramolecule_list>
        <macromolecule_list>
            <protein_or_peptide macromolecule_id="1">
                <name>Alpha-synuclein</name>
                <natural_source database="NCBI">
                    <organism ncbi="9606">human</organism>
                </natural_source>
                <molecular_weight>
                    <theoretical units="MDa">0.014476108</theoretical>
                </molecular_weight>
                <number_of_copies>1</number_of_copies>
                <enantiomer>LEVO</enantiomer>
                <sequence>
                    <string>MDVFMKGLSKAKEGVVAAAEKTKQGVAEAAGKTKEGVLYVGSKTKEGVVHGVATVAEKTKEQVTNVGGAVVTGVTAVAQK
TVEGAGSIAAATGFVKKDQLGKNEEGAPQEGILEDMPVDPDNEAYEMPSEEGYQDYEPEA</string>
                </sequence>
            </protein_or_peptide>
            <protein_or_peptide macromolecule_id="2">
                <name>Unknown fragment</name>
                <natural_source database="NCBI">
                    <organism ncbi="9606">Homo sapiens</organism>
                </natural_source>
                <molecular_weight>
                    <theoretical units="MDa">0.000783958</theoretical>
                </molecular_weight>
                <number_of_copies>1</number_of_copies>
                <enantiomer>LEVO</enantiomer>
                <sequence>
                    <string>(UNK)(UNK)(UNK)(UNK)(UNK)(UNK)(UNK)(UNK)(UNK)</string>
                </sequence>
            </protein_or_peptide>
            <protein_or_peptide macromolecule_id="3">
                <name>Unknown fragment</name>
                <natural_source database="NCBI">
                    <organism ncbi="9606">Homo sapiens</organism>
                </natural_source>
                <molecular_weight>
                    <theoretical units="MDa">0.0006277749999999999</theoretical>
                </molecular_weight>
                <number_of_copies>1</number_of_copies>
                <enantiomer>LEVO</enantiomer>
                <sequence>
                    <string>(UNK)(UNK)(UNK)(UNK)V(UNK)(UNK)</string>
                </sequence>
            </protein_or_peptide>
            <ligand macromolecule_id="4">
                <name>water</name>
                <molecular_weight>
                    <theoretical units="MDa">1.8015e-05</theoretical>
                </molecular_weight>
                <number_of_copies>6</number_of_copies>
                <formula>HOH</formula>
            </ligand>
        </macromolecule_list>
    </sample>
    <structure_determination_list>
        <structure_determination structure_determination_id="1">
            <method>helical</method>
            <aggregation_state>filament</aggregation_state>
            <specimen_preparation_list>
                <helical_preparation preparation_id="1">
                    <buffer>
                        <ph>7.5</ph>
                    </buffer>
                    <vitrification>
                        <cryogen_name>ETHANE</cryogen_name>
                    </vitrification>
                </helical_preparation>
            </specimen_preparation_list>
            <microscopy_list>
                <helical_microscopy microscopy_id="1">
                    <microscope>FEI TITAN KRIOS</microscope>
                    <illumination_mode>FLOOD BEAM</illumination_mode>
                    <imaging_mode>BRIGHT FIELD</imaging_mode>
                    <electron_source>FIELD EMISSION GUN</electron_source>
                    <acceleration_voltage units="kV">300</acceleration_voltage>
                    <nominal_defocus_min units="µm">0.6</nominal_defocus_min>
                    <nominal_defocus_max units="µm">1.4000000000000001</nominal_defocus_max>
                    <image_recording_list>
                        <image_recording image_recording_id="1">
                            <film_or_detector_model>FEI FALCON IV (4k x 4k)</film_or_detector_model>
                            <average_electron_dose_per_image units="e/Å^2">40.0</average_electron_dose_per_image>
                        </image_recording>
                    </image_recording_list>
                </helical_microscopy>
            </microscopy_list>
            <helical_processing image_processing_id="1">
                <image_recording_id>1</image_recording_id>
                <final_reconstruction>
                    <applied_symmetry>
                        <helical_parameters>
                            <delta_z units="Å">4.76</delta_z>
                            <delta_phi units="deg">0.86</delta_phi>
                            <axial_symmetry>C1</axial_symmetry>
                        </helical_parameters>
                    </applied_symmetry>
                    <resolution res_type="BY AUTHOR" units="Å">2.2</resolution>
                    <resolution_method>FSC 0.143 CUT-OFF</resolution_method>
                    <software_list>
                        <software>
                            <name>RELION</name>
                            <version>4.0</version>
                        </software>
                    </software_list>
                    <number_images_used>68330</number_images_used>
                </final_reconstruction>
                <final_angle_assignment>
                    <type>NOT APPLICABLE</type>
                </final_angle_assignment>
            </helical_processing>
        </structure_determination>
    </structure_determination_list>
    <map format="CCP4" size_kbytes="67109">
        <file>emd_15285.map.gz</file>
        <symmetry>
            <space_group>1</space_group>
        </symmetry>
        <data_type>IMAGE STORED AS FLOATING POINT NUMBER (4 BYTES)</data_type>
        <dimensions>
            <col>256</col>
            <row>256</row>
            <sec>256</sec>
        </dimensions>
        <origin>
            <col>0</col>
            <row>0</row>
            <sec>0</sec>
        </origin>
        <spacing>
            <x>256</x>
            <y>256</y>
            <z>256</z>
        </spacing>
        <cell>
            <a units="Å">186.112</a>
            <b units="Å">186.112</b>
            <c units="Å">186.112</c>
            <alpha units="deg">90.0</alpha>
            <beta units="deg">90.0</beta>
            <gamma units="deg">90.0</gamma>
        </cell>
        <axis_order>
            <fast>X</fast>
            <medium>Y</medium>
            <slow>Z</slow>
        </axis_order>
        <statistics>
            <minimum>-0.01729697</minimum>
            <maximum>0.044076517</maximum>
            <average>0.00015815366</average>
            <std>0.002012545</std>
        </statistics>
        <pixel_spacing>
            <x units="Å">0.727</x>
            <y units="Å">0.727</y>
            <z units="Å">0.727</z>
        </pixel_spacing>
        <contour_list>
            <contour primary="true">
                <level>0.0082</level>
                <source>AUTHOR</source>
            </contour>
        </contour_list>
        <label>::::EMDATABANK.org::::EMD-15285::::</label>
    </map>
    <interpretation>
        <segmentation_list>
            <segmentation>
                <file>emd_15285_msk_1.map</file>
            </segmentation>
        </segmentation_list>
        <half_map_list>
            <half_map format="CCP4" size_kbytes="67109">
                <file>emd_15285_half_map_2.map.gz</file>
                <symmetry>
                    <space_group>1</space_group>
                </symmetry>
                <data_type>IMAGE STORED AS FLOATING POINT NUMBER (4 BYTES)</data_type>
                <dimensions>
                    <col>256</col>
                    <row>256</row>
                    <sec>256</sec>
                </dimensions>
                <origin>
                    <col>0</col>
                    <row>0</row>
                    <sec>0</sec>
                </origin>
                <spacing>
                    <x>256</x>
                    <y>256</y>
                    <z>256</z>
                </spacing>
                <cell>
                    <a units="Å">186.112</a>
                    <b units="Å">186.112</b>
                    <c units="Å">186.112</c>
                    <alpha units="deg">90.0</alpha>
                    <beta units="deg">90.0</beta>
                    <gamma units="deg">90.0</gamma>
                </cell>
                <axis_order>
                    <fast>X</fast>
                    <medium>Y</medium>
                    <slow>Z</slow>
                </axis_order>
                <statistics>
                    <minimum>-0.011388118</minimum>
                    <maximum>0.02408547</maximum>
                    <average>0.00015662932</average>
                    <std>0.0013408439</std>
                </statistics>
                <pixel_spacing>
                    <x units="Å">0.727</x>
                    <y units="Å">0.727</y>
                    <z units="Å">0.727</z>
                </pixel_spacing>
                <contour_list>
                    <contour primary="true">
                        <source>AUTHOR</source>
                    </contour>
                </contour_list>
                <label>::::EMDATABANK.org::::EMD-15285::::</label>
            </half_map>
            <half_map format="CCP4" size_kbytes="67109">
                <file>emd_15285_half_map_1.map.gz</file>
                <symmetry>
                    <space_group>1</space_group>
                </symmetry>
                <data_type>IMAGE STORED AS FLOATING POINT NUMBER (4 BYTES)</data_type>
                <dimensions>
                    <col>256</col>
                    <row>256</row>
                    <sec>256</sec>
                </dimensions>
                <origin>
                    <col>0</col>
                    <row>0</row>
                    <sec>0</sec>
                </origin>
                <spacing>
                    <x>256</x>
                    <y>256</y>
                    <z>256</z>
                </spacing>
                <cell>
                    <a units="Å">186.112</a>
                    <b units="Å">186.112</b>
                    <c units="Å">186.112</c>
                    <alpha units="deg">90.0</alpha>
                    <beta units="deg">90.0</beta>
                    <gamma units="deg">90.0</gamma>
                </cell>
                <axis_order>
                    <fast>X</fast>
                    <medium>Y</medium>
                    <slow>Z</slow>
                </axis_order>
                <statistics>
                    <minimum>-0.01110918</minimum>
                    <maximum>0.025871778</maximum>
                    <average>0.00016066889</average>
                    <std>0.0013704423</std>
                </statistics>
                <pixel_spacing>
                    <x units="Å">0.727</x>
                    <y units="Å">0.727</y>
                    <z units="Å">0.727</z>
                </pixel_spacing>
                <contour_list>
                    <contour primary="true">
                        <source>AUTHOR</source>
                    </contour>
                </contour_list>
                <label>::::EMDATABANK.org::::EMD-15285::::</label>
            </half_map>
        </half_map_list>
    </interpretation>
</emd>