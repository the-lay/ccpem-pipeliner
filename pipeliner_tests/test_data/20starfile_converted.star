
# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_optics

loop_ 
_rlnOpticsGroupName #1 
_rlnOpticsGroup #2 
_rlnMtfFileName #3 
_rlnMicrographOriginalPixelSize #4 
_rlnVoltage #5 
_rlnSphericalAberration #6 
_rlnAmplitudeContrast #7 
_rlnImagePixelSize #8 
_rlnImageSize #9 
_rlnImageDimensionality #10 
_rlnDetectorPixelSize #11 
_rlnMagnification #12 
convertedOpticsGroup1            1           "" 1.215601562261156   300.000000     2.000000     0.070000 1.215601562261156          256            2     5.000000 41131.898438 
 

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_particles

loop_ 
_rlnCtfFigureOfMerit #1 
_rlnCoordinateX #2 
_rlnDefocusAngle #3 
_rlnDefocusV #4 
_rlnLogLikeliContribution #5 
_rlnMicrographName #6 
_rlnAngleTilt #7 
_rlnClassNumber #8 
_rlnGroupNumber #9 
_rlnCoordinateY #10 
_rlnAnglePsi #11 
_rlnAngleRot #12 
_rlnOriginX #13 
_rlnAutopickFigureOfMerit #14 
_rlnMaxValueProbDistribution #15 
_rlnNormCorrection #16 
_rlnNrOfSignificantSamples #17 
_rlnOriginY #18 
_rlnImageName #19 
_rlnDefocusU #20 
_rlnRandomSubset #21 
_rlnOpticsGroup #22 
    0.121563  2759.000000    13.390000 20835.980469 1.355668e+05 images/stack_101016-2_0001_2x_SumCorr.mrc    94.604259            2            1  1362.000000   -38.988060    20.246157    -3.797830     1.173185     1.000000     0.509441            1    -7.797830 1@sClass12456-merge.mrcs 21562.509766            1            1 
    0.121563  3387.000000    13.390000 20835.980469 1.359507e+05 images/stack_101016-2_0001_2x_SumCorr.mrc    99.182877            1            1  1420.000000   128.584219    11.450013     1.825188     0.678425     1.000000     0.504943            1     9.266188 2@sClass12456-merge.mrcs 21562.509766            2            1 
    0.121563   391.000000    13.390000 20835.980469 1.359287e+05 images/stack_101016-2_0001_2x_SumCorr.mrc    89.054355            1            1  1521.000000    48.829958    23.496992    -7.138368     0.868732     1.000000     0.509666            1     3.861632 3@sClass12456-merge.mrcs 21562.509766            1            1 
 
