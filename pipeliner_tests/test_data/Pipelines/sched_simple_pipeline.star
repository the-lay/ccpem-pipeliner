
# version 30001 / CCP-EM_pipeliner

data_pipeline_general

_rlnPipeLineJobCounter                      3
 

# version 30001 / CCP-EM_pipeliner

data_pipeline_processes

loop_ 
_rlnPipeLineProcessName #1 
_rlnPipeLineProcessAlias #2 
_rlnPipeLineProcessTypeLabel #3 
_rlnPipeLineProcessStatusLabel #4 
Refine3D/job001/       None            relion.refine3d           Succeeded  
Import/job002/       None            relion.import.other            Scheduled 
PostProcess/job003/       None            relion.postprocess            Scheduled 

# version 30001 / CCP-EM_pipeliner

data_pipeline_nodes

loop_ 
_rlnPipeLineNodeName #1 
_rlnPipeLineNodeTypeLabel #2 
emd_3488_mask.mrc            Mask3D.mrc
Refine3D/job001/3488_run_half1_class001_unfil.mrc            DensityMap.mrc.relion.refine3d.halfmap
Import/job002/emd_3488_mask.mrc            Mask3D.mrc           
Refine3D/job001/run_data.star            ParticlesData.star.relion.refine3d
Refine3D/job001/run_half1_class001_unfil.mrc            DensityMap.mrc.relion.refine3d.halfmap
Refine3D/job001/run_class001.mrc            DensityMap.mrc.relion.refine3d
PostProcess/job003/postprocess.mrc             DensityMap.mrc.relion.postprocessed
PostProcess/job003/postprocess_masked.mrc            DensityMap.mrc.relion.postprocessed.masked
PostProcess/job003/logfile.pdf             LogFile.pdf.relion.postprocess
PostProcess/job003/postprocess.star            ProcessData.star.relion.postprocess

# version 30001 / CCP-EM_pipeliner

data_pipeline_input_edges

loop_ 
_rlnPipeLineEdgeFromNode #1 
_rlnPipeLineEdgeProcess #2 
emd_3488_mask.mrc Import/job002/
Refine3D/job001/3488_run_half1_class001_unfil.mrc PostProcess/job003/
Import/job002/emd_3488_mask.mrc PostProcess/job003/
# version 30001 / CCP-EM_pipeliner

data_pipeline_output_edges

loop_ 
_rlnPipeLineEdgeProcess #1 
_rlnPipeLineEdgeToNode #2 
Refine3D/job001/ Refine3D/job001/run_data.star 
Refine3D/job001/ Refine3D/job001/run_half1_class001_unfil.mrc 
Refine3D/job001/ Refine3D/job001/run_class001.mrc Import/job002/ Import/job002/emd_3488_mask.mrc
PostProcess/job003/ PostProcess/job003/postprocess.mrc 
PostProcess/job003/ PostProcess/job003/postprocess_masked.mrc 
PostProcess/job003/ PostProcess/job003/logfile.pdf 
PostProcess/job003/ PostProcess/job003/postprocess.star 