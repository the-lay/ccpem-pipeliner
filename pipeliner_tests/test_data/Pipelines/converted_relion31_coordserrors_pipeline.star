# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_pipeline_general

_rlnPipeLineJobCounter    32
 

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_pipeline_processes

loop_ 
_rlnPipeLineProcessName #1 
_rlnPipeLineProcessAlias #2 
_rlnPipeLineProcessTypeLabel #3 
_rlnPipeLineProcessStatusLabel #4 
Import/job001/ Import/movies/ relion.import.movies    Succeeded 
MotionCorr/job002/ MotionCorr/relioncor2/ relion.motioncorr.own    Succeeded 
CtfFind/job003/ CtfFind/ctffind4/ relion.ctffind.ctffind4    Succeeded 
ManualPick/job004/ ManualPick/justatest/ relion.manualpick    Succeeded 
Select/job005/ Select/5mics/ relion.select.interactive    Succeeded 
AutoPick/job006/ AutoPick/LoG/ relion.autopick.log    Succeeded 
Extract/job007/ Extract/LoG/ relion.extract    Succeeded 
Class2D/job008/ Class2D/LoG/ relion.class2d.em    Succeeded 
Select/job009/ Select/templates4autopick/ relion.select.interactive    Succeeded 
AutoPick/job010/ AutoPick/optimise_params/ relion.autopick.ref2d    Succeeded 
AutoPick/job011/ AutoPick/template/ relion.autopick.ref2d    Succeeded 
Extract/job012/ Extract/template/ relion.extract    Succeeded 
Class2D/job013/ Class2D/template/ relion.class2d.em    Succeeded 
Select/job014/ Select/class2d_template/ relion.select.interactive    Succeeded 
InitialModel/job015/ InitialModel/symC1/ relion.initialmodel    Succeeded 
Class3D/job016/ Class3D/first_exhaustive/ relion.class3d    Succeeded 
Select/job017/ Select/class3d_first_exhaustive/ relion.select.interactive    Succeeded 
Extract/job018/ Extract/best3dclass_bigbox/ relion.extract    Succeeded 
Refine3D/job019/ Refine3D/first3dref/ relion.refine3d    Succeeded 
MaskCreate/job020/ MaskCreate/first3dref/ relion.maskcreate    Succeeded 
PostProcess/job021/ PostProcess/first3dref/ relion.postprocess    Succeeded 
CtfRefine/job022/ CtfRefine/aberrations/ relion.ctfrefine    Succeeded 
CtfRefine/job023/ CtfRefine/magnification/ relion.ctfrefine.anisomag    Succeeded 
CtfRefine/job024/ CtfRefine/defocus/ relion.ctfrefine    Succeeded 
Refine3D/job025/ Refine3D/ctfrefined/ relion.refine3d    Succeeded 
PostProcess/job026/ PostProcess/ctfrefined/ relion.postprocess    Succeeded 
Polish/job027/ Polish/train/ relion.polish.train    Succeeded 
Polish/job028/ Polish/polish/ relion.polish    Succeeded 
Refine3D/job029/ Refine3D/shiny/ relion.refine3d    Succeeded 
PostProcess/job030/ PostProcess/shiny/ relion.postprocess    Succeeded 
LocalRes/job031/ LocalRes/shiny/ relion.localres.own    Succeeded 
 

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_pipeline_nodes

loop_ 
_rlnPipeLineNodeName #1 
_rlnPipeLineNodeTypeLabel #2 
Import/job001/movies.star MicrographMovieGroupMetadata.star.relion 
MotionCorr/job002/corrected_micrographs.star MicrographGroupMetadata.star.relion.motioncorr 
MotionCorr/job002/logfile.pdf LogFile.pdf.relion.motioncorr 
MotionCorr/job002/corrected_micrographs.star MicrographGroupMetadata.star.relion.motioncorr 
CtfFind/job003/micrographs_ctf.star MicrographGroupMetadata.star.relion.ctf 
CtfFind/job003/logfile.pdf LogFile.pdf.relion.ctffind 
ManualPick/job004/coords_suffix_manualpick.star MicrographCoordsGroup.star.UNSUPPORTED_deprecated_format
ManualPick/job004/micrographs_selected.star MicrographGroupMetadata.star.relion 
Select/job005/micrographs_selected.star MicrographGroupMetadata.star.relion 
AutoPick/job006/coords_suffix_autopick.star MicrographCoordsGroup.star.UNSUPPORTED_deprecated_format 
AutoPick/job006/logfile.pdf LogFile.pdf.relion.autopick 
Extract/job007/particles.star ParticlesData.star.relion 
Class2D/job008/run_it025_data.star ParticlesData.star.relion.class2d 
Class2D/job008/run_it025_optimiser.star ProcessData.star.relion.optimiser.class2d 
Select/job009/particles.star ParticlesData.star.relion 
Select/job009/class_averages.star Image2DGroupMetadata.star.relion.classaverages
AutoPick/job010/coords_suffix_autopick.star MicrographCoordsGroup.star.UNSUPPORTED_deprecated_format
AutoPick/job010/logfile.pdf LogFile.pdf.relion.autopick 
AutoPick/job011/coords_suffix_autopick.star MicrographCoordsGroup.star.UNSUPPORTED_deprecated_format 
AutoPick/job011/logfile.pdf LogFile.pdf.relion.autopick 
Extract/job012/particles.star ParticlesData.star.relion 
Class2D/job013/run_it025_data.star ParticlesData.star.relion.class2d 
Class2D/job013/run_it025_optimiser.star ProcessData.star.relion.optimiser.class2d 
Select/job014/particles.star ParticlesData.star.relion 
Select/job014/class_averages.star Image2DGroupMetadata.star.relion.classaverages
InitialModel/job015/run_it150_data.star ParticlesData.star.relion.initialmodel 
InitialModel/job015/run_it150_optimiser.star ProcessData.star.relion.optimser.initialmodel 
InitialModel/job015/run_it150_class001.mrc DensityMap.mrc.relion.initialmodel 
InitialModel/job015/run_it150_class001_symD2.mrc DensityMap.mrc.relion.initialmodel 
Class3D/job016/run_it025_data.star ParticlesData.star.relion.class3d 
Class3D/job016/run_it025_optimiser.star ProcessData.star.optimiser.relion.class3d 
Class3D/job016/run_it025_class001.mrc DensityMap.mrc.relion.class3d 
Class3D/job016/run_it025_class002.mrc DensityMap.mrc.relion.class3d 
Class3D/job016/run_it025_class003.mrc DensityMap.mrc.relion.class3d 
Class3D/job016/run_it025_class004.mrc DensityMap.mrc.relion.class3d 
Select/job017/particles.star ParticlesData.star.relion 
Extract/job018/particles.star ParticlesData.star.relion 
Extract/job018/coords_suffix_extract.star MicrographCoordsGroup.star.UNSUPPORTED_deprecated_format 
Class3D/job016/run_it025_class001_box256.mrc DensityMap.mrc.relion.class3d 
Refine3D/job019/run_data.star ParticlesData.star.relion.refine3d 
Refine3D/job019/run_half1_class001_unfil.mrc DensityMap.mrc.relion.refine3d.halfmap 
Refine3D/job019/run_class001.mrc DensityMap.mrc.relion.refine3d 
MaskCreate/job020/mask.mrc Mask3D.mrc.relion 
PostProcess/job021/postprocess.mrc DensityMap.mrc.relion.postprocessed
PostProcess/job021/postprocess_masked.mrc DensityMap.mrc.relion.postprocessed.masked 
PostProcess/job021/logfile.pdf LogFile.pdf.relion.postprocess 
PostProcess/job021/postprocess.star ProcessData.star.relion.postprocess 
CtfRefine/job022/logfile.pdf LogFile.pdf.relion.ctfrefine 
CtfRefine/job022/particles_ctf_refine.star ParticlesData.star.relion.ctfrefine
CtfRefine/job023/logfile.pdf LogFile.pdf.relion.ctfrefine 
CtfRefine/job023/particles_ctf_refine.star ParticlesData.star.relion.anisomagrefine
CtfRefine/job024/logfile.pdf LogFile.pdf.relion.ctfrefine 
CtfRefine/job024/particles_ctf_refine.star ParticlesData.star.relion.ctfrefine
Refine3D/job025/run_data.star ParticlesData.star.relion.refine3d 
Refine3D/job025/run_half1_class001_unfil.mrc DensityMap.mrc.relion.refine3d.halfmap 
Refine3D/job025/run_class001.mrc DensityMap.mrc.relion.refine3d 
PostProcess/job026/postprocess.mrc DensityMap.mrc.relion.postprocessed 
PostProcess/job026/postprocess_masked.mrc DensityMap.mrc.relion.postprocessed.masked 
PostProcess/job026/logfile.pdf LogFile.pdf.relion.postprocess 
PostProcess/job026/postprocess.star ProcessData.star.relion.postprocess 
Polish/job027/opt_params_all_groups.txt ProcessData.txt.relion.polishparams 
Polish/job028/logfile.pdf LogFile.pdf.relion.polish 
Polish/job028/shiny.star ParticlesData.star.relion.polished 
Refine3D/job029/run_data.star ParticlesData.star.relion.refine3d 
Refine3D/job029/run_half1_class001_unfil.mrc DensityMap.mrc.relion.refine3d.halfmap 
Refine3D/job029/run_class001.mrc DensityMap.mrc.relion.refine3d 
PostProcess/job030/postprocess.mrc DensityMap.mrc.relion.postprocessed 
PostProcess/job030/postprocess_masked.mrc DensityMap.mrc.relion.postprocessed.masked  
PostProcess/job030/logfile.pdf LogFile.pdf.relion.postprocess
PostProcess/job030/postprocess.star ProcessData.star.relion.postprocess 
LocalRes/job031/relion_locres_filtered.mrc DensityMap.mrc.relion.localresfiltered 
LocalRes/job031/relion_locres.mrc Image3D.mrc.relion.localres 
 

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_pipeline_input_edges

loop_ 
_rlnPipeLineEdgeFromNode #1 
_rlnPipeLineEdgeProcess #2 
Import/job001/movies.star MotionCorr/job002/ 
MotionCorr/job002/corrected_micrographs.star CtfFind/job003/ 
MotionCorr/job002/corrected_micrographs.star CtfFind/job003/ 
CtfFind/job003/micrographs_ctf.star ManualPick/job004/ 
ManualPick/job004/coords_suffix_manualpick.star Select/job005/ 
Select/job005/micrographs_selected.star AutoPick/job006/ 
CtfFind/job003/micrographs_ctf.star Extract/job007/ 
AutoPick/job006/coords_suffix_autopick.star Extract/job007/ 
Select/job005/micrographs_selected.star Extract/job007/ 
Extract/job007/particles.star Class2D/job008/ 
Class2D/job008/run_it025_optimiser.star Select/job009/ 
Select/job005/micrographs_selected.star AutoPick/job010/ 
Select/job009/class_averages.star AutoPick/job010/ 
CtfFind/job003/micrographs_ctf.star AutoPick/job011/ 
Select/job009/class_averages.star AutoPick/job011/ 
Select/job005/micrographs_selected.star Extract/job012/ 
AutoPick/job011/coords_suffix_autopick.star Extract/job012/ 
CtfFind/job003/micrographs_ctf.star Extract/job012/ 
Extract/job012/particles.star Class2D/job013/ 
Class2D/job013/run_it025_optimiser.star Select/job014/ 
Select/job014/particles.star InitialModel/job015/ 
Select/job014/particles.star Class3D/job016/ 
InitialModel/job015/run_it150_class001_symD2.mrc Class3D/job016/ 
Class3D/job016/run_it025_optimiser.star Select/job017/ 
CtfFind/job003/micrographs_ctf.star Extract/job018/ 
Select/job017/particles.star Extract/job018/ 
Extract/job018/particles.star Refine3D/job019/ 
Class3D/job016/run_it025_class001_box256.mrc Refine3D/job019/ 
Refine3D/job019/run_class001.mrc MaskCreate/job020/ 
MaskCreate/job020/mask.mrc PostProcess/job021/ 
Refine3D/job019/run_half1_class001_unfil.mrc PostProcess/job021/ 
Refine3D/job019/run_data.star CtfRefine/job022/ 
Refine3D/job019/run_data.star CtfRefine/job023/ 
CtfRefine/job022/particles_ctf_refine.star CtfRefine/job023/ 
CtfRefine/job023/particles_ctf_refine.star CtfRefine/job024/ 
CtfRefine/job024/particles_ctf_refine.star Refine3D/job025/ 
Refine3D/job019/run_class001.mrc Refine3D/job025/ 
MaskCreate/job020/mask.mrc PostProcess/job026/ 
Refine3D/job025/run_half1_class001_unfil.mrc PostProcess/job026/ 
Refine3D/job025/run_data.star Polish/job027/ 
Refine3D/job025/run_data.star Polish/job028/ 
Polish/job028/shiny.star Refine3D/job029/ 
Refine3D/job025/run_class001.mrc Refine3D/job029/ 
MaskCreate/job020/mask.mrc Refine3D/job029/ 
MaskCreate/job020/mask.mrc PostProcess/job030/ 
Refine3D/job029/run_half1_class001_unfil.mrc PostProcess/job030/ 
Refine3D/job029/run_half1_class001_unfil.mrc LocalRes/job031/ 
 

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_pipeline_output_edges

loop_ 
_rlnPipeLineEdgeProcess #1 
_rlnPipeLineEdgeToNode #2 
Import/job001/ Import/job001/movies.star 
MotionCorr/job002/ MotionCorr/job002/corrected_micrographs.star 
MotionCorr/job002/ MotionCorr/job002/logfile.pdf 
CtfFind/job003/ CtfFind/job003/micrographs_ctf.star 
CtfFind/job003/ CtfFind/job003/logfile.pdf 
ManualPick/job004/ ManualPick/job004/coords_suffix_manualpick.star 
ManualPick/job004/ ManualPick/job004/micrographs_selected.star 
Select/job005/ Select/job005/micrographs_selected.star 
AutoPick/job006/ AutoPick/job006/coords_suffix_autopick.star 
AutoPick/job006/ AutoPick/job006/logfile.pdf 
Extract/job007/ Extract/job007/particles.star 
Class2D/job008/ Class2D/job008/run_it025_data.star 
Class2D/job008/ Class2D/job008/run_it025_optimiser.star 
Select/job009/ Select/job009/particles.star 
Select/job009/ Select/job009/class_averages.star 
AutoPick/job010/ AutoPick/job010/coords_suffix_autopick.star 
AutoPick/job010/ AutoPick/job010/logfile.pdf 
AutoPick/job011/ AutoPick/job011/coords_suffix_autopick.star 
AutoPick/job011/ AutoPick/job011/logfile.pdf 
Extract/job012/ Extract/job012/particles.star 
Class2D/job013/ Class2D/job013/run_it025_data.star 
Class2D/job013/ Class2D/job013/run_it025_optimiser.star 
Select/job014/ Select/job014/particles.star 
Select/job014/ Select/job014/class_averages.star 
InitialModel/job015/ InitialModel/job015/run_it150_data.star 
InitialModel/job015/ InitialModel/job015/run_it150_optimiser.star 
InitialModel/job015/ InitialModel/job015/run_it150_class001.mrc 
InitialModel/job015/ InitialModel/job015/run_it150_class001_symD2.mrc 
Class3D/job016/ Class3D/job016/run_it025_data.star 
Class3D/job016/ Class3D/job016/run_it025_optimiser.star 
Class3D/job016/ Class3D/job016/run_it025_class001.mrc 
Class3D/job016/ Class3D/job016/run_it025_class002.mrc 
Class3D/job016/ Class3D/job016/run_it025_class003.mrc 
Class3D/job016/ Class3D/job016/run_it025_class004.mrc 
Class3D/job016/ Class3D/job016/run_it025_class001_box256.mrc 
Select/job017/ Select/job017/particles.star 
Extract/job018/ Extract/job018/particles.star 
Extract/job018/ Extract/job018/coords_suffix_extract.star 
Refine3D/job019/ Refine3D/job019/run_data.star 
Refine3D/job019/ Refine3D/job019/run_half1_class001_unfil.mrc 
Refine3D/job019/ Refine3D/job019/run_class001.mrc 
MaskCreate/job020/ MaskCreate/job020/mask.mrc 
PostProcess/job021/ PostProcess/job021/postprocess.mrc 
PostProcess/job021/ PostProcess/job021/postprocess_masked.mrc 
PostProcess/job021/ PostProcess/job021/logfile.pdf 
PostProcess/job021/ PostProcess/job021/postprocess.star 
CtfRefine/job022/ CtfRefine/job022/logfile.pdf 
CtfRefine/job022/ CtfRefine/job022/particles_ctf_refine.star 
CtfRefine/job023/ CtfRefine/job023/logfile.pdf 
CtfRefine/job023/ CtfRefine/job023/particles_ctf_refine.star 
CtfRefine/job024/ CtfRefine/job024/logfile.pdf 
CtfRefine/job024/ CtfRefine/job024/particles_ctf_refine.star 
Refine3D/job025/ Refine3D/job025/run_data.star 
Refine3D/job025/ Refine3D/job025/run_half1_class001_unfil.mrc 
Refine3D/job025/ Refine3D/job025/run_class001.mrc 
PostProcess/job026/ PostProcess/job026/postprocess.mrc 
PostProcess/job026/ PostProcess/job026/postprocess_masked.mrc 
PostProcess/job026/ PostProcess/job026/logfile.pdf 
PostProcess/job026/ PostProcess/job026/postprocess.star 
Polish/job027/ Polish/job027/opt_params_all_groups.txt 
Polish/job028/ Polish/job028/logfile.pdf 
Polish/job028/ Polish/job028/shiny.star 
Refine3D/job029/ Refine3D/job029/run_data.star 
Refine3D/job029/ Refine3D/job029/run_half1_class001_unfil.mrc 
Refine3D/job029/ Refine3D/job029/run_class001.mrc 
PostProcess/job030/ PostProcess/job030/postprocess.mrc 
PostProcess/job030/ PostProcess/job030/postprocess_masked.mrc 
PostProcess/job030/ PostProcess/job030/logfile.pdf 
PostProcess/job030/ PostProcess/job030/postprocess.star 
LocalRes/job031/ LocalRes/job031/relion_locres_filtered.mrc 
LocalRes/job031/ LocalRes/job031/relion_locres.mrc 