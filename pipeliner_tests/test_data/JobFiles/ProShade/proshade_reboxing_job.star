
# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_job

_rlnJobTypeLabel    proshade.map_utilities.rebox
_rlnJobIsContinue    0
_rlnJobIsTomo    0

# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
  'do_queue'        No 
 'input_map'        'test.mrc' 
 'output_map'       'test_reboxed.mrc' 
 'add_space'        True
'extra_space'       1.0
'min_dedicated'      1 
'other_args'         '' 
        qsub         qsub 
  qsubscript        'sub.sh' 
   queuename      openmpi 
 
