
# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_job

_rlnJobTypeLabel    proshade.map_utilities.overlay
_rlnJobIsContinue    0
_rlnJobIsTomo    0

# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
  'do_queue'        No 
 'static_struct'        'test1.mrc' 
 'moving_struct'        'test2.pdb' 
 'output_map'       'test2_overlaid.pdb' 
 'add_space'        False
'extra_space'       20.0
'min_dedicated'      1 
'other_args'         '' 
        qsub         qsub 
  qsubscript        'sub.sh' 
   queuename      openmpi 
 
