# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_job

_rlnJobTypeLabel             relion.postprocess
 
_rlnJobIsContinue                       0
 
_rlnJobIsTomo                           0

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
     'fn_in'           '' 
   'fn_mask'           '' 
      angpix            1 
'do_auto_bfac'          Yes 
'autob_lowres'           10 
'do_adhoc_bfac'           No 
'adhoc_bfac'        -1000 
    'fn_mtf'           '' 
'mtf_angpix'          1.0 
'do_skip_fsc_weighting'           No 
  'low_pass'            5 
  'do_queue'           No 
   queuename      openmpi 
        qsub         qsub 
  qsubscript '' 
'min_dedicated'            1 
'other_args'           '' 
 