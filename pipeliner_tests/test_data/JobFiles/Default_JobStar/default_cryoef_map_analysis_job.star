
# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_job

_rlnJobTypeLabel    cryoef.map_analysis
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'angular_acc'           -1 
  'b_factor'         -300 
'box_size_px'           -1 
  'do_queue'           No 
'input_file'           '' 
'max_tilt_angle'           -1 
'min_dedicated'            1 
'other_args'           '' 
'particle_diameter_ang'           -1 
        qsub         qsub 
  qsubscript           '' 
   queuename      openmpi 
  resolution           -1 
    symmetry           '' 
 
