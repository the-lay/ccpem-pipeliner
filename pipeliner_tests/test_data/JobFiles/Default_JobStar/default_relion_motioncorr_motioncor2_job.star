# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_job

_rlnJobTypeLabel    relion.motioncorr.motioncor2
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo                           0

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'bin_factor'            1 
'do_dose_weighting'          Yes
  'do_queue'           No
'do_save_noDW'           No 
'dose_per_frame'            1 
'eer_grouping'           32 
'first_frame_sum'            1 
 'fn_defect'           '' 
'fn_gain_ref'           '' 
'fn_motioncor2_exe' 'Enter path to MOTIONCOR2 executable' 
 'gain_flip' 'No flipping (0)' 
  'gain_rot' 'No rotation (0)' 
   'gpu_ids'            0 
'group_frames'            1 
'input_star_mics'           '' 
'last_frame_sum'           -1 
'min_dedicated'            1 
    'nr_mpi'            1 
'mpi_command'       'mpirun -n XXXmpinodesXXX'
'nr_threads'            1
'other_args'           '' 
'other_motioncor2_args'           '' 
   'patch_x'            1 
   'patch_y'            1 
'pre_exposure'            0 
     bfactor          150 
        qsub         qsub 
  qsubscript '' 
   queuename      openmpi
do_own_motioncor   No
'do_float16'        No
