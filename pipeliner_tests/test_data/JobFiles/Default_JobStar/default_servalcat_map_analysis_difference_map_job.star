
# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_job

_rlnJobTypeLabel    servalcat.map_analysis.difference_map
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
  'do_queue'           No 
'input_half_map1' '' 
'input_half_map2' '' 
'input_ligand'           '' 
'input_mask' '' 
'input_model' '' 
'min_dedicated'            1 
        qsub         qsub
  qsubscript           '' 
   queuename      openmpi 
  resolution          -1 
 
