# version 30001 / CCP-EM pipeliner

data_job

_rlnJobTypeLabel                           relion.reproject
_rlnJobIsContinue                       0
_rlnJobIsTomo				0

# version 30001 / CCP-EM pipeliner

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2
map_filename		emd_3488.mrc
angles_filename		reproj_angles.star
nr_uniform			10
apix				1.07
do_queue         	Yes 
min_dedicated       1 
qsub       			qsub 
qsubscript 			submission_script.sh 
queuename    		openmpi 
other_args         	"" 
qsub_extra_1		extra_moja
qsub_extra_2		extra_mbili
qsub_extra_3		extra_tatu
qsub_extra_4		extra_nne