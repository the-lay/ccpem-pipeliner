
# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_job

_rlnJobTypeLabel    icebreaker.micrograph_analysis.micrographs
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
  'do_queue'           No 
   'in_mics'           'MotionCorr/job002/corrected_micrographs.star' 
'min_dedicated'            1 
'nr_threads'            8 
'other_args'           '' 
        qsub         qsub 
  qsubscript '' 
   queuename      openmpi 
 
