# version 30001

data_job

_rlnJobTypeLabel                   relion.select
_rlnJobIsContinue                       0
_rlnJobIsTomo				0

# version 30001

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
discard_label rlnImageName 
discard_sigma          4 
rank_threshold			0.5
do_rank_relative		No 
do_discard         No 
  do_queue         No 
 do_random         No 
do_recenter        No 
do_regroup         No 
do_remove_duplicates         No 
do_select_values         No 
  do_split         No 
duplicate_threshold         30 
   fn_data         "" 
    fn_mic         ""
  fn_model  		test_optimiser.star
image_angpix         -1 
min_dedicated          1 
 nr_groups          1 
  nr_split         -1 
other_args         "" 
      qsub       qsub 
qsubscript "" 
 queuename    openmpi 
select_label rlnCtfFigureOfMerit 
select_maxval      23456. 
select_minval     12345. 
split_size        100 