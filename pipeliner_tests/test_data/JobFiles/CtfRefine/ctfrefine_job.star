# version 30001

data_job

_rlnJobTypeLabel                        relion.ctfrefine.anisomag
_rlnJobIsContinue                       0
_rlnJobIsTomo				0

# version 30001

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
do_queue	No
   fn_data Refine3D/job600/run_data.star 
   fn_post PostProcess/job700/postprocess.star 
min_dedicated          1 
    minres         29 
    nr_mpi         16 
nr_threads          8 
other_args         "" 
      qsub       qsub 
qsubscript "" 
 queuename    openmpi 