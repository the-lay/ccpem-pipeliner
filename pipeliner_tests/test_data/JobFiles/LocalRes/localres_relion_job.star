
# version 30001

data_job

_rlnJobTypeLabel                       relion.localres.own
_rlnJobIsContinue                       0
_rlnJobIsTomo				0

# version 30001

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
adhoc_bfac       -30 
    angpix          1.244 
  do_queue         No 
     fn_in Refine3D/job029/run_half1_class001_unfil.mrc 
   fn_mask   	   "" 
    fn_mtf         "" 
min_dedicated          1 
    nr_mpi          12 
nr_threads          1 
other_args         "" 
      qsub       qsub 
qsubscript "" 
 queuename    openmpi 
   stepres          1 