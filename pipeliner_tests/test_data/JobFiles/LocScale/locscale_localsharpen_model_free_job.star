
# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_job

_rlnJobTypeLabel    locscale.postprocess.local_sharpening
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
 'input_map'           'Import/job001/emd_3488.mrc' 
'input_mask'           '' 
'input_model'           '' 
  resolution       7.0
  n_nodes           1
  model_free        True
