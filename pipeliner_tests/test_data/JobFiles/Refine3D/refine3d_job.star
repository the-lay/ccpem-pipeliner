# version 30001

data_job

_rlnJobTypeLabel                    relion.refine3d
_rlnJobIsContinue                       0
_rlnJobIsTomo				0

# version 30001

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
auto_faster         No 
auto_local_sampling "1.8 degrees" 
ctf_corrected_ref        Yes 
ctf_intact_first_peak         No 
do_combine_thru_disc         No 
do_ctf_correction        Yes 
   do_pad1         No 
do_parallel_discio        Yes 
do_preread_images        Yes 
  do_queue         No 
do_solvent_fsc         No 
do_zero_mask        Yes 
   fn_cont         "" 
    fn_img Extract/job018/particles.star 
   fn_mask         "" 
    fn_ref Class3D/job016/run_it025_class001_box256.mrc 
   gpu_ids    4:5:6:7 
  ini_high         50 
min_dedicated          1 
    nr_mpi          5 
   nr_pool          30 
nr_threads          6 
offset_range          5 
offset_step          1 
other_args         "" 
particle_diameter        200 
      qsub       qsub 
qsubscript "" 
 queuename    openmpi 
ref_correct_greyscale         No 
  sampling "7.5 degrees" 
scratch_dir         "" 
skip_gridding        Yes 
  sym_name         D2 
   use_gpu        Yes
   relax_sym    ""