
# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_job

_rlnJobTypeLabel    cinderella.select.class_averages
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
    'do_gpu'           Yes
  'do_queue'           No 
'input_classes'           'my_classes.mrcs'
'min_dedicated'            1 
'other_args'           '' 
'trained_model'           'my_model.h5'
         gpu           '1'
        qsub         qsub 
  qsubscript           '' 
   queuename      openmpi 
      weight          0.7 
 
