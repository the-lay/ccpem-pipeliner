# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_job

_rlnJobTypeLabel    relion.ctffind.ctffind4
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo                           0

# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
   'ctf_win'           -1 
'do_phaseshift'           No 
  'do_queue'           Yes 
'fn_ctffind_exe' 'Enter path to CTFFIND executable' 
'input_star_mics'           '' 
'min_dedicated'            1 
    'nr_mpi'            1 
'other_args'           '' 
 'phase_max'          90 
 'phase_min'            0 
'phase_step'           11 
'slow_search'           No 
'use_given_ps'           No 
  'use_noDW'           No 
         box          512 
        dast          100 
       dfmax        50000 
       dfmin         5000 
      dfstep          500 
        qsub         qsub 
  qsubscript '' 
   queuename      openmpi 
      resmax            5 
      resmin           30 