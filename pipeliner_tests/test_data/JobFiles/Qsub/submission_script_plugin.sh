#$ -cwd -V
#$ -l h_rt=96:00:00     # specifiy max run time here
#$ -m be
#$ -e Reproject/job001/run.err
#$ -o Reproject/job001/run.out
#$ -P openmpi
#$  -M extra_moja    # put your email address here 
#$  -l coproc_v100=4   # GPUS in relion should be left blank
 
## load modules 
module load intel/19.0.4 cuda/10.1.168

## Here are some more extra variables
extra_mbili
extra_tatu

## set library paths
export PATH=$CUDA_HOME/bin:$PATH
export LD_LIBRARY_PATH=$CUDA_HOME/lib64:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/home/ufaserv1_k/fbscem/relion_arc/arc4/relion/build/lib/:

## print some diagnostic info 
module list
nvidia-smi -L
which relion_refine_mpi

## run relion

let NSLOTS=NSLOTS/2
relion_project --i emd_3488.mrc --ang reproj_angles.star --angpix 1.07 --o Reproject/job001/reproj --pipeline_control Reproject/job001/
XXXCHECK_COMMAND_PATHXXX Reproject/job001 reproj.mrcs reproj.star

#One more extra command for good measure 
extra_nne
dedicated nodes: 1
output name: Reproject/job001/
