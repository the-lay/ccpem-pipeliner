
# version 30001

data_pipeline_general

_rlnPipeLineJobCounter                      3


# version 30001

data_pipeline_processes

loop_
_rlnPipeLineProcessName #1
_rlnPipeLineProcessAlias #2
_rlnPipeLineProcessTypeLabel #3
_rlnPipeLineProcessStatusLabel #4
Import/job001/          None pipeliner.fetch.emdb Succeeded
Refine3D/job002/        None  relion.refine3d  Succeeded

# version 30001

data_pipeline_nodes

loop_
_rlnPipeLineNodeName #1
_rlnPipeLineNodeTypeLabel #2
Import/job001/emd_0234.mrc DensityMap.mrc.from_emdb
Refine3D/job002/run_data.star ParticlesData.star.relion.refine3d
Refine3D/job002/run_optimiser.star ProcessData.star.relion.optimiser.refine3d
Refine3D/job002/run_half1_class001_unfil.mrc DensityMap.mrc.relion.halfmap.refine3d
Refine3D/job002/run_class001.mrc DensityMap.mrc.relion.refine3d

# version 30001

data_pipeline_input_edges

loop_
_rlnPipeLineEdgeFromNode #1
_rlnPipeLineEdgeProcess #2
Import/job001/emd_0234.mrc Refine3D/job002/



# version 30001

data_pipeline_output_edges

loop_
_rlnPipeLineEdgeProcess #1
_rlnPipeLineEdgeToNode #2
Import/job001/ Import/job001/emd_0234.mrc
Refine3D/job002/ Refine3D/job002/run_data.star
Refine3D/job002/ Refine3D/job002/run_optimiser.star
Refine3D/job002/ Refine3D/job002/run_half1_class001_unfil.mrc
Refine3D/job002/ Refine3D/job002/run_class001.mrc
