
# version 30001

data_job

_rlnJobType                            12
_rlnJobIsContinue                       0
_rlnJobIsTomo				0

# version 30001

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
    angpix         -1 
  do_helix         No 
  do_queue         No 
extend_inimask          0 
     fn_in Refine3D/job019/run_class001.mrc 
helical_z_percentage         30 
inimask_threshold      0.005 
lowpass_filter         15 
min_dedicated         24 
nr_threads         12 
other_args         "" 
      qsub       qsub 
qsubscript "" 
 queuename    openmpi 
width_mask_edge          6 