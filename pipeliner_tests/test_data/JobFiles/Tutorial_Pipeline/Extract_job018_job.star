
# version 30001

data_job

_rlnJobType                             5
_rlnJobIsContinue                       0
_rlnJobIsTomo				0

# version 30001

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
    angpix          1 
bg_diameter        200 
black_dust         -1 
coords_suffix         "" 
do_cut_into_segments        Yes 
do_extract_helical_tubes        Yes 
do_extract_helix         No 
 do_invert        Yes 
   do_norm        Yes 
  do_queue         No 
do_recenter        Yes 
do_reextract        Yes 
do_rescale        Yes 
do_reset_offsets         No 
do_set_angpix         No 
extract_size        360 
fndata_reextract Select/job017/particles.star 
helical_bimodal_angular_priors        Yes 
helical_nr_asu          1 
helical_rise          1 
helical_tube_outer_diameter        200 
min_dedicated         24 
    nr_mpi         12 
other_args         "" 
      qsub       qsub 
qsubscript "" 
 queuename    openmpi 
recenter_x          0 
recenter_y          0 
recenter_z          0 
   rescale        256 
 star_mics CtfFind/job003/micrographs_ctf.star 
white_dust         -1 