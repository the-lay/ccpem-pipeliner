
# version 30001

data_job

_rlnJobType                            18
_rlnJobIsContinue                       0
_rlnJobIsTomo				0

# version 30001

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
ctf_intact_first_peak         No 
do_combine_thru_disc         No 
do_ctf_correction        Yes 
   do_pad1         No 
do_parallel_discio        Yes 
do_preread_images        Yes 
  do_queue         No 
do_solvent        Yes 
   fn_cont         "" 
    fn_img Select/job014/particles.star 
   gpu_ids    4:5:6:7 
min_dedicated         24 
nr_classes          1 
    nr_mpi          5 
   nr_pool         30 
nr_threads          6 
offset_range          6 
offset_step          2 
other_args         "" 
particle_diameter        200 
      qsub       qsub 
qsubscript "" 
 queuename    openmpi 
  sampling 15__degrees 
scratch_dir         "" 
sgd_fin_iter         25 
sgd_fin_resol         15 
sgd_fin_subset_size        500 
sgd_inbetween_iter        100 
sgd_ini_iter         25 
sgd_ini_resol         35 
sgd_ini_subset_size        100 
sgd_sigma2fudge_halflife         -1 
sgd_write_iter         10 
  sym_name         C1 
   use_gpu        Yes 