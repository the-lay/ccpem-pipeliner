==============
Pipeliner Jobs
==============

Pipeliner jobs
--------------

.. automodule:: pipeliner.pipeliner_job
    :members:
    :undoc-members:
    :show-inheritance:

Display tools
-------------

Use these methods to create ``ResultsDisplayObjects``
used by the pipeliner GUI Doppio to create graphical
outputs for each job.

.. automodule:: pipeliner.display_tools
    :members:
    :undoc-members:
    :show-inheritance:

ResultsDisplay Objects
----------------------
These objects generally should not be instantiated directly
they should instead be created using the functions above.

.. automodule:: pipeliner.results_display_objects
    :members:
    :undoc-members:
    :show-inheritance:


Deposition Objects
------------------

DepositionObjects are returned by a ``PipelinerJob``'s
``prepare_onedep_data`` function and are used to prepare
automated depositions to the PDB, EMDB, and EMPIAR.

EMPIAR DepositionObjects

.. automodule:: pipeliner.deposition_tools.empiar_deposition_objects
    :members:
    :undoc-members:
    :show-inheritance:

PDB/EMDB DepositionObjects

PDB/EMDB Deposition object correspond to the schema
here: http://ftp.ebi.ac.uk/pub/databases/emdb/doc/XML-schemas/emdb-schemas/v3/current_v3/doc/Untitled.html

Do not create the PDB/EMDB ``DepositionObject`` dataclasses directly,
instead use the corresponding 'entry' function.

.. automodule:: pipeliner.deposition_tools.pdb_deposition_objects
    :members:
    :undoc-members:
    :show-inheritance:

Deposition tools
----------------
These functions combine the information from the
DepositionObjects returned by a ``PipelinerJob`` into a
format for automated deposition.

.. automodule:: pipeliner.deposition_tools.onedep_deposition
    :members:
    :undoc-members:
    :show-inheritance: