Writing new jobs
================

New software can be added to the ccpem-pipeliner by writing a
:class:`~pipeliner.pipeliner_job.PipelinerJob`.

A :class:`~pipeliner.pipeliner_job.PipelinerJob` is completely self contained and has
everything needed to run a new piece of software in the pipeliner framework. It must
accomplish the following tasks:

1) Define the job's input parameters and how they are displayed in the GUI

2) Define how the parameters will be validated

3) Generate the commands that need to be run

4) Define the job’s output nodes (i.e. the output files that are displayed in the GUI
   and made available as inputs to other jobs in the pipeline)

5) Perform any final tasks that need to happen after the commands have been executed

6) Make objects that allow the GUI to display the job’s results graphically

7) Gather metadata about the job

8) Define how to clean up the job

9) Create the objects necessary to create a PDB/EMDB/EMPIAR deposition

.. note::
 A minimal :class:`~pipeliner.pipeliner_job.PipelinerJob` only needs to define input
 parameters, define output nodes, and generate commands. Adding the additional
 methods will add functionality to the job and integrate it into Doppio, the
 pipeliner GUI.

Making a new ``PipelinerJob``
-----------------------------
Required imports:

.. code-block::  python

 from pipeliner.pipeliner_job import PipelinerJob, Ref, ExternalProgram
 from pipeliner.job_options import StringJobOption # and other types as necessary
 from pipeliner.node_factory import create_node
 from pipeliner.display_tools import create_results_display_object

Make a class for the new job:

.. code-block:: python

 class MyJob(PipelinerJob):
     PROCESS_NAME = "software.function.keywords"
     OUT_DIR = "MyJob"

     def __init__(self):
         super().__init__()  # don't forget to initialize the PipelinerJob superclass first

``PROCESS_NAME`` is the name the pipeliner will use to identify the job, and should
match the job type name that is used when the job is added to the ``setup.cfg`` file
(see :ref:`"Adding the new job to the pipeliner" <adding entry points>` below).

``OUT_DIR`` is both the directory where the job's output directory (``jobNNN/``) will be
written and is used to group jobs together in the GUI.

Some examples:

.. code-block:: python

  class RelionLogAutopick(PipelinerJob):
      PROCESS_NAME = "relion.autopick.log"
      OUT_DIR = "AutoPick"

  class RelionTrainTopaz(PipelinerJob):
      PROCESS_NAME = "relion.autopick.topaz.train"
      OUT_DIR = "AutoPick"

  class CryoloAutopick(PipelinerJob):
      PROCESS_NAME = "cryolo.autopick"
      OUT_DIR = "AutoPick"


``__init__`` method
-------------------

The job's ``__init__`` needs to do the following things:
 - define information about the job
 - define the parameters for the job

Define information about the job
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Information about the job and the programs it needs to run are stored in its
:class:`~pipeliner.pipeliner_job.JobInfo` object. For example:

.. code-block:: python

 self.jobinfo.display_name = "RELION initial model generation
 self.jobinfo.version = "0.1"
 self.jobinfo.programs = [ExternalProgram(command="relion_refine")]
 self.jobinfo.short_desc = "Create a de novo 3D initial model"
 self.jobinfo.long_desc = "Relion 4.0 uses a gradient-driven algorithm to generate a de novo 3D"
 self.jobinfo.references = [
     Ref(
         authors=["Scheres SHW"],
         title="RELION: implementation of a Bayesian approach to cryo-EM structure”,
         journal="J Struct Biol.",
         year="2012",
         volume="180",
         issue="3",
         pages="519-30",
         doi="10.1016/j.jsb.2012.09.006",
     )
 ]

.. note::

 Each program in :attr:`~pipeliner.pipeliner_job.JobInfo.programs` should be entered
 with an :class:`~pipeliner.pipeliner_job.ExternalProgram` object.
 These programs will be checked for availability, if they are not found in the system ``PATH``
 the GUI will mark the job as unavailable.

Define the job's input parameters
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Input parameters are defined by adding :class:`~pipeliner.job_options.JobOption`
objects to the job's :attr:`~pipeliner.pipeliner_job.PipelinerJob.joboptions` attribute. There are 8 types of
:class:`~pipeliner.job_options.JobOption` :

+---------------------------------------------------------+-----------+--------------------------------------------------+--------------------------------------------------------------------+
| Type                                                    | Data type | Appearance in GUI                                | Usage notes                                                        |
+=========================================================+===========+==================================================+====================================================================+
| :class:`~pipeliner.job_options.StringJobOption`         | ``str``   | Text entry field                                 |                                                                    |
+---------------------------------------------------------+-----------+--------------------------------------------------+--------------------------------------------------------------------+
| :class:`~pipeliner.job_options.FloatJobOption`          | ``float`` | Number entry field                               |                                                                    |
+---------------------------------------------------------+-----------+--------------------------------------------------+--------------------------------------------------------------------+
| :class:`~pipeliner.job_options.IntJobOption`            | ``int``   | Number entry field                               |                                                                    |
+---------------------------------------------------------+-----------+--------------------------------------------------+--------------------------------------------------------------------+
| :class:`~pipeliner.job_options.BooleanJobOption`        | ``bool``  | Checkbox                                         |                                                                    |
+---------------------------------------------------------+-----------+--------------------------------------------------+--------------------------------------------------------------------+
| :class:`~pipeliner.job_options.MultipleChoiceJobOption` | ``str``   | Drawdown list                                    |                                                                    |
+---------------------------------------------------------+-----------+--------------------------------------------------+--------------------------------------------------------------------+
| :class:`~pipeliner.job_options.InputNodeJobOption`      | ``str``   | Text entry field (with options)                  | Files that are already expected to be part of the project          |
+---------------------------------------------------------+-----------+--------------------------------------------------+--------------------------------------------------------------------+
| :class:`~pipeliner.job_options.FileNameJobOption`       | ``str``   | Text entry field (with options) and file browser | Files **NOT** already expected to be part of the Project           |
+---------------------------------------------------------+-----------+--------------------------------------------------+--------------------------------------------------------------------+
| :class:`~pipeliner.job_options.PathJobOption`           | ``str``   | Text entry field                                 | A Path to a file or dir either inside or outside the project       |
+---------------------------------------------------------+-----------+--------------------------------------------------+--------------------------------------------------------------------+

.. note::

 The job option types and GUI behaviour for input nodes and files are currently under
 review and will probably change in the near future. See also :ref:`file paths` below.

Special predefined job options
""""""""""""""""""""""""""""""

There are two methods in :class:`~pipeliner.pipeliner_job.PipelinerJob` that create
special predefined ``JobOptions``.

:meth:`~pipeliner.pipeliner_job.PipelinerJob.make_additional_args`:

This adds a special :class:`~pipeliner.job_options.StringJobOption` called
``other_args``.

This can be used in calling
:meth:`~pipeliner.pipeliner_job.PipelinerJob.parse_additional_args` in the job's
:meth:`~pipeliner.pipeliner_job.PipelinerJob.get_commands` method.
It returns a list of the additional args with the quotation structure preserved.

For example, if the value for ``self.joboptions[“other_args”]`` is
``'arg1 arg2 "arg3 arg4"'``, it would return:  ``['arg1', 'arg2', '"arg3 arg4"']``

:meth:`~pipeliner.pipeliner_job.PipelinerJob.get_runtab_options`:

This adds the job options in RELION’s 'Run' tab:

+---------------+---------------------------------------------------+-------------------------------------------------------------+
| Name          | JobOption type                                    | Description                                                 |
+===============+===================================================+=============================================================+
| nr_mpi        | :class:`~pipeliner.job_options.IntJobOption`      | Number of MPIs to use only; added  if ``mpi=True``          |
+---------------+---------------------------------------------------+-------------------------------------------------------------+
| nr_threads    | :class:`~pipeliner.job_options.StringJobOption`   | Number of threads to use; only added  if ``threads=True``   |
+---------------+---------------------------------------------------+-------------------------------------------------------------+
| do_queue      | :class:`~pipeliner.job_options.BooleanJobOption`  | Should the job be submitted to the queue?                   |
+---------------+---------------------------------------------------+-------------------------------------------------------------+
| queuename     | :class:`~pipeliner.job_options.StringJobOption`   | Name of the queue                                           |
+---------------+---------------------------------------------------+-------------------------------------------------------------+
| qsub          | :class:`~pipeliner.job_options.StringJobOption`   | Queue submission  command to use                            |
+---------------+---------------------------------------------------+-------------------------------------------------------------+
| qsubscript    | :class:`~pipeliner.job_options.PathJobOption`     | Path to template for the submission script                  |
+---------------+---------------------------------------------------+-------------------------------------------------------------+
| min_dedicated | :class:`~pipeliner.job_options.IntJobOption`      | Minimum dedicated cores per node                            |
+---------------+---------------------------------------------------+-------------------------------------------------------------+

``get_commands`` method
-----------------------

The :meth:`~pipeliner.pipeliner_job.PipelinerJob.get_commands` method does two things.
It must return the commands the job will run and define the job's outputs.

Return the commands to run
^^^^^^^^^^^^^^^^^^^^^^^^^^

The commands must be returned in a 'list of lists' format, where each entry in the
outer list is a list of strings (i.e. a command and arguments, suitable for passing to
:func:`subprocess.run`).

.. code-block:: python

 [
     ["first", "command", "to", "run"],
     ["next", "command", "to", "run"],
     ["final", "command", "to", "run"],
 ]

The commands can be assembled in any manner you see fit.  A variety of attributes
of the :class:`~pipeliner.pipeliner_job.PipelinerJob` are available to get pieces of
data during this process:

+------------------------------------------------------------+-------------------------------------------------------------+--------------------------+
| Attribute                                                  | Description                                                 | Data type                |
+============================================================+=============================================================+==========================+
| :attr:`~pipeliner.pipeliner_job.PipelinerJob.output_dir`   | The output dir assigned when the job is run                 | ``str``                  |
+------------------------------------------------------------+-------------------------------------------------------------+--------------------------+
| :attr:`~pipeliner.pipeliner_job.PipelinerJob.joboptions`   | A dict containing all of the JobOptions defined in __init__ | ``dict{str: JobOption}`` |
+------------------------------------------------------------+-------------------------------------------------------------+--------------------------+
| :attr:`~pipeliner.pipeliner_job.PipelinerJob.is_continue`  | Is the job being continued?                                 | ``bool``                 |
+------------------------------------------------------------+-------------------------------------------------------------+--------------------------+
| :attr:`~pipeliner.pipeliner_job.PipelinerJob.is_mpi`       | Does the job use MPI                                        | ``bool``                 |
+------------------------------------------------------------+-------------------------------------------------------------+--------------------------+
| :attr:`~pipeliner.pipeliner_job.PipelinerJob.input_nodes`  | List of the the job's ``InputNodes``                        | ``list[Node]``           |
+------------------------------------------------------------+-------------------------------------------------------------+--------------------------+
| :attr:`~pipeliner.pipeliner_job.PipelinerJob.output_nodes` | List of the The job's ``OutputNodes``                       | ``list[Node]``           |
+------------------------------------------------------------+-------------------------------------------------------------+--------------------------+

Define the outputs
^^^^^^^^^^^^^^^^^^

The second task that needs tobe performed by
:meth:`~pipeliner.pipeliner_job.PipelinerJob.get_commands` is to define the outputs of
the job. The outputs of the job are defined by adding
:class:`~pipeliner.nodes.Node` objects to
:attr:`~pipeliner.pipeliner_job.PipelinerJob.output_nodes`.

Example of defining a :class:`~pipeliner.nodes.Node`:

.. code-block:: python

 input_file = self.joboptions["my_input"].get_string()
 self.output_nodes.append(create_node(input_file, "nodetype.ext.kwds"))

.. _file paths:

File paths in pipeliner jobs
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The project directory is the root for all the pipeliner's file handling: input and
output files are stored as relative paths from the project directory, and jobs are run
with the project directory as their working directory.

Jobs should be self-contained, i.e. they should write files only into their own job
directory, :attr:`~pipeliner.pipeliner_job.PipelinerJob.output_dir` (which will have
the form ``JobType/jobNNN/``, for example ``Import/job001/``). The job's commands
should take care to direct their output files only to this directory (or subdirectories
within it) and not elsewhere in the project.

For example:

.. code-block:: python

  commands = [
     ["touch", os.path.join(self.output_dir, "output_file_1")],  # this is fine
     ["cp", input_file_1, input_file_2, self.output_dir],        # this is also fine

     # The next command is NOT fine - it would write the file directly in the project directory
     ["touch", "output_file_2"],
 ]

.. note::

 At the time :meth:`~pipeliner.pipeliner_job.PipelinerJob.get_commands` is called,
 :attr:`~pipeliner.pipeliner_job.PipelinerJob.output_dir` has been assigned but the
 directory does not exist yet. Your job should not need to write anything in the job
 directory at this stage. If it does, the thing you're trying to do is probably better
 done in one of the job's commands or possibly in
 :meth:`~pipeliner.pipeliner_job.PipelinerJob.post_run_actions`.

The only exception to the rule that all paths should be relative and within the project
is where a job needs to access some centrally-installed file or disk using an absolute
path, for example a program executable, queue submission script template or local
scratch disk. In these cases, absolute paths can be used but the files should be
treated as external to the project and not added as input or output nodes.

Some uncooperative programs do not have the ability to specify where their results are
written. In these cases the location from which the commands are executed can be
changed by setting the :attr:`~pipeliner.pipeliner_job.PipelinerJob.working_dir`
attribute. The most common use case is programs that write their outputs directly into
the working directory, in which case
:attr:`~pipeliner.pipeliner_job.PipelinerJob.working_dir` should be set to
:attr:`~pipeliner.pipeliner_job.PipelinerJob.output_dir`.

If :attr:`~pipeliner.pipeliner_job.PipelinerJob.working_dir` has been set then it is
imperative that any references to other files in the
:class:`~pipeliner.pipeliner_job.PipelinerJob` functions are given relative to the
working directory rather than the project directory. The easiest way to accomplish this
is by using :func:`os.path.relpath`. For example, in a pipeliner job the path to an
input file is typically found using something like this:
``input_file = self.joboptions["input_file"].get_string()``. Normally ``input_file``
can then be used directly, but in a job where
:attr:`~pipeliner.pipeliner_job.PipelinerJob.working_dir` is set
``os.path.relpath(input_file, self.working_dir)`` should be used instead.

``parameter_validation`` method
-------------------------------

The :meth:`~pipeliner.pipeliner_job.PipelinerJob.parameter_validation` method serves
to do advanced validation of the ``JobOptions`` before a job is run.
Simple validation is done automatically:

 - parameters that are required have non-empty values
 - parameter values are of the right type
 - parameter values are within their specified ranges

:meth:`~pipeliner.pipeliner_job.PipelinerJob.parameter_validation` is used for more
specific validation tasks that take into account more than one job option such as:

 - parameter A must be > parameter B
 - parameters C, D, and E cannot all be equal

The method should return a list of
:class:`~pipeliner.job_options.JobOptionValidationResult` objects.

Some examples:

.. code-block:: python

 def parameter_validation(self):
   errors = []

   jobop_a = self.joboptions["param_a"]
   jobop_b = self.joboptions["param_b"]
   if jobop_a.get_number() < jobop_b.get_number():
     errors.append(
       JobOptionValidationResult(
         type="error",
         raised_by=[jobop_a, jobop_b],
         message="A must be greater than B",
       )
     )

   jobop_c = self.joboptions["param_c"].get_string()
   jobop_d = self.joboptions["param_d"].get_string()
   jobop_e = self.joboptions["param_d"].get_string()

   if jobop_c == jobop_d == jobop_e:
     errors.append(
       JobOptionValidationResult(
         type="error",
         raised_by=[jobop_c, jobop_d],
         message="C, D, and E cannot all be the same!",
       )
     )

   return errors

``post_run_actions`` method
---------------------------

The :meth:`~pipeliner.pipeliner_job.PipelinerJob.post_run_actions` method does
anything that needs to happen after the job has run. This is usually nothing but, for
example, if the program doesn’t know the name or number of its output nodes until it
has finished, they can be created here.

Some jobs also use this method to clean up after themselves, for example deleting
stray files, but this is discouraged. If at all possible, write the job so it doesn't
need cleaning up in the first place!

Example of a :meth:`~pipeliner.pipeliner_job.PipelinerJob.post_run_actions` method: In
this case the job doesn't know the number of outputs it will produce until after it has
executed the commands so they must be created
*ex post facto* by :meth:`~pipeliner.pipeliner_job.PipelinerJob.post_run_actions`:

.. code-block:: python

 def post_run_actions(self):
   outputs = glob(self.output_name + "result_*.mrc")
     for f in outputs:
       self.output_nodes.append(create_node(f, "DensityMap.mrc.myjob"))

``create_results_display`` method
---------------------------------

The :meth:`~pipeliner.pipeliner_job.PipelinerJob.create_results_display` method
generates result display objects that allow the Doppio GUI to display results from the
pipeliner.

There are currently 9 types of
:class:`~pipeliner.results_display_objects.ResultsDisplayObject`:

+---------------------------------------------------------------------+--------------------------------------------------------------+
| Type                                                                | Description                                                  |
+---------------------------------------------------------------------+--------------------------------------------------------------+
| :class:`~pipeliner.results_display_objects.ResultsDisplayText`      | A single line of text                                        |
+---------------------------------------------------------------------+--------------------------------------------------------------+
| :class:`~pipeliner.results_display_objects.ResultsDisplayMontage`   | An array of thumbnail images                                 |
+---------------------------------------------------------------------+--------------------------------------------------------------+
| :class:`~pipeliner.results_display_objects.ResultsDisplayGraph`     | A plot with points, lines, or both                           |
+---------------------------------------------------------------------+--------------------------------------------------------------+
| :class:`~pipeliner.results_display_objects.ResultsDisplayImage`     |                                                              |
+---------------------------------------------------------------------+--------------------------------------------------------------+
| :class:`~pipeliner.results_display_objects.ResultsDisplayHistogram` |                                                              |
+---------------------------------------------------------------------+--------------------------------------------------------------+
| :class:`~pipeliner.results_display_objects.ResultsDisplayTable`     | Multiple text containing cells with a header row             |
+---------------------------------------------------------------------+--------------------------------------------------------------+
| :class:`~pipeliner.results_display_objects.ResultsDisplayMapModel`  | Integrated 3D viewer for mrc, map, cif, and pdb files        |
+---------------------------------------------------------------------+--------------------------------------------------------------+
| :class:`~pipeliner.results_display_objects.ResultsDisplayRvapi`     |  A general Object that displays a webpage                    |
+---------------------------------------------------------------------+--------------------------------------------------------------+
| :class:`~pipeliner.results_display_objects.ResultsDisplayPending`   | A special class of ``ResultsDisplayObject`` generated        |
|                                                                     | when there has been an error. Doppio attempts to update      |
|                                                                     | any ``ResultsDisplayPending`` objects when a job is viewed.  |
+---------------------------------------------------------------------+--------------------------------------------------------------+

They should all be created with the
:func:`pipeliner.display_tools.create_results_display_object` function. This function
safely creates the object, returning a
:class:`~pipeliner.results_display_objects.ResultsDisplayPending` object with an
explanation if any errors are encountered.

``gather_metadata`` method
--------------------------

The :meth:`~pipeliner.pipeliner_job.PipelinerJob.gather_metadata` method returns a
dict of metadata about the results of the job. It doesn't need to gather any
information about the parameters used, this will be done automatically.

Do this in any way you see fit, just return a :class:`dict`.

``prepare_clean_up_lists`` method
---------------------------------

The :meth:`~pipeliner.pipeliner_job.PipelinerJob.prepare_clean_up_lists` method
returns a :class:`list` with two items; a list of files to delete and a list of
directories that should be removed when the job is cleaned up. The purpose of cleanup
is to free up disk space by removing unneeded files.

.. warning::
 Don't delete anything yet! This method should prepare lists of files for deletion,
 but they might not actually be deleted (e.g. if the user decides to cancel the clean
 up) so don't do anything irreversible here.

There are two levels of cleanup:

 - Standard: delete files that are not necessary, such as intermediate iterations or
   tmp files
 - Harsh: delete more, such as output files that can be reproduced easily

Make sure :meth:`~pipeliner.pipeliner_job.PipelinerJob.prepare_clean_up_lists` doesn't
delete anything important or used by Doppio for results display.

Example :meth:`~pipeliner.pipeliner_job.PipelinerJob.prepare_clean_up_lists`:

.. code-block:: python

 def prepare_clean_up_lists(self, do_harsh):
    files_to_remove, dirs_to_remove = [], []
    
    tmp_files = glob(self.output_name + "*.tmp")
    tmp_dir = self.output_name + "tmpfiles"
    
    files_to_remove.extend(tmp_files)
    dirs_to_remove.extend(tmp_dir)
    
    if do_harsh:
        extra_files = glob(self.output_name + "*.extra")
        extra_dir = self.output_name + "extrafiles" 
        files_to_remove.extend(extra_files)
        dirs_to_remove.extend(extra_dir)  
    
    return [files_to_remove, dirs_to_remove]

``prepare_onedep_data`` method
------------------------------

The :meth:`~pipeliner.pipeliner_job.PipelinerJob.prepare_onedep_data` method returns a
list of deposition objects which are used to prepare data for deposition in to the PDB,
EMDB, and EMPIAR.

This method must be implemented in any :class:`~pipeliner.pipeliner_job.PipelinerJob`
that produces data included in a database deposition.  For the PDB and EMDB these are
defined by the published
`schema <http://ftp.ebi.ac.uk/pub/databases/emdb/doc/XML-schemas/emdb-schemas/v3/current_v3/doc/Untitled.html>`_.

For example: A 3D refinement in RELION (pipeliner job ``relion.refine3d``)
returns a :class:`list` with two deposition objects:
[ :class:`~pipeliner.deposition_tools.pdb_deposition_objects.FinalReconstructionType`,
:class:`~pipeliner.deposition_tools.empiar_deposition_objects.EmpiarParticlesType` ]

A given job should return EVERY deposition object that holds any of the data the job
produced.

Deposition objects are generated using their associated ``_entry`` function. In the
example, the
:class:`~pipeliner.deposition_tools.pdb_deposition_objects.FinalReconstructionType`
deposition object is created by the
:func:`~pipeliner.deposition_tools.pdb_deposition_objects.final_reconstruction_type_entry`
function, which composes the final deposition object from multiple nested deposition
objects each generated with their own specific ``_entry`` function.

.. code-block:: python

 def onedep_deposition(self):
   sym = this_function_gets_symmetry()
   reso = this_function_gets_the_resolution()

   sp_filter = spatial_filtering_type_entry(
       high_frequency_cutoff=reso,
       software_list=("relion_refine",),
   )

   rec_filter = reconstruction_filtering_type_entry(spatial_filtering=sp_filter)

   recdep = final_reconstruction_type_entry(
       applied_symmetry=sym,
       algorithm="FOURIER SPACE",
       resolution=reso,
       resolution_method="FSC 0.143 CUT-OFF",
       reconstruction_filtering=rec_filter,
   )

   return [recdep]

.. _Adding entry points:

Adding the new job to the pipeliner
-----------------------------------

Now you have a file ``my_new_job.py``.
It contains a class ``MyJob`` that describes a job of the type ``mysoftware.function``.

Well done!

 1) Put your job file into ``ccpem-pipeliner/pipeliner/jobs/other``

 2) Add an entry point definition for your new job in ``ccpem-pipeliner/setup.cfg``:
    Go to the ``ccpem_pipeliner.jobs`` section
    Add your job in the format:

    ``jobtype = package_name.module_name:ClassName``

     e.g. ``mysoftware.function = pipeliner.jobs.other.my_new_job:MyJob``

 3) Update the pipeliner installation with: ``pip install –e .``

 4) Check for your job by running ``pipeliner --job_info mysoftware.function``
    from the command line


Node type names
===============

These top-level node types are already in use in the pipeliner:

+----------------------+------------------------------------------------------------------------------+
| Type Name            | Description                                                                  |
+======================+==============================================================================+
| AtomCoords           | An atomic model file                                                         |
+----------------------+------------------------------------------------------------------------------+
| DensityMap           | A 3D cryoEM density map (could be half map, full map, sharpened etc)         |
+----------------------+------------------------------------------------------------------------------+
| Image2D              | A single 2D image (not actually used yet, could be dropped?)                 |
+----------------------+------------------------------------------------------------------------------+
| Image2DStack         | A single file containing a stack of 2D images                                |
+----------------------+------------------------------------------------------------------------------+
| Image3D              | Any 3D image that is not a density map or mask, for example a local          |
|                      | resolution map, 3D FSC or cryoEF 3D transfer function                        |
+----------------------+------------------------------------------------------------------------------+
| Images2DData         | Information about groups of 2D images, for example 2D references or          |
|                      | reprojections (similar to ParticlesData, but ParticlesData is specifically   |
|                      | for real extracted particles)                                                |
+----------------------+------------------------------------------------------------------------------+
| Images3DData         | Information about groups of 3D images, for example 3D references             |
+----------------------+------------------------------------------------------------------------------+
| LigandDescription    | The stereo-chemical description of a ligand molecule                         |
+----------------------+------------------------------------------------------------------------------+
| LogFile              | A log file from a process. Could be PDF, text or other formats               |
+----------------------+------------------------------------------------------------------------------+
| Mask2D               | A mask for use with 2D images                                                |
+----------------------+------------------------------------------------------------------------------+
| Mask3D               | A mask for use with 3D volumes                                               |
+----------------------+------------------------------------------------------------------------------+
| MlModel              | Any sort of machine learning model                                           |
+----------------------+------------------------------------------------------------------------------+
| MicrographsCoords    | A file containing coordinate info, e.g. a star file with a list of coordinate|
|                      | files as created by a Relion picking job                                     |
+----------------------+------------------------------------------------------------------------------+
| MicrographsData      | A set of micrographs and metadata about them, for example a Relion           |
|                      | corrected_micrographs.star file                                              |
+----------------------+------------------------------------------------------------------------------+
| MicrographMoviesData | Metadata about multiple micrograph movies e.g. movies.star                   |
+----------------------+------------------------------------------------------------------------------+
| ParticlesData        | A set of particles and their metadata, e.g particles.star from Relion        |
+----------------------+------------------------------------------------------------------------------+
| ProcessData          | Other data resulting from a process that might be of use for other processes,|
|                      | for example an optimiser star file, postprocess star file or polishing params|
+----------------------+------------------------------------------------------------------------------+
| Restraints           | Distance and angle restraints for atomic model refinement                    |
+----------------------+------------------------------------------------------------------------------+
| RigidBodies          | A description of rigid bodies in an atomic model                             |
+----------------------+------------------------------------------------------------------------------+
| Sequence             | A protein or nucleic acid sequence file                                      |
+----------------------+------------------------------------------------------------------------------+
| StructureFactors     | A set of structure factors in reciprocal space, usually corresponding to a   |
|                      | real-space density map                                                       |
+----------------------+------------------------------------------------------------------------------+
