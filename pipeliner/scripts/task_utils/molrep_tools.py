#!/usr/bin/env python3

#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
import argparse

keywords_file = "keywords.txt"
description = "Python tools for running Molrep via CCP-EM Pipeliner"
parser = argparse.ArgumentParser(prog="molrep_tools", description="".join(description))
parser.add_argument(
    "-s",
    "--symlink_mrc_map",
    default=None,
    help="Create .map symlink to .mrc input as molrep does not accept .mrc ext",
)
parser.add_argument(
    "-k",
    "--keywords",
    default=None,
    nargs="+",
    help="Keywords string for molrep keywords text file",
)
parser.add_argument(
    "-f", "--fix_pdb", action="store_true", help="Fix output PDB format"
)


def symlink_mrc_map(src):
    """
    Create soft link for input .mrc to .map

    This is done as Molrep (as of CCP4 8.02) will not accept maps with .mrc
    extension as valid inputs.  File linked to .mrc to minimize space
    """
    if src.endswith(".mrc"):
        try:
            dst = src.replace(".mrc", ".map")
            os.symlink(src, dst)
        except FileExistsError:
            pass


def write_keywords_file(keywords):
    """
    Takes list of keyword strings and writes to keyword file for Molrep
    """
    f = open(keywords_file, "w")
    for word in keywords:
        f.write(word + "\n")
    f.close()


def fix_pdb():
    """
    Strip problematic Molrep PDB separators starting with '#'

    This is done in Python (rather than a command line tool such as sed) to
    make it work more easily on all platforms.
    """
    output_pdb_name = "molrep.pdb"
    backup_pdb_name = "molrep.pdb.orig"
    print(
        "Backing up original output file {} to {}".format(
            output_pdb_name, backup_pdb_name
        )
    )
    os.rename(output_pdb_name, backup_pdb_name)
    print("Stripping bad separators from {}...".format(output_pdb_name))
    line_count = 0
    bad_line_count = 0
    with open(backup_pdb_name) as orig, open(output_pdb_name, "w") as output:
        for line in orig:
            line_count += 1
            if not line.startswith("#"):
                output.write(line)
            else:
                bad_line_count += 1
    print(
        "Processed {} lines, removed {} bad separators".format(
            line_count, bad_line_count
        )
    )


def main():
    print("Molrep Tools")
    args = parser.parse_args()
    if args.fix_pdb:
        fix_pdb()
    if args.symlink_mrc_map is not None:
        symlink_mrc_map(src=args.symlink_mrc_map)
    if args.keywords is not None:
        write_keywords_file(keywords=args.keywords)


if __name__ == "__main__":
    main()
