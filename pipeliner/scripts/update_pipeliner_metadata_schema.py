#!/usr/bin/env python3

#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
import json
from pipeliner.utils import get_pipeliner_root
from pipeliner.metadata_tools import make_job_parameters_schema
from pipeliner.api.api_utils import get_job_types
from pipeliner.pipeliner_job import PipelinerJob
from pipeliner.utils import make_pretty_header


def update_schema(job: PipelinerJob, test_schema: str, schema_file: str, s_type: str):
    try:
        with open(schema_file, "r") as schema_template:
            exp_schema = json.loads(schema_template.read())
    except FileNotFoundError:
        with open(schema_file, "w") as new_params_schema:
            json.dump(test_schema, new_params_schema, indent=4)
            return (
                f"No {s_type} schema found for {job.PROCESS_NAME}; "
                f"Wrote pipeliner{schema_file.replace(str(get_pipeliner_root()), '')}"
            )

    if test_schema != exp_schema:
        with open(schema_file, "w") as new_params_schema:
            json.dump(test_schema, new_params_schema, indent=4)
        return f"Updated {s_type} schema for {job.PROCESS_NAME}"


def main():
    md_dir = os.path.join(get_pipeliner_root(), "metadata_schema")
    params_dir = os.path.join(md_dir, "parameters")
    made_changes = []

    for job in get_job_types():
        jname = job.PROCESS_NAME.replace(".", "_")

        # check for the main schema
        default_schema = {
            "$schema": "http://json-schema.org/draft-04/schema#",
            "title": f"RESULTS: {job.PROCESS_NAME}",
            "description": f"{job.jobinfo.short_desc}",
            "type": "object",
            "properties": {
                "run parameters": {
                    "description": "Complete parameters used for running the job",
                    "$ref": f"parameters/params_{jname}.json",
                },
                "results": {
                    "description": "Data on the results of the job",
                    "$ref": f"results/results_{jname}.json",
                },
            },
        }
        schema_file = os.path.join(md_dir, f"{jname}.json")
        check = update_schema(job, default_schema, schema_file, "main")
        if check:
            made_changes.append(check)

        # check the params schema
        schema_file = os.path.join(
            get_pipeliner_root(),
            params_dir,
            f"params_{jname}.json",
        )
        params_schema = make_job_parameters_schema(job.PROCESS_NAME)
        check = update_schema(job, params_schema, schema_file, "parameters")
        if check:
            made_changes.append(check)

    print(make_pretty_header("PIPELINER METADATA SCHEMA UPDATE"))
    if made_changes:
        for update in made_changes:
            print(f"- {update}")
    else:
        print("All schema are up to date")


if __name__ == "__main__":
    main()
