#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
import mrcfile
import json
from glob import glob
from typing import Dict, Tuple, Callable, Optional, List
from gemmi import cif
from PIL import Image, UnidentifiedImageError

from pipeliner.process import Process
from pipeliner.display_tools import (
    create_results_display_object,
    mini_montage_from_stack,
    mini_montage_from_starfile,
    make_map_model_thumb_and_display,
)
from pipeliner.results_display_objects import ResultsDisplayObject

# The second term of a node should describe the general type of file it is, even if
# the extension may differ IE: maymap.mrc, mymap.mrcs, and mymap.map should all be
# DensityMap.mrc node types

# pipeliner style node labels AKA "top-level node names"
NODE_ATOMCOORDS = "AtomCoords"
NODE_ATOMCOORDSGROUPMETADATA = "AtomCoordsGroupMetadata"
NODE_DENSITYMAP = "DensityMap"
NODE_DENSITYMAPMETADATA = "DensityMapMetadata"
NODE_DENSITYMAPGROUPMETADATA = "DensityMapGroupMetadata"
NODE_IMAGE2D = "Image2D"
NODE_IMAGE2DSTACK = "Image2DStack"
NODE_IMAGE2DMETADATA = "Image2DMetadata"
NODE_IMAGE2DGROUPMETADATA = "Image2DGroupMetadata"
NODE_IMAGE3D = "Image3D"
NODE_IMAGE3DMETADATA = "Image3DMetadata"
NODE_IMAGE3DGROUPMETADATA = "Image3DGroupMetadata"
NODE_LIGANDDESCRIPTION = "LigandDescription"
NODE_LOGFILE = "LogFile"
NODE_MASK2D = "Mask2D"
NODE_MASK3D = "Mask3D"
NODE_MICROGRAPHCOORDS = "MicrographCoords"
NODE_MICROGRAPHCOORDSGROUP = "MicrographCoordsGroup"
NODE_MICROGRAPH = "Micrograph"
NODE_MICROGRAPHMETADATA = "MicrographMetadata"
NODE_MICROGRAPHGROUPMETADATA = "MicrographGroupMetadata"
NODE_MICROGRAPHMOVIE = "MicrographMovie"
NODE_MICROGRAPHMOVIEMETADATA = "MicrographMovieMetadata"
NODE_MICROGRAPHMOVIEGROUPMETADATA = "MicrographMovieGroupMetadata"
NODE_MICROSCOPEDATA = "MicroscopeData"
NODE_MLMODEL = "MlModel"
NODE_PARTICLESDATA = "ParticlesData"
NODE_PROCESSDATA = "ProcessData"
NODE_RESTRAINTS = "Restraints"
NODE_RIGIDBODIES = "RigidBodies"
NODE_SEQUENCE = "Sequence"
NODE_SEQUENCEGROUP = "SequenceGroup"
NODE_SEQUENCEALIGNMENT = "SequenceAlignment"
NODE_STRUCTUREFACTORS = "StructureFactors"
NODE_TILTSERIES = "TiltSeries"
NODE_TILTSERIESMETADATA = "TiltSeriesMetadata"
NODE_TILTSERIESGROUPMETADATA = "TiltSeriesGroupMetadata"
NODE_TILTSERIESMOVIE = "TiltSeriesMovie"
NODE_TILTSERIESMOVIEMETADATA = "TiltSeriesMovieMetadata"
NODE_TILTSERIESMOVIEGROUPMETADATA = "TiltSeriesMovieGroupMetadata"
NODE_TOMOGRAM = "Tomogram"
NODE_TOMOGRAMMETADATA = "TomogramMetadata"
NODE_TOMOGRAMGROUPMETADATA = "TomogramGroupMetadata"
NODE_NEWNODETYPE = "NEW NODE TYPE (not recommended)"

NODE_DISPLAY_FILE = "node_display.star"
NODE_DISPLAY_DIR = "NodeDisplay"


def check_file_is_mrc(file: str) -> bool:
    """Validate that a file is a mrc file"""
    try:
        mrcfile.open(file, header_only=True)
        return True
    except Exception as e:
        print(f"File '{file}' does not appear to be a valid MRC file\n{e}")
        return False


def check_file_is_cif(file: str) -> bool:
    """Validate that a file is a cif or star file"""
    try:
        cif.read(file)
        return True
    except Exception as e:
        print(f"File '{file}' does not appear to be CIF/STAR file\n{e}")
        return False


def check_file_is_tif(file: str) -> bool:
    try:
        with Image.open(file) as im:
            imgformat = im.format  # type: ignore # known issue with mypy
    except UnidentifiedImageError:
        return False
    if imgformat == "TIFF":
        return True
    return False


def check_file_is_text(file: str) -> bool:
    try:
        with open(file, "r") as f:
            f.read()
            return True
    except UnicodeDecodeError:
        return False


class GenNodeFormatConverter(object):
    """Convert node types for files that have different but equivalent file extensions

    Attributes:
        allowed_ext (dict): A dict of extensions that are allowed for that nodetype
            IE: {("mrc", "mrcs", "map"): "mrc"}
        check_funct (staticmethod): A function that checks a file is of the expected
            type, must return a bool
    """

    def __init__(
        self,
        allowed_ext: Dict[Tuple[str, ...], str],
        check_funct: Dict[str, Callable],
    ):
        """Instantiate a GenNodeFormatConverter

        Args:
            allowed_ext (Dict[Tuple[str],str]): Extensions that are possible for that
                type of node file and the general name they converge to IE:
                {("mrc", "mrcs", "map"): "mrc"}.  This is just for generalising node
                types, the file CAN have other extensions besides ones in the dict
            check_funct (Dict[str, staticmethod]): A function that checks a file is of
                the expected type, must take the file as input and return an error
                 message if there is a problem or an empty string if all good.
                 Matched to the general nodetypes in the dict IE: {"mrc": check_if_mrc,
                  "star": check_if_star}
        """
        self.allowed_ext = allowed_ext
        self.check_funct = check_funct

    def gen_nodetype(
        self, filename: str, override_format: str = "", validate_file: bool = True
    ) -> str:
        """Assign a file to a general node type

        Args:
            filename (str): File to create the node for
            override_format (str): The node's format
            validate_file (bool): DO validation on the file type - turn it off to speed
                up rewriting an entire pipeline
        """
        gen_ext = ""
        ext = os.path.splitext(filename)[1].strip(".")
        ext = override_format if override_format else ext
        ext = "XX" if not ext else ext

        for type_tup in self.allowed_ext:
            if ext in type_tup:
                gen_ext = self.allowed_ext[type_tup]
                break

        gen_ext = ext if not gen_ext else gen_ext

        if gen_ext in self.check_funct and os.path.isfile(filename) and validate_file:
            if not self.check_funct[gen_ext](filename):
                return "X" + gen_ext + "X"

        return gen_ext


class Node(object):
    """Nodes store info about input and output files of jobs that have been run

    Attributes:
        name (str): The name of the file the node represents
        toplevel_type (str): The toplevel node type
        toplevel_description: (str): A description of the toplevel type
        format (str): The node's generalised file type IE ('mrc' for mrc, 'mrcs' or
            'map')
        kwds (list[str]): Keywords associated with the node
        output_from_process (:class:`~pipeliner.process.Process`): The
            Process object for process that created the file
        input_for_processes_list (list): A list of
            :class:`~pipeliner.process.Process` objects for processes
            that use the node as an input
        format_converter (Optional[GenNodeFormatConverter]): An object that defines
            the expected extensions for the node and how to validate them
        type (str): The full node type
    """

    def __init__(
        self,
        name: str,
        toplevel_type: str,
        kwds: Optional[List[str]] = None,
        toplevel_description: str = (
            "A general nodetype for files without a more specific Node " "class defined"
        ),
        format_converter: Optional[GenNodeFormatConverter] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        """Create a Node object
        Args:
            name (str): The name of the file the node represents
            toplevel_type (str): The toplevel nodetype
            kwds (list): node keywords
            toplevel_description (str): A description of what the nodetype is for
            format_converter (GenNodeFormatConverter): The object that is used to
                generalise the node format and validate the files
            do validation (bool): Should the node attempt to validate the file type
                might wnat to turn this off if a large number of nodes are generated
                at the same time (like when the ProjectGraph reads a pipeline)
            override_format (str): Override the automatically assigned format for a node
                can be used to assign format to a file that has no extension, defaults
                to 'XX' for files of unknown format with no extension to infer it from.

        Raises:
            ValueError: If the name is empty
        """
        if name:
            self.name = str(name)
        else:
            raise ValueError("Cannot create a node with no name")
        self.kwds = [] if not kwds else [x.lower() for x in kwds]
        self.toplevel_type = toplevel_type
        self.toplevel_description = toplevel_description

        # updated by the node factory if the node format can be generalised
        ext = os.path.splitext(self.name)[1].strip(".")
        if override_format:
            self.format = override_format
        elif ext:
            self.format = ext
        else:
            self.format = "XX"
        self.format_converter = format_converter
        if self.format_converter:
            self.format = self.format_converter.gen_nodetype(
                self.name, self.format, validate_file=do_validation
            )

        # type is taken as given, subclasses will validate and update
        self.type = self.toplevel_type + "." + self.format
        if self.kwds:
            joinkwds = ".".join(self.kwds)
            self.type += f".{joinkwds}"

        # must have producer process, can have consumers
        self.output_from_process: Optional[Process] = None
        self.input_for_processes_list: list = []

    def clear(self):
        """Clear the input and output processes from a Node"""
        self.output_from_process = None
        self.input_for_processes_list.clear()

    def generalise_format(self):
        """Set the node's format

        generalise node format for files that have different but equivalent file
        extensions EX 'mrc' for '.map', or '.mrc' files

        defaults to the file extension
        """
        if self.format_converter:
            self.format = self.format_converter.gen_nodetype(self.name)

    def keyword_validation(self):
        """placeholder for function that validates the node based on type and keywords

        IE if a node is .star format and has the keywords 'relion' and 'ctf' it must
        have specific columns in the starfile

        This function will be specific to each node type
        """
        pass

    def default_results_display(self, output_dir) -> ResultsDisplayObject:
        """Generate a ResultsDisplayObject for the node

        This method will be used for every node in Process if there is no
        'create_results_display' function defined
        """
        if check_file_is_text(self.name):
            return create_results_display_object("textfile", file_path=self.name)

        return create_results_display_object(
            "text",
            title=self.name,
            display_data=(
                f"The node type for this file in {output_dir} ({self.type}) has no "
                "default display method"
            ),
            associated_data=[self.name],
        )

    def not_compatible_with_default_output(self) -> ResultsDisplayObject:
        """Return a ResultsDisplayText object for jobs with no default display object"""
        return create_results_display_object(
            "text",
            title="No graphical display for this node",
            display_data=f"File format not compatible with {self.toplevel_type} node's "
            "default display method",
            associated_data=[self.name],
        )

    def write_default_result_file(self):
        outdir = os.path.join(os.path.dirname(self.name), NODE_DISPLAY_DIR)
        if not os.path.isdir(outdir):
            os.makedirs(outdir)

        # get the name for the file to write
        do = self.default_results_display(output_dir=outdir)
        rdfiles = glob(f"{outdir}/results_display*_{do.dobj_type}.json")
        n = 1
        for f in rdfiles:
            dt = f.split("_")[-1].strip(".json")
            n += 1 if dt == do.dobj_type else n

        # write the file
        rd_file = f"results_display{n:03d}_{do.dobj_type}.json"
        final_out = os.path.join(outdir, rd_file)
        with open(final_out, "w") as outf:
            json.dump(do.__dict__, outf, indent=4)

        # update the reference file
        nodes = None
        if os.path.isfile(NODE_DISPLAY_FILE):
            ciffile = cif.read_file(NODE_DISPLAY_FILE)
            block = ciffile.find_block("pipeliner_node_display")
            nodes = block.find("_pipeliner", ["NodeName", "DisplayFile"])

        # updated cif document
        new_cif = cif.Document()
        new_bl = new_cif.add_new_block("pipeliner_node_display")
        updated = new_bl.init_loop("_pipeliner", ["NodeName", "DisplayFile"])

        # see if the node already exists
        fc = 0
        if nodes:
            for line in nodes:
                if self.name == line[0]:
                    oline, found = [self.name, final_out], 1
                else:
                    oline, found = line, 0
                fc += found
                updated.add_row(oline)
            if not fc:
                updated.add_row([self.name, final_out])
        else:
            updated.add_row([self.name, final_out])

        new_cif.write_file(NODE_DISPLAY_FILE)

    def get_result_display_object(self) -> ResultsDisplayObject:
        """Get the default results display object for the node"""
        odir = os.path.dirname(self.name)
        # if its missing write it
        if not os.path.isfile(NODE_DISPLAY_FILE):
            self.write_default_result_file()
            return self.default_results_display(odir)

        # if it's there read it
        rd_f = cif.read_file(NODE_DISPLAY_FILE)
        bl = rd_f.find_block("pipeliner_node_display")
        rd_data = bl.find("_pipeliner", ["NodeName", "DisplayFile"])
        for i in rd_data:
            if i[0] == self.name:
                with open(i[1]) as f:
                    do_data = json.load(f)
                dobj = create_results_display_object(**do_data)
                if dobj.dobj_type == "pending":
                    os.remove(i[1])
                    self.write_default_result_file()
                    return self.default_results_display(output_dir=odir)
                else:
                    return dobj

        # if not found write it
        self.write_default_result_file()
        return self.default_results_display(odir)


# Node classes
class AtomCoordsNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_ATOMCOORDS,
            toplevel_description="An atomic model file",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("pdb", "ent"): "pdb", ("mmcif", "cif"): "cif"},
                check_funct={"cif": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    def default_results_display(self, output_dir) -> ResultsDisplayObject:
        return create_results_display_object(
            "mapmodel",
            title=self.name,
            models=[self.name],
            associated_data=[self.name],
            start_collapsed=False,
        )


class AtomCoordsGroupMetadataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_ATOMCOORDSGROUPMETADATA,
            toplevel_description=(
                "Data about a set of atomic coordinates files, a list of atomic models"
                "for example"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("cif", "mmcif"): "cif"},
                check_funct={"star": check_file_is_cif, "cif": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )


class DensityMapNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_DENSITYMAP,
            toplevel_description=(
                "A 3D cryoEM density map (could be half map, full map, sharpened etc)"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("mrc", "mrcs", "map"): "mrc"},
                check_funct={"mrc": check_file_is_mrc},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    def default_results_display(self, output_dir) -> ResultsDisplayObject:
        return make_map_model_thumb_and_display(
            outputdir=output_dir,
            maps=[self.name],
            start_collapsed=False,
        )


class DensityMapMetadataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_DENSITYMAPMETADATA,
            toplevel_description="Data about a single density map",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("cif", "mmcif"): "cif"},
                check_funct={"star": check_file_is_cif, "cif": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )


class DensityMapGroupMetadataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_DENSITYMAPGROUPMETADATA,
            toplevel_description="Data about multiple density maps",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("cif", "mmcif"): "cif"},
                check_funct={"star": check_file_is_cif, "cif": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )


class Image2DNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_IMAGE2D,
            toplevel_description="A single 2D image",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("tif", "tiff"): "tif"},
                check_funct={},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    def default_results_display(self, output_dir) -> ResultsDisplayObject:
        return create_results_display_object(
            "image",
            title=self.name,
            image_path=self.name,
            image_desc="",
            associated_data=[self.name],
        )


class Image2DMetadataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_IMAGE2DMETADATA,
            toplevel_description="Data about a single 2D image or stack",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("cif", "mmcif"): "cif"},
                check_funct={"star": check_file_is_cif, "cif": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )


class Image2DGroupMetadataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_IMAGE2DGROUPMETADATA,
            toplevel_description="Data about a group of 2D images or stacks",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("cif", "mmcif"): "cif"},
                check_funct={"star": check_file_is_cif, "cif": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    def default_results_display(self, output_dir) -> ResultsDisplayObject:
        # for relion 2D class averages
        if (
            all(x in self.kwds for x in ["relion", "classaverages"])
            and self.format == "star"
        ):
            return mini_montage_from_starfile(
                starfile=self.name,
                block="",
                column="_rlnReferenceImage",
                outputdir=output_dir,
            )
        # all others use default
        return super().default_results_display(output_dir)


class Image2DStackNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_IMAGE2DSTACK,
            toplevel_description="A single file containing a stack of 2D images",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("mrc", "mrcs"): "mrc"},
                check_funct={"mrc": check_file_is_mrc},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    def default_results_display(self, output_dir) -> ResultsDisplayObject:
        return mini_montage_from_stack(stack_file=self.name, outputdir=output_dir)


class Image3DNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_IMAGE3D,
            toplevel_description=(
                "Any 3D image that is not a density map or mask, for example a local"
                " resolution map, 3D FSC or cryoEF 3D transfer function"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("mrc", "mrcs"): "mrc"},
                check_funct={"mrc": check_file_is_mrc},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # currently no default format - what to use for default image3D viewing


class Image3DMetadataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_IMAGE3DMETADATA,
            toplevel_description=(
                "Data about a single 3D image, except density maps, which have their "
                "own specific node type (DensityMap)"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("cif", "mmcif"): "cif"},
                check_funct={"star": check_file_is_cif, "cif": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default format - too many file type possibilities


class Image3DGroupMetadataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_IMAGE3DGROUPMETADATA,
            toplevel_description=(
                "Data about a group of 3D images, but not masks or "
                "density maps, which have their own specific types"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("cif", "mmcif"): "cif"},
                check_funct={"star": check_file_is_cif, "cif": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )


class LigandDescriptionNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_LIGANDDESCRIPTION,
            toplevel_description="The stereochemical description of a ligand molecule",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={},
                check_funct={},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default_display format - don't know what the format of these files is


class LogFileNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_LOGFILE,
            toplevel_description=(
                "A log file from a process. Could be PDF, text or any other format"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("err", "log", "out", "txt"): "txt"},
                check_funct={},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    def default_results_display(self, output_dir) -> ResultsDisplayObject:
        # for pdfs
        if self.format == "pdf":
            return create_results_display_object(
                "pdffile",
                title=self.name,
                file_path=self.name,
            )
        return super().default_results_display(output_dir)


class Mask2DNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_MASK2D,
            toplevel_description="A mask for use with 2D images",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("mrc", "mrcs"): "mrc"},
                check_funct={"mrc": check_file_is_mrc},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    def default_results_display(self, output_dir) -> ResultsDisplayObject:
        return create_results_display_object(
            "image",
            title=self.name,
            image_path=self.name,
            image_desc="",
            associated_data=[self.name],
        )


class Mask3DNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_MASK3D,
            toplevel_description="A mask for use with 3D volumes",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("mrc", "mrcs", "map"): "mrc"},
                check_funct={"mrc": check_file_is_mrc},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    def default_results_display(self, output_dir) -> ResultsDisplayObject:
        return make_map_model_thumb_and_display(
            outputdir=output_dir,
            maps=[self.name],
        )


class MicrographCoordsNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_MICROGRAPHCOORDS,
            toplevel_description=(
                "A file containing coordinate info for a single micrograph, "
                "e.g. a starfile or .box file produced from picking"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={},
                check_funct={"star": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method too many possible formats


class MicrographCoordsGroupNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_MICROGRAPHCOORDSGROUP,
            toplevel_description=(
                "A file containing coordinate info for multiple micrographs, "
                "e.g. a star file with a list of coordinate files as created by "
                "a Relion picking job"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={},
                check_funct={"star": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )


class MicrographNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_MICROGRAPH,
            toplevel_description="A single micrograph",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("mrc", "mrcs"): "mrc", ("tiff", "tif"): "tif"},
                check_funct={"mrc": check_file_is_mrc, "tif": check_file_is_tif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    def default_results_display(self, output_dir) -> ResultsDisplayObject:
        return create_results_display_object(
            "image",
            title=self.name,
            image_path=self.name,
            image_descr="",
            associated_data=[self.name],
        )


class MicrographMetadataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_MICROGRAPHMETADATA,
            toplevel_description="Metadata about a single micrograph",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("cif", "mmcif"): "cif"},
                check_funct={"star": check_file_is_cif, "cif": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method too many possible formats


class MicrographGroupMetadataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_MICROGRAPHGROUPMETADATA,
            toplevel_description=(
                "Metadata about a set of micrographs, for example a Relion "
                "corrected_micrographs.star file"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("cif", "mmcif"): "cif"},
                check_funct={"star": check_file_is_cif, "cif": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    def default_results_display(self, output_dir) -> ResultsDisplayObject:
        # for relion micrographs files
        if "relion" in self.kwds and self.format == "star":
            return mini_montage_from_starfile(
                starfile=self.name,
                block="micrographs",
                column="_rlnMicrographName",
                outputdir=output_dir,
                nimg=3,
            )
        # all others use defults
        return super().default_results_display(output_dir)


class MicrographMovieNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_MICROGRAPHMOVIE,
            toplevel_description="A single multiframe micrograph movie",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("mrc", "mrcs"): "mrc", ("tiff", "tif"): "tif"},
                check_funct={"mrc": check_file_is_mrc, "tif": check_file_is_tif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    def default_results_display(self, output_dir) -> ResultsDisplayObject:
        return mini_montage_from_stack(
            stack_file=self.name,
            outputdir=output_dir,
        )


class MicrographMovieMetadataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_MICROGRAPHMOVIEMETADATA,
            toplevel_description="Metadata about a single multiframe micrograph movie",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("cif", "mmcif"): "cif"},
                check_funct={"star": check_file_is_cif, "cif": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default results display too many possible file formats


class MicrographMovieGroupMetadataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_MICROGRAPHMOVIEGROUPMETADATA,
            toplevel_description=(
                "Metadata about multiple micrograph movies e.g. movies.star"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("cif", "mmcif"): "cif"},
                check_funct={"star": check_file_is_cif, "cif": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method too many possible formats
    def default_results_display(self, output_dir) -> ResultsDisplayObject:
        # for relion movie files
        if "relion" in self.kwds and self.format == "star":
            return mini_montage_from_starfile(
                starfile=self.name,
                block="movies",
                column="_rlnMicrographMovieName",
                outputdir=output_dir,
                nimg=2,
            )

        # all others use default
        return super().default_results_display(output_dir)


class MicroscopeDataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_MICROSCOPEDATA,
            toplevel_description=(
                "Data about the microscope such as collection params, defect files, "
                "or mtf"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={
                    ("cif", "mmcif"): "cif",
                    ("mrc", "mrcs"): "mrc",
                    ("tiff", "tif"): "tif",
                },
                check_funct={
                    "star": check_file_is_cif,
                    "mrc": check_file_is_mrc,
                    "cif": check_file_is_cif,
                    "tif": check_file_is_tif,
                },
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method too many possible formats


class MlModelNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_MLMODEL,
            toplevel_description="A machine learning model",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={},
                check_funct={},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    def default_results_display(self, output_dir) -> ResultsDisplayObject:
        return create_results_display_object(
            "text",
            title="No graphical output for ML Models",
            display_data=(
                f"The node type for this file in {output_dir} ({self.type}) has no "
                "graphical output"
            ),
            associated_data=[self.name],
        )


class ParticlesDataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_PARTICLESDATA,
            toplevel_description=(
                "A set of particles and their metadata, e.g particles.star from Relion"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("star",): "star"},
                check_funct={"star": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    def default_results_display(self, output_dir) -> ResultsDisplayObject:
        if self.format == "star" and "relion" in self.kwds:
            return mini_montage_from_starfile(
                starfile=self.name,
                block="particles",
                column="_rlnImageName",
                outputdir=output_dir,
            )

        return self.not_compatible_with_default_output()


class ProcessDataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_PROCESSDATA,
            toplevel_description=(
                "Other data resulting from a process that might be of use for other "
                "processes, for example an optimiser star file, postprocess star "
                "file or polishing params"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("star",): "star"},
                check_funct={"star": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display format - to many possible file formats


class RestraintsNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_RESTRAINTS,
            toplevel_description=(
                "Distance and angle restraints for atomic model refinement"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={},
                check_funct={},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method, too many possible file formats


class RigidBodiesNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_RIGIDBODIES,
            toplevel_description="A description of rigid bodies in an atomic model",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={},
                check_funct={},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method, too many possible file formats


class SequenceNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_SEQUENCE,
            toplevel_description="A protein or nucleic acid sequence file",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("fasta", "seq"): "fasta"},
                check_funct={},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method need a SequenceDisplayObject


class SequenceAlignmentNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_SEQUENCEALIGNMENT,
            toplevel_description=(
                "Files that contain or are used to generate multi sequence alignments"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={},
                check_funct={},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method need a SequenceDisplayObject


class StructureFactorsNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_STRUCTUREFACTORS,
            toplevel_description=(
                "A set of structure factors in reciprocal space, usually "
                "corresponding to a realspace density map"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={},
                check_funct={},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method don't know file format


class TiltSeriesNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_TILTSERIES,
            toplevel_description="A single tomograpic tilt series",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("mrc", "mrcs"): "mrc"},  # TODO: add IMOD exts
                check_funct={"mrc": check_file_is_mrc},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method need a TiltSeries display object


class TiltSeriesMetadataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_TILTSERIESMETADATA,
            toplevel_description="Data about a single tomograpic tilt series",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("star",): "star"},  # TODO: add IMOD exts
                check_funct={"star": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method maybe can make more specific ones later


class TiltSeriesGroupMetadataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_TILTSERIESGROUPMETADATA,
            toplevel_description=(
                "Data about a group of multiple tomograpic tilt series"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("star",): "star"},  # TODO: add IMOD exts
                check_funct={"star": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method need a TiltSeries display object


class TiltSeriesMovieNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_TILTSERIESMOVIE,
            toplevel_description=(
                "A file containing a multiframe movie for a single tomograpic tilt "
                "series"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("mrc", "mrcs"): "mrc"},  # TODO: add IMOD exts
                check_funct={"mrc": check_file_is_mrc},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method need a TiltSeries display object


class TiltSeriesMovieGroupMetadataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_TILTSERIESMOVIEGROUPMETADATA,
            toplevel_description=(
                "Data about a group of multiple tomograpic tilt series multiframe "
                "movies"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("star",): "star"},  # TODO: add IMOD exts
                check_funct={"star": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method need a TiltSeries display object


class TiltSeriesMovieMetadataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_TILTSERIESMOVIEMETADATA,
            toplevel_description=(
                "A file containing data about multiframe movie for a single "
                "tomograpic tilt series"
            ),
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("star",): "star"},  # TODO: add IMOD exts
                check_funct={"star": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method need specific methods for specific file formats


class TomogramNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_TOMOGRAM,
            toplevel_description="A single tomogram",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("mrc", "mrcs"): "mrc"},  # TODO: add IMOD exts
                check_funct={"mrc": check_file_is_mrc},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method need a TomoGram display object, could try a MapModel
    # display object but don't know how Mol* would handle it.


class TomogramMetadataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_TOMOGRAMMETADATA,
            toplevel_description="Data about a single tomogram tilt series",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("star",): "star"},  # TODO: add IMOD exts
                check_funct={"star": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method could make specific ones for specific file formats


class TomogramGroupMetadataNode(Node):
    def __init__(
        self,
        name: str,
        kwds: Optional[List[str]] = None,
        do_validation: bool = True,
        override_format: str = "",
    ):
        super().__init__(
            name=name,
            toplevel_type=NODE_TOMOGRAMGROUPMETADATA,
            toplevel_description="Data about a group of tomgrams",
            kwds=kwds,
            format_converter=GenNodeFormatConverter(
                allowed_ext={("star",): "star"},  # TODO: add IMOD exts
                check_funct={"star": check_file_is_cif},
            ),
            do_validation=do_validation,
            override_format=override_format,
        )

    # no default display method too many possible file formats
