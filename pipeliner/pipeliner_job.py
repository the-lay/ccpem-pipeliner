#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
import math
import operator
import shutil
import shlex
import re

from collections import OrderedDict
from gemmi import cif
from typing import Optional, List, Union, Dict

from pipeliner import user_settings
from pipeliner.starfile_handler import JobStar
from pipeliner.relion_compatibility import get_job_type

from pipeliner.star_writer import write
from pipeliner.job_options import (
    StringJobOption,
    TRUES,
    BooleanJobOption,
    IntJobOption,
    FloatJobOption,
    JobOptionValidationResult,
    PathJobOption,
)
from pipeliner.node_factory import create_node
from pipeliner.utils import (
    truncate_number,
    touch,
    quotate_command_list,
    smart_strip_quotes,
    get_pipeliner_root,
    run_subprocess,
)

from pipeliner.nodes import NODE_LOGFILE, Node


class ExternalProgram(object):
    """
    Class to store info about external programs called by the pipeliner

    Attributes:
        command (str): The command that will be used to run the program
        exe_path (str): The path to the executable for the program
        vers_com (List[str]): The command that needs to be run to get the version
        vers_lines (List[int]): The lines from the output of the version command that
            contain the version info
    """

    def __init__(
        self,
        command: str,
        vers_com: Optional[List[str]] = None,
        vers_lines: Optional[List[int]] = None,
    ):
        """
        Create an ExternalProgram object

        Args:
            command (str): The command used to call the program from the unix shell
            vers_com (List[str]):  A list of command name and arguments to be run to get
                the program's version
            vers_lines: Optional[List[int]]=None: A list of lines from the stdout
                of the vers_com that contain the version information.  If `None` the
                entire stdout is used
        """
        self.command = command
        self.vers_com = vers_com
        self.vers_lines = vers_lines

        # check that the exe exists
        self.exe_path = shutil.which(command)

        # get the program version

    def get_version(self):
        if self.vers_com and self.exe_path:
            try:
                if self.vers_com[0] == "acedrg":
                    # Special workaround for acedrg because it leaves temporary
                    # directories and files behind after printing its version info. The
                    # problem has been reported and will hopefully be fixed in a future
                    # version of acedrg.
                    # (We don't do this for all job types because using a temporary
                    #  directory slows things down a lot. Version checking is already
                    #  too slow so we only want to use a temporary directory when really
                    #  needed.)
                    import tempfile

                    with tempfile.TemporaryDirectory() as tempdir:
                        versproc = run_subprocess(
                            self.vers_com,
                            capture_output=True,
                            timeout=1,
                            cwd=tempdir.name,  # type: ignore[attr-defined]
                        )
                else:
                    versproc = run_subprocess(
                        self.vers_com, capture_output=True, timeout=1
                    )
                vers_raw = versproc.stdout.decode().split("\n")

                # a few attempts to clean up messy version info
                # try an alternate split if \n didn't work
                if len(vers_raw) == 1:
                    vers_raw = vers_raw[0].split(";")

                if self.vers_lines:
                    vers_info = ";".join([vers_raw[x] for x in self.vers_lines])
                else:
                    vers_info = ";".join(vers_raw)
                # remove excess spaces
                vers_info = vers_info.replace("    ", " ")
                return vers_info

            except Exception as e:
                return f"Error trying to determine version: {str(e)}"
        else:
            return "No version info available"


class Ref(object):
    """
    Class to hold metadata about a citation or reference, typically a journal article.

    Attributes:
        authors (list): The authors of the reference.
        title (str): The reference's title.
        journal (str): The journal.
        year (str): The year of publication.
        volume (str): The volume number.
        issue (str): The issue number.
        pages (str): The page numbers.
        doi (str): The reference's Digital Object Identifier.
        other_metadata (dict): Other metadata as needed. Gathered from kwargs
    """

    def __init__(
        self,
        authors: Optional[Union[str, List[str]]] = None,
        title: str = "",
        journal: str = "",
        year: str = "",
        volume: str = "",
        issue: str = "",
        pages: str = "",
        doi: str = "",
        **kwargs,
    ):
        """
        Create a new Ref object.

        All arguments are optional. Use additional keyword arguments to store other
        metadata if necessary.

        Args:
            authors (list of str): The authors of the reference.
            title (str): The reference's title.
            journal (str): The journal.
            year (str): The year of publication.
            volume (str): The volume number.
            issue (str): The issue number.
            pages (str): The page numbers.
            doi (str): The reference's Digital Object Identifier.
            **kwargs (str): Other metadata as needed.
        """
        # Ensure self.authors is stored as a list
        if authors is not None:
            if isinstance(authors, str):
                self.authors = [authors]
            else:
                self.authors = list(authors)
        else:
            self.authors = []

        if not all((title, journal, year)):
            raise ValueError(
                "Reference objects need a title, journal, and year at minimum"
            )

        self.title = title
        self.journal = journal
        self.year = year
        self.volume = volume
        self.issue = issue
        self.pages = pages
        self.doi = doi
        self.other_metadata = kwargs

    def __str__(self) -> str:
        """Prints out the reference in the pipeliner's display format

        Format is:
            <authors>
            <title>
            <journal>(<year>) <vol>(<issue>) doi: <doi>
            <other metadata key 1>: <other_metadata value 1>
            ...
            <other metadata key n>: <other_metadata value n>

        Returns:
            str: The formatted reference
        """
        thestring = []
        if 0 < len(self.authors) < 5:
            thestring.append(f"{', '.join(self.authors)}")
        elif len(self.authors) > 5:
            thestring.append(f"{self.authors[0]} et al.")
        if self.title != "":
            thestring.append(f"\n{self.title}")

        journal = f"{self.journal} " if self.journal != "" else ""
        year = f"({self.year})" if self.year != "" else ""
        vol = f" {self.volume}" if self.volume != "" else ""
        issue = f"({self.issue})" if self.issue != "" else ""
        pages = f":{self.pages}" if self.pages != "" else ""
        doi = f" doi: {self.doi}" if self.doi != "" else ""
        thestring.append(f"\n{journal}{year}{vol}{issue}{pages}{doi}")
        if len(self.other_metadata) > 0:
            for addarg in self.other_metadata:
                thestring.append(f"\n{addarg}: {self.other_metadata[addarg]}")

        return "".join(thestring)

    def __eq__(self, other) -> bool:
        """Determine if two references are the same paper...

        Try to give some leeway for slightly different punctuation and abbreviations in
        journal names
        """
        # easy way: do the dois match?
        if self.doi and other.doi:
            return True if self.doi == other.doi else False

        # or do it the hard way
        # compare titles
        titles = [re.findall("[a-z0-9]", x.title.lower()) for x in (self, other)]
        titles_match = titles[0] == titles[1]

        # compare journals
        jsplit1 = [re.findall("[a-z0-9]", x.lower()) for x in self.journal.split(" ")]
        jsplit2 = [re.findall("[a-z0-9]", x.lower()) for x in other.journal.split(" ")]

        jtests = []
        for n, i in enumerate(jsplit1):
            if len(i) == len(jsplit2[n]):
                jtests.append(i == jsplit2[n])
            elif len(i) < len(jsplit2[n]):
                jtests.append(jsplit2[n][: len(i)] == i)
            else:
                jtests.append(jsplit2[n] == i[: len(jsplit2[n])])
        journals_match = all(jtests)

        # compare years
        years_match = self.year == other.year

        return all((years_match, titles_match, journals_match))


class JobInfo(object):
    """
    Class for storing info about jobs.

    This is used to generate documentation for the job within the
    pipeliner

    Attributes:
        display_name (str): A user-friendly name to describe the job in a GUI, this
            should not include the software used, because that info is pulled from
            the job type
        version (str): The version number of the pipeliner job
        job_author (str): Who wrote the pipeliner job
        short_desc (str): A one line "title" for the job
        long_desc (str): A detained description about what the job does
        documentation (str): A URL for online documentation
        programs (List[`~pipeliner.pipeliner_job.ExternalProgram`]): A list of
            3rd party software used by the job.  These are used by the pipeliner
            to determine if the job can be run, so they need to include
            all executables the job might call. If any program on this list
            cannot be found with `which` then the job will be marked as unable to run.
        references (list): A list of
            :class:`~pipeliner.pipeliner_job.Ref` objects
    """

    def __init__(
        self,
        display_name: str = "Pipeliner job",
        version: str = "0.0",
        job_author: Optional[str] = None,
        short_desc: str = "No short description for this job",
        long_desc: str = "No long description for this job",
        documentation: str = "No online documentation available",
        external_programs: Optional[List[ExternalProgram]] = None,
        references: Optional[List[Ref]] = None,
    ):
        """Create a JobInfo object

        Args:
            display_name (str): A user-friendly name to describe the job in a GUI, this
                should not include the software used, because that info is pulled from
                the job type
            version (str): The job version number
            job_author (str): Who wrote the job for the pipeliner
            short_desc (str): A one line description of what the job does
            long_desc (str):  A longer description with background info
            documentation (str): Address for on-line documentation
            external_programs (List[:class:~`pipeliner.pipeliner_job.ExternalProgram]):
                A list of :class:ExternalProgram objects for the  programs used by the
                job
            references (list): A list of :class:Ref objects for the job's for the
                main papers describing the software used
            availability_checks (bool): This can be set to False if other checks
                for the job to be available (besides programs missing from the $PATH)
                have failed.  IE: A directory is missing
        """
        self.display_name = display_name
        self.version = version
        self.job_author = job_author
        self.short_desc = short_desc
        self.long_desc = long_desc
        self.documentation = documentation
        self.programs = external_programs if external_programs is not None else []
        self.references = references if references is not None else []
        self.availability_checks = True

    @property
    def is_available(self):
        """``True`` if executables were found for all the job's programs, or ``False``
        otherwise."""
        checks = [prog.exe_path for prog in self.programs] + [self.availability_checks]
        return all(checks)


class PipelinerJob(object):
    """
    Super-class for job objects.

    Each job type has its own sub-class.

    WARNING: do not instantiate this class directly, use the factory functions in this
    module.

    Attributes:
        jobinfo (:class:`~pipeliner.pipeliner_job.JobInfo`): Contains
            information about the job such as references
        output_dir (str): The path of the output directory created by this job
        alias (str): the alias for the job if one has been assigned
        is_continue (bool): If this job is a continuation of an older job or a new one
        input_nodes (list): A list of :class:`~pipeliner.nodes.Node` objects
            for each file used as in input
        output_nodes (list): A list of :class:`~pipeliner.nodes.Node` objects
            for files produced by the job
        joboptions (dict): A dict of :class:`~pipeliner.job_options.JobOption` objects
            specifying the parameters for the job
        final_commands (list): A list of commands to be run by the job.  Each item is a
            list of arguments
        is_mpi (bool): Does the job use multi-threading?
        is_tomo (bool): Is the job a tomography job?
        working_dir (str): The working directory to be used when running the job. This
            should normally be left as ``None``, meaning the job will be run in the
            project directory. Jobs that write files in their working directory should
            instead work somewhere within the job's output directory, and take care to
            adjust the paths of input and output files accordingly.
        raw_options (dict): A dict of all raw joboptions as they were read in

    """

    PROCESS_NAME = ""
    OUT_DIR = ""

    def __init__(self):
        """Creates a PipelinerJob object

        Raises:
            NotImplementedError: If this class is instantiated directly except in
                testing
        """
        if type(self) == PipelinerJob:
            raise NotImplementedError(
                "Cannot create PipelinerJob objects directly - use a sub-class"
            )

        self.jobinfo = JobInfo()
        self.output_dir = ""
        self.alias = None
        self.is_continue = False
        self.input_nodes: List[Node] = []
        self.output_nodes: List[Node] = []
        self.joboptions = {}
        self.final_commands: List[List[List[str]]] = []
        self.is_mpi = False
        self.is_tomo = False
        self.type = ""
        self.working_dir = None  # this should typically be None or self.output_dir
        self.raw_options = {}

        # If the job is run multiple times in a schedule
        # should every subsequent run always be a continuation job?
        self.always_continue_in_schedule = False

        # if the job is continued should the previous output
        # nodes be deleted first?
        self.del_nodes_on_continue = False

    def clear(self):
        """Clear all attributes of the job"""

        self.output_dir = ""
        self.alias = None
        self.input_nodes.clear()
        self.output_nodes.clear()
        self.is_continue = False

    def set_option(self, line: str):
        """Sets a value in the joboptions dict from a run.job file

        Args:
            line (str): A line from a run.job file

        Raises:
            RuntimeError: If the line does not contain '=='
            RuntimeError: If the value of the line does not match any of the
                joboptions keys
        """

        if len(line) == 0:
            return
        if "==" not in line:
            raise RuntimeError("No '==' entry on JobOption line: {}".format(line))
        key, val = line.split(" == ")
        found = None
        for k, v in self.joboptions.items():
            if v.label == key:
                found = k
                break
        if found is None:
            raise RuntimeError("Job does not contain label: {}".format(key))
        self.joboptions[found].set_string(val)

    def read(self, filename: str):
        """Reads parameters from a run.job or job.star file

        Args:
            filename (str): The file to read.  Can be a run.job or job.star file

        Raises:
            ValueError: If the file is a job.star file and job option from the
                :class:`~pipeliner.pipeliner_job.PipelinerJob` is missing
                from the input file
        """
        if filename.endswith(".job"):
            # make a dict of params from the file {joboption_label: joboption_value}
            with open(filename) as job_file:
                job_lines = job_file.readlines()
            job_lines_split = [x.partition("==") for x in job_lines]
            j_param = {key.strip(): val.strip() for key, _, val in job_lines_split}
            self.raw_options = j_param

            # overwrite joboption values in the job with those from the file
            self.is_continue = True if j_param.get("is_continue") == "true" else False
            self.is_tomo = True if j_param.get("is_tomo") == "true" else False

            for joboption in self.joboptions.values():
                try:
                    joboption.value = j_param[joboption.label]

                # if a joboption is missing from the file just use the default
                except KeyError:
                    print(
                        f"WARNING: {filename}; no value for `{joboption.label}` "
                        f"using default value: {joboption.default_value}"
                    )
        elif filename.endswith("job.star"):
            job_options = JobStar(filename).all_options_as_dict()
            job_type_label = get_job_type(job_options)[1]
            self.raw_options = job_options

            self.is_continue = job_options["_rlnJobIsContinue"].lower() in TRUES
            del job_options[job_type_label]
            del job_options["_rlnJobIsContinue"]
            del job_options["_rlnJobIsTomo"]
            for joboption in self.joboptions:
                # overwrite joboption values with those from the file
                try:
                    if type(job_options[joboption]) is str:
                        job_options[joboption] = smart_strip_quotes(
                            job_options[joboption]
                        )
                    self.joboptions[joboption].value = job_options[joboption]

                # if a joboption is missing from the file just use the defaule
                except KeyError:
                    print(
                        f"WARNING: {filename}; no value for "
                        f"'{self.joboptions[joboption].label}' using default value:"
                        f" {self.joboptions[joboption].default_value}"
                    )

    def write_runjob(self, fn: Optional[str] = None):
        """Writes a run.job file

        Args:
            fn (str): The name of the file to write. Defaults to the file the pipeliner
                uses for storing GUI parameters.  A directory can also be entered and it
                will add on the file name 'run.job'

        """
        self.add_compatibility_joboptions()
        final_order = []
        groupings = self.get_joboption_groups()
        for group in groupings:
            final_order += groupings[group]

        hidden_name = f".gui_{self.PROCESS_NAME.replace('.','_')}"
        filename = hidden_name if fn is None else fn
        if filename != hidden_name and not filename.endswith("run.job"):
            filename += "run.job"
        with open(filename, "w+") as f:
            f.write("job_type == {}\n".format(self.PROCESS_NAME))
            cont_val = "true" if self.is_continue else "false"
            f.write(f"is_continue == {cont_val}\n")
            tomo_val = "true" if self.is_tomo else "false"
            f.write(f"is_tomo == {tomo_val}\n")
            for jo in final_order:
                label = self.joboptions[jo].label
                value = str(self.joboptions[jo].value)
                f.write(label + " == " + value + "\n")

    def add_compatibility_joboptions(self):
        """Write additional joboptions for back compatibility

        Some JobOptions are needed by the original program (hey Relion 4), but
        not the pipeliner, they are added here so the files pipeliner writes will
        be back compatible with their original program.
        """
        pass

    def default_params_dict(self) -> dict:
        """Get a dict with the job's parameters and default values

        Returns:
            dict: All the job's parameters {parameter: default value}
        """
        params_dict = {}
        for jo in self.joboptions:
            params_dict[jo] = self.joboptions[jo].default_value
        params_dict["_rlnJobTypeLabel"] = self.PROCESS_NAME
        continue_val = "1" if self.is_continue else "0"
        params_dict["_rlnJobIsContinue"] = continue_val
        tomo_val = "1" if self.is_tomo else "0"
        params_dict["_rlnJobIsTomo"] = tomo_val
        return params_dict

    def write_jobstar(
        self,
        output_dir: str,
        output_fn: str = "job.star",
        is_continue: bool = False,
    ):
        """Write a job.star file.

        Args:
            output_dir (str): The output directory.
            output_fn (str): The name of the file to write. Defaults to job.star
            is_continue (bool): Is the file for a continuation of a previously run job?
                If so only the parameters that can be changed on continuation are
                written.  Overrules is_continue attribute of the job
        """
        self.add_compatibility_joboptions()
        filename = output_dir + output_fn
        jobstar = cif.Document()

        job_block = jobstar.add_new_block("job")
        job_block.set_pair("_rlnJobTypeLabel", str(self.PROCESS_NAME))
        continue_val = "1" if is_continue else str(int(self.is_continue))
        job_block.set_pair("_rlnJobIsContinue", continue_val)
        tomo_val = "1" if self.is_tomo else "0"
        job_block.set_pair("_rlnJobIsTomo", tomo_val)

        jobop_block = jobstar.add_new_block("joboptions_values")
        jobop_loop = jobop_block.init_loop(
            "_", ["rlnJobOptionVariable", "rlnJobOptionValue"]
        )

        rows = []
        for option in self.joboptions:
            if not is_continue:
                rows.append(
                    [
                        cif.quote(option),
                        cif.quote(str(self.joboptions[option].value)),
                    ]
                )
            else:
                if self.joboptions[option].in_continue:
                    rows.append(
                        [
                            cif.quote(option),
                            cif.quote(str(self.joboptions[option].value)),
                        ]
                    )
        rows.sort(key=lambda x: x[0])
        for row in rows:
            jobop_loop.add_row(row)

        write(jobstar, filename)

    def initialise_pipeline(
        self,
        output_dir: str,
        defaultname: str,
        job_counter: int,
    ) -> str:
        """Gets the pipeline ready to add a new job

        Sets the output name and clears the input and output nodes

        Args:
            output_dir (str): Where the job should write its results.  If blank it
                is set to the next job number based on the job counter
            defaultname (str): The name of the job type
            job_counter (int): The number that job will get

        Returns:
            str: The output name
        """
        self.output_nodes = []
        self.input_nodes = []

        if output_dir == "":  # for continue jobs, use the same output name
            output_dir = "{}/job{:03}/".format(defaultname, job_counter)
        self.output_dir = output_dir
        return output_dir

    def save_job_submission_script(
        self, output_script: str, output_dir: str, commands: list, nmpi: int
    ) -> str:
        """Writes a submission script for jobs submitted to a queue

        Args:
            output_script (str): The name for the script to be written
            output_dir (str): The job's output directory
            commands (list): The job's commands. In a list of lists format
            nmpi (int): The number of MPI used by the job.  Should be 1 if the job is
                not multi-threaded

        Returns:
            str: The name of the submission script that was written

        Raises:
            ValueError: If no submission script template was specified in the job's
                joboptions
            ValueError: If the submission script template is not found
            RuntimeError: If the output script could not be written
        """
        # error checking
        fn_qsub = self.joboptions["qsubscript"].get_string(
            True, "ERROR: no submission script template specified"
        )
        if not os.path.isfile(fn_qsub):
            raise ValueError("Error reading template submission script in: " + fn_qsub)
        try:
            touch(output_script)
        except Exception as ex:
            raise RuntimeError(
                "Error writing to job submission script in: " + output_dir
            ) from ex

        nthr = self.joboptions.get("nr_threads", 1)
        if nthr != 1:
            nthr = int(nthr.get_number())

        ncores = nmpi * nthr
        ndedi = self.joboptions["min_dedicated"].get_number()
        fnodes = ncores / ndedi
        nnodes = math.ceil(fnodes)
        # should this be written to the run.err as well?
        # should we set ndedi to ncores if ncores < ndedi?
        if fnodes % 1 != 0:
            print(
                " Warning! You're using {} MPI processes with {} threads each "
                "(i.e. {} cores), while asking for {} nodes with {} cores.\n"
                "It is more efficient to make the number of cores (i.e. mpi*threads)"
                " a multiple of the minimum number of dedicated cores per"
                " node ".format(
                    truncate_number(nmpi, 0),
                    truncate_number(nthr, 0),
                    truncate_number(ncores, 0),
                    truncate_number(nnodes, 0),
                    truncate_number(ndedi, 0),
                )
            )

        queuename = self.joboptions["queuename"].get_string()
        qsub_extra_1 = self.joboptions.get("qsub_extra_1")
        qsub_extra_2 = self.joboptions.get("qsub_extra_2")
        qsub_extra_3 = self.joboptions.get("qsub_extra_3")
        qsub_extra_4 = self.joboptions.get("qsub_extra_4")
        extras = {}
        n = 1
        for extra in (qsub_extra_1, qsub_extra_2, qsub_extra_3, qsub_extra_4):
            qsubvar = "qsub_extra_" + str(n)
            if extra is not None:
                extras[qsubvar] = extra.get_string()
            else:
                extras[qsubvar] = None
            n += 1

        quoted_commands = quotate_command_list(commands)

        replacements = {
            "XXXmpinodesXXX": str(int(nmpi)),
            "XXXthreadsXXX": str(int(nthr)),
            "XXXcoresXXX": str(int(ncores)),
            "XXXdedicatedXXX": str(int(ndedi)),
            "XXXnameXXX": output_dir,
            "XXXerrfileXXX": output_dir + "run.err",
            "XXXoutfileXXX": output_dir + "run.out",
            "XXXqueueXXX": queuename,
            "XXXextra1XXX": extras["qsub_extra_1"],
            "XXXextra2XXX": extras["qsub_extra_2"],
            "XXXextra3XXX": extras["qsub_extra_3"],
            "XXXextra4XXX": extras["qsub_extra_4"],
            "XXXcommandXXX": "\n".join([" ".join(x) for x in quoted_commands]),
        }

        with open(fn_qsub) as template_data:
            template = template_data.readlines()

        with open(output_script, "w") as outscript:
            for line in template:
                for sub in replacements:
                    if sub in line:
                        line = line.replace(sub, replacements[sub])
                outscript.write(line)
        return fn_qsub

    def handle_doppio_userfiles(self, commands: List[list]) -> List[list]:
        """Tasks that have to be performed to deal with the doppio dropzone

        move files from UserFiles to the output dir
        update the input node names (UserFiles/xxx -> OutputDir/XXX)
        update the joboptions so everything else subsequently written looks like the
        files had always been in the output dir to begin with

        Args:
            commands (List[list]): The commands to run in list of lists format

        Returns:
            List[list]: The updated commands
        """
        # this job handles all this stuff on it's own
        if self.PROCESS_NAME == "relion.import.other":
            return commands

        # otherwise do it here
        if self.working_dir:
            uf_rel_path = os.path.relpath("UserFiles", self.working_dir)
        else:
            uf_rel_path = "UserFiles"

        # update the joboptions
        com_files, mv_files = {}, {}
        for jo in self.joboptions:
            if jo != "qsubscript":
                jobop = self.joboptions[jo]
                if (
                    jobop.joboption_type in ["FILENAME", "INPUTNODE", "MULTIFILENAME"]
                    and "UserFiles/" in jobop.value
                ):
                    files = (
                        jobop.get_list()
                        if jobop.joboption_type == "MULTIFILENAME"
                        else [jobop.get_string()]
                    )
                    for f in files:
                        updated = f.replace("UserFiles/", self.output_dir)
                        if not self.working_dir:
                            fp = updated
                        else:
                            fp = os.path.relpath(updated, self.working_dir)
                        uf_file = f.replace("UserFiles", uf_rel_path)
                        com_files[uf_file] = fp
                        fn = os.path.basename(f)
                        true_path = os.path.join(self.output_dir, fn)
                        if "UserFiles/" in f:
                            mv_files[true_path] = (uf_file, fp)
                    jobop.value = jobop.value.replace("UserFiles/", self.output_dir)
        # update the final command list
        updated_coms = []

        for subcom in commands:
            for key in com_files.keys():
                subcom = [x.replace(key, com_files[key]) for x in subcom]
            updated_coms.append(subcom)

        # update the nodes
        # first make all the input nodes
        self.create_input_nodes()
        for node in self.input_nodes:
            if node.name in mv_files:
                mf = mv_files[node.name]
                updated_coms.insert(0, ["mv", mf[0], mf[1]])

        # sanity check make sure no files will be accidentally overwritten
        nodenames = [x.name for x in self.input_nodes + self.output_nodes]
        doubles = [i for i, x in enumerate(nodenames) if nodenames.count(x) > 1]
        if doubles:
            baddies = set([nodenames[x] for x in doubles])
            # remove the uploaded files
            for f in mv_files:
                if os.path.isfile(mv_files[f][0]):
                    os.remove(mv_files[f][0])
            raise ValueError(
                "The following uploaded file(s) have the same name as another node in "
                "this job, rename the file before uploading: "
                f"{', '.join([x for x in baddies])}"
            )
        return updated_coms

    def prepare_final_command(
        self,
        commands: List[list],
        do_makedir: bool,
        ignore_queue: bool = False,
    ) -> list:
        """Assemble commands to be run for a job

        The commands are in a lists of lists format.  Each item in the main list is
        a single command and composed of a list of the arguments for that command.

        An additional command to run the check completion script is added to the
        commands list.

        Decides if a queue submission script is needed. If so it is written and the
        commands list is changed to the queue submission command

        Args:
            commands (list): The commands to run as a list of lists
            do_makedir (bool): Should the output directory be created if it doesn't
                already exist?
            ignore_queue (bool): Do not make a submission script, even if the job is
                sent to the queue, used for generating commands for display

        Returns:
            list: [[[Actual, command], [to, be, run]], [[the, Job, commands]]] If the
                job is being submitted to a queue `[0]` will be the qsub command and
                `[1]` will be the actual job commands.  For local jobs they will
                be identical
        """

        # make sure the commands list is all strings
        commands = [[str(x) for x in i] for i in commands]
        commands = self.handle_doppio_userfiles(commands)

        # Create output directory
        if do_makedir:
            job_dir = os.path.dirname(self.output_dir)
            if not os.path.isdir(job_dir) and len(job_dir) > 0:
                os.makedirs(job_dir)

        # check for MPI
        nr_mpi_jo = self.joboptions.get("nr_mpi")
        if nr_mpi_jo is not None:
            nr_mpi = int(nr_mpi_jo.get_number())
        else:
            nr_mpi = 0
        self.is_mpi = True if nr_mpi > 1 else False

        # run the job locally or submit to queue
        do_queue = self.joboptions.get("do_queue")
        if do_queue is not None and not ignore_queue:
            do_queue = do_queue.get_boolean()
        else:
            do_queue = False

        # if there are no specified output nodes use the run.out logfile
        if len(self.output_nodes) == 0:
            run_of = os.path.join(self.output_dir, "run.out")
            self.output_nodes.append(create_node(run_of, NODE_LOGFILE))

        # perform the status check to see if the job finished successfully
        outfile_names = [x.name.replace(self.output_dir, "") for x in self.output_nodes]
        check_comp = os.path.join(get_pipeliner_root(), "scripts/check_completion.py")
        odir_from_wdir = os.path.relpath(self.output_dir, self.working_dir)

        if len(outfile_names) > 0:
            commands.append([check_comp, odir_from_wdir, *outfile_names])
        if do_queue:
            output_script = self.output_dir + "run_submit.script"

            PipelinerJob.save_job_submission_script(
                self, output_script, self.output_dir, commands, nr_mpi
            )

            qsub = self.joboptions["qsub"].get_string()
            self.final_commands = [[[qsub, output_script]]]

        else:
            # check for allowed number of mpis
            warn = user_settings.get_warning_local_mpi()
            if warn is not None and nr_mpi > warn:
                errmsg = (
                    f"WARNING: you're submitting a local job with {nr_mpi} parallel MPI"
                    f" processes. Only {warn} are allowed by the"
                    f" PIPELINER_WARNING_LOCAL_MPI environment variable. This might"
                    f" cause the job to fail."
                )
                print(errmsg)
                with open(os.path.join(self.output_dir, "run.err"), "a") as errfile:
                    errfile.write(errmsg)

            self.final_commands = [commands]

        # add the 2nd copy of the job commands
        self.final_commands.append(commands)
        return self.final_commands

    def get_runtab_options(
        self, mpi: bool = False, threads: bool = False, addtl_args: bool = False
    ):
        """Get the options found in the Run tab of the GUI,
        which are common to for all jobtypes

        Adds entries to the joboptions dict for queueing, MPI, threading, and
        additional arguments. This method should be used when initialising a
        :class:`~pipeliner.pipeliner_job.PipelinerJob` subclass

        Args:
            mpi (bool): Should MPI options be included?
            threads (bool): Should multi-threading options be included
            addtl_args (bool): Should and 'additional arguments' be added

        """

        mpimax = user_settings.get_mpi_max()
        default_nrmpi = user_settings.get_default_nrmpi()
        if mpi:
            self.joboptions["nr_mpi"] = IntJobOption(
                label="Number of MPI procs:",
                default_value=default_nrmpi,
                suggested_min=1,
                suggested_max=mpimax,
                step_value=1,
                help_text=(
                    "Number of MPI nodes to use in parallel. When set to 1, MPI will"
                    " not be used. The maximum can be set through the environment"
                    " variable PIPELINER_MPI_MAX and the default can be set using"
                    " PIPELINER_DEFAULT_NRMPI."
                ),
                in_continue=True,
                jobop_group="Running options",
            )
            mpirun_cmd = user_settings.get_mpirun_command()
            self.joboptions["mpi_command"] = StringJobOption(
                label="MPI run command:",
                default_value=mpirun_cmd,
                help_text=(
                    "This command will be prepended to jobs that use MPI. The number of"
                    " MPIs specified in 'Number of MPI procs:' will be substituted for"
                    " 'XXXmpinodesXXX' in the final command. The default can be set"
                    " through the environment variable PIPELINER_MPIRUN_COMMAND. Note"
                    " that the RELION_MPI_RUN environment variable is ignored."
                ),
                in_continue=True,
                required_if=[("nr_mpi", ">", 1)],
                deactivate_if=[("nr_mpi", "=", 1)],
                jobop_group="Running options",
            )

        if threads:
            threadmax = user_settings.get_thread_max()
            default_nrthreads = user_settings.get_default_nrthreads()
            self.joboptions["nr_threads"] = IntJobOption(
                label="Number of threads:",
                default_value=default_nrthreads,
                suggested_min=1,
                suggested_max=threadmax,
                step_value=1,
                help_text=(
                    "Number of shared-memory (POSIX) threads to use in"
                    " parallel. When set to 1, no multi-threading will"
                    " be used. The maximum can be set through the environment"
                    " variable PIPELINER_THREAD_MAX and the default can be set using"
                    " PIPELINER_DEFAULT_NRTHREADS."
                ),
                in_continue=True,
                jobop_group="Running options",
            )

        self.make_queue_options()

        if addtl_args:
            self.make_additional_args()

    def get_mpi_command(self) -> List[str]:
        nr_mpi = str(self.joboptions["nr_mpi"].get_number())
        mpi_str = self.joboptions["mpi_command"].get_string()
        update_mpi_str = mpi_str.replace("XXXmpinodesXXX", nr_mpi)
        return shlex.split(update_mpi_str)

    def make_additional_args(self):
        """Get the additional arguments job option"""
        self.joboptions["other_args"] = StringJobOption(
            label="Additional arguments:",
            default_value="",
            help_text="",
            in_continue=True,
            jobop_group="Running options",
        )

    def make_queue_options(self):
        """Get options related to queueing and queue submission,
        which are common to for all jobtypes"""

        queue_default = user_settings.get_queue_use()

        self.joboptions["do_queue"] = BooleanJobOption(
            label="Submit to queue?",
            default_value=queue_default,
            help_text=(
                "If set to Yes, the job will be submit to a queue,"
                " otherwise the job will be executed locally. Note "
                "that only MPI jobs may be sent to a queue. The default "
                "can be set through the environment variable PIPELINER_QUEUE_USE."
            ),
            in_continue=True,
            jobop_group="Queue submission options",
        )

        queue_name_default = user_settings.get_queue_name()
        self.joboptions["queuename"] = StringJobOption(
            label="Queue name:",
            default_value=queue_name_default,
            help_text=(
                "Name of the queue to which to submit the job. The"
                " default name can be set through the environment"
                " variable PIPELINER_QUEUE_NAME."
            ),
            in_continue=True,
            deactivate_if=[("do_queue", "=", False)],
            required_if=[("do_queue", "=", True)],
            jobop_group="Queue submission options",
        )

        qsub_default = user_settings.get_qsub_command()
        self.joboptions["qsub"] = StringJobOption(
            label="Queue submit command:",
            default_value=qsub_default,
            help_text=(
                "Name of the command used to submit scripts to the queue,"
                " e.g. qsub or bsub. Note that the person who installed PIPELINER"
                " should have made a custom script for your cluster/queue setup."
                " Check this is the case (or create your own script"
                " if you have trouble submitting jobs. The default "
                "command can be set through the environment variable "
                "PIPELINER_QSUB_COMMAND."
            ),
            in_continue=True,
            deactivate_if=[("do_queue", "=", False)],
            required_if=[("do_queue", "=", True)],
            jobop_group="Queue submission options",
        )

        qsub_template_default = user_settings.get_qsub_template()
        self.joboptions["qsubscript"] = PathJobOption(
            label="Standard submission script:",
            default_value=qsub_template_default,
            help_text=(
                "The template for your standard queue job submission script. Its"
                " default location may be changed by setting the environment variable"
                " PIPELINER_QSUB_TEMPLATE. In the template script a number of variables"
                " will be replaced: \nXXXcommandXXX = relion command + arguments;"
                " \nXXXqueueXXX = The queue name;\nXXXmpinodesXXX = The number of MPI"
                " nodes;\nXXXthreadsXXX = The number of threads;\nXXXcoresXXX ="
                " XXXmpinodesXXX * XXXthreadsXXX;\nXXXdedicatedXXX = The minimum number"
                " of dedicated cores on each node;\nXXXnodesXXX = The number of"
                " requested nodes = CEIL(XXXcoresXXX / XXXdedicatedXXX);\nIf these"
                " options are not enough for your standard jobs, you may define a"
                " user-specified number of extra variables: XXXextra1XXX, XXXextra2XXX,"
                " etc. The number of extra variables is controlled through the"
                " environment variable PIPELINER_QSUB_EXTRA_COUNT. Their help text is"
                " set by the environment variables PIPELINER_QSUB_EXTRA1,"
                " PIPELINER_QSUB_EXTRA2, etc For example, setenv"
                " PIPELINER_QSUB_EXTRA_COUNT 1, together with setenv"
                " PIPELINER_QSUB_EXTRA1 'Max number of hours in queue' will result in"
                " an additional (text) ein the GUI Any variables XXXextra1XXX in the"
                " template script will be replaced by the corresponding value.Likewise,"
                " default values for the extra entries can be set through environment"
                " variables PIPELINER_QSUB_EXTRA1_DEFAULT,"
                " PIPELINER_QSUB_EXTRA2_DEFAULT, etc. But note that (unlike all other"
                " entries in the GUI) the extra values are not remembered from one run"
                " to the other."
            ),
            in_continue=True,
            deactivate_if=[("do_queue", "=", False)],
            required_if=[("do_queue", "=", True)],
            jobop_group="Queue submission options",
        )

        min_cores_default = user_settings.get_minimum_dedicated()
        self.joboptions["min_dedicated"] = IntJobOption(
            label="Minimum dedicated cores per node:",
            default_value=min_cores_default,
            suggested_min=1,
            suggested_max=64,
            step_value=1,
            help_text=(
                "Minimum number of dedicated cores that need to be requested "
                "on each node. This is useful to force the queue to fill up entire "
                "nodes of a given size. The default can be set through the environment"
                " variable PIPELINER_MINIMUM_DEDICATED."
            ),
            in_continue=True,
            deactivate_if=[("do_queue", "=", False)],
            required_if=[("do_queue", "=", True)],
            jobop_group="Queue submission options",
        )

        self.get_extra_options()

    def get_extra_options(self):
        """Get user specified extra queue submission options"""
        n_extra = user_settings.get_qsub_extra_count()
        if n_extra > 0:
            for extranum in range(1, n_extra + 1):
                extras = user_settings.get_qsub_extras(extranum)
                self.joboptions["qsub_extra_{}".format(extranum)] = StringJobOption(
                    label=extras["name"],
                    default_value=extras["default"],
                    help_text=extras["help"],
                    in_continue=True,
                    jobop_group="Queue submission options",
                )

    def gather_metadata(self):
        """
        Placeholder function for metadata gathering

        Each job class should define this individually

        Returns:
            dict: A placeholder "No metadata available" and the reason why

        """

        return {"No metadata": "No metadata available for this job type"}

    def prepare_clean_up_lists(self, do_harsh: bool = False):
        """
        Placeholder function for preparation of list of files to clean up

        Each job class should define this individually

        Args:
            do_harsh (bool): Should a harsh cleanup be performed

        Returns:
            tuple: Two empty lists ``([files, to, delete], [dirs, to, delete])``
        """
        if do_harsh:
            return [], []

        return [], []

    def get_commands(self):
        raise NotImplementedError(
            "The get_commands method must be overwritten in subclasses of PipelinerJob"
        )

    def post_run_actions(self):
        """
        Placeholder function for actions to do after the job has finished

        Each job class should define this individually.  This is used for job where
        somthing needs to be done after the job has completed, such as jobs where the
        number/names of output nodes is not known until the job has finished
        """

        pass

    def get_current_output_nodes(self) -> list:
        """
        Get the current output nodes if the job was stopped prematurely

        For most jobs there will not be any but for jobs with many iterations
        the most recent interation can be used of teh job is aborted or failed
        and then later marked as successful

        Returns:
            list: of :class:`~pipeliner.nodes.Node` objects
        """

        return []

    def parameter_validation(self) -> List[JobOptionValidationResult]:
        """Advanced validation of job parameters

        This is a placeholder function for additional validation to be done by
        individual job subtypes, such as comparing JobOption values IE:
        JobOption A must be > JobOption B

        returns:
            list: A list :class:`~pipeliner.job_options.JobOptionValidationResult`
                objects
        """
        return []

    def validate_joboptions(self) -> List[JobOptionValidationResult]:
        """Make sure all the joboptions meet their validation criteria

        Returns:
            list: A list :class:`~pipeliner.job_options.JobOptionValidationResult`
                objects

        """
        errors = []
        for joname in self.joboptions:
            jo = self.joboptions[joname]
            errs = jo.validate()
            errors.extend(errs)

        adval = self.parameter_validation()
        adval = adval if adval is not None else []
        if len(adval) > 0:
            errors.extend(adval)

        dynamic_req_errors = self.validate_dynamically_required_joboptions()
        if len(dynamic_req_errors) > 0:
            errors.extend(dynamic_req_errors)

        return errors

    def check_joboption_is_now_deactivated(self, jo: str) -> bool:
        """Check if a joboption has become deactivated in relation to others

        For example if job option A is False, job option B is now deactiavted

        Args:
            jo (str): The name of the JobOption to test
        Returns:
            bool: Has the JobOption been deactivated
        """
        ops = {
            "=": operator.eq,
            "!=": operator.ne,
            ">": operator.gt,
            ">=": operator.ge,
            "<": operator.lt,
            "<=": operator.le,
        }

        jobop = self.joboptions[jo]
        if not jobop.deactivate_if:
            return False

        is_now_deact = False
        for deact in jobop.deactivate_if:
            parent, op, exp_value = deact
            parent_jo = self.joboptions[parent]
            pjo_type = type(parent_jo)
            if pjo_type == BooleanJobOption:
                compare_val = parent_jo.get_boolean()
            elif pjo_type in [FloatJobOption, IntJobOption]:
                compare_val = parent_jo.get_number()
            else:
                compare_val = parent_jo.get_string()
            is_now_deact = ops[op](compare_val, exp_value)

        return is_now_deact

    def check_joboption_is_now_required(self, jo: str) -> list:
        """Check if a joboption has become required in relation to others

        For example if job option A is True, job option B is now required

        Args:
            jo (str): The name of the joboption to test
        Returns:
            list: :class:`pipeliner.job_options.JobOptionValidationResult`:
                for any errors found
        """
        ops = {
            "=": operator.eq,
            "!=": operator.ne,
            ">": operator.gt,
            ">=": operator.ge,
            "<": operator.lt,
            "<=": operator.le,
        }

        jobop = self.joboptions[jo]
        if not jobop.required_if:
            return []

        jobop_type = type(jobop)
        if jobop_type == BooleanJobOption:
            jo_val = jobop.get_boolean()
        elif jobop_type in [FloatJobOption, IntJobOption]:
            jo_val = jobop.get_number()
        else:
            jo_val = jobop.get_string()
        errors = []
        for req in jobop.required_if:
            parent, op, exp_value = req
            parent_jo = self.joboptions[parent]
            pjo_type = type(parent_jo)
            if pjo_type == BooleanJobOption:
                compare_val = parent_jo.get_boolean()
            elif pjo_type in [FloatJobOption, IntJobOption]:
                compare_val = parent_jo.get_number()
            else:
                compare_val = parent_jo.get_string()
            is_now_req = ops[op](compare_val, exp_value)

            if is_now_req and jo_val in [None, ""]:
                msg = (
                    f'Because "{parent_jo.label}" {op} {exp_value},'
                    f' "{jobop.label}" is required'
                )
                errors.append(
                    JobOptionValidationResult("error", [jobop, parent_jo], msg)
                )
        return errors

    def validate_dynamically_required_joboptions(
        self,
    ) -> List[JobOptionValidationResult]:
        """Check all joboptions if they have become required because of if_required

        For example if job option A is True, job option B is now required

        Returns:
            list: :class:`pipeliner.job_options.JobOptionValidationResult`:
                for any errors found
        """

        errors = []
        for jo in self.joboptions:
            now_req_errors = self.check_joboption_is_now_required(jo)
            if now_req_errors:
                errors.extend(now_req_errors)
        return errors

    def validate_input_files(self) -> List[JobOptionValidationResult]:
        """Check that files specified as inputs actually exist

        Returns:
            list: A list of :class:`pipeliner.job_options.JobOptionValidationResult`
                objects
        """
        errors = []
        for joname in self.joboptions:
            jo = self.joboptions[joname]

            if jo.joboption_type in ["FILENAME", "INPUTNODE", "MULTIFILENAME", "PATH"]:
                is_req = jo.is_required
                if jo.required_if:
                    is_req = bool(self.check_joboption_is_now_required(joname))
                deactivated = self.check_joboption_is_now_deactivated(joname)
                if (not deactivated and jo.value) or is_req:
                    errs = jo.check_file()
                    errors.extend(errs)

        return errors

    def prepare_onedep_data(self) -> list:
        """Placeholder for function to return deposition data objects

        The specific list returned should be defined by each jobtype

        Returns:
            list: The deposition object(s) returned by the specific job.  These
                need to be of the types defined in `pipeliner.onedep_deposition`
        """

        return []

    def parse_additional_args(self) -> List[str]:
        """Parse the additional arguments job option and return a list

        Returns:
            list: A list ready to append to the command.  Quotated strings are preserved
                as quoted strings all others are split into individual items
        """

        return shlex.split(self.joboptions["other_args"].get_string())

    def get_additional_reference_info(self) -> List[Ref]:
        """A placeholder function for job that need to return additional references

        This if for references that are not included in self.job info, such as ones
        pulled from the EMDB/PDB in fetch jobs
        """
        return []

    def create_input_nodes(self):
        """Automatically add the job's input nodes to its nodelist"""
        curr_in_nodes = [(x.name, x.type) for x in self.input_nodes]
        for jop in self.joboptions.values():
            nodes = jop.get_input_nodes()
            for node in nodes:
                node_exists = (node.name, node.type) in curr_in_nodes
                if not node_exists:
                    self.input_nodes.append(node)

    def create_results_display(self) -> list:
        """This function creates the objects to be displayed by the GUI

        Placeholder for individual jobs to have their own

        Returns:
            list: a :class:`~pipeliner.results_display_objects.ResultsDisplayText`
                saying there is no specific method for this job
        """
        rdos = []
        for node in self.output_nodes:
            dispo = node.default_results_display(self.output_dir)
            rdos.append(dispo)
        return rdos

    def set_joboption_order(self, new_order=List[str]):
        """Replace the joboptions dict with an ordered dict

        Use this to set the order the joboptions will appear in the GUI. If a joboption
        is not specified in the list it will be tagged on to the end of the list.


        Args:
            new_order (list[str]): A list of joboption keys, in the order they should
                appear

        Raises:
            ValueError: If a nonexistent joboption is specified
        """
        # error checking
        jo_keys = list(self.joboptions)
        bad = []
        for njo in new_order:
            if njo not in jo_keys:
                bad.append(njo)
        if bad:
            raise ValueError(f"{bad} not in self.joboptions dict")

        # make the ordered dict
        ordered_jos = [self.joboptions[x] for x in new_order]
        ordered_jodict = OrderedDict(zip(new_order, ordered_jos))

        # add any not included in the list
        for jokey in jo_keys:
            if jokey not in ordered_jodict:
                ordered_jodict[jokey] = self.joboptions[jokey]

        self.joboptions = ordered_jodict

    def get_joboption_groups(self) -> Dict[str, List[str]]:
        """Put the joboptions in groups according to their jobop_group attribute

        Assumes that the joboptions have already been put in order of priority by
        self.set_joboption_order() or were in order to begin with.

        Groups are ordered based on the highest priority joboption in that group from
        the order of the joboptions, except that "Main" is always the first group.
        Joboptions within the groups are ordered by priority.

        Returns:
            Dict[str, List[str]]: The joboptions groupings {group: [jopbop, ... jobop]}
        """

        groupings: Dict[str, List[str]] = {"Main": []}
        for jo in self.joboptions:
            group = self.joboptions[jo].jobop_group
            if group in groupings:
                groupings[group].append(jo)
            else:
                groupings[group] = [jo]

        return groupings
