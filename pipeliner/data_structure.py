#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
from pipeliner.nodes import (
    NODE_LOGFILE,
    NODE_MASK3D,
    NODE_MICROGRAPHCOORDSGROUP,
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_MICROGRAPHMOVIEGROUPMETADATA,
    NODE_PARTICLESDATA,
    NODE_PROCESSDATA,
    NODE_DENSITYMAP,
    NODE_IMAGE2DGROUPMETADATA,
    NODE_IMAGE3D,
)

# processes variables
IMPORT_MOVIES_NAME = "relion.import.movies"
IMPORT_OTHER_NAME = "relion.import.other"
IMPORT_DIR = "Import"
IMPORT_RELIONSTYLE_NAME = "relion.import"
IMPORT_TYPE_NUM = 0  # deprecated
IMPORT_COORDS_NAME = "relion.import.coordinates"

POSTPROCESS_JOB_NAME = "relion.postprocess"
POSTPROCESS_DIR = "PostProcess"
# no relionstyle name
POSTPROCESS_TYPE_NUM = 15  # deprecated

MASKCREATE_JOB_NAME = "relion.maskcreate"
MASKCREATE_DIR = "MaskCreate"
# no relionstyle name
MASKCREATE_TYPE_NUM = 12  # deprecated

CTFFIND_CTFFIND4_NAME = "relion.ctffind.ctffind4"
CTFFIND_GCTF_NAME = "relion.ctffind.gctf"
CTFFIND_DIR = "CtfFind"
CTFFIND_RELIONSTYLE_NAME = "relion.ctffind"
CTFFIND_TYPE_NUM = 2  # deprecated

MOTIONCORR_OWN_NAME = "relion.motioncorr.own"
MOTIONCORR_MOTIONCOR2_NAME = "relion.motioncorr.motioncor2"
MOTIONCORR_DIR = "MotionCorr"
MOTIONCORR_RELIONSTYLE_NAME = "relion.motioncorr"  # will be deprecated
MOTIONCORR_TYPE_NUM = 1  # deprecated

MANUALPICK_JOB_NAME = "relion.manualpick"
MANUALPICK_HELICAL_NAME = "relion.manualpick.helical"
MANUALPICK_DIR = "ManualPick"
MANUALPICK_RELIONSTYLE_NAME = "relion.manualpick"  # will be deprecated
MANUALPICK_TYPE_NUM = 3  # deprecated

AUTOPICK_REF2D_NAME = "relion.autopick.ref2d"
AUTOPICK_REF3D_NAME = "relion.autopick.ref3d"
AUTOPICK_REF2D_HELICAL_NAME = "relion.autopick.ref2d.helical"
AUTOPICK_REF3D_HELICAL_NAME = "relion.autopick.ref3d.helical"
AUTOPICK_LOG_NAME = "relion.autopick.log"
AUTOPICK_LOG_HELICAL_NAME = "relion.autopick.log.helical"
AUTOPICK_TOPAZ_NAME = "relion.autopick.topaz.pick"
AUTOPICK_TOPAZ_TRAIN_NAME = "relion.autopick.topaz.train"
AUTOPICK_TOPAZ_HELICAL_NAME = "relion.autopick.topaz.pick.helical"
AUTOPICK_TOPAZ_TRAIN_HELICAL_NAME = "relion.autopick.topaz.train.helical"

AUTOPICK_DIR = "AutoPick"
AUTOPICK_RELIONSTYLE_NAME = "relion.autopick"  # will be deprecated
AUTOPICK_TYPE_NUM = 4  # deprecated

EXTRACT_PARTICLE_NAME = "relion.extract"
EXTRACT_HELICAL_NAME = "relion.extract.helical"
EXTRACT_REXTRACT_NAME = "relion.extract.reextract"
EXTRACT_DIR = "Extract"
# no relionstyle name
EXTRACT_TYPE_NUM = 5  # deprecated

SELECT_ONVALUE_NAME = "relion.select.onvalue"
SELECT_AUTO2D_NAME = "relion.select.class2dauto"
SELECT_REMOVEDUP_NAME = "relion.select.removeduplicates"
SELECT_DISCARD_NAME = "relion.select.discard"
SELECT_INTERACTIVE_NAME = "relion.select.interactive"
SELECT_SPLIT_NAME = "relion.select.split"
SELECT_DIR = "Select"
SELECT_RELIONSTYLE_NAME = "relion.select"  # will be deprecated
SELECT_TYPE_NUM = 7  # deprecated

CLASS2D_PARTICLE_NAME_EM = "relion.class2d.em"
CLASS2D_HELICAL_NAME_EM = "relion.class2d.em.helical"
CLASS2D_PARTICLE_NAME_VDAM = "relion.class2d.vdam"
CLASS2D_HELICAL_NAME_VDAM = "relion.class2d.vdam.helical"
CLASS2D_DIR = "Class2D"
CLASS2D_PARTICLE_NAME = "relion.class2d"  # deprecated, VDAM and EM types now split
# no relionstyle name
CLASS2D_TYPE_NUM = 8  # deprecated

INIMODEL_JOB_NAME = "relion.initialmodel"
INIMODEL_DIR = "InitialModel"
# no relionstyle name
INIMODEL_TYPE_NUM = 18  # deprecated


CLASS3D_PARTICLE_NAME = "relion.class3d"
CLASS3D_MULTIREF_PARTICLE_NAME = "relion.class3d.multiref"
CLASS3D_HELICAL_NAME = "relion.class3d.helical"
CLASS3D_MULTIREF_HELICAL_NAME = "relion.class3d.helical.multiref"

CLASS3D_DIR = "Class3D"
# no relionstyle name
CLASS3D_TYPE_NUM = 9  # deprecated

REFINE3D_PARTICLE_NAME = "relion.refine3d"
REFINE3D_HELICAL_NAME = "relion.refine3d.helical"
REFINE3D_DIR = "Refine3D"
# no relionstyle name
REFINE3D_TYPE_NUM = 10  # deprecated

MULTIBODY_REFINE_NAME = "relion.multibody.refine"
MULTIBODY_FLEXANALYSIS_NAME = "relion.multibody.flexanalysis"
MULTIBODY_DIR = "MultiBody"
MULTIBODY_RELIONSTYLE_NAME = "relion.multibody"  # to  be deprecated
MULTIBODY_TYPE_NUM = 19  # deprecated

JOINSTAR_MOVIES_NAME = "relion.joinstar.movies"
JOINSTAR_MICS_NAME = "relion.joinstar.micrographs"
JOINSTAR_PARTS_NAME = "relion.joinstar.particles"
JOINSTAR_DIR = "JoinStar"
JOINSTAR_RELIONSTYLE_NAME = "relion.joinstar"  # will be deprecated
JOINSTAR_TYPE_NUM = 13  # deprecated

SUBTRACT_JOB_NAME = "relion.subtract"
SUBTRACT_REVERT_NAME = "relion.subtract.revert"
SUBTRACT_DIR = "Subtract"
SUBTRACT_RELIONSTYLE_NAME = "relion.subtract"  # will be deprecated
SUBTRACT_TYPE_NUM = 14  # deprecated

LOCALRES_RESMAP_NAME = "relion.localres.resmap"
LOCALRES_OWN_NAME = "relion.localres.own"
LOCALRES_DIR = "LocalRes"
LOCALRES_RELIONSTYLE_NAME = "relion.localres"  # to be deprecated
LOCALRES_TYPE_NUM = 16  # deprecated


CTFREFINE_REFINE_NAME = "relion.ctfrefine"
CTFREFINE_ANISO_NAME = "relion.ctfrefine.anisomag"
CTFREFINE_DIR = "CtfRefine"
# no relionstyle name
CTFREFINE_TYPE_NUM = 21  # deprecated

# external type number changed in this version
RELION_EXTERNAL_NAME = "relion.external"
EXTERNAL_DIR = "External"
# no relionstyle name
EXTERNAL_TYPE_NUM = 22  # deprecated

BAYESPOLISH_TRAIN_NAME = "relion.polish.train"
BAYESPOLISH_POLISH_NAME = "relion.polish"
BAYESPOLISH_DIR = "Polish"
# no relion style name
BAYESPOLISH_TYPE_NUM = 20  # deprecated

# sort is deprecated but left in for back compatibility
SORT_DIR = "Sort"
SORT_TYPE_NUMBER = 6  # deprecated

# job names that need to be converted between RELION and the pipeliner
# either because the relion name is too general or they are ambiguous
# between RELION 4.0 and the pipeliner they need to be checked before being
# used to create a job.
JOBNAMES_NEED_CONVERSION = (
    MANUALPICK_JOB_NAME,
    EXTRACT_PARTICLE_NAME,
    CLASS2D_PARTICLE_NAME,
    CLASS3D_PARTICLE_NAME,
    REFINE3D_PARTICLE_NAME,
    CTFREFINE_REFINE_NAME,
    BAYESPOLISH_POLISH_NAME,
    IMPORT_RELIONSTYLE_NAME,
    MOTIONCORR_RELIONSTYLE_NAME,
    CTFFIND_RELIONSTYLE_NAME,
    AUTOPICK_RELIONSTYLE_NAME,
    SELECT_RELIONSTYLE_NAME,
    JOINSTAR_RELIONSTYLE_NAME,
    SUBTRACT_RELIONSTYLE_NAME,
    LOCALRES_RELIONSTYLE_NAME,
    MULTIBODY_RELIONSTYLE_NAME,
)


RELION_GENERAL_PROCS = (
    IMPORT_DIR,
    MOTIONCORR_DIR,
    CTFFIND_DIR,
    MANUALPICK_DIR,
    AUTOPICK_DIR,
    EXTRACT_DIR,
    SORT_DIR,  # deprecated but left in
    SELECT_DIR,
    CLASS2D_DIR,
    CLASS3D_DIR,
    REFINE3D_DIR,
    MASKCREATE_DIR,
    JOINSTAR_DIR,
    SUBTRACT_DIR,
    POSTPROCESS_DIR,
    LOCALRES_DIR,
    INIMODEL_DIR,
    MULTIBODY_DIR,
    BAYESPOLISH_DIR,
    CTFREFINE_DIR,
    EXTERNAL_DIR,
)

# conversions for back compatibility with 3.1
# takes 3.1 process number and returns general proctype
PROC_NUM_2_GENERAL_NAME = {
    IMPORT_TYPE_NUM: IMPORT_DIR,
    MOTIONCORR_TYPE_NUM: MOTIONCORR_DIR,
    CTFFIND_TYPE_NUM: CTFFIND_DIR,
    MANUALPICK_TYPE_NUM: MANUALPICK_DIR,
    AUTOPICK_TYPE_NUM: AUTOPICK_DIR,
    EXTRACT_TYPE_NUM: EXTRACT_DIR,
    # 6: "Sort",  # depreciated
    SORT_TYPE_NUMBER: SORT_DIR,
    SELECT_TYPE_NUM: SELECT_DIR,
    CLASS2D_TYPE_NUM: CLASS2D_DIR,
    CLASS3D_TYPE_NUM: CLASS3D_DIR,
    REFINE3D_TYPE_NUM: REFINE3D_DIR,
    # 11: "Polish",  # depreciated
    MASKCREATE_TYPE_NUM: MASKCREATE_DIR,
    JOINSTAR_TYPE_NUM: JOINSTAR_DIR,
    SUBTRACT_TYPE_NUM: SUBTRACT_DIR,
    POSTPROCESS_TYPE_NUM: POSTPROCESS_DIR,
    LOCALRES_TYPE_NUM: LOCALRES_DIR,
    # 17: "MovieRefine",  # depreciated
    INIMODEL_TYPE_NUM: INIMODEL_DIR,
    MULTIBODY_TYPE_NUM: MULTIBODY_DIR,
    BAYESPOLISH_TYPE_NUM: BAYESPOLISH_DIR,
    CTFREFINE_TYPE_NUM: CTFREFINE_DIR,
    EXTERNAL_TYPE_NUM: EXTERNAL_DIR,
}

# nodes variables

NODES_DIR = ".Nodes/"

# General Nodes types used by Relion 4
NODE_MOVIES_LABEL = "relion.MovieStar"
NODE_MICS_LABEL = "relion.MicrographStar"
NODE_MIC_COORDS_LABEL = "relion.CoordinateStar"
NODE_PART_DATA_LABEL = "relion.ParticleStar"
NODE_REFS_LABEL = "relion.ReferenceStar"
NODE_3DREF_LABEL = "relion.DensityMap"
NODE_MASK_LABEL = "relion.Mask"
NODE_OPTIMISER_LABEL = "relion.OptimiserStar"
NODE_HALFMAP_LABEL = "relion.HalfMap"
NODE_RESMAP_LABEL = "relion.LocalResolutionMap"
NODE_PDF_LOGFILE_LABEL = "relion.PdfLogfile"
NODE_POST_LABEL = "relion.PostprocessStar"
NODE_POLISH_PARAMS_LABEL = "relion.PolishParams"
NODE_OTHER_LABEL = "Other"

# convert old relion3.x style node types to new RELION style labels
NODE_TYPE2LABEL = {
    0: NODE_MOVIES_LABEL,
    1: NODE_MICS_LABEL,
    2: NODE_MIC_COORDS_LABEL,
    3: NODE_PART_DATA_LABEL,
    # 4: "Movie data", deprecated not used anymore
    5: NODE_REFS_LABEL,
    6: NODE_3DREF_LABEL,
    7: NODE_MASK_LABEL,
    8: NODE_OPTIMISER_LABEL,  # deprecated, but left in
    9: NODE_OPTIMISER_LABEL,
    10: NODE_HALFMAP_LABEL,
    11: NODE_3DREF_LABEL,  # deprecated, but left in
    12: NODE_RESMAP_LABEL,
    13: NODE_PDF_LOGFILE_LABEL,
    14: NODE_POST_LABEL,
    15: NODE_POLISH_PARAMS_LABEL,
    99: NODE_OTHER_LABEL,
}

# convert old relion3.x style node types to new style labels
NODE_INT2NODELABEL = {
    0: NODE_MICROGRAPHMOVIEGROUPMETADATA,
    1: NODE_MICROGRAPHGROUPMETADATA,
    2: NODE_MICROGRAPHCOORDSGROUP,
    3: NODE_PARTICLESDATA,
    # 4: "Movie data", deprecated not used anymore
    5: NODE_IMAGE2DGROUPMETADATA,
    6: NODE_DENSITYMAP,
    7: NODE_MASK3D,
    8: NODE_PROCESSDATA,  # deprecated, but left in
    9: NODE_PROCESSDATA,
    10: NODE_DENSITYMAP,
    11: NODE_DENSITYMAP,  # deprecated, but left in
    12: NODE_IMAGE3D,
    13: NODE_LOGFILE,
    14: NODE_PROCESSDATA,
    15: NODE_PROCESSDATA,
}


# status variables

# relion 4/ pipeliner style job status labels
JOBSTATUS_RUN = "Running"
JOBSTATUS_SCHED = "Scheduled"
JOBSTATUS_SUCCESS = "Succeeded"
JOBSTATUS_FAIL = "Failed"
JOBSTATUS_ABORT = "Aborted"

# convert old style relion 2.x/3.x status to relion 4/pipeliner format
STATUS2LABEL = {
    0: JOBSTATUS_RUN,
    1: JOBSTATUS_SCHED,
    2: JOBSTATUS_SUCCESS,
    3: JOBSTATUS_FAIL,
    4: JOBSTATUS_ABORT,
}

# control files
RELION_SUCCESS_FILE = "RELION_JOB_EXIT_SUCCESS"
RELION_FAIL_FILE = "RELION_JOB_EXIT_FAILURE"
RELION_ABORT_FILE = "RELION_JOB_EXIT_ABORTED"
ABORT_TRIGGER = "RELION_JOB_ABORT_NOW"

SUCCESS_FILE = "PIPELINER_JOB_EXIT_SUCCESS"
FAIL_FILE = "PIPELINER_JOB_EXIT_FAILURE"
ABORT_FILE = "PIPELINER_JOB_EXIT_ABORTED"

# the file the pipeliner uses to track project names
PROJECT_FILE = ".CCPEM_pipeliner_project"
JOBINFO_FILE = ".CCPEM_pipeliner_jobinfo"
CLEANUP_LOG = ".CCPEM_pipeliner_cleanup"

# control file to disable tasks that may require reading large starfiles
STARFILE_READS_OFF = ".PIPELINER_SKIP_STARFILE_READS"
