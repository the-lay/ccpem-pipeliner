from pkg_resources import get_distribution

from . import env_setup

__version__ = get_distribution("pipeliner").version

# Set up python environment for project
try:
    env_setup.setup_python_environment()
except Exception:
    print("WARNING: unable to set up pipeliner python environment")
