#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

from typing import List, Optional

from pipeliner.utils import check_for_illegal_symbols
from pipeliner.nodes import (
    Node,
    NODE_ATOMCOORDS,
    AtomCoordsNode,
    NODE_ATOMCOORDSGROUPMETADATA,
    AtomCoordsGroupMetadataNode,
    NODE_DENSITYMAP,
    DensityMapNode,
    NODE_DENSITYMAPMETADATA,
    DensityMapMetadataNode,
    NODE_DENSITYMAPGROUPMETADATA,
    DensityMapGroupMetadataNode,
    NODE_IMAGE2D,
    Image2DNode,
    NODE_IMAGE2DSTACK,
    Image2DStackNode,
    NODE_IMAGE2DMETADATA,
    Image2DMetadataNode,
    NODE_IMAGE2DGROUPMETADATA,
    Image2DGroupMetadataNode,
    NODE_IMAGE3D,
    Image3DNode,
    NODE_IMAGE3DMETADATA,
    Image3DMetadataNode,
    NODE_IMAGE3DGROUPMETADATA,
    Image3DGroupMetadataNode,
    NODE_LIGANDDESCRIPTION,
    LigandDescriptionNode,
    NODE_LOGFILE,
    LogFileNode,
    NODE_MASK2D,
    Mask2DNode,
    NODE_MASK3D,
    Mask3DNode,
    NODE_MICROGRAPHCOORDS,
    MicrographCoordsNode,
    NODE_MICROGRAPHCOORDSGROUP,
    MicrographCoordsGroupNode,
    NODE_MICROGRAPH,
    MicrographNode,
    NODE_MICROGRAPHMETADATA,
    MicrographMetadataNode,
    NODE_MICROGRAPHGROUPMETADATA,
    MicrographGroupMetadataNode,
    NODE_MICROGRAPHMOVIE,
    MicrographMovieNode,
    NODE_MICROGRAPHMOVIEMETADATA,
    MicrographMovieMetadataNode,
    NODE_MICROGRAPHMOVIEGROUPMETADATA,
    MicrographMovieGroupMetadataNode,
    NODE_MICROSCOPEDATA,
    MicroscopeDataNode,
    NODE_MLMODEL,
    MlModelNode,
    NODE_PARTICLESDATA,
    ParticlesDataNode,
    NODE_PROCESSDATA,
    ProcessDataNode,
    NODE_RESTRAINTS,
    RestraintsNode,
    NODE_RIGIDBODIES,
    RigidBodiesNode,
    NODE_SEQUENCE,
    SequenceNode,
    NODE_SEQUENCEALIGNMENT,
    SequenceAlignmentNode,
    NODE_STRUCTUREFACTORS,
    StructureFactorsNode,
    NODE_TILTSERIES,
    TiltSeriesNode,
    NODE_TILTSERIESMETADATA,
    TiltSeriesMetadataNode,
    NODE_TILTSERIESGROUPMETADATA,
    TiltSeriesGroupMetadataNode,
    NODE_TILTSERIESMOVIE,
    TiltSeriesMovieNode,
    NODE_TILTSERIESMOVIEMETADATA,
    TiltSeriesMovieMetadataNode,
    NODE_TILTSERIESMOVIEGROUPMETADATA,
    TiltSeriesMovieGroupMetadataNode,
    NODE_TOMOGRAM,
    TomogramNode,
    NODE_TOMOGRAMMETADATA,
    TomogramMetadataNode,
    NODE_TOMOGRAMGROUPMETADATA,
    TomogramGroupMetadataNode,
    NODE_NEWNODETYPE,
)

NODE_CLASSES = {
    NODE_ATOMCOORDS: AtomCoordsNode,
    NODE_ATOMCOORDSGROUPMETADATA: AtomCoordsGroupMetadataNode,
    NODE_DENSITYMAP: DensityMapNode,
    NODE_DENSITYMAPMETADATA: DensityMapMetadataNode,
    NODE_DENSITYMAPGROUPMETADATA: DensityMapGroupMetadataNode,
    NODE_IMAGE2D: Image2DNode,
    NODE_IMAGE2DSTACK: Image2DStackNode,
    NODE_IMAGE2DMETADATA: Image2DMetadataNode,
    NODE_IMAGE2DGROUPMETADATA: Image2DGroupMetadataNode,
    NODE_IMAGE3D: Image3DNode,
    NODE_IMAGE3DMETADATA: Image3DMetadataNode,
    NODE_IMAGE3DGROUPMETADATA: Image3DGroupMetadataNode,
    NODE_LIGANDDESCRIPTION: LigandDescriptionNode,
    NODE_LOGFILE: LogFileNode,
    NODE_MASK2D: Mask2DNode,
    NODE_MASK3D: Mask3DNode,
    NODE_MICROGRAPHCOORDS: MicrographCoordsNode,
    NODE_MICROGRAPHCOORDSGROUP: MicrographCoordsGroupNode,
    NODE_MICROGRAPH: MicrographNode,
    NODE_MICROGRAPHMETADATA: MicrographMetadataNode,
    NODE_MICROGRAPHGROUPMETADATA: MicrographGroupMetadataNode,
    NODE_MICROGRAPHMOVIE: MicrographMovieNode,
    NODE_MICROGRAPHMOVIEMETADATA: MicrographMovieMetadataNode,
    NODE_MICROGRAPHMOVIEGROUPMETADATA: MicrographMovieGroupMetadataNode,
    NODE_MICROSCOPEDATA: MicroscopeDataNode,
    NODE_MLMODEL: MlModelNode,
    NODE_PARTICLESDATA: ParticlesDataNode,
    NODE_PROCESSDATA: ProcessDataNode,
    NODE_RESTRAINTS: RestraintsNode,
    NODE_RIGIDBODIES: RigidBodiesNode,
    NODE_SEQUENCE: SequenceNode,
    NODE_SEQUENCEALIGNMENT: SequenceAlignmentNode,
    NODE_STRUCTUREFACTORS: StructureFactorsNode,
    NODE_TILTSERIES: TiltSeriesNode,
    NODE_TILTSERIESMETADATA: TiltSeriesMetadataNode,
    NODE_TILTSERIESGROUPMETADATA: TiltSeriesGroupMetadataNode,
    NODE_TILTSERIESMOVIE: TiltSeriesMovieNode,
    NODE_TILTSERIESMOVIEMETADATA: TiltSeriesMovieMetadataNode,
    NODE_TILTSERIESMOVIEGROUPMETADATA: TiltSeriesMovieGroupMetadataNode,
    NODE_TOMOGRAM: TomogramNode,
    NODE_TOMOGRAMMETADATA: TomogramMetadataNode,
    NODE_TOMOGRAMGROUPMETADATA: TomogramGroupMetadataNode,
}


def all_node_toplevel_types(add_new=False, new_pos=-1) -> List[str]:
    """Get a list of all the node types

    Args:
        add_new (bool): If True 'NEW NODE TYPE (not recommended)' is added to the
            list of node types
        new_pos (int): position to insert the New node tyoe in the nodes list
    """
    nodes = list(NODE_CLASSES)
    nodes.sort()
    if add_new:
        if new_pos >= 0:
            nodes.insert(new_pos, NODE_NEWNODETYPE)
        else:
            nodes.append(NODE_NEWNODETYPE)
    return nodes


def create_node(
    filename: str,
    nodetype: str,
    kwds: Optional[List[str]] = None,
    do_validation: bool = True,
    override_format: str = "",
) -> Node:
    check_for_illegal_symbols(nodetype, "node type")
    if nodetype not in NODE_CLASSES:
        return Node(
            name=filename,
            toplevel_type=nodetype,
            kwds=kwds,
            do_validation=do_validation,
            override_format=override_format,
        )

    the_node = NODE_CLASSES[nodetype](
        name=filename,
        kwds=kwds,
        do_validation=do_validation,
        override_format=override_format,
    )
    if do_validation:
        the_node.keyword_validation()
    return the_node
