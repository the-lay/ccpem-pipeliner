#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os

from pipeliner.jobs.relion.relion_job import RelionJob
from pipeliner.jobs.relion.relion_job import relion_program

from pipeliner.job_options import (
    InputNodeJobOption,
    FloatJobOption,
    FileNameJobOption,
    IntJobOption,
    files_exts,
)
from pipeliner.display_tools import mini_montage_from_starfile
from pipeliner.starfile_handler import DataStarFile
from pipeliner.nodes import (
    NODE_DENSITYMAP,
    NODE_PROCESSDATA,
    NODE_IMAGE2DSTACK,
)
from pipeliner.node_factory import create_node


class ReprojectJob(RelionJob):
    PROCESS_NAME = "relion.reproject"
    OUT_DIR = "Reproject"

    def __init__(self):
        super().__init__()
        self.jobinfo.display_name = "RELION Reproject"

        self.jobinfo.short_desc = "Generate 2D projections from a 3D map"
        self.jobinfo.long_desc = (
            "Just a reminder: DO NOT use high resolution projections as autopicking"
            " references"
        )
        self.jobinfo.programs = [relion_program("relion_project")]

        self.joboptions["map_filename"] = InputNodeJobOption(
            label="Input map mrc file:",
            node_type=NODE_DENSITYMAP,
            default_value="",
            directory="",
            pattern=files_exts("mrc map files", [".mrc"]),
            help_text="The map that will be used for re-projections",
            is_required=True,
        )

        self.joboptions["angles_filename"] = FileNameJobOption(
            label="Angles file:",
            node_type=NODE_PROCESSDATA,
            node_kwds=["relion", "eulerangles"],
            default_value="",
            directory="",
            pattern=files_exts("angles star file", [".star"]),
            help_text=(
                "This should be a star file with a block called data_angles containing"
                " the following columns: rlnAngleRot, _rlnAngleTilt, and _rlnAnglePsi"
            ),
            deactivate_if=[("nr_uniform", ">", 0)],
        )

        self.joboptions["nr_uniform"] = IntJobOption(
            label="OR: Do this many uniform projections:",
            default_value=-1,
            suggested_min=1,
            suggested_max=180,
            step_value=1,
            help_text="Alternately do this many uniform projections",
            in_continue=True,
            deactivate_if=[("angles_filename", "!=", "")],
        )

        self.joboptions["apix"] = FloatJobOption(
            label="Reference pixel size (A)",
            default_value=1.07,
            suggested_min=0.1,
            suggested_max=10,
            step_value=0.01,
            help_text="The pixel size of the reference in Angstroms",
        )

        self.get_runtab_options(mpi=False, threads=False, addtl_args=True)

    def get_commands(self):

        command = ["relion_project"]

        map_file = self.joboptions["map_filename"].get_string()
        command += ["--i", map_file]

        nr_uniform = self.joboptions["nr_uniform"].get_number()
        angles_file = self.joboptions["angles_filename"].get_string()
        if angles_file != "":
            command += ["--ang", angles_file]
        elif nr_uniform > 0:
            command += ["--nr_uniform", str(nr_uniform)]
        else:
            raise ValueError(
                "ERROR: An angles file or a number of uniform projections must be"
                " specified"
            )

        apix = self.joboptions["apix"].get_string()
        command += ["--angpix", apix]

        command += ["--o", "{}reproj".format(self.output_dir)]

        other_args = self.joboptions["other_args"].get_string()
        if other_args != "":
            command += self.parse_additional_args()

        self.output_nodes.append(
            create_node(
                os.path.join(self.output_dir, "reproj.mrcs"),
                NODE_IMAGE2DSTACK,
                ["relion", "projections"],
            )
        )

        self.output_nodes.append(
            create_node(
                os.path.join(self.output_dir, "reproj.star"),
                NODE_PROCESSDATA,
                ["relion", "eulerangles"],
            )
        )

        commands = [command]
        return commands

    def create_results_display(self) -> list:
        rp_file = DataStarFile(os.path.join(self.output_dir, "reproj.star"))
        phi = rp_file.column_as_list("particles", "_rlnAngleRot")
        theta = rp_file.column_as_list("particles", "_rlnAngleTilt")
        psi = rp_file.column_as_list("particles", "_rlnAnglePsi")
        rp_labels = [
            f"phi/rot: {x[0]} theta/tilt: {x[1]}, psi: {x[2]}"
            for x in zip(phi, theta, psi)
        ]
        return [
            mini_montage_from_starfile(
                starfile=os.path.join(self.output_dir, "reproj.star"),
                block="particles",
                column="_rlnImageName",
                outputdir=self.output_dir,
                title="Reprojections",
                nimg=-1,
                labels=rp_labels,
            )
        ]

    def gather_metadata(self):
        rp_file = DataStarFile(os.path.join(self.output_dir, "reproj.star"))
        n_rp = rp_file.count_block("particles")
        return {"nProj": n_rp}
