#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os

from .relion_job import RelionJob
from pipeliner.jobs.relion.relion_job import relion_program

from pipeliner.job_options import (
    InputNodeJobOption,
    files_exts,
    EXT_MRC_MAP,
    BooleanJobOption,
    FloatJobOption,
    IntJobOption,
)
from pipeliner.data_structure import (
    MASKCREATE_DIR,
    MASKCREATE_JOB_NAME,
)
from pipeliner.nodes import (
    NODE_DENSITYMAP,
    NODE_MASK3D,
)
from pipeliner.node_factory import create_node
from pipeliner.display_tools import make_map_model_thumb_and_display
from pipeliner.deposition_tools.pdb_deposition_objects import (
    background_mask_type_entry,
    software_type_entry,
    DEPOSITION_COMMENT,
)


class RelionMaskCreate(RelionJob):
    PROCESS_NAME = MASKCREATE_JOB_NAME
    OUT_DIR = MASKCREATE_DIR

    def __init__(self):
        super().__init__()
        self.jobinfo.display_name = "RELION mask create"

        self.jobinfo.short_desc = "Create a 3-dimensional mask"
        self.jobinfo.long_desc = "Masks contain values from 0 to 1"

        self.jobinfo.programs = [relion_program("relion_mask_create")]

        self.joboptions["fn_in"] = InputNodeJobOption(
            label="Input 3D map",
            node_type=NODE_DENSITYMAP,
            default_value="",
            directory="",
            pattern=files_exts("MRC map", EXT_MRC_MAP),
            help_text=(
                "Provide an input MRC map from which to start binarising the map."
            ),
            is_required=True,
        )

        self.joboptions["lowpass_filter"] = FloatJobOption(
            label="Lowpass filter map (A)",
            default_value=10,
            suggested_min=10,
            suggested_max=100,
            step_value=5,
            help_text=(
                "Lowpass filter that will be applied to the input map, prior to"
                " binarisation. To calculate solvent masks, a lowpass filter of 15-20A"
                " may work well."
            ),
            in_continue=True,
            is_required=True,
        )

        self.joboptions["angpix"] = FloatJobOption(
            label="Pixel size (A)",
            default_value=-1,
            suggested_min=0.3,
            suggested_max=5,
            step_value=0.1,
            help_text=(
                "Provide the pixel size of the input map in Angstroms to calculate"
                " the low-pass filter. This value is also used in the output image"
                " header."
            ),
            in_continue=True,
            is_required=True,
        )

        self.joboptions["inimask_threshold"] = FloatJobOption(
            label="Initial binarisation threshold",
            default_value=0.02,
            suggested_min=0,
            suggested_max=0.5,
            step_value=0.01,
            help_text=(
                "This threshold is used to make an initial binary mask from the average"
                " of the two unfiltered half-reconstructions. If you don't know what"
                " value to use, display one of the unfiltered half-maps in a 3D surface"
                " rendering viewer and find the lowest threshold that gives no noise"
                " peaks outside the reconstruction."
            ),
            in_continue=True,
            is_required=True,
        )

        self.joboptions["extend_inimask"] = IntJobOption(
            label="Extend binary map this many pixels:",
            default_value=3,
            suggested_min=0,
            suggested_max=20,
            step_value=1,
            help_text=(
                "The initial binary mask is extended this number of pixels in all"
                " directions."
            ),
            in_continue=True,
            is_required=True,
        )

        self.joboptions["width_mask_edge"] = IntJobOption(
            label="Add a soft-edge of this many pixels:",
            default_value=3,
            suggested_min=0,
            suggested_max=20,
            step_value=1,
            help_text=(
                "The extended binary mask is further extended with a raised-cosine"
                " soft edge of the specified width."
            ),
            in_continue=True,
            is_required=True,
        )

        self.joboptions["do_helix"] = BooleanJobOption(
            label="Mask a 3D helix?",
            default_value=False,
            help_text=(
                "Generate a mask for 3D helix which spans across Z axis of the box."
            ),
            in_continue=True,
            jobop_group="Helical processing options",
        )

        self.joboptions["helical_z_percentage"] = FloatJobOption(
            label="Central Z length (%):",
            default_value=30.0,
            suggested_min=5.0,
            suggested_max=80.0,
            step_value=1.0,
            help_text=(
                "Reconstructed helix suffers from inaccuracies of orientation searches."
                " The central part of the box contains more reliable information"
                " compared to the top and bottom parts along Z axis. Set this value (%)"
                " to the central part length along Z axis divided by the box size."
                " Values around 30% are commonly used but you may want to try different"
                " lengths."
            ),
            in_continue=True,
            deactivate_if=[("do_helix", "=", False)],
            jobop_group="Helical processing options",
        )

        self.get_runtab_options(mpi=False, threads=True, addtl_args=True)

    def get_commands(self):

        fn_out = self.output_dir + "mask.mrc"

        self.command = ["relion_mask_create"]

        map_in = self.joboptions["fn_in"].get_string()
        if len(map_in) == 0:
            raise ValueError("Empty field for input map")

        self.command += ["--i", map_in, "--o", fn_out]

        lowpass_filter = self.joboptions["lowpass_filter"].get_string()
        self.command += ["--lowpass", lowpass_filter]

        angpix = self.joboptions["angpix"].get_string()
        self.command += ["--angpix", angpix]

        inimask_threshold = self.joboptions["inimask_threshold"].get_string()
        self.command += ["--ini_threshold", inimask_threshold]

        extend_inimask = self.joboptions["extend_inimask"].get_string()
        self.command += ["--extend_inimask", extend_inimask]

        width_mask_edge = self.joboptions["width_mask_edge"].get_string()
        self.command += ["--width_soft_edge", width_mask_edge]

        do_helix = self.joboptions["do_helix"].get_boolean()
        if do_helix:
            helical_z_percentage = (
                self.joboptions["helical_z_percentage"].get_number() / 100
            )
            self.command += ["--helix", "--z_percentage", str(helical_z_percentage)]

        other_args = self.joboptions["other_args"].get_string()
        if len(other_args) != 0:
            self.command += self.parse_additional_args()

        nr_threads = self.joboptions["nr_threads"].get_string()
        self.command += ["--j", nr_threads]

        self.output_nodes.append(create_node(fn_out, NODE_MASK3D, ["relion"]))

        commands = [self.command]
        return commands

    def create_results_display(self):
        mask = os.path.join(self.output_dir, "mask.mrc")
        thresh = self.joboptions["inimask_threshold"].get_number()
        extend = self.joboptions["extend_inimask"].get_number()
        soft = self.joboptions["width_mask_edge"].get_number()
        input_map = self.joboptions["fn_in"].get_string()
        return [
            make_map_model_thumb_and_display(
                maps=[mask, input_map],
                maps_opacity=[0.5, 1.0],
                title="Created mask overlaid on input map",
                outputdir=self.output_dir,
                maps_data=(
                    f"Threshold: {thresh}; Extend: {extend} px; Soft edge: {soft} px"
                ),
                start_collapsed=False,
            )
        ]

    def prepare_onedep_data(self) -> list:
        relionmaskcreate = software_type_entry(
            name=self.jobinfo.programs[0].command,
            version=self.jobinfo.programs[0].get_version(),
        )

        return [
            background_mask_type_entry(
                geometrical_shape="OTHER",
                software_list=(relionmaskcreate,),
                details=DEPOSITION_COMMENT,
            )
        ]
