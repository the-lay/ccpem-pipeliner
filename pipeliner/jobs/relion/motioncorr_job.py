#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from glob import glob

from pipeliner.jobs.relion.relion_job import relion_program
from pipeliner import user_settings
from pipeliner.jobs.relion.relion_job import RelionJob

from pipeliner.data_structure import (
    MOTIONCORR_OWN_NAME,
    MOTIONCORR_MOTIONCOR2_NAME,
    MOTIONCORR_DIR,
)
from pipeliner.nodes import (
    NODE_MICROGRAPHMOVIEGROUPMETADATA,
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_LOGFILE,
    NODE_MICROSCOPEDATA,
)
from pipeliner.node_factory import create_node
from pipeliner.job_options import (
    FileNameJobOption,
    InputNodeJobOption,
    StringJobOption,
    PathJobOption,
    GAIN_ROTATION,
    GAIN_FLIP,
    files_exts,
    EXT_STARFILE,
    BooleanJobOption,
    MultipleChoiceJobOption,
    FloatJobOption,
    IntJobOption,
)
from pipeliner.starfile_handler import DataStarFile
from pipeliner.pipeliner_job import Ref
from pipeliner.deposition_tools.empiar_deposition_objects import (
    prepare_empiar_mics_parts,
)
from pipeliner.display_tools import mini_montage_from_starfile


class RelionMotionCorrJob(RelionJob):
    OUT_DIR = MOTIONCORR_DIR

    def __init__(self):
        super().__init__()
        self.always_continue_in_schedule = True

        self.jobinfo.programs = [relion_program("relion_run_motioncorr")]

        self.jobinfo.references.append(
            Ref(
                authors=[
                    "Zheng SQ",
                    "Palovcak E",
                    "Armache J",
                    "Verba KA",
                    "Cheng Y",
                    "Agard DA",
                ],
                title=(
                    "MotionCor2: anisotropic correction of beam-induced motion for"
                    " improved cryo-electron microscopy"
                ),
                journal="Nature Methods",
                year="2017",
                volume="14",
                issue="4",
                pages="331-332",
                doi="10.1038/nmeth.4193",
            )
        )

        self.joboptions["input_star_mics"] = InputNodeJobOption(
            label="Input movies STAR file:",
            node_type=NODE_MICROGRAPHMOVIEGROUPMETADATA,
            node_kwds=["relion"],
            default_value="",
            directory="",
            pattern=files_exts("Micrographs STAR file", EXT_STARFILE),
            help_text="A STAR file with all micrographs to run MOTIONCORR on",
            is_required=True,
        )

        self.joboptions["first_frame_sum"] = IntJobOption(
            label="First frame for corrected sum:",
            default_value=1,
            suggested_min=1,
            suggested_max=32,
            step_value=1,
            help_text="First frame to use in corrected average (starts counting at 1).",
            is_required=True,
        )

        self.joboptions["last_frame_sum"] = IntJobOption(
            label="Last frame for corrected sum:",
            default_value=-1,
            suggested_min=0,
            suggested_max=32,
            step_value=1,
            help_text=(
                "Last frame to use in corrected average. Values equal to or smaller"
                " than 0 mean 'use all frames'."
            ),
            is_required=True,
        )

        self.joboptions["eer_grouping"] = IntJobOption(
            label="EER fractionation:",
            default_value=32,
            suggested_min=1,
            suggested_max=100,
            step_value=1,
            help_text=(
                "The number of hardware frames to group into one fraction. This option"
                " is relevant only for Falcon4 movies in the EER format. Note that all"
                " 'frames' in the GUI (e.g. first and last frame for corrected sum,"
                " dose per frame) refer to fractions, not raw detector frames. See"
                " https://www3.mrc-lmb.cam.ac.uk/relion/index.php/Image_compression"
                "#Falcon4_EER for detailed guidance on EER processing."
            ),
            is_required=True,
        )

        self.joboptions["bfactor"] = FloatJobOption(
            label="Bfactor:",
            default_value=150,
            suggested_min=0,
            suggested_max=1500,
            step_value=50,
            help_text="The B-factor that will be applied to the micrographs.",
            is_required=True,
        )

        self.joboptions["patch_x"] = IntJobOption(
            label="Number of patches X:",
            default_value=1,
            suggested_min=1,
            suggested_max=100,
            step_value=1,
            help_text="Number of patches (in X and Y direction) to apply motioncor2.",
            is_required=True,
        )

        self.joboptions["patch_y"] = IntJobOption(
            label="Number of patches Y:",
            default_value=1,
            suggested_min=1,
            suggested_max=100,
            step_value=1,
            help_text="Number of patches (in X and Y direction) to apply motioncor2.",
            is_required=True,
        )

        self.joboptions["group_frames"] = IntJobOption(
            label="Group frames:",
            default_value=1,
            suggested_min=1,
            suggested_max=5,
            step_value=1,
            help_text=(
                "Average together this many frames before calculating the beam-induced"
                " shifts."
            ),
            is_required=True,
        )

        self.joboptions["bin_factor"] = FloatJobOption(
            label="Binning factor:",
            default_value=1,
            suggested_min=1,
            suggested_max=2,
            step_value=1,
            help_text=(
                "Bin the micrographs this much by a windowing operation in the Fourier"
                " Transform. Binning at this level is hard to un-do later on, but may"
                " be useful to down-scale super-resolution images. Float-values may be"
                " used. Do make sure though that the resulting micrograph size is even."
            ),
            is_required=True,
        )

        self.joboptions["fn_gain_ref"] = FileNameJobOption(
            label="Gain-reference image:",
            default_value="",
            node_type=NODE_MICROSCOPEDATA,
            node_kwds=["gainreference"],
            pattern=files_exts("Gain reference", [".mrc"]),
            directory=".",
            help_text=(
                "Location of the gain-reference file to be applied to the input"
                " micrographs. Leave this empty if the movies are already"
                " gain-corrected."
            ),
        )

        self.joboptions["gain_rot"] = MultipleChoiceJobOption(
            label="Gain rotation:",
            choices=GAIN_ROTATION,
            default_value_index=0,
            help_text=(
                "Rotate the gain reference by this number times 90 degrees clockwise in"
                " relion_display. This is the same as -RotGain in MotionCor2. Note that"
                " MotionCor2 uses a different convention for rotation so it says"
                " 'counter-clockwise'. Valid values are 0, 1, 2 and 3."
            ),
        )

        self.joboptions["gain_flip"] = MultipleChoiceJobOption(
            label="Gain flip:",
            choices=GAIN_FLIP,
            default_value_index=0,
            help_text=(
                "Flip the gain reference after rotation. This is the same as -FlipGain"
                " in MotionCor2. 0 means do nothing, 1 means flip Y (upside down) and 2"
                " means flip X (left to right)."
            ),
        )

        self.joboptions["fn_defect"] = FileNameJobOption(
            label="Defect file:",
            default_value="",
            node_type=NODE_MICROSCOPEDATA,
            node_kwds=["defectfile"],
            pattern=files_exts("Defect file", [".mrc", ".tiff"]),
            directory=".",
            help_text=(
                "Location of a UCSF MotionCor2-style defect text file or a defect map"
                " that describe the defect pixels on the detector. Each line of a"
                " defect text file should contain four numbers specifying x, y, width"
                " and height of a defect region. A defect map is an image (MRC or"
                " TIFF), where 0 means good and 1 means bad pixels. The coordinate"
                " system is the same as the input movie before application of binning,"
                " rotation and/or flipping.\nNote that the format of the defect text is"
                " DIFFERENT from the defect text produced by SerialEM! One can convert"
                " a SerialEM-style defect file into a defect map using IMOD utilities"
                " e.g. 'clip defect -D defect.txt -f tif movie.mrc defect_map.tif'. See"
                " explanations in the SerialEM manual.\n\nLeave empty if you don't have"
                " any defects, or don't want to correct for defects on your detector."
            ),
        )

        # Dose-weighting

        self.joboptions["do_dose_weighting"] = BooleanJobOption(
            label="Do dose-weighting?",
            default_value=True,
            help_text="If set to Yes, the averaged micrographs will be dose-weighted.",
        )

        self.joboptions["do_save_noDW"] = BooleanJobOption(
            label="Save non-dose weighted as well?",
            default_value=False,
            help_text=(
                "Aligned but non-dose weighted images are sometimes useful in CTF"
                " estimation, although there is no difference in most cases. Whichever"
                " the choice, CTF refinement job is always done on dose-weighted"
                " particles."
            ),
            deactivate_if=[("do_dose_weighting", "=", False)],
        )

        self.joboptions["dose_per_frame"] = FloatJobOption(
            label="Dose per frame (e/A2):",
            default_value=1,
            suggested_min=0,
            suggested_max=5,
            step_value=0.2,
            help_text="Dose per movie frame (in electrons per squared Angstrom).",
            is_required=True,
        )

        self.joboptions["pre_exposure"] = FloatJobOption(
            label="Pre-exposure (e/A2):",
            default_value=0,
            suggested_min=0,
            suggested_max=5,
            step_value=0.5,
            help_text="Pre-exposure dose (in electrons per squared Angstrom).",
            is_required=True,
        )

        self.joboptions["group_frames"] = IntJobOption(
            label="Group frames:",
            default_value=1,
            suggested_min=1,
            suggested_max=5,
            step_value=1,
            help_text=(
                "Average together this many frames before calculating the beam-induced"
                " shifts."
            ),
            is_required=True,
        )

        self.get_runtab_options(mpi=True, threads=True, addtl_args=True)

    def relion_back_compatibility_joboptions(self, do_own=False):
        # add joboptions that Relion needs for display, but aren't used by pipeliner
        # this is done here so these JobOptions are written to the run.job and job.star
        # files for Relion, but are not used during job creation
        self.joboptions["do_own_motioncor"] = BooleanJobOption(
            label="Use RELION's own implementation?",
            default_value=do_own,
            jobop_group="Relion compatibility options",
        )

    def common_commands(self):
        nr_mpi = int(self.joboptions["nr_mpi"].get_number())
        if nr_mpi > 1:
            self.command = self.get_mpi_command() + ["relion_run_motioncorr_mpi"]
        else:
            self.command = ["relion_run_motioncorr"]

        input_star_mics = self.joboptions["input_star_mics"].get_string(
            True, "Empty field for input STAR file"
        )

        self.command += ["--i", input_star_mics]

        fn_corrected_mics = self.output_dir + "corrected_micrographs.star"
        self.output_nodes.append(
            create_node(
                fn_corrected_mics,
                NODE_MICROGRAPHGROUPMETADATA,
                ["relion", "motioncorr"],
            )
        )

        fn_logfile = self.output_dir + "logfile.pdf"
        self.output_nodes.append(
            create_node(fn_logfile, NODE_LOGFILE, ["relion", "motioncorr"])
        )

        self.command += [
            "--o",
            self.output_dir,
            "--first_frame_sum",
            self.joboptions["first_frame_sum"].get_string(),
            "--last_frame_sum",
            self.joboptions["last_frame_sum"].get_string(),
        ]

    def common_commands2(self):
        fn_defect = self.joboptions["fn_defect"].get_string()
        if len(fn_defect) > 0:
            self.command += ["--defect_file", fn_defect]

        self.command += [
            "--bin_factor",
            self.joboptions["bin_factor"].get_string(),
            "--bfactor",
            self.joboptions["bfactor"].get_string(),
            "--dose_per_frame",
            self.joboptions["dose_per_frame"].get_string(),
            "--preexposure",
            self.joboptions["pre_exposure"].get_string(),
            "--patch_x",
            self.joboptions["patch_x"].get_string(),
            "--patch_y",
            self.joboptions["patch_y"].get_string(),
            "--eer_grouping",
            self.joboptions["eer_grouping"].get_string(),
        ]

        group_frames = self.joboptions["group_frames"].get_string()
        if len(group_frames) > 0 and int(group_frames) > 1:
            self.command += [
                "--group_frames",
                self.joboptions["group_frames"].get_string(),
            ]

        fn_gain_ref = self.joboptions["fn_gain_ref"].get_string()
        if len(fn_gain_ref) > 0:
            # make sure this is the same error check that relion is doing
            gain_rot_opts = GAIN_ROTATION
            gain_flip_opts = GAIN_FLIP
            gain_rot = self.joboptions["gain_rot"].get_string()
            gain_flip = self.joboptions["gain_flip"].get_string()

            if gain_rot not in gain_rot_opts or gain_flip not in gain_flip_opts:
                raise ValueError("Illegal gain_rot and/or gain_flip.")
            gain_rot = str(gain_rot_opts.index(gain_rot))
            gain_flip = str(gain_flip_opts.index(gain_flip))
            self.command += [
                "--gainref",
                fn_gain_ref,
                "--gain_rot",
                gain_rot,
                "--gain_flip",
                gain_flip,
            ]

        do_dose_weighting = self.joboptions["do_dose_weighting"].get_boolean()
        if do_dose_weighting:
            self.command.append("--dose_weighting")
            save_nodw = self.joboptions["do_save_noDW"].get_boolean()
            if save_nodw:
                self.command.append("--save_noDW")

    def additional_args(self):
        if self.is_continue:
            self.command.append("--only_do_unfinished")

        other_args = self.joboptions["other_args"].get_string()
        if len(other_args) > 0:
            self.command += self.parse_additional_args()

    def prepare_clean_up_lists(self, do_harsh=False):
        """Return list of intermediate files/dirs to remove"""

        # get all of the subdirs
        subdirs = [x[0] for x in os.walk(self.output_dir, topdown=False)]
        subdirs.remove(self.output_dir)
        del_files, del_dirs = [], []

        for sd in subdirs:
            if do_harsh:
                # remove entire movies directory
                del_dirs.append(sd)
            else:
                exts = ["/*.com", "/*.err", "/*.out", "/*.log"]
                for ext in exts:
                    del_files += glob(sd + ext)
        return del_files, del_dirs

    def gather_metadata(self):
        """Get information about this job and return a dict that will be
        used to create a json file for accumulated metadata"""

        datafile = DataStarFile(
            os.path.join(self.output_dir, "corrected_micrographs.star")
        )
        metadata = {
            "MicrographCount": datafile.count_block("micrographs"),
            "LogFiles": [f"{os.path.join(self.output_dir, 'logfile.pdf')}"],
        }

        return metadata

    # needs to return an EMPIAR correctedMics object
    def prepare_onedep_data(self):
        mpfile = os.path.join(self.output_dir, "corrected_micrographs.star")
        return prepare_empiar_mics_parts(mpfile, is_parts=False, is_cor_parts=False)

    def create_results_display(self):
        outfile = os.path.join(self.output_dir, "corrected_micrographs.star")
        return [
            mini_montage_from_starfile(
                starfile=outfile,
                block="micrographs",
                column="_rlnMicrographName",
                nimg=2,
                outputdir=self.output_dir,
            )
        ]


class RelionMotionCorrOwn(RelionMotionCorrJob):

    PROCESS_NAME = MOTIONCORR_OWN_NAME

    def __init__(self):
        RelionJob.__init__(self)
        RelionMotionCorrJob.__init__(self)
        self.jobinfo.display_name = "RELION motion correction (RelionCorr)"

        self.jobinfo.short_desc = (
            "Motion correct and merge multi-frame movies using RELION's implementation"
        )
        self.jobinfo.long_desc = (
            "The Motion correction job-type implements RELION’s own (CPU-based)"
            " implementation of the UCSF motioncor2 program, Besides executing the"
            " calculations on the CPU/GPU, there are three other differences between"
            " the two implementations:\n- Bayesian polishing (for per-particle"
            " motion-correction) can only read local motion tracks from the Relion"
            " implementation.\n- The motioncor2 program performs outlier-pixel"
            " detection on-the-fly, and this information is not conveyed to Bayesian"
            " polishing , which may result in unexpectedly bad particles after"
            " polishing.\n- The Relion implementation can write out the sum of power"
            " spectra over several movie frames, which can be passed directly into"
            " ctffind 4.1 for faster CTF-estimation."
        )
        self.joboptions["do_save_ps"] = BooleanJobOption(
            label="Save sum of power spectra?",
            default_value=False,
            help_text=(
                "Sum of non-dose weighted power spectra provides better signal for CTF"
                " estimation. The power spectra can be used by CTFFIND4 but not by"
                " GCTF. This option is not available for UCSF MotionCor2."
            ),
        )

        self.joboptions["group_for_ps"] = FloatJobOption(
            label="Sum power spectra every e/A2:",
            default_value=4,
            suggested_min=0,
            suggested_max=10,
            step_value=0.5,
            help_text=(
                "McMullan et al (Ultramicroscopy, 2015) suggest summing power spectra"
                " every 4.0 e/A2 gives optimal Thon rings"
            ),
            deactivate_if=[("do_save_ps", "=", False)],
            required_if=[("do_save_ps", "=", True)],
        )

        self.joboptions["do_float16"] = BooleanJobOption(
            label="Write output in float16?",
            default_value=True,
            help_text=(
                "If set to Yes, RelionCor2 will write output images in float16 MRC "
                "format. This will save a factor of two in disk space compared to "
                "the default of writing in float32. Note that RELION and CCPEM will "
                "read float16 images, but other programs may not (yet) do so. For "
                "example, Gctf will not work with float16 images. Also note that "
                "this option does not work with UCSF MotionCor2. For CTF estimation, "
                "use CTFFIND-4.1 with pre-calculated power spectra (activate the "
                "'Save sum of power spectra' option)."
            ),
        )

        self.set_joboption_order(
            [
                "input_star_mics",
                "first_frame_sum",
                "last_frame_sum",
                "eer_grouping",
                "do_float16",
                "bfactor",
                "patch_x",
                "patch_y",
                "group_frames",
                "do_save_ps",
                "group_for_ps",
                "bin_factor",
                "fn_gain_ref",
                "gain_rot",
                "gain_flip",
                "fn_defect",
                "do_dose_weighting",
                "do_save_noDW",
                "dose_per_frame",
                "pre_exposure",
                "nr_mpi",
                "nr_threads",
                "do_queue",
                "queuename",
                "qsub",
                "qsubscript",
                "min_dedicated",
                "other_args",
            ]
        )

    def get_commands(self):

        self.common_commands()
        self.command.append("--use_own")
        self.command += ["--j", self.joboptions["nr_threads"].get_string()]
        self.common_commands2()

        save_ps = self.joboptions["do_save_ps"].get_boolean()
        if save_ps:
            dose_for_ps = self.joboptions["group_for_ps"].get_number()
            if dose_for_ps <= 0:
                raise ValueError("Invalid dose for the grouping for power spectra.")

            dose_rate = self.joboptions["dose_per_frame"].get_number()
            if dose_rate <= 0:
                raise ValueError(
                    "Please specify the dose rate to calculate "
                    "the grouping for power spectra."
                )

            grouping_for_ps = int(round(dose_for_ps / dose_rate, 0))
            if grouping_for_ps == 0:
                grouping_for_ps = 1
            self.command += ["--grouping_for_ps", str(grouping_for_ps)]

        if self.joboptions["do_float16"].get_boolean():
            self.command.append("--float16")

        self.additional_args()
        commands = [self.command]
        return commands

    def add_compatibility_joboptions(self):
        self.relion_back_compatibility_joboptions(do_own=True)


class RelionMotionCorrMotionCor2(RelionMotionCorrJob):

    PROCESS_NAME = MOTIONCORR_MOTIONCOR2_NAME

    def __init__(self):
        RelionJob.__init__(self)
        RelionMotionCorrJob.__init__(self)
        self.jobinfo.display_name = "RELION motion correction (MotionCor2)"

        self.jobinfo.short_desc = (
            "Motion correct and merge multi-frame movies using MotionCor2"
        )
        self.jobinfo.long_desc = (
            "The MotionCor2 program performs outlier pixel detection on-the-fly, and"
            " this information is not conveyed to Bayesian polishing , which may result"
            " in unexpectedly bad particles after polishing.\n- The RELION"
            " implementation can write out the sum of power spectra over several movie"
            " frames, which can be passed directly into CTFFIND 4.1 for faster"
            " CTF-estimation."
        )

        # MotionCor2
        # getting MotionCor2 exe path from env variable
        default_motioncor2_location = user_settings.get_motioncor2_executable()

        self.joboptions["fn_motioncor2_exe"] = PathJobOption(
            label="MOTIONCOR2 executable:",
            default_value=default_motioncor2_location,
            help_text=(
                "Location of the MotionCor2 executable. You can control the default of"
                " this field by setting environment variable"
                " PIPELINER_MOTIONCOR2_EXECUTABLE, or by editing the first few lines in"
                " src/gui_jobwindow.h and recompile the code."
            ),
        )

        self.joboptions["other_motioncor2_args"] = StringJobOption(
            label="Other MOTIONCOR2 arguments",
            default_value="",
            help_text="Additional arguments that need to be passed to MotionCor2.",
        )

        self.joboptions["gpu_ids"] = StringJobOption(
            label="Which GPUs to use:",
            default_value="0",
            help_text=(
                "Provide a list of which GPUs (0,1,2,3, etc) to use. MPI-processes are"
                " separated by ':'. For example, to place one rank on device 0 and one"
                " rank on device 1, provide '0:1'.\n Note that multiple MotionCor2"
                " processes should not share a GPU; otherwise, it can lead to crash or"
                " broken outputs (e.g. black images) ."
            ),
        )

        self.set_joboption_order(
            [
                "input_star_mics",
                "first_frame_sum",
                "last_frame_sum",
                "eer_grouping",
                "bfactor",
                "patch_x",
                "patch_y",
                "group_frames",
                "bin_factor",
                "fn_gain_ref",
                "gain_rot",
                "gain_flip",
                "fn_defect",
                "do_dose_weighting",
                "do_save_noDW",
                "dose_per_frame",
                "pre_exposure",
                "fn_motioncor2_exe",
                "other_motioncor2_args",
                "gpu_ids",
                "nr_mpi",
                "nr_threads",
                "do_queue",
                "queuename",
                "qsub",
                "qsubscript",
                "min_dedicated",
                "other_args",
            ]
        )

    def get_commands(self):

        self.common_commands()
        self.command.append("--use_motioncor2")
        self.command += [
            "--motioncor2_exe",
            self.joboptions["fn_motioncor2_exe"].get_string(),
        ]
        other_motioncor2_args = self.joboptions["other_motioncor2_args"].get_string()
        if len(other_motioncor2_args) > 0:
            self.command += [
                "--other_motioncor2_args",
                "{}".format(self.joboptions["other_motioncor2_args"].get_string()),
            ]
        self.command += [
            "--gpu",
            "{}".format(self.joboptions["gpu_ids"].get_string()),
        ]
        self.common_commands2()
        self.additional_args()
        commands = [self.command]
        return commands

    def add_compatibility_joboptions(self):
        self.joboptions["do_float16"] = BooleanJobOption(
            label="Write output in float16?", default_value=False
        )
        self.relion_back_compatibility_joboptions(do_own=False)
