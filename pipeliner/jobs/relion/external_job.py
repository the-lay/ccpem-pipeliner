#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

from pipeliner.pipeliner_job import ExternalProgram
from .relion_job import RelionJob
from pipeliner.job_options import (
    InputNodeJobOption,
    StringJobOption,
    PathJobOption,
    MultiStringJobOption,
    MultipleChoiceJobOption,
    files_exts,
    EXT_MRC_MAP,
    EXT_STARFILE,
)
from pipeliner.data_structure import (
    SUCCESS_FILE,
    FAIL_FILE,
    RELION_EXTERNAL_NAME,
    EXTERNAL_DIR,
)
from pipeliner.nodes import (
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_PARTICLESDATA,
    NODE_MICROGRAPHCOORDSGROUP,
    NODE_MICROGRAPHMOVIEGROUPMETADATA,
    NODE_DENSITYMAP,
    NODE_MASK3D,
    NODE_NEWNODETYPE,
)
from pipeliner.node_factory import create_node
from pipeliner.node_factory import all_node_toplevel_types


class RelionExternal(RelionJob):
    OUT_DIR = EXTERNAL_DIR
    PROCESS_NAME = RELION_EXTERNAL_NAME

    def __init__(self):
        super().__init__()
        self.jobinfo.display_name = "Run external program"
        self.jobinfo.programs = []
        self.jobinfo.short_desc = "Run programs through RELION's external job function"
        self.jobinfo.long_desc = (
            "This function has mostly been replaced by the pipeliner's plugin feature"
            " but is still available."
        )

        # I/O
        self.joboptions["fn_exe"] = PathJobOption(
            label="External executable:",
            default_value="",
            help_text=(
                "Location of the script that will launch the external program. This"
                " script should write all its output in the directory specified with"
                " --o. Also, it should write in that same directory a file called {}"
                " upon successful exit, and {} upon failure.".format(
                    SUCCESS_FILE, FAIL_FILE
                )
            ),
        )

        # Optional input nodes
        self.joboptions["in_mov"] = InputNodeJobOption(
            label="Input movies:",
            node_type=NODE_MICROGRAPHMOVIEGROUPMETADATA,
            node_kwds=["relion"],
            default_value="",
            directory="",
            pattern=files_exts("Movies STAR files", EXT_STARFILE),
            help_text=(
                "Input movies. This will be passed with a --in_movies argument to the"
                " executable."
            ),
        )

        self.joboptions["in_mic"] = InputNodeJobOption(
            label="Input micrographs:",
            node_type=NODE_MICROGRAPHGROUPMETADATA,
            node_kwds=["relion"],
            default_value="",
            directory="",
            pattern=files_exts("Micrographs STAR files", EXT_STARFILE),
            help_text=(
                "Input micrographs. This will be passed with a --in_mics argument to"
                " the executable."
            ),
        )

        self.joboptions["in_part"] = InputNodeJobOption(
            label="Input particles:",
            node_type=NODE_PARTICLESDATA,
            node_kwds=["relion"],
            default_value="",
            directory="",
            pattern=files_exts("Particles STAR file", EXT_STARFILE),
            help_text=(
                "Input particles. This will be passed with a --in_parts argument to the"
                " executable."
            ),
        )

        self.joboptions["in_coords"] = InputNodeJobOption(
            label="Input coordinates:",
            node_type=NODE_MICROGRAPHCOORDSGROUP,
            node_kwds=["relion"],
            default_value="",
            directory="",
            pattern=files_exts(
                "coords STAR files", ["coords_suffix*.star"], exact=True
            ),
            help_text=(
                "Input coordinates. This will be passed with a --in_coords argument to"
                " the executable."
            ),
        )

        self.joboptions["in_3dref"] = InputNodeJobOption(
            label="Input 3D reference:",
            node_type=NODE_DENSITYMAP,
            default_value="",
            directory="",
            pattern=files_exts("MRC Map", EXT_MRC_MAP),
            help_text=(
                "Input 3D reference map. This will be passed with a --in_3dref argument"
                " to the executable."
            ),
        )

        self.joboptions["in_mask"] = InputNodeJobOption(
            label="Input 3D mask:",
            node_type=NODE_MASK3D,
            default_value="",
            directory="",
            pattern=files_exts("MRC Mask", EXT_MRC_MAP),
            help_text=(
                "Input 3D mask. This will be passed with a --in_mask argument to the"
                " executable."
            ),
        )

        # Optional parameters
        self.joboptions["param1_label"] = StringJobOption(
            label="Param1 - label:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )

        self.joboptions["param1_value"] = StringJobOption(
            label="Param1 - value:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )
        self.joboptions["param2_label"] = StringJobOption(
            label="Param2 - label:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )

        self.joboptions["param2_value"] = StringJobOption(
            label="Param2 - value:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )
        self.joboptions["param3_label"] = StringJobOption(
            label="Param3 - label:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )

        self.joboptions["param3_value"] = StringJobOption(
            label="Param3 - value:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )
        self.joboptions["param4_label"] = StringJobOption(
            label="Param4 - label:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )

        self.joboptions["param4_value"] = StringJobOption(
            label="Param4 - value:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )
        self.joboptions["param5_label"] = StringJobOption(
            label="Param5 - label:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )

        self.joboptions["param5_value"] = StringJobOption(
            label="Param5 - value:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )
        self.joboptions["param6_label"] = StringJobOption(
            label="Param6 - label:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )

        self.joboptions["param6_value"] = StringJobOption(
            label="Param6 - value:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )
        self.joboptions["param7_label"] = StringJobOption(
            label="Param7 - label:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )

        self.joboptions["param7_value"] = StringJobOption(
            label="Param7 - value:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )
        self.joboptions["param8_label"] = StringJobOption(
            label="Param8 - label:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )

        self.joboptions["param8_value"] = StringJobOption(
            label="Param8 - value:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )
        self.joboptions["param9_label"] = StringJobOption(
            label="Param9 - label:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )

        self.joboptions["param9_value"] = StringJobOption(
            label="Param9 - value:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )
        self.joboptions["param10_label"] = StringJobOption(
            label="Param10 - label:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )

        self.joboptions["param10_value"] = StringJobOption(
            label="Param10 - value:",
            default_value="",
            help_text=(
                "Define label and value for optional parameters to the script. These"
                " will be passed as an argument --label value"
            ),
            in_continue=True,
        )
        self.get_runtab_options(mpi=False, threads=True, addtl_args=True)

        # addition for making output nodes

        self.joboptions["fn_output"] = StringJobOption(
            label="Output file name (optional):",
            default_value="",
            help_text=(
                "The name of the output file (if known) this will allow the creation of"
                " an output node for this job, otherwise the resulting file will have"
                " to be imported "
            ),
        )

        self.joboptions["output_nodetype"] = MultipleChoiceJobOption(
            label="Output node type:",
            choices=all_node_toplevel_types(add_new=True),
            default_value_index=0,
            help_text=(
                "Select the node type to assign to the file, this tells the pipeliner "
                "what kind of file it is"
            ),
        )

        self.joboptions["alt_nodetype"] = StringJobOption(
            label="OR: Create a new node type:",
            default_value="",
            help_text=(
                "Add a new node type.  Only do this if none of the standard node"
                " types are applicable. They should almost always be. Actually don't"
                " do this!"
            ),
            required_if=[("output_nodetype", "=", NODE_NEWNODETYPE)],
            deactivate_if=[("output_nodetype", "!=", NODE_NEWNODETYPE)],
        )

        self.joboptions["output_kwds"] = MultiStringJobOption(
            label="Output node keywords:",
            default_value="",
            help_text=(
                "Enter keywords to describe the node, separated by `:::`. "
                "This should be things like the program that created the file "
                "and any relevant descriptions of the file.  For example a halfmap"
                " is of the DensityMap node type and should have 'halfmap' in its"
                " keywords"
            ),
        )

        external_exe = ExternalProgram(command="")
        external_exe.command = "<External programs>"
        external_exe.exe_path = "<Varies depending on parameters>"
        self.jobinfo.programs = [external_exe]

    def get_commands(self):

        fn_exe = self.joboptions["fn_exe"].get_string(
            True,
            "ERROR: empty field for the external executable script...",
        )
        command = [fn_exe]

        # add the exe to the list of programs needed to run
        self.jobinfo.programs = [fn_exe]

        innodes = {
            self.joboptions["in_mic"]: "--in_mics",
            self.joboptions["in_mov"]: "--in_movies",
            self.joboptions["in_coords"]: "--in_coords",
            self.joboptions["in_part"]: "--in_parts",
            self.joboptions["in_3dref"]: "--in_3Dref",
            self.joboptions["in_mask"]: "--in_mask",
        }

        for i in list(innodes):
            innode_name = i.get_string()
            if len(innode_name) > 0:
                command += [innodes[i], innode_name]

        # Added an output nodes section
        fn_output = self.joboptions["fn_output"].get_string()
        if fn_output:
            newout = self.output_dir + fn_output
            out_nodetype = self.joboptions["output_nodetype"].get_string()
            if out_nodetype == NODE_NEWNODETYPE:
                out_nodetype = self.joboptions["alt_nodetype"].get_string()
            out_kwds = self.joboptions["output_kwds"].get_list()
            print(out_kwds)
            self.output_nodes.append(create_node(newout, out_nodetype, out_kwds))

        command += ["--o", self.output_dir]

        params = [
            "param1",
            "param2",
            "param3",
            "param4",
            "param5",
            "param6",
            "param7",
            "param8",
            "param9",
            "param10",
        ]
        for param in params:
            param_name = self.joboptions[param + "_label"].get_string()
            if len(param_name) > 0:
                command.append("--" + param_name)
                param_val = self.joboptions[param + "_value"].get_string()
                if "'" not in param_val and '"' not in param_val:
                    param_val = param_val.split()
                else:
                    param_val = [param_val]
                if len(param_val) > 0:
                    command.extend(param_val)

        # don't send the thread command if no threads are specified
        if self.joboptions["nr_threads"].get_number() > 1:
            command += ["--j", self.joboptions["nr_threads"].get_string()]

        other_args = self.joboptions["other_args"].get_string()
        if len(other_args) > 0:
            command += self.parse_additional_args()

        commands = [command]
        return commands
