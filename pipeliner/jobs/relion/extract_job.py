#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from glob import glob
from pipeliner.utils import truncate_number
from .relion_job import RelionJob
from pipeliner.jobs.relion.relion_job import relion_program
from pipeliner.job_options import (
    InputNodeJobOption,
    files_exts,
    EXT_STARFILE,
    BooleanJobOption,
    FloatJobOption,
    IntJobOption,
)
from pipeliner.data_structure import (
    EXTRACT_DIR,
    EXTRACT_HELICAL_NAME,
    EXTRACT_PARTICLE_NAME,
    EXTRACT_REXTRACT_NAME,
)
from pipeliner.nodes import (
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_PARTICLESDATA,
    NODE_MICROGRAPHCOORDSGROUP,
)
from pipeliner.node_factory import create_node

from pipeliner.deposition_tools.empiar_deposition_objects import (
    prepare_empiar_mics_parts,
)
from pipeliner.display_tools import mini_montage_from_starfile


class RelionExtractJob(RelionJob):
    OUT_DIR = EXTRACT_DIR

    def __init__(self):
        super().__init__()
        self.always_continue_in_schedule = True
        self.jobinfo.programs = [relion_program("relion_preprocess")]

        self.jobinfo.long_desc = (
            "Extracts the particles and gather all required metadata"
        )

        self.joboptions["star_mics"] = InputNodeJobOption(
            label="micrograph STAR file:",
            node_type=NODE_MICROGRAPHGROUPMETADATA,
            node_kwds=["relion"],
            default_value="",
            directory="",
            pattern=files_exts("Micrographs STAR file", EXT_STARFILE),
            help_text=(
                "Filename of the STAR file that contains all micrographs from which to"
                " extract particles."
            ),
            is_required=True,
        )

        self.joboptions["extract_size"] = IntJobOption(
            label="Particle box size (pix):",
            default_value=128,
            suggested_min=64,
            suggested_max=512,
            step_value=8,
            help_text=(
                "Size of the extracted particles (in pixels). This should be an even"
                " number!"
            ),
        )

        self.joboptions["do_invert"] = BooleanJobOption(
            label="Invert contrast?",
            default_value=True,
            help_text="If set to Yes, the contrast in the particles will be inverted.",
        )

        self.joboptions["do_norm"] = BooleanJobOption(
            label="Normalize particles?",
            default_value=True,
            help_text=(
                "If set to Yes, particles will be normalized in the way RELION"
                " prefers it."
            ),
        )

        self.joboptions["bg_diameter"] = IntJobOption(
            label="Diameter background circle (pix):",
            default_value=-1,
            suggested_min=-1,
            suggested_max=600,
            step_value=10,
            help_text=(
                "Particles will be normalized to a mean value of zero and a"
                " standard-deviation of one for all pixels in the background area.The"
                " background area is defined as all pixels outside a circle with this"
                " given diameter in pixels (before rescaling). When specifying a"
                " negative value, a default value of 75% of the Particle box size will"
                " be used."
            ),
            deactivate_if=[("do_norm", "=", False)],
        )

        self.joboptions["white_dust"] = FloatJobOption(
            label="Stddev for white dust removal:",
            default_value=-1,
            suggested_min=-1,
            suggested_max=10,
            step_value=0.1,
            help_text=(
                "Remove very white pixels from the extracted particles. Pixels values"
                " higher than this many times the image stddev will be replaced with"
                " values from a Gaussian distribution. \n \n Use negative value to"
                " switch off dust removal."
            ),
            deactivate_if=[("do_norm", "=", False)],
        )

        self.joboptions["black_dust"] = FloatJobOption(
            label="Stddev for black dust removal:",
            default_value=-1,
            suggested_min=-1,
            suggested_max=10,
            step_value=0.1,
            help_text=(
                "Remove very black pixels from the extracted particles.Pixels values"
                " higher than this many times the image stddev will be replaced with"
                " values from a Gaussian distribution. \n \n Use negative value to"
                " switch off dust removal."
            ),
            deactivate_if=[("do_norm", "=", False)],
        )

        self.joboptions["do_rescale"] = BooleanJobOption(
            label="Rescale particles?",
            default_value=False,
            help_text=(
                "If set to Yes, particles will be re-scaled. Note that the particle"
                " diameter below will be in the down-scaled images."
            ),
        )

        self.joboptions["rescale"] = IntJobOption(
            label="Re-scaled size (pixels):",
            default_value=128,
            suggested_min=64,
            suggested_max=512,
            step_value=8,
            help_text="The re-scaled value needs to be an even number",
            deactivate_if=[("do_rescale", "=", False)],
        )

        self.joboptions["do_fom_threshold"] = BooleanJobOption(
            label="Use autopick FOM threshold?",
            default_value=False,
            help_text=(
                "If set to Yes, only particles with rlnAutopickFigureOfMerit values"
                " below the threshold below will be extracted."
            ),
        )

        self.joboptions["minimum_pick_fom"] = FloatJobOption(
            label="Minimum autopick FOM:",
            default_value=0,
            suggested_min=-5,
            suggested_max=10,
            step_value=0.1,
            help_text=(
                "The minimum value for the rlnAutopickFigureOfMerit for"
                " particles to be extracted."
            ),
            deactivate_if=[("do_fom_threshold", "=", False)],
        )

        self.joboptions["do_float16"] = BooleanJobOption(
            label="Write output in float16?",
            default_value=True,
            help_text=(
                "If set to Yes, this program will write output images in float16 MRC"
                " format. This will save a factor of two in disk space compared to the"
                " default of writing in float32. Note that RELION and CCPEM will read"
                " float16 images, but other programs may not (yet) do so."
            ),
        )

        self.get_runtab_options(mpi=True, threads=False, addtl_args=True)

    def common_commands(self):
        nr_mpi = int(self.joboptions["nr_mpi"].get_number())
        if nr_mpi > 1:
            self.command = self.get_mpi_command() + ["relion_preprocess_mpi"]
        else:
            self.command = ["relion_preprocess"]

        star_mics = self.joboptions["star_mics"].get_string(
            True, "ERROR: Empty field for input STAR file"
        )
        self.command += ["--i", star_mics]

    def common_commands2(self):
        extract_size = self.joboptions["extract_size"].get_number()
        fn_ostar = self.output_dir + "particles.star"

        self.command += [
            "--part_star",
            fn_ostar,
            "--part_dir",
            self.output_dir,
            "--extract",
            "--extract_size",
            str(int(extract_size)),
        ]

        do_rescale = self.joboptions["do_rescale"].get_boolean()
        do_norm = self.joboptions["do_norm"].get_boolean()
        do_invert = self.joboptions["do_invert"].get_boolean()

        bg_diameter = self.joboptions["bg_diameter"].get_number()
        if bg_diameter < 0:
            bg_diameter = 0.75 * extract_size
        bg_radius = int(bg_diameter / 2)

        if do_rescale:
            rescale = self.joboptions["rescale"].get_number()
            self.command += ["--scale", str(int(rescale))]
            bg_radius = int((rescale * bg_radius) / extract_size)

        if do_norm:
            bg_radius = int(bg_radius)
            self.command += [
                "--norm",
                "--bg_radius",
                str(bg_radius),
                "--white_dust",
                self.joboptions["white_dust"].get_string(),
                "--black_dust",
                self.joboptions["black_dust"].get_string(),
            ]

        if do_invert:
            self.command.append("--invert_contrast")

        if self.joboptions["do_fom_threshold"].get_boolean():
            self.command += [
                "--minimum_pick_fom",
                self.joboptions["minimum_pick_fom"].get_string(),
            ]
        if self.joboptions["do_float16"].get_boolean():
            self.command.append("--float16")

        return fn_ostar

    def common_commands3(self):
        if self.is_continue:
            self.command.append("--only_do_unfinished")

        other_args = self.joboptions["other_args"].get_string()
        if len(other_args) > 0:
            self.command += self.parse_additional_args()

    def prepare_clean_up_lists(self, do_harsh=False):
        """Return list of intermediate files/dirs to remove"""

        # get all of the subdirs
        subdirs = [x[0] for x in os.walk(self.output_dir, topdown=False)]
        subdirs.remove(self.output_dir)
        del_files, del_dirs = [], []

        for sd in subdirs:
            if do_harsh:
                # remove everything star files and particle stacks
                del_dirs.append(sd)
            else:
                # only remove starfiles with metadata
                del_files += glob(sd + "/*_extract.star")
        return del_files, del_dirs

    def prepare_onedep_data(self):
        mpfile = os.path.join(self.output_dir, "particles.star")
        return prepare_empiar_mics_parts(mpfile, is_parts=True, is_cor_parts=False)

    def gather_metadata(self):
        outfile = os.path.join(self.output_dir, "run.out")
        with open(outfile, "r") as of:
            outlines = of.readlines()

        metadata_dict = {}

        for line in outlines:
            if "Joining metadata" in line:
                metadata_dict["NumberOfMicrographs"] = int(line.split()[6])
            if "Written out" in line:
                metadata_dict["NumberOfParticles"] = int(line.split()[5])

        return metadata_dict

    def create_results_display(self):
        outstar = os.path.join(self.output_dir, "particles.star")
        dispobj = mini_montage_from_starfile(
            starfile=outstar,
            block="particles",
            column="_rlnImageName",
            outputdir=self.output_dir,
            nimg=30,
        )
        return [dispobj]


class RelionExtract(RelionExtractJob):
    PROCESS_NAME = EXTRACT_PARTICLE_NAME

    def __init__(self):
        RelionJob.__init__(self)
        RelionExtractJob.__init__(self)

        self.jobinfo.display_name = "RELION extract particles (single particle)"
        self.jobinfo.short_desc = "Extract particle images from micrographs"

        # TO DO: This should be changed to coords_fn as
        # coords_suffix is the 3.1 nomenclature
        self.joboptions["coords_suffix"] = InputNodeJobOption(
            label="Input coordinates:",
            node_type=NODE_MICROGRAPHCOORDSGROUP,
            node_kwds=["relion"],
            default_value="",
            directory="",
            pattern=files_exts("Coordinates files", EXT_STARFILE),
            help_text="Filename of the coords file from relion picking",
            is_required=True,
        )

        self.set_joboption_order(["star_mics", "coords_suffix"])

    def get_commands(self):

        self.common_commands()

        coords_suffix = self.joboptions["coords_suffix"]
        fn_coords_suffix = coords_suffix.get_string(
            True,
            "ERROR: empty field for coordinate STAR file...",
        )
        self.command += ["--coord_list", fn_coords_suffix]
        fn_ostar = self.common_commands2()
        self.output_nodes.append(create_node(fn_ostar, NODE_PARTICLESDATA, ["relion"]))
        self.common_commands3()
        commands = [self.command]
        return commands


class RelionExtractHelical(RelionExtractJob):
    PROCESS_NAME = EXTRACT_HELICAL_NAME

    def __init__(self):
        RelionJob.__init__(self)
        RelionExtractJob.__init__(self)
        self.jobinfo.display_name = "RELION extract particles (helical)"

        self.jobinfo.short_desc = "Particle extraction of overlapping helical segments"
        self.jobinfo.long_desc += (
            ". Assigns an additional psi prior used in helical"
            " classification/refinement"
        )

        self.joboptions["coords_suffix"] = InputNodeJobOption(
            label="Input coordinates:",
            node_type=NODE_MICROGRAPHCOORDSGROUP,
            node_kwds=["relion", "helixstartend"],
            default_value="",
            directory="",
            pattern=files_exts("Coordinates STAR file", EXT_STARFILE),
            help_text=(
                "Filename of the coords_suffix file with the directory structure and"
                " the suffix of all coordinate files."
            ),
            is_required=True,
        )

        self.joboptions["helical_tube_outer_diameter"] = FloatJobOption(
            label="Tube diameter (A):",
            default_value=200,
            suggested_min=100,
            suggested_max=1000,
            step_value=10,
            help_text=(
                "Outer diameter (in Angstroms) of helical tubes. This value should be"
                " slightly larger than the actual width of helical tubes."
            ),
            jobop_group="Helical processing options",
        )

        self.joboptions["helical_bimodal_angular_priors"] = BooleanJobOption(
            label="Use bimodal angular priors?",
            default_value=True,
            help_text=(
                "Normally it should be set to Yes and bimodal angular priors will be"
                " applied in the following classification and refinement jobs. Set to"
                " No if the 3D helix looks the same when rotated upside down."
            ),
            jobop_group="Helical processing options",
        )

        self.joboptions["do_extract_helical_tubes"] = BooleanJobOption(
            label="Coordinates are start-end only?",
            default_value=True,
            help_text=(
                "Set to Yes if you want to extract helical segments from manually"
                " picked tube coordinates (starting and end points of helical tubes in"
                " RELION, EMAN or XIMDISP format). Set to No if segment coordinates"
                " (RELION auto-picked results or EMAN / XIMDISP segments) are provided."
            ),
            jobop_group="Helical processing options",
        )

        self.joboptions["do_cut_into_segments"] = BooleanJobOption(
            label="Cut helical tubes into segments?",
            default_value=True,
            help_text=(
                "Set to Yes if you want to extract multiple helical segments with a"
                " fixed inter-box distance. If it is set to No, only one box at the"
                " center of each helical tube will be extracted."
            ),
            jobop_group="Helical processing options",
        )

        self.joboptions["helical_nr_asu"] = IntJobOption(
            label="Number of unique asymmetrical units:",
            default_value=1,
            suggested_min=1,
            suggested_max=100,
            step_value=1,
            help_text=(
                "Number of unique helical asymmetrical units in each segment box. This"
                " integer should not be less than 1. The inter-box distance (pixels) ="
                " helical rise (Angstroms) * number of asymmetrical units / pixel size"
                " (Angstroms). The optimal inter-box distance might also depend on the"
                " box size, the helical rise and the flexibility of the structure. In"
                " general, an inter-box distance of ~10% * the box size seems"
                " appropriate."
            ),
            deactivate_if=[("do_cut_into_segments", "=", False)],
            jobop_group="Helical processing options",
        )
        # put a more informative help message here
        self.joboptions["helical_rise"] = FloatJobOption(
            label="Helical rise (A):",
            default_value=1,
            suggested_min=0,
            suggested_max=100,
            step_value=0.01,
            help_text=(
                "Helical rise in Angstroms. (Please click '?' next to the option above"
                " for details about how the inter-box distance is calculated.)"
            ),
            deactivate_if=[("do_cut_into_segments", "=", False)],
            jobop_group="Helical processing options",
        )

        self.set_joboption_order(
            [
                "star_mics",
                "coords_suffix",
                "extract_size",
                "do_invert",
                "do_norm",
                "bg_diameter",
                "white_dust",
                "black_dust",
                "do_extract_helical_tubes",
                "helical_tube_outer_diameter",
                "helical_bimodal_angular_priors",
                "do_cut_into_segments",
                "helical_nr_asu",
                "helical_rise",
                "do_rescale",
                "rescale",
                "do_fom_threshold",
                "minimum_pick_fom",
                "do_float16",
                "nr_mpi",
                "do_queue",
                "queuename",
                "qsub",
                "qsubscript",
                "other_args",
            ]
        )

    def get_commands(self):

        self.common_commands()

        coords_fn = self.joboptions["coords_suffix"].get_string(
            True,
            "ERROR: empty field for coordinate STAR file...",
        )

        self.command += ["--coord_list", coords_fn]

        fn_ostar = self.common_commands2()
        self.output_nodes.append(
            create_node(fn_ostar, NODE_PARTICLESDATA, ["relion", "helicalsegments"])
        )

        self.command.append("--helix")
        htod = self.joboptions["helical_tube_outer_diameter"].get_string()
        self.command += ["--helical_outer_diameter", htod]

        if self.joboptions["helical_bimodal_angular_priors"].get_boolean():
            self.command.append("--helical_bimodal_angular_priors")
        helical_tubes = self.joboptions["do_extract_helical_tubes"].get_boolean()
        if helical_tubes:
            self.command.append("--helical_tubes")
            if self.joboptions["do_cut_into_segments"].get_boolean():
                self.command.append("--helical_cut_into_segments")
                helical_nr_asu = self.joboptions["helical_nr_asu"].get_string()
                self.command += ["--helical_nr_asu", helical_nr_asu]
                helical_rise = self.joboptions["helical_rise"].get_string()
                self.command += ["--helical_rise", helical_rise]
            else:
                self.command += ["--helical_nr_asu", "1", "--helical_rise", "1"]

        self.common_commands3()
        commands = [self.command]

        if helical_tubes:
            fn_out = self.output_dir + "helix_segments.star"
            self.output_nodes.append(
                create_node(
                    fn_out, NODE_MICROGRAPHCOORDSGROUP, ["relion", "helicalsegments"]
                )
            )

        return commands


class RelionExtractReExtract(RelionExtractJob):
    PROCESS_NAME = EXTRACT_REXTRACT_NAME

    def __init__(self):
        RelionJob.__init__(self)
        RelionExtractJob.__init__(self)
        self.jobinfo.display_name = "RELION re-extract particles"
        self.jobinfo.short_desc = "Re-extract particles after refinement"

        self.joboptions["fndata_reextract"] = InputNodeJobOption(
            label="Refined particles STAR file:",
            node_type=NODE_PARTICLESDATA,
            node_kwds=["relion"],
            default_value="",
            directory="",
            pattern=files_exts("Particles STAR file", EXT_STARFILE),
            help_text=(
                "Filename of the STAR file with the refined particle coordinates, e.g."
                " from a previous 2D or 3D classification or auto-refine run."
            ),
            is_required=True,
        )

        self.joboptions["do_reset_offsets"] = BooleanJobOption(
            label="Reset the refined offsets to zero?",
            default_value=False,
            help_text=(
                "If set to Yes, the input origin offsets will be reset to zero. This"
                " may be useful after 2D classification of helical segments, where one"
                " does not want neighbouring segments to be translated on top of each"
                " other for a subsequent 3D refinement or classification."
            ),
        )

        self.joboptions["do_recenter"] = BooleanJobOption(
            label="OR: re-center refined coordinates?",
            default_value=False,
            help_text=(
                "If set to Yes, the input coordinates will be re-centered according to"
                " the refined origin offsets in the provided _data.star file. The unit"
                " is pixel, not angstrom. The origin is at the center of the box, not"
                " at the corner."
            ),
        )

        self.joboptions["recenter_x"] = IntJobOption(
            label="Re-center on X-coordinate (in pix):",
            default_value=0,
            help_text=(
                "Re-extract particles centered on this X-coordinate (in pixels in the"
                " reference)"
            ),
            deactivate_if=[("do_recenter", "=", False)],
        )

        self.joboptions["recenter_y"] = IntJobOption(
            label="Re-center on Y-coordinate (in pix):",
            default_value=0,
            help_text=(
                "Re-extract particles centered on this Y-coordinate (in pixels in the"
                " reference)"
            ),
            deactivate_if=[("do_recenter", "=", False)],
        )

        self.joboptions["recenter_z"] = IntJobOption(
            label="Re-center on Z-coordinate (in pix):",
            default_value=0,
            help_text=(
                "Re-extract particles centered on this Z-coordinate (in pixels in the"
                " reference)"
            ),
            deactivate_if=[("do_recenter", "=", False)],
        )

        self.set_joboption_order(
            [
                "fndata_reextract",
                "star_mics",
                "extract_size",
                "do_invert",
                "do_norm",
                "bg_diameter",
                "white_dust",
                "black_dust",
                "do_rescale",
                "rescale",
                "do_reset_offsets",
                "do_recenter",
                "recenter_x",
                "recenter_y",
                "recenter_z",
                "do_fom_threshold",
                "minimum_pick_fom",
                "do_float16",
                "nr_mpi",
                "do_queue",
                "queuename",
                "qsub",
                "qsubscript",
                "other_args",
            ]
        )

    def get_commands(self):

        self.common_commands()
        fndata_reextract = self.joboptions["fndata_reextract"].get_string(
            True,
            "ERROR: empty field for refined particles STAR file...",
        )

        do_reset_offsets = self.joboptions["do_reset_offsets"].get_boolean()
        do_recenter = self.joboptions["do_recenter"].get_boolean()
        if do_reset_offsets and do_recenter:
            raise ValueError(
                "ERROR: you cannot both reset refined offsets and recenter on refined"
                " coordinates, choose one..."
            )
        self.command += ["--reextract_data_star", fndata_reextract]
        if do_reset_offsets:
            self.command.append("--reset_offsets")
        elif do_recenter:
            recenter_x = self.joboptions["recenter_x"].get_number()
            recenter_y = self.joboptions["recenter_y"].get_number()
            recenter_z = self.joboptions["recenter_z"].get_number()
            self.command += [
                "--recenter",
                "--recenter_x",
                truncate_number(recenter_x, 1),
                "--recenter_y",
                truncate_number(recenter_y, 1),
                "--recenter_z",
                truncate_number(recenter_z, 1),
            ]

        fn_ostar = self.common_commands2()
        self.output_nodes.append(create_node(fn_ostar, NODE_PARTICLESDATA, ["relion"]))

        self.common_commands3()

        commands = [self.command]

        # TO DO: Check if this file is now written in the relion 4 style
        # and update accordingly
        fn_out = self.output_dir + "reextract.star"
        self.output_nodes.append(
            create_node(fn_out, NODE_MICROGRAPHCOORDSGROUP, ["relion"])
        )

        return commands
