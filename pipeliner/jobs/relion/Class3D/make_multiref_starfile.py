#!/usr/bin/env python3
#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
import sys
from typing import Optional, List
from gemmi import cif
import argparse

from pipeliner.star_writer import write


def main(in_args: Optional[List[str]] = None):

    parser = argparse.ArgumentParser()
    parser.add_argument("--output_dir", type=str, required=True)
    parser.add_argument("--references", type=str, required=True, nargs="+")
    if in_args is None:
        in_args = sys.argv[1:]
    args = parser.parse_args(in_args)

    output_dir = args.output_dir
    refs = args.references
    reffile = os.path.join(output_dir, "references.star")
    refs_doc = cif.Document()
    refs_block = refs_doc.add_new_block("references")
    loop = refs_block.init_loop("_rln", ["ReferenceImage"])
    for ref in refs:
        loop.add_row([ref])
    write(refs_doc, reffile)


if __name__ == "__main__":
    main()
