#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from typing import List

from pipeliner.jobs.relion.relion_job import RelionJob, relion_program
from pipeliner.job_options import (
    InputNodeJobOption,
    FloatJobOption,
    BooleanJobOption,
    MultipleChoiceJobOption,
    IntJobOption,
)
from pipeliner.nodes import NODE_DENSITYMAP
from pipeliner.display_tools import make_map_model_thumb_and_display
from pipeliner.node_factory import create_node


class RelionFilterMap(RelionJob):
    OUT_DIR = "FilterMap"
    PROCESS_NAME = "relion.map_utilities.filter_map"

    def __init__(self):
        super().__init__()
        self.jobinfo.programs = [relion_program("relion_image_handler")]
        self.jobinfo.display_name = "Filter map"
        self.jobinfo.short_desc = "Apply low-pass and/or high-pass filters to a map"

        self.jobinfo.long_desc = "Apply low-pass or high pass filters to a map"

        self.joboptions["input_map"] = InputNodeJobOption(
            label="Input map",
            node_type=NODE_DENSITYMAP,
            help_text="The map to filter",
            is_required=True,
            default_value="",
        )

        self.joboptions["do_lowpass"] = BooleanJobOption(
            label="Low pass filter the map?",
            default_value=False,
            help_text="Should a low-pass filter be applied to the map",
        )

        self.joboptions["lowpass"] = FloatJobOption(
            label="Low pass filter resolution (\u212B)",
            default_value=1,
            hard_min=0.1,
            suggested_min=0.5,
            deactivate_if=[("do_lowpass", "=", False)],
            required_if=[("do_lowpass", "=", True)],
            help_text=(
                "Resolution of low-pass filter.  Resolutions lower (worse) than this "
                "filter are retained"
            ),
        )

        self.joboptions["do_highpass"] = BooleanJobOption(
            label="High pass filter the map?",
            default_value=False,
            help_text="Should a high-pass filter be applied to the map",
        )

        self.joboptions["highpass"] = FloatJobOption(
            label="High pass filter resolution (\u212B)",
            default_value=1,
            hard_min=0.1,
            suggested_min=0.5,
            deactivate_if=[("do_highpass", "=", False)],
            required_if=[("do_highpass", "=", True)],
            help_text=(
                "Resolution of high-pass filter.  Resolutions higher (better) than "
                "this value are retained"
            ),
        )

        self.joboptions["do_advanced"] = BooleanJobOption(
            label="Use advanced options?",
            default_value=False,
            help_text="Advanced filtering options",
            jobop_group="Advanced options",
        )

        self.joboptions["filter_direction"] = MultipleChoiceJobOption(
            label="Low-pass filter directionality",
            choices=["non-directional", "X", "Y", "Z"],
            default_value_index=0,
            help_text="Useful for maps with aniostropic resolution",
            deactivate_if=[("do_lowpass", "=", False), ("do_advanced", "=", False)],
            jobop_group="Advanced options",
        )

        self.joboptions["filter_edge_width"] = IntJobOption(
            label="Filter edge width (resolution shells)",
            hard_min=1,
            default_value=2,
            deactivate_if=[("do_advanced", "=", False)],
            required_if=[("do_advanced", "=", True)],
            help_text=(
                "Width of the raised cosine on the low/high-pass filter edge "
                "(in resolution shells)"
            ),
            jobop_group="Advanced options",
        )

    def get_commands(self) -> List[List[str]]:
        do_hp = self.joboptions["do_highpass"].get_boolean()
        do_lp = self.joboptions["do_lowpass"].get_boolean()
        lp_val = self.joboptions["lowpass"].get_number() if do_lp else -1
        hp_val = self.joboptions["highpass"].get_number() if do_hp else 0

        if not do_hp and not do_lp:
            raise ValueError("No filtering selected")
        if lp_val == hp_val:
            raise ValueError(
                "High-pass and low-pass values are identical, no filtering will occur"
            )

        inmap = self.joboptions["input_map"].get_string(True, "Input map not found")
        command = ["relion_image_handler", "--i", inmap]

        if do_lp:
            command.extend(["--lowpass", lp_val])

        if do_hp:
            command.extend(["--highpass", hp_val])

        if self.joboptions["do_advanced"].get_boolean():
            edge_width = self.joboptions["filter_edge_width"].get_number()
            direction = self.joboptions["filter_direction"].get_string()
            adv = ["--filter_edge_width", edge_width]
            dir = [] if direction == "non-directional" else ["--directional", direction]
            command.extend(adv + dir)

        outfile = os.path.splitext(os.path.basename(inmap))[0] + "_filt.mrc"
        outname = os.path.join(self.output_dir, outfile)
        self.output_nodes.append(create_node(outname, NODE_DENSITYMAP, ["filtered"]))
        command.extend(["--o", outname])
        return [command]

    def create_results_display(self) -> list:
        inmap = self.joboptions["input_map"].get_string()
        outfile = os.path.splitext(os.path.basename(inmap))[0] + "_filt.mrc"
        outname = os.path.join(self.output_dir, outfile)
        do_hp = self.joboptions["do_highpass"].get_boolean()
        do_lp = self.joboptions["do_lowpass"].get_boolean()
        lp_val = self.joboptions["lowpass"].get_number()
        hp_val = self.joboptions["highpass"].get_number()
        title = "Filtered map"
        title = title + f" Low-pass filter {lp_val} \u212B" if do_lp else title
        title = title + f" High-pass filter {hp_val} \u212B" if do_hp else title
        return [
            make_map_model_thumb_and_display(
                title=title,
                outputdir=self.output_dir,
                maps=[outname],
                start_collapsed=False,
            )
        ]
