#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from typing import List

from pipeliner.jobs.relion.relion_job import RelionJob, relion_program
from pipeliner.job_options import (
    InputNodeJobOption,
    FloatJobOption,
)
from pipeliner.nodes import NODE_DENSITYMAP
from pipeliner.display_tools import make_map_model_thumb_and_display
from pipeliner.node_factory import create_node


class RelionApplyBFactor(RelionJob):
    OUT_DIR = "BFactor"
    PROCESS_NAME = "relion.map_utilities.apply_bfactor"

    def __init__(self):
        super().__init__()
        self.jobinfo.programs = [relion_program("relion_image_handler")]
        self.jobinfo.display_name = "Apply B-factor"
        self.jobinfo.short_desc = "Apply B-factor to a map"

        self.jobinfo.long_desc = (
            "The term B-factor, sometimes called the Debye-Waller factor, temperature "
            "factor, or atomic displacement parameter, is used to account for the "
            "attenuation of scattering caused by thermal motion."
        )

        self.joboptions["input_map"] = InputNodeJobOption(
            label="Input map",
            node_type=NODE_DENSITYMAP,
            help_text="The map to apply B-factor correction to",
            is_required=True,
            default_value="",
        )

        self.joboptions["bfact"] = FloatJobOption(
            label="B-factor to apply (\u212B^2)",
            default_value=-100,
            suggested_max=0,
            is_required=True,
        )

    def get_commands(self) -> List[List[str]]:
        inmap = self.joboptions["input_map"].get_string(True, "Input map not found")
        bfact = self.joboptions["bfact"].get_number()
        outfile = os.path.splitext(os.path.basename(inmap))[0] + "_bfact.mrc"
        outname = os.path.join(self.output_dir, outfile)
        self.output_nodes.append(create_node(outname, NODE_DENSITYMAP, ["b_factor"]))

        command = [
            "relion_image_handler",
            "--i",
            inmap,
            "--bfactor",
            bfact,
            "--o",
            outfile,
        ]

        return [command]

    def create_results_display(self) -> list:
        bfact = self.joboptions["bfact"].get_number()
        inmap = self.joboptions["input_map"].get_string()
        outfile = os.path.splitext(os.path.basename(inmap))[0] + "_bfact.mrc"
        outname = os.path.join(self.output_dir, outfile)
        return [
            make_map_model_thumb_and_display(
                title=f"Map with b-factor applied: {bfact} \u212B^2",
                outputdir=self.output_dir,
                maps=[outname],
                start_collapsed=False,
            )
        ]
