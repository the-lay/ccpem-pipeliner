#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
import sys
from typing import List, Tuple
from glob import glob
from gemmi import cif

from pipeliner.jobs.relion.relion_job import RelionJob, relion_program
from pipeliner.job_options import (
    FileNameJobOption,
    StringJobOption,
    BooleanJobOption,
    PathJobOption,
    MultipleChoiceJobOption,
    FloatJobOption,
    IntJobOption,
    MultiStringJobOption,
    InputNodeJobOption,
)
from pipeliner.data_structure import (
    IMPORT_DIR,
    IMPORT_MOVIES_NAME,
    IMPORT_OTHER_NAME,
)
from pipeliner.nodes import (
    NODE_MICROGRAPHCOORDSGROUP,
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_MICROGRAPHMOVIEGROUPMETADATA,
    NODE_IMAGE2DGROUPMETADATA,
    NODE_PARTICLESDATA,
    NODE_MICROSCOPEDATA,
    NODE_NEWNODETYPE,
)

from pipeliner.node_factory import all_node_toplevel_types, create_node
from pipeliner.starfile_handler import JobStar, DataStarFile
from pipeliner.job_options import files_exts
from pipeliner.deposition_tools.empiar_deposition_objects import prepare_empiar_raw_mics
from pipeliner.display_tools import (
    mini_montage_from_starfile,
    make_map_model_thumb_and_display,
    create_results_display_object,
    mini_montage_from_stack,
    make_particle_coords_thumb,
    tiff_thumbnail,
)
from pipeliner.mrc_image_tools import mrc_thumbnail

RELION_NODE_TYPES = [
    "RELION coordinates STAR file (.star)",
    "Particles STAR file (.star)",
    "2D references STAR file (.star)",
    "Micrographs STAR file (.star)",
]


class RelionImportMovies(RelionJob):
    OUT_DIR = IMPORT_DIR
    PROCESS_NAME = IMPORT_MOVIES_NAME

    def __init__(self):
        super().__init__()
        self.jobinfo.programs = [relion_program("relion_import")]
        self.jobinfo.display_name = "Import raw micrographs"
        self.jobinfo.short_desc = "Import movies or single-frame micrographs"

        self.jobinfo.long_desc = (
            "Import raw micrographs and create a star file containing the collected"
            " micrographs and their metadata"
        )

        self.joboptions["fn_in_raw"] = PathJobOption(
            label="Raw input files:",
            default_value="Movies/*.tif",
            help_text=(
                "Provide a Linux wildcard that selects all raw movies or micrographs to"
                " be imported."
            ),
            wildcard_allowed=True,
            must_be_in_project=True,
            in_continue=True,
            is_required=True,
        )
        self.joboptions["is_multiframe"] = BooleanJobOption(
            label="Are these multi-frame movies?",
            default_value=True,
            help_text=(
                "Set to Yes for multi-frame movies, set to No for single-frame"
                " micrographs."
            ),
            in_continue=True,
        )
        self.joboptions["optics_group_name"] = StringJobOption(
            label="Optics group name:",
            default_value="opticsGroup1",
            help_text=(
                "Name of this optics group. Each group of movies/micrographs with"
                " different optics characteristics for CTF refinement should have a"
                " unique name."
            ),
            in_continue=True,
            is_required=True,
            validation_regex="^[a-zA-Z0-9_]+$",
        )
        self.joboptions["fn_mtf"] = FileNameJobOption(
            label="MTF of the detector:",
            default_value="",
            pattern=files_exts("STAR Files", [".star"]),
            directory=".",
            help_text=(
                "As of release-3.1, the MTF of the detector is used in the refinement"
                " stages of refinement. If you know the MTF of your detector, provide"
                " it here. Curves for some well-known detectors may be downloaded from"
                " the RELION Wiki. Also see there for the exact format \n If you do not"
                " know the MTF of your detector and do not want to measure it, then by"
                " leaving this entry empty, you include the MTF of your detector in"
                " your overall estimated B-factor upon sharpening the map. Although"
                " that is probably slightly less accurate, the overall quality of your"
                " map will probably not suffer very much. \n \n Note that when"
                " combining data from different detectors, the differences between"
                " their MTFs can no longer be absorbed in a single B-factor, and"
                " providing the MTF here is important!"
            ),
            in_continue=True,
            node_type=NODE_MICROSCOPEDATA,
            node_kwds=["mtf"],
        )
        self.joboptions["angpix"] = FloatJobOption(
            label="Pixel size (Angstrom):",
            default_value=1.4,
            suggested_min=0.5,
            suggested_max=3,
            step_value=0.1,
            help_text="Pixel size in Angstroms. ",
            in_continue=True,
            hard_min=0.0,
            is_required=True,
        )
        self.joboptions["kV"] = IntJobOption(
            label="Voltage (kV):",
            default_value=300,
            suggested_min=50,
            suggested_max=500,
            step_value=10,
            help_text="Voltage the microscope was operated on (in kV)",
            in_continue=True,
            hard_min=0,
            is_required=True,
        )
        self.joboptions["Cs"] = FloatJobOption(
            label="Spherical aberration (mm):",
            default_value=2.7,
            suggested_min=0,
            suggested_max=8,
            step_value=0.1,
            help_text=(
                "Spherical aberration of the microscope used to collect these images"
                " (in mm). Typical values are 2.7 (FEI Titan & Talos, most JEOL"
                " CRYO-ARM), 2.0 (FEI Polara), 1.4 (some JEOL CRYO-ARM) and 0.01"
                " (microscopes with a Cs corrector)."
            ),
            in_continue=True,
            hard_min=0,
            is_required=True,
        )
        self.joboptions["Q0"] = FloatJobOption(
            label="Amplitude contrast:",
            default_value=0.1,
            suggested_min=0,
            suggested_max=0.3,
            step_value=0.01,
            help_text=(
                "Fraction of amplitude contrast. Often values around 10% work better"
                " than theoretically more accurate lower values..."
            ),
            in_continue=True,
            hard_min=0.0,
            is_required=True,
        )
        self.joboptions["beamtilt_x"] = FloatJobOption(
            label="Beamtilt in X (mrad):",
            default_value=0.0,
            suggested_min=-1.0,
            suggested_max=1.0,
            step_value=0.1,
            help_text=(
                "Known beamtilt in the X-direction (in mrad). Set to zero if unknown."
            ),
            in_continue=True,
            is_required=True,
        )
        self.joboptions["beamtilt_y"] = FloatJobOption(
            label="Beamtilt in Y (mrad):",
            default_value=0.0,
            suggested_min=-1.0,
            suggested_max=1.0,
            step_value=0.1,
            help_text=(
                "Known beamtilt in the Y-direction (in mrad). Set to zero if unknown."
            ),
            in_continue=True,
            is_required=True,
        )

        self.joboptions["is_synthetic"] = BooleanJobOption(
            label="Does the node contain synthetic data?",
            default_value=False,
            help_text=(
                "It is always important to differentiate synthetic data from "
                "experimental data!"
            ),
        )

    def get_commands(self):
        is_synthetic = self.joboptions["is_synthetic"].get_boolean()
        kwds = ["relion", "synthetic"] if is_synthetic else ["relion"]

        self.command = ["relion_import"]
        if self.joboptions["is_multiframe"].get_boolean():
            fn_out = "movies.star"
            node = create_node(
                self.output_dir + fn_out,
                NODE_MICROGRAPHMOVIEGROUPMETADATA,
                kwds,
            )
            self.output_nodes.append(node)
            self.command.append("--do_movies")
        else:
            fn_out = "micrographs.star"
            node = create_node(
                self.output_dir + fn_out,
                NODE_MICROGRAPHGROUPMETADATA,
                kwds,
            )
            self.output_nodes.append(node)
            self.command.append("--do_micrographs")

        optics_group = self.joboptions["optics_group_name"].get_string()
        if optics_group == "":
            raise ValueError("ERROR: please specify an optics group name.")
        og = optics_group.replace("$$", "")
        og = og.replace("-", "")
        if not og.isalnum():
            raise ValueError(
                "ERROR: an optics group name may contain only numbers, letters and"
                " hyphens(-)."
            )
        self.command += ["--optics_group_name", "{}".format(optics_group)]

        fn_mtf = self.joboptions["fn_mtf"].get_string()
        if len(fn_mtf) > 0:
            self.command += ["--optics_group_mtf", fn_mtf]

        self.command += [
            "--angpix",
            self.joboptions["angpix"].get_string(),
            "--kV",
            self.joboptions["kV"].get_string(),
            "--Cs",
            self.joboptions["Cs"].get_string(),
            "--Q0",
            self.joboptions["Q0"].get_string(),
            "--beamtilt_x",
            self.joboptions["beamtilt_x"].get_string(),
            "--beamtilt_y",
            self.joboptions["beamtilt_y"].get_string(),
        ]

        fn_in = self.joboptions["fn_in_raw"].get_string()
        self.command += [
            "--i",
            "{}".format(fn_in),
            "--odir",
            self.output_dir,
            "--ofile",
            fn_out,
        ]

        if self.is_continue:
            self.command.append("--continue")

        commands = [self.command]
        return commands

    def add_compatibility_joboptions(self):
        # add joboptions that Relion needs for display, but aren't used by pipeliner
        # this is done here so these JobOptions are written to the run.job and job.star
        # files for Relion, but are not used during job creation
        self.joboptions["do_raw"] = BooleanJobOption(
            label="Import raw movies/micrographs?",
            default_value=True,
            jobop_group="Relion compatibility options",
        )
        self.joboptions["do_other"] = BooleanJobOption(
            label="Import other node types?",
            default_value=False,
            jobop_group="Relion compatibility options",
        )

    def gather_metadata(self):
        """Get information about this job and return a dict that will be
        used to create a json file for accumulated metadata"""
        datafile = DataStarFile(os.path.join(self.output_dir, "movies.star"))
        metadata = {"MovieCount": datafile.count_block("movies")}
        return metadata

    def prepare_onedep_data(self):
        return prepare_empiar_raw_mics(os.path.join(self.output_dir, "movies.star"))

    def create_results_display(self):
        movfile = os.path.join(self.output_dir, "movies.star")
        dispobj = mini_montage_from_starfile(
            starfile=movfile,
            block="movies",
            column="_rlnMicrographMovieName",
            outputdir=self.output_dir,
            nimg=2,
        )
        return [dispobj]


class RelionImportOther(RelionJob):
    OUT_DIR = IMPORT_DIR
    PROCESS_NAME = IMPORT_OTHER_NAME

    def __init__(self):
        super().__init__()
        self.do_status_check = False
        self.jobinfo.display_name = "Import files"
        self.jobinfo.short_desc = (
            "Import files other than raw micrographs or coordinate files"
        )
        self.jobinfo.programs = []
        self.jobinfo.long_desc = (
            "Import files so they are visible to the pipeline. This "
            "step is not always absolutely necessary, but will make the "
            "project more well organised"
        )
        self.joboptions["fn_in_other"] = FileNameJobOption(
            label="Input file:",
            default_value="",
            pattern=files_exts(name="Input File"),
            directory=".",
            help_text="Select a file to import.",
            is_required=True,
        )

        self.joboptions["is_relion"] = BooleanJobOption(
            label="Is the file a RELION STAR file?",
            default_value=False,
            help_text="RELION STAR files need to be handled slightly differently",
        )

        self.joboptions["node_type"] = MultipleChoiceJobOption(
            label="RELION STAR file type:",
            choices=RELION_NODE_TYPES,
            default_value_index=0,
            help_text="Select the type of RELION STAR file this is.",
            is_required=True,
            deactivate_if=[("is_relion", "=", False)],
        )

        self.joboptions["optics_group_particles"] = StringJobOption(
            label="Rename optics group for particles:",
            default_value="",
            help_text=(
                "Only for the import of a particles STAR file with a single, or no,"
                " optics groups defined: rename the optics group for the imported"
                " particles to this string."
            ),
            validation_regex="^[a-zA-Z0-9_]*$",
            deactivate_if=[
                ("is_relion", "=", False),
                ("node_type", "!=", "Particles STAR file (.star)"),
            ],
        )

        self.joboptions["pipeliner_node_type"] = MultipleChoiceJobOption(
            label="Node type for imported file:",
            choices=all_node_toplevel_types(add_new=True),
            default_value_index=0,
            help_text=(
                "Select the node type to assign to the file, this tells the pipeliner "
                "what kind of file it is"
            ),
            required_if=[("alt_nodetype", "!=", "")],
            deactivate_if=[("alt_nodetype", "!=", ""), ("is_relion", "=", True)],
        )

        self.joboptions["alt_nodetype"] = StringJobOption(
            label="OR: Create a new node type:",
            default_value="",
            help_text=(
                "Add a new node type.  Only do this if none of the standard node"
                " types are applicable. They should almost always be. Actually don't"
                " do this!"
            ),
            required_if=[
                ("pipeliner_node_type", "=", NODE_NEWNODETYPE),
            ],
            deactivate_if=[
                ("pipeliner_node_type", "!=", NODE_NEWNODETYPE),
                ("is_relion", "=", True),
            ],
        )

        self.joboptions["kwds"] = MultiStringJobOption(
            label="Keywords",
            default_value="",
            help_text=(
                "Enter keywords to describe the node, separated by `:::`. "
                "This should be things like the program that created the file "
                "and any relevant descriptions of the file.  For example a halfmap"
                " is of the DensityMap node type and should have 'halfmap' in its"
                " keywords"
            ),
        )

        self.joboptions["is_synthetic"] = BooleanJobOption(
            label="Does the node contain synthetic data?",
            default_value=False,
            help_text=(
                "It is always important to differentiate synthetic data from "
                "experimental data!"
            ),
        )

    def relion_import(self):
        self.command = ["relion_import"]
        fn_in = self.joboptions["fn_in_other"].get_string()
        node_type = self.joboptions["node_type"].get_string()

        fn_out = os.path.basename("/" + fn_in)
        node_translate = {
            "RELION coordinates STAR file (.star)": NODE_MICROGRAPHCOORDSGROUP,
            "Particles STAR file (.star)": NODE_PARTICLESDATA,
            "2D references STAR file (.star)": NODE_IMAGE2DGROUPMETADATA,
            "Micrographs STAR file (.star)": NODE_MICROGRAPHGROUPMETADATA,
        }
        try:
            mynodetype = node_translate[node_type]
        except KeyError:
            raise ValueError(
                "It appears that this job is being run from a RELION job.star or "
                "run.job file.  This file type is not compatible with the pipeliner"
                " relion.import.other job type.  Try creating a new input params "
                "file with 'pipeliner --default_jobstar relion.import.other'"
            )
        is_synthetic = self.joboptions["is_synthetic"].get_boolean()
        kwds = ["relion", "synthetic"] if is_synthetic else ["relion"]
        node = create_node(self.output_dir + fn_out, mynodetype, kwds)
        self.output_nodes.append(node)

        if mynodetype == NODE_PARTICLESDATA:
            self.command.append("--do_particles")
            optics_group = self.joboptions["optics_group_particles"].get_string()
            if optics_group != "":
                self.command += ["--particles_optics_group_name", optics_group]
        else:
            self.command.append("--do_other")

        self.command += ["--i", fn_in]
        self.command += ["--odir", self.output_dir]
        self.command += ["--ofile", fn_out]
        if self.is_continue:
            self.command.append("--continue")
        commands = [self.command]
        return commands

    def non_relion_import(self):
        # make the node type
        fn_in = self.joboptions["fn_in_other"].get_string()
        nodetype = self.joboptions["pipeliner_node_type"].get_string()
        if nodetype == NODE_NEWNODETYPE:
            alt_nt = self.joboptions["alt_nodetype"].get_string(
                True,
                "Manually entered node type is required if " "new nodetype is selected",
            )
            nodetype = alt_nt.replace(" ", "")

        raw_kwd = self.joboptions["kwds"].get_list()
        kwds = [x.replace(" ", "_") for x in raw_kwd]
        if self.joboptions["is_synthetic"].get_boolean() and "synthetic" not in kwds:
            kwds.append("synthetic")
        fname = os.path.basename(fn_in)
        final_fname = os.path.join(self.output_dir, f"{fname}")
        self.output_nodes.append(create_node(final_fname, nodetype, kwds))

        if os.path.dirname(fn_in) != "UserFiles":
            return [["cp", fn_in, final_fname]]
        else:
            return [["mv", fn_in, final_fname]]

    def get_commands(self):
        # choose the right way to generate commands
        if self.joboptions["is_relion"].get_boolean():
            return self.relion_import()
        else:
            return self.non_relion_import()

    def gather_metadata(self):
        """Get information about this job and return a dict that will be
        used to create a json file for accumulated metadata.  This could be
        expanded later to collect more detailed info about specific types
        of other imports"""

        jobstar = JobStar(os.path.join(self.output_dir, "job.star"))
        joboptions = jobstar.joboptions
        md_dict = {
            "FileName_in": joboptions["fn_in_other"],
            "NodeType": joboptions["node_type"],
        }
        if joboptions.get("optics_group_particles") is not None:
            md_dict["OpticsGroup"] = joboptions["optics_group_particles"]

        return md_dict

    def prepare_clean_up_lists(self, do_harsh=False):
        """There is no cleanup for this job"""
        pre = "Harsh " if do_harsh else ""
        print(pre + "Cleaning up " + self.output_dir)
        return [], []

    def relion_create_results_display(self):
        nt = self.joboptions["node_type"].get_string()
        fn_in = self.joboptions["fn_in_other"].get_string()
        fn_out = os.path.join(self.output_dir, os.path.basename("/" + fn_in))
        ex = os.path.splitext(fn_in)[1]
        # defining what kind of display to make for different node types
        minimontage = {
            "Particles STAR file (.star)": ("particles", "_rlnImageName", 30),
            "2D references STAR file (.star)": (None, "_rlnReferenceImage", -1),
            "Micrographs STAR file (.star)": ("micrographs", "_rlnMicrographName", 2),
        }

        if nt in minimontage and ex == ".star":
            return [
                mini_montage_from_starfile(
                    starfile=fn_out,
                    block=minimontage[nt][0],
                    column=minimontage[nt][1],
                    nimg=minimontage[nt][2],
                    outputdir=self.output_dir,
                )
            ]

        elif nt in minimontage and ex == ".mrcs":
            return [
                mini_montage_from_stack(stack_file=fn_out, outputdir=self.output_dir)
            ]

        elif nt == "RELION coordinates STAR file (.star)":
            infile = DataStarFile(fn_out).get_block("coordinate_files")
            mc_data = infile.find(["_rlnMicrographName", "_rlnMicrographCoordinates"])
            themic, coords = mc_data[0]
            return [
                make_particle_coords_thumb(
                    in_mrc=themic, in_coords=coords, out_dir=self.output_dir
                )
            ]
        else:
            return [
                create_results_display_object(
                    "text",
                    title=f"No results display for {nt}",
                    display_data=(
                        f"This node type {nt}, currently has no graphical display"
                    ),
                    associated_data=[fn_out],
                )
            ]

    def non_relion_results_display(self):
        thenode = self.output_nodes[0]
        imgname = os.path.basename(thenode.name).split(".")[0]
        ext = thenode.format

        def no_display(thenode):
            # general fail
            return create_results_display_object(
                "text",
                title="No results to display",
                display_data="No results display function has been added for"
                f" this node type: {thenode.type}",
                associated_data=[thenode.name],
            )

        def mapmodel3d(ext, thenode):
            # mrc maps, atomic models
            maps, models = [], []
            if ext in ["pdb", "cif"]:
                models.append(thenode.name)
            elif ext in ["mrc", "map"]:
                maps.append(thenode.name)
            else:
                return no_display(thenode)
            return make_map_model_thumb_and_display(
                outputdir=self.output_dir,
                maps=maps,
                models=models,
                start_collapsed=False,
            )

        def single_2d_image(ext, thenode):
            if self.joboptions["pipeliner_node_type"].get_string() == "Mask2D":
                title = f"Imported 2D mask (.{ext})"
            else:
                title = imgname
            thumbs_dir = os.path.join(self.output_dir, "Thumbnails")
            os.makedirs(thumbs_dir)
            if ext == "mrc":
                mrc_thumbnail(
                    thenode.name,
                    640,
                    os.path.join(thumbs_dir, imgname + ".png"),
                )
            elif ext == ["tif"]:
                tiff_thumbnail(
                    thenode.name,
                    640,
                    os.path.join(thumbs_dir, imgname + ".png"),
                )
            else:
                return no_display(thenode)

            return create_results_display_object(
                "image",
                title=title,
                image_path=os.path.join(self.output_dir, f"Thumbnails/{imgname}.png"),
                image_desc=f"imported image; {thenode.name}; {thenode.type}",
                associated_data=[thenode.name],
            )

        def image_stack_2d(thenode):
            return mini_montage_from_stack(
                stack_file=thenode.name,
                outputdir=self.output_dir,
                title="Imported stack",
            )

        # display is a bit rudimentary for now, can add more types in the future

        tl = thenode.toplevel_type
        if tl in ["AtomCoords", "DensityMap", "Image3D", "Mask3D"]:
            return [mapmodel3d(ext, thenode)]
        elif tl in ["Image2D", "Mask2D"]:
            return [single_2d_image(ext, thenode)]
        elif tl in ["Image2DStack"]:
            return [image_stack_2d(thenode)]
        else:
            return [no_display(thenode)]

    def create_results_display(self):
        if self.joboptions["is_relion"].get_boolean():
            return self.relion_create_results_display()
        else:
            return self.non_relion_results_display()

    def add_compatibility_joboptions(self):
        # add joboptions that Relion needs for display, but aren't used by pipeliner
        # this is done here so these JobOptions are written to the run.job and job.star
        # files for Relion, but are not used during job creation
        self.joboptions["do_raw"] = BooleanJobOption(
            label="Import raw movies/micrographs?",
            default_value=False,
            jobop_group="Relion compatibility options",
        )
        self.joboptions["do_other"] = BooleanJobOption(
            label="Import other node types?",
            default_value=True,
            jobop_group="Relion compatibility options",
        )


class RelionImportCoords(RelionJob):
    OUT_DIR = IMPORT_DIR
    PROCESS_NAME = "relion.import.coordinates"

    def __init__(self):
        super().__init__()
        self.do_status_check = False
        self.jobinfo.display_name = "Import coordinate files"
        self.jobinfo.short_desc = "Import coordinate files in .box or .star format"
        self.jobinfo.programs = []
        self.jobinfo.long_desc = (
            "Import coordinate files so they are visible to the pipeline. Each coords"
            " file should have the same name as the micrograph that it "
        )
        self.joboptions["coords_search_string"] = PathJobOption(
            label="Raw coordinates files search string:",
            default_value="",
            help_text="Search string to find the raw particles file in either .box or "
            ".star format.  Each coordinate file's name must start with the name of the"
            " associated micrograph.  IE: mic1_coords.box for mic1.mrc",
            wildcard_allowed=True,
            must_be_in_project=True,
        )

        self.joboptions["micrographs_starfile"] = InputNodeJobOption(
            label="Associated micrographs starfile",
            node_type=NODE_MICROGRAPHGROUPMETADATA,
            node_kwds=["relion"],
            directory="",
            default_value="",
            pattern=files_exts("Micrographs STAR file", [".star"]),
            help_text="RELION corrected micrographs starfile that contains the "
            "micrographs the coordinates refer to",
        )

    def get_mics_coords_search(self) -> Tuple[List[str], str, str]:
        """Get some values needed for both get_commands and postrun actions"""
        mics_sf = DataStarFile(self.joboptions["micrographs_starfile"].get_string())
        mics_list = mics_sf.column_as_list("micrographs", "_rlnMicrographName")
        coordsdir = os.path.join(self.output_dir, "CoordinateFiles")
        search_string = self.joboptions["coords_search_string"].get_string()
        return mics_list, coordsdir, search_string

    def get_commands(self):
        mics_list, coordsdir, search_string = self.get_mics_coords_search()

        # check the micrographs all have unique names
        filenames = set([os.path.basename(x) for x in mics_list])
        if len(filenames) != len(mics_list):
            raise ValueError(
                "Some micrographs do not have unique filenames.  All micrographs need "
                "to have unique names!"
            )
        # touch the output node, so run.out isn't made into an output node
        outfile = os.path.join(self.output_dir, "coordinates.star")
        self.output_nodes.append(create_node(outfile, "MicrographsCoords", ["relion"]))

        coms = [["mkdir", coordsdir]]
        coords_files = glob(search_string)
        if not len(coords_files):
            raise ValueError("No coordinate files were found")
        coords_files.sort()

        # can't use shell expansion of *.box
        for cf in coords_files:
            coms.append(["cp", cf, coordsdir]),

        coms.append(["touch", os.path.join(self.output_dir, "coordinates.star")])
        return coms

    def post_run_actions(self):
        mics_list, coordsdir, search_string = self.get_mics_coords_search()
        output_star = cif.Document()
        output_star.add_new_block("coordinate_files")
        cf_block = output_star.find_block("coordinate_files")
        loop = cf_block.init_loop(
            "_rln", ["MicrographName #1", "MicrographCoordinates #2"]
        )
        coords_glob = coordsdir + "/*.*"
        coords = [os.path.basename(x) for x in glob(coords_glob)]
        found_mics = []
        found_cfiles = []
        for mic in mics_list:
            mic_shortname = os.path.basename(mic).split(".")[0]
            for cfile in coords:
                cfile_shortname = os.path.basename(cfile)
                if cfile_shortname.startswith(mic_shortname):
                    loop.add_row([mic, os.path.join(coordsdir, cfile)])
                    found_mics.append(mic)
                    found_cfiles.append(cfile)
        outfile = os.path.join(self.output_dir, "coordinates.star")
        # remove the placeholder and write the file
        os.remove(outfile)
        output_star.write_file(outfile)

        if not len(found_cfiles):
            raise ValueError(
                f"None of {len(coords)} coordinate files were matched to Micrographs"
            )

        for mic in mics_list:
            if mic not in found_mics:
                sys.stderr.write(f"No associated coordinate file found for {mic}\n")
        for cfile in coords:
            if cfile not in found_cfiles:
                sys.stderr.write(f"No associated micrograph found for {cfile}\n")
        print(
            f"{len(found_mics)}/{len(mics_list)} micrographs were associated with "
            "coordinate files"
        )
        print(
            f"{len(found_cfiles)}/{len(coords)} coordinate files were associated with "
            "micrographs"
        )

    def create_results_display(self):
        """count the individual particle starfiles rather than the summary
        star file for on-the-fly updating"""
        # make the histogram
        sum_output = DataStarFile(os.path.join(self.output_dir, "coordinates.star"))
        partsfiles = sum_output.column_as_list(
            "coordinate_files", "_rlnMicrographCoordinates"
        )
        pcounts = []
        coordsext = self.joboptions["coords_search_string"].get_string().split(".")[-1]
        for pf in partsfiles:
            if coordsext == "box":
                with open(pf) as f:
                    pcounts.append(len(f.readlines()))
            elif coordsext == "star":
                pcounts.append(DataStarFile(pf).count_block())

        total_parts = sum(pcounts)

        graph = create_results_display_object(
            "histogram",
            title=f"{total_parts} picked particles",
            data_to_bin=pcounts,
            xlabel="Number of particles",
            ylabel="Micrographs",
            associated_data=[os.path.join(self.output_dir, "coordinates.star")],
        )

        # make the sample image
        out_file = os.path.join(self.output_dir, "coordinates.star")
        fb = DataStarFile(out_file).get_block("coordinate_files")
        files = fb.find(["_rlnMicrographName", "_rlnMicrographCoordinates"])[0]

        image = make_particle_coords_thumb(files[0], files[1], self.output_dir)

        return [graph, image]
