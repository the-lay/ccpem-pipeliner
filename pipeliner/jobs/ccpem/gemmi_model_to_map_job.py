#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
from pathlib import Path
from pipeliner.pipeliner_job import PipelinerJob
from pipeliner.job_options import (
    InputNodeJobOption,
    FloatJobOption,
    files_exts,
)
import json
from pipeliner.node_factory import create_node
from pipeliner.display_tools import (
    make_map_model_thumb_and_display,
    create_results_display_object,
)
from pipeliner.pipeliner_job import Ref

from pipeliner.jobs.ccpem.gemmi_tools import model_to_map
from pipeliner.nodes import NODE_ATOMCOORDS, NODE_DENSITYMAP


class GemmiModelToMap(PipelinerJob):
    PROCESS_NAME = "gemmi.map_utilities.model_to_map"
    OUT_DIR = "GemmiModelToMap"

    def __init__(self):
        super().__init__()
        self.jobinfo.display_name = "Gemmi Model to Map"
        self.jobinfo.short_desc = "Convert atomic model to map"
        self.jobinfo.long_desc = (
            "Calculates map from atomic using sfcalc command from Gemmi library."
        )
        self.jobinfo.programs = []
        self.version = "0.1"
        self.job_author = "Tom Burnley"
        self.jobinfo.references = [
            Ref(
                authors=["Wojdyr M"],
                title=("GEMMI: A library for structural biology"),
                journal="Journal of Open Source Software",
                year="2022",
                volume="7",
                issue="43",
                pages="4200",
                doi="10.21105/joss.04200",
            )
        ]
        self.jobinfo.documentation = "https://gemmi.readthedocs.io/"
        self.joboptions["input_model"] = InputNodeJobOption(
            label="Input model",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".pdb"]),
            default_value="",
            directory="",
            help_text="Model to generate map from",
            is_required=True,
            create_node=False,
        )
        self.joboptions["resolution"] = FloatJobOption(
            label="Resolution",
            default_value=10.0,
            suggested_min=0.5,
            suggested_max=100,
            hard_min=0,
            step_value=0.1,
            help_text="Resolution in Angstrom",
            is_required=True,
        )
        self.get_runtab_options()

    def get_commands(self):
        # Run in the job output directory
        self.working_dir = self.output_dir

        # model_to_map --resolution=4.0 --pdb=refined.pdb

        # Get parameters
        input_model = self.joboptions["input_model"].get_string(
            True, "Input file missing"
        )
        self.input_nodes.append(
            create_node(self.joboptions["input_model"].get_string(), NODE_ATOMCOORDS)
        )
        resolution = self.joboptions["resolution"].get_number()

        command = [model_to_map.__file__]
        command += ["--resolution", str(resolution)]
        command += ["--pdb", os.path.relpath(input_model, self.working_dir)]

        map_out = os.path.join(
            self.output_dir, "gemmi_" + Path(input_model).stem + ".mrc"
        )
        self.output_nodes.append(create_node(map_out, NODE_DENSITYMAP, ["simulated"]))
        pdb_out = os.path.join(
            self.output_dir, "gemmi_" + Path(input_model).stem + ".pdb"
        )
        self.output_nodes.append(create_node(pdb_out, NODE_ATOMCOORDS))

        commands = [command]
        return commands

    def create_results_display(self):
        thumbdir = os.path.join(self.output_dir, "Thumbnails")
        if not os.path.isdir(thumbdir):
            os.makedirs(thumbdir)
        output_map = self.output_nodes[0].name
        output_model = self.output_nodes[1].name
        input_model = self.input_nodes[0].name

        display_objs = []
        map_display = make_map_model_thumb_and_display(
            maps=[output_map],
            maps_opacity=[0.5],
            title="Map from model",
            models=[output_model],
            outputdir=self.output_dir,
            start_collapsed=False,
        )
        display_objs.append(map_display)
        # b-factor distribution
        bfact_json = os.path.join(
            self.output_dir, "bfact_" + Path(input_model).stem + ".json"
        )
        if os.path.isfile(bfact_json):
            with open(bfact_json, "r") as b:
                dict_b_iso = json.load(b)
            bfact_plotlyhist_obj = create_results_display_object(
                "plotlyhistogram",
                data=dict_b_iso,
                title="Atomic B-factor distribution",
                associated_data=[bfact_json],
            )
            display_objs.append(bfact_plotlyhist_obj)
        return display_objs
