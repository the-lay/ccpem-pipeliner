#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
import shutil
from typing import List
from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram
from pipeliner.job_options import (
    InputNodeJobOption,
    FloatJobOption,
    BooleanJobOption,
    files_exts,
    JobOptionValidationResult,
)
from pipeliner.node_factory import create_node
from pipeliner.pipeliner_job import Ref
from pipeliner.jobs.ccpem.model_validation.molprobity import get_molprobity_results
from pipeliner.jobs.ccpem.model_validation.smoc import get_smoc_results
from pipeliner.jobs.ccpem.model_validation.tortoize import get_tortoize_results
from pipeliner.jobs.ccpem.model_validation import validation_results

from pipeliner.display_tools import create_results_display_object
from ccpem_pyutils.scripts import analyse_bfactors
from pipeliner.nodes import (
    NODE_ATOMCOORDS,
    NODE_DENSITYMAP,
    NODE_LOGFILE,
    # NODE_LIGANDDESCRIPTION,
)

import json

bfactor_path = None

bfactor_path = os.path.realpath(analyse_bfactors.__file__)

results_path = os.path.realpath(validation_results.__file__)


class ModelValidate(PipelinerJob):
    PROCESS_NAME = "pipeliner.validation.model_validation.evaluate"
    OUT_DIR = "Model_Validation"

    def __init__(self):
        super().__init__()
        self.jobinfo.display_name = "Model Validation"
        self.jobinfo.short_desc = "Atomic model validation"
        self.jobinfo.long_desc = (
            "Validate protein atomic model quality and agreement with map."
            "Reports outliers based on:"
            "Stereochemistry:  (Molprobity),"
            "Local fit of atomic model in density: (TEMPy SMOC),"
        )
        self.jobinfo.programs = [
            ExternalProgram("molprobity.reduce"),
            ExternalProgram("molprobity.molprobity"),
            ExternalProgram("molprobity.ramalyze"),
            ExternalProgram("molprobity.rotalyze"),
        ]
        self.version = "0.2"
        self.job_author = "Agnel Joseph"
        self.jobinfo.references = [
            Ref(
                authors=["Williams C et al."],
                title=(
                    "MolProbity: More and better reference data for improved"
                    " all-atom structure validation ."
                ),
                journal="Protein Sci",
                year="2018",
                volume="27",
                issue="1",
                pages="293-315",
                doi="10.1002/pro.3330",
            ),
            Ref(
                authors=["Joseph et al."],
                title=(
                    "Refinement of atomic models in high resolution "
                    "EM reconstructions using Flex-EM and local assessment."
                ),
                journal="Methods",
                year="2016",
                volume="100",
                issue="1",
                pages="42-49",
                doi="10.1016/j.ymeth.2016.03.007",
            ),
        ]
        self.jobinfo.documentation = "https://doi.org/10.1107/S205979832101278X"

        self.outlier_plot_modes = {
            "b-factor_dev": "lines",
            "molprobity": "markers",
            "smoc": "lines",
            "smoc_outlier": "markers",
            "tortoize_ramaz": "lines",
            "tortoize_torsionz": "lines",
        }

        self.joboptions["input_model"] = InputNodeJobOption(
            label="Input model",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".pdb"]),
            default_value="",
            directory="",
            help_text="The input model to be evaluated",
            is_required=True,
        )

        # self.joboptions["input_ligand"] = InputNodeJobOption(
        #     label="Input ligand",
        #     node_type=NODE_LIGANDDESCRIPTION,
        #     pattern=files_exts("Ligand definition", [".cif"]),
        #     default_value="",
        #     directory="",
        #     help_text="The input model",
        # )

        self.joboptions["input_map"] = InputNodeJobOption(
            label="Input map",
            node_type=NODE_DENSITYMAP,
            default_value="",
            directory="",
            pattern=files_exts("3D map", [".mrc"]),
            help_text="The input map to evaluate the model against",
            is_required=False,
        )

        self.joboptions["resolution"] = FloatJobOption(
            label="Resolution",
            default_value=-1,
            suggested_min=0.5,
            suggested_max=20,
            step_value=0.1,
            help_text="Map resolution in Angstrom",
            required_if=[("input_map", "!=", "")],
            deactivate_if=[("input_map", "=", "")],
        )

        # self.joboptions["additional_map1"] = InputNodeJobOption(
        #     label="Input additional map 1 (half map 1 or independent map1)",
        #     node_type=NODE_DENSITYMAP,
        #     default_value="",
        #     directory="",
        #     pattern=files_exts("3D map", [".mrc"]),
        #     help_text="Additional input map to evaluate the model against",
        #     is_required=False,
        # )
        #
        # self.joboptions["additional_map2"] = InputNodeJobOption(
        #     label="Input additional map 2 (half map 2 or independent map2)",
        #     node_type=NODE_DENSITYMAP,
        #     default_value="",
        #     directory="",
        #     pattern=files_exts("3D map", [".mrc"]),
        #     help_text="Additional input map to evaluate the model against",
        #     required_if=[("half_map_refinement", "=", True)],
        # )

        self.joboptions["run_molprobity"] = BooleanJobOption(
            label="Molprobity",
            default_value=True,
            help_text="Run Molprobity geometry evaluation",
        )
        self.joboptions["run_smoc"] = BooleanJobOption(
            label="TEMPy SMOC",
            default_value=False if not shutil.which("TEMPY.smoc") else True,
            help_text="Run SMOC model-map fit evaluation",
        )
        self.joboptions["run_tortoize"] = BooleanJobOption(
            label="TORTOIZE",
            default_value=False if not shutil.which("tortoize") else True,
            help_text="Run Tortoize model quality evaluation",
        )

        self.get_runtab_options()

    def validate_joboptions(self) -> List[JobOptionValidationResult]:
        errors = []
        if self.joboptions["run_smoc"].get_boolean() and not shutil.which("TEMPY.smoc"):
            errors.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["run_smoc"]],
                    message=(
                        "'TEMPY.smoc' is not available in the system path, check that "
                        "it is installed"
                    ),
                )
            )
        if self.joboptions["run_tortoize"].get_boolean() and not shutil.which(
            "tortoize"
        ):
            errors.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["run_smoc"]],
                    message=(
                        "'tortoize' is not available in the system path, check that "
                        "it is installed"
                    ),
                )
            )
        return errors

    def encode(self):
        return self.__dict__

    def get_commands(self):
        # Run in the job output directory
        self.working_dir = self.output_dir

        # Get parameters
        input_model = self.joboptions["input_model"].get_string(
            True, "Input file missing"
        )
        self.input_model = os.path.relpath(input_model, self.working_dir)
        modelid = os.path.splitext(os.path.basename(self.input_model))[0]
        self.modelid = modelid  # TODO: long files names to be trimmed ?

        input_map = self.joboptions["input_map"].get_string(False, "Input file missing")
        self.input_map = os.path.relpath(input_map, self.working_dir)
        mapid = os.path.splitext(os.path.basename(self.input_map))[0]
        self.mapid = mapid  #

        resolution = self.joboptions["resolution"].get_number()
        run_molprobity = self.joboptions["run_molprobity"].get_boolean()
        run_smoc = self.joboptions["run_smoc"].get_boolean()
        run_tortoize = self.joboptions["run_tortoize"].get_boolean()
        # bfactor distribution
        commands = []
        if bfactor_path:
            # pdb_num = 1  # this needs to be set for multiple model input
            bfact_command = ["python3", bfactor_path]
            bfact_command += ["-p", self.input_model]
            bfact_command += ["-mid", self.modelid]
            commands += [bfact_command]
        # molprobity
        if run_molprobity:
            # this needs fixing: reduce output pdb is printed in stdout
            # and needs to be saved in a file for molprobity input
            # running the command in a separate shell to redirect output
            reduce_command = ["sh", "-c"]
            if (
                os.path.splitext(self.input_model)[-1].lower() == ".cif"
                or os.path.splitext(self.input_model)[-1].lower() == ".mmcif"
            ):
                reduce_out = "reduce_out.cif"
            else:
                reduce_out = "reduce_out.pdb"
            # use input model if reduce run fails. TODO: check this works
            copy_reduce_command = ["cp", self.input_model, reduce_out]
            commands += [copy_reduce_command]
            # reduce_out = os.path.join(self.output_dir, reduce_out)
            reduce_run = " ".join(
                [
                    self.jobinfo.programs[0].command,
                    "-FLIP",
                    "-Quiet",
                    self.input_model,
                    ">",
                    reduce_out,
                ]
            )
            reduce_command += [reduce_run]
            # run molprobity
            molprobity_command = [self.jobinfo.programs[1].command, reduce_out]
            molprobity_command += ["output.percentiles=True"]
            # run ramalyze
            ramalyze_command = ["sh", "-c"]
            ramalyze_out = "ramalyze_out"
            ramalyze_run = " ".join(
                [self.jobinfo.programs[2].command, self.input_model, ">", ramalyze_out]
            )
            ramalyze_command += [ramalyze_run]
            # run rotalyze
            rotalyze_command = ["sh", "-c"]
            rotalyze_out = "rotalyze_out"
            rotalyze_run = " ".join(
                [self.jobinfo.programs[3].command, self.input_model, ">", rotalyze_out]
            )
            rotalyze_command += [rotalyze_run]
            # gather results
            molprobity_results_command = [
                "python3",
                os.path.realpath(get_molprobity_results.__file__),
                "-molp",
                "molprobity.out",
                "-rama",
                ramalyze_out,
                "-rota",
                rotalyze_out,
                "-id",
                self.modelid,
            ]
            commands += [
                reduce_command,
                molprobity_command,
                ramalyze_command,
                rotalyze_command,
                molprobity_results_command,
            ]
            self.molprobity_output = os.path.join(self.output_dir, "molprobity.out")
            self.output_nodes.append(
                create_node(
                    self.molprobity_output,
                    NODE_LOGFILE,
                    ["molprobity", "output"],
                )
            )
            self.molprobity_output = os.path.realpath(self.molprobity_output)
        # smoc
        if run_smoc:
            # pdb_num = 1  # this needs to be set for multiple model input
            smoc_command = ["TEMPy.smoc"]
            smoc_command += ["-m", self.input_map]
            smoc_command += ["-p", self.input_model]
            smoc_command += ["-r", resolution]
            smoc_command += ["--smoc-window", 1]
            smoc_command += ["--output-format", "json"]
            smoc_command += ["--output-prefix", "smoc_score"]
            smoc_results_command = [
                "python3",
                os.path.realpath(get_smoc_results.__file__),
                "-smoc",
                "smoc_score.json",
                "-coord",
                self.modelid + "_residue_coordinates.json",
                "-id",
                self.modelid + "_" + self.mapid,
            ]
            commands += [smoc_command, smoc_results_command]
            modelid = os.path.splitext(os.path.basename(input_model))[0]
            # smoc_modelid = modelid + "_" + str(pdb_num)
            # writes out only pdb format, needs to include cif output
            # output_modelfile = os.path.join(
            #     self.output_dir, smoc_modelid + "_smoc.pdb"
            # )
            self.smoc_output = os.path.join(self.output_dir, "smoc_score.json")
            # self.output_nodes.append(create_node(output_modelfile,
            # OUTPUT_NODE_SMOCMODEL))
            self.output_nodes.append(
                create_node(self.smoc_output, NODE_LOGFILE, ["tempy", "smoc_scores"])
            )
            self.smoc_output = os.path.realpath(self.smoc_output)
            # self.smoc_df_output = os.path.join(self.output_dir, "smoc_score.csv")
            # self.smoc_df_output = os.path.realpath(self.smoc_df_output)
        if run_tortoize:
            tortoize_out = self.modelid + "_tortoize.json"
            tortoize_command = [
                "tortoize",
                self.input_model,
                tortoize_out,
            ]
            tortoize_results_command = [
                "python3",
                os.path.realpath(get_tortoize_results.__file__),
                "-tortoize",
                tortoize_out,
                "-id",
                self.modelid,
            ]
            commands += [tortoize_command, tortoize_results_command]

        # local_output = os.path.join(self.output_dir, modelid + "_outliersummary.txt")
        # self.output_nodes.append(
        #   create_node(
        #       local_output,
        #       NODE_LOGFILE,
        #       ["modelval", "local_output"],
        #   )
        # )
        if run_molprobity or run_smoc or run_tortoize:
            results_command = ["python3", results_path]
            commands += [results_command]

            glob_output_file = "global_report.txt"
            global_output = os.path.join(self.output_dir, glob_output_file)
            self.output_nodes.append(
                create_node(
                    global_output,
                    NODE_LOGFILE,
                    ["modelval", "glob_output"],
                )
            )

            cluster_output = os.path.join(self.output_dir, "outlier_clusters.csv")
            self.output_nodes.append(
                create_node(
                    cluster_output,
                    NODE_LOGFILE,
                    ["modelval", "cluster_output"],
                )
            )

        return commands

    def gather_metadata(self):

        outfile = os.path.join(self.output_dir, "run.out")
        with open(outfile, "r") as of:
            outlines = of.readlines()

        metadata_dict = {}
        metadata_dict["Peptide Plane"] = {}

        for line in outlines:
            if "Ramachandran outliers" in line:
                metadata_dict["RamachandranOutliers"] = float(line.split()[3])
            if "favoured" in line:
                metadata_dict["Favoured"] = float(line.split()[2])
            if "Rotamer outliers" in line:
                metadata_dict["RotamerOutliers"] = float(line.split()[3])
            if "Cis-proline" in line:
                metadata_dict["PeptidePlane"]["CisProline"] = float(line.split()[2])
            if "Cis-general" in line:
                metadata_dict["PeptidePlane"]["CisGeneral"] = float(line.split()[2])
            if "Twisted Proline" in line:
                metadata_dict["PeptidePlane"]["TwistedProline"] = float(line.split()[3])
            if "Twisted General" in line:
                metadata_dict["PeptidePlane"]["TwistedGeneral"] = float(line.split()[3])
            if "C-beta deviations" in line:
                metadata_dict["CBetaDeviations"] = int(line.split()[3])
            if line.strip().startswith("Clashscore"):
                metadata_dict["ClashScore"] = {}
                metadata_dict["ClashScore"]["ClashScore"] = float(line.split()[2])
                metadata_dict["ClashScore"]["Percentile"] = float(line.split()[4])
                metadata_dict["ClashScore"]["N"] = int(
                    line.split()[7].split("=")[1].replace(")", "")
                )
            if "RMS(bonds)" in line:
                metadata_dict["RMSBonds"] = float(line.split()[2])
            if "RMS(angles)" in line:
                metadata_dict["RMSAngles"] = float(line.split()[2])
            if "MolProbity score" in line:
                metadata_dict["MolProbityScore"] = {}
                metadata_dict["MolProbityScore"]["MolProbityScore"] = float(
                    line.split()[3]
                )
                metadata_dict["MolProbityScore"]["Percentile"] = float(line.split()[5])
                metadata_dict["MolProbityScore"]["N"] = int(
                    line.split()[8].split("=")[1].replace(")", "")
                )
            if "Resolution" in line:
                metadata_dict["Resolution"] = float(line.split()[2])
            if "RefinementProgram" in line:
                metadata_dict["RefinementProgram"] = line.split()[3]

        return metadata_dict

    def create_results_display(self):
        input_model = self.joboptions["input_model"].get_string(
            True, "Input file missing"
        )
        self.input_model = os.path.relpath(input_model, self.output_dir)
        modelid = os.path.splitext(os.path.basename(self.input_model))[0]
        self.modelid = modelid  # TODO: long files names to be trimmed ?

        input_map = self.joboptions["input_map"].get_string(False, "Input file missing")
        self.input_map = os.path.relpath(input_map, self.output_dir)
        mapid = os.path.splitext(os.path.basename(self.input_map))[0]
        self.mapid = mapid
        run_molprobity = self.joboptions["run_molprobity"].get_boolean()
        run_smoc = self.joboptions["run_smoc"].get_boolean()
        run_tortoize = self.joboptions["run_tortoize"].get_boolean()

        display_objects = []
        # b-factor distribution plot
        bfact_json = os.path.join(
            self.output_dir, self.modelid + "_residue_bfactors.json"
        )
        with open(bfact_json, "r") as oj:
            bfact_data = json.load(oj)
        list_bfactors = []
        for model in bfact_data:
            if model == "max_dev":
                continue
            for chain in bfact_data[model]:
                for residue in bfact_data[model][chain]:
                    list_bfactors.append(bfact_data[model][chain][residue][0])
        plotlyhist_obj = create_results_display_object(
            "plotlyhistogram",
            data={"Residue B-factors": list_bfactors},
            title="Atomic B-factor distribution",
            associated_data=[bfact_json],
        )
        display_objects.append(plotlyhist_obj)
        # make global geometry stats table
        molprob_data = []
        if run_molprobity:
            mp_json = os.path.join(
                self.output_dir, self.modelid + "_molprobity_summary.json"
            )
            with open(mp_json, "r") as mpj:
                mp_data = json.load(mpj)
            labels = ["Metric", "Score", "Expected/Percentile"]

            for metric in mp_data:
                molp_metric = " ".join(metric.split("_"))
                score = mp_data[metric][0]
                pct = mp_data[metric][1]
                if "RamachandranZ" not in molp_metric:
                    molprob_data.append([molp_metric, score, pct])
        if run_tortoize:
            tort_json = os.path.join(
                self.output_dir, self.modelid + "_tortoize_summary.json"
            )
            with open(tort_json, "r") as j:
                tor_data = json.load(j)

            labels = ["Metric", "Score", "Expected/Percentile"]
            tortoize_data = []
            for model in tor_data:
                for metric in tor_data[model]:
                    molp_metric = " ".join(metric.split("_"))
                    score = round(float(tor_data[model][metric][0]), 3)
                    pct = tor_data[model][metric][1]
                    tortoize_data.append([molp_metric, score, pct])
                break
            molprob_data += tortoize_data
        if run_molprobity or run_tortoize:
            mp_summary = create_results_display_object(
                "table",
                title="Global geometry scores",
                headers=labels,
                table_data=molprob_data,
                associated_data=[mp_json],
            )

            display_objects.append(mp_summary)
        if run_molprobity or run_smoc or run_tortoize:
            # outlier clusters table
            clusters_file = os.path.join(self.output_dir, "outlier_clusters.csv")
            clusters = []
            with open(clusters_file, "r") as cf:
                for line in cf:
                    if line[0] == "#":
                        continue
                    line_split = line.split(",")
                    clusters.append(line_split)
            clusters_table = create_results_display_object(
                "table",
                title="Outlier clusters",
                headers=[
                    "Cluster",
                    "Chain_Residue",
                    "Outlier Type(s)",
                ],
                table_data=clusters,
                associated_data=[clusters_file],
            )
            display_objects.append(clusters_table)

            # residue outlier plot
            outplot_json = os.path.join(
                self.output_dir, self.modelid + "_plot_data.json"
            )
            with open(outplot_json, "r") as oj:
                outplot_data = json.load(oj)

            for model in outplot_data:
                for chain in outplot_data[model]:
                    x_data_series = []
                    y_data_series = []
                    series_labels = []
                    list_modes = []
                    min_y = 1.0
                    max_y = -1.0
                    for outlier in outplot_data[model][chain]:
                        series_labels.append(outlier)
                        x_data_series.append(outplot_data[model][chain][outlier][0])
                        y_data_series.append(outplot_data[model][chain][outlier][1])
                        min_y = min(min_y, min(y_data_series[-1]))
                        max_y = max(max_y, max(y_data_series[-1]))
                        # "b-factor_dev/" + str(max_bfact_dev)
                        list_modes.append(
                            self.outlier_plot_modes[outlier.split("/")[0]]
                        )
                    outplot_obj = create_results_display_object(
                        "graph",
                        xvalues=x_data_series,
                        yvalues=y_data_series,
                        title=f"Outliers: chain {chain}",
                        associated_data=[outplot_json],
                        data_series_labels=series_labels,  # [f"Chain {chain}"],
                        xaxis_label="Residue",
                        yaxis_label="Scores/Outliers",
                        yrange=[min_y - 0.1, max_y],
                        modes=list_modes,
                    )
                    display_objects.append(outplot_obj)
        return display_objects
