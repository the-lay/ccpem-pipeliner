#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
from typing import List, Any


class residue_summary_table_settings:
    # NOTE: data_keys has same keywords as in molprobity log
    data_titles = [
        "residue",
        "molprobity",
        "cablam",
        "smoc",
        "fdr",
        "fsc",
        "diffmap",
        "jpred",
    ]
    list_horz_headers = [
        "Residue",
        "Molprobity <br> outliers",
        "CaBLAM <br> outliers",
        "SMOC <br> outliers",
        "FDR <br> outliers",
        "FSC <br> outliers",
        "Diffmap <br> outliers",
        "Jpred <br> outliers",
    ]
    list_horz_header_tooltips = [
        "Residue number",
        "Outlier types from Molprobity",
        "Outlier types from CaBLAM",
        "Outlier based on SMOC z-score",
        "Outlier based on FDR z-score",
        "Outlier based on FSC z-score",
        "Outlier based on Diffmap z-score",
        "Outlier based on Jpred SS prediction vs current SS",
    ]
    list_vert_headers = List[Any]
    list_vert_header_tooltips = List[Any]
