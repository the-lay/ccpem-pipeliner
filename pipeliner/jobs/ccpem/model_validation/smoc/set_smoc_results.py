#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
import numpy as np
import pandas as pd
from collections import OrderedDict
import json
from typing import Union, Type

try:
    from scipy.spatial import cKDTree as kdt
except ImportError:
    from scipy.spatial import KDTree as kdt


class SetSMOCResults:
    def __init__(
        self,
        pipeliner_instance,
        list_chains=[],
        cootset_instance=None,
        glob_reportfile=None,
        fdr_process=None,
    ):
        self.job_location = pipeliner_instance.output_dir
        self.cootset = cootset_instance
        self.smoc_results = OrderedDict()
        self.smoc_csvs = OrderedDict()  # smoc scores per model
        self.dict_dataframes = OrderedDict()
        # outlier details
        self.dict_outlier_details = OrderedDict()
        # output json files for each model
        self.dict_jsonfiles = OrderedDict()
        self.list_modelids = []
        self.list_pdbids = []
        self.list_mapids = []
        self.residue_outliers = OrderedDict()
        self.residue_z = OrderedDict()
        self.residue_coordinates = OrderedDict()
        self.dict_cootdata = OrderedDict()
        self.dict_ca_coord = OrderedDict()
        self.list_modelids = []

        self.input_model = pipeliner_instance.input_model
        model_id = os.path.splitext(os.path.basename(self.input_model))[0]
        mapid = os.path.splitext(os.path.basename(pipeliner_instance.input_map))[0]
        self.list_modelids.append(model_id)

        run_smoc = pipeliner_instance.joboptions["run_smoc"].get_boolean()
        # resolution = pipeliner_instance.joboptions["resolution"].get_number()
        self.list_process_names = []

        if run_smoc:
            process_name = "smoc"
            self.list_process_names.append(process_name)
            smoc_df_output = pipeliner_instance.smoc_df_output
            if process_name not in self.dict_dataframes:
                self.dict_dataframes[process_name] = {}
            self.dict_dataframes[process_name][model_id] = pd.read_csv(
                smoc_df_output, skipinitialspace=True, skip_blank_lines=True
            )
            if process_name not in self.dict_jsonfiles:
                self.dict_jsonfiles[process_name] = {}
            self.dict_jsonfiles[process_name][model_id] = model_id + "_1_smoc.json"
            if process_name not in self.smoc_csvs:
                self.smoc_csvs[process_name] = []
            self.smoc_csvs[process_name].append(model_id + "_1_smoc.csv")

            try:
                self.smoc_results["smoc"].append(smoc_df_output)
            except KeyError:
                self.smoc_results["smoc"] = [smoc_df_output]

        # if process_name == 'fdr':
        #     try: process_result = glob.glob(os.path.join(job.location,
        #               '*_FDR_ranked_residues.csv'))[0]
        #     except: process_result = None
        #     modelid = job.name.split(' ')[-1]
        #     if not process_name in self.dict_dataframes:
        #         self.dict_dataframes[process_name] = {}
        #     if os.path.isfile(process_result):
        #         self.dict_dataframes[process_name][modelid] = pd.read_csv(
        #               process_result)
        #
        #     if not process_name in self.dict_jsonfiles:
        #         self.dict_jsonfiles[process_name] = {}
        #     self.dict_jsonfiles[process_name][modelid] = os.path.join(job.location,
        #                                             modelid+"_fdr.json")
        #
        #     if not process_name in self.smoc_csvs:
        #         self.smoc_csvs[process_name] = []
        #     self.smoc_csvs[process_name].append(
        #             os.path.join(job.location,modelid+'_fdr.csv'))
        #
        #     try: self.smoc_results['fdr'].append(process_result)
        #     except KeyError:
        #         self.smoc_results['fdr'] = [process_result]

        pdbid = model_id
        if pdbid not in self.list_pdbids:
            self.list_pdbids.append(pdbid)
            if self.job_location is not None:
                ca_coordfile = "ca_coord.json"
                if os.path.isfile(ca_coordfile):
                    self.dict_ca_coord[pdbid] = ca_coordfile

        if mapid not in self.list_mapids:
            self.list_mapids.append(mapid)
        # first map is used as main map, TODO: check this
        self.main_map = mapid
        # self.residue_outliers[pdbid] = {}
        self.residue_coordinates[mapid] = {}

        self.list_chains = list_chains
        self.add_chain_sections = False
        if len(self.list_chains) == 0:
            self.add_chain_sections = True

        self.min_val = None

        # set data for local outliers
        self.set_local_data()
        for process_name in self.list_process_names:
            self.check_chain_order_cootdata(process_name)
        self.add_cootdata(process_name)
        #         self.delete_cootdata()
        self.save_outlier_json()
        #         #set smoc plots
        self.set_local_results()
        # del results
        self.delete_dictresults()

    def add_cootdata(self, process_name):
        if self.cootset is not None:
            for pdbid in self.dict_cootdata:
                self.cootset.add_data_to_cootfile(
                    self.dict_cootdata[pdbid], modelid=pdbid, method=process_name
                )

    def delete_cootdata(self):
        self.dict_cootdata.clear()
        del self.dict_cootdata

    def delete_dictplots(self):
        self.dict_pdbdata.clear()
        del self.dict_pdbdata
        self.dict_mapdata.clear()
        del self.dict_mapdata

    def delete_dictresults(self):
        self.dict_outlier_details.clear()
        del self.dict_outlier_details
        self.residue_z.clear()
        del self.residue_z

    def set_local_data(self):
        #         if not self.add_chain_sections:
        #             for chain in self.list_chains:
        #                 smoc_subsection = 'smoc'+chain
        #                 self.add_subsection_to_chain(chain,smoc_subsection)
        self.dict_pdbdata = OrderedDict()
        self.dict_mapdata = OrderedDict()

        for process_name in self.dict_dataframes:
            ct_model = 0
            for mid in self.list_modelids:
                if mid in self.dict_dataframes[process_name]:
                    df = self.dict_dataframes[process_name][mid]
                    self.set_results_data(
                        df, mid, self.smoc_csvs[process_name][ct_model], process_name
                    )
                    ct_model += 1

    def save_outlier_json(self):
        for mid in self.dict_outlier_details:
            try:
                jsonfile = self.dict_jsonfiles[mid]
            except KeyError:
                continue
            with open(jsonfile, "w") as jf:
                json.dump(self.dict_outlier_details[mid], jf)

    def set_local_results(self):
        self.delete_dictplots()

    def parse_smoc_df(
        self,
        df,
        process_name,
        mid,
        list_chain_model,
        dict_pdb_ca_coord,
        sco,
        flag_coord_exists,
    ):
        mapid = mid.split(":")[0]
        pdbid = mid.split(":")[-1]
        # Get data
        for pdb in df:
            if "_" not in pdb:
                continue
            # add plots for each chain
            chain = pdb.split("_")[-1]
            # ct_data_index = 0
            if "." in chain:
                chain = chain.split(".")[0]
                # pdbname = ".".join(pdb.split(".")[:-1])
            if chain == "":
                chain = " "
            # added_smoc_text = False

            # add chain sections
            if self.add_chain_sections:
                if chain not in self.list_chains:
                    self.list_chains.append(chain)

            if chain in self.list_chains:
                if chain not in self.residue_outliers[process_name][mapid][pdbid]:
                    self.residue_outliers[process_name][mapid][pdbid][chain] = {}
                    self.residue_z[process_name][mapid][pdbid][chain] = {}
                    self.residue_coordinates[mapid][pdbid][chain] = {}
                if chain not in list_chain_model:
                    dict_results = {}
                    list_chain_model.append(chain)

                # ct_res_chain = 0
                pdb_df = df[pdb].dropna(axis=0, how="any")

                if pdb_df[0] == "resnum":
                    dict_results["residue"] = []
                    for val in pdb_df[1:]:
                        residue_num = int(float(val))
                        dict_results["residue"].append(residue_num)
                        try:
                            ca_coord = dict_pdb_ca_coord["1"][chain][str(residue_num)][
                                0
                            ]
                            res_name = dict_pdb_ca_coord["1"][chain][str(residue_num)][
                                1
                            ]
                            try:
                                dict_results["CAx"].append(ca_coord[0])
                                dict_results["CAy"].append(ca_coord[1])
                                dict_results["CAz"].append(ca_coord[2])
                                dict_results["resname"].append(res_name)
                            except KeyError:
                                dict_results["CAx"] = [ca_coord[0]]
                                dict_results["CAy"] = [ca_coord[1]]
                                dict_results["CAz"] = [ca_coord[2]]
                                dict_results["resname"] = [res_name]
                        except KeyError:
                            flag_coord_exists = False
                if not flag_coord_exists:
                    if "CAx" in dict_results:
                        del dict_results["CAx"]
                    if "CAy" in dict_results:
                        del dict_results["CAy"]
                    if "CAz" in dict_results:
                        del dict_results["CAz"]

                if pdb_df[0] == "smoc":
                    dict_results["smoc"] = []
                    for val in pdb_df[1:]:
                        dict_results["smoc"].append(round(float(val), 3))
                elif pdb_df[0] == "z_local":
                    dict_results["z_local"] = []
                    for val in pdb_df[1:]:
                        dict_results["z_local"].append(round(float(val), 3))
                elif pdb_df[0] == "z_global":
                    dict_results["z_global"] = []
                    for val in pdb_df[1:]:
                        dict_results["z_global"].append(round(float(val), 3))

                self.set_results_chain(
                    dict_results, sco, process_name, chain, pdbid, mapid, mid
                )

    def parse_fdr_df(
        self,
        df,
        process_name,
        mid,
        list_chain_model,
        dict_pdb_ca_coord,
        sco,
        flag_coord_exists,
    ):
        mapid = mid.split(":")[0]
        pdbid = mid.split(":")[-1]
        list_chains = df.Chain_name.unique()
        # Get data
        for chain in list_chains:
            # add chain sections
            if self.add_chain_sections:
                if chain not in self.list_chains:
                    self.list_chains.append(chain)
                chain_section = chain
                chain_name = "chain " + chain
                self.add_section_chain(chain_section, chain_name)

            if chain in self.list_chains:
                if chain not in self.residue_outliers[process_name][mapid][pdbid]:
                    self.residue_outliers[process_name][mapid][pdbid][chain] = {}
                    self.residue_z[process_name][mapid][pdbid][chain] = {}
                    self.residue_coordinates[mapid][pdbid][chain] = {}
                if chain not in list_chain_model:
                    dict_results = {}
                    list_chain_model.append(chain)

            df_chain = df.loc[df["Chain_name"] == chain]
            list_resnum = []
            list_scores = []
            dict_results = {}
            df_res = df_chain["residue_id"].tolist()
            df_score = df_chain["conf_score"].tolist()
            for res_l in range(len(df_chain["residue_id"])):
                res = df_res[res_l]
                sc = df_score[res_l]
                try:
                    resnum = int(res)
                except ValueError:
                    continue  # skip insertion codes
                list_resnum.append(resnum)
                list_scores.append(float(sc))
                try:
                    ca_coord = dict_pdb_ca_coord["1"][chain][str(resnum)][0]
                    res_name = dict_pdb_ca_coord["1"][chain][str(resnum)][1]
                    try:
                        dict_results["CAx"].append(ca_coord[0])
                        dict_results["CAy"].append(ca_coord[1])
                        dict_results["CAz"].append(ca_coord[2])
                        dict_results["resname"].append(res_name)
                    except KeyError:
                        dict_results["CAx"] = [ca_coord[0]]
                        dict_results["CAy"] = [ca_coord[1]]
                        dict_results["CAz"] = [ca_coord[2]]
                        dict_results["resname"] = [res_name]
                except IOError:
                    flag_coord_exists = False
            if not flag_coord_exists:
                if "CAx" in dict_results:
                    del dict_results["CAx"]
                if "CAy" in dict_results:
                    del dict_results["CAy"]
                if "CAz" in dict_results:
                    del dict_results["CAz"]

            dict_results["residue"] = list_resnum
            dict_results[process_name] = list_scores

            self.set_results_chain(
                dict_results, sco, process_name, chain, pdbid, mapid, mid
            )

    def set_results_data(self, df, mid, mid_csv=None, process_name="smoc"):
        mapid = mid.split(":")[0]
        pdbid = mid.split(":")[-1]
        list_chain_model = []
        #         if not process_name in self.dict_outlier_details:
        #             self.dict_outlier_details[process_name] = {}
        if mid not in self.dict_outlier_details:
            self.dict_outlier_details[mid] = {}
        if pdbid not in self.dict_cootdata:
            self.dict_cootdata[pdbid] = {}
        if process_name not in self.dict_cootdata[pdbid]:
            self.dict_cootdata[pdbid][process_name] = []
        if process_name not in self.residue_outliers:
            self.residue_outliers[process_name] = {}
            self.residue_z[process_name] = {}

        if mapid not in self.residue_outliers[process_name]:
            self.residue_outliers[process_name][mapid] = {}
            self.residue_z[process_name][mapid] = {}
            self.residue_coordinates[mapid] = {}
        if pdbid not in self.residue_outliers[process_name][mapid]:
            self.residue_outliers[process_name][mapid][pdbid] = {}
            self.residue_z[process_name][mapid][pdbid] = {}
            self.residue_coordinates[mapid][pdbid] = {}
        if mid_csv is not None:
            scoreout = mid_csv
            sco = open(scoreout, "w")
            sco.write("#chain,residue,score,z_local,z_global\n")

        # check if ca coordinates are available
        flag_coord_exists = False
        if pdbid in self.dict_ca_coord:
            ca_coord_json = self.dict_ca_coord[pdbid]
            with open(ca_coord_json, "r") as j:
                dict_pdb_ca_coord = json.load(j)
            flag_coord_exists = True
        else:
            dict_pdb_ca_coord = {}

        if process_name == "smoc":
            # Get data
            self.parse_smoc_df(
                df,
                process_name,
                mid,
                list_chain_model,
                dict_pdb_ca_coord,
                sco,
                flag_coord_exists,
            )
        elif process_name == "fdr":
            self.parse_fdr_df(
                df,
                process_name,
                mid,
                list_chain_model,
                dict_pdb_ca_coord,
                sco,
                flag_coord_exists,
            )
        #         pyrvapi.rvapi_set_plot_xmin('plot1', 'graphWidget1', 0.0)
        #         pyrvapi.rvapi_set_plot_ymin('plot1', 'graphWidget1', 0.0)

        sco.close()

    def set_results_chain(
        self, dict_results, sco, process_name, chain, pdbid, mapid, mid
    ):

        z_cutoff = -1.5
        if "residue" in dict_results and process_name in dict_results:
            try:
                assert len(dict_results["residue"]) == len(dict_results[process_name])
            except AssertionError:
                print(pdbid)
                print(len(dict_results["residue"]), len(dict_results[process_name]))
                print(dict_results["residue"], dict_results[process_name])

            if self.min_val is None:
                self.min_val = min(dict_results[process_name])
            else:
                self.min_val = min(self.min_val, min(dict_results[process_name]))
            # added_smoc_text = True
            # store plot data for each pdb and map
            if process_name not in self.dict_pdbdata:
                self.dict_pdbdata[process_name] = {}
            if chain not in self.dict_pdbdata[process_name]:
                self.dict_pdbdata[process_name][chain] = OrderedDict()
            if pdbid not in self.dict_pdbdata[process_name][chain]:
                self.dict_pdbdata[process_name][chain][pdbid] = OrderedDict()
            if mapid not in self.dict_pdbdata[process_name][chain][pdbid]:
                self.dict_pdbdata[process_name][chain][pdbid][mapid] = OrderedDict()

            self.dict_pdbdata[process_name][chain][pdbid][mapid] = [
                dict_results["residue"][:],
                dict_results[process_name][:],
            ]
            if "resname" in dict_results:
                if chain not in self.dict_outlier_details[mid]:
                    self.dict_outlier_details[mid][chain] = {}
                # save outliers in dict
                for res_l in range(len(dict_results["residue"])):
                    try:
                        self.dict_outlier_details[mid][chain][process_name].append(
                            [
                                str(dict_results["residue"][res_l])
                                + " "
                                + dict_results["resname"][res_l],
                                dict_results[process_name][res_l],
                            ]
                        )
                    except KeyError:
                        self.dict_outlier_details[mid][chain][process_name] = [
                            [
                                str(dict_results["residue"][res_l])
                                + " "
                                + dict_results["resname"][res_l],
                                dict_results[process_name][res_l],
                            ]
                        ]
            if process_name not in self.dict_mapdata:
                self.dict_mapdata[process_name] = {}
            if chain not in self.dict_mapdata[process_name]:
                self.dict_mapdata[process_name][chain] = OrderedDict()
            if mapid not in self.dict_mapdata[process_name][chain]:
                self.dict_mapdata[process_name][chain][mapid] = OrderedDict()
            if pdbid not in self.dict_mapdata[process_name][chain][mapid]:
                self.dict_mapdata[process_name][chain][mapid][pdbid] = OrderedDict()

            self.dict_mapdata[process_name][chain][mapid][pdbid] = [
                dict_results["residue"][:],
                dict_results[process_name][:],
            ]

            if (
                "resname" in dict_results
                and "CAx" in dict_results
                and "CAy" in dict_results
                and "CAz" in dict_results
            ):
                if process_name == "fdr":
                    smoc_z, list_z_local, list_z_global = self.select_outliers_by_score(
                        dict_results, process_name, z_cutoff
                    )
                elif process_name == "smoc":
                    if "z_local" in dict_results:
                        smoc_z = np.array(dict_results["z_local"]) < z_cutoff
                        list_z_local = dict_results["z_local"]
                        list_z_global = dict_results["z_global"]

                    else:
                        smoc_z, list_z_local, list_z_global = self.select_outliers_by_z(
                            dict_results, process_name, z_cutoff
                        )

                for res_l in range(len(dict_results["residue"])):
                    # write z scores
                    residue_scores = ",".join(
                        [
                            chain,
                            str(dict_results["resname"][res_l])
                            + "_"
                            + str(dict_results["residue"][res_l]),
                            "{0:.2f}".format(float(dict_results[process_name][res_l])),
                            "{0:.2f}".format(list_z_local[res_l]),
                            "{0:.2f}".format(list_z_global[res_l]),
                        ]
                    )

                    sco.write(residue_scores + "\n")

                    if process_name == "fdr":
                        self.residue_z[process_name][mapid][pdbid][chain][
                            str(dict_results["residue"][res_l])
                        ] = float(dict_results[process_name][res_l])
                    else:
                        self.residue_z[process_name][mapid][pdbid][chain][
                            str(dict_results["residue"][res_l])
                        ] = list_z_local[res_l]

                    if smoc_z[res_l]:
                        self.residue_outliers[process_name][mapid][pdbid][chain][
                            str(dict_results["residue"][res_l])
                        ] = "Outlier"
                        self.residue_coordinates[mapid][pdbid][chain][
                            str(dict_results["residue"][res_l])
                        ] = (
                            dict_results["CAx"][res_l],
                            dict_results["CAy"][res_l],
                            dict_results["CAz"][res_l],
                        )

                        # TODO: use only the full map (map0) for coot outliers
                        if (
                            mapid not in self.list_mapids
                            or self.list_mapids.index(mapid) > 0
                        ):
                            continue
                        try:
                            self.dict_cootdata[pdbid][process_name].append(
                                (
                                    chain,
                                    dict_results["residue"][res_l],
                                    dict_results["resname"][res_l],
                                    dict_results[process_name][res_l],
                                    (
                                        dict_results["CAx"][res_l],
                                        dict_results["CAy"][res_l],
                                        dict_results["CAz"][res_l],
                                    ),
                                )
                            )
                        except IndexError:
                            print("Could not set coot data for", pdbid, chain)
                            self.dict_cootdata[pdbid][process_name].append(
                                (
                                    chain,
                                    dict_results["residue"][res_l],
                                    "",
                                    dict_results[process_name][res_l],
                                    ("", "", ""),
                                )
                            )

    def check_chain_outliers(self, chain, process_name):
        for mid in self.dict_outlier_details:
            if chain in self.dict_outlier_details[mid]:
                if process_name in self.dict_outlier_details[mid][chain]:
                    return True
        return False

    def check_chain_order_cootdata(self, process_name):
        for pdbid in self.dict_cootdata:
            list_chain_sort = []
            for chain in self.list_chains:
                for resdata in self.dict_cootdata[pdbid][process_name]:
                    if resdata[0] in self.list_chains:
                        if resdata[0] == chain:
                            list_chain_sort.append(resdata)
                    else:
                        list_chain_sort.append(resdata)
            self.dict_cootdata[pdbid][process_name] = list_chain_sort[:]

    def select_outliers_by_score(self, dict_results, process_name, z_cutoff=-2.5):
        list_smoc = dict_results[process_name]
        array_smoc = np.array(list_smoc)
        list_z_global = self.calc_z_median(list_smoc)
        # if coords are available, set z score locally
        if "CAx" in dict_results and "CAy" in dict_results and "CAz" in dict_results:
            indi = list(
                zip(dict_results["CAx"], dict_results["CAy"], dict_results["CAz"])
            )
            try:
                from scipy.spatial import cKDTree

                gridtree = cKDTree(indi)
            except ImportError:
                try:
                    from scipy.spatial import KDTree

                    gridtree = KDTree(indi)
                except ImportError:
                    gridtree = None
            list_z_local = []
            if gridtree is not None:
                list_z = []
                for res_l in range(len(list_smoc)):
                    ca_coord = indi[res_l]
                    val = list_smoc[res_l]
                    list_neigh = self.get_indices_sphere(gridtree, ca_coord, dist=10.0)
                    list_smoc_local = array_smoc[list_neigh]
                    z_local = self.calc_z_median(list_smoc_local, val)
                    # res_index = list_neigh.index(l)
                    list_z_local.append(z_local)
                    # list_z.append(z_local < z_cutoff)

        else:
            list_z_local = [0.0] * len(list_z_global)  # when no local z is calculated
            # z = self.calc_z_median(array_smoc)
            # list_z = z < z_cutoff
        if process_name == "fdr":
            list_z = array_smoc < 0.9
        return list_z, list_z_local, list_z_global

    def select_outliers_by_z(self, dict_results, process_name, z_cutoff=-2.5):
        list_smoc = dict_results[process_name]
        array_smoc = np.array(list_smoc)
        # use z = 0 for a very narrow distribution
        if np.median(list_smoc) > 0.6 and np.std(list_smoc) < 0.05:
            list_z_global = [0.0] * len(list_smoc)
        else:
            list_z_global = self.calc_z_median(list_smoc)
        # if coords are available, set z score locally
        if "CAx" in dict_results and "CAy" in dict_results and "CAz" in dict_results:
            indi = list(
                zip(dict_results["CAx"], dict_results["CAy"], dict_results["CAz"])
            )
            try:
                from scipy.spatial import cKDTree

                gridtree = cKDTree(indi)
            except ImportError:
                try:
                    from scipy.spatial import KDTree

                    gridtree = KDTree(indi)
                except ImportError:
                    gridtree = None
            list_z_local = []
            if gridtree is not None:
                list_z = []
                for res_l in range(len(list_smoc)):
                    ca_coord = indi[res_l]
                    val = list_smoc[res_l]
                    list_neigh = self.get_indices_sphere(gridtree, ca_coord, dist=12.0)
                    list_smoc_local = array_smoc[list_neigh]
                    if (
                        np.median(list_smoc_local) > 0.6
                        and np.std(list_smoc_local) < 0.05
                    ):
                        z_local = 0.0
                    else:
                        z_local = self.calc_z_median(list_smoc_local, val)
                    # res_index = list_neigh.index(l)
                    list_z_local.append(z_local)
                    list_z.append(z_local < z_cutoff)

        else:
            list_z_local = [0.0] * len(list_z_global)  # when no local z is calculated
            z = self.calc_z_median(array_smoc)
            list_z = z < z_cutoff
        return list_z, list_z_local, list_z_global

    def calc_z(
        self,
        list_vals: Union[list, np.ndarray],
        val: float = None,
    ) -> Union[float, list]:
        # remove selected value before calculating z
        if val is not None:
            if isinstance(val, list):
                list_vals.remove(val)  # remove val from list
            elif isinstance(val, np.ndarray):
                list_vals = list_vals[list_vals != val]
            else:
                pass
        z: Union[list, float] = 0.0
        sum_mapval = np.sum(list_vals)
        mean_mapval = sum_mapval / len(list_vals)
        # median_mapval = np.median(list_vals)
        std_mapval = np.std(list_vals)
        if val is None:
            if std_mapval == 0.0:
                z = [0.0] * len(list_vals)
            else:
                z = np.round((np.array(list_vals) - mean_mapval) / std_mapval, 2)
        else:
            if std_mapval == 0.0:
                z = 0.0
            else:
                z = (val - mean_mapval) / std_mapval
        return z

    def calc_z_median(
        self,
        list_vals: Union[list, np.ndarray],
        val: float = None,
    ) -> Union[float, list]:
        # remove selected value before calculating z
        if val is not None:
            if isinstance(val, list):
                list_vals.remove(val)  # remove val from list
            elif isinstance(val, np.ndarray):
                list_vals = list_vals[list_vals != val]
            else:
                pass

        # sum_smoc = np.sum(list_vals)
        z: Union[list, float] = 0.0
        median_smoc = np.median(list_vals)
        mad_smoc = np.median(np.absolute(np.array(list_vals) - median_smoc))
        if val is None:
            if mad_smoc == 0.0:
                z = [0.0] * len(list_vals)
            else:
                z = np.around((np.array(list_vals) - median_smoc) / mad_smoc, 2)
        else:
            if mad_smoc == 0.0:
                z = 0.0
            else:
                z = round(((val - median_smoc) / mad_smoc), 2)
        return z

    def get_indices_sphere(
        self,
        gridtree: Type[kdt],
        coord: Union[list, np.ndarray],
        dist: float = 5.0,
    ) -> list:
        list_points = gridtree.query_ball_point([coord[0], coord[1], coord[2]], dist)
        return list_points
