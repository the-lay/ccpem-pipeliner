#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

from collections import OrderedDict
import re
import argparse
import json
import warnings
import os

dict_referencerange = OrderedDict(
    [
        ("Ramachandran_outliers", "< 0.05%"),
        ("Ramachandran_favored", "> 98%"),
        (
            "RamachandranZ_whole",
            "bad |Rama-Z| > 3, suspicious 3 > |Rama-Z| > 2, good |Rama-Z| < 2",
        ),
        (
            "RamachandranZ_helix",
            "bad |Rama-Z| > 3, suspicious 3 > |Rama-Z| > 2, good |Rama-Z| < 2",
        ),
        (
            "RamachandranZ_sheet",
            "bad |Rama-Z| > 3, suspicious 3 > |Rama-Z| > 2, good |Rama-Z| < 2",
        ),
        (
            "RamachandranZ_loop",
            "bad |Rama-Z| > 3, suspicious 3 > |Rama-Z| > 2, good |Rama-Z| < 2",
        ),
        ("Rotamer_outliers", "< 0.3%"),
        ("CBeta_deviations", "0"),
        ("Clashscore", " "),
        ("Molprobity_score", " "),
        ("Cis_proline", "0%"),
        ("Cis_general", "0%"),
        ("Rms_bonds", "< 0.02"),
        ("Rms_angles", "< 2.0"),
    ]
)


class MolprobityLogParser(object):
    """
    Parser for Molprobity stdout
    """

    def __init__(
        self, stdout, model_id="", rama_out=None, rota_out=None, resname_file=None
    ):
        self.stdout = stdout
        self.dict_summary_raw = {}
        self.dict_summary = OrderedDict()
        self.dict_outliers = OrderedDict()
        self.dict_res_outliers = OrderedDict()
        self.dict_outliers_headers = {}
        self.summary_start = False
        self.outlier_start = False
        self.dict_stat_len = {
            "dihedral_angles": 4,
            "bond_lengths": 2,
            "bond_angles": 3,
            "chiral_volumes": None,
            "planar_groups": None,
            "sugar_pucker": 3,
            "backbone_torsion_suites": 3,
        }
        self.model_id = model_id
        self.generate_iris_data = True
        self.rama_out = rama_out
        self.rota_out = rota_out
        if not rama_out and not rota_out:
            warnings.warn(
                "Ramalyze and Rotalyze results not found, "
                "skipping Molprobity from Iris data"
            )
            self.generate_iris_data = False
        elif self.rama_out:
            self.get_ramalyze_outliers()
        if self.rota_out:
            self.get_rotalyze_outliers()
        if resname_file:
            with open(resname_file, "r") as j:
                self.dict_residue_names = json.load(j)

        stat = None
        stdout_fh = open(self.stdout, "r")
        # parse the lines
        for line in stdout_fh:
            self.is_summary(line)
            self.is_ramaZ(line)
            self.is_outlier(line)
            if self.summary_start:
                self.get_summary(line[:-1])
            #                        ----------Bond lengths----------
            if "------" in line and line.strip()[-2] == "-":
                stat = line.strip()
                stat = stat.strip("-")
                stat = "_".join([s.strip().lower() for s in stat.split()])
            if self.outlier_start and stat is not None:
                if len(line.strip()) > 0:
                    self.get_outliers(line, stat)
        stdout_fh.close()
        if self.generate_iris_data:
            self.generate_iris_data_summary()
            self.generate_iris_data_residue()

    def is_ramaZ(self, line):
        line_split = line.split()
        if not self.summary_start or len(line_split) == 0:
            self.ramaz_start = False
        else:
            if line_split[0].lower() == "rama-z":
                self.ramaz_start = True
            if (
                line[0] == "="
                and line_split[1].lower() == "summary"
                and line[-2] == "="
            ):
                self.ramaz_start = False

    def get_summary_value(self, line):

        if "=" in line:
            # Rotamer outliers      =   0.00 %
            return line.split("=")[1].strip()
        elif ":" in line:
            # whole: -5.68 (0.22), residues: 558
            return ":".join(line.split(":")[1:]).strip()

    def get_ramaz_values(self, line):
        ramaz_str = self.get_summary_value(line)
        list_ramaz = self.extract_numeric_from_string(ramaz_str)
        return list_ramaz

    def extract_numeric_from_string(self, string):
        return re.findall(r"[-+]?(?:\d*\.*\d+)", string)

    def get_percentile(self, line):
        percentile_value = line.split("(percentile:")[1]
        percentile_value = percentile_value.split()[0].strip()
        if percentile_value[-1] == ")":
            percentile_value = percentile_value[:-1]
        return percentile_value

    def get_summary(self, line):
        """
        Get global molprobity statistics from log
        NOTE that the pattern search is based on the log output
        """
        if self.ramaz_start:
            if "whole:" in line:
                # -5.68 (0.22), residues: 558
                self.dict_summary_raw["RamachandranZ_whole"] = [
                    self.get_ramaz_values(line)[0],
                    dict_referencerange["RamachandranZ_whole"],
                ]
            elif "helix:" in line:
                # -5.68 (0.22), residues: 558
                self.dict_summary_raw["RamachandranZ_helix"] = [
                    self.get_ramaz_values(line)[0],
                    dict_referencerange["RamachandranZ_helix"],
                ]
            elif "sheet:" in line:
                # -5.68 (0.22), residues: 558
                self.dict_summary_raw["RamachandranZ_sheet"] = [
                    self.get_ramaz_values(line)[0],
                    dict_referencerange["RamachandranZ_sheet"],
                ]
            elif "loop :" in line:
                # -5.68 (0.22), residues: 558
                self.dict_summary_raw["RamachandranZ_loop"] = [
                    self.get_ramaz_values(line)[0],
                    dict_referencerange["RamachandranZ_loop"],
                ]
        elif "Ramachandran outliers" in line and "=" in line:
            stat_str = self.get_summary_value(line)
            self.dict_summary_raw["Ramachandran_outliers"] = [
                str(self.extract_numeric_from_string(stat_str)[0]) + "%",
                dict_referencerange["Ramachandran_outliers"],
            ]
        elif "favored" in line and "=" in line:
            stat_str = self.get_summary_value(line)
            self.dict_summary_raw["Ramachandran_favored"] = [
                str(self.extract_numeric_from_string(stat_str)[0]) + "%",
                dict_referencerange["Ramachandran_favored"],
            ]
        elif "Rotamer outliers" in line and "=" in line:
            stat_str = self.get_summary_value(line)
            self.dict_summary_raw["Rotamer_outliers"] = [
                str(self.extract_numeric_from_string(stat_str)[0]) + "%",
                dict_referencerange["Rotamer_outliers"],
            ]
        elif "Clashscore" in line and "=" in line:
            stat_str = self.get_summary_value(line)
            self.dict_summary_raw["Clashscore"] = [
                self.extract_numeric_from_string(stat_str)[0],
                str(self.get_percentile(stat_str)) + " (percentile)",
            ]
        elif "C-beta deviations" in line and "=" in line:
            stat_str = self.get_summary_value(line)
            self.dict_summary_raw["CBeta_deviations"] = [
                self.extract_numeric_from_string(stat_str)[0],
                dict_referencerange["CBeta_deviations"],
            ]
        elif "RMS(bonds)" in line and "=" in line:
            stat_str = self.get_summary_value(line)
            self.dict_summary_raw["Rms_bonds"] = [
                self.extract_numeric_from_string(stat_str)[0],
                dict_referencerange["Rms_bonds"],
            ]
        elif "RMS(angles)" in line and "=" in line:
            stat_str = self.get_summary_value(line)
            self.dict_summary_raw["Rms_angles"] = [
                self.extract_numeric_from_string(stat_str)[0],
                dict_referencerange["Rms_angles"],
            ]
        elif "MolProbity score" in line and "=" in line:
            stat_str = self.get_summary_value(line)
            self.dict_summary_raw["Molprobity_score"] = [
                self.extract_numeric_from_string(stat_str)[0],
                self.get_percentile(stat_str) + " (percentile)",
            ]
        elif "Cis-proline" in line and ":" in line:
            stat_str = self.get_summary_value(line)
            self.dict_summary_raw["Cis_proline"] = [
                self.extract_numeric_from_string(stat_str)[0],
                dict_referencerange["Cis_proline"],
            ]
        elif "Cis-general" in line and ":" in line:
            stat_str = self.get_summary_value(line)
            self.dict_summary_raw["Cis_general"] = [
                self.extract_numeric_from_string(stat_str)[0],
                dict_referencerange["Cis_general"],
            ]

    def is_summary(self, line):

        line_split = line.strip().split()
        # Molprobity Statistics.
        try:
            if (
                len(line_split) == 2
                and line_split[0].lower() == "molprobity"
                and line_split[1].lower() == "statistics."
            ):
                self.summary_start = True
                self.outlier_start = False
        except IndexError:
            pass
        # =============== Molprobity validation ================
        # This start includes bond lengths and angles deviation
        # if (
        #     line[0] == "="
        #     and line.split()[1].lower() == "molprobity"
        #     and line.split()[2].lower() == "validation"
        #     and line[-2] == "="
        # ):
        #     self.summary_start = True
        #     self.outlier_start = False
        # ================== Summary ==================
        if (
            line[0] == "="
            and line_split[1].lower() == "summary"
            and line.strip()[-2] == "="
        ):
            self.summary_start = True
            self.outlier_start = False

        try:
            if line_split[0] == "Results" or line_split[0] == "Refinement":
                self.summary_start = False
        except IndexError:
            pass

    def is_outlier(self, line):
        line_split = line.strip().split()
        if line[0] == "=" and line_split[1] == "Geometry":
            self.outlier_start = True
            self.summary_start = False
        if "------" in line and line.strip()[-2] == "-":
            self.summary_start = False
            self.outlier_start = True
            self.ramaz_start = False
        if (
            line[0] == "="
            and line_split[1].lower() == "molprobity"
            and line.strip()[-2] == "="
        ):
            self.outlier_start = False

    def get_outlier_id(self, line):
        id_split = line[:19].strip().split()
        return id_split

    def get_outlier_atoms(self, line):
        chainID = line[2:4].strip()
        resnum = line[4:9].strip()
        # resname = line[10:13].strip()
        atom_name = line[15:18].strip()
        return chainID, resnum, atom_name

    def get_outlier_residue(self, line):
        # resname = line[11:13].strip()
        resnum = line[4:8].strip()
        chainID = line[2:4].strip()
        return chainID, resnum

    def get_outlier_suite(self, line):
        chainID = line[6:8].strip()
        resnum = line[9:13].strip()
        # resname = line[3:6].strip()
        return chainID, resnum

    def skip_outlier_lines(self, line, measure):
        list_id_types = ["atoms", "residue", "Suite"]
        line_split = line.strip().split()
        if line_split[0] in list_id_types:
            self.dict_outliers_headers[measure] = line.strip().split()
            return True
        if line[2] != " " or len(line_split) < 3 or "delta:" in line:
            return True
        if (
            measure not in self.dict_outliers_headers
            or len(self.dict_outliers_headers[measure]) == 0
        ):  # asn/gln/his_flips
            return True
        return False

    def get_outlier_id_from_line(self, line, measure):
        # parse ids and values
        if self.dict_outliers_headers[measure][0] == "atoms":
            chainID, resnum, atom_name = self.get_outlier_atoms(line)
            outlier_id = "_".join(["1", chainID, resnum, atom_name])
            return chainID, resnum, outlier_id
        elif self.dict_outliers_headers[measure][0] == "residue":
            if not line[:3] == "   ":
                return
            chainID, resnum = self.get_outlier_residue(line)
            outlier_id = "_".join(["1", chainID, resnum])
            return chainID, resnum, outlier_id
        elif self.dict_outliers_headers[measure][0] == "Suite":
            if not line[:3] == "   ":
                return
            chainID, resnum = self.get_outlier_suite(line)
            outlier_id = "_".join(["1", chainID, resnum])
            return chainID, resnum, outlier_id
        else:
            # TODO: any other outlier types?
            return

    def get_outliers(self, line, measure):
        if self.skip_outlier_lines(line, measure):
            return
        line_split = line.strip().split()
        # parse ids and values
        outlier_details = self.get_outlier_id_from_line(line, measure)
        if not outlier_details:
            return
        chainID, resnum, outlier_id = outlier_details

        try:
            if chainID not in self.dict_res_outliers["1"]:
                self.dict_res_outliers["1"][chainID] = {}
        except KeyError:
            self.dict_res_outliers["1"] = {chainID: {}}
        try:
            self.dict_outliers[measure][-1].append(outlier_id)
        except KeyError:
            self.dict_outliers[measure] = [[outlier_id]]
        if len(line_split) > 4:

            sigma = ""
            if "sigma" in line_split[-1]:
                sigma = line_split[-1]
            if self.dict_outliers_headers[measure][0] == "atoms":
                atom_list = "-".join(self.dict_outliers[measure][-1])
                list_outlier_details = [measure, atom_list, sigma]
            else:
                list_outlier_details = [measure, sigma]
            # self.dict_res_outliers["1"][chainID][resnum] =
            # [[measure1,sigma1],[measure2,""]]
            try:
                if measure not in self.dict_res_outliers["1"][chainID][resnum]:
                    self.dict_res_outliers["1"][chainID][resnum].append(
                        list_outlier_details
                    )
            except KeyError:
                self.dict_res_outliers["1"][chainID][resnum] = [list_outlier_details]

            # self.dict_outliers[measure] = [[atom1,atom2,..],[atom7,atom8,..]]
            try:
                # new atom set
                self.dict_outliers[measure].append([])
            except KeyError:
                self.dict_outliers[measure] = [[]]

    def remove_empty_outliers(self):
        for stat in self.dict_outliers:
            if self.dict_outliers[stat][-1] == []:
                try:
                    self.dict_outliers[stat] = self.dict_outliers[stat][:-1]
                except IndexError:
                    del self.dict_outliers[stat]

    def get_ramalyze_outliers(self):
        if "1" not in self.dict_res_outliers:
            self.dict_res_outliers["1"] = OrderedDict()
        self.dict_ramalyze = OrderedDict()
        assert os.path.isfile(self.rama_out)
        with open(self.rama_out, "r") as o:
            for line in o:
                if line[:7] == "residue":
                    continue
                lsplit = line.strip().split(":")
                # A   2  LEU:0.81:-166.05:110.32:Allowed:General
                try:
                    chain, resnum, resname = lsplit[0].split()
                except ValueError:
                    continue
                if chain not in self.dict_ramalyze:
                    self.dict_ramalyze[chain] = OrderedDict()
                self.dict_ramalyze[chain][resnum] = lsplit[-2]
                if chain not in self.dict_res_outliers["1"]:
                    self.dict_res_outliers["1"][chain] = OrderedDict()
                # save outliers
                if lsplit[-2].lower() == "outlier":
                    res_id = "_".join(["1", chain, str(resnum)])
                    list_outlier_details = [
                        "ramachandran_outlier",
                        res_id,
                        "phi:" + lsplit[2] + "-" + "psi:" + lsplit[3],
                    ]
                    measure = "ramachandran_outlier"
                    try:
                        if measure not in self.dict_res_outliers["1"][chain][resnum]:
                            self.dict_res_outliers["1"][chain][resnum].append(
                                list_outlier_details
                            )
                    except KeyError:
                        self.dict_res_outliers["1"][chain][resnum] = [
                            list_outlier_details
                        ]

    def get_rotalyze_outliers(self):
        self.dict_rotalyze = OrderedDict()
        assert os.path.isfile(self.rota_out)
        with open(self.rota_out, "r") as o:
            for line in o:
                if line[:7] == "residue":
                    continue
                lsplit = line.strip().split(":")
                # A   2  LEU:0.81:-166.05:110.32:Allowed:General
                try:
                    chain, resnum, resname = lsplit[0].split()
                except ValueError:
                    continue
                if chain not in self.dict_rotalyze:
                    self.dict_rotalyze[chain] = OrderedDict()
                self.dict_rotalyze[chain][resnum] = lsplit[-2]
                # save outliers
                if lsplit[-2].lower() == "outlier":
                    res_id = "_".join(["1", chain, str(resnum)])
                    list_outlier_details = [
                        "rotamer_outlier",
                        res_id,
                        "chi1:"
                        + lsplit[3]
                        + "-"
                        + "chi2:"
                        + lsplit[4]
                        + "-"
                        + "chi3:"
                        + lsplit[5]
                        + "-"
                        + "chi4:"
                        + lsplit[6],
                    ]
                    measure = "rotamer_outlier"
                    try:
                        if measure not in self.dict_res_outliers["1"][chain][resnum]:
                            self.dict_res_outliers["1"][chain][resnum].append(
                                list_outlier_details
                            )
                    except KeyError:
                        self.dict_res_outliers["1"][chain][resnum] = [
                            list_outlier_details
                        ]

    def generate_iris_data_summary(self):
        """
        Maps results onto a dictionary format that Iris display expects
        """
        self.iris_data = {}
        self.iris_data["model_wide"] = {}
        self.iris_data["model_wide"]["summary"] = OrderedDict()
        self.iris_data["model_wide"]["summary"]["cbeta_deviations"] = float(
            self.dict_summary_raw["CBeta_deviations"][0]
        )
        self.iris_data["model_wide"]["summary"]["clashscore"] = float(
            self.dict_summary_raw["Clashscore"][0]
        )
        self.iris_data["model_wide"]["summary"]["ramachandran_outliers"] = float(
            self.dict_summary_raw["Ramachandran_outliers"][0][:-1]
        )  # remove%
        self.iris_data["model_wide"]["summary"]["ramachandran_favoured"] = float(
            self.dict_summary_raw["Ramachandran_favored"][0][:-1]
        )  # remove%
        self.iris_data["model_wide"]["summary"]["rms_bonds"] = float(
            self.dict_summary_raw["Rms_bonds"][0]
        )
        self.iris_data["model_wide"]["summary"]["rms_angles"] = float(
            self.dict_summary_raw["Rms_angles"][0]
        )
        self.iris_data["model_wide"]["summary"]["rotamer_outliers"] = float(
            self.dict_summary_raw["Rotamer_outliers"][0][:-1]
        )  # remove%
        self.iris_data["model_wide"]["summary"]["molprobity_score"] = float(
            self.dict_summary_raw["Molprobity_score"][0]
        )

    def get_int(self, n, prefix_msg="", suffix_msg=""):
        try:
            resnum = int(n)
        except (TypeError, ValueError) as e:
            if hasattr(e, "message"):
                warnings.warn(
                    prefix_msg
                    + "{} not an int".format(n)
                    + suffix_msg
                    + ":{}".format(e.message)
                )
            else:
                warnings.warn(
                    prefix_msg
                    + "{} not an int".format(n)
                    + suffix_msg
                    + ":{}".format(e.message)
                )
            raise ValueError
        return resnum

    def generate_iris_data_residue(self):
        """
        Maps per-residue results onto a dictionary format that Iris display expects
        """
        for c in self.dict_ramalyze:
            self.iris_data[c] = OrderedDict()
            for n in self.dict_ramalyze[c]:
                try:
                    resnum = self.get_int(n)
                except ValueError:
                    continue
                self.iris_data[c][resnum] = {
                    "clash": 2,
                    "c-beta": None,
                    "nqh_flips": None,
                    "omega": None,
                    "ramachandran": 2,
                    "rotamer": 2,
                    "cmo": None,
                }
                try:
                    # {"1": {"A": {"201": [["dihedral_angles",
                    # "1_A_201_C1A:1_A_201_C2A:1_A_201_CAA:1_A_201_CBA", "4.3*sigma"]]}
                    for outliers in self.dict_res_outliers["1"][c][str(resnum)]:
                        outlier_type = outliers[0]
                        if "clash" in outlier_type.lower():
                            self.iris_data[c][resnum]["clash"] = 0
                        # TODO : add other outliers here?
                except KeyError:
                    pass
                self.set_iris_rama_outlierflag(c, n, resnum)
                self.set_iris_rota_outlierflag(c, n, resnum)
            # default value to fill if resnum dont match with Iris numbering
            self.iris_data[c]["fill"] = {
                "clash": 2,
                "c-beta": None,
                "nqh_flips": None,
                "omega": None,
                "ramachandran": 2,
                "rotamer": 2,
                "cmo": None,
            }

    def set_iris_rama_outlierflag(self, c: str, n: str, resnum: int):
        try:
            rama_category = self.dict_ramalyze[c][n].lower()
            if rama_category == "outlier":
                self.iris_data[c][resnum]["ramachandran"] = 0
            elif rama_category == "allowed":
                self.iris_data[c][resnum]["ramachandran"] = 1
        except ValueError:
            pass

    def set_iris_rota_outlierflag(self, c: str, n: str, resnum: int):
        try:
            rota_category = self.dict_rotalyze[c][n].lower()
            if rota_category == "outlier":
                self.iris_data[c][resnum]["rotamer"] = 0
            elif rota_category == "allowed":
                self.iris_data[c][resnum]["rotamer"] = 1
        except (KeyError, ValueError) as e:
            print("Rotalyze output data not found for {}, {}".format(n, e))

    def save_outputs(self):
        self.remove_empty_outliers()
        if self.model_id:
            out_json = self.model_id + "_molprobity_summary.json"
        else:
            out_json = "molprobity_summary.json"
        # reorder the summary table
        for stat in dict_referencerange:
            self.dict_summary[stat] = self.dict_summary_raw[stat]
        with open(out_json, "w") as j:
            json.dump(self.dict_summary, j)
        if self.model_id:
            out_json = self.model_id + "_residue_molprobity_outliers.json"
        else:
            out_json = "residue_molprobity_outliers.json"
        with open(out_json, "w") as j:
            json.dump(self.dict_res_outliers, j)
        if self.model_id:
            out_json = self.model_id + "_molprobity_outliers.json"
        else:
            out_json = "molprobity_outliers.json"
        with open(out_json, "w") as j:
            json.dump(self.dict_outliers, j)
        if self.generate_iris_data:
            dict_iris_data = {}
            if self.model_id:
                out_json = self.model_id + "_molprobity_iris.json"
                dict_iris_data["molprobity"] = self.iris_data
            else:
                out_json = "molprobity_iris.json"
                dict_iris_data["molprobity"] = self.iris_data
            with open(out_json, "w") as j:
                json.dump(dict_iris_data, j)


def parse_args():
    parser = argparse.ArgumentParser(description="get SMOC score results")
    parser.add_argument(
        "-molp",
        "--molp",
        required=True,
        help="Input molprobity output file (.out, .txt)",
    )
    parser.add_argument(
        "-rama",
        "--rama",
        required=False,
        help="Input ramalyze output file (.out, .txt)",
    )
    parser.add_argument(
        "-rota",
        "--rota",
        required=False,
        help="Input rotalyze output file (.out, .txt)",
    )
    parser.add_argument(
        "-resname",
        "--resname",
        required=False,
        help="Input c-alpha coords (.json)",
    )
    parser.add_argument(
        "-odir",
        "--odir",
        required=False,
        default=None,
        help="Output directory",
    )
    parser.add_argument(
        "-id",
        "--id",
        required=False,
        default="pdbid",
        help="ID used to save results json file",
    )

    return parser.parse_args()


def main():
    args = parse_args()
    # read input
    molprobity_out = args.molp
    model_id = args.id
    mp = MolprobityLogParser(
        molprobity_out,
        model_id=model_id,
        rama_out=args.rama,
        rota_out=args.rota,
    )
    mp.save_outputs()


if __name__ == "__main__":
    main()
