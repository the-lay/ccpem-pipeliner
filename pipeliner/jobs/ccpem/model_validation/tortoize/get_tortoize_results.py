#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import argparse
import json
import warnings


def parse_args():
    parser = argparse.ArgumentParser(description="get SMOC score results")
    parser.add_argument(
        "-tortoize",
        "--tortoize",
        required=True,
        help="Input tortoize scores (.json)",
    )

    parser.add_argument(
        "-odir",
        "--odir",
        required=False,
        default=None,
        help="Output directory",
    )
    parser.add_argument(
        "-id",
        "--id",
        required=False,
        default="pdbid",
        help="ID used to save results json file",
    )

    return parser.parse_args()


def parse_tortoize_json(
    tortoize_json: str,
    model_id: str,
):

    # Get data
    dict_tortoize: dict = {}
    dict_summary: dict = {}

    with open(tortoize_json, "r") as j:
        tortoize_scores = json.load(j)
    for model in tortoize_scores["model"]:
        dict_tortoize[model] = {}
        model_torsion_sd = tortoize_scores["model"][model]["torsion-jackknife-sd"]
        model_torsion_z = tortoize_scores["model"][model]["torsion-z"]
        model_rama_sd = tortoize_scores["model"][model]["ramachandran-jackknife-sd"]
        model_rama_z = tortoize_scores["model"][model]["ramachandran-z"]
        for resdata in tortoize_scores["model"][model]["residues"]:
            resnum = resdata["seqID"]
            try:
                chain = resdata["pdb"]["strandID"]
            except KeyError:
                chain = resdata["asymID"]
            if chain not in dict_tortoize[model]:
                dict_tortoize[model][chain] = {}
            try:
                sstype = resdata["ramachandran"]["ss-type"]
                rama_z = resdata["ramachandran"]["z-score"]
            except KeyError:
                print("Tortoize: No ramachandran scores for {}{}".format(resnum, chain))
                continue
            try:
                # torsion_sstype = resdata["torsion"]["ss-type"]
                torsion_z = resdata["torsion"]["z-score"]
            except KeyError:
                continue
            dict_tortoize[model][chain][resnum] = [sstype, rama_z, torsion_z]
        if model not in dict_summary:
            dict_summary[model] = {}
        dict_summary[model]["Ramachandran-jackknife-sd"] = [model_rama_sd, ""]
        dict_summary[model]["Ramachandran-z"] = [
            model_rama_z,
            "bad: |Rama-Z|>3; unlikely: 2<|Rama-Z|<3; good: |Rama-Z|<2",
        ]
        dict_summary[model]["Torsion-jackknife-sd"] = [model_torsion_sd, ""]
        dict_summary[model]["Torsion-z"] = [
            model_torsion_z,
            "bad: |Torsion-Z|>3; unlikely: 2<|Torsion-Z|<3; good: |Torsion-Z|<2",
        ]
    out_json = model_id + "_residue_tortoize.json"
    with open(out_json, "w") as j:
        json.dump(dict_tortoize, j)

    out_global_json = model_id + "_tortoize_summary.json"
    with open(out_global_json, "w") as j:
        json.dump(dict_summary, j)

    generate_iris_data(dict_tortoize, model_id)


def get_int(n, prefix_msg="", suffix_msg=""):
    try:
        resnum = int(n)
    except (TypeError, ValueError) as e:
        if hasattr(e, "message"):
            warnings.warn(
                prefix_msg
                + "{} not an int".format(n)
                + suffix_msg
                + ":{}".format(e.message)
            )
        else:
            warnings.warn(
                prefix_msg
                + "{} not an int".format(n)
                + suffix_msg
                + ":{}".format(e.message)
            )
        raise ValueError
    return resnum


def generate_iris_data(dict_tortoize, model_id=None):
    dict_iris_data = {}
    dict_ramaz_data = {}
    dict_torsionz_data = {}
    for m in dict_tortoize:
        for c in dict_tortoize[m]:
            if c not in dict_ramaz_data:
                dict_ramaz_data[c] = {}
                dict_torsionz_data[c] = {}
            for n in dict_tortoize[m][c]:
                try:
                    resnum = get_int(n)
                except ValueError:
                    continue
                dict_ramaz_data[c][resnum] = dict_tortoize[m][c][n][1]
                dict_torsionz_data[c][resnum] = dict_tortoize[m][c][n][2]
            # default fill values for Iris
            dict_ramaz_data[c]["fill"] = 0.0
            dict_torsionz_data[c]["fill"] = 0.0
    dict_iris_data["rama_z"] = dict_ramaz_data
    dict_iris_data["torsion_z"] = dict_torsionz_data
    if model_id:
        out_json = model_id + "_tortoize_iris.json"
    else:
        out_json = "tortoize_iris.json"
    with open(out_json, "w") as j:
        json.dump(dict_iris_data, j)


def main():
    args = parse_args()
    # read input
    tortoize_out = args.tortoize
    model_id = args.id
    parse_tortoize_json(tortoize_json=tortoize_out, model_id=model_id)


if __name__ == "__main__":
    main()
