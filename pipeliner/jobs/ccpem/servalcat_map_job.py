#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram
from pipeliner.job_options import (
    InputNodeJobOption,
    FloatJobOption,
    files_exts,
)
from pipeliner.node_factory import create_node
from pipeliner.pipeliner_job import Ref
from pipeliner.display_tools import make_map_model_thumb_and_display
from pipeliner.utils import run_subprocess
from pipeliner.nodes import (
    NODE_DENSITYMAP,
    NODE_ATOMCOORDS,
    NODE_LIGANDDESCRIPTION,
    NODE_MASK3D,
)


class DifferenceMap(PipelinerJob):
    PROCESS_NAME = "servalcat.map_analysis.difference_map"
    OUT_DIR = "ServalcatMap"

    def __init__(self):
        super(self.__class__, self).__init__()
        self.vers_com = (["servalcat", "--version"], [])
        self.jobinfo.display_name = " Servalcat"
        self.jobinfo.short_desc = "Map calculation"
        self.jobinfo.long_desc = (
            "Standalone Servalcat functions that do not require Refmac"
        )
        self.jobinfo.programs = [
            ExternalProgram(
                command="servalcat", vers_com=["servalcat", "--version"], vers_lines=[0]
            ),
        ]
        self.version = "0.1"
        self.job_author = "Tom Burnley"
        self.jobinfo.references = [
            Ref(
                authors=["Yamashita K", "Palmer CM", "Burnley T", "Murshudov GN"],
                title=(
                    "Cryo-EM single particle structure refinement and map "
                    "calculation using Servalcat."
                ),
                journal="Acta Cryst. D",
                year="2022",
                volume="77",
                issue="1",
                pages="1282-1291",
                doi="10.1107/S2059798321009475",
            )
        ]
        self.jobinfo.documentation = "https://github.com/keitaroyam/servalcat"
        self.joboptions["input_model"] = InputNodeJobOption(
            label="Input model",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".pdb"]),
            default_value="",
            directory="",
            help_text="The input model to be refined",
            is_required=True,
        )
        self.joboptions["input_ligand"] = InputNodeJobOption(
            label="Input ligand",
            node_type=NODE_LIGANDDESCRIPTION,
            pattern=files_exts("Ligand definition", [".cif"]),
            default_value="",
            directory="",
            help_text="The input model",
        )

        self.joboptions["resolution"] = FloatJobOption(
            label="Resolution",
            default_value=-1,
            suggested_min=0.5,
            suggested_max=20,
            hard_min=0,
            step_value=0.1,
            help_text="Resolution in Angstrom",
            is_required=True,
        )

        self.joboptions["input_half_map1"] = InputNodeJobOption(
            label="Input map 1",
            node_type=NODE_DENSITYMAP,
            default_value="",
            directory="",
            pattern=files_exts("3D map ", [".mrc", ".map"]),
            help_text="Half map 1 from 3D refinement",
            is_required=True,
        )
        self.joboptions["input_half_map2"] = InputNodeJobOption(
            label="Input map 2",
            node_type=NODE_DENSITYMAP,
            default_value="",
            directory="",
            pattern=files_exts("3D map ", [".mrc", ".map"]),
            help_text="Half map 2 from 3D refinement",
            is_required=True,
        )
        self.joboptions["input_mask"] = InputNodeJobOption(
            label="Input mask",
            node_type=NODE_MASK3D,
            default_value="",
            directory="",
            pattern=files_exts("3D map ", [".mrc", ".map"]),
            help_text="The input mask required for difference map calculation",
        )
        self.get_runtab_options()

    def get_commands(self):
        # Run in the job output directory
        self.working_dir = self.output_dir

        # servalcat fofc
        # --model refined_omit.pdb
        # --halfmaps ../emd_9937_half_map_1.map.gz ../emd_9937_half_map_2.map.gz
        # --mask  ../emd_9937_msk_1.map
        # --resolution 2.9 --normalized_map
        # -o diffmap_omit

        command = [self.jobinfo.programs[0].command]

        # Get parameters
        input_half_map1 = self.joboptions["input_half_map1"].get_string(
            True, "Input file missing"
        )
        input_half_map2 = self.joboptions["input_half_map2"].get_string(
            True, "Input file missing"
        )
        input_model = self.joboptions["input_model"].get_string(
            True, "Input file missing"
        )
        resolution = self.joboptions["resolution"].get_number()
        input_mask = self.joboptions["input_mask"].get_string(
            True, "Input file missing"
        )

        # Set command parameters
        command += ["fofc"]

        command += ["--model", os.path.relpath(input_model, self.working_dir)]
        command += [
            "--halfmaps",
            os.path.relpath(input_half_map1, self.working_dir),
            os.path.relpath(input_half_map2, self.working_dir),
        ]
        command += ["--mask", os.path.relpath(input_mask, self.working_dir)]
        command += ["--resolution", str(resolution)]

        output_prefix = "diffmap"
        # Make optional at later date?
        command += ["--normalized_map", "-o", output_prefix]

        commands = [command]
        return commands

    def post_run_actions(self):
        final_mtz = os.path.join(self.output_dir, "diffmap.mtz")
        dmmrc_path = os.path.join(self.output_dir, "diffmap.mrc")
        # TODO: consider running this as an additional command in the main job
        run_subprocess(["gemmi", "sf2map", final_mtz, dmmrc_path])
        self.output_nodes.append(
            create_node(
                os.path.join(self.output_dir, "diffmap.mrc"),
                NODE_DENSITYMAP,
                ["servalcat", "diffmap"],
            )
        )

    def gather_metadata(self):

        metadata_dict = {}

        outfile = os.path.join(self.output_dir, "run.out")
        with open(outfile, "r") as of:
            outlines = of.readlines()

        for line in outlines:
            if "sharpening method" in line:
                metadata_dict["SharpeningMethod"] = line.split(":")[1]
            if "Whole volume" in line:
                metadata_dict["WholeVolume"] = int(line.split()[2])
            if "Masked volume" in line:
                metadata_dict["MaskedVolume"] = int(line.split()[2])
            if "Global mean" in line:
                metadata_dict["GlobalMean"] = float(line.split()[2])
            if "Global std" in line:
                metadata_dict["GlobalStd"] = float(line.split()[2])
            if "Masked mean" in line:
                metadata_dict["MaskedMean"] = float(line.split()[2])
            if "Masked std" in line:
                metadata_dict["MaskedStd"] = float(line.split()[2])

        return metadata_dict

    def create_results_display(self):
        thumbdir = os.path.join(self.output_dir, "Thumbnails")
        if not os.path.isdir(thumbdir):
            os.makedirs(thumbdir)
        models = self.joboptions["input_model"].get_string()
        return [
            make_map_model_thumb_and_display(
                maps=[os.path.join(self.output_dir, "diffmap.mrc")],
                maps_opacity=[0.5],
                title="FoFc difference map",
                models=[models],
                outputdir=self.output_dir,
                start_collapsed=False,
            )
        ]
