#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
from glob import glob
from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram
from pipeliner.job_options import (
    InputNodeJobOption,
    MultipleChoiceJobOption,
    files_exts,
)
from pipeliner.node_factory import create_node
from pipeliner.display_tools import make_map_model_thumb_and_display
from pipeliner.nodes import NODE_ATOMCOORDS, NODE_DENSITYMAP


class SliceNDice(PipelinerJob):
    PROCESS_NAME = "slicendice.fit_model"
    OUT_DIR = "SliceNDice"

    def __init__(self):
        super().__init__()
        self.jobinfo.display_name = "Slice'N'Dice"
        self.jobinfo.short_desc = "Model fitting pipeline for predicted models"
        self.jobinfo.long_desc = (
            "Model fitting pipeline for predicted models. N.B. requires CCP4."
        )
        self.jobinfo.programs = [ExternalProgram("slicendice")]
        self.version = "0.1"
        self.job_author = "Tom Burnley"
        # XXX Todo add ref when available
        # self.jobinfo.references = [
        #     Ref(
        #         authors=["Yamashita K", "Palmer CM", "Burnley T", "Murshudov GN"],
        #         title=(
        #             "Cryo-EM single particle structure refinement and map "
        #             "calculation using Servalcat."
        #         ),
        #         journal="Acta Cryst. D",
        #         year="2022",
        #         volume="77",
        #         issue="1",
        #         pages="1282-1291",
        #         doi="10.1107/S2059798321009475",
        #     )
        # ]
        self.jobinfo.documentation = "https://gitlab.com/rmk65/slicendice"
        self.joboptions["input_model"] = InputNodeJobOption(
            label="Input model",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".pdb"]),
            default_value="",
            directory="",
            help_text="The input model to be refined",
            is_required=True,
        )
        self.joboptions["input_map"] = InputNodeJobOption(
            label="Input map",
            node_type=NODE_DENSITYMAP,
            default_value="",
            directory="",
            pattern=files_exts("3D map", [".mrc"]),
            help_text="The input map to dock the predicted models clusters to",
            is_required=False,
        )
        self.joboptions["xyz_source"] = MultipleChoiceJobOption(
            label="Source",
            choices=["pdb", "alphafold", "rosetta"],
            default_value_index=1,
            help_text="Source of predicted model",
        )

        self.get_runtab_options()

    def get_commands(self):
        # Run in the job output directory
        self.working_dir = self.output_dir

        # mapin is optional, if not supplied slice'n'dice runs in 'slice' mode
        # i.e. model clusters are produced for downstream docking.
        # If mapin supplied then 'dice' mode triggered and statistical docking
        # performed.
        #
        # slicendice -xyzin 7U5C_5_model_1_relaxed_2.pdb -xyz_source alphafold
        #       -mapin emd_26346.map
        command = [self.jobinfo.programs[0].command]

        # Get parameters
        input_model = self.joboptions["input_model"].get_string(
            True, "Input file missing"
        )
        xyz_source = self.joboptions["xyz_source"].get_string()
        input_map = self.joboptions["input_map"].get_string()

        command += ["-xyzin", os.path.relpath(input_model, self.working_dir)]
        command += ["-xyz_source", str(xyz_source)]
        if input_map != "":
            command += ["-mapin", os.path.relpath(input_map, self.working_dir)]

        # Expect at least one pdb output
        pdb = "slicendice_0/split_1/pdb_" + os.path.basename(str(input_model)).replace(
            ".pdb", "_cluster_0.pdb"
        ).replace(".cif", "_cluster_0.pdb")
        self.output_nodes.append(
            create_node(
                os.path.join(self.output_dir, pdb), NODE_ATOMCOORDS, ["slicendice"]
            )
        )
        commands = [command]
        return commands

    def post_run_actions(self):
        # SliceNDice/job998/slicendice_0/split_1/pdb_open_fold_7U5C_5_model_1_relaxed_2_cluster_0.pdb

        # Find all PDB in split output directories and add as node type
        search_pdbs = os.path.join(self.output_dir, "slicendice_0/split_*/*.pdb")
        pdbs = glob(search_pdbs)
        for pdb in sorted(pdbs):
            self.output_nodes.append(create_node(pdb, NODE_ATOMCOORDS, ["slicendice"]))

    def gather_metadata(self):

        metadata_dict = {}

        outfile = os.path.join(self.output_dir, "run.out")
        with open(outfile, "r") as of:
            outlines = of.readlines()

        for i, line in enumerate(outlines):
            if "Splitting" in line:
                metadata_dict["SplitClusters"] = int(line.split()[3])

        return metadata_dict

    def create_results_display(self):
        # For each split_n dir get pdbs and add to map to create multiple display
        # objects (i.e. one per split directory)
        thumbdir = os.path.join(self.output_dir, "Thumbnails")
        if not os.path.isdir(thumbdir):
            os.makedirs(thumbdir)
        input_map = self.joboptions["input_map"].get_string()
        if input_map == "":
            maps = None
            maps_opacity = None
        else:
            maps = [input_map]
            maps_opacity = [0.5]
        # Find split directories and get all output PDBs
        split_dirs = glob(os.path.join(self.output_dir, "slicendice_0/split_*/"))
        split_displays = []
        for split_dir in split_dirs:
            models = []
            for pdb in sorted(glob(os.path.join(split_dir, "*.pdb"))):
                models.append(pdb)
            title = os.path.basename(os.path.normpath(split_dir))
            title = title.replace("split_", "SliceNDice Split ")
            split_displays.append(
                make_map_model_thumb_and_display(
                    title=title,
                    maps=maps,
                    maps_opacity=maps_opacity,
                    models=models,
                    outputdir=self.output_dir,
                )
            )
        return split_displays
