#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os

from pipeliner.pipeliner_job import PipelinerJob, Ref, ExternalProgram
from pipeliner.job_options import (
    MultipleChoiceJobOption,
    InputNodeJobOption,
    IntJobOption,
    files_exts,
)
from pipeliner.node_factory import create_node
from pipeliner.display_tools import (
    make_map_model_thumb_and_display,
)
from pipeliner.scripts.task_utils import molrep_tools
from pipeliner.nodes import NODE_DENSITYMAP, NODE_ATOMCOORDS


class MolrepJob(PipelinerJob):
    """Molecular replacement with Molrep.
    Covers standard rot fn + trans fn, as well as SAPTF. These represent two methods for
    locating search models in the cryoEM map, but have the same input and output nodes
    and so are considered in the same class.
    """

    PROCESS_NAME = "molrep.fit_model"
    OUT_DIR = "Molrep"

    def __init__(self):
        super().__init__()
        self.jobinfo.display_name = "Molrep"
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Martyn Winn"
        self.jobinfo.short_desc = "Molecular replacement with Molrep"
        self.jobinfo.long_desc = (
            "Molrep fits one or more atomic models into a cryoEM map by the technique"
            " of Molecular Replacement. The spherically averaged phased translation "
            "function locates the model in the cryoEM map using a spherically averaged"
            " calculated map, then determines its optimal orientation and refines the "
            "position. The rotation translation function is the more traditional "
            "molecular replacement method. The latter is generally faster but less "
            "sensitive."
        )
        self.jobinfo.documentation = "https://www.ccp4.ac.uk/html/molrep.html"
        self.jobinfo.programs = [
            ExternalProgram("molrep"),
            ExternalProgram("ccpem-python"),
        ]
        self.jobinfo.references = [
            Ref(
                authors=["A.Vagin", "A.Teplyakov"],
                title="Molecular replacement with MOLREP",
                journal="Acta Crystallogr D Biol Crystallogr.",
                year="2010",
                volume="66",
                issue="1",
                pages="22-25",
                doi="10.1107/S0907444909042589",
            ),
            Ref(
                authors=["A.A.Vagin", "M.N.Isupov"],
                title=(
                    "Spherically averaged phased translation function and"
                    " its application to the search for molecules and fragments"
                    " in electron-density maps"
                ),
                journal="Acta Crystallogr D Biol Crystallogr.",
                year="2001",
                volume="57",
                issue="10",
                pages="1451-1456",
                doi="10.1107/s0907444901012409",
            ),
        ]

        self.joboptions["mode"] = MultipleChoiceJobOption(
            label="Mode",
            choices=["SAPTF", "RTF"],
            default_value_index=1,
            help_text="Choice of search methods to use",
        )

        self.joboptions["input_map"] = InputNodeJobOption(
            label="Input map",
            node_type=NODE_DENSITYMAP,
            default_value="",
            directory="",
            pattern=files_exts("3D map", [".mrc", ".map"]),
            help_text="The input cryoEM map into which models will be placed",
            is_required=True,
        )

        self.joboptions["input_model"] = InputNodeJobOption(
            label="Input model",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".pdb", ".ent"]),
            default_value="",
            directory="",
            help_text="The input model to be fitted into map",
            is_required=True,
        )

        self.joboptions["copies"] = IntJobOption(
            label="Copies to find",
            default_value=1,
            suggested_min=1,
            suggested_max=10,
            step_value=1,
            help_text="Number of copies of search model to find",
            in_continue=True,
            is_required=True,
        )

        self.joboptions["subunits"] = IntJobOption(
            label="Subunits in search model",
            default_value=1,
            suggested_min=1,
            suggested_max=10,
            step_value=1,
            help_text=(
                "Number of identical monomers present in search model."
                "Specify for multimers"
            ),
            in_continue=True,
            is_required=True,
        )

        self.joboptions["fixed_model"] = InputNodeJobOption(
            label="Fixed model",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".pdb", ".ent"]),
            default_value="",
            directory="",
            help_text=(
                "Fixed model file (pdb format). This model will be fixed in"
                " its position and orientation during the search."
            ),
            is_required=False,
        )

        self.joboptions["rotation_peaks"] = IntJobOption(
            label="Rotation peaks",
            default_value=1,
            suggested_min=1,
            suggested_max=200,
            step_value=1,
            help_text="The number of peaks from the rotation function to be used",
            in_continue=True,
            is_required=True,
        )

        self.joboptions["translation_peaks"] = IntJobOption(
            label="Translation peaks",
            default_value=1,
            suggested_min=1,
            suggested_max=50,
            step_value=1,
            help_text="The number of peaks from the translation function to be used",
            in_continue=True,
            is_required=True,
        )
        self.get_runtab_options()

    def get_commands(self):
        # Run in the job output directory
        self.working_dir = self.output_dir

        # CCP-EM gui creates:
        #    molrep -m 5me2_a.pdb -f emd_3488.map -i << eof
        #    _NMON 4
        #    _PRF S
        #    NCSM 1
        #    stick n
        #    eof
        input_model_file = self.joboptions["input_model"].get_string(
            True, "Input file missing"
        )
        input_map_file = self.joboptions["input_map"].get_string(
            True, "Input file missing"
        )

        fixed_model_file = self.joboptions["fixed_model"].get_string()

        if input_map_file.endswith(".mrc"):
            input_map_corrected = str(input_map_file).replace(".mrc", ".map")
        else:
            input_map_corrected = str(input_map_file)

        molrep_command = [
            "molrep",
            "-m",
            os.path.relpath(input_model_file, self.working_dir),
            "-f",
            os.path.relpath(input_map_corrected, self.working_dir),
            "-k",
            "keywords.txt",
        ]

        if fixed_model_file != "":
            molrep_command.extend(
                ["-mx", os.path.relpath(fixed_model_file, self.working_dir)]
            )

        # Prepare for Molrep, link any .mrc maps to .map ext. Write keywords
        # file
        tools_script = molrep_tools.__file__
        if tools_script.endswith(".pyc"):
            tools_script.replace(".pyc", ".py")
        prep_com = [
            tools_script,
            "-s",
            os.path.relpath(input_map_file, self.working_dir),
            "-k",
        ]

        # Add keywords
        keywords = ""
        if self.joboptions["mode"].get_string() == "SAPTF":
            keywords += " '_PRF S'"
        ncopies = self.joboptions["copies"].get_number()
        keywords += "_NMON {}\n".format(str(ncopies))
        nsubunits = self.joboptions["subunits"].get_number()
        keywords += "NCSM {}\n".format(str(nsubunits))
        keywords += "stick\n"
        rotation_peaks = self.joboptions["rotation_peaks"].get_number()
        keywords += "np {}\n".format(str(rotation_peaks))
        translation_peaks = self.joboptions["translation_peaks"].get_number()
        keywords += "npt {}\n".format(str(translation_peaks))
        #
        prep_com.append(keywords)

        # Fix Molrep PDB output
        fix_output_com = [tools_script, "-f"]

        output_file = os.path.join(self.output_dir, "molrep.pdb")
        self.output_nodes.append(create_node(output_file, NODE_ATOMCOORDS))

        # Set commands list
        commands = [prep_com, molrep_command, fix_output_com]
        return commands

    def gather_metadata(self):

        metadata_dict = {}

        outfile = os.path.join(self.output_dir, "run.out")
        with open(outfile, "r") as of:
            outlines = of.readlines()

        summary_lines = False
        for i, line in enumerate(outlines):
            if "--- Summary (V2) ---" in line.strip():
                summary_lines = True
            elif "Checking job completion" in line.strip():
                summary_lines = False
            if summary_lines:
                if line.strip().startswith("Nmon"):
                    data_line = outlines[i + 1].split()
                    metadata_dict["Nmon"] = int(data_line[0])
                    metadata_dict["RF"] = int(data_line[1])
                    metadata_dict["TF"] = int(data_line[2])
                    metadata_dict["Theta"] = float(data_line[3])
                    metadata_dict["Phi"] = float(data_line[4])
                    metadata_dict["Chi"] = float(data_line[5])
                    metadata_dict["Tx"] = float(data_line[6])
                    metadata_dict["Ty"] = float(data_line[7])
                    metadata_dict["Tz"] = float(data_line[8])
                    metadata_dict["TFsg"] = float(data_line[9])
                    metadata_dict["wRfac"] = float(data_line[10])
                    metadata_dict["Score"] = float(data_line[11])

        return metadata_dict

    def create_results_display(self):
        thumbdir = os.path.join(self.output_dir, "Thumbnails")
        if not os.path.isdir(thumbdir):
            os.makedirs(thumbdir)
        models = [os.path.join(self.output_dir, "molrep.pdb")]
        fixed_model = self.joboptions["fixed_model"].get_string()
        if fixed_model != "":
            models.append(fixed_model)
        maps = [self.joboptions["input_map"].get_string()]
        return [
            make_map_model_thumb_and_display(
                maps=maps,
                maps_opacity=[0.5],
                models=models,
                title="Molrep docked model",
                outputdir=self.output_dir,
                start_collapsed=False,
            )
        ]
