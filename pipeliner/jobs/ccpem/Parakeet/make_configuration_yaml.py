#
#     Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/

import sys
from typing import Dict, List

import yaml
import argparse


class ParakeetConfigYAML(object):
    def __init__(self, name="config.yaml") -> None:
        # initialise class to hold a dictionary with the same
        # values as parakeet.config.new --full True
        self.config_dict = self.initialise_config_dict()

        self.name = name

    def update_cluster(self, cluster):
        self.config_dict["cluster"] = cluster
        return

    def update_device(self, device):
        self.config_dict["device"] = device
        return

    def update_microscope(self, microscope):
        self.config_dict["microscope"] = microscope
        return

    def update_sample(self, sample):
        self.config_dict["sample"] = sample
        return

    def update_scan(self, scan):
        self.config_dict["scan"] = scan
        return

    def update_simulation(self, simulation):
        self.config_dict["simulation"] = simulation
        return

    def output_yaml(self, outfile):
        with open(outfile, "w") as writefile:
            yaml.dump(self.config_dict, writefile)
        return

    def initialise_config_dict(self):
        config_dict = {}

        # cluster
        config_dict["cluster"] = {"max_workers": 1, "method": None}
        # device
        config_dict["device"] = "gpu"

        # microscope
        config_dict["microscope"] = {
            "beam": {},
            "detector": {},
            "lens": {},
            "model": None,
            "phase_plate": False,
        }
        # microscope->beam
        config_dict["microscope"]["beam"] = {
            "acceleration_voltage_spread": 8.0e-07,
            "electrons_per_angstrom": 45.0,
            "energy": 300.0,
            "energy_spread": 2.66e-06,
            "phi": 0,
            "illumination_semiangle": 0.02,
            "theta": 0,
        }
        # microscope->detector
        config_dict["microscope"]["detector"] = {
            "dqe": False,
            "nx": 1000,
            "ny": 1000,
            "origin": [0, 0],
            "pixel_size": 1.0,
        }
        # microscope->lens
        config_dict["microscope"]["lens"] = {
            "c_10": -20000,
            "c_12": 0,
            "c_21": 0,
            "c_23": 0,
            "c_30": 2.7,
            "c_32": 0,
            "c_34": 0,
            "c_41": 0,
            "c_43": 0,
            "c_45": 0,
            "c_50": 0,
            "c_52": 0,
            "c_54": 0,
            "c_56": 0,
            "c_c": 2.7,
            "current_spread": 3.3e-07,
            "phi_12": 0,
            "phi_21": 0,
            "phi_23": 0,
            "phi_32": 0,
            "phi_34": 0,
            "phi_41": 0,
            "phi_43": 0,
            "phi_45": 0,
            "phi_52": 0,
            "phi_54": 0,
            "phi_56": 0,
        }
        # microscope->model
        # microscope->phase_plate

        # sample
        config_dict["sample"] = {
            "box": [1000, 1000, 500],
            "centre": [500, 500, 250],
            "ice": None,
            "molecules": {},
            "shape": {},
            "sputter": None,
        }
        # sample->box
        # sample->centre
        # sample->ice (slow ice)
        # sample->molecules
        config_dict["sample"]["molecules"] = {
            "pdb": [
                {
                    "id": "4v1w.pdb",
                    "instances": 1,
                },
            ]
        }
        # sample->shape
        config_dict["sample"]["shape"] = {
            "cube": {
                "length": 1000.0,
            },
            "cuboid": {
                "length_x": 1000.0,
                "length_y": 1000.0,
                "length_z": 500.0,
            },
            "cylinder": {
                "length": 1000.0,
                "radius": 500.0,
                "offset_x": None,
                "offset_z": None,
                "axis": [0, 1, 0],
            },
            "margin": [0, 0, 0],
            "type": "cuboid",
        }
        # sample->shape->cube
        # sample->shape->cuboid
        # sample->shape->cylinder
        # sample->shape->margin
        # sample->shape->type
        # sample->sputter (not supported)

        # scan (not supported)
        config_dict["scan"] = {
            "angles": None,
            "axis": [0, 1, 0],
            "drift": None,
            "exposure_time": 1,
            "mode": "still",
            "num_images": 1,
            "positions": None,
            "start_angle": 0,
            "start_pos": 0,
            "step_angle": 0,
            "step_pos": "auto",
        }
        # scan->angles (not supported)
        # scan->axis
        # scan->exposure_time (not supported)
        # scan->mode
        # scan->num_images (not supported)
        # scan->positions (not supported)
        # scan->start_angle
        # scan->start_pos
        # scan->step_pos

        # simulation
        config_dict["simulation"] = {
            "division_thickness": 100,
            "ice": False,
            "inelastic_model": None,
            "margin": 100,
            "mp_loss_position": "peak",
            "mp_loss_width": 50,
            "padding": 100,
            "radiation_damage_model": False,
            "sensitivity_coefficient": 0.022,
            "slice_thickness": 3.0,
        }
        # simulation->division_thickness (not supported)
        # simulation->ice (fast ice)
        # simulation->inelastic_model
        # simulation->margin
        # simulation->mp_loss_position
        # simulation->mp_loss_width
        # simulation->padding
        # simulation->radiation_damage_model
        # simulation->sensitivity_coefficient
        # simulation->slice_thickness
        return config_dict


def determine_molecules(args) -> Dict[str, List[Dict]]:
    molecules_list = []
    # use pdb_filepaths if they are filled
    if args.pdb_filepaths:
        molecules_list = args.pdb_filepaths

    # grab the instances list and convert to int from string
    instances_list = args.pdb_instances
    for instance in instances_list:
        instance = int(instance)
    # check you have a number of instances for each molecule used
    assert len(molecules_list) > 0
    assert len(molecules_list) == len(instances_list)

    # label either as "id or "filename" depending on whether
    # pdb is pdb or local
    molecules_outgoing_list = []
    if args.pdb_source == "local":
        for (molecule, instances) in zip(molecules_list, instances_list):
            molecules_outgoing_list.append(
                {
                    "filename": molecule,
                    "instances": instances,
                },
            )
    elif args.pdb_source == "pdb":
        for (molecule, instances) in zip(molecules_list, instances_list):
            molecules_outgoing_list.append(
                {
                    "id": molecule,
                    "instances": instances,
                },
            )

    molecules = {args.pdb_source: molecules_outgoing_list}

    return molecules


def setup_parser_args():
    parser = argparse.ArgumentParser()
    """
    parser.add_argument(
        "--max_workers",
        help=(
            "Maximum number of worker processes to use on a cluster. Must specify"
            " cluster method."
        ),
        type=int,
        default=1,
        required=False,
    )

    parser.add_argument(
        "--method",
        help=(
            "Maximum number of worker processes to use on a cluster. Must specify"
            " cluster method."
        ),
        type=str,
        default=None,
        required=False,
    )
    """

    parser.add_argument(
        "--device",
        help=(
            "An enumeration to set whether to run on the GPU or CPU."
            " Options are 'cpu' or 'gpu'. Defaults to gpu"
        ),
        type=str,
        default="gpu",
        required=False,
    )

    parser.add_argument(
        "--acceleration_voltage_spread",
        help=("The acceleration voltage spread (dV/V)." " Defaults to 8.0e-07"),
        type=float,
        default=8.0e-7,
        required=False,
    )

    parser.add_argument(
        "--electrons_per_angstrom",
        help=(
            "Dose of electrons per square angstrom to use in Parakeet simulation"
            "Defaults to 45.0"
        ),
        type=float,
        default=45.0,
        required=False,
    )

    parser.add_argument(
        "--energy",
        help=(
            "Electron beam energy (kV) to use in Parakeet simulation"
            " Defaults to 300.0kV"
        ),
        type=float,
        default=300.0,
        required=False,
    )

    parser.add_argument(
        "--energy_spread",
        help=("Electron beam energy spread (dE/E)" " Defaults to 2.66e-6"),
        type=float,
        default=2.66e-6,
        required=False,
    )

    parser.add_argument(
        "--illumination_semiangle",
        help=("The illumination semiangle (mrad)." " Defaults to 0.02"),
        type=float,
        default=0.02,
        required=False,
    )

    parser.add_argument(
        "--phi",
        help=("The beam tilt phi angle (deg)" " Defaults to 0"),
        type=float,
        default=0.0,
        required=False,
    )

    parser.add_argument(
        "--theta",
        help=("The beam tilt theta angle (deg)" " Defaults to 0"),
        type=float,
        default=0.0,
        required=False,
    )

    parser.add_argument(
        "--dqe",
        help=("Use the DQE model (True/False)" " Defaults to False"),
        type=bool,
        default=False,
        required=False,
    )

    parser.add_argument(
        "--nx",
        help=(
            "Number of pixels along the x"
            " axis of the image(s) to be"
            " simulated using Parakeet"
        ),
        type=int,
        default=1000,
        required=False,
    )

    parser.add_argument(
        "--ny",
        help=(
            "Number of pixels along the y"
            " axis of the image(s) to be"
            " simulated using Parakeet"
        ),
        type=int,
        default=1000,
        required=False,
    )

    parser.add_argument(
        "--pixel_size",
        help=(
            "Pixel size (angstroms) to use"
            " in Parakeet simulation."
            " All pixels are square."
            " Defaults to 1.0"
        ),
        type=float,
        default=1.0,
        required=False,
    )

    parser.add_argument(
        "--Detector_origin_x",
        help=(
            "Detector origin along x axis" " in pixels wrt lab frame." " Defaults to 0"
        ),
        type=int,
        default=0,
        required=False,
    )

    parser.add_argument(
        "--Detector_origin_y",
        help=(
            "Detector origin along y axis" " in pixels wrt lab frame." " Defaults to 0"
        ),
        type=int,
        default=0,
        required=False,
    )

    parser.add_argument(
        "--phase_plate",
        help=(
            "Whether to use a"
            " phase plate in"
            " Parakeet simulation"
            " Defaults to False"
        ),
        type=bool,
        default=False,
        required=False,
    )

    parser.add_argument(
        "--c_10",
        help=(
            "Average defocus value (A) (input a negative number for"
            " underfocus) to use in Parakeet simulation."
        ),
        type=float,
        default=-20000,
        required=False,
    )

    parser.add_argument(
        "--c_12",
        help=("The 2-fold astigmatism (A)" " Defaults to 0"),
        type=float,
        default=0.0,
        required=False,
    )

    parser.add_argument(
        "--phi_12",
        help=("The Azimuthal angle of 2-fold astigmatism (rad)" " Defaults to 0"),
        type=float,
        default=0.0,
        required=False,
    )

    parser.add_argument(
        "--c_21",
        help=("The Axial coma (A)" " Defaults to 0"),
        type=float,
        default=0.0,
        required=False,
    )

    parser.add_argument(
        "--phi_21",
        help=("The Azimuthal angle of axial coma (rad)", " Defaults to 0"),
        type=float,
        default=0.0,
        required=False,
    )

    parser.add_argument(
        "--c_23",
        help=("The 3-fold astigmatism (A)" " Defaults to 0"),
        type=float,
        default=0.0,
        required=False,
    )

    parser.add_argument(
        "--phi_23",
        help=("The Azimuthal angle of 3-fold astigmatism (rad)" " Defaults to 0"),
        type=float,
        default=0.0,
        required=False,
    )

    parser.add_argument(
        "--c_30",
        help=("Spherical aberration (A)" " Defaults to 2.7"),
        type=float,
        default=2.7,
        required=False,
    )

    parser.add_argument(
        "--c_32",
        help=("The Axial star aberration (A)" " Defaults to 0"),
        type=float,
        default=0.0,
        required=False,
    )

    parser.add_argument(
        "--phi_32",
        help=("The Azimuthal angle of axial star aberration (rad)" " Defaults to 0"),
        type=float,
        default=0.0,
        required=False,
    )

    parser.add_argument(
        "--c_34",
        help=("The 4-fold astigmatism (A)", " Defaults to 0"),
        type=float,
        default=0.0,
        required=False,
    )

    parser.add_argument(
        "--phi_34",
        help=(
            "The Azimuthal angle of 4-fold astigmatism (rad)",
            " Defaults to 0",
        ),
        type=float,
        default=0.0,
        required=False,
    )

    parser.add_argument(
        "--c_41",
        help=("The 4th order axial coma (A)" " Defaults to 0"),
        type=float,
        default=0.0,
        required=False,
    )

    parser.add_argument(
        "--phi_41",
        help=("The Azimuthal angle of 4th order axial coma (rad)", " Defaults to 0"),
        type=float,
        default=0.0,
        required=False,
    )

    parser.add_argument(
        "--c_43",
        help=("The 3-lobe aberration (A)" " Defaults to 0"),
        type=float,
        default=0.0,
        required=False,
    )

    parser.add_argument(
        "--phi_43",
        help=("The Azimuthal angle of 3-lobe aberration (rad)" " Defaults to 0"),
        type=float,
        default=0.0,
        required=False,
    )

    parser.add_argument(
        "--c_45",
        help=("The 5-fold astigmatism (A)" " Defaults to 0"),
        type=float,
        default=0.0,
        required=False,
    )

    parser.add_argument(
        "--phi_45",
        help=("The Azimuthal angle of 5-fold astigmatism (rad)" " Defaults to 0"),
        type=float,
        default=0.0,
        required=False,
    )

    parser.add_argument(
        "--c_50",
        help=("The 5th order spherical aberration (A)" " Defaults to 0"),
        type=float,
        default=0.0,
        required=False,
    )

    parser.add_argument(
        "--c_52",
        help=("The 5th order axial star aberration (A)" " Defaults to 0"),
        type=float,
        default=0.0,
        required=False,
    )

    parser.add_argument(
        "--phi_52",
        help=(
            "The Azimuthal angle of 5th order axial star aberration (rad)"
            " Defaults to 0"
        ),
        type=float,
        default=0.0,
        required=False,
    )

    parser.add_argument(
        "--c_54",
        help=("The 5th order rosette aberration (A)" " Defaults to 0"),
        type=float,
        default=0.0,
        required=False,
    )

    parser.add_argument(
        "--phi_54",
        help=(
            "The Azimuthal angle of 5th order rosette aberration (rad)" " Defaults to 0"
        ),
        type=float,
        default=0.0,
        required=False,
    )

    parser.add_argument(
        "--c_56",
        help=("The 6-fold astigmatism (A)" " Defaults to 0"),
        type=float,
        default=0.0,
        required=False,
    )

    parser.add_argument(
        "--phi_56",
        help=("The Azimuthal angle of 6-fold astigmatism (rad)" " Defaults to 0"),
        type=float,
        default=0.0,
        required=False,
    )

    parser.add_argument(
        "--c_c",
        help=("Chromatic aberration (A)" " Defaults to 2.7"),
        type=float,
        default=2.7,
        required=False,
    )

    parser.add_argument(
        "--current_spread",
        help=("The current spread (dI/I)" " Defaults to 0.33e-6"),
        type=float,
        default=0.33e-6,
        required=False,
    )

    parser.add_argument(
        "--model",
        help=(
            "Use commercial microscope ('talos' or 'krios')"
            " instead of user-provided microscope values."
            " Defaults to None (user-provided values)"
        ),
        type=str,
        default=None,
        required=False,
    )

    parser.add_argument(
        "--box_x",
        help=("Sample box size along x axis (Angstroms)" " Defaults to 1000"),
        type=float,
        default=1000.0,
        required=False,
    )

    parser.add_argument(
        "--box_y",
        help=("Sample box size along y axis (Angstroms)" " Defaults to 1000"),
        type=float,
        default=1000.0,
        required=False,
    )

    parser.add_argument(
        "--box_z",
        help=("Sample box size along z axis (Angstroms)" " Defaults to 500"),
        type=float,
        default=500.0,
        required=False,
    )

    parser.add_argument(
        "--centre_x",
        help=(
            "Center of tomographic rotation around sample x axis (A)" " Defaults to 500"
        ),
        type=float,
        default=500.0,
        required=False,
    )

    parser.add_argument(
        "--centre_y",
        help=(
            "Center of tomographic rotation around sample y axis (A)" " Defaults to 500"
        ),
        type=float,
        default=500.0,
        required=False,
    )

    parser.add_argument(
        "--centre_z",
        help=(
            "Center of tomographic rotation around sample z axis (A)" " Defaults to 250"
        ),
        type=float,
        default=250.0,
        required=False,
    )

    """
    parser.add_argument(
        "--slow_ice",
        help=(
            "Generate the (very slow) atomic ice model instead of fast GRF?"
            " Defaults to None (not used)"
        ),
        type=bool,
        default=None,
        required=False,
    )
    """

    parser.add_argument(
        "--type",
        help=(
            "An enumeration of sample shape types."
            " Options are 'cube', 'cuboid' or 'cylinder'."
            " Defaults to cuboid"
        ),
        type=str,
        default="cuboid",
        required=False,
    )

    parser.add_argument(
        "--cube_length",
        help=("The cube side length (A)" " Defaults to 1000"),
        type=float,
        default=1000.0,
        required=False,
    )

    parser.add_argument(
        "--cuboid_length_x",
        help=("The cuboid X side length (A)" " Defaults to 1000"),
        type=float,
        default=1000.0,
        required=False,
    )

    parser.add_argument(
        "--cuboid_length_y",
        help=("The cuboid Y side length (A)" " Defaults to 1000"),
        type=float,
        default=1000.0,
        required=False,
    )

    parser.add_argument(
        "--cuboid_length_z",
        help=("The cuboid Z side length (A)" " Defaults to 1000"),
        type=float,
        default=500.0,
        required=False,
    )

    parser.add_argument(
        "--cylinder_length",
        help=("The cylinder length (A)" " Defaults to 1000"),
        type=float,
        default=1000.0,
        required=False,
    )

    parser.add_argument(
        "--cylinder_radius",
        help=("The cylinder radius (A)" " Defaults to 500"),
        type=float,
        default=500.0,
        required=False,
    )

    parser.add_argument(
        "--margin_x",
        help=(
            "The x axis margin used to define how close to the edges particles"
            " should be placed (A)"
            " Default value is 0"
        ),
        type=float,
        default=0.0,
        required=False,
    )

    parser.add_argument(
        "--margin_y",
        help=(
            "The y axis margin used to define how close to the edges particles"
            " should be placed (A)"
            " Default value is 0"
        ),
        type=float,
        default=0.0,
        required=False,
    )

    parser.add_argument(
        "--margin_z",
        help=(
            "The z axis margin used to define how close to the edges particles"
            " should be placed (A)"
            " Default value is 0"
        ),
        type=float,
        default=0.0,
        required=False,
    )

    parser.add_argument(
        "--fast_ice",
        help=(
            "Use the Gaussian Random Field ice model (True/False)." " Defaults to False"
        ),
        type=bool,
        default=False,
        required=False,
    )

    parser.add_argument(
        "--simulation_margin",
        help=("The margin around the image." " Defaults to 100"),
        type=int,
        default=100,
        required=False,
    )

    """
    parser.add_argument(
        "--inelastic_model",
        help=("The inelastic model parameters." " Defaults to None"),
        type=str,
        default=None,
        required=False,
    )

    parser.add_argument(
        "--mp_loss_position",
        help=("The MPL energy filter position." " Defaults to peak"),
        type=str,
        default="peak",
        required=False,
    )

    parser.add_argument(
        "--mp_loss_width",
        help=("The MPL energy filter width (eV)." " Defaults to 50"),
        type=float,
        default=50,
        required=False,
    )
    """

    parser.add_argument(
        "--simulation_padding",
        help=("Additional padding." " Defaults to 100"),
        type=int,
        default=100,
        required=False,
    )

    parser.add_argument(
        "--radiation_damage_model",
        help=("Use the radiation damage model?" " Defaults to False"),
        type=bool,
        default=False,
        required=False,
    )

    parser.add_argument(
        "--sensitivity_coefficient",
        help=(
            "The radiation damage model sensitivity coefficient." " Defaults to 0.022"
        ),
        type=float,
        default=0.022,
        required=False,
    )

    parser.add_argument(
        "--slice_thickness",
        help=("The multislice thickness (A)." " Defaults to 3.0"),
        type=float,
        default=3.0,
        required=False,
    )

    parser.add_argument(
        "--pdb_source",
        help=(
            "Whether to use local or parakeet dir as source of molecule(s)."
            " Defaults to pdb"
        ),
        choices=["local", "pdb"],
        nargs="?",
        type=str,
        default="pdb",
        required=False,
    )

    parser.add_argument(
        "--pdb_filepaths",
        help=("The filename(s) of the atomic coordinates to use (*.pdb, *.cif)"),
        nargs="+",
        type=str,
        default=["4v1w"],
        required=False,
    )

    parser.add_argument(
        "--pdb_instances",
        help=(
            "The instances of the molecule to put into the sample model. This "
            "field can be set as either an integer or a list of MoleculePose "
            "objects. If it is set to an integer == 1 then the molecule will be "
            "positioned in the centre of the sample volume; any other integer "
            "will result in the molecules being positioned at random positions "
            "and orientations in the volume. If a list of MoleculePose objects "
            "is given then an arbitrary selection of random and assigned "
            "positions and poses can be set"
        ),
        nargs="+",
        type=int,
        default=[1],
        required=False,
    )

    parser.add_argument(
        "--config_yaml_filename",
        help=("Filename for parakeet configuration file"),
        type=str,
        default="config.yaml",
        required=False,
    )

    return parser


def parse_args(parser, args):
    parsed_args = parser.parse_intermixed_args(args)
    return parsed_args


def main(raw_args):
    parser = setup_parser_args()
    args = parse_args(parser, raw_args)

    # Create a config yaml class with a given name and default values
    parakeet_config = ParakeetConfigYAML(args.config_yaml_filename)

    # determine the molecules to insert from args provided
    molecules = determine_molecules(args)

    # update the default parameters with those provided via args
    # cluster
    """
    parakeet_config.update_cluster(
        {
            "max_workers": args.max_workers,
            "method": args.method,
        }
    )
    """

    # device
    parakeet_config.update_device(args.device)

    # microscope
    parakeet_config.update_microscope(
        {
            "beam": {
                "acceleration_voltage_spread": args.acceleration_voltage_spread,
                "electrons_per_angstrom": args.electrons_per_angstrom,
                "energy": args.energy,
                "energy_spread": args.energy_spread,
                "phi": args.phi,
                "illumination_semiangle": args.illumination_semiangle,
                "theta": args.theta,
            },
            "detector": {
                "dqe": args.dqe,
                "nx": args.nx,
                "ny": args.ny,
                "origin": [
                    args.Detector_origin_x,
                    args.Detector_origin_y,
                ],
                "pixel_size": args.pixel_size,
            },
            "lens": {
                "c_10": args.c_10,
                "c_12": args.c_12,
                "c_21": args.c_21,
                "c_23": args.c_23,
                "c_30": args.c_30,
                "c_32": args.c_32,
                "c_34": args.c_34,
                "c_41": args.c_41,
                "c_43": args.c_43,
                "c_45": args.c_45,
                "c_50": args.c_50,
                "c_52": args.c_52,
                "c_54": args.c_54,
                "c_56": args.c_56,
                "c_c": args.c_c,
                "current_spread": args.current_spread,
                "phi_12": args.phi_12,
                "phi_21": args.phi_21,
                "phi_23": args.phi_23,
                "phi_32": args.phi_32,
                "phi_34": args.phi_34,
                "phi_41": args.phi_41,
                "phi_43": args.phi_43,
                "phi_45": args.phi_45,
                "phi_52": args.phi_52,
                "phi_54": args.phi_54,
                "phi_56": args.phi_56,
            },
            "model": args.model,
            "phase_plate": args.phase_plate,
        }
    )
    # microscope->beam
    # microscope->detector
    # microscope->lens
    # microscope->model
    # microscope->phase_plate

    # sample
    parakeet_config.update_sample(
        {
            "box": [
                args.box_x,
                args.box_y,
                args.box_z,
            ],
            "centre": [
                args.centre_x,
                args.centre_y,
                args.centre_z,
            ],
            "ice": None,
            "molecules": molecules,
            "shape": {
                "cube": {"length": args.cube_length},
                "cuboid": {
                    "length_x": args.cuboid_length_x,
                    "length_y": args.cuboid_length_y,
                    "length_z": args.cuboid_length_z,
                },
                "cylinder": {
                    "length": args.cylinder_length,
                    "radius": args.cylinder_radius,
                    "offset_x": None,
                    "offset_z": None,
                    "axis": [0, 1, 0],
                },
                "margin": [
                    args.margin_x,
                    args.margin_y,
                    args.margin_z,
                ],
                "type": args.type,
            },
            "sputter": None,
            "coords": None,
        }
    )
    # sample->box
    # sample->centre
    # sample->ice (slow ice)
    # sample->molecules
    # sample->shape
    # sample->shape->cube
    # sample->shape->cuboid
    # sample->shape->cylinder
    # sample->shape->margin
    # sample->shape->type
    # sample->sputter (not supported)

    # scan (not supported)
    # scan->angles (not supported)
    # scan->axis
    # scan->exposure_time (not supported)
    # scan->mode
    # scan->num_images (not supported)
    # scan->positions (not supported)
    # scan->start_angle
    # scan->start_pos
    # scan->step_pos

    # simulation
    parakeet_config.update_simulation(
        {
            "division_thickness": 100,
            "ice": args.fast_ice,
            "margin": args.simulation_margin,
            # "inelastic_model": args.inelastic_model,
            # "mp_loss_position": args.mp_loss_position,
            # "mp_loss_width": args.mp_loss_width,
            "padding": args.simulation_padding,
            "radiation_damage_model": args.radiation_damage_model,
            "sensitivity_coefficient": args.sensitivity_coefficient,
            "slice_thickness": args.slice_thickness,
        }
    )
    # simulation->division_thickness (not supported)
    # simulation->ice (fast ice)
    # simulation->inelastic_model
    # simulation->margin
    # simulation->mp_loss_position
    # simulation->mp_loss_width
    # simulation->padding
    # simulation->radiation_damage_model
    # simulation->sensitivity_coefficient
    # simulation->slice_thickness

    parakeet_config.output_yaml(parakeet_config.name)


if __name__ == "__main__":
    main(sys.argv[1:])
