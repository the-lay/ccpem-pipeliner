#!/usr/bin/env python3
#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import json
import sys
from typing import Optional, List
import argparse


def main(in_args: Optional[List[str]] = None):

    parser = argparse.ArgumentParser()
    parser.add_argument("--job_title", type=str, required=True)
    parser.add_argument("--input_map", type=str, required=True)
    parser.add_argument("--resolution", type=float, required=True)
    parser.add_argument("--input_seq", type=str, required=True)
    parser.add_argument("--extend_pdb", type=str, required=True)
    parser.add_argument("--ncycle", type=int, required=True)
    parser.add_argument("--ncycle_refmac", type=int, required=True)
    parser.add_argument("--ncycle_buc1st", type=int, required=True)
    parser.add_argument("--ncycle_bucnth", type=int, required=True)
    parser.add_argument("--map_sharpen", type=float, required=True)
    parser.add_argument("--ncpus", type=int, required=True)
    parser.add_argument("--lib_in", type=str, required=True)
    parser.add_argument("--keywords", type=str, required=True, const="None")
    parser.add_argument("--refmac_keywords", type=str, required=True, const="None")

    if in_args is None:
        in_args = sys.argv[1:]
    args = parser.parse_args(in_args)

    args_file = "tmp_buccaneer_args.json"
    args_dict = {}
    # convert "None" to actual NoneType
    for ad in vars(args):
        args_dict[ad] = None if vars(args)[ad] == "None" else vars(args)[ad]
    with open(args_file, "w") as fp:
        json.dump(args_dict, fp)


if __name__ == "__main__":
    main()
