#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
import json

from pipeliner import user_settings
from pipeliner.pipeliner_job import PipelinerJob, Ref, ExternalProgram
from pipeliner.job_options import (
    StringJobOption,
    InputNodeJobOption,
    FloatJobOption,
    BooleanJobOption,
    files_exts,
)
from pipeliner.node_factory import create_node
from pipeliner.display_tools import (
    create_results_display_object,
)
from pipeliner.nodes import NODE_DENSITYMAP, NODE_ATOMCOORDS, NODE_LOGFILE


class ProShadeJobSym(PipelinerJob):
    PROCESS_NAME = "proshade.map_analysis.symmetry"
    OUT_DIR = "ProShade"

    def __init__(self):
        super().__init__()
        self.jobinfo.display_name = "ProShade Symmetry Determination"
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Martyn Winn, Matt Iadanza"
        self.jobinfo.short_desc = "Local symmetry detection"
        self.jobinfo.long_desc = (
            "ProShade (Protein Shape Description and Symmetry Detection) tool "
            "provides several separate functionalities. These functionalities "
            "accept both, coordinate data as well as map data. Symmetry detection "
            "allows finding the symmetry, the symmetry axes and symmetry group "
            "elements for any input file."
        )
        self.jobinfo.documentation = (
            "https://www2.mrc-lmb.cam.ac.uk/groups/"
            "murshudov/content/proshade/proshade.html"
        )
        self.jobinfo.programs = [
            ExternalProgram(
                command="proshade", vers_com=["proshade", "-v"], vers_lines=[0]
            )
        ]
        self.jobinfo.references = [
            Ref(
                authors=[
                    "R. A. Nicholls, M. Tykac, O. Kovalevskiy and G. N. Murshudov"
                ],
                title="Current approaches for the fitting and refinement of "
                "atomic models into cryo-EM maps using CCP-EM",
                journal="Acta Crystallogr D Biol Crystallogr.",
                year="2018",
                volume="74",
                pages="492-505",
                doi="10.1107/S2059798318007313",
            ),
        ]

        self.sharedir = user_settings.get_ccpem_share_dir()
        if not self.sharedir:
            self.jobinfo.availability_checks = False
        elif not os.path.isdir(self.sharedir):
            self.jobinfo.availability_checks = False

        self.joboptions["input_map"] = InputNodeJobOption(
            label="Input map",
            node_type=NODE_DENSITYMAP,
            default_value="",
            directory="",
            pattern=files_exts("3D map", [".mrc"]),
            help_text="The input map",
            is_required=True,
        )

        self.joboptions["point_sym"] = StringJobOption(
            label="Expected symmetry",
            default_value="",
            validation_regex="[CD][0-9]+|^[TO]$",
            help_text="The symmetry to be preferred, if found. This parameter forces "
            "the symmetry search to output the results for this type of "
            "symmetry, if found. The parameter should have the first letter "
            "C for cyclic, D for dihedral, T for tetrahedral, O for octahedral "
            "or I for icosahedral. C and D must be followed by a "
            "number specifying the required fold, but T, O and I must not.",
            in_continue=True,
        )

        self.get_runtab_options(addtl_args=True)

    def get_commands(self):
        command = ["proshade", "--rvpath", self.sharedir, "-S"]
        input_file = self.joboptions["input_map"].get_string(True, "Input file missing")
        command += ["-f", input_file]

        command += self.parse_additional_args()

        report_dest = os.path.join(self.output_dir, "proshade_report")
        moving_com = ["mv", "proshade_report", report_dest]

        # needs to return a list of lists
        final_commands_list = [command, moving_com]
        return final_commands_list

    # TODO: check that this function handles the log generated when
    #  no symmetry was found - don't know what this looks like and other
    #  possible log file formats I may have not anticipated

    def parse_results(self, return_lists=False):
        """Parse the output that went to run.out and return a dict or lists"""
        # parse the logfile from proshade symmetry
        outlog = os.path.join(self.output_dir, "run.out")
        with open(outlog, "r") as ol:
            outdata = ol.readlines()
        div_count = 0
        axes, elements, alts = [], [], []
        exclude = ["----", "Symmetry elements table:", "Alternatives:"]
        for line in outdata:
            if not any([x in line for x in exclude]) and line != "\n":
                if div_count == 8:
                    axes.append(line)
                elif div_count == 10:
                    elements.append(line)
                elif div_count == 12:
                    alts.append(line)
                elif div_count > 12:
                    break
            if "--------" in line:
                div_count += 1

        # prepare the data
        outdict = {"Symmetry axes": [], "Symmetry elements": [], "Alternatives": []}
        outax, outelems, outalts = [], [], []

        for ax in axes:
            dat = ax.split()
            angle = "".join(dat[5:8]) if len(dat) == 9 else dat[5]
            od = {
                "Symmetry Type": dat[0],
                "Fold": dat[1],
                "x": dat[2],
                "y": dat[3],
                "z": dat[4],
                "Angle": angle,
                "Peak Height": dat[-1],
            }
            outdict["Symmetry axes"].append(od)
            outax.append(dat[:5] + [angle] + [dat[-1]])

        for elm in elements:
            dat = elm.split()
            od = {
                "Symmetry Type": dat[0],
                "x": dat[1],
                "y": dat[2],
                "z": dat[3],
                "Ang (deg)": dat[4],
            }
            outdict["Symmetry elements"].append(od)
            outelems.append(dat)
        for alt in alts:
            dat = alt.split()
            angle = "".join(dat[5:8]) if len(dat) == 9 else dat[5]
            od = {
                "Symmetry Type": dat[0],
                "Fold": dat[1],
                "x": dat[2],
                "y": dat[3],
                "z": dat[4],
                "Angle": angle,
                "Peak Height": dat[-1],
            }
            outdict["Alternatives"].append(od)
            outalts.append(dat[:5] + [angle] + [dat[-1]])
        if return_lists:
            return [outax, outelems, outalts]
        return outdict

    def post_run_actions(self):
        """Read the run.out and create a proper output node"""
        outf = os.path.join(self.output_dir, "proshade_symmetry.json")
        outdict = self.parse_results()
        with open(outf, "w") as outfile:
            json.dump(outdict, outfile, indent=4)
        self.output_nodes.append(
            create_node(outf, NODE_LOGFILE, ["proshade", "map_analysis", "symmetry"])
        )

    def gather_metadata(self):
        def str_convert(obj):
            if "Fold" in obj:
                obj["Fold"] = int(obj["Fold"].replace("+", "").replace(".00", ""))
            if "x" in obj:
                obj["x"] = float(obj["x"].replace("+", ""))
            if "y" in obj:
                obj["y"] = float(obj["y"].replace("+", ""))
            if "z" in obj:
                obj["z"] = float(obj["z"].replace("+", ""))
            if "Angle" in obj:
                obj["Angle"] = eval(obj["Angle"].replace("pi", "*np.pi"))
            if "Ang (deg)" in obj:
                obj["Ang (deg)"] = float(obj["Ang (deg)"].replace("+", ""))
            if "Peak Height" in obj:
                obj["Peak Height"] = float(obj["Peak Height"].replace("+", ""))
            return obj

        with open("parse_json.json", "r") as j:
            metadata_dict = json.loads(j.read(), object_hook=str_convert)

        return metadata_dict

    def create_results_display(self):
        """create a ResultsDisplayTable object for each of the 3 outputs
        and a rvapi display object"""
        # do the three tables
        outf = os.path.join(self.output_dir, "proshade_symmetry.json")
        axes, elems, alts = self.parse_results(return_lists=True)

        ax_t = create_results_display_object(
            "table",
            title="Symmetry Axes",
            headers=[
                "Symmetry Type",
                "Fold",
                "x",
                "y",
                "z",
                "Angle",
                "Peak Height",
            ],
            table_data=axes,
            associated_data=[outf],
        )

        elem_t = create_results_display_object(
            "table",
            title="Symmetry Elements",
            headers=[
                "Symmetry Type",
                "x",
                "y",
                "z",
                "Angle",
            ],
            table_data=elems,
            associated_data=[outf],
        )

        alt_t = create_results_display_object(
            "table",
            title="Alternatives",
            headers=[
                "Symmetry Type",
                "Fold",
                "x",
                "y",
                "z",
                "Angle",
                "Peak Height",
            ],
            table_data=alts,
            associated_data=[outf],
        )

        # then the rvapi
        report_dir = os.path.join(self.output_dir, "proshade_report")
        rvapi = create_results_display_object(
            "rvapi", title="ProShade results", rvapi_dir=report_dir
        )
        return [ax_t, elem_t, alt_t, rvapi]


class ProShadeJobRebox(PipelinerJob):
    """CCP-EM gui creates:
    proshade --rvpath /xtal/ccpem-20220125/share -E -f emd_13271.map
    --clearMap emd_13271_reboxed --cellBorderSpace 1.0
    """

    PROCESS_NAME = "proshade.map_utilities.rebox"
    OUT_DIR = "ProShade"

    def __init__(self):
        super().__init__()
        self.jobinfo.display_name = "ProShade Automated Reboxing"
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Martyn Winn, Matt Iadanza"
        self.jobinfo.short_desc = "Find smaller box for input map "
        self.jobinfo.long_desc = (
            "ProShade (Protein Shape Description and Symmetry Detection) tool "
            "provides several separate functionalities. The map re-boxing "
            "functionality masks the input (map only) and attempts to find smaller "
            "box containing all the mask, thus decreasing the linear processing "
            "time for the map. This functionality is also available for half maps."
        )
        self.jobinfo.documentation = (
            "https://www2.mrc-lmb.cam.ac.uk/groups/"
            "murshudov/content/proshade/proshade.html"
        )
        self.jobinfo.programs = [
            ExternalProgram(
                command="proshade", vers_com=["proshade", "-v"], vers_lines=[0]
            )
        ]
        self.jobinfo.references = [
            Ref(
                authors=[
                    "R. A. Nicholls, M. Tykac, O. Kovalevskiy and G. N. Murshudov"
                ],
                title="Current approaches for the fitting and refinement of "
                "atomic models into cryo-EM maps using CCP-EM",
                journal="Acta Crystallogr D Biol Crystallogr.",
                year="2018",
                volume="74",
                pages="492-505",
                doi="10.1107/S2059798318007313",
            ),
        ]

        self.sharedir = user_settings.get_ccpem_share_dir()
        if not self.sharedir:
            self.jobinfo.availability_checks = False
        elif not os.path.isdir(self.sharedir):
            self.jobinfo.availability_checks = False

        self.joboptions["input_map"] = InputNodeJobOption(
            label="Input map",
            node_type=NODE_DENSITYMAP,
            default_value="",
            directory="",
            pattern=files_exts("3D map", [".mrc"]),
            help_text="The input map",
            is_required=True,
        )

        self.joboptions["add_space"] = BooleanJobOption(
            label="Add extra space around structure?",
            default_value=False,
            help_text="(Optional) Add extra space around the masked structure",
            in_continue=True,
        )
        self.joboptions["extra_space"] = FloatJobOption(
            label="Extra space around structure",
            default_value=0.0,
            suggested_min=0.0,
            help_text="How much extra space to add (Angstroms)",
            in_continue=True,
            deactivate_if=[("add_space", "=", False)],
            required_if=[("add_space", "=", True)],
        )

        self.get_runtab_options(addtl_args=True)

    def get_commands(self):
        command = ["proshade", "--rvpath", self.sharedir, "-E"]
        input_file = self.joboptions["input_map"].get_string(True, "Input file missing")
        command += ["-f", input_file]
        output_file = os.path.join(self.output_dir, "reboxed_map.mrc")
        command += [" --clearMap ", output_file]

        self.output_nodes.append(
            create_node(output_file, NODE_DENSITYMAP, ["proshade"])
        )

        do_extra_space = self.joboptions["add_space"].get_boolean()
        if do_extra_space:
            alt_arg = self.joboptions["extra_space"].get_number()
            command += ["--cellBorderSpace", alt_arg]
        command += self.parse_additional_args()

        report_dest = os.path.join(self.output_dir, "proshade_report")
        moving_com = ["mv", "proshade_report", report_dest]

        # needs to return a list of lists
        final_commands_list = [command, moving_com]
        return final_commands_list

    def gather_metadata(self):
        def strip_tags(tag):
            if "<pre>" in tag:
                tag = tag.replace("<pre>", " ")
            if "</pre>" in tag:
                tag = tag.replace("</pre>", " ")
            if "<b>" in tag:
                tag = tag.replace("<b>", " ")
            if "</b>" in tag:
                tag = tag.replace("</b>", " ")
            return tag

        metadata_dict = {}

        with open(self.output_dir + "proshade_report/task.tsk", "r") as rf:
            results_file = rf.readlines()

        for line in results_file:
            if "ResultsSection" in line:
                line = strip_tags(line)
                if "Original structure dims" in line:
                    line = line.split()
                    metadata_dict["OriginalDims"] = [
                        int(line[4]),
                        int(line[6]),
                        int(line[8]),
                    ]
                if "ReBoxedDims" in line:
                    line = line.split()
                    metadata_dict["Re-boxed structure dims"] = [
                        int(line[4]),
                        int(line[6]),
                        int(line[8]),
                    ]
                if "New volume" in line:
                    metadata_dict["VolPct"] = float(line.split()[8])
                if "Linear processing" in line:
                    metadata_dict["SpeedUp"] = float(line.split()[4])

        return metadata_dict

    def create_results_display(self):
        report_dir = os.path.join(self.output_dir, "proshade_report")
        return [
            create_results_display_object(
                "rvapi", title="ProShade results", rvapi_dir=report_dir
            )
        ]


class ProShadeJobOverlay(PipelinerJob):
    """CCP-EM gui creates:
    proshade --rvpath /xtal/ccpem-20220125/share -O -f in1.mrc -f in2.mrc
     --clearMap in2_overlaid.mrc --cellBorderSpace 20.0
    TODO ProShade won't accept mmCIF format coordinate files
    """

    PROCESS_NAME = "proshade.map_utilities.overlay"
    OUT_DIR = "ProShade"

    def __init__(self):
        super().__init__()
        self.jobinfo.display_name = "ProShade Overlay"
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Martyn Winn, Matt Iadanza"
        self.jobinfo.short_desc = "Overlay one map/model on another "
        self.jobinfo.long_desc = (
            "ProShade (Protein Shape Description and Symmetry Detection) tool "
            "provides several separate functionalities. The overlay "
            "functionality finds the rotation and translation which optimally overlays "
            "(or fits) one structure into another. Each structure can be a "
            "coordinate file or a map."
        )
        self.jobinfo.documentation = (
            "https://www2.mrc-lmb.cam.ac.uk/groups/"
            "murshudov/content/proshade/proshade.html"
        )
        self.jobinfo.programs = [
            ExternalProgram(
                command="proshade", vers_com=["proshade", "-v"], vers_lines=[0]
            )
        ]
        self.jobinfo.references = [
            Ref(
                authors=[
                    "R. A. Nicholls, M. Tykac, O. Kovalevskiy and G. N. Murshudov"
                ],
                title="Current approaches for the fitting and refinement of "
                "atomic models into cryo-EM maps using CCP-EM",
                journal="Acta Crystallogr D Biol Crystallogr.",
                year="2018",
                volume="74",
                pages="492-505",
                doi="10.1107/S2059798318007313",
            ),
        ]

        self.sharedir = user_settings.get_ccpem_share_dir()
        if not self.sharedir:
            self.jobinfo.availability_checks = False
        elif not os.path.isdir(self.sharedir):
            self.jobinfo.availability_checks = False

        self.joboptions["static_struct"] = InputNodeJobOption(
            label="Static map or coordinates",
            node_type="Created later",
            create_node=False,
            default_value="",
            directory="",
            pattern=files_exts("3D map or coordinates", [".mrc", ".map", ".pdb"]),
            help_text="The first structure which remains static",
            is_required=True,
        )
        self.joboptions["moving_struct"] = InputNodeJobOption(
            label="Moving map or coordinates",
            node_type="Created later",
            create_node=False,
            default_value="",
            directory="",
            pattern=files_exts("3D map or coordinates", [".mrc", ".map", ".pdb"]),
            help_text="The second structure which is moved to match the first",
            is_required=True,
        )

        self.joboptions["add_space"] = BooleanJobOption(
            label="Add extra space around structure?",
            default_value=False,
            help_text=(
                "(Optional) Extra space may be required to accommodate "
                "rotation/translation of second structure"
            ),
            in_continue=True,
        )
        self.joboptions["extra_space"] = FloatJobOption(
            label="Extra space around structure",
            default_value=0.0,
            suggested_min=0.0,
            help_text="How much extra space to add (Angstroms)",
            in_continue=True,
            deactivate_if=[("add_space", "=", False)],
            required_if=[("add_space", "=", True)],
        )

        self.get_runtab_options(addtl_args=True)

    def get_commands(self):
        # TODO: We will need to set -rvpath from an env var or add it as a joboption
        # because it will vary
        command = ["proshade", "--rvpath", self.sharedir, "-O"]
        input1_file = self.joboptions["static_struct"].get_string(
            True, "Input file missing"
        )
        command += ["-f", input1_file]
        input2_file = self.joboptions["moving_struct"].get_string(
            True, "Input file missing"
        )
        command += ["-f", input2_file]

        # based on filename extension, but should do better
        # is there nothing in Gemmi? perhaps test with mrcfile.validate()
        if input1_file.split(".")[-1] == "map" or input1_file.split(".")[-1] == "mrc":
            node_type = NODE_DENSITYMAP
        else:
            node_type = NODE_ATOMCOORDS
        self.input_nodes.append(create_node(input1_file, node_type))
        if input2_file.split(".")[-1] == "map" or input2_file.split(".")[-1] == "mrc":
            node_type = NODE_DENSITYMAP
            output_ext = "mrc"
            out_node_type = NODE_DENSITYMAP
        else:
            node_type = NODE_ATOMCOORDS
            output_ext = "pdb"
            out_node_type = NODE_ATOMCOORDS
        self.input_nodes.append(create_node(input2_file, node_type))

        # output type according to second (moved) input file
        output_file = os.path.join(
            self.output_dir,
            os.path.basename(input2_file).split(".")[0] + "_overlaid." + output_ext,
        )
        command += [" --clearMap ", output_file]
        self.output_nodes.append(
            create_node(output_file, out_node_type, ["proshade", "overlaid"])
        )

        do_extra_space = self.joboptions["add_space"].get_boolean()
        if do_extra_space:
            alt_arg = self.joboptions["extra_space"].get_number()
            command += ["--cellBorderSpace", alt_arg]
        command += self.parse_additional_args()

        report_dest = os.path.join(self.output_dir, "proshade_report")
        moving_com = ["mv", "proshade_report", report_dest]

        # needs to return a list of lists
        final_commands_list = [command, moving_com]
        return final_commands_list

    def gather_metadata(self):
        def strip_tags(tag):
            if "<pre>" in tag:
                tag = tag.replace("<pre>", " ")
            if "</pre>" in tag:
                tag = tag.replace("</pre>", " ")
            if "<b>" in tag:
                tag = tag.replace("<b>", " ")
            if "</b>" in tag:
                tag = tag.replace("</b>", " ")
            return tag

        metadata_dict = {}

        with open(self.output_dir + "proshade_report/task.tsk", "r") as rf:
            results_file = rf.readlines()

        for line in results_file:
            if "ResultsSection" in line:
                line = strip_tags(line)
                if "Patterson maps optimal rotation" in line:
                    line = line.split()
                    metadata_dict["PattersonMapsOptimalRotation"] = [
                        float(line[8].strip("+")),
                        float(line[9].strip("+")),
                        float(line[10].strip("+")),
                    ]
                if "Phased maps" in line:
                    line = line.split()
                    metadata_dict["PhasedMapsOptimalTranslation"] = [
                        float(line[7].strip("+")),
                        float(line[8].strip("+")),
                        float(line[9].strip("+")),
                    ]
                if "Correlation between rotated" in line:
                    metadata_dict["PattersonMapsCorrelation"] = float(
                        line.split()[7].strip("+")
                    )
                if "Correlation between translated" in line:
                    metadata_dict["PhasedMapsCorrelation"] = float(
                        line.split()[8].strip("+")
                    )

        return metadata_dict

    def create_results_display(self):
        report_dir = os.path.join(self.output_dir, "proshade_report")
        return [create_results_display_object("rvapi", rvapi_dir=report_dir)]
