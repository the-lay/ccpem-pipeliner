#! /usr/bin/env python3
#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import gemmi
import argparse
from pathlib import Path
import json

# Gemmi read the docs:
# https://gemmi.readthedocs.io/en/latest/index.html

# Gemmi Python API:
# https://project-gemmi.github.io/python-api/index.html


def calculate_map(pdb_in, pdb_out, map_out, resolution):
    # Read structure
    structure = gemmi.read_structure(pdb_in)

    # Remove hydrogens
    print("Removing hydrogens")
    structure.remove_hydrogens()

    # Remove waters
    print("Removing waters")
    structure.remove_waters()

    # Remove waters and ligands
    # structure.remove_ligands_and_waters()

    # Remove alternative conformations
    # structure.remove_alternative_conformations()

    # Move coordinates to centre of P1 cubic box with margin of 10 Angstroms
    margin = 10.0
    print(
        "Move coordinates to centre of P1 cubic box with margin of {}A".format(margin)
    )
    spacegroup = "P1"
    structure.spacegroup_hm = spacegroup
    box = structure.calculate_box(margin=margin)
    com = structure[0].calculate_center_of_mass()
    dims = (
        box.maximum[0] - box.minimum[0],
        box.maximum[1] - box.minimum[1],
        box.maximum[2] - box.minimum[2],
    )

    list_b_iso = []  # for histogram plot
    offset = 0.5 * max(dims)
    trans = (offset - com[0], offset - com[1], offset - com[2])
    for model in structure:
        for chain in model:
            for residue in chain:
                for atom in residue:
                    atom.pos = gemmi.Position(
                        atom.pos.x + trans[0],
                        atom.pos.y + trans[1],
                        atom.pos.z + trans[2],
                    )
                    list_b_iso.append(atom.b_iso)
    bfact_out = "bfact_" + Path(pdb_in).stem + ".json"
    with open(bfact_out, "w") as b:
        json.dump({"b_iso": list_b_iso}, b)

    # Better to create new cell rather than set_cell
    # In some rare cases (e.g. 5me2) scalen is set this needs to overwritten
    structure.cell = gemmi.UnitCell(max(dims), max(dims), max(dims), 90, 90, 90)

    print(
        "Update unit cell: {} {} {} {} {} {} {}".format(
            max(dims), max(dims), max(dims), 90, 90, 90, spacegroup
        )
    )

    # Save modified PDB
    structure.write_pdb(pdb_out)
    print("Write modified PDB: {}".format(pdb_out))

    # Calculate Density w/ Electron form factors
    # See Density for FFT:
    #   https://gemmi.readthedocs.io/en/latest/hkl.html

    dencalc = gemmi.DensityCalculatorE()
    # Set resolution in Angstrom
    dencalc.d_min = resolution
    # Typical sampling rate
    dencalc.rate = 1.5
    dencalc.set_grid_cell_and_spacegroup(structure)
    dencalc.put_model_density_on_grid(structure[0])
    denmap = gemmi.Ccp4Map()
    denmap.grid = dencalc.grid

    # Set to map to mode 2 (32-bit signed real) for details see:
    #   https://www.ccpem.ac.uk/mrc_format/mrc2014.php
    denmap.update_ccp4_header(2, True)
    denmap.write_ccp4_map(map_out)
    print("Write map: {}".format(map_out))


def main():
    """
    Load coordinate file in PDB or mmCIF format and generates density map
    with electron form factors at given resolution
    """
    print("-" * 80)
    print("\nGemmi model to map\n")
    print("-" * 80)

    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--pdb", type=str, required=True)
    parser.add_argument("-r", "--resolution", type=float, default=3.0)
    args = parser.parse_args()

    # Set output paths
    pdb_out = "gemmi_" + Path(args.pdb).stem + ".pdb"
    map_out = "gemmi_" + Path(args.pdb).stem + ".mrc"

    calculate_map(
        pdb_in=args.pdb, pdb_out=pdb_out, map_out=map_out, resolution=args.resolution
    )


if __name__ == "__main__":
    main()
