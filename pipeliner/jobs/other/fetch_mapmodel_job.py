#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import requests
import os
import traceback
import subprocess
import xml.etree.ElementTree as Et
from time import sleep
from gemmi import cif
from typing import List, Tuple
from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram, Ref
from pipeliner.job_options import (
    StringJobOption,
    MultipleChoiceJobOption,
    BooleanJobOption,
)
from pipeliner.job_options import JobOptionValidationResult
from pipeliner.display_tools import (
    make_map_model_thumb_and_display,
    create_results_display_object,
)
from pipeliner.results_display_objects import ResultsDisplayObject
from pipeliner.deposition_tools.pdb_deposition_objects import (
    other_startup_model_type_entry,
    DEPOSITION_COMMENT,
)
from pipeliner.node_factory import create_node, NODE_ATOMCOORDS, NODE_DENSITYMAP


def url_has_error(address) -> str:
    """Check of a web address is live

    Args:
        address (str): The url to check

    Returns:
        str: Any errors reported
    """
    err = ""
    try:
        response = requests.head(address)
        res = response.status_code
        if res >= 400:
            err = response.reason
    except Exception as e:
        err = str(e)
    return err


class FetchPdb(PipelinerJob):
    PROCESS_NAME = "pipeliner.fetch.pdb"
    OUT_DIR = "Fetch"

    def __init__(self):
        super().__init__()

        self.jobinfo.display_name = "Fetch from PDB"
        self.jobinfo.short_desc = "Download atomic models from PDB"
        self.jobinfo.long_desc = "Download atomic models from PDB"
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza"
        self.jobinfo.programs = [ExternalProgram("wget"), ExternalProgram("gzip")]

        self.joboptions["db_id"] = StringJobOption(
            label="PDB ID number",
            help_text="Enter just the number portion of the model's PDB ID",
            is_required=True,
        )
        self.joboptions["format"] = MultipleChoiceJobOption(
            label="Download format:",
            choices=["pdb", "mmCIF"],
            default_value_index=1,
            help_text=(
                "Format to download model files in.  .mmcif is the current "
                "standard but some programs will only use the older .pdb format"
            ),
        )

    def get_address(self) -> str:
        dbid = self.joboptions["db_id"].get_string().lower()
        fmat = self.joboptions["format"].get_string()
        ext = "cif" if fmat == "mmCIF" else "ent"
        fx = "" if fmat == "mmCIF" else "pdb"
        addy = (
            f"https://files.wwpdb.org/pub/pdb/data/structures/all/{fmat}/{fx}{dbid}"
            f".{ext}.gz"
        )
        return addy

    def validate_joboptions(self) -> List[JobOptionValidationResult]:
        addy = self.get_address()
        dbid = self.joboptions["db_id"].get_string().lower()
        err = url_has_error(addy)
        if err:
            return [
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["db_id"]],
                    message=f"PDB entry {dbid}{err}",
                )
            ]
        return []

    def get_commands(self) -> List[List[str]]:
        address = self.get_address()
        coms = [["wget", address, "-P", self.output_dir]]

        fname = os.path.join(self.output_dir, address.split("/")[-1])
        coms.append(["gzip", "-d", fname])

        unzipped = fname.strip(".gz")
        if unzipped.endswith(".ent"):
            coms.append(["mv", unzipped, unzipped.replace(".ent", ".pdb")])
            unzipped = unzipped.replace(".ent", ".pdb")

        self.output_nodes.append(create_node(unzipped, NODE_ATOMCOORDS, ["from_pdb"]))

        return coms

    def get_pdb_cite(self):
        # find the primary citation
        on = self.output_nodes[0].name
        ref_vals = {}
        try:
            if on.endswith(".cif"):
                md = cif.read(on).sole_block()
                # authors
                aut_table = md.find("_citation_author.", ["citation_id", "name"])
                authors = []
                if aut_table:
                    for i in aut_table:
                        if i[0] == "primary":
                            authors.append(i[1].strip("'"))
                    if len(authors) > 5:
                        authors = authors[:5] + ["et al."]

                cif_info = {
                    "title": md.find_pair("_citation.title"),
                    "journal": md.find_pair("_citation.journal_abbrev"),
                    "vol": md.find_pair("_citation.journal_volume"),
                    "fp": md.find_pair("_citation.page_first"),
                    "year": md.find_pair("_citation.year"),
                    "pubmed": md.find_pair("_citation.pdbx_database_id_PubMed"),
                    "doi": md.find_pair("_citation.pdbx_database_id_DOI"),
                }

                for i in cif_info:
                    if i:
                        ref_vals[i] = cif_info[i][1].strip("'")
                    else:
                        ref_vals[i] = "Unavailable"

                res_xtal = md.find_pair("_reflns.d_resolution_high")
                res_em = md.find_pair("_em_3d_reconstruction.resolution")
                if res_xtal:
                    reso = res_xtal[1].strip("'")
                elif res_em:
                    reso = res_em[1].strip("'")
                else:
                    reso = "Resolution not available"

                citation = Ref(
                    authors=authors,
                    title=ref_vals["title"],
                    year=ref_vals["year"],
                    volume=ref_vals["vol"],
                    pages=ref_vals["fp"],
                    doi=ref_vals["doi"],
                    pubmed=ref_vals["pubmed"],
                    journal=ref_vals["journal"],
                )

            elif on.endswith(".pdb"):
                subs = {"AUT": "", "TIT": "", "REF": "", "PMI": "", "DOI": ""}
                with open(self.output_nodes[0].name, "r") as pdb:
                    lines = pdb.readlines()
                reso = "X"
                for ln in lines:
                    if ln[:6] == "REMARK" and ln[11:22] == "RESOLUTION.":
                        reso = ln[26:30]
                    if ln[:4] == "JRNL":
                        sub = ln[12:15]
                        subs[sub] += ln[19:].strip() + " "
                authors = subs["AUT"].split(",")
                authors = [
                    ".".join(map(lambda s: s.strip().capitalize(), x.split(".")))
                    for x in authors
                ]
                if len(authors) > 5:
                    authors = authors[:5] + ["et al."]

                ref_vals["journal"] = subs["REF"][0:27].capitalize().strip()
                ref_vals["vol"] = subs["REF"][33:36].strip()
                ref_vals["fp"] = subs["REF"][38:42].strip()
                ref_vals["year"] = subs["REF"][43:47]
                ref_vals["title"] = subs["TIT"].capitalize().replace("\n", "")
                ref_vals["pubmed"] = subs["PMI"].strip()
                ref_vals["doi"] = subs["DOI"].strip()

                citation = Ref(
                    authors=authors,
                    title=ref_vals["title"],
                    year=ref_vals["year"],
                    volume=ref_vals["vol"],
                    pages=ref_vals["fp"],
                    doi=ref_vals["doi"],
                    pubmed=ref_vals["pubmed"],
                    journal=ref_vals["journal"],
                )

            return citation, reso

        except Exception as e:
            tb = str(traceback.format_exc())
            citation = Ref(
                title="Error getting citation info:", journal=str(e), year=tb
            )
            return citation, reso

    def create_results_display(self) -> list:
        cite, reso = self.get_pdb_cite()
        return [
            make_map_model_thumb_and_display(
                outputdir=self.output_dir,
                maps=[],
                models=[self.output_nodes[0].name],
                title=f"Model preview: pdb-{self.joboptions['db_id'].get_string()}; "
                f"{reso} \u212B",
            ),
            create_results_display_object(
                "text",
                title="Literature reference",
                display_data=str(cite),
                associated_data=[self.output_nodes[0].name],
            ),
        ]

    def get_additional_reference_info(self):
        return [self.get_pdb_cite()[0]]

    def prepare_onedep_data(self) -> list:
        dbid = self.joboptions["db_id"].get_string().lower()
        return [
            other_startup_model_type_entry(
                startup_model=f"PDB entry {dbid} downloaded from www.rcsb.org",
                details=DEPOSITION_COMMENT,
            )
        ]


class FetchEmdb(PipelinerJob):
    PROCESS_NAME = "pipeliner.fetch.emdb"
    OUT_DIR = "Fetch"

    def __init__(self):
        super().__init__()

        self.jobinfo.display_name = "Fetch from EMDB"
        self.jobinfo.short_desc = "Download maps from EMDB"
        self.jobinfo.long_desc = "Download maps from EMDB"
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza"
        self.jobinfo.programs = [ExternalProgram("wget"), ExternalProgram("gzip")]

        self.joboptions["db_id"] = StringJobOption(
            label="EMDB ID number",
            help_text="Enter just the number portion of the model's EMDB ID",
            is_required=True,
        )

        self.joboptions["get_associated_models"] = BooleanJobOption(
            label="Also download associated model files?",
            help_text="If true the pipeliner will also attempt to get any "
            "associated model files from the pdb",
            default_value=False,
        )

        self.joboptions["asoc_mod_format"] = MultipleChoiceJobOption(
            label="Format for associated models:",
            help_text="Choose what format to download the associated models in.",
            choices=["mmCIF", "pdb"],
            default_value_index=0,
            deactivate_if=[("get_associated_models", "=", False)],
        )

        self.joboptions["get_halfmaps"] = BooleanJobOption(
            label="Download associated halfmaps if available?",
            help_text="Get any halfmaps that were deposited with the map",
            default_value=False,
        )

    def get_addresses(self) -> Tuple[str, str]:
        dbid = self.joboptions["db_id"].get_string().lower()
        addy_map = (
            f"https://files.wwpdb.org/pub/emdb/structures/EMD-{dbid}/map/emd_{dbid}"
            ".map.gz"
        )
        addy_metadata = (
            f"https://files.wwpdb.org/pub/emdb/structures/EMD-{dbid}/header/emd-{dbid}"
            ".xml"
        )
        return addy_map, addy_metadata

    def halfmap_addresses(self) -> Tuple[str, str]:
        db_id = self.joboptions["db_id"].get_string().lower()
        hm1 = (
            f"https://ftp.ebi.ac.uk/pub/databases/emdb/structures/EMD-{db_id}/other/"
            f"emd_{db_id}_half_map_1.map.gz"
        )
        hm2 = (
            f"https://ftp.ebi.ac.uk/pub/databases/emdb/structures/EMD-{db_id}/other/"
            f"emd_{db_id}_half_map_2.map.gz"
        )
        return hm1, hm2

    def extend_com_for_halfmaps(self):
        hm_coms = []
        # check for the availability for halfmaps
        hm1_addy, hm2_addy = self.halfmap_addresses()
        hmerr = ""
        for hm in [hm1_addy, hm2_addy]:
            hmerr += url_has_error(hm)
        if not hmerr:
            for halfmap in [hm1_addy, hm2_addy]:
                zipped = os.path.join(self.output_dir, halfmap.split("/")[-1])
                hm_coms.extend(
                    [
                        ["wget", halfmap, "-P", self.output_dir],
                        ["gzip", "-d", zipped],
                        [
                            "mv",
                            zipped.strip(".gz"),
                            zipped.replace(".map.gz", ".mrc"),
                        ],
                    ]
                )
        return hm_coms

    def validate_joboptions(self) -> List[JobOptionValidationResult]:
        addy = self.get_addresses()[0]
        dbid = self.joboptions["db_id"].get_string().lower()
        err = url_has_error(addy)
        if err:
            return [
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["db_id"]],
                    message=f"EMDB entry {dbid} {err}",
                )
            ]
        if (
            self.joboptions["get_halfmaps"].get_boolean()
            and not self.extend_com_for_halfmaps()
        ):
            return [
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["get_halfmaps"]],
                    message=(
                        "Halfmaps were requested but none are available for this entry"
                    ),
                )
            ]

        return []

    def get_commands(self) -> List[List[str]]:
        map_address, md_address = self.get_addresses()
        coms = [
            ["wget", md_address, "-P", self.output_dir],
            ["wget", map_address, "-P", self.output_dir],
        ]

        fname = os.path.join(self.output_dir, map_address.split("/")[-1])
        coms.append(["gzip", "-d", fname])

        unzipped = fname.strip(".gz")
        coms.append(["mv", unzipped, unzipped.replace(".map", ".mrc")])
        unzipped = unzipped.replace(".map", ".mrc")
        self.output_nodes.append(create_node(unzipped, NODE_DENSITYMAP, ["from_emdb"]))

        # do the halfmaps
        if self.joboptions["get_halfmaps"].get_boolean():
            id = self.joboptions["db_id"].get_string()
            hm_coms = self.extend_com_for_halfmaps()
            if hm_coms:
                coms.extend(hm_coms)
                self.output_nodes.append(
                    create_node(
                        os.path.join(self.output_dir, f"emd_{id}_half_map_1.mrc"),
                        NODE_DENSITYMAP,
                        ["halfmap"],
                    )
                )
                self.output_nodes.append(
                    create_node(
                        os.path.join(self.output_dir, f"emd_{id}_half_map_2.mrc"),
                        NODE_DENSITYMAP,
                        ["halfmap"],
                    )
                )

        return coms

    def get_xml_root(self):
        dbid = self.joboptions["db_id"].get_string()
        xml = os.path.join(self.output_dir, f"emd-{dbid}.xml")
        return Et.parse(xml).getroot()

    def get_additional_reference_info(self):
        # citation info
        root = self.get_xml_root()
        jp = "./crossreferences/citation_list/primary_citation/journal_citation"
        jcite = root.findall(jp)[0]
        pub = jcite.attrib["published"]
        is_published = True if pub == "true" else False

        if is_published:
            doi, pubmed = "", ""
            authors = jcite.findall("./author")
            auth_list = [str(x.text) for x in authors]
            if len(auth_list) > 5:
                auth_list = auth_list[:5] + ["et al."]
            title = str(jcite.findall("./title")[0].text)
            journal = str(jcite.findall("./journal_abbreviation")[0].text)
            year = str(jcite.findall("./year")[0].text)
            ers = jcite.findall("./external_references")
            for er in ers:
                if er.attrib["type"] == "DOI":
                    doi = f" DOI: {er.text}"
                if er.attrib["type"] == "PUBMED":
                    pubmed = f" PubMed: {er.text}"

            return [
                Ref(
                    authors=auth_list,
                    title=title,
                    journal=journal,
                    year=year,
                    doi=doi,
                    pubmed=pubmed,
                )
            ]

    def parse_emdb_header(
        self,
    ) -> Tuple[str, str, str, str, str, List[List[str]], List[List[str]]]:
        title, citation, dims, px, reso = "", "", "", "", ""
        asoc_mods: List[List[str]] = []
        root = self.get_xml_root()
        entryname = root.attrib["emdb_id"]

        try:
            ref = self.get_additional_reference_info()[0]
            citation = str(ref)
        except TypeError:
            citation = ""

        # map info
        mappath = "./map"
        mapdata = root.findall(mappath)[0]
        col = mapdata.findall("./dimensions/col")[0].text
        row = mapdata.findall("./dimensions/row")[0].text
        sec = mapdata.findall("./dimensions/sec")[0].text
        dims = f"{col} x {row} x {sec}"
        px = str(mapdata.findall("./pixel_spacing/x")[0].text)
        sdpath = "./structure_determination_list/structure_determination"
        method = str(root.findall(sdpath + "/method")[0].text)
        reso = str(
            root.findall(
                f"{sdpath}/{method.lower()}_processing/final_reconstruction/resolution"
            )[0].text
        )

        # associated maps
        ap = "./crossreferences/emdb_list/emdb_reference"
        allmaps = root.findall(ap)
        map_details = {}
        for i in allmaps:
            mapid = i.findall("emdb_id")[0].text
            the_d = i.findall("details")
            the_map_deets = "None available" if not the_d else the_d[0].text
            map_details[mapid] = the_map_deets

        title = f"{entryname}: {map_details[entryname]}"
        asoc_maps = []
        for m in map_details:
            if m != entryname:
                asoc_maps.append([m, map_details[m]])

        # associated models
        ap = "./crossreferences/pdb_list/pdb_reference"
        asocmod = root.findall(ap)
        for i in asocmod:
            asoc_mods.append([str(i.findall("pdb_id")[0].text)])

        return title, citation, dims, px, reso, asoc_maps, asoc_mods

    def get_pdb_asoc_address(self, pdbid) -> str:

        fmat = self.joboptions["asoc_mod_format"].get_string()
        ext = "cif" if fmat == "mmCIF" else "ent"
        fx = "" if fmat == "mmCIF" else "pdb"
        addy = (
            f"https://files.wwpdb.org/pub/pdb/data/structures/all/{fmat}/{fx}"
            f"{pdbid.lower()}.{ext}.gz"
        )
        return addy

    def post_run_actions(self):
        if not self.joboptions["get_associated_models"].get_boolean():
            return
        root = self.get_xml_root()
        ap = "./crossreferences/pdb_list/pdb_reference"
        asocmod = root.findall(ap)
        asoc_mod_coms = []
        asoc_mod_nodes = []
        for i in asocmod:
            pdbid = str(i.findall("pdb_id")[0].text)
            addy = self.get_pdb_asoc_address(pdbid)
            asoc_mod_coms.append(["wget", addy, "-P", self.output_dir])

            fname = os.path.join(self.output_dir, addy.split("/")[-1])
            asoc_mod_coms.append(["gzip", "-d", fname])

            unzipped = fname.strip(".gz")
            if unzipped.endswith(".ent"):
                asoc_mod_coms.append(["mv", unzipped, unzipped.replace(".ent", ".pdb")])
                unzipped = unzipped.replace(".ent", ".pdb")
            asoc_mod_nodes.append(create_node(unzipped, NODE_ATOMCOORDS, ["from_pdb"]))

        for n, com in enumerate(asoc_mod_coms):
            print(f"Executing ({n}/{len(asoc_mod_coms)}): {' '.join(com)}")
            subprocess.run(com)

        # just to make sure the models have unzipped
        sleep(2)
        for node in asoc_mod_nodes:
            if os.path.isfile(node.name):
                self.output_nodes.append(node)
            else:
                print(f"Error getting associated file: {node.name}")

    def create_results_display(self) -> list:
        title, citation, dims, px, reso, asoc_maps, asoc_mods = self.parse_emdb_header()
        params = create_results_display_object(
            "table",
            title=title,
            headers=[
                "Resolution (\u212B)",
                "Map dimensions (px)",
                "pixel size (\u212B)",
            ],
            table_data=[[reso, dims, px]],
            associated_data=[self.output_nodes[0].name],
        )
        mods = []
        for on in self.output_nodes:
            if on.format in ["pdb", "cif", "mmcif"]:
                mods.append(on.name)
        the_map = make_map_model_thumb_and_display(
            outputdir=self.output_dir,
            maps=[self.output_nodes[0].name],
            models=mods,
            maps_opacity=[0.5] if mods else [1.0],
            title="Map and associated models preview" if mods else "Map preview",
        )
        dispobjs: List[ResultsDisplayObject] = [params, the_map]

        if citation:
            litref = create_results_display_object(
                "text",
                title="Literature reference",
                display_data=citation,
                associated_data=[self.output_nodes[0].name],
            )
        else:
            litref = create_results_display_object(
                "text",
                title="No reference available",
                display_data="No reference available for this entry",
                associated_data=[self.output_nodes[0].name],
            )
        dispobjs.append(litref)

        if asoc_maps:
            a_maps = create_results_display_object(
                "table",
                title="Associated EMDB entries",
                headers=["EMDB ID", "Details"],
                table_data=asoc_maps,
                associated_data=[self.output_nodes[0].name],
            )
            dispobjs.append(a_maps)
        if asoc_mods:
            a_mods = create_results_display_object(
                "table",
                title="Associated PDB entries",
                headers=["PDB ID"],
                table_data=asoc_mods,
                associated_data=[self.output_nodes[0].name],
            )
            dispobjs.append(a_mods)

        id = self.joboptions["db_id"].get_string()
        hm1 = os.path.join(self.output_dir, f"emd_{id}_half_map_1.mrc")
        hm2 = os.path.join(self.output_dir, f"emd_{id}_half_map_2.mrc")
        if self.joboptions["get_halfmaps"].get_boolean():
            dispobjs.append(
                make_map_model_thumb_and_display(
                    outputdir=self.output_dir,
                    maps=[hm1, hm2],
                    title=f"Halfmaps for emd-{id}",
                )
            )

        return dispobjs

    def prepare_onedep_data(self) -> list:
        dbid = self.joboptions["db_id"].get_string()
        return [
            other_startup_model_type_entry(
                startup_model=f"EMDB entry {dbid} downloaded from www.rcsb.org",
                details=DEPOSITION_COMMENT,
            )
        ]


class FetchAlphaFold(PipelinerJob):
    PROCESS_NAME = "pipeliner.fetch.alphafold"
    OUT_DIR = "Fetch"

    def __init__(self):
        super().__init__()

        self.jobinfo.display_name = "Fetch atomic model predictions from AlphaFold DB"
        self.jobinfo.short_desc = (
            "Download predicted structures from the AlphaFold Protein Structure "
            "Database"
        )
        self.jobinfo.long_desc = (
            "Download predicted structures from the AlphaFold Protein Structure "
            "Database"
        )
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza"
        self.jobinfo.programs = [ExternalProgram("wget")]

        self.jobinfo.references = [
            Ref(
                authors=[
                    "Varadi M",
                    "Anyango S",
                    "Deshpande M",
                    "Nair S",
                    "Natassia C",
                    "Yordanova G",
                    "Yuan D",
                    "Stroe O",
                    "Wood G",
                    "Laydon A",
                    "Žídek A",
                    "Green T",
                    "Tunyasuvunakool K",
                    "Petersen S",
                    "Jumper J",
                    "Clancy E",
                    "Green R",
                    "Vora A",
                    "Lutfi M",
                    "Figurnov M",
                    "Cowie A",
                    "Hobbs N",
                    "Kohli P",
                    "Kleywegt G",
                    "Birney E",
                    "Hassabis D",
                    "Velankar S",
                ],
                title="AlphaFold Protein Structure Database: massively expanding the "
                "structural coverage of protein-sequence space with high-accuracy"
                " models",
                journal="Nucleic Acids Research",
                year="2021",
                volume="50",
                issue="D1",
                pages="D439–D444",
                doi="10.1093/nar/gkab1061",
            ),
            Ref(
                authors=[
                    "Jumper J",
                    "Evans R",
                    "Pritzel A",
                    "Green T",
                    "Figurnov M",
                    "Ronneberger O",
                    "Tunyasuvunakool K",
                    "Bates R",
                    "Žídek A",
                    "Potapenko A",
                    "Bridgland A",
                    "Meyer C",
                    "Kohl SAA",
                    "Ballard AJ",
                    "Cowie A",
                    "Romera-Paredes B",
                    "Nikolov S",
                    "Jain R",
                    "Adler J",
                    "Back T",
                    "Petersen S",
                    "Reiman D",
                    "Clancy E",
                    "Zielinski M",
                    "Steinegger M",
                    "Pacholska M",
                    "Berghammer T",
                    "Bodenstein A",
                    "Silver D",
                    "Vinyals O",
                    "Senior AW",
                    "Kavukcuoglu K",
                    "Kohli P",
                    "Hassabis D",
                ],
                title="Highly accurate protein structure prediction with AlphaFold",
                journal="Nature",
                year="2021",
                volume="596",
                pages="583-589",
                doi="10.1038/s41586-021-03819-2",
            ),
        ]

        self.joboptions["uniprot_id"] = StringJobOption(
            label="UNIPROT accession number",
            help_text="Enter the protein's UNIPORT accession number",
            is_required=True,
        )
        self.joboptions["format"] = MultipleChoiceJobOption(
            label="Download format:",
            choices=["pdb", "mmCIF"],
            default_value_index=1,
            help_text=(
                "Format to download model files in.  .mmcif is the current "
                "standard but some programs will only use the older .pdb format"
            ),
        )

    def get_addresses(self) -> Tuple[str, str]:
        dbid = self.joboptions["uniprot_id"].get_string().upper()
        fmat = self.joboptions["format"].get_string()
        ext = "cif" if fmat == "mmCIF" else "pdb"
        model = f"https://alphafold.ebi.ac.uk/files/AF-{dbid}-F1-model_v4.{ext}"
        plot = (
            f"https://alphafold.ebi.ac.uk/files/AF-{dbid}-F1-predicted_aligned_"
            f"error_v4.json"
        )
        return model, plot

    def validate_joboptions(self) -> List[JobOptionValidationResult]:
        addy = self.get_addresses()[0]
        dbid = self.joboptions["uniprot_id"].get_string().upper()
        err = url_has_error(addy)

        if err:
            return [
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["uniprot_id"]],
                    message=f"Entry {dbid} {err}",
                )
            ]
        return []

    def get_commands(self) -> List[List[str]]:
        model, plot = self.get_addresses()
        coms = [
            ["wget", model, "-P", self.output_dir],
            ["wget", plot, "-P", self.output_dir],
        ]

        outname = os.path.join(self.output_dir, os.path.basename(model))
        self.output_nodes.append(
            create_node(outname, NODE_ATOMCOORDS, ["simulated", "alphafold"])
        )

        return coms

    def create_results_display(self) -> list:
        return [
            make_map_model_thumb_and_display(
                outputdir=self.output_dir,
                maps=[],
                models=[self.output_nodes[0].name],
                title=(
                    "Model preview: Alpha fold prediction UNIPROT-"
                    f"{self.joboptions['uniprot_id'].get_string().upper()}; "
                ),
            ),
            # TODO: Add a heatmap plot from the predicted accuracy matrix
        ]

    def prepare_onedep_data(self) -> list:
        dbid = self.joboptions["uniprot_id"].get_string().lower()
        return [
            other_startup_model_type_entry(
                startup_model=f"Alphafold prediction for UNIPROT-{dbid}",
                details=DEPOSITION_COMMENT,
            )
        ]
