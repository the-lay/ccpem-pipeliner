#! /usr/bin/env python3

#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import sys
import os
import json


def main():
    (
        input_size,
        mask_radius,
        batch_size,
        good_path,
        bad_path,
        pretrained_weights,
        outname,
        nb_epoch,
        nb_early_stop,
    ) = sys.argv[1:]

    pretrained_weights = "" if pretrained_weights == "no_ptw" else pretrained_weights

    config = {
        "model": {"input_size": [int(input_size), int(input_size)]},
        "train": {
            "batch_size": int(batch_size),
            "good_path": good_path,
            "bad_path": bad_path,
            "pretrained_weights": pretrained_weights,
            "saved_weights_name": f"{outname}trained_model.h5",
            "learning_rate": 1e-4,
            "nb_epoch": int(nb_epoch),
            "nb_early_stop": int(nb_early_stop),
        },
    }
    if mask_radius != "no_mr":
        config["model"]["mask_radius"] = int(mask_radius)

    with open(os.path.join(outname, "training_config.json"), "w") as outfile:
        json.dump(config, outfile, indent=4)


if __name__ == "__main__":
    main()
