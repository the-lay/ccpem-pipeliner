#!/usr/bin/env python

#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
import shutil
from datetime import datetime
from typing import Optional, Tuple, Union

from pipeliner.utils import make_pretty_header, wrap_text, run_subprocess
from pipeliner.api.manage_project import PipelinerProject
from pipeliner.starfile_handler import JobStar
from pipeliner.job_factory import (
    gather_all_jobtypes,
    new_job_of_type,
)
from pipeliner.job_options import (
    JobOption,
    SAMPLING,
    NODETYPE,
    NODETYPE_OPTIONS,
    GAIN_ROTATION,
    CTF_FIT,
    GAIN_FLIP,
)
from pipeliner.utils import print_nice_columns


class InteractiveJobEntry:
    """A class for interactive job creation sessions"""

    def __init__(self, joboptions_dict, pipeline, is_continue=False):
        """
        Initialize an :class:`InteractiveJobEntry`

        Args:

        job (dict): :class:pipeliner.job_options.JobOption` objects for the current "
            job, keys are the job.star parameter names
        pipeline (:`class:pipeliner.project_graph.ProjectGraph`): The pipeliner
            ProjectGraph for the current project
        jobop_values (dict): The previous values for all joboptions if the job is
            being continued. Keys are the job.star parameter names
        jobop_keys (dict): Translates run.job style labels to job.star parameter
            names
        is_continue (bool): If the interactive session is creating a job continuation

        """

        self.job = joboptions_dict
        self.pipeline = pipeline
        self.jobop_values = {}  # {variable_name: old value}
        self.jobop_keys = {}  # {label: variable name}
        self.is_continue = is_continue

    def get_jo_vals(self, jo: JobOption) -> str:
        """Display job option label and default value, takes user input

        This function is used when creating a new job only there is a separate
        function for if the job being created is a continuation

        Args:
        jo (:class:`pipeliner.job_options.JobOption`): The JobOption selected

        Returns:
            str: The user entered value or the joboption's default value

        """

        print(make_pretty_header(jo.label.replace(":", "")))

        default_value = str(jo.default_value)

        print("Default value: " + default_value)
        val = str(input(jo.label + " "))
        if val == "":
            val = default_value
        if val == "-h":
            print("\n")
            wrap_text(str(jo.help_text))
            return self.get_jo_vals(jo)
        if "$" in val:
            com = val[1:].lstrip()
            run_subprocess(com.split())
            return self.get_jo_vals(jo)
        if val == "SKIP":
            val = ""
        return val

    def int_joboptions(self):
        """Get user input data for all joboptions in a new job

        Returns:
            None
        """
        joboptions = self.job.joboptions
        for joboption in joboptions:
            jo = joboptions[joboption]
            if "Continue from here" not in jo.label:
                pass_check = False
                while pass_check is False:
                    value = self.get_jo_vals(jo)
                    pass_check, err_msg, value = self.check_jo_value(jo, value)
                    print(err_msg)

            self.job.joboptions[joboption].value = value

    def get_cont_jo_vals(self, jo: JobOption) -> str:
        """Display job option label and default value, takes user input

        This function is used when creating a continuation of an existing job.
        The default value used is the previously entered value.

        Args:
        jo (:class:`pipeliner.job_options.JobOption`): The JobOption selected

        Returns:
            str: The user entered value or the previous value

        """
        jo_var = self.jobop_keys[jo.label]
        default_value = str(self.jobop_values[jo_var])
        if default_value == '""' or default_value == "''":
            default_value = ""

        if jo.in_continue:
            print(make_pretty_header(jo.label.replace(":", "")))

            print("Previous value was: " + default_value)
            val = input(jo.label + " ")
            if val == "":
                val = default_value
            if val == "-h":
                print("\n")
                wrap_text(str(jo.help_text))
                return self.get_cont_jo_vals(jo)
            if "$" in val:
                com = val[1:].lstrip()
                run_subprocess(com.split())
                return self.get_cont_jo_vals(jo)
            if jo.label == "Continue from here: " and (
                "optimiser.star" not in val or self.job.output_dir not in val
            ):
                print(
                    "ERROR: An optimiser.star file from this run "
                    f"({self.job.output_dir}) must be specified!"
                )
                return self.get_cont_jo_vals(jo)

            else:
                if val == "SKIP":
                    val = ""
                return val
        else:
            print(f"Job option {jo.label} cannot be altered in continue jobs")
            print("Using value: " + default_value)
            return default_value

    def cont_joboptions(self):
        """Get user input data for all job parameters in a continued job
           Check the user input value and set it in self.joboptions if is
           passes the check

        Returns:
            None
        """

        for jo in self.job.joboptions:
            jobop = self.job.joboptions[jo]
            if jobop.in_continue:
                pass_check = False
                while pass_check is False:
                    value = self.get_cont_jo_vals(jobop)
                    pass_check, err_msg, value = self.check_jo_value(jobop, value)
                    print(err_msg)

                self.job.joboptions[jo].value = value

    def check_jo_value(
        self, joboption: JobOption, value: str
    ) -> Tuple[bool, Optional[str], Union[str, float]]:
        """Checks the value entered is the correct format for the job option type

        Args:
            joboption (:class:`pipeliner.job_options.JobOption`): The JobOption
                being checked
            value (str): User entered value being checked

        Returns:
            tuple: contains the items:

            [0] bool: Did the test pass
            [1] str: Error message (if any)
            [2] str: The user entered value
        """

        jo_type = joboption.joboption_type
        valtest = False
        err_msg: Optional[str] = ""
        if jo_type in ["FILENAME", "INPUTNODE"]:
            valtest = os.path.isfile(value)
            if value in ["", "SKIP"] or value is None:
                print(
                    "WARNING: No file specified\nIf this field "
                    "is optional (EX. mask in a 3D refine) there is no"
                    " problem.\nIf it is required the job will crash"
                )
                valtest = True
            elif not valtest:
                print(
                    f"ERROR: File {value} does not exist. Enter 'SKIP' to skip"
                    " putting a file in this field"
                )

        if jo_type == "MULTIPLECHOICE":
            radios = {
                "SAMPLING": SAMPLING,
                "NODETYPE": NODETYPE,
                "NODETYPE_OPTIONS": NODETYPE_OPTIONS,
                "GAIN_ROTATION": GAIN_ROTATION,
                "CTF_FIT": CTF_FIT,
                "GAIN_FLIP": GAIN_FLIP,
            }
            valtest = value in radios[joboption.default_value]
            choices = "\n".join(radios[joboption.default_value])
            err_msg = (
                f"ERROR: {value} is not a valid option\n "
                f"Available choices are: {choices}"
            )

        if jo_type == "BOOLEAN":
            valtest = value in ["Yes", "No"]
            err_msg = (
                "ERROR: You must enter 'Yes' or 'No'." " The value is case sensitive"
            )

        new_value: Union[str, float] = value

        if jo_type == "SLIDER":
            try:
                new_value = float(value)
                valtest = True
            except ValueError:
                valtest = False
            err_msg = "ERROR: Value must be a number"

        if jo_type == "TEXTBOX":
            valtest = True
            try:
                new_value = float(value)
            except ValueError:
                pass
            err_msg = "Can't do no wrong here..."
        if valtest:
            err_msg = None
        return (valtest, err_msg, new_value)


def interactive_job_create():
    """
    Runs an interactive job creation session

    If created in a directory with an active project allows for
    the creation of a continuation jobstar file for any completed
    jobs in the project.  Otherwise only allows making jobstars for new jobs.

    Returns:
        list: A list of the files created

        This will be both a run.job and job.star file for a new job but only a
        continue_job.star file for a continuation

    """

    def test_pick(message, opt_range):
        """
        Checks that a selection is in the range of the options

        Args:
            message (str): The message user sees on the input line
            opt_range (int): The numbe of options available

        Returns:
            int: The option selected, if it is valid, `None` if the selection
                was invalid
        """
        try:
            pick = input(message)
            in_jobtype = int(pick)
            assert in_jobtype in range(opt_range + 1)
            return in_jobtype - 1
        except AssertionError or ValueError:
            print("ERROR: Select a valid job type")
            return None

    proj = PipelinerProject(make_new_project=True)
    try:
        proj.initialize_existing_project()
        continuation = input("Is this a continuation of a previous job (y/n): ")
    except ValueError:
        continuation = False

    print(make_pretty_header("Interactive job creator"))

    n = 1
    procdict = {}

    if continuation:
        is_continue = True
        continue_list = list()
        for job in proj.pipeline.process_list:
            procdict[n] = job
            continue_list.append(f"{n:>2d}) {job.name}")
            n += 1
        the_job = None
        print_nice_columns(continue_list, "ERROR: No jobs found")
        while not the_job:
            the_job = test_pick(
                "Which job to continue?: ", len(proj.pipeline.process_list)
            )
        old_job = procdict[the_job]
        job = new_job_of_type(old_job.type)
        job.output_dir = old_job.name
        job.alias = old_job.alias
        job.is_continue = True
        continue_options = []
        for jo in job.joboptions:
            if job.joboptions[jo].in_continue:
                continue_options.append(jo)
        ojob_op_vals = [job.joboptions[x].label for x in continue_options]
        jobop_keys = dict(zip(ojob_op_vals, continue_options))
        new_cont_job = InteractiveJobEntry(job, proj.pipeline)
        new_cont_job.jobop_keys = jobop_keys
        job_star = JobStar(old_job.name + "job.star")

        print(
            make_pretty_header(
                "Enter the job options for the continuation of job"
                + old_job.name
                + "\nNote: Some job options cannot be altered from"
                " the values in the original run"
            )
        )
        print(
            "Leave blank to use the previous value\nType -h for help\nUse a $"
            f" to run unix commands (IE '$ls {job.output_dir}' to find file"
            " names\n"
        )

        new_cont_job.jobop_values = job_star.joboptions

        new_cont_job.cont_joboptions()

    else:
        # get all the job types
        job_dict = gather_all_jobtypes()
        jl_keys = list(job_dict)
        jl_keys.sort()
        jobs_list = list()
        for n, jobtype in enumerate(jl_keys):
            procdict[n + 1] = job_dict[jobtype].PROCESS_NAME
            jobs_list.append(f"{n+1:>2d}) {jobtype}")
        print_nice_columns(jobs_list, "ERROR: No available jobs")
        is_continue = False
        raw_jobtype = None
        while raw_jobtype is None:
            raw_jobtype = test_pick("Select the type of job: ", len(job_dict))
        jobtype = procdict[raw_jobtype + 1]
        job = new_job_of_type(jobtype)

        print(
            make_pretty_header(f"Enter values for the new {jl_keys[raw_jobtype]} job")
        )
        print(
            "Leave blank for previous value\nType -h for help\nUse a $ to run unix"
            " commands (IE '$ls Class2D/job008' to find file names) "
        )

        new_int_job = InteractiveJobEntry(job, proj.pipeline)
        new_int_job.int_joboptions()

    if not os.path.isdir("RunFiles"):
        os.makedirs("RunFiles")

    if not is_continue:
        now = datetime.now()
        dt_string = now.strftime("%y-%m-%d_%H%M%S")
        filename = job.type + "_" + dt_string
        job.write(fn="RunFiles/")
        job.write_jobstar("RunFiles/")
        job_file = filename + "_run.job"
        jobstar_file = filename + "_job.star"
        shutil.move("RunFiles/run.job", os.path.join("RunFiles/", job_file))
        shutil.move("RunFiles/job.star", os.path.join("RunFiles/", jobstar_file))
        print(make_pretty_header("Finished!"))
        print(
            f"Wrote job files {job_file} and {jobstar_file} in RunFiles/\nEither of "
            "these files can be used to run the job using --run_job"
        )
        return [job_file, jobstar_file]
    else:
        continue_file = os.path.join(old_job.name, "continue_")
        job.write_jobstar(continue_file)
        run_subprocess(["ls", "-al", old_job.name])
        print(make_pretty_header("Finished!"))
        print(
            f"Wrote {0}continue_job.star.\nThis file can be used to continue "
            "the job using --continue_job {old_job.name}"
        )
        return [continue_file + "job.star"]
