#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

"""
==========
JobOptions
==========

JobOptions are used to store parameters for jobs.  They contain all the info
necessary for on-the-fly GUI generation.
"""
from __future__ import annotations

import os.path
import re
import shutil
from glob import glob
from typing import Any, List, Optional, Union

from pipeliner.data_structure import (
    NODE_MOVIES_LABEL,
    NODE_MICS_LABEL,
    NODE_MIC_COORDS_LABEL,
    NODE_PART_DATA_LABEL,
    NODE_REFS_LABEL,
    NODE_3DREF_LABEL,
    NODE_MASK_LABEL,
    NODE_HALFMAP_LABEL,
    NODE_OTHER_LABEL,
)

from pipeliner.node_factory import create_node
from pipeliner.nodes import Node

# values that should be treated as True
TRUES = ["yes", "true", "y", "1"]
FALSES = ["no", "false", "n", "0"]

# RELION 'radio' (actually pull down menu) standard options
SAMPLING = [
    "30 degrees",
    "15 degrees",
    "7.5 degrees",
    "3.7 degrees",
    "1.8 degrees",
    "0.9 degrees",
    "0.5 degrees",
    "0.2 degrees",
    "0.1 degrees",
]

# extensions for different filetypes
# for use with files_extensions() function
EXT_MICROGRAPH_MOVIES = [".mrcs", ".tiff"]
EXT_TOMOGRAMS = [".mrc"]
EXT_STARFILE = [".star"]
EXT_COORDS = [".box", ".star"]
EXT_2DREFS = [".star", ".mrcs"]
EXT_RELION_HALFMAP = ["_unfil.mrc"]
EXT_MRC_MAP = [".mrc"]
EXT_RELION_OPT = ["_optimiser.star"]
EXT_RELION_MODEL = ["_model.star"]


def files_exts(
    name: str = "File", exts: Optional[List[str]] = None, exact: bool = False
) -> str:
    """Produce a description of files and their extensions

    In a format compatible with PyQt5 QFileDialog

    Args:
        name (str): The type of file IE: 'Micrograph movies'
        exts (list): The acceptable extensions or file search strings IE:
            '.txt' to find \\u002a.txt or '_half1.star' for \\u002a_hal1.star
        exact (bool): Match the file name(s) exactly, do not prepend a '\\u002a'

    Returns:
        str: Formatted for PyQt5 QFileDialog: 'name (\\u002aext1 \\u002aext2
            \\u002aext3)'
    """
    if exts is None:
        flag = "(*.*)"
    elif not exact:
        flag = f"({' '.join(['*'+x for x in exts])})"
    else:
        flag = f"({' '.join([x for x in exts])})"

    return f"{name} {flag}"


NODETYPE = [
    "2D micrograph movies (*.mrcs, *.tiff)",
    "2D micrographs/tomograms (*.mrc)",
    "2D/3D particle coordinates (*.box, *_pick.star)",
    "Particles STAR file (.star)",
    # "Movie-particles STAR file (.star)",   NOT USED ANY MORE
    "2D references (.star or .mrcs)",
    "Micrographs STAR file (.star)",
    "3D reference (.mrc)",
    "3D mask (.mrc)",
    "Unfiltered half-map (unfil.mrc)",
    "Other",
]

NODETYPE_OPTIONS = {
    "2D micrograph movies (*.mrcs, *.tiff)": NODE_MOVIES_LABEL,
    "2D micrographs/tomograms (*.mrc)": NODE_MICS_LABEL,
    "2D/3D particle coordinates (*.box, *_pick.star)": NODE_MIC_COORDS_LABEL,
    "Particles STAR file (.star)": NODE_PART_DATA_LABEL,
    # "Movie-particles STAR file (.star)": NOT USED ANY MORE,
    "2D references (.star or .mrcs)": NODE_REFS_LABEL,
    "Micrographs STAR file (.star)": NODE_MICS_LABEL,
    "3D reference (.mrc)": NODE_3DREF_LABEL,
    "3D mask (.mrc)": NODE_MASK_LABEL,
    "Unfiltered half-map (unfil.mrc)": NODE_HALFMAP_LABEL,
    "Other": NODE_OTHER_LABEL,
}

GAIN_ROTATION = [
    "No rotation (0)",
    "90 degrees (1)",
    "180 degrees (2)",
    "270 degrees (3)",
]

CTF_FIT = ["No", "Per-micrograph", "Per-particle"]

GAIN_FLIP = ["No flipping (0)", "Flip upside down (1)", "Flip left to right (2)"]


class JobOption(object):
    """A JobOption stores a parameter, its value, and info about the GUI for that param

    This is a general class with several more specialised class subclasses.

    """

    joboption_type = "ERROR: no job option type set"

    def __init__(
        self,
        *,
        label: str,
        default_value: Any = "",
        help_text: str = "",
        in_continue: bool = False,
        is_required: bool = False,
        required_if: Optional[list] = None,
        deactivate_if: Optional[list] = None,
        only_in_continue: bool = False,
        jobop_group: str = "Main",
    ):
        """Create a JobOption object

        Args:
            label (str): A verbose label for the parameter.  This is what appears in a
                run.job file
            default_value (None) = The default value for the parameter.  The type
                will be set by the specific class method
            help_text (str): Text that will be displayed in the GUI if help is clicked
            in_continue (bool): If this parameter can be modified in a job that is
                continued
            is_required (bool): Is this joboption always required?
            required_if (list): Validation option: a list of logical statements, if all
                are true the joboption will be required by the GUI.  Requires a list of
                tuples: (joboption, operator, value).  Valid operators are: '=', '>',
                '>=', '<', and '<='
            deactivate_if (list): a list of logical statements, if all are true the
                joboption will be deactivated by the GUI. Requires a list of tuples:
                (joboption, operator, value) Valid operators are: '=', '>', '>=', '<',
                and '<='
            only_in_continue (bool): The joboption should only be available if the job
                is being continued

        Raises:
            NotImplementedError: If this class is instantiated directly
        """
        if type(self) == JobOption:
            raise NotImplementedError(
                "Do not create JobOption objects directly - use a sub-class"
            )
        self.label = label
        self.value = default_value
        self.default_value = default_value
        self.help_text = help_text
        self.in_continue = in_continue
        self.is_required = is_required
        self.required_if = required_if if required_if is not None else []
        self.deactivate_if = deactivate_if if deactivate_if is not None else []
        self.only_in_continue = only_in_continue
        self.jobop_group = jobop_group

    def get_string(self, required: bool = False, errormsg: str = "") -> str:
        """Get the value of a JobOption as a string

        Args:
            required (bool): If a value is required for this parameter
            errormsg (str): The message to display if the parameter is required
                and no value was entered

        Returns:
            str: The value

        Raises:
            ValueError: If the value is required and missing
        """
        if required and str(self.value) == "":
            raise ValueError(errormsg)
        return str(self.value)

    def set_string(self, set_to: str) -> None:
        """Set the value of a JobOption to a string

        Args:
            set_to (str): The string to set the value to
        """
        self.value = str(set_to)
        return None

    def validate(self) -> List[JobOptionValidationResult]:
        """Basic validation of input parameters

        will be set by individual JobOption subtypes

        """
        results = []
        if self.is_required and self.value in ["", None]:
            results.append(
                JobOptionValidationResult("error", [self], "Required parameter empty")
            )
        return results

    def check_file(self) -> List[JobOptionValidationResult]:
        """Check that specified input files exist

        Returns:
            str: An error message if the file does not exist
        """
        in_path = shutil.which(self.value)
        is_file = glob(self.value)

        found = any([in_path, is_file])
        if not found:
            return [
                JobOptionValidationResult(
                    "error", [self], f"File {self.value} not found"
                )
            ]
        return []

    def get_input_nodes(self) -> List[Node]:
        """Get the input node(s) related to this job option.

        Most job option types do not create input nodes and so they should rely on this
        default implementation that returns an empty list.

        Options that do create nodes should return a list of the corresponding Node
        objects.
        """
        return []


class StringJobOption(JobOption):
    """Create a job option object for a string parameter.

    Args:
        label (str): A verbose label for the parameter.
        default_value (None) = The default value for the parameter.
        help_text (str): Text that will be displayed in the GUI if help is clicked.
        in_continue (bool): If this parameter can be modified in a job that is
            continued
    """

    joboption_type = "STRING"

    def __init__(
        self,
        *,
        label: str,
        default_value: str = "",
        help_text: str = "",
        in_continue: bool = False,
        only_in_continue: bool = False,
        is_required: bool = False,
        required_if: Optional[list] = None,
        deactivate_if: Optional[list] = None,
        validation_regex: Optional[str] = None,
        jobop_group: str = "Main",
    ):
        super().__init__(
            label=label,
            default_value=default_value,
            help_text=help_text,
            in_continue=in_continue,
            only_in_continue=only_in_continue,
            is_required=is_required,
            required_if=required_if if required_if is not None else [],
            deactivate_if=deactivate_if if deactivate_if is not None else [],
            jobop_group=jobop_group,
        )
        self.validation_regex = validation_regex

    def validate(self) -> List[JobOptionValidationResult]:
        """Basic validation of the input parameters"""
        results = super().validate()
        if results:
            return results
        if self.validation_regex is not None:
            reg = re.compile(self.validation_regex)
            if not reg.match(self.value):
                results.append(
                    JobOptionValidationResult(
                        "error",
                        [self],
                        f"Value format must match pattern '{self.validation_regex}'",
                    )
                )
        return results


class PathJobOption(StringJobOption):
    """A JobOption for file paths or search strings

    Args:
       label (str): A verbose label for the parameter.
       default_value (None) = The default value for the parameter.
       wildcard_allowed (bool): Is the path allowed to have a wildcard?
       is_dir (bool): Is a directory expected rather than a file?
       must_be_abs_path (bool): Does the path hav to be an absolute path? As opposed
           to relative to the project dir
       help_text (str): Text that will be displayed in the GUI if help is clicked.
       in_continue (bool): If this parameter can be modified in a job that is
           continued
    """

    joboption_type = "PATH"

    def __init__(
        self,
        *,
        label: str,
        default_value: str = "",
        help_text: str = "",
        wildcard_allowed: bool = False,
        is_dir: bool = False,
        must_be_in_project: bool = False,
        in_continue: bool = False,
        only_in_continue: bool = False,
        is_required: bool = False,
        required_if: Optional[list] = None,
        deactivate_if: Optional[list] = None,
        validation_regex: Optional[str] = None,
        jobop_group: str = "Main",
    ):
        super().__init__(
            label=label,
            default_value=default_value,
            help_text=help_text,
            in_continue=in_continue,
            only_in_continue=only_in_continue,
            is_required=is_required,
            required_if=required_if if required_if is not None else [],
            deactivate_if=deactivate_if if deactivate_if is not None else [],
            jobop_group=jobop_group,
        )
        self.validation_regex = validation_regex
        self.wildcard_allowed = wildcard_allowed
        self.must_be_in_project = must_be_in_project
        self.is_dir = is_dir

    def validate(self) -> List[JobOptionValidationResult]:
        """Basic validation of the input parameters"""
        results = super().validate()
        if results:
            return results
        if self.validation_regex is not None:
            reg = re.compile(self.validation_regex)
            if not reg.match(self.value):
                results.append(
                    JobOptionValidationResult(
                        "error",
                        [self],
                        f"Value format must match pattern '{self.validation_regex}'",
                    )
                )
        if self.must_be_in_project:
            if any([self.value.startswith(x) for x in ["/", "../", "~/"]]):
                results.append(
                    JobOptionValidationResult(
                        "error",
                        [self],
                        "This field requires a path relative to the project and all "
                        "files must be in the project.  IE: Path cannot start with "
                        "'/' or '../'",
                    )
                )
        if not self.wildcard_allowed and "*" in self.value:
            results.append(
                JobOptionValidationResult(
                    "error",
                    [self],
                    "Wildcards (*) are not allowed in this parameter",
                )
            )

        return results

    def check_file(self) -> List[JobOptionValidationResult]:
        """Check that specified input files exist

        Returns:
            str: An error message if the file does not exist
        """
        in_path = shutil.which(self.value)
        is_file = glob(self.value)
        is_dir = os.path.isdir(self.value)

        if not self.is_dir:
            found = any([in_path, is_file])
            if not found:
                return [
                    JobOptionValidationResult(
                        "error", [self], f"File {self.value} not found"
                    )
                ]
        else:
            if not is_dir:
                return [
                    JobOptionValidationResult(
                        "error", [self], f"Directory {self.value} not found"
                    )
                ]
        return []


class MultiStringJobOption(JobOption):
    """Create a job option object for a multiple strings. For things like keyword lists

    The value for this should be a string containing values separated by ::: which
    will be converted into a list by: `[x.strip() for x in the_str.split(":::")]`

    The only reason there can't just be a self.get_list() function in a regular
    StringJobOption is the GUI must handle them differently.

    For regex validation all values in the list of inputs must match the regex

    Args:
        label (str): A verbose label for the parameter.
        default_value (None) = The default value for the parameter.
        help_text (str): Text that will be displayed in the GUI if help is clicked.
        in_continue (bool): If this parameter can be modified in a job that is
            continued
    """

    joboption_type = "MULTISTRING"

    def __init__(
        self,
        *,
        label: str,
        default_value: str = "",
        help_text: str = "",
        in_continue: bool = False,
        only_in_continue: bool = False,
        is_required: bool = False,
        required_if: Optional[list] = None,
        deactivate_if: Optional[list] = None,
        validation_regex: Optional[str] = None,
        jobop_group: str = "Main",
    ):
        super().__init__(
            label=label,
            default_value=default_value,
            help_text=help_text,
            in_continue=in_continue,
            only_in_continue=only_in_continue,
            is_required=is_required,
            required_if=required_if if required_if is not None else [],
            deactivate_if=deactivate_if if deactivate_if is not None else [],
            jobop_group=jobop_group,
        )
        self.validation_regex = validation_regex

    def get_list(self) -> List[str]:
        """Return the items in self.value as a list of strings

        Returns:
            List[str]: The items in the JobOptions separated string
                as a list, with any white space at the beginning and/or end
                removed
        """
        return [x.strip() for x in self.value.split(":::") if len(x.strip()) > 0]

    def validate(self) -> List[JobOptionValidationResult]:
        """Basic validation of the input parameters"""
        results = super().validate()
        if results:
            return results

        # validate the individual items in the string list
        if self.validation_regex is not None and self.value:
            reg = re.compile(self.validation_regex)
            for sval in self.get_list():
                if not reg.match(sval):
                    results.append(
                        JobOptionValidationResult(
                            "error",
                            [self],
                            f"Value '{sval}' format does not match pattern "
                            f"'{self.validation_regex}'",
                        )
                    )
        return results


class FileNameJobOption(JobOption):
    """Define a job option as a file name

    GUI will open a file browser for the user to find an input

    Args:
        label (str): A verbose label for the parameter.  This is what appears in a
            run.job file
        default_value (str) = This should almost always by "", as the default value
            will usually be set from the string and pattern.
        pattern (str): Info about the search string for file types. It should be
            in the format <description> (<example>) e.g.: `"MRC files
            (*.mrc)"`
        directory (str): The directory the GUI should to open the file browser in
        help_text (str): Text that will be displayed in the GUI if help is clicked
        in_continue (bool): If this parameter can be modified in a job that is
            continued
        suggestion (str): A hint that will appear in the data entry field if no
            no default value is set, it will not be used. IE: 'Enter file path here'
            If you want the suggestion used as the default then just put it in
            default_value
        node_type (str): The type of input node that should be created for this file,
            if one is necessary.  If this is `None` no node will be created
    """

    joboption_type = "FILENAME"

    def __init__(
        self,
        *,
        label: str,
        pattern: str = "",
        directory: str = "",
        default_value: str = "",
        help_text: str = "",
        in_continue: bool = False,
        only_in_continue: bool = False,
        is_required: bool = False,
        required_if: Optional[list] = None,
        deactivate_if: Optional[list] = None,
        validation_regex: str = None,
        suggestion: str = "",
        node_type: Optional[str] = None,
        node_kwds: Optional[List[str]] = None,
        jobop_group: str = "Main",
    ):
        if suggestion != "":
            suggest = suggestion
        else:
            suggest = pattern

        super().__init__(
            label=label,
            default_value=default_value,
            help_text=help_text,
            in_continue=in_continue,
            only_in_continue=only_in_continue,
            is_required=is_required,
            required_if=required_if if required_if is not None else [],
            deactivate_if=deactivate_if if deactivate_if is not None else [],
            jobop_group=jobop_group,
        )
        self.pattern = pattern
        self.directory = directory
        self.validation_regex = validation_regex
        self.suggestion = suggest
        self.node_kwds = node_kwds
        self.node_type = node_type

    def get_input_nodes(self) -> List[Node]:
        # If there is a node type, we should create a node for the input file
        # Otherwise we should not create a node so return an empty list
        if self.value and self.node_type is not None:
            return [create_node(self.value, self.node_type, self.node_kwds)]
        else:
            return []

    def validate(self) -> List[JobOptionValidationResult]:
        """Basic validation of the input parameters

        Returns:
            List[:class:`~pipeliner.job_options.JobOptionValidationResult`]: The
                validation error of one exists
        """
        results = super().validate()
        if results:
            return results

        if self.validation_regex is not None:
            reg = re.compile(self.validation_regex)
            if not reg.match(self.value):
                results.append(
                    JobOptionValidationResult(
                        "error",
                        [self],
                        f"Value format must match pattern '{self.validation_regex}'",
                    )
                )
        return results


class MultiFileNameJobOption(JobOption):
    """Define a job option as multiple file names

    GUI will open a file browser for the user to find an input for each one.
    The value for this should be a string of values separated by ::: which will be
    converted into a list by: `[x.strip() for x in the_str.split(":::")]`

    Args:
        label (str): A verbose label for the parameter.  This is what appears in a
            run.job file
        default_value (str) = This should almost always by "", as the default value
            will usually be set from the string and pattern
        pattern (str): Info about the search string for file types. It should be
            in the format <description> (<example>) e.g.: `"MRC files
            (*.mrc)"`
        directory (str): The directory the GUI should to open the file browser in
        help_text (str): Text that will be displayed in the GUI if help is clicked
        in_continue (bool): If this parameter can be modified in a job that is
            continued
        suggestion (str): A hint that will appear in the data entry field if no
            no default value is set, it will not be used. IE: 'Enter file path here'
            If you want the suggestion used as the default then just put it in
            default_value
        node_type (str): The type of input node that should be created for this file,
            if one is necessary.
        node_kwds (List[str]): Keywords for the nodes, all nodes get the same keywords
    """

    joboption_type = "MULTIFILENAME"

    def __init__(
        self,
        *,
        label: str,
        pattern: str = "",
        directory: str = "",
        default_value: str = "",
        help_text: str = "",
        in_continue: bool = False,
        only_in_continue: bool = False,
        is_required: bool = False,
        required_if: Optional[list] = None,
        deactivate_if: Optional[list] = None,
        validation_regex: str = None,
        suggestion: str = "",
        node_type: Optional[str] = None,
        node_kwds: Optional[List[str]] = None,
        jobop_group: str = "Main",
    ):
        if suggestion != "":
            suggest = suggestion
        else:
            suggest = pattern

        super().__init__(
            label=label,
            default_value=default_value,
            help_text=help_text,
            in_continue=in_continue,
            only_in_continue=only_in_continue,
            is_required=is_required,
            required_if=required_if if required_if is not None else [],
            deactivate_if=deactivate_if if deactivate_if is not None else [],
            jobop_group=jobop_group,
        )
        self.pattern = pattern
        self.directory = directory
        self.validation_regex = validation_regex
        self.suggestion = suggest
        self.node_type = node_type
        self.node_kwds = node_kwds

    def get_list(self) -> List[str]:
        """Return the items in self.value as a list of strings

        Returns:
            List[str]: The items in the JobOptions comma separated string
                as a list, with any white space at the beginning and/or end
                removed
        """
        return [x.strip() for x in self.value.split(":::") if len(x.strip()) > 0]

    def get_input_nodes(self) -> List[Node]:
        """Get the input nodes from this job option.

        Raises:
            ValueError: If not all the files are correct for their nodetype
        """
        # If there is a node type, we should create a node for each input file
        # Otherwise we should not create any nodes so return an empty list
        if self.node_type is None:
            return []
        err_files = []
        nodes = []
        for fname in self.get_list():
            fnode = create_node(fname, self.node_type, self.node_kwds)
            nodes.append(fnode)
            nx = fnode.format
            if nx.startswith("X") and nx.endswith("X"):
                err_files.append(fnode.name)
        if err_files:
            ef = ",".join(err_files)
            raise ValueError(
                f"Not all files are valid for {self.node_type} node type; {ef}"
            )
        return nodes

    def validate(self) -> List[JobOptionValidationResult]:
        """Basic validation of the input parameters

        Returns:
            List[:class:`~pipeliner.job_options.JobOptionValidationResult`]: The
            validation errors of any exist
        """
        results = super().validate()
        if results:
            return results

        if self.validation_regex is not None:
            reg = re.compile(self.validation_regex)
            val_list = self.get_list()
            for entered in val_list:
                if not reg.match(entered):
                    results.append(
                        JobOptionValidationResult(
                            "error",
                            [self],
                            "Value format must match pattern "
                            f"'{self.validation_regex}'",
                        )
                    )
        return results

    def check_file(self) -> List[JobOptionValidationResult]:
        """Check that specified input files exist

        Returns:
            List[:class:~pipeliner.job_options.JobOptionValidationResult]: if any of
            the files do not exist
        """
        errs = []
        for entered in self.get_list():
            in_path = shutil.which(entered)
            is_file = glob(entered)
            if not (in_path or is_file):
                errs.append(
                    JobOptionValidationResult(
                        "error", [self], f"File {self.value} not found"
                    )
                )
        return errs


class InputNodeJobOption(JobOption):
    """Define a job option as a file name, create an input node for it

    GUI will open a file browser for the user to find an input

    Args:
        label (str): A verbose label for the parameter.  This is what appears in a
            run.job file
        node_type (str): The type of node using the standard <type>.<ext>.<keywords>
            format
        default_value (str) = The default value for the parameter.  The type
            will be set by the specific class method
        directory (str) = The directory to start in when searching for the files in the
            file browser
        pattern (str): Info about the search string for file types. It should be
            in the format <description> (<example>) IE: `"MRC files
            (*.mrc)"`
        directory (str): The directory the GUI should to open the file browser in
        help_text (str): Text that will be displayed in the GUI if help is clicked
        in_continue (bool): If this parameter can be modified in a job that is
            continued
        suggestion (str): A hint that will appear in the data entry field if no
            no default value is set, it will not be used. IE: 'Enter file path here'
            If you want the suggestion used as the default then just put it in
            default_value
        create_node (bool): Should the node be automatically created, assuming it's
            value is not `None`.  This should almost always be `True`
    """

    joboption_type = "INPUTNODE"

    def __init__(
        self,
        *,
        label: str,
        node_type: str,
        directory: str = "",
        default_value: str = "",
        pattern: str = "",
        help_text: str = "",
        in_continue: bool = False,
        only_in_continue: bool = False,
        is_required: bool = False,
        required_if: Optional[list] = None,
        deactivate_if: Optional[list] = None,
        validation_regex: Optional[str] = None,
        suggestion: str = "",
        create_node: bool = True,
        jobop_group: str = "Main",
        node_kwds: Optional[List[str]] = None,
    ):
        if suggestion != "":
            suggest = suggestion
        else:
            suggest = pattern

        super().__init__(
            label=label,
            default_value=default_value,
            help_text=help_text,
            in_continue=in_continue,
            only_in_continue=only_in_continue,
            is_required=is_required,
            required_if=required_if if required_if is not None else [],
            deactivate_if=deactivate_if if deactivate_if is not None else [],
            jobop_group=jobop_group,
        )
        self.pattern = pattern
        self.node_type = node_type
        self.directory = directory
        self.validation_regex = validation_regex
        self.suggestion = suggest
        self.create_node = create_node
        self.node_kwds = node_kwds if node_kwds else None

    def get_input_nodes(self) -> List[Node]:
        if self.value and self.create_node:
            return [create_node(self.value, self.node_type, self.node_kwds)]
        else:
            return []

    def validate(self) -> List[JobOptionValidationResult]:
        """Basic validation of the input parameters

        Returns:
            List[:class:`~pipeliner.job_options.JobOptionValidationResult`]: The
            validation error of one exists
        """
        results = super().validate()
        if results:
            return results

        if self.validation_regex is not None:
            reg = re.compile(self.validation_regex)
            if not reg.match(self.value):
                results.append(
                    JobOptionValidationResult(
                        "error",
                        [self],
                        f"Value format must match pattern '{self.validation_regex}'",
                    )
                )
        return results


class BooleanJobOption(JobOption):
    """Define a job option as a boolean

    Args:
        label (str): A verbose label for the parameter.  This is what appears in a
            run.job file
        default_value (bool): The default value
        help_text (str): Text that will be displayed in the GUI if help is clicked
        in_continue (bool): If this parameter can be modified in a job that is
            continued
    """

    joboption_type = "BOOLEAN"

    def __init__(
        self,
        *,
        label: str,
        default_value: bool,
        help_text: str = "",
        in_continue: bool = False,
        only_in_continue: bool = False,
        deactivate_if: Optional[list] = None,
        jobop_group: str = "Main",
    ):
        # Convert bool value to string for storage
        # TODO: replace with simple bool value instead
        #  This is a bit tricky because various parts of the code use `value` or
        #  `default_value` directly, expecting it to be a string
        string_value = "Yes" if default_value else "No"
        super().__init__(
            label=label,
            default_value=string_value,
            help_text=help_text,
            in_continue=in_continue,
            only_in_continue=only_in_continue,
            deactivate_if=deactivate_if if deactivate_if is not None else [],
            jobop_group=jobop_group,
        )

    def get_boolean(self) -> bool:
        """Get a boolean value

        Returns:
            bool: The value
        """
        return self.value.lower() in TRUES

    def validate(self) -> List[JobOptionValidationResult]:
        """Basic validation of the input parameters

        Returns:
            :class:`~pipeliner.job_options.JobOptionValidationResult`: The validation
                error of one exists
        """
        results = super().validate()
        if results:
            return results
        if self.value not in (True, False) and self.value.lower() not in TRUES + FALSES:
            results.append(
                JobOptionValidationResult(
                    "error",
                    [self],
                    "Boolean job option must have a value that equates"
                    f" to true or false {TRUES} or {FALSES}",
                )
            )
        return results


class MultipleChoiceJobOption(JobOption):
    """Define a job option as a pull down menu

    Radio is a misnomer, the GUI will display a pull down menu with the options

    Args:
        label (str): A verbose label for the parameter.  This is what appears in a
            run.job file
        choices (list): A list :class:`str` options for the menu
        default_value_index (int): Index of the initial option for the radio; also used
            as the default value
        help_text (str): Text that will be displayed in the GUI if help is clicked
        in_continue (bool): If this parameter can be modified in a job that is
            continued

    Raises:
        ValueError: If the index of the initial option is not in the radio menu
    """

    joboption_type = "MULTIPLECHOICE"

    def __init__(
        self,
        *,
        label: str,
        choices: List[str],
        default_value_index: int,
        help_text: str = "",
        in_continue: bool = False,
        only_in_continue: bool = False,
        is_required=True,
        required_if: Optional[list] = None,
        deactivate_if: Optional[list] = None,
        jobop_group: str = "Main",
    ):
        try:
            default_value = choices[default_value_index]
        except KeyError:
            raise ValueError(
                "ERROR: Index of default value not in multiple choice options"
                f" index={default_value_index}, radio_menu has {len(choices)} items"
            )
        super().__init__(
            label=label,
            default_value=default_value,
            help_text=help_text,
            in_continue=in_continue,
            only_in_continue=only_in_continue,
            is_required=is_required,
            required_if=required_if if required_if is not None else [],
            deactivate_if=deactivate_if if deactivate_if is not None else [],
            jobop_group=jobop_group,
        )
        self.choices = choices

    def validate(self) -> List[JobOptionValidationResult]:
        """Basic validation of the input parameters

        Returns:
            List[:class:`~pipeliner.job_options.JobOptionValidationResult`]: The
            validation error of one exists
        """
        results = super().validate()
        if results:
            return results

        if self.value not in self.choices:
            results.append(
                JobOptionValidationResult(
                    "error",
                    [self],
                    f"'{self.value}' not in list of accepted values: {self.choices}",
                )
            )
        return results


class FloatJobOption(JobOption):
    """Define a job option as a slider for inputting numbers as floats

    The min value and max value are for display only, they do not limit the actual
    values that can be input

    Args:
        label (str): A verbose label for the parameter.  This is what appears in a
                run.job file
        default_value(float): The default value for the parameter
        suggested_min (float): The suggested minimum value for the slider
        suggested_max (float): The suggested maximum value for the slider
        step_value (float): The step value for the slider
        help_text (str): Text that will be displayed in the GUI if help is clicked
        in_continue (bool): If this parameter can be modified in a job that is
            continued
        hard_min (float): An error will be raised if the value is < this value
        hard_max (float): An error will be raised if the value is > this value
    """

    joboption_type = "FLOAT"

    def __init__(
        self,
        *,
        label: str,
        default_value: Optional[Union[str, float]],
        suggested_min: Optional[float] = None,
        suggested_max: Optional[float] = None,
        step_value: float = 1.0,
        help_text: str = "",
        in_continue: bool = False,
        only_in_continue: bool = False,
        is_required: bool = False,
        required_if: Optional[list] = None,
        deactivate_if: Optional[list] = None,
        hard_min: Optional[float] = None,
        hard_max: Optional[float] = None,
        jobop_group: str = "Main",
    ):
        super().__init__(
            label=label,
            default_value=default_value,
            help_text=help_text,
            in_continue=in_continue,
            only_in_continue=only_in_continue,
            is_required=is_required,
            required_if=required_if if required_if is not None else [],
            deactivate_if=deactivate_if if deactivate_if is not None else [],
            jobop_group=jobop_group,
        )
        self.suggested_min = suggested_min
        self.suggested_max = suggested_max
        self.step_value = step_value
        self.hard_min = hard_min
        self.hard_max = hard_max

        for val in [
            default_value,
            suggested_min,
            suggested_max,
            step_value,
            hard_min,
            hard_max,
        ]:
            if val is not None and type(val) not in (float, int):
                raise ValueError(
                    f"Improper JobOption value in '{label}', all values must be"
                    " numbers for this JobOption type."
                )

    def get_number(self, required: bool = False, errormsg: str = "") -> float:
        """Get the value of a JobOption as a number

        Args:
            required (bool): If a value is required for this parameter
            errormsg (str): The message to display if the parameter is required
                and no value was entered

        Returns:
            float: The value

        Raises:
            ValueError: If the value cannot be converted to a :class:`float`
            ValueError: If the value is required and missing
        """
        try:
            val = float(self.value)
        except ValueError:
            raise ValueError(
                "ERROR: Could not return a number from the value: " + self.value
            )
        if required and str(self.value) == "":
            raise ValueError(errormsg)

        return val

    def validate(self) -> List[JobOptionValidationResult]:
        """Basic validation check on the job option

        Returns:
            :class:`~pipeliner.job_options.JobOptionValidationResult`: The validation
                error of one exists
        """
        results = super().validate()
        if results:
            return results

        if self.hard_min is not None:
            if self.get_number() < self.hard_min:
                results.append(
                    JobOptionValidationResult(
                        "error", [self], f"Value cannot be less than {self.hard_min}"
                    )
                )
        if self.hard_max is not None:
            if self.get_number() > self.hard_max:
                results.append(
                    JobOptionValidationResult(
                        "error", [self], f"Value cannot be greater than {self.hard_max}"
                    )
                )

        # -1 is a special case as it is often used to mean use the default or ignore
        if self.suggested_min is not None and self.suggested_min != -1:
            if self.get_number() < self.suggested_min and self.get_number() != -1.0:
                results.append(
                    JobOptionValidationResult(
                        "warning",
                        [self],
                        f"Value outside suggested range < {self.suggested_min}",
                    )
                )

        if self.suggested_max is not None and self.suggested_max != -1:
            if self.get_number() > self.suggested_max and self.get_number() != 999.99:
                results.append(
                    JobOptionValidationResult(
                        "warning",
                        [self],
                        f"Value outside suggested range > {self.suggested_max}",
                    )
                )
        return results


class IntJobOption(JobOption):
    """Define a job option as a slider for inputting numbers as integers

    The slider min value and max value are for display only, they do not
    limit the actual values that can be input

    Args:
        label (str): A verbose label for the parameter.  This is what appears in a
                run.job file
        default_value (int): The default value for the parameter
        suggested_min (int): The suggested minimum value for the slider
        suggested_max (int): The suggested maximum value for the slider
        step_value (int): The step value for the slider
        help_text (str): Text that will be displayed in the GUI if help is clicked
        in_continue (bool): If this parameter can be modified in a job that is
            continued
        hard_min (int): An error will be raised if the value is < this value
        hard_max (int): An error will be raised if the value is > this value

    Raises:
        ValueError: If the suggested_min, suggested_max, default, hard_min , hard_max,
            or step values are not integers
    """

    joboption_type = "INT"

    def __init__(
        self,
        *,
        label: str,
        default_value: Optional[int],
        suggested_min: Optional[int] = None,
        suggested_max: Optional[int] = None,
        step_value: int = 1,
        help_text: str = "",
        in_continue: bool = False,
        only_in_continue: bool = False,
        is_required: bool = False,
        required_if: Optional[list] = None,
        deactivate_if: Optional[list] = None,
        hard_min: Optional[int] = None,
        hard_max: Optional[int] = None,
        jobop_group: str = "Main",
    ):
        super().__init__(
            label=label,
            default_value=default_value,
            help_text=help_text,
            in_continue=in_continue,
            only_in_continue=only_in_continue,
            is_required=is_required,
            required_if=required_if if required_if is not None else [],
            deactivate_if=deactivate_if if deactivate_if is not None else [],
            jobop_group=jobop_group,
        )
        for val in [
            default_value,
            suggested_min,
            suggested_max,
            step_value,
            hard_min,
            hard_max,
        ]:
            if val is not None and type(val) != int:
                raise ValueError(
                    f"Improper JobOption value in '{label}', all values must be"
                    " integers for this JobOption type.  Perhaps a FloatJobOption is"
                    " more appropriate?"
                )

        self.suggested_min = suggested_min
        self.suggested_max = suggested_max
        self.step_value = step_value
        self.hard_min = hard_min
        self.hard_max = hard_max

    def get_number(self, required: bool = False, errormsg: str = "") -> int:
        """Get the value of a JobOption as a number

        Args:
            required (bool): If a value is required for this parameter
            errormsg (str): The message to display if the parameter is required
                and no value was entered

        Returns:
            int: The value

        Raises:
            ValueError: If the value cannot be converted to a :class:`float`
            ValueError: If the value is required and missing
        """
        try:
            val = int(self.value)
        except ValueError:
            raise ValueError(
                "ERROR: Could not return a number from the value: " + self.value
            )
        if required and str(self.value) == "":
            raise ValueError(errormsg)

        return val

    def validate(self) -> List[JobOptionValidationResult]:
        """Basic validation check on the job option

        Returns:
            :class:`~pipeliner.job_options.JobOptionValidationResult`: The validation
                error of one exists
        """
        results = super().validate()
        if results:
            return results

        if self.hard_min is not None:
            if self.get_number() < self.hard_min:
                results.append(
                    JobOptionValidationResult(
                        "error", [self], f"Value cannot be less than {self.hard_min}"
                    )
                )
        if self.hard_max is not None:
            if self.get_number() > self.hard_max:
                results.append(
                    JobOptionValidationResult(
                        "error", [self], f"Value cannot be greater than {self.hard_max}"
                    )
                )
        # -1 is a special case as it is often used to mean use the default or ignore
        if self.suggested_min is not None:
            if self.get_number() < self.suggested_min and self.get_number() != -1:
                results.append(
                    JobOptionValidationResult(
                        "warning",
                        [self],
                        f"Value outside suggested range < {self.suggested_min}",
                    )
                )
        if self.suggested_max is not None:
            if self.get_number() > self.suggested_max and self.get_number() != -1:
                results.append(
                    JobOptionValidationResult(
                        "warning",
                        [self],
                        f"Value outside suggested range > {self.suggested_max}",
                    )
                )
        return results


class JobOptionValidationResult(object):
    """A class for handling validation of joboptions

    Attributes:
         type (str): Either 'warning' or 'error'
         raised_by (list): List of JobOption objects that raised the warning/error.
            Generally this will only be one JobOption, unless the warning/error
            was raised from comparing two or more JobOptions
         message (str): Description of the error/warning
    """

    def __init__(self, result_type: str, raised_by: List[JobOption], message: str):
        """Create a JobOptionValidationResult object

        Args:
            result_type (str): `warning` or `error`
            raised_by (list): The JobOption(s) that raised the error
            message (str): The warning or error message to display

        Raises:
            ValueError: If result_type is not in the list of appropriate values
        """

        if result_type not in ["warning", "error"]:
            raise ValueError("result_type must be either 'warning' or 'error'")
        self.type = result_type
        self.message = message
        if type(raised_by) != list:
            raise ValueError("'raised_by' must be a list")
        self.raised_by = raised_by
