#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import numpy as np
import json
import os
from typing import List, Union, Optional, Sequence, Dict, Any
import plotly.express as px

# import plotly.io as po
import pandas as pd
from pipeliner.utils import str_is_hex_colour


class ResultsDisplayObject(object):
    """Abstract super-class for results display objects

    Attributes:
        title (str): The title
        start_collapsed (bool): Should the object start out collapsed when displayed in
            the GUI
        dobj_type (str): Used to identify what kind of ResultsDisplayObject it is
        flag (str): A message that is displayed if the results display object is showing
            somthing scientifically dubious.
    """

    def __init__(self, title: str, start_collapsed: bool = False, flag=""):
        self.title = title
        self.start_collapsed = start_collapsed
        self.dobj_type = ""
        self.flag = flag

    def write_displayobj_file(self, outdir, n):
        """Write a json file from a ResultsDisplayObject object

        Args:
            outdir (str): The directory to write the output in
            n (int): The number of the display obj

        Raises:
            NotImplementedError: If a write attempt is made from the superclass
        """
        if not self.dobj_type:
            raise NotImplementedError

        outfile = os.path.join(outdir, f".results_display{n:03d}_{self.dobj_type}.json")
        with open(outfile, "w") as of:
            json.dump(self.__dict__, of)


class ResultsDisplayPending(ResultsDisplayObject):
    """A placeholder class for when a job is not able to produce results yet"""

    def __init__(
        self,
        *,
        title: str = "Results pending...",
        message: str = "The result not available yet",
        reason: str = "unknown",
        start_collapsed: bool = False,
        flag: str = "",
    ):
        """Instantiate a ResultsDisplayPending object

        Args:
            message (str): The message to display to the user
            reason (str): The reason that the result was not prepared
                usually an exception from the get_results_display() of the
                job
            start_collapsed (Optional[bool]): Should the display start out collapsed
        """
        super().__init__(title, start_collapsed)
        self.dobj_type = "pending"
        self.flag = flag
        self.message = message
        self.reason = str(reason)


class ResultsDisplayText(ResultsDisplayObject):
    """A class to display general text in the GUI results tab

    It is best to not instantiate this class directly. Instead create
    it using `create_results_display_object`

    Attributes:
        title (str): the title of the section
        display_data (str): The text to display
        associated_data (list): Data files associated with this result
        start_collapsed (bool): Should the object start out collapsed when displayed in
            the GUI
    """

    def __init__(
        self,
        *,
        title: str,
        display_data: str,
        associated_data: list,
        start_collapsed: bool = False,
        flag: str = "",
    ):
        """Instantiate a ResultsDisplayText object

        Args:
            title (str): the title of the section
            display_data (str): A the text to display
            associated_data (list): A list of data files associated with these data
            start_collapsed (bool): Should the object start out collapsed when displayed
                in the GUI
        """
        super().__init__(title, start_collapsed)
        self.dobj_type = "text"
        self.flag = flag
        self.display_data = display_data
        self.associated_data = associated_data


class ResultsDisplayMontage(ResultsDisplayObject):
    """An object to send to the GUI to make an image montage

    This one is an image montage with info about the specific images
    It is best to not instantiate this class directly. Instead create
    it using `create_results_display_object`

    Attributes:
        title (str): The title of the object/graph
        xvalues: (list): The x coordinates by image
        yvalues: (list): The y coordinates by image
        labels (list): Data labels for the images
        associated_data (list): A list of files that contributed the data used in the
            image/graph
        img (str): Path to an image to display
        start_collapsed (bool): Should the object start out collapsed when displayed in
            the GUI
    """

    def __init__(
        self,
        *,
        xvalues: list,
        yvalues: list,
        img: str,
        title: str,
        associated_data: list,
        labels: list = None,
        start_collapsed: bool = False,
        flag: str = "",
    ):
        """Create a ResultsDisplayMontage object

        Args:
            xvalues: (list): The x coordinates by image
            yvalues: (list): The y coordinates by image
            labels (list): Data labels for the images, in order
            img (str): Path to an image to display
            title (str): The title of the object/graph
            associated_data (list): A list of files that contributed the data used
                in the image/graph
            start_collapsed (bool): Should the object start out collapsed when displayed
                in the GUI
        """
        super().__init__(title, start_collapsed)
        self.dobj_type = "montage"
        self.flag = flag
        self.xvalues = xvalues
        self.yvalues = yvalues
        self.labels = [] if not labels else labels
        self.associated_data = associated_data
        self.img = img

        if not len(xvalues) == len(yvalues):
            raise ValueError("The number of x points and y points do not match")


class ResultsDisplayGraph(ResultsDisplayObject):
    """A simple graph for the GUI to display

    It is best to not instantiate this class directly. Instead create
    it using `create_results_display_object`

    Attributes:
        title (str): The title of the object/graph
        xvalues: (list): list of x coordinate data series,
            can have multiple data series
        xaxis_label (str): Label for the x axis if a graph
        xrange (list): Range of x to be displayed, displays the
            full range if `None`.  If the x axis needs to be reveresd
            then enter the values backwards [max, min]
        yvalues: (list): List y coordinate data series
            can have multiple data series
        yaxis_label (str): Label for the y axis if a graph
        yrange (list): Range of y to be displayed, displays the
            full range if `None`. If the y axis needs to be reveresd
            then enter the values backwards [max, min]
        data_series_labels (list): List of names of the different data series
        associated_data (list): A list of files that contributed the data used in the
            image/graph
        modes (list): Controls the appearance of each data series,
            choose from 'lines', 'markers' 'or lines+markers'
        start_collapsed (bool): Should the object start out collapsed when displayed in
            the GUI
    """

    def __init__(
        self,
        *,
        xvalues: list,
        yvalues: list,
        title: str,
        associated_data: list,
        data_series_labels: list,
        xaxis_label: str = "",
        xrange: list = None,
        yaxis_label: str = "",
        yrange: list = None,
        modes: list = None,
        start_collapsed: bool = False,
        flag: str = "",
    ):
        """Create a ResultsDisplayGraph object

        Args:
            title (str): The title of the object/graph
            xvalues: (list): list of x coordinate data series,
                can have multiple data series
            xaxis_label (str): Label for the x axis if a graph
            xrange (list): Range of x to be displayed, displays the
                full range if `None`. If the x axis needs to be reveresd
                then enter the values backwards [max, min]
            yvalues: (list): List y coordinate data series
                can have multiple data series
            yaxis_label (str): Label for the y axis if a graph
            yrange (list): Range of y to be displayed, displays the
                full range if `None`. If the x axis needs to be reveresd
                then enter the values backwards [max, min]
            data_series_labels (list): List of names of the different data series
            associated_data (list): A list of files that contributed the data
                used in the image/graph
            modes (list): Controls the appearance of each data series,
                choose from 'lines', 'markers' 'or lines+markers'
            start_collapsed (bool): Should the object start out collapsed when displayed
                in the GUI
        Raises:
            ValueError: If the counts of the xvalues, yvalues are not the same
            ValueError: If the any data series doesn't have an equal number of
                x and y values
            ValueError: If the mode is an invalid choice
        """
        super().__init__(title, start_collapsed)
        self.dobj_type = "graph"
        self.flag = flag
        if None in [xvalues, yvalues]:
            raise ValueError(
                "x and y value lists must be provided for ResultsDisplayGraph objects"
            )
        if title is None:
            raise ValueError("A ResultsDisplayGraph object must have a title")
        modes = ["lines"] if modes is None else modes
        for mode in modes:
            if mode not in ["markers", "lines", "lines+markers"]:
                raise ValueError(
                    "mode must be in ['markers', 'lines', 'lines+markers']"
                )
        self.xvalues = xvalues
        self.xaxis_label = xaxis_label
        xr_minmax = sum(xvalues, [])  # type: List[Union[float, int]]
        self.xrange = xrange if xrange else [min(xr_minmax), max(xr_minmax)]
        self.yvalues = yvalues
        self.yaxis_label = yaxis_label
        yr_minmax = sum(yvalues, [])  # type: List[Union[float, int]]
        self.yrange = yrange if yrange else [min(yr_minmax), max(yr_minmax)]
        self.associated_data = associated_data
        self.data_series_labels = [] if not data_series_labels else data_series_labels

        if len(modes) != len(xvalues):
            if len(modes) == 1:
                modes = modes * len(xvalues)
            else:
                raise ValueError(
                    "The number of plot mode types and the data series do not match"
                )

        self.modes = modes

        if len(xvalues) != len(yvalues):
            raise ValueError(
                "The number of x data series and y data series are not equal"
            )
        if data_series_labels and len(data_series_labels) != len(xvalues):
            raise ValueError(
                "The number of data series labels doesn't match the number "
                "of data series"
            )

        comb = zip(xvalues, yvalues)
        for i in comb:
            if len(i[0]) != len(i[1]):
                raise ValueError(
                    "One or more data series doesn't have a matching number "
                    "of x and y points"
                )


class ResultsDisplayImage(ResultsDisplayObject):
    """A class for the GUI to display a single image

    It is best to not instantiate this class directly. Instead create
    it using `create_results_display_object`

    Attributes:
        title (str): The title for the image
        image_path (str): The path to the image
        image_desc (str): A description of the image
        associated_data (list): Data files associated with the image
        start_collapsed (bool): Should the object start out collapsed when displayed in
            the GUI
    """

    def __init__(
        self,
        *,
        title: str,
        image_path: str,
        image_desc: str,
        associated_data: list,
        start_collapsed: bool = False,
        flag: str = "",
    ):
        """Instantiate a ResultsDisplayImage object

        Args:
            title (str): The title for the image
            image_path (str): The path to the image
            image_desc (str): A description of the image
            associated_data (list): Data files associated with the image
            start_collapsed (bool): Should the object start out collapsed when displayed
                in the GUI
        """

        super().__init__(title, start_collapsed)
        self.dobj_type = "image"
        self.flag = flag
        self.image_path = image_path
        self.image_desc = image_desc
        self.associated_data = associated_data if associated_data is not None else {}


class ResultsDisplayHistogram(ResultsDisplayObject):
    """A class for the GUI to display a histogram

    It is best to not instantiate this class directly. Instead create
    it using `create_results_display_object`

    Args:
        title (str): The title of the histogram
        data_to_bin (list): The data to bin
        xlabel (str): Label for the x axis
        ylabel (str): Label for the y axis
        associated_data (list): List of data files associated with the histogram
        bins (list): A list of bin counts, if they are known
        bin_edges (list): A list of the bin edges, if they are already known
        start_collapsed (bool): Should the object start out collapsed when displayed in
            the GUI
    Raises:
        ValueError: If no data or bins are specified
        ValueError: If an attempt is made to specify bins or bin edges when
            data to bin are being provided
        ValueError: If the associated data is not a list, or not provided
    """

    def __init__(
        self,
        *,
        title: str,
        associated_data: list,
        data_to_bin: Optional[List[float]] = None,
        xlabel: str = "",
        ylabel: str = "",
        bins: Optional[List[int]] = None,
        bin_edges: Optional[List[float]] = None,
        start_collapsed: bool = False,
        flag: str = "",
    ):
        super().__init__(title, start_collapsed)
        self.dobj_type = "histogram"
        self.flag = flag
        # checks
        if not data_to_bin and (not bins or not bin_edges):
            raise ValueError("Data to bin must be provided or pre-made bins specified")
        if data_to_bin:
            if bins or bin_edges:
                raise ValueError("Cannot specify edges or bins if data are provided")
            if len(data_to_bin) == 0:
                raise ValueError("No data to operate on")

        if not bins and not bin_edges and data_to_bin:
            bins_np, bin_edges_np = np.histogram(data_to_bin, bins="fd")
            self.bins = [int(x) for x in list(bins_np.astype(float))]
            self.bin_edges = list(bin_edges_np.astype(float))
        elif bins and bin_edges:
            self.bins = bins
            self.bin_edges = bin_edges
        self.xlabel = xlabel
        self.ylabel = ylabel
        self.associated_data = associated_data


class ResultsDisplayPlotlyObj(ResultsDisplayObject):
    """This uses the plotly express class to create plotly.graph_objects.Figure object
    https://plotly.com/python/plotly-express/
    Use this class to generate plotly Figure objects for custom plots

    Attributes:
        data: The data to bin. Following types are allowed
                list - list of values to be binned
                array and dict - converted to a pandas dataframe internally
                pandas dataframe - ensure column names are added
                if 'x' indicates a column name
                More details https://plotly.com/python/px-arguments/
        plot_type (str): Required, type of plot or the
            plotly express function to call
            https://plotly.com/python-api-reference/plotly.express.html
        title (str): The title of the plot
        plotlyfig (plotly.graph_objects.Figure): plotly.graph_objects.Figure
            object generated from input data
        associated_data (list): A list of the associated data files
        start_collapsed (bool): Should the object start out collapsed when displayed in
            the GUI
    """

    def __init__(
        self,
        *,
        data: Union[list, pd.DataFrame, np.ndarray, dict, None] = None,
        plot_type: str,
        title: str,
        associated_data: list,
        start_collapsed: bool = False,
        flag: str = "",
        **kwargs,
    ):
        """
        Args:
            data (list, array, dataframe): The data to bin
                list - list of values to be binned
                array and dict - converted to a pandas dataframe internally
                pandas dataframe - ensure column names are added
                if 'x' indicates a column name
                More details https://plotly.com/python/px-arguments/
            plot_type (str): Required, type of plot or the
                plotly express function to call
                https://plotly.com/python-api-reference/plotly.express.html
            title (str): The title of the histogram
            associated_data (list): List of data files associated with the histogram
            start_collapsed (bool): Should the object start out collapsed when
                displayed in the GUI
            **kwargs : additional keyword arguments specific for the plot type
        Raises:
            ValueError: If plot type (plot_type) not recognised by Plotly Express
        """
        super().__init__(title, start_collapsed)
        self.dobj_type = "plotlyobj"
        self.flag = flag
        self.associated_data = associated_data

        if data is None:
            raise ValueError("No data to operate on")
        if isinstance(data, pd.DataFrame):  # not json serializable
            self.data: Union[
                list, pd.DataFrame, np.ndarray, dict, None
            ] = data.to_dict()
        else:
            self.data = data
        self.associated_data = associated_data
        self.plot_type = plot_type
        if not hasattr(px, plot_type):
            raise ValueError(
                "Input plot type not recognised by Plotly Express. "
                "See: https://plotly.com/python/plotly-express/"
            )
        dict_args = {}
        for arg in kwargs:
            if arg != "plotlyFig":
                dict_args[arg] = kwargs[arg]
        plotlyfig = getattr(px, plot_type)(data, **dict_args)
        self.plotlyfig = plotlyfig.to_json()

    def write_displayobj_file(self, outdir, n):
        """Write a json file from a plotly.graph_objects.Figure object

        Args:
            outdir (str): The directory to write the output in
            n (int): The number of the display obj
        """
        # self.plotlyfig_json = os.path.join(outdir, f"plotlyfig{n:03d}_generic.json")
        # po.write_json(self.plotlyFig, self.plotlyfig_json)
        # self.__dict__.pop("plotlyFig", None)  # remove if present as an attribute
        outfile = os.path.join(outdir, f".results_display{n:03d}_plotlygeneric.json")
        with open(outfile, "w") as of:
            json.dump(self.__dict__, of)


class ResultsPlotlyHistogram(ResultsDisplayObject):
    """A class that generates plotly.graph_objects.Figure object
    to display a histogram
    Uses plotly express histogram
    https://plotly.com/python-api-reference/generated/plotly.express.histogram.html
    Examples here:
    https://plotly.com/python/histograms/

    Attributes:
        data: The data to bin. Following types are allowed
                    list - list of values to be binned
                    array and dict - converted to a pandas dataframe internally
                    pandas dataframe - ensure column names are added
                    if 'x' indicates a column name
                    More details https://plotly.com/python/px-arguments/
        title (str): The title of the plot
        plotlyfig (plotly.graph_objects.Figure): plotly.graph_objects.Figure
            object generated from input data
        associated_data (list): A list of the associated data files
        start_collapsed (bool): Should the object start out collapsed when displayed in
            the GUI
    """

    def __init__(
        self,
        *,
        data: Union[List[float], pd.DataFrame, np.ndarray, dict, None] = None,
        title: str,
        x: Union[str, list, None] = None,
        y: Union[str, list, None] = None,
        color: Union[str, int, list, None] = None,
        nbins: Optional[int] = None,
        range_x: Optional[list] = None,
        range_y: Optional[list] = None,
        category_orders: Optional[dict] = None,
        labels: Optional[dict] = None,
        bin_counts: Optional[List[float]] = None,
        bin_centres: Optional[List[float]] = None,
        associated_data: list,
        start_collapsed: bool = False,
        flag: str = "",
        **kwargs,
    ):
        """
        Args:
            data (list, array, dataframe): The data to bin
                list - list of values to be binned
                array and dict - converted to a pandas dataframe internally
                pandas dataframe - ensure column names are added
                if 'x' indicates a column name
                More details https://plotly.com/python/px-arguments/
            title (str): The title of the histogram
            x (str, list): Column name from the dataframe or dict
                str input should match with column name in  dataframe or
                key in dict
            y (str, list): Column name from the dataframe or dict
                str input should match with column name in  dataframe or
                key in dict
            color (str, int, list, tuple): color of histogram bars
                If column name or a list/tuple provided,
                values are used to assign color to marks.
            nbins (int): Number of bins
            range_x (list of two numbers):
                overrides auto-scaling on the x-axis in cartesian coordinates.
            range_y (list of two numbers):
                overrides auto-scaling on the y-axis in cartesian coordinates.
            category_orders (dict): Order of the data categories for the histogram
                use for categorical data only. For example,
                with categorical data from the column name 'day'
                {'day':['Monday','Tuesday','Wednesday']}
            labels (dict): X and Y labels. Example:
                {'x':'total_bill', 'y':'count'}
                Overrides other options and column names
            bin_counts (list): A list of bin counts, if they are known
            bin_centres (list): A list of the bin centres (same length as bin counts),
                if they are already known
            associated_data (list): List of data files associated with the histogram
            start_collapsed (bool): Should the object start out collapsed when
                displayed in the GUI
            **kwargs : additional keyword arguments
        Raises:
            ValueError: If no data OR (bin_counts and bin_centre) provided
        """
        super().__init__(title, start_collapsed)
        self.dobj_type = "plotlyhistogram"
        self.flag = flag
        dict_args: Dict[str, Any] = {}

        # check and set input
        if data is None and not bin_counts and not bin_centres:
            raise ValueError("No data to operate on")
        if x:
            dict_args["x"] = x
        if y:
            dict_args["y"] = y
        if color:
            dict_args["color"] = color
        if nbins:
            dict_args["nbins"] = nbins
        if range_x:
            dict_args["range_x"] = range_x
        if range_y:
            dict_args["range_y"] = range_y
        if category_orders:
            dict_args["category_orders"] = category_orders
        if labels:
            dict_args["labels"] = labels
        for arg in kwargs:
            if arg != "plotlyFig":
                dict_args[arg] = kwargs[arg]
        if isinstance(data, pd.DataFrame):  # not json serializable
            self.data: Union[
                List[float], pd.DataFrame, np.ndarray, dict, None
            ] = data.to_dict()
        else:
            self.data = data
        self.associated_data = associated_data
        if bin_counts and bin_centres:
            dict_args["x"] = bin_centres
            dict_args["y"] = bin_counts
            plotlyfig = px.bar(**dict_args)
        else:
            plotlyfig = px.histogram(data, **dict_args)
        self.plotlyfig = plotlyfig.to_json()

    def write_displayobj_file(self, outdir, n):
        """Write a json file from a plotly.graph_objects.Figure object

        Args:
            outdir (str): The directory to write the output in
            n (int): The number of the display obj
        """
        # self.plotlyfig_json = os.path.join(outdir, f"plotlyfig{n:03d}_histogram.json")
        # po.write_json(self.plotlyFig, self.plotlyfig_json)
        # self.__dict__.pop("plotlyFig", None)  # remove if present as an attribute
        outfile = os.path.join(outdir, f".results_display{n:03d}_plotlyhistogram.json")
        with open(outfile, "w") as of:
            json.dump(self.__dict__, of)


class ResultsPlotlyScatter(ResultsDisplayObject):
    """A class that generates plotly.graph_objects.Figure object
    to display a scatter plot
    Uses plotly express scatter
    https://plotly.com/python-api-reference/generated/plotly.express.scatter.html
    Examples here:
    https://plotly.com/python/line-and-scatter/

    Attributes:
        data: The data to bin. Following types are allowed
                list - list of values to be binned
                array and dict - converted to a pandas dataframe internally
                pandas dataframe - ensure column names are added
                if 'x' indicates a column name
                More details https://plotly.com/python/px-arguments/
        title (str): The title of the plot
        plotlyfig (plotly.graph_objects.Figure): plotly.graph_objects.Figure
            object generated from input data
        associated_data (list): A list of the associated data files
        start_collapsed (bool): Should the object start out collapsed when displayed in
            the GUI
    """

    def __init__(
        self,
        *,
        data: Union[List[List[float]], pd.DataFrame, np.ndarray, dict, None] = None,
        title: str,
        x: Union[str, List[float], None] = None,
        y: Union[str, List[float], None] = None,
        color: Union[str, int, Sequence[str], None] = None,
        size: Union[str, int, Sequence[str], None] = None,
        symbol: Union[str, int, Sequence[str], None] = None,
        hover_name: Union[str, int, Sequence[str], None] = None,
        range_color: Optional[list] = None,
        range_x: Optional[list] = None,
        range_y: Optional[list] = None,
        category_orders: Optional[dict] = None,
        labels: Optional[dict] = None,
        associated_data: list,
        start_collapsed: bool = False,
        flag: str = "",
        **kwargs,
    ):
        """
        Args:
            data (list of lists, dict, numpy array, pandas dataframe): The data to bin
                list - list of list of x and y values to be plotted
                array and dict - converted to a pandas dataframe internally
                pandas dataframe - ensure column names are added
                if 'x' indicates a column name
                More details https://plotly.com/python/px-arguments/
            title (str): The title of the histogram
            x (str, list): List of x values OR
                Column name from the dataframe or dict, OR
                A list of the bin edges, if they are already known
            y (str, list): List of y values OR
                Column name from the dataframe or dict, OR
                A list of bin counts, if they are known
            color (str, int, list, tuple): color of the plot
                If column name or a list/tuple provided,
                values are used to assign color to markers.
            size (str, int, list, tuple): size of the markers
                If column name or a list/tuple provided,
                values are used to assign color to marker sizes.
            symbol (str, int, list, tuple): symbol of the markers
                If column name or a list/tuple provided,
                values are used to assign symbols to markers.
            hover_name (str, int, list, tuple): If column name or a list/tuple provided,
                values are highlighted in the hover tooltip.
            range_color (list of two numbers):
                overrides auto-scaling on the continuous color scale.
            range_x (list of two numbers):
                overrides auto-scaling on the x-axis in cartesian coordinates.
            range_y (list of two numbers):
                overrides auto-scaling on the y-axis in cartesian coordinates.
            category_orders (dict): Order of the data categories for the plot
                use for categorical data only. For example,
                with categorical data from the column name 'day'
                {'day':['Monday','Tuesday','Wednesday']}
            labels (dict): X and Y labels. Example:
                {'x':'total_bill', 'y':'count'}
                Overrides other options and column names
            associated_data (list): List of data files associated with the histogram
            start_collapsed (bool): Should the object start out collapsed when
                displayed in the GUI
            **kwargs : additional keyword arguments
        Raises:
            ValueError: If no data OR (list of x and y values) provided
        """

        super().__init__(title, start_collapsed)
        self.dobj_type = "plotlyscatter"
        self.flag = flag
        dict_args: Dict[str, Any] = {}
        # check and set input
        if data is None and not isinstance(x, list) and not isinstance(y, list):
            raise ValueError("No data to operate on")
        if x:
            dict_args["x"] = x
        if y:
            dict_args["y"] = y
        if color:
            dict_args["color"] = color
        if size:
            dict_args["size"] = size
        if symbol:
            dict_args["symbol"] = symbol
        if hover_name:
            dict_args["hover_name"] = hover_name
        if range_color:
            dict_args["range_color"] = range_color
        if range_x:
            dict_args["range_x"] = range_x
        if range_y:
            dict_args["range_y"] = range_y
        if category_orders:
            dict_args["category_orders"] = category_orders
        if labels:
            dict_args["labels"] = labels
        for arg in kwargs:
            if arg != "plotlyFig":
                dict_args[arg] = kwargs[arg]
        if isinstance(data, pd.DataFrame):  # not json serializable
            self.data: Union[
                List[List[float]], pd.DataFrame, np.ndarray, dict, None
            ] = data.to_dict()
        else:
            self.data = data
        self.associated_data = associated_data
        # if data is provided as x and y
        if isinstance(x, list) and isinstance(y, list):
            plotlyfig = px.scatter(**dict_args)
        else:
            plotlyfig = px.scatter(data, **dict_args)
        self.plotlyfig = plotlyfig.to_json()

    def write_displayobj_file(self, outdir, n):
        """Write a json file from a plotly.graph_objects.Figure object

        Args:
            outdir (str): The directory to write the output in
            n (int): The number of the display obj
        """
        # self.plotlyfig_json = os.path.join(outdir, f"plotlyfig{n:03d}_scatter.json")
        # po.write_json(self.plotlyFig, self.plotlyfig_json)
        # self.__dict__.pop("plotlyFig", None)  # remove if present as an attribute
        outfile = os.path.join(outdir, f".results_display{n:03d}_plotlyscatter.json")
        with open(outfile, "w") as of:
            json.dump(self.__dict__, of)


class ResultsDisplayTable(ResultsDisplayObject):
    """An object for the GUI to display a table

    It is best to not instantiate this class directly. Instead create
    it using `create_results_display_object`

    Attributes:
        title (str): The title of the table
        headers (list): The column headers for the table
        table_data (list): A list of lists, on per row
        associated_data (list): A list of the associated data files
        header_tooltips (list): Tooltips for each column. Column header by default
        start_collapsed (bool): Should the object start out collapsed when displayed in
            the GUI
    """

    def __init__(
        self,
        *,
        title: str,
        headers: list,
        table_data: list,
        associated_data: list,
        header_tooltips: Optional[list] = None,
        start_collapsed: bool = False,
        flag: str = "",
    ):
        """Create a ResultsDisplayTable object
        Args:
            title (str): The title of the table
            headers (list): The column headers for the table
            table_data (list): A list of lists, one per row
            associated_data (list): A list of the associated data files
            header_tooltips (list): Tooltips for each column. Column header by default
            start_collapsed (bool): Should the object start out collapsed when displayed
                in the GUI
        Raises:
            ValueError: If the number of columns in the data are inconsistent
            ValueError: If the number of columns in the data doesn't match the
                number of headers
        """
        super().__init__(title, start_collapsed)
        self.dobj_type = "table"
        self.flag = flag
        # checks
        data_lengths = set([len(x) for x in table_data])
        if len(data_lengths) != 1:
            raise ValueError(
                f"Mismatch in number of columns in data: {[len(x) for x in table_data]}"
            )
        if list(data_lengths)[0] != len(headers):
            raise ValueError(
                "Mismatch between number of headers and data columns"
                f"{list(data_lengths)[0]}, {len(headers)}"
            )
        if not header_tooltips or len(header_tooltips) != len(headers):
            header_tooltips = headers

        self.header_tooltips = header_tooltips
        self.headers = headers
        self.table_data = table_data
        self.associated_data = associated_data


class ResultsDisplayMapModel(ResultsDisplayObject):
    """An object for overlaying multiple maps and/or models

    It is best to not instantiate this class directly. Instead create
    it using `create_results_display_object`

    Attributes:
        title (str): The title that appears at the top of the accordian in
            the GUI
        associated_data (list): A list of associated data files
        maps (list): List of map paths, mrc format
        models (list): List of model paths, pdb or mmcif format
        maps_opacity (list): Opacity for each map from 0-1 if not specified set
            at 0.5 for all maps
        models_data (str): Any extra info about the models
        maps_data (str): Any extra info about the maps
        maps_colours (list): Hex values for colouring the maps specific colours, in
            the form "#XXXXXX" where X is a hex digit (0-9 or a-f). If None, the
            standard colours will be used
        models_colours (list): Hex values for colouring the models specific colours, in
            the form "#XXXXXX" where X is a hex digit (0-9 or a-f). If None, the
            standard colours will be used
        start_collapsed (bool): Should the object start out collapsed when displayed in
            the GUI
    Raises:
        ValueError: If no maps or models were specified
        ValueError: If the map is not .mrc format
        ValueError: If models are not in pdb of mmcif format
        ValueError: If the number of maps and map opacities don't match
    """

    def __init__(
        self,
        title: str,
        associated_data: list,
        maps: list = None,
        models: list = None,
        maps_data: str = "",
        models_data: str = "",
        maps_opacity: list = None,
        maps_colours: list = None,
        models_colours: list = None,
        start_collapsed: bool = True,
        flag: str = "",
    ):
        super().__init__(title, start_collapsed)
        self.dobj_type = "mapmodel"
        self.flag = flag
        maps = maps if maps else []
        models = models if models else []
        if maps == [] and models == []:
            raise ValueError(
                "No maps of models specified for ResultsDisplayMapModel objects"
            )
        maps_opacity = [] if maps_opacity is None else maps_opacity
        if len(maps_opacity) == 0:
            mapoval = 1.0 if len(maps) == 1 and len(models) == 0 else 0.5
            maps_opacity = [mapoval] * len(maps)
        if len(maps_opacity) != len(maps):
            raise ValueError(
                "Number of maps and specified opacities don't match for"
                "creating ResultsDisplayMapModel object"
            )
        for map_path in maps:
            if map_path.split(".")[-1] not in ["mrc", "map"]:
                raise ValueError(
                    "ResultsDisplayMapModel objects must have a .mrc map as input"
                )
        self.maps = maps
        self.maps_opacity = maps_opacity

        maps_colours = [] if maps_colours is None else maps_colours
        if maps_colours:
            if len(maps_colours) != len(maps):
                raise ValueError(
                    "Number of specified colors not equal to number of maps"
                )
            if not all([str_is_hex_colour(x) for x in maps_colours]):
                raise ValueError("Invalid colour hex code in 'maps_colours'")
        self.maps_colours = maps_colours

        models_colours = [] if models_colours is None else models_colours
        if models_colours:
            if len(models_colours) != len(models):
                raise ValueError(
                    "Number of specified colors not equal to number of models"
                )
            if not all([str_is_hex_colour(x) for x in models_colours]):
                raise ValueError("Invalid colour hex code in 'models_colours'")
        self.models_colours = models_colours

        formats = ["pdb", "cif", "mmcif", "pdbx"]
        for model_path in models:
            if model_path.split(".")[-1] not in formats:
                raise ValueError(
                    "ResultsDisplayMapModel objects must have a model in one for the "
                    f"following formats is input {formats}"
                )
        self.models = models
        self.maps_data = maps_data
        self.models_data = models_data
        self.associated_data = associated_data


class ResultsDisplayHtml(ResultsDisplayObject):
    """An object for the GUI to display html

    It is best to not instantiate this class directly. Instead create
    it using `create_results_display_object`

    This can be used for general HTML display in Doppio.  Either provide a directory
    with index.html or specify a html file or provide a html string as input.

    Attributes:
        html_dir (str): Path to the html directory (optional)
        html_file (str): Path to a standalone html file or in the given html_dir
                        (optional)
        html_str (str): Input html as string (optional)
        start_collapsed (bool): Should the object start out collapsed when displayed in
            the GUI
    """

    def __init__(
        self,
        *,
        title: str,
        associated_data: list,
        html_dir: str = "",
        html_file: str = "",
        html_str: str = "",
        start_collapsed: bool = False,
        flag: str = "",
    ):
        super().__init__(title, start_collapsed)
        self.dobj_type = "html"
        self.flag = flag
        if os.path.isdir(html_dir):
            self.html_dir = html_dir
            if os.path.isfile(html_file):
                self.html_file = html_file  # specific html file in the directory
            else:
                self.html_file = "index.html"
        elif os.path.isfile(html_file):
            self.html_file = html_file
        elif html_str:
            self.html_str = html_str
        else:
            raise ValueError(
                f"Directory {html_dir} and path {html_file} do not exist."
                f"html string input {html_str} is also empty."
                "Please provide a valid value for atleast one of them"
            )
        self.associated_data = associated_data


class ResultsDisplayRvapi(ResultsDisplayObject):
    """An object for the GUI to display rvapi objects

    It is best to not instantiate this class directly. Instead create
    it using `create_results_display_object`

    This can be used for general HTML display in Doppio.  Create a directory
    with index.html and it will be shown in the results display tab

    Attributes:
        rvapi_dir (str): Path to the rvapi directory
        start_collapsed (bool): Should the object start out collapsed when displayed in
            the GUI
    """

    def __init__(
        self,
        *,
        title: str,
        rvapi_dir: str,
        start_collapsed: bool = False,
        flag: str = "",
    ):
        super().__init__(title, start_collapsed)
        self.dobj_type = "rvapi"
        self.flag = flag
        self.rvapi_dir = rvapi_dir
        if not os.path.isdir(rvapi_dir):
            raise ValueError(f"Directory {rvapi_dir} does not exist")


class ResultsDisplayTextFile(ResultsDisplayObject):
    """An object for the GUI to display ascii tecxt files

    It is best to not instantiate this class directly. Instead create
    it using `create_results_display_object`

    This can be used for default display of files that have ascii encoded text but
    the formats are too variable to make a more complex ResultsDisplayFile

    Attributes:
        file_path (str): Path to the file
        start_collapsed (bool): Should the object start out collapsed when displayed in
            the GUI
    """

    def __init__(
        self,
        *,
        file_path: str,
        title: str = "",
        start_collapsed: bool = False,
        flag: str = "",
    ):
        super().__init__(file_path if not title else title, start_collapsed)
        self.dobj_type = "textfile"
        self.flag = flag
        self.file_path = file_path
        with open(file_path) as f:
            try:
                f.read()
            except UnicodeDecodeError:
                raise ValueError(f"File '{file_path}' is not a valid text file")


class ResultsDisplayPdfFile(ResultsDisplayObject):
    """An object for the GUI to display pdf files

    It is best to not instantiate this class directly. Instead create
    it using `create_results_display_object`

    Attributes:
        file_path (str): Path to the file
        start_collapsed (bool): Should the object start out collapsed when displayed in
            the GUI
    """

    def __init__(
        self,
        *,
        file_path: str,
        title: str = "",
        start_collapsed: bool = False,
        flag: str = "",
    ):
        super().__init__(file_path if not title else title, start_collapsed)
        self.dobj_type = "pdffile"
        self.flag = flag
        self.file_path = file_path


RESULTS_DISPLAY_OBJECTS = {
    "graph": ResultsDisplayGraph,
    "histogram": ResultsDisplayHistogram,
    "html": ResultsDisplayHtml,
    "image": ResultsDisplayImage,
    "mapmodel": ResultsDisplayMapModel,
    "montage": ResultsDisplayMontage,
    "pdffile": ResultsDisplayPdfFile,
    "pending": ResultsDisplayPending,
    "plotlyobj": ResultsDisplayPlotlyObj,
    "plotlyhistogram": ResultsPlotlyHistogram,
    "plotlyscatter": ResultsPlotlyScatter,
    "rvapi": ResultsDisplayRvapi,
    "table": ResultsDisplayTable,
    "text": ResultsDisplayText,
    "textfile": ResultsDisplayTextFile,
}
