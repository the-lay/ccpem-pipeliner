#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from typing import Optional, List, Union, Tuple, Any, Dict, Set
from dataclasses import dataclass
from pipeliner.mrc_image_tools import get_tiff_stats, get_mrc_stats
from pipeliner.deposition_tools.pdb_deposition_objects import DEPOSITION_COMMENT
from pipeliner.starfile_handler import DataStarFile


@dataclass
class Micrograph:
    file: str
    ext: str
    n_frames: int
    dimx: int
    dimy: int
    dtype: str
    headtype: str
    apix: float
    voltage: float
    spherical_abberation: float


def get_imgfile_info(
    imgfile: str, blockname: str, img_block_col: str
) -> Tuple[dict, List[str]]:
    """Get information from the starfile containing image info

    Args:
        imgfile (str): The node object for the
            file
        blockname (str): Name of the images block in the starfile
        img_block_col (str): The name of the column for the images in the image
            data block of the starfile

    Returns:
        tuple: ( Dict with info about the opitcs groups {og_number:
        (apix, voltage, sphere. ab)}, List of full paths (relative to the working
        dir) for all the images in the file, except in the case of movies
        then the path is relative to import dir)
    """
    imgs_sf = DataStarFile(imgfile)
    # optics groups info
    opticsblock = imgs_sf.get_block("optics")
    og_info = opticsblock.find(
        [
            "_rlnOpticsGroup",
            "_rlnMicrographOriginalPixelSize",
            "_rlnVoltage",
            "_rlnSphericalAberration",
        ]
    )
    og_dict = {}  # {og number: pixelsize}
    for og in og_info:
        og_dict[og[0]] = (float(og[1]), float(og[2]), float(og[3]))

    # get optics group info
    imgsblock = imgs_sf.get_block(blockname)
    all_img = imgsblock.find([img_block_col, "_rlnOpticsGroup"])

    return og_dict, all_img


EMPIAR_DATATYPES = {
    0: "('T2', '')",
    1: "('T4', '')",
    2: "(T7, '')",
    3: "('OT', 'transform : complex 16-bit integers')",
    4: "('OT', 'transform : complex 32-bit integers')",
    6: "('T3', '')",
    12: "('OT', '16-bit float')",
}

EMPIAR_TIFF_DATATYPES = {
    "int8": "('T2', '')",
    "int16": "('T4', '')",
    "int32": "(T7, '')",
    "uint16": "('T3', '')",
    "float32": "('OT', '16-bit float')",
}


def empiar_type_is_valid(empobj: Any):
    def empiar_check(emp_dep_obj: Any, attribute: str, number: int):
        """Check that an attribute in empiar format is valid

        Checks values in the form ("T<n>, "") for things like
        EMPIARs, header_format value

        Args:
            emp_dep_obj (dataclass): The EMPIAR DepositionObject
            attribute (str): Name of the attribute to check
            number (int); Max value for the 'T' value, OT (OytherType) will
                always be added as well
        Returns:
            str: The error or None if there is no error
        """
        tvals = ["OT"] + [f"T{x}" for x in range(1, number + 1)]
        value = emp_dep_obj.getatt(attribute)
        if len(value) != 2:
            return "{attribute} is not in the correct format Tuple[str, str]"
        if value[0] not in tvals:
            return (
                f"{value}[0] is an invalid value for {attribute}. "
                "Value must be in {tvals}"
            )

    errors = []
    for ch in ["header_format", "data_format", "voxel_type"]:
        errors.append(empiar_check(empobj, ch, 7))
    errors.append(empiar_check(empobj, "category", 10))

    if not all([x is None for x in errors]):
        raise ValueError()


@dataclass
class EmpiarMovieSetTypeContents:
    name: str = "Multiframe micrograph movies"
    directory: Optional[str] = None
    category: Optional[str] = None
    header_format: Optional[str] = None
    data_format: Optional[str] = None
    num_images_or_tilt_series: Optional[int] = None
    frames_per_image: Optional[int] = None
    voxel_type: Optional[str] = None
    pixel_width: Optional[float] = None
    pixel_height: Optional[float] = None
    details: Optional[str] = None
    image_width: Optional[int] = None
    image_height: Optional[int] = None
    micrographs_file_pattern: Optional[str] = None


@dataclass
class EmpiarMovieSetType:
    """Actual name is 'Multiframe micrograph movies'"""

    multiframe_micrograph_movies: EmpiarMovieSetTypeContents

    def check(self):
        empiar_type_is_valid(self.multiframe_micrograph_movies)
        return self


def empiar_movie_set_type_entry(
    name: str = "Multiframe micrograph movies",
    directory: Optional[str] = None,
    category: Optional[str] = None,
    header_format: Optional[str] = None,
    data_format: Optional[str] = None,
    num_images_or_tilt_series: Optional[int] = None,
    frames_per_image: Optional[int] = None,
    voxel_type: Optional[str] = None,
    pixel_width: Optional[float] = None,
    pixel_height: Optional[float] = None,
    details: Optional[str] = None,
    image_width: Optional[int] = None,
    image_height: Optional[int] = None,
    micrographs_file_pattern: Optional[str] = None,
) -> EmpiarMovieSetType:

    return EmpiarMovieSetType(
        EmpiarMovieSetTypeContents(
            name,
            directory,
            category,
            header_format,
            data_format,
            num_images_or_tilt_series,
            frames_per_image,
            voxel_type,
            pixel_width,
            pixel_height,
            details,
            image_width,
            image_height,
            micrographs_file_pattern,
        )
    )


@dataclass
class EmpiarCorrectedMicsTypeContents:
    name: str = "Corrected micrographs"
    directory: Optional[str] = None
    category: Optional[str] = None
    header_format: Optional[str] = None
    data_format: Optional[str] = None
    num_images_or_tilt_series: Optional[int] = None
    frames_per_image: Optional[int] = None
    voxel_type: Optional[str] = None
    pixel_width: Optional[float] = None
    pixel_height: Optional[float] = None
    details: Optional[str] = None
    image_width: Optional[int] = None
    image_height: Optional[int] = None
    micrographs_file_pattern: Optional[str] = None


@dataclass
class EmpiarCorrectedMicsType:
    corrected_micrographs: EmpiarCorrectedMicsTypeContents

    def check(self):
        empiar_type_is_valid(self.corrected_micrographs)
        return self


def empiar_corrected_mics_type_entry(
    name: str = "Corrected micrographs",
    directory: Optional[str] = None,
    category: Optional[str] = None,
    header_format: Optional[str] = None,
    data_format: Optional[str] = None,
    num_images_or_tilt_series: Optional[int] = None,
    frames_per_image: Optional[int] = None,
    voxel_type: Optional[str] = None,
    pixel_width: Optional[float] = None,
    pixel_height: Optional[float] = None,
    details: Optional[str] = None,
    image_width: Optional[int] = None,
    image_height: Optional[int] = None,
    micrographs_file_pattern: Optional[str] = None,
) -> EmpiarCorrectedMicsType:

    return EmpiarCorrectedMicsType(
        EmpiarCorrectedMicsTypeContents(
            name,
            directory,
            category,
            header_format,
            data_format,
            num_images_or_tilt_series,
            frames_per_image,
            voxel_type,
            pixel_width,
            pixel_height,
            details,
            image_width,
            image_height,
            micrographs_file_pattern,
        )
    )


@dataclass
class EmpiarParticlesTypeContents:
    name: str = "Particle images"
    directory: Optional[str] = None
    category: Optional[str] = None
    header_format: Optional[str] = None
    data_format: Optional[str] = None
    num_images_or_tilt_series: Optional[int] = None
    frames_per_image: Optional[int] = None
    voxel_type: Optional[str] = None
    pixel_width: Optional[float] = None
    pixel_height: Optional[float] = None
    details: Optional[str] = None
    image_width: Optional[int] = None
    image_height: Optional[int] = None
    micrographs_file_pattern: Optional[str] = None
    picked_particles_file_pattern: Optional[str] = None


@dataclass
class EmpiarParticlesType:
    """Actual name is 'Particle images'"""

    particle_images: EmpiarParticlesTypeContents

    def check(self):
        empiar_type_is_valid(self.particle_images)
        return self


def empiar_particles_type_entry(
    name: str = "Particle images",
    directory: Optional[str] = None,
    category: Optional[str] = None,
    header_format: Optional[str] = None,
    data_format: Optional[str] = None,
    num_images_or_tilt_series: Optional[int] = None,
    frames_per_image: Optional[int] = None,
    voxel_type: Optional[str] = None,
    pixel_width: Optional[float] = None,
    pixel_height: Optional[float] = None,
    details: Optional[str] = None,
    image_width: Optional[int] = None,
    image_height: Optional[int] = None,
    micrographs_file_pattern: Optional[str] = None,
    picked_particles_file_pattern: Optional[str] = None,
) -> EmpiarParticlesType:

    return EmpiarParticlesType(
        EmpiarParticlesTypeContents(
            name,
            directory,
            category,
            header_format,
            data_format,
            num_images_or_tilt_series,
            frames_per_image,
            voxel_type,
            pixel_width,
            pixel_height,
            details,
            image_width,
            image_height,
            micrographs_file_pattern,
            picked_particles_file_pattern,
        )
    )


@dataclass
class EmpiarRefinedParticlesTypeContents:
    name: str = "Per-particle motion corrected particle images"
    directory: Optional[str] = None
    category: Optional[str] = None
    header_format: Optional[str] = None
    data_format: Optional[str] = None
    num_images_or_tilt_series: Optional[int] = None
    frames_per_image: Optional[int] = None
    voxel_type: Optional[str] = None
    pixel_width: Optional[float] = None
    pixel_height: Optional[float] = None
    details: Optional[str] = None
    image_width: Optional[int] = None
    image_height: Optional[int] = None
    micrographs_file_pattern: Optional[str] = None
    picked_particles_file_pattern: Optional[str] = None


@dataclass
class EmpiarRefinedParticlesType:
    """Actual name is: 'Per-particle motion corrected particle images'"""

    per_particle_motion_corrected_particle_images: EmpiarRefinedParticlesTypeContents

    def check(self):
        empiar_type_is_valid(self.per_particle_motion_corrected_particle_images)
        return self


def empiar_refined_particles_type_entry(
    name: str = "Per-particle motion corrected particle images",
    directory: Optional[str] = None,
    category: Optional[str] = None,
    header_format: Optional[str] = None,
    data_format: Optional[str] = None,
    num_images_or_tilt_series: Optional[int] = None,
    frames_per_image: Optional[int] = None,
    voxel_type: Optional[str] = None,
    pixel_width: Optional[float] = None,
    pixel_height: Optional[float] = None,
    details: Optional[str] = None,
    image_width: Optional[int] = None,
    image_height: Optional[int] = None,
    micrographs_file_pattern: Optional[str] = None,
    picked_particles_file_pattern: Optional[str] = None,
) -> EmpiarRefinedParticlesType:

    return EmpiarRefinedParticlesType(
        EmpiarRefinedParticlesTypeContents(
            name,
            directory,
            category,
            header_format,
            data_format,
            num_images_or_tilt_series,
            frames_per_image,
            voxel_type,
            pixel_width,
            pixel_height,
            details,
            image_width,
            image_height,
            micrographs_file_pattern,
            picked_particles_file_pattern,
        )
    )


def prepare_empiar_raw_mics(movfile: str) -> List[EmpiarMovieSetType]:
    """Prepare the raw micrographs portion of an EMPIAR deposition

    Args:
        movfile (str): Movies star file to operate on

    Returns:
        List[:class:`EmpiarMovieSetType`]: A
            DepositionObject used to create a deposition
    """
    depo_objs = []

    # separate out all the movies and make sure they have the same
    # pixelsize, data type and etc...

    og_dict, movs = get_imgfile_info(movfile, "movies", "_rlnMicrographMovieName")

    # prepend the import dir name to each move file
    dn = os.path.dirname(movfile)
    full_path_movs = [(os.path.join(dn, x[0]), x[1]) for x in movs]

    # make the sets to split up the movies
    mov_sets = set([os.path.dirname(x[0]) for x in full_path_movs])
    split_movies: Dict[str, List[Micrograph]] = {}
    for ms in mov_sets:
        split_movies[ms] = []

    # get info about each movie, tiffs and mrcs handled separately
    donegroups = []
    for movie in full_path_movs:
        mov_group = os.path.dirname(movie[0])
        movie_error = False
        # tiffs
        if movie[0].split(".")[-1] in ("tif", "tiff"):
            if mov_group not in donegroups:
                tiffstats = get_tiff_stats(movie[0])
            if len(tiffstats[0]) != 3:
                dimx, dimy = tiffstats[0][0]
                n_frames = len(tiffstats)
                dtype = EMPIAR_TIFF_DATATYPES[tiffstats[0][6].split(": ")[1]]
            # just in case of single frame movies
            else:
                dimx, dimy, n_frames = tiffstats[0][0]
                dtype = EMPIAR_TIFF_DATATYPES[tiffstats[6].split(": ")[1]]
            headtype = "('T3', '')"

        # mrcs
        elif movie[0].split(".")[-1] in ("mrc", "mrcs"):
            if mov_group not in donegroups:
                mstats = get_mrc_stats(movie[0])
            if len(mstats[0]) != 3:
                dimx = mstats[0][0][0]
                dimy = mstats[0][0][1]
                n_frames = len(mstats)
                dtype = EMPIAR_DATATYPES[int(mstats[0][6].split(": ")[0])]
            # in case of single frame movies
            else:
                dimx, dimy, n_frames = mstats[0]
                dtype = EMPIAR_DATATYPES[int(mstats[6].split(": ")[0])]
            headtype = "('T2', '')"
        else:
            print(f"WARNING: Unknown movie type encountered for {movie[0]}")
            movie_error = True

        if not movie_error:
            # make a Micrograph object for this movie
            the_mov = Micrograph(
                file=movie[0],
                ext=movie[0].split(".")[-1],
                n_frames=n_frames,
                dimx=dimx,
                dimy=dimy,
                headtype=headtype,
                dtype=dtype,
                apix=og_dict[movie[1]][0],
                voltage=og_dict[movie[1]][1],
                spherical_abberation=og_dict[movie[1]][2],
            )
            # assign the movie to its group
            split_movies[mov_group].append(the_mov)
            donegroups.append(mov_group)

    # make the movie imagesets
    for movset in split_movies:
        # check that all movies in this dir have same stats
        nf: Tuple[str, set] = (
            "frame number",
            set([x.n_frames for x in split_movies[movset]]),
        )
        xs: Tuple[str, set] = (
            "x dimension",
            set([x.dimx for x in split_movies[movset]]),
        )
        ys: Tuple[str, set] = (
            "y dimension",
            set([x.dimy for x in split_movies[movset]]),
        )
        ap: Tuple[str, set] = (
            "pixel size",
            set([x.apix for x in split_movies[movset]]),
        )
        dt: Tuple[str, set] = (
            "data type",
            set([x.dtype for x in split_movies[movset]]),
        )
        ht: Tuple[str, set] = (
            "header format",
            set([x.headtype for x in split_movies[movset]]),
        )
        ex: Tuple[str, set] = (
            "file extension",
            set([x.ext for x in split_movies[movset]]),
        )
        # don't need to check these two
        vt = set([x.voltage for x in split_movies[movset]])
        sa = set([x.spherical_abberation for x in split_movies[movset]])
        problem = False
        for check in [nf, xs, ys, ap, dt, ht, ex]:
            if len(check[1]) > 1:
                print(
                    f"WARNING: Skipping movies in {movset} because {check[0]}"
                    "is not the same for every movie"
                )
                problem = True
        if problem:
            continue

        # text for spherical abberation and voltage in details field
        spherical_abberation = str(list(sa)) if len(list(sa)) > 1 else list(sa)[0]
        vlt = str(list(vt)) if len(list(sa)) > 1 else list(sa)[0]

        # prepare the common file path for this movie set
        this_set = [x.file for x in split_movies[movset]]
        reversed_strings = [x[::-1] for x in this_set]
        reversed_lcs = os.path.commonprefix(reversed_strings)
        lcs = reversed_lcs[::-1]
        file_path = f"{os.path.commonprefix(this_set)}*{lcs}"
        deets = (
            f"Voltage {vlt}; Spherical aberration {spherical_abberation}; Movie "
            f"data in file: {movfile}; {DEPOSITION_COMMENT}"
        )
        depoobj = empiar_movie_set_type_entry(
            name="Multiframe micrograph movies",
            directory=movset,
            category="('T2', '')",
            header_format=str(list(ht[1])[0]),
            data_format=str(list(dt[1])[0]),
            num_images_or_tilt_series=len(split_movies[movset]),
            frames_per_image=int(list(nf[1])[0]),
            voxel_type=str(list(dt[1])[0]),
            pixel_width=float(list(ap[1])[0]),
            pixel_height=float(list(ap[1])[0]),
            details=deets,
            image_width=int(list(xs[1])[0]),
            image_height=int(list(ys[1])[0]),
            micrographs_file_pattern=file_path,
        )
        depo_objs.append(depoobj)
    return depo_objs


def prepare_empiar_mics_parts(
    mpfile: str, is_parts: bool, is_cor_parts: bool
) -> List[
    Union[EmpiarParticlesType, EmpiarRefinedParticlesType, EmpiarCorrectedMicsType]
]:
    """Prepare the micrographs or particles portion of an EMPIAR deposition

    Args:

        mpfile (str): The name of the file containing the micrographs or particles
        is_parts (bool): Is the image set particles? will affect the info in the
            details
        is_cor_parts (bool): Is the image set corrected (polished particles)?
    Returns:
        list: A list of deposition objects
    """
    # separate out all the micrographs and make sure they have the same
    # pixelsize, data type and etc...

    imgs_block = "particles" if is_parts else "micrographs"
    img_file_col = "_rlnImageName" if is_parts else "_rlnMicrographName"
    imginfo = get_imgfile_info(mpfile, imgs_block, img_file_col)
    og_dict = imginfo[0]
    full_path_mics: Union[Set[Tuple[str, str]], List[str]] = imginfo[1]

    # make the sets to split up the micrographs
    # strip the particle numbers if necessary
    if is_parts:
        full_path_mics = set([(x[0].split("@")[1], x[1]) for x in full_path_mics])
        mic_sets = set([os.path.dirname(x[0]) for x in full_path_mics])
        header_type = "T2"
        if is_cor_parts:
            category = "T6"
            name = "Per-particle motion corrected particle images"

        else:
            category = "T5"
            name = "Particle images"

    else:
        mic_sets = set([os.path.dirname(x[0]) for x in full_path_mics])
        header_type = "T1"
        category = "T1"
        name = "Corrected micrographs"

    split_micrographs: Dict[str, List[Micrograph]] = {}
    for ms in mic_sets:
        split_micrographs[ms] = []

    # get info about each micrograph, tiffs and mrcs handled separately
    # TODO: This is way to slow, maybe only do the first of each?
    exp_ext = "mrcs" if is_parts else "mrc"
    donegroups = []
    for micrograph in full_path_mics:
        mic_group = os.path.dirname(micrograph[0])
        micrograph_error = False
        if micrograph[0].split(".")[-1] == exp_ext:
            # the stats are only calculated on the 1st mic in the dir
            # assumes they have the same type and dimensions
            # doing it for every mic is very slow...
            if mic_group not in donegroups:
                mstats = get_mrc_stats(micrograph[0])
            dimx = mstats[0][0]
            dimy = mstats[0][1]
            n_frames = mstats[0][2]
            dtype = EMPIAR_DATATYPES[int(mstats[6].split(":")[0])]
            headtype = f"('{header_type}', '')"
        else:
            print(f"WARNING: Unknown image type encountered for {micrograph[0]}")
            micrograph_error = True

        if not micrograph_error:
            # make a Micrograph object for this micrograph,
            the_mic = Micrograph(
                file=micrograph[0],
                ext=micrograph[0].split(".")[-1],
                n_frames=n_frames,
                dimx=dimx,
                dimy=dimy,
                headtype=headtype,
                dtype=dtype,
                apix=og_dict[micrograph[1]][0],
                voltage=og_dict[micrograph[1]][1],
                spherical_abberation=og_dict[micrograph[1]][2],
            )
            # assign the micrograph to its group
            split_micrographs[mic_group].append(the_mic)
            donegroups.append(mic_group)

    # make the micrograph imagesets
    depoobjs = []
    for micset in split_micrographs:
        # check that all micrographs in this dir have same stats

        nf = 1  # number of frames is always 1

        xs = ("x dimension", set([x.dimx for x in split_micrographs[micset]]))
        ys = ("y dimension", set([x.dimy for x in split_micrographs[micset]]))
        ap = ("pixel size", set([x.apix for x in split_micrographs[micset]]))
        dt = ("data type", set([x.dtype for x in split_micrographs[micset]]))
        ht = ("header format", set([x.headtype for x in split_micrographs[micset]]))
        # don't need to check these two
        vt = set([x.voltage for x in split_micrographs[micset]])
        sa = set([x.spherical_abberation for x in split_micrographs[micset]])

        # this checking is currently disabled, because we are not
        # actually checking stats on every image, it's too slow

        # problem = False
        # for check in [xs, ys, ap, dt, ht, ex]:
        #    if len(check[1]) > 1:
        #        print(
        #            f"WARNING: Skipping micrographs in {micset} because {check[0]}"
        #            "is not the same for every micrograph"
        #        )
        #        problem = True
        # if problem:
        #    continue

        # text for spherical abberation and voltage in details field
        spherical_abberation = str(list(sa)) if len(list(sa)) > 1 else list(sa)[0]
        vlt = str(list(vt)) if len(list(sa)) > 1 else list(sa)[0]

        part_count = DataStarFile(mpfile).count_block("particles") if is_parts else 0

        # prepare the commont file path for this micrograph set
        this_set = [x.file for x in split_micrographs[micset]]
        reversed_strings = [x[::-1] for x in this_set]
        reversed_lcs = os.path.commonprefix(reversed_strings)
        lcs = reversed_lcs[::-1]
        file_pat = f"{os.path.commonprefix(this_set)}*{lcs}"
        pcount = f"{part_count} total particles; " if is_parts else ""
        deets = (
            f"{pcount}Voltage {vlt}; Spherical aberration {spherical_abberation}; "
            f"Image data in file: {mpfile}; {DEPOSITION_COMMENT}"
        )
        depoobj: Union[
            EmpiarParticlesType,
            EmpiarRefinedParticlesType,
            EmpiarCorrectedMicsType,
            EmpiarRefinedParticlesType,
        ]

        if is_cor_parts:
            depoobj = empiar_refined_particles_type_entry(
                name=name,
                directory=micset,
                category=f"('{category}', '')",
                header_format=str(list(ht[1])[0]),
                data_format=str(list(dt[1])[0]),
                num_images_or_tilt_series=len(split_micrographs[micset]),
                frames_per_image=nf,
                voxel_type=str(list(dt[1])[0]),
                pixel_width=float(list(ap[1])[0]),
                pixel_height=float(list(ap[1])[0]),
                details=deets,
                image_width=int(list(xs[1])[0]),
                image_height=int(list(ys[1])[0]),
                micrographs_file_pattern=file_pat,
                picked_particles_file_pattern=mpfile,
            )
        elif is_parts:
            depoobj = empiar_particles_type_entry(
                name=name,
                directory=micset,
                category=f"('{category}', '')",
                header_format=str(list(ht[1])[0]),
                data_format=str(list(dt[1])[0]),
                num_images_or_tilt_series=len(split_micrographs[micset]),
                frames_per_image=nf,
                voxel_type=str(list(dt[1])[0]),
                pixel_width=float(list(ap[1])[0]),
                pixel_height=float(list(ap[1])[0]),
                details=deets,
                image_width=int(list(xs[1])[0]),
                image_height=int(list(ys[1])[0]),
                micrographs_file_pattern=file_pat,
                picked_particles_file_pattern=mpfile,
            )
        else:
            depoobj = empiar_corrected_mics_type_entry(
                name=name,
                directory=micset,
                category=f"('{category}', '')",
                header_format=str(list(ht[1])[0]),
                data_format=str(list(dt[1])[0]),
                num_images_or_tilt_series=len(split_micrographs[micset]),
                frames_per_image=nf,
                voxel_type=str(list(dt[1])[0]),
                pixel_width=float(list(ap[1])[0]),
                pixel_height=float(list(ap[1])[0]),
                details=deets,
                image_width=int(list(xs[1])[0]),
                image_height=int(list(ys[1])[0]),
                micrographs_file_pattern=file_pat,
            )
        depoobjs.append(depoobj)

    return depoobjs
