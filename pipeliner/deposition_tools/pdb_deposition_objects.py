#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

from typing import Optional, Tuple, Any
from dataclasses import dataclass
from pipeliner import __version__ as pipevers

DEPOSITION_COMMENT = f"CCP-EM pipeliner {pipevers}"


@dataclass
class SoftwareTypeContents:
    name: Optional[str] = None
    version: Optional[str] = None
    processing_details: Optional[str] = None


@dataclass
class SoftwareType:
    software_type: SoftwareTypeContents = SoftwareTypeContents()

    def check(self):
        return self


def software_type_entry(
    name: Optional[str] = None,
    version: Optional[str] = None,
    processing_details: Optional[str] = None,
) -> SoftwareType:

    return SoftwareType(SoftwareTypeContents(name, version, processing_details))


def software_list_is_valid(software_list: Tuple[Any, ...]):
    sl_types = [type(x) == SoftwareType for x in software_list]
    if not all(sl_types):
        return False
    else:
        return True


@dataclass
class OtherTypeContents:
    software_list: Tuple[SoftwareType, ...] = ()
    details: Optional[str] = None


@dataclass
class OtherType:
    other: OtherTypeContents = OtherTypeContents()

    def check(self):
        return self


def other_type_entry(
    software_list: Tuple[SoftwareType, ...] = (),
    details: Optional[str] = None,
) -> OtherType:
    return OtherType(OtherTypeContents(software_list, details))


# ctf correction
CTF_CORRECTION_SPACE_OPTIONS = ["real", "reciprocal", None]


@dataclass
class PhaseReversalTypeContents:
    anisotropic: Optional[bool] = None
    correction_space: Optional[str] = None


@dataclass
class PhaseReversalType:
    phase_reversal: PhaseReversalTypeContents = PhaseReversalTypeContents()

    def check(self):
        if self.phase_reversal.correction_space not in CTF_CORRECTION_SPACE_OPTIONS:
            raise ValueError("Invalid CTF correction space")
        return self


def phase_reversal_type_entry(
    anisotropic: Optional[bool] = None,
    correction_space: Optional[str] = None,
) -> PhaseReversalType:

    return PhaseReversalType(PhaseReversalTypeContents(anisotropic, correction_space))


AMPLITUDE_CORRECTION_TYPES = ["real", "reciprocal", None]


@dataclass
class AmplitudeCorrectionTypeContents:
    factor: Optional[float] = None
    correction_space: Optional[str] = None


@dataclass
class AmplitudeCorrectionType:
    amplitude_correction: AmplitudeCorrectionTypeContents = (
        AmplitudeCorrectionTypeContents()
    )

    def check(self):
        if self.amplitude_correction.correction_space not in AMPLITUDE_CORRECTION_TYPES:
            raise ValueError("Invalid amplitude correction space type")
        return self


def amplitude_correction_type_entry(
    factor: Optional[float] = None,
    correction_space: Optional[str] = None,
) -> AmplitudeCorrectionType:

    return AmplitudeCorrectionType(
        AmplitudeCorrectionTypeContents(factor, correction_space)
    )


# TO DO: this one has complete validation, need to do this on all others
# can some of the validation be done more effeciently with the type hints?
CTF_CORR_OP_OPTS = ["multiplication", "division", None]


@dataclass
class CtfCorrectionTypeContents:
    phase_reversal: Optional[PhaseReversalType] = None
    amplitude_correction: Optional[AmplitudeCorrectionType] = None
    correction_operation: Optional[str] = None
    software_list: Tuple[SoftwareType, ...] = ()
    details: Optional[str] = None


@dataclass
class CtfCorrectionType:
    ctf_correction: CtfCorrectionTypeContents = CtfCorrectionTypeContents()

    def check(self):
        errors = []
        if self.ctf_correction.correction_operation not in CTF_CORR_OP_OPTS:
            errors.append("Invalid CTF correction operation")
        if type(self.ctf_correction.phase_reversal) not in (
            type(None),
            PhaseReversalType,
        ):
            errors.append("phase_reversal must be PhaseReversalType or None")
        if type(self.ctf_correction.amplitude_correction) not in (
            type(None),
            AmplitudeCorrectionType,
        ):
            errors.append(
                "amplitude_correction must be AmplitudeCorrectionType or None"
            )
        if not software_list_is_valid(self.ctf_correction.software_list):
            errors.append("software_list must contain SoftwareType objects")
        if len(errors) > 0:
            raise ValueError(", ".join(errors))
        return self


def ctf_correction_type_entry(
    phase_reversal: Optional[PhaseReversalType] = None,
    amplitude_correction: Optional[AmplitudeCorrectionType] = None,
    correction_operation: Optional[str] = None,
    software_list: Tuple[SoftwareType, ...] = (),
    details: Optional[str] = None,
) -> CtfCorrectionType:

    return CtfCorrectionType(
        CtfCorrectionTypeContents(
            phase_reversal,
            amplitude_correction,
            correction_operation,
            software_list,
            details,
        )
    )


# particle picking
@dataclass
class ParticleSelectionTypeContents:
    number_selected: Optional[int] = None
    reference_model: Optional[str] = None
    method: Optional[str] = None
    software: Tuple[SoftwareType, ...] = ()
    details: Optional[str] = None


@dataclass
class ParticleSelectionType:
    particle_selection: ParticleSelectionTypeContents = ParticleSelectionTypeContents()

    def check(self):
        return self


def particle_selection_type_entry(
    number_selected: Optional[int] = None,
    reference_model: Optional[str] = None,
    method: Optional[str] = None,
    software: Tuple[SoftwareType, ...] = (),
    details: Optional[str] = None,
) -> ParticleSelectionType:

    return ParticleSelectionType(
        ParticleSelectionTypeContents(
            number_selected,
            reference_model,
            method,
            software,
            details,
        )
    )


# starting model
@dataclass
class RandomConicalTiltTypeContents:
    number_images: Optional[int] = None
    tilt_angle: Optional[float] = None
    software_list: Tuple[SoftwareType, ...] = ()
    details: Optional[str] = None


@dataclass
class RandomConicalTiltType:
    random_conical_tilt: RandomConicalTiltTypeContents = RandomConicalTiltTypeContents()

    def check(self):
        if not software_list_is_valid(self.random_conical_tilt.software_list):
            raise ValueError("software_list must contain SoftwareType objects")
        return self


def random_conical_tilt_type_entry(
    number_images: Optional[int] = None,
    tilt_angle: Optional[float] = None,
    software_list: Tuple[SoftwareType, ...] = (),
    details: Optional[str] = None,
) -> RandomConicalTiltType:

    return RandomConicalTiltType(
        RandomConicalTiltTypeContents(
            number_images,
            tilt_angle,
            software_list,
            details,
        )
    )


@dataclass
class OrthogonalTiltTypeContents:
    software_list: Tuple[SoftwareType, ...] = ()
    number_images: Optional[int] = None
    tilt_angle1: Optional[float] = None
    tilt_angle2: Optional[float] = None
    details: Optional[str] = None


@dataclass
class OrthogonalTiltType:
    orthogonal_tilt: OrthogonalTiltTypeContents = OrthogonalTiltTypeContents()

    def check(self):
        if not software_list_is_valid(self.orthogonal_tilt.software_list):
            raise ValueError("software_list must contain SoftwareType objects")
        return self


def orthogonal_tilt_type_entry(
    software_list: Tuple[SoftwareType, ...] = (),
    number_images: Optional[int] = None,
    tilt_angle1: Optional[float] = None,
    tilt_angle2: Optional[float] = None,
    details: Optional[str] = None,
) -> OrthogonalTiltType:

    return OrthogonalTiltType(
        OrthogonalTiltTypeContents(
            software_list,
            number_images,
            tilt_angle1,
            tilt_angle2,
            details,
        )
    )


# TODO: put regex validation of emdb_id
@dataclass
class EmdbTypeContents:
    emdb_id: Optional[str] = None


@dataclass
class EmdbType:
    emdb_id: EmdbTypeContents = EmdbTypeContents()

    def check(self):
        return self


def emdb_type_entry(emdb_id: Optional[str] = None):
    return EmdbType(EmdbTypeContents(emdb_id))


# TODO: put regex validation of both fields
@dataclass
class PdbChainTypeContents:
    chain_id: Optional[str] = None
    residue_range: Optional[str] = None


@dataclass
class PdbChainType:
    pdb_chain: PdbChainTypeContents = PdbChainTypeContents()

    def check(self):
        return self


def pdb_chain_type_entry(
    chain_id: Optional[str] = None,
    residue_range: Optional[str] = None,
) -> PdbChainType:

    return PdbChainType(PdbChainTypeContents(chain_id, residue_range))


@dataclass
class PdbModelTypeContents:
    pdb_id: Optional[str] = None
    chain_id_list: Tuple[PdbChainType, ...] = ()


@dataclass
class PdbModelType:
    pdb_model: PdbModelTypeContents = PdbModelTypeContents()

    def check(self):
        if len(self.pdb_model.chain_id_list) > 0:
            types = [type(x) == PdbChainType for x in self.pdb_model.chain_id_list]
            if not all(types):
                raise ValueError("chain_id_list must contain PdbChainType objects")
        return self


def pdb_model_type_entry(
    pdb_id: Optional[str] = None, chain_id_list: Tuple[PdbChainType, ...] = ()
) -> PdbModelType:

    return PdbModelType(PdbModelTypeContents(pdb_id, chain_id_list))


@dataclass
class InSilicoStartupModelTypeContents:
    startup_model: Optional[str] = None
    details: Optional[str] = None


@dataclass
class InSilicoStartupModelType:
    """actual string name shoud be: startup_model type_of_model='insilico'"""

    in_silico_startup_model: InSilicoStartupModelTypeContents = (
        InSilicoStartupModelTypeContents()
    )

    def check(self):
        return self


def in_silico_startup_model_type_entry(
    startup_model: Optional[str] = None,
    details: Optional[str] = None,
) -> InSilicoStartupModelType:

    return InSilicoStartupModelType(
        InSilicoStartupModelTypeContents(startup_model, details)
    )


@dataclass
class OtherStartupModelTypeContents:
    startup_model: Optional[str] = None
    details: Optional[str] = None


@dataclass
class OtherStartupModelType:

    other_startup_model: OtherStartupModelTypeContents = OtherStartupModelTypeContents()

    def check(self):
        return self


def other_startup_model_type_entry(
    startup_model: Optional[str] = None,
    details: Optional[str] = None,
) -> OtherStartupModelType:

    return OtherStartupModelType(OtherStartupModelTypeContents(startup_model, details))


@dataclass
class RandomConicalTiltStartupModelTypeContents:
    startup_model: Optional[RandomConicalTiltType] = None
    details: Optional[str] = None


@dataclass
class RandomConicalTiltStartupModelType:

    random_conical_tilt_startup_model: RandomConicalTiltStartupModelTypeContents = (
        RandomConicalTiltStartupModelTypeContents()
    )

    def check(self):
        if type(self.random_conical_tilt_startup_model.startup_model) not in (
            RandomConicalTiltType,
            type(None),
        ):
            raise ValueError("startup_model must be RandomConicalTiltType or None")
        return self


def random_conical_tilt_startup_model_type_entry(
    startup_model: Optional[RandomConicalTiltType] = None,
    details: Optional[str] = None,
) -> RandomConicalTiltStartupModelType:

    return RandomConicalTiltStartupModelType(
        RandomConicalTiltStartupModelTypeContents(startup_model, details)
    )


@dataclass
class EmdbStartupModelTypeContents:
    startup_model: Optional[EmdbType] = None
    details: Optional[str] = None


@dataclass
class EmdbStartupModelType:
    """Actual name is: startup_model type_of_model='emdb_id'"""

    emdb_id_starting_model: EmdbStartupModelTypeContents = (
        EmdbStartupModelTypeContents()
    )

    def check(self):
        if type(self.emdb_id_starting_model.startup_model) not in (
            EmdbType,
            type(None),
        ):
            raise ValueError("startup_model must be EmdbType or None")
        return self


def emdb_startup_model_type_entry(
    startup_model: Optional[EmdbType] = EmdbType(),
    details: Optional[str] = None,
) -> EmdbStartupModelType:

    return EmdbStartupModelType(EmdbStartupModelTypeContents(startup_model, details))


@dataclass
class PdbModelStartupModelTypeContents:
    startup_model: Optional[PdbModelType] = None
    details: Optional[str] = None


@dataclass
class PdbModelStartupModelType:

    pdb_model_startup_model: PdbModelStartupModelTypeContents = (
        PdbModelStartupModelTypeContents()
    )

    def check(self):
        if type(self.pdb_model_startup_model.startup_model) not in (
            PdbModelType,
            type(None),
        ):
            raise ValueError("startup_model must be PdbModelType or None")
        return self


def pdb_model_startup_model_type_entry(
    startup_model: Optional[PdbModelType] = PdbModelType(),
    details: Optional[str] = None,
) -> PdbModelStartupModelType:

    return PdbModelStartupModelType(
        PdbModelStartupModelTypeContents(startup_model, details)
    )


# initial angle assignment / final angle assignment
ANGLE_ASSIGNMENT_METHODS = [
    "ANGULAR RECONSTITUTION",
    "COMMON LINE",
    "NOT APPLICABLE",
    "OTHER",
    "PROJECTION MATCHING",
    "RANDOM ASSIGNMENT",
    "MAXIMUM LIKELIHOOD",
    None,
]


@dataclass
class ProjectionMatchingProcessingTypeContents:
    number_reference_projections: Optional[int] = None
    merit_function: Optional[str] = None
    angular_sampling: Optional[float] = None


@dataclass
class ProjectionMatchingProcessingType:
    projection_matching_processing: ProjectionMatchingProcessingTypeContents = (
        ProjectionMatchingProcessingTypeContents()
    )

    def check(self):
        return self


def projection_matching_processing_type_entry(
    number_reference_projections: Optional[int] = None,
    merit_function: Optional[str] = None,
    angular_sampling: Optional[float] = None,
) -> ProjectionMatchingProcessingType:

    return ProjectionMatchingProcessingType(
        ProjectionMatchingProcessingTypeContents(
            number_reference_projections, merit_function, angular_sampling
        )
    )


@dataclass
class InitialAngleTypeContents:
    type: Optional[str] = None
    projection_matching_processing: Optional[ProjectionMatchingProcessingType] = None
    software_list: Tuple[SoftwareType, ...] = ()
    details: Optional[str] = None


@dataclass
class InitialAngleType:
    initial_angle_assignment: InitialAngleTypeContents = InitialAngleTypeContents()

    def check(self):
        errors = []
        if self.initial_angle_assignment.type not in ANGLE_ASSIGNMENT_METHODS:
            errors.append("invalid initial angle assignment type")
        if type(self.initial_angle_assignment.projection_matching_processing) not in (
            type(None),
            ProjectionMatchingProcessingType,
        ):
            errors.append(
                "projection_matching_processing must be ProjectionMatching"
                "Processing object or None"
            )
        if not software_list_is_valid(self.initial_angle_assignment.software_list):
            errors.append("software_list must contain SoftwareType objects")
        if len(errors) > 0:
            raise ValueError(", ".join(errors))
        else:
            return self


def initial_angle_type_entry(
    entry_type: Optional[str] = None,
    projection_matching_processing: Optional[ProjectionMatchingProcessingType] = None,
    software_list: Tuple[SoftwareType, ...] = (),
    details: Optional[str] = None,
) -> InitialAngleType:

    return InitialAngleType(
        InitialAngleTypeContents(
            entry_type,
            projection_matching_processing,
            software_list,
            details,
        )
    )
    # need to check against initial angle types


@dataclass
class FinalAngleTypeContents:
    type: Optional[str] = None
    projection_matching_processing: Optional[ProjectionMatchingProcessingType] = None
    software_list: Tuple[SoftwareType, ...] = ()
    details: Optional[str] = None


@dataclass
class FinalAngleType:
    final_angle_assignment: FinalAngleTypeContents = FinalAngleTypeContents()

    def check(self):
        errors = []
        if self.final_angle_assignment.type not in ANGLE_ASSIGNMENT_METHODS:
            errors.append("invalid initial angle assignment type")
        if type(self.final_angle_assignment.projection_matching_processing) not in (
            type(None),
            ProjectionMatchingProcessingType,
        ):
            errors.append(
                "projection_matching_processing must be ProjectionMatching"
                "Processing object or None"
            )
        if not software_list_is_valid(self.final_angle_assignment.software_list):
            errors.append("software_list must contain SoftwareType objects")
        if len(errors) > 0:
            raise ValueError(", ".join(errors))
        else:
            return self


def final_angle_type_entry(
    entry_type: Optional[str] = None,
    projection_matching_processing: Optional[ProjectionMatchingProcessingType] = None,
    software_list: Tuple[SoftwareType, ...] = (),
    details: Optional[str] = None,
) -> FinalAngleType:

    return FinalAngleType(
        FinalAngleTypeContents(
            entry_type,
            projection_matching_processing,
            software_list,
            details,
        )
    )


# final MRA
@dataclass
class FinalMultiReferenceAlignmentTypeContents:
    number_reference_projections: Optional[int] = None
    merit_function: Optional[str] = None
    angular_sampling: Optional[float] = None
    software_list: Tuple[SoftwareType, ...] = ()
    details: Optional[str] = None


@dataclass
class FinalMultiReferenceAlignmentType:
    final_multi_reference_alignment: FinalMultiReferenceAlignmentTypeContents = (
        FinalMultiReferenceAlignmentTypeContents()
    )

    def check(self):
        if not software_list_is_valid(
            self.final_multi_reference_alignment.software_list
        ):
            raise ValueError("Software list must contain SoftwareType objects")
        return self


def final_multi_reference_alignment_type_entry(
    number_reference_projections: Optional[int] = None,
    merit_function: Optional[str] = None,
    angular_sampling: Optional[float] = None,
    software_list: Tuple[SoftwareType, ...] = (),
    details: Optional[str] = None,
) -> FinalMultiReferenceAlignmentType:

    return FinalMultiReferenceAlignmentType(
        FinalMultiReferenceAlignmentTypeContents(
            number_reference_projections,
            merit_function,
            angular_sampling,
            software_list,
            details,
        )
    )


# final 2D classification
@dataclass
class Final2DClassificationTypeContents:
    number_classes: Optional[int] = None
    average_number_members_per_class: Optional[int] = None
    software_list: Tuple[SoftwareType, ...] = ()
    details: Optional[str] = None


@dataclass
class Final2DClassificationType:
    final_two_d_classification: Final2DClassificationTypeContents = (
        Final2DClassificationTypeContents()
    )

    def check(self):
        if not software_list_is_valid(self.final_two_d_classification.software_list):
            raise ValueError("Software list must contain SoftwareType objects")
        return self


def final_2d_classification_type_entry(
    number_classes: Optional[int] = None,
    average_number_members_per_class: Optional[int] = None,
    software_list: Tuple[SoftwareType, ...] = (),
    details: Optional[str] = None,
) -> Final2DClassificationType:

    return Final2DClassificationType(
        Final2DClassificationTypeContents(
            number_classes,
            average_number_members_per_class,
            software_list,
            details,
        )
    )


# final 3D classification
@dataclass
class Final3DClassificationTypeContents:
    number_classes: Optional[int] = None
    average_number_members_per_class: Optional[int] = None
    software_list: Tuple[SoftwareType, ...] = ()
    details: Optional[str] = None


@dataclass
class Final3DClassificationType:
    final_three_d_classification: Final3DClassificationTypeContents = (
        Final3DClassificationTypeContents()
    )

    def check(self):
        if not software_list_is_valid(self.final_three_d_classification.software_list):
            raise ValueError("Software list must contain SoftwareType objects")
        return self


def final_3d_classification_type_entry(
    number_classes: Optional[int] = None,
    average_number_members_per_class: Optional[int] = None,
    software_list: Tuple[SoftwareType, ...] = (),
    details: Optional[str] = None,
) -> Final3DClassificationType:

    return Final3DClassificationType(
        Final3DClassificationTypeContents(
            number_classes,
            average_number_members_per_class,
            software_list,
            details,
        )
    )


@dataclass
class DimensionsTypeContents:
    radius: Optional[float] = None
    width: Optional[float] = None
    height: Optional[float] = None
    depth: Optional[float] = None


@dataclass
class DimensionsType:
    dimensions: DimensionsTypeContents = DimensionsTypeContents()

    def check(self):
        return self


def dimensions_type_entry(
    radius: Optional[float] = None,
    width: Optional[float] = None,
    height: Optional[float] = None,
    depth: Optional[float] = None,
) -> DimensionsType:

    return DimensionsType(DimensionsTypeContents(radius, width, height, depth))


GEOMETRICAL_SHAPES = [
    "SPHERE",
    "SOFT SPHERE",
    "GAUSSIAN",
    "CIRCLE",
    "RECTANGLE",
    "CYLINDER",
    "OTHER",
    None,
]


@dataclass
class BackgroundMaskTypeContents:
    geometrical_shape: Optional[str] = None
    dimensions: Optional[DimensionsType] = None
    software_list: Tuple[SoftwareType, ...] = ()
    details: Optional[str] = None


@dataclass
class BackgroundMaskType:
    background_masked: BackgroundMaskTypeContents = BackgroundMaskTypeContents()

    def check(self):
        errors = []
        if not software_list_is_valid(self.background_masked.software_list):
            errors.append("Software list must contain SoftwareType objects")
        if type(self.background_masked.dimensions) not in (
            type(None),
            DimensionsType,
        ):
            errors.append("dimensions must be DimensionsType or None")
        if self.background_masked.geometrical_shape not in GEOMETRICAL_SHAPES:
            errors.append("Invalid entry for geometrical shape")
        if len(errors) > 0:
            raise ValueError(", ".join(errors))
        return self


def background_mask_type_entry(
    geometrical_shape: Optional[str] = None,
    dimensions: Optional[DimensionsType] = None,
    software_list: Tuple[SoftwareType, ...] = (),
    details: Optional[str] = None,
) -> BackgroundMaskType:

    return BackgroundMaskType(
        BackgroundMaskTypeContents(
            geometrical_shape,
            dimensions,
            software_list,
            details,
        )
    )


@dataclass
class SpatialFilteringTypeContents:
    low_frequency_cutoff: Optional[float] = None
    high_frequency_cutoff: Optional[float] = None
    filter_function: Optional[str] = None
    software_list: Tuple[SoftwareType, ...] = ()


@dataclass
class SpatialFilteringType:
    spatial_filtering: SpatialFilteringTypeContents = SpatialFilteringTypeContents()

    def check(self):
        if not software_list_is_valid(self.spatial_filtering.software_list):
            raise ValueError("Software list must contain SoftwareType objects")
        return self


def spatial_filtering_type_entry(
    low_frequency_cutoff: Optional[float] = None,
    high_frequency_cutoff: Optional[float] = None,
    filter_function: Optional[str] = None,
    software_list: Tuple[SoftwareType, ...] = (),
) -> SpatialFilteringType:

    return SpatialFilteringType(
        SpatialFilteringTypeContents(
            low_frequency_cutoff,
            high_frequency_cutoff,
            filter_function,
            software_list,
        )
    )


@dataclass
class SharpeningTypeContents:
    software_list: Tuple[SoftwareType, ...] = ()
    details: Optional[str] = None


@dataclass
class SharpeningType:
    sharpening: SharpeningTypeContents = SharpeningTypeContents()

    def check(self):
        if not software_list_is_valid(self.sharpening.software_list):
            raise ValueError("software_list can only contain SoftwareType")
        return self


def sharpening_type_entry(
    software_list: Tuple[SoftwareType, ...] = (),
    details: Optional[str] = None,
) -> SharpeningType:

    return SharpeningType(
        SharpeningTypeContents(
            software_list,
            details,
        )
    )


@dataclass
class BfSharpeningTypeContents:
    brestore: Optional[float] = None
    software_list: Tuple[SoftwareType, ...] = ()
    details: Optional[str] = None


@dataclass
class BfSharpeningType:
    """Actual name is: b-factorSharpening"""

    bfactorSharpening: BfSharpeningTypeContents = BfSharpeningTypeContents()

    def check(self):
        if not software_list_is_valid(self.bfactorSharpening.software_list):
            raise ValueError("software_list can only contain SoftwareType")
        return self


def bf_sharpening_type_entry(
    brestore: Optional[float] = None,
    software_list: Tuple[SoftwareType, ...] = (),
    details: Optional[str] = None,
) -> BfSharpeningType:

    return BfSharpeningType(BfSharpeningTypeContents(brestore, software_list, details))


@dataclass
class ReconstructionFilteringTypeContents:
    background_masked: Optional[BackgroundMaskType] = None
    spatial_filtering: Optional[SpatialFilteringType] = None
    sharpening: Optional[SharpeningType] = None
    bfactorSharpening: Optional[BfSharpeningType] = None
    other: Optional[OtherType] = None


@dataclass
class ReconstructionFilteringType:
    reconstruction_filtering: ReconstructionFilteringTypeContents = (
        ReconstructionFilteringTypeContents()
    )

    def check(self):
        errors = []
        if type(self.reconstruction_filtering.background_masked) not in (
            BackgroundMaskType,
            type(None),
        ):
            errors.append("background_masked must be BackgroundMaskedType or None")
        if type(self.reconstruction_filtering.sharpening) not in (
            SharpeningType,
            type(None),
        ):
            errors.append("sharpening must be SharpeningType or None")
        if type(self.reconstruction_filtering.bfactorSharpening) not in (
            BfSharpeningType,
            type(None),
        ):
            errors.append("bfactor_sharpening must be SharpeningType or None")
        if type(self.reconstruction_filtering.other) not in (
            OtherType,
            type(None),
        ):
            errors.append("other must be OtherType or None")
        if type(self.reconstruction_filtering.spatial_filtering) not in (
            SpatialFilteringType,
            type(None),
        ):
            errors.append("spatial_filtering must be SpatialFilteringType or None")
        if len(errors) > 0:
            raise ValueError(", ".join(errors))
        return self


def reconstruction_filtering_type_entry(
    background_masked: Optional[BackgroundMaskType] = None,
    spatial_filtering: Optional[SpatialFilteringType] = None,
    sharpening: Optional[SharpeningType] = None,
    bfactorsharpening: Optional[BfSharpeningType] = None,
    other: Optional[OtherType] = None,
) -> ReconstructionFilteringType:

    return ReconstructionFilteringType(
        ReconstructionFilteringTypeContents(
            background_masked,
            spatial_filtering,
            sharpening,
            bfactorsharpening,
            other,
        )
    )


@dataclass
class HelicalParamsTypeContents:
    delta_z: Optional[float] = None
    delta_phi: Optional[float] = None
    axial_symmetry: Optional[str] = None


@dataclass
class HelicalParamsType:
    helical_parameters: HelicalParamsTypeContents = HelicalParamsTypeContents()

    def check(self):
        return self


def helical_params_type_entry(
    delta_z: Optional[float] = None,
    delta_phi: Optional[float] = None,
    axial_symmetry: Optional[str] = None,
) -> HelicalParamsType:

    return HelicalParamsType(
        HelicalParamsTypeContents(delta_z, delta_phi, axial_symmetry)
    )


@dataclass
class SymmetryTypeContents:
    space_group: Optional[int] = None
    point_group: Optional[str] = None
    helical_parameters: Optional[HelicalParamsType] = None


@dataclass
class SymmetryType:
    applied_symmetry: SymmetryTypeContents = SymmetryTypeContents()

    def check(self):
        if type(self.applied_symmetry.helical_parameters) not in (
            type(None),
            HelicalParamsType,
        ):
            raise ValueError("helical_parameters must be None or HelicalParamsType")
        return self


def symmetry_type_entry(
    space_group: Optional[int] = None,
    point_group: Optional[str] = None,
    helical_parameters: Optional[HelicalParamsType] = None,
) -> SymmetryType:

    return SymmetryType(
        SymmetryTypeContents(space_group, point_group, helical_parameters)
    )


RECON_ALGORITHM_TYPES = [
    "ALGEBRAIC (ARTS)",
    "BACK PROJECTION",
    "EXACT BACK PROJECTION",
    "FOURIER SPACE",
    "SIMULTANEOUS ITERATIVE (SIRT]",
    None,
]

RESO_METHODS = [
    "DIFFRACTION PATTERN/LAYERLINES",
    "FSC 0.143 CUT-OFF",
    "FSC 0.33 CUT-OFF",
    "FSC 0.5 CUT-OFF",
    "FSC 1/2 BIT CUT-OFF",
    "FSC 3 SIGMA CUT-OFF",
    "OTHER",
    None,
]


@dataclass
class FinalReconstructionTypeContents:
    number_of_classes_used: Optional[int] = None
    applied_symmetry: Optional[SymmetryType] = None
    algorithm: Optional[str] = None
    resolution: Optional[float] = None
    resolution_method: Optional[str] = None
    reconstruction_filtering: Optional[ReconstructionFilteringType] = None


@dataclass
class FinalReconstructionType:
    final_reconstruction: FinalReconstructionTypeContents = (
        FinalReconstructionTypeContents()
    )

    def check(self):
        errors = []
        if type(self.final_reconstruction.applied_symmetry) not in (
            SymmetryType,
            type(None),
        ):
            errors.append("applied_symmetry must be SymmetryType or None")
        if self.final_reconstruction.resolution_method not in RESO_METHODS:
            errors.append("Invalid resolution_method")
        if self.final_reconstruction.algorithm not in RECON_ALGORITHM_TYPES:
            errors.append("Invalid algorithm")
        if type(self.final_reconstruction.reconstruction_filtering) not in (
            type(None),
            ReconstructionFilteringType,
        ):
            errors.append(
                "reconstruction_filtering must be ReconstructionFilteringType or None"
            )
        if len(errors) > 0:
            raise ValueError(", ".join(errors))
        return self


def final_reconstruction_type_entry(
    number_of_classes_used: Optional[int] = None,
    applied_symmetry: Optional[SymmetryType] = None,
    algorithm: Optional[str] = None,
    resolution: Optional[float] = None,
    resolution_method: Optional[str] = None,
    reconstruction_filtering: Optional[ReconstructionFilteringType] = None,
) -> FinalReconstructionType:

    return FinalReconstructionType(
        FinalReconstructionTypeContents(
            number_of_classes_used,
            applied_symmetry,
            algorithm,
            resolution,
            resolution_method,
            reconstruction_filtering,
        )
    )
