#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

"""These utilities are used by the pipeliner for basic tasks such as nice looking
on-screen display, checking file names, and getting directory and file names"""

from datetime import datetime
from pathlib import Path
from typing import List, Tuple
import os
import subprocess
import sys
import re


def touch(filename: str):
    """Create an empty file

    Args:
        filename (str): The name for the file to create
    """
    open(filename, "w").close()


def get_pipeliner_root() -> Path:
    """Get the directory of the main pipeliner module

    Returns:
        :class:`~pathlib.Path`: The path of the pipeliner
    """
    return Path(__file__).parent


def truncate_number(number: float, maxlength: int) -> str:
    """Return a number with no more than x decimal places but no trailing 0s

    This is used to format numbers in the exact same way that RELION does it.
    IE: with maxlength 3; 1.2000 = 1.2, 1.0 = 1, 1.23 = 1.23. RELION commands are happy
    to accept numbers with any number of decimal places or trailing 0s. This
    function is just to maintain continuity between RELION and pipeliner
    commands

    Args:
        number (float): The number to be truncated
        maxlength (int): The maximum number of decimal places
    """

    rounded = round(number, maxlength)
    if int(rounded) == rounded:
        return str(int(rounded))
    return str(rounded)


def decompose_pipeline_filename(fn_in: str) -> Tuple[str, int, str]:
    """Breaks a job name into usable pieces

    Returns everything before the job number, the job number as an int
    and everything after the job number setup for up to 20 dirs deep.
    The 20 directory limit is from the relion code but no really necessary anymore

    Args:
        fn_in (str): The job or file name to be broken down in the format:
            <jobtype>/jobxxx/<filename>

    Returns:
        tuple: The decomposed file name: (:class:`str`, :class:`int`, :class:`str`)
            `[0]` Everything before 'job' in the file name

            `[1]` The job number

            `[2]` Everything after the job number

    Raises:
        ValueError: If the input file name is more than 20 directories deep
    """

    fn_split = fn_in.split("/")
    # > 20 dirs doesn't really need to be an error anymore with the way it
    # is being done now.
    if len(fn_split) > 20:
        raise ValueError(
            "decomposePipelineFileName: BUG or found more than 20"
            " directories deep structure for pipeline filename: " + fn_in
        )
    # return everything before job number, job number, and everything after
    i = 0
    for chunk in fn_split:
        if len(chunk) == 6 and chunk[0:3] == "job":
            try:
                fn_jobnr = int(chunk[3:])
            except ValueError:
                fn_jobnr = 0
            if fn_jobnr:
                fn_pre = "/".join(fn_split[:i])
                fn_post = "/".join(fn_split[i + 1 :])
                return fn_pre, fn_jobnr, fn_post
        i += 1
    # return just the file name with jobnumber 0 if not a pipeliner file
    return "", 0, fn_in


def date_time_tag(compact: bool = False) -> str:
    """Get a current date and time tag

    It can return a compact version or one that is easier to read

    Args:
        compact (bool): Should the returned tag be in the compact form

    Returns:
        str: The datetime tag

        compact format is: `YYYYMMDDHHMMSS`

        verbose form is: `YYYY-MM-DD HH:MM:SS`
    """
    now = datetime.now()
    if compact:
        date_time = now.strftime("%Y%m%d%H%M%S")
    else:
        date_time = now.strftime("%Y-%m-%d %H:%M:%S")
    return date_time


def check_for_illegal_symbols(
    check_string: str, string_name: str = "input", exclude: str = ""
):
    """Check a text string doesn't have any of the disallowed symbols.

    Illegal symbols are !*?()^/\\#<>&%{}$."' and @.

    Args:
        check_string (str): The string to be checked
        string_name (str): The name of the string being checked; for more informative
            error messages
        exclude (str): Any symbols that are normally in the illegal symbols list but
            should be allowed.
    Returns:
        str: An error message if any illegal symbols are present
    """
    badsym = ""
    illegal = [
        "!",
        "*",
        "?",
        "(",
        ")",
        "^",
        "/",
        "\\",
        "|",
        "#",
        "<",
        ">",
        "&",
        "%",
        "{",
        "}",
        "$",
        ",",
        '"',
        "'",
        "@",
        ":",
    ]
    for symbol in exclude:
        illegal.remove(symbol)
    for symbol in illegal:
        if symbol in check_string:
            badsym += symbol
    if len(badsym) > 0:
        return (
            f"ERROR: Symbol(s) '{''.join(badsym)}' in {string_name}. {string_name}"
            f" cannot contain any of the following symbols: {' '.join(illegal)}"
        )


def clean_jobname(jobname: str) -> str:
    """Makes sure job names are in the correct format

    Job names must have a trailing slash, cannot begin with a slash,
    and have no illegal characters

    Args:
        jobname (str): The jon name to be checked

    Returns:
        str: The job name, with corrections in necessary

    """
    # fix missing trailing slashes
    if jobname[-1] != "/":
        jobname = jobname + "/"

    # fix leading slashes
    if jobname[0] == "/":
        jobname = jobname[1:]

    # fix double slashes
    if "//" in jobname:
        jobname = "".join(
            jobname[i]
            for i in range(len(jobname))
            if i == 0 or not (jobname[i - 1] == jobname[i] and jobname[i] == "/")
        )

    # return error if illegal characters present
    error_message = check_for_illegal_symbols(jobname, "job name", "/")
    if error_message:
        raise ValueError(error_message)
    return jobname


def get_job_number(job_name):
    jn = clean_jobname(job_name)
    raw = jn.split("/")[-2]
    return int(raw.strip("job"))


def quotate_command_list(commands: List[list]) -> List[list]:
    """Adds quotation marks to command arguments that need them

    If a command is to be run in terminal some args need to be quotated.  Quotation
    marks are not needed if the command list is run with subprocess.run but they are if
    the command is run as a string in a qsub script or in the terminal

    Any arg that contains a space or the set of characters !*?()^#<>&%{}$@ will be
    quotated

    Args:
        commands (list): The commands are a list of lists. Each item in the main list
            is a single command, which itself is a list of the individual arguments

    Returns:
        list: A correctly quotated command list

        The list is in the same list of lists format
    """
    quote_chars = [
        "!",
        "*",
        "?",
        "(",
        ")",
        "^",
        "#",
        "<",
        ">",
        "&",
        "%",
        "{",
        "}",
        "$",
        "@",
        ":",
    ]
    quoted_commands: List[List[str]] = []
    for n, com in enumerate(commands):
        quoted_commands.append([])
        for arg in com:
            if any(item in arg for item in quote_chars):
                quoted_commands[n].append(f'"{arg}"')
            else:
                quoted_commands[n].append(arg)
    return quoted_commands


def print_nice_columns(
    list_in: List[str], err_msg: str = "ERROR: No items in input list"
):
    """Takes a list of items and makes three columns for nicer on-screen display

    Args:
        list_in (str): The list to display in columns
        err_msg (str): The message to display if the list is empty
    """

    if len(list_in) == 0:
        print(f"\n{err_msg}")
        return

    list_in.sort()
    if len(list_in) <= 10:
        for i in list_in:
            print(i)
        print("\n")
        return

    third = int(len(list_in) / 3)
    chunk1 = list_in[0:third]
    c1 = max([len(x) for x in chunk1]) + 2
    chunk2 = list_in[third : 2 * third]
    c2 = max([len(x) for x in chunk2]) + 2
    chunk3 = list_in[2 * third :]
    c3 = max([len(x) for x in chunk3]) + 2
    comb_data = zip(chunk1, chunk2, chunk3)

    spacer = ["", ""]
    if len(chunk1) < len(chunk3):
        chunk1 += spacer
        chunk2 += spacer
    elif len(chunk3) < len(chunk1):
        chunk3 += spacer

    for row in comb_data:
        print(f"{row[0].ljust(c1)} {row[1].ljust(c2)} {row[2].ljust(c3)}")
    print("\n")


def make_pretty_header(
    text: str, char: str = "-=", top: bool = True, bottom: bool = True
):
    """Make nice looking headers for on-screen display

    Args:
        text (str): The text to put in the header
        char (str): What characters to use for the header

    Returns:
        str: A nice looking header
    """
    length = max([len(x) for x in text.split("\n")])
    if len(char) > 1:
        tb = bb = char * int(length / len(char))
        mod = length % len(char)
        tb += char[0:mod]
        bb += char[0:mod]

    else:
        tb = bb = "-" * length
    tb += "\n"
    bb = f"\n{bb}"

    tb = tb if top else ""
    bb = bb if bottom else ""

    return f"{tb}{text}{bb}"


def wrap_text(text_string: str):
    """Produces <= 55 character wide wrapped text for on-screen display

    Args:
        text_string (str): The text to be displayed
    """
    n = 0
    printed = 0
    text_split = text_string.split(" ")
    while printed <= len(text_split) - 1:
        line_string = ""
        length = 0
        while length < 55 and printed <= len(text_split) - 1:
            line_string += text_split[n] + " "
            length += len(text_split[n])
            if "\n" in text_split[n]:
                length = 0
            printed += 1
            n += 1
        print(line_string)


WINDOWS_LINE_ENDING = b"\r\n"
UNIX_LINE_ENDING = b"\n"


def fix_newlines(file_path: str):
    """Replace LF+CR new lines in files with LF, because RELION doesn't like them

    Args:
        file_path (str): The file to fix
    """
    with open(file_path, "rb") as open_file:
        content = open_file.read()

    content = content.replace(WINDOWS_LINE_ENDING, UNIX_LINE_ENDING)

    with open(file_path, "wb") as open_file:
        open_file.write(content)


def find_common_string(input_strings: List[str]) -> str:
    """Find the common part of a list of strings starting from the beginning

    Args:
        input_strings (list): List of strings to compare

    Returns:
        str: The common portion of the strings

    Raises:
        ValueError: If input_list is shorter than 2
    """

    common = ""
    for n, character in enumerate(list(input_strings[0])):
        for the_str in input_strings:
            if list(the_str)[n] != character:
                return common
        common += character
    return common


def smart_strip_quotes(in_string: str) -> str:
    """Strip the quotes from a string in an intelligent manner

    Remove leading and ending ' and " but don't remove them internally

    Args:
        in_string (str): The input string

    Returns:
        str: the string with leading and ending quotes removed
    """
    if len(in_string) == 0:
        return ""
    if in_string[0] in ["'", '"']:
        in_string = in_string[1:]
    if in_string[-1] in ["'", '"']:
        in_string = in_string[:-1]
    return in_string


def run_subprocess(*args, **kwargs):
    # Check if we are in a Pyinstaller bundle, and if so, reverse its changes to the
    # LD_LIBRARY_PATH
    # See https://pyinstaller.org/en/stable/runtime-information.html
    env = None
    if getattr(sys, "frozen", False) and hasattr(sys, "_MEIPASS"):
        env = dict(os.environ)  # make a copy of the environment
        lp_key = "LD_LIBRARY_PATH"  # for GNU/Linux and *BSD.
        lp_orig = env.get(lp_key + "_ORIG")
        if lp_orig is not None:
            env[lp_key] = lp_orig  # restore the original, unmodified value
        else:
            # This happens when LD_LIBRARY_PATH was not set.
            # Remove the env var as a last resort:
            env.pop(lp_key, None)
    return subprocess.run(*args, env=env, **kwargs)


def str_is_hex_colour(in_string) -> bool:
    """Test that a string is a hexadecimal colour code

    Valid codes consist of a # symbol followed by exactly six hexadecimal digits
    (0-9 or a-f, lower or upper case).

    Args:
        in_string (str): The string to test

    Returns:
        bool: is it a valid colour code?

    """
    reg = re.compile("^#([A-Fa-f0-9]{6})$")
    if reg.match(in_string):
        return True
    return False
